import { ShUser } from 'Shared/Model/User/ShUser';
import { Debug, PromiseUtil } from 'Shared/Core/ShUtil';
import { Model } from 'Shared/Model/ShModel';
import { eido } from 'Shared/Core/ShCore';
import { BaseObject } from 'Shared/Core/BaseObject';
import { CObject } from 'Shared/Core/CObject';
import { SecMaster } from 'Shared/Model/ShData';

export class ClData {
    markets             : any = {};
    secMaster           : any = {};

    constructor() {
    }

    getMarket(marketId) {
        if (!this.markets[marketId]) {
            this.markets[marketId] = {
                classifications: {}
            };
        }

        return this.markets[marketId];
    }

    getSecMaster(market: string): Promise<SecMaster> {
        /// If we have already loaded the secMaster for this market, then just return that ...
        if (this.secMaster[market])
            return PromiseUtil.resolvedPromise(this.secMaster[market]);

        /// Otherwise we load it up
        return CObject.post('[apiRoot][apiVersion]/dataAPI/getSecMaster', {
            market: market
        }).then((securities: SecMaster) => {
            this.secMaster[market] = securities;
            return securities;
        });
    }

    findSecurity(search: string) {
        return this.getSecMaster
    }

    getFloatData(market: string, universeId: string, figis: string[], dataIds: string[], startDate: number, endDate: number) {
        return CObject.post('[apiRoot][apiVersion]/dataAPI/getFloat', {
            market: market,
            universeId: universeId,
            startDate: startDate,
            endDate: endDate,
            figis: figis.join(','),
            dataIds: dataIds.join(',')
        });
    }

    getUniverseClassifications(marketId, universeId, dataId) {
        if (!dataId)
            dataId = "classification.sector";

        let market = this.getMarket(marketId);

        if (market.classifications[dataId])
            return PromiseUtil.resolvedPromise(market.classifications[dataId]);

        return CObject["post"]('[apiRoot][apiVersion]/dataAPI/getClassificationForUniverse', {
            market: marketId,
            universeId: universeId,
            dataId: dataId
        }).then((data) => {
            market.classifications[dataId] = data;
            return data;
        });
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /// Static methods
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
}

export default function init() {
    console.info("[Model] Init ClData");

    // initialize the model!
    eido.dataCache = new ClData();
}
