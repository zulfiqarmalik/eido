import { ShUser } from 'Shared/Model/User/ShUser';
import { Debug, PromiseUtil } from 'Shared/Core/ShUtil';
import { Model } from 'Shared/Model/ShModel';
import { eido } from 'Shared/Core/ShCore';
import { UserPreferences } from './ClUserPreferences';
import WSClient from 'Shared/Core/WSClient';
import { ModelObject, ModelValue, BaseObject, CopyOptions, ModelClassId } from 'Shared/Core/BaseObject';
import { CObject } from 'Shared/Core/CObject';

@ModelObject()
export class User extends ShUser {
    @ModelClassId(User, "User") __dummy: string;

    //////////////////////////////////////////////////////////////////////////////
    /// Not part of the model
    //////////////////////////////////////////////////////////////////////////////
    private static _user        : User = null;
    private _wsClient           : WSClient = null;
    // private _prefs              : UserPreferences = new UserPreferences();

    get wsClient(): WSClient    { return this._wsClient;    }
    // get prefs(): UserPreferences{ return this._prefs;       }
    static get user(): User     { return User._user;        }
    static get current(): User  { return User._user;        }
    
    copyFrom(src: BaseObject, options?: CopyOptions) {
        // let prefs = this._prefs;
        super.copyFrom(src, options);
        // this._prefs = prefs;
        // this._prefs._id = this._id;
        return this;
    }

    logout() {
        console.log("Logging the user out ...");
        
        /// Delete the access token first of all 
        User.deleteAccessToken();
        return this.delete('[apiRoot][apiVersion]/user', {})
            .then(() => {
                /// Clear the local storage
                window.localStorage.clear();

                if (this._wsClient && this._wsClient.isConnected()) {
                    console.log("Disconnecting WS Client ...");
                    this._wsClient.disconnect();
                }

                this._wsClient = null;
            });
    }
    
    setProfileImage() {
    }

    loadSettings(keys: string[], defaultValues: string[]): Promise<string[]> {
        let promises: Promise<string>[] = new Array<Promise<string> >(keys.length);

        keys.map((key, i) => {
            promises[i] = this.loadSetting(key, defaultValues[i]);
        });

        return Promise.all(promises);
    }

    loadSetting(key: string, defaultValue: string): Promise<string> {
        let value: string = window.localStorage.getItem(key);

        if (typeof value === undefined || value === null) {
            /// Try to get it from the server ...
            return this.get('[apiRoot][apiVersion]/UserSetting', {
                key: key, 
                defaultValue: defaultValue
            }).then((result: any) => {
                window.localStorage.setItem(result.key, result.value);
                return result.value;
            });
        }

        return PromiseUtil.resolvedPromise(value);
    }

    saveSetting(key: string, value: string): Promise<string> {
        /// Set in the local cache
        window.localStorage.setItem(key, value);
        return this.put('[apiRoot][apiVersion]/UserSetting', {
            key: key,
            value: value
        }).then(() => {
            return value;
        });
    }
    
    // updateFromDiff(diff) {
    //     return CObject.patch('[apiRoot][apiVersion]/user', {
    //         diff: diff
    //     }).then(function(user) {
    //         // update the value in the cache to the new one ...
    //         eido._cached.user = user;
    //         return eido._cached.user;
    //     });
    // }

    // update(data) {
    //     return CObject.patch('[apiRoot][apiVersion]/user', {
    //         user: data
    //     });
    // }
    
    bindWSSignals() {
    }

    postLogin() {
        // check to see whether we are already connected ...
        if (this._wsClient && this._wsClient.isConnected())
            return PromiseUtil.resolvedPromise(this);
            
        console.log('[User] Initialising web socket client ...');

        // Debug.assert(this.accessToken, "Invalid user login without any access token!");
        // console.log("[User] Got access token:", this.accessToken, "- Updating local storage ...");
        // User.saveAccessToken(this.accessToken);
        
        // otherwise we setup a new websocket connection ...
        this._wsClient = new WSClient();

        return this._wsClient.connect(this.wssUrl())
            .then(() => {
                console.log('[User] Web socket client initialised');
                this.bindWSSignals();
                return this;
            })
            .catch((err) => {
                console.error('[User] Unable to initialise websocket client');
                throw err;
            });
    }
    
    isCurrentUser() {
        // TODO: Is this needed anymore?
        // Debug.assert(eido._cached.user, 'Invalid current user!');
        // return this._id.equals(eido._cached.user._id) ? true : false;
        return true;
    }
    
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /// Static methods
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    static saveAccessToken(accessToken) {
        if (accessToken)
            localStorage["AT"] = accessToken;
    }

    static deleteAccessToken() {
        delete localStorage["AT"];
    }

    static loadAccessToken() {
        let accessToken = localStorage["AT"];
        if (!accessToken)
            return "";
        return accessToken;
    }

    static resetPassword(email: string) {
        Debug.assert(email, `Invalid email password for resetting: ${email}`);
        
        return CObject["put"]('[apiRoot][apiVersion]/user/resetpassword', {
            email: email 
        });
    }
    
    static login(email: string, password: string) {
        return CObject["post"]('[apiRoot][apiVersion]/user', {
            email: email,
            password: password,
        }).then((user) => {
            User._user = user;
            return user.postLogin();
        });
    }

    static register(email: string, password: string) {
        return CObject.put('[apiRoot][apiVersion]/user', {
            email: email,
            password: password,
        }).then((user) => {
            // User._user = user;
            // return user.postLogin();
            return;
        });
    }

    static changePassword(oldPassword: string, newPassword: string) {
        return CObject["put"]('[apiRoot][apiVersion]/user/password', {
            oldPassword: oldPassword,
            newPassword: newPassword
        });
    }

    static getCurrentFromServer() {
        return CObject["get"]('[apiRoot][apiVersion]/user', {})
            .then((user) => {
                // ok so the user is logged in and we can get the user from the session data ...
                if (user) {
                    User._user = user;
                    return user.postLogin();
                }
                
                return PromiseUtil.resolvedPromise(null);
            });
    }

    static autoLogin() {
        return User.getCurrentFromServer();
    }
}

export default function init() {
    new UserPreferences();

    console.info("[Model] Init User");

    // initialize the model!
    new User();
}
