import { UserPreferences as ShUserPreferences } from '../../../Shared/Model/User/ShUserPreferences';
import { ModelObject, ModelClassId } from '../../../Shared/Core/BaseObject';

@ModelObject()
export class UserPreferences extends ShUserPreferences {
    @ModelClassId(UserPreferences, "UserPreferences") __dummy: string;
}
