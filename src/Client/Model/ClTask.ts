import { eido } from 'Shared/Core/ShCore';
import { ShUtil, Debug } from 'Shared/Core/ShUtil';
import { Model } from 'Shared/Model/ShModel';

import { PushNotificationType } from 'Shared/Core/PushNotificationType';
import Signal from "Shared/Core/Signals";

import { TaskStatus, ShTask } from 'Shared/Model/ShTask';

export class Task extends ShTask {
    static get Model() {
        return {
            data: {
            }
        }
    } 

    constructor() {
        super();
    }
}

export default function init() {
    console.info("[Model] Init ClTask");

    // initialize the model!
    new Task();
}
