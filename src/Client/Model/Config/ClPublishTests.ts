import { ShPublishTests } from 'Shared/Model/Config/ShPublishTests';
import { ModelObject, ModelClassId } from 'Shared/Core/BaseObject';

@ModelObject()
export class PublishTests extends ShPublishTests {
    @ModelClassId(PublishTests, "PublishTests") __dummy: string;
}
