import { PromiseUtil, Debug } from 'Shared/Core/ShUtil';
import { RawStatsType } from 'Shared/Model/Config/ShStrategy';
import { Markets as gMarkets, MarketInfo, ConfigType, RefConfigInfo } from 'Shared/Model/Config/ShAlpha';
import { ObjectID } from "Shared/Core/ObjectID";
import { Positions } from 'Shared/Model/Stats/Positions';
import { PushNotificationType } from 'Shared/Core/PushNotificationType';
import { TaskStatus } from 'Shared/Model/ShTask';
import { Alpha } from './ClAlpha';
import * as path from 'path';
import { InfoCache, AlphaOrStrategy, AlphaOrStrategyInfo } from './ClConfigBase';
import { ModelObject, CopyOptions, ModelClassId } from 'Shared/Core/BaseObject';
import { SecrecyLevel } from 'Shared/Model/SecrecyLevel';
import { Config } from 'Shared/Model/Config/Config';
import { LTStats } from 'Shared/Model/Stats/LTStats';
import { User } from 'Client/Model/User/ClUser';
import { CObject } from 'Shared/Core/CObject';

////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// StrategyInfo: Client side class that helps in managing a list of strategies for the current user and provides
/// several utility functions to ease management and duplication of logic
////////////////////////////////////////////////////////////////////////////////////////////////////////////////
export interface StrategyInfoCache {
    [details: string] : AlphaOrStrategyInfo;
} 

@ModelObject()
export class Strategy extends AlphaOrStrategy {
    @ModelClassId(Strategy, "Strategy") __dummy: string;
    
    static initInfoCache() {
        gMarkets.map((minfo: MarketInfo) => {
            Strategy._infoCache[minfo.name] = null; ///new StrategyInfo(minfo.name, null);
        });
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /// Not part of the Model
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    private static _infoCache   : StrategyInfoCache = {};
    _baseConfig                 : Config = null;
    _config                     : Config = null;
    _positions                  : any = {};
    _alphaStats                 : any = {};
    _positionFiles              : string[] = [];
    _alphas                     : Alpha[];

    getRunStartNotificationId() { return PushNotificationType.strategyRun; }
    getRunFinishedNotificationId() { return PushNotificationType.strategyRun_Finished; }
    getRunStatusNotificationId() { return PushNotificationType.strategyRun_Status; }

    _waitForTask(resolve, reject, endPointName, durationId, task, tickCallback) {
        let self = this; 

        // if (tickCallback) {
            User.user.wsClient.signal(PushNotificationType.taskUpdate).add((ws, notification) => {
                if (tickCallback)
                    tickCallback(notification.percent, notification.statusMessage);
            }, task);
        // }

        User.user.wsClient.signal(PushNotificationType.taskFinished).add((ws, notification) => {
            /// Ok, now that the task is finished, we remove all the bindings ...
            User.user.wsClient.signal(PushNotificationType.taskUpdate).removeAllForContext(task);
            User.user.wsClient.signal(PushNotificationType.taskFinished).removeAllForContext(task);

            /// ... then we analyse the result
            let result = notification.result;

            /// If the task finished properly then we send a success notification with the data ...
            if (notification.status == TaskStatus.finished) {
                resolve(notification.result);
                return;
            }

            /// Otherwise we send a rejection with the result ...
            reject(notification.result);
        }, task);
    }

    _sendPositions(resolve, endPointName, durationId, positions) {
        if (endPointName != "getPositionsICS")
            positions.ic = null; /// This is not really valid unless we're getting ICS!
        this._positions[durationId] = positions;
        resolve(positions);
    }

    static durationId(duration, startDate, endDate) {
        if (startDate && endDate) {
            return duration + "_" + startDate.toString() + "-" + endDate.toString();
        }

        return duration;
    }

    updateAlphaWeight(alphaId: ObjectID, weight: number, reversion: boolean): Promise<Strategy> {
        return CObject.patch(`[apiRoot][apiVersion]/strategy/alpha`, {
            strategyId: this._id.toString(),
            alphaId: alphaId.toString(),
            weight: weight,
            reversion: reversion
        });
    }

    getAllAlphas() {
        if (this._alphas)
            return PromiseUtil.resolvedPromise(this._alphas);

        let allAlphas = super.getAllAlphas();
        let promises = new Array(allAlphas.length);

        allAlphas.map((alphaSec, ai) => {
            let alpha = new Alpha(); 

            alpha._id = ObjectID.create(alphaSec.map.iid);
            alpha.alphaSec = alphaSec;
            alpha.containerId = alphaSec.map.dbName;
            alpha.configType = alphaSec.map.configType || ConfigType.any;
            alpha._didLoad = false;
            alpha._isStrategy = this.isMaster;
            promises[ai] = alpha.load(false, true);
        });

        return Promise.all(promises)
            .then((alphas) => {
                this._alphas = alphas;
                return this._alphas;
            });
    }

    saveBookSize(bookSize: number) {
        return this.patch('[apiRoot][apiVersion]/strategy/booksize', {
            strategyId: this._id.toString(),
            dbName: this.containerId,
            bookSize: bookSize
        }).then(() => { 
            this.setBookSize(bookSize);
            return this;
        });
    }

    addAlpha(alpha: Alpha) {
        return this.put('[apiRoot][apiVersion]/strategy/alpha', {
            strategyId: this._id.toString(),
            dbName: alpha.containerId,
            alphaId: alpha._id.toString(),
            configType: alpha.configType,
            weight: 1.0,
            reversion: alpha.keyStats && alpha.keyStats.ir < 0.0 ? true : false
        }).then((strategy) => { 
            /// Copy everything to our object and then send that back
            this.copyFrom(strategy, new CopyOptions(SecrecyLevel.all));

            return this;
        });
    }

    removeAlpha(alpha: Alpha) {
        return this.delete('[apiRoot][apiVersion]/strategy/alpha', {
            strategyId: this._id.toString(),
            alphaId: alpha._id.toString()
        }).then((strategy) => { 
            /// Copy everything to our object and then send that back
            this.copyFrom(strategy, new CopyOptions(SecrecyLevel.all));

            return this;
        });
    }

    // getAllStats() {
    //     return new eido.Promise((resolve, reject) => {
    //         this.getFinalStats()
    //             .then(() => {
    //                 return this.getRawStats();
    //             })
    //             .then(() => {
    //                 return this.getPreOptStats();
    //             })
    //             .then(() => {
    //                 resolve({
    //                     stats: this._stats, 
    //                     rawStats: this._rawStats, 
    //                     preoptStats: this._preoptStats
    //                 });
    //             });
    //     });
    // }
    
    /// Fully loads the alpha from the DB
    load(loadAllAlphas: boolean = true): Promise<Strategy> {
        /// If all alphas has been loaded then we have the strategy
        if (this._allAlphas)
            return PromiseUtil.resolvedPromise(this);

        Debug.assert(this.containerId, "No containerId for this alpha!");
        
        return this.get('[apiRoot][apiVersion]/alpha', {
            alphaId: this._id.toString(),
            market: this.market,
            dbName: this.containerId,
            noSource: true
        }).then((strategy) => { 
            /// Copy everything to our object and then send that back
            this.copyFrom(strategy, new CopyOptions(SecrecyLevel.all));

            /// Get all the alphas and put them in the cache
            if (loadAllAlphas)
                return this.getAllAlphas();
            return PromiseUtil.resolvedPromise(this);
        }).then(() => this);
    }

    getAllStats(...args): Promise<LTStats> { 
        return super.getAllStats(false);
    }
    
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /// Static methods
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    static create(category: string, name: string, market: string, universeId: string, refConfigfilename: string, isMaster: boolean, delay: number): Promise<AlphaOrStrategyInfo> {
        let endPoint = !isMaster ? '[apiRoot][apiVersion]/strategy' : '[apiRoot][apiVersion]/master';

        Debug.assert(refConfigfilename, `Invalid refConfig supplied while creating a strategy`);

        return CObject.put(endPoint, {
            category: category,
            name: name,
            market: market,
            universeId: universeId,
            refConfigfilename: refConfigfilename,
            delay: delay
        }).then((strategy) => {
            let cacheKey = `${market}.unpublished`;
            Strategy._infoCache[cacheKey].add(strategy);
            return Strategy._infoCache[cacheKey];
            // return strategy;
        });
    }

    static findStratCategory(stratCategories, title) {
        for (let i = 0; i < stratCategories.length; i++) {
            if (stratCategories[i].title == title)
                return stratCategories[i];
        }

        return null;
    }

    static findStrategy(strategies, id) {
        for (let i = 0; i < strategies.length; i++) {
            if (strategies[i]._id == id)
                return strategies[i];
        }

        return null;
    }

    static getStrategy(id: ObjectID): Promise<AlphaOrStrategy> {
        let strategy: Strategy = new Strategy();
        strategy._id = id;
        return strategy.load(true);
    }

    static getPublished(market: string): Promise<AlphaOrStrategyInfo> {
        let cacheKey = `${market}.published`;

        if (Strategy._infoCache[cacheKey])
            return PromiseUtil.resolvedPromise(Strategy._infoCache[cacheKey]);

        return CObject.get('[apiRoot][apiVersion]/strategy/all', {
            market: market,
            isPublished: true
        }).then((strategies: Strategy[]) => {
            Strategy._infoCache[cacheKey] = new AlphaOrStrategyInfo(market, strategies);
            return Strategy._infoCache[cacheKey];
        });
    }

    static getUnPublished(market: string): Promise<AlphaOrStrategyInfo> {
        let cacheKey = `${market}.unpublished`;

        if (Strategy._infoCache[cacheKey])
            return PromiseUtil.resolvedPromise(Strategy._infoCache[cacheKey]);

        return CObject.get('[apiRoot][apiVersion]/strategy/all', {
            market: market,
            isPublished: false
        }).then((strategies: Strategy[]) => {
            Strategy._infoCache[cacheKey] = new AlphaOrStrategyInfo(market, strategies);
            return Strategy._infoCache[cacheKey];
        });
    }
}

export default function init() {
    console.info("[Model] Init Strategy");
    new Strategy();
    // console.log(Strategy.__modelProperties);
}

Strategy.initInfoCache();
