import { PromiseUtil, Debug } from 'Shared/Core/ShUtil';
import { AlphaSubType, ConfigType, AlphaType } from 'Shared/Model/Config/ShAlpha';
import { PublishTests } from './ClPublishTests';
import { PushNotificationType } from 'Shared/Core/PushNotificationType';
import { ConfigSection } from "Shared/Model/Config/ConfigSection";
import { InfoCache, AlphaOrStrategy, AlphaOrStrategyInfo } from './ClConfigBase';
import { ModelObject, CopyOptions, ModelClassId, ModelValue } from 'Shared/Core/BaseObject';
import { SecrecyLevel } from 'Shared/Model/SecrecyLevel';
import { CObject } from 'Shared/Core/CObject';
import { ObjectID } from 'Shared/Core/ObjectID';
import { SortOrder } from 'Shared/Model/ShModel';
import { AlphaFeed } from 'Shared/Model/Config/AlphaFeed';

////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// AlphaInfo: Client side class that helps in managing a list of alphas for the current user and provides
/// several utility functions to ease management and duplication of logic
////////////////////////////////////////////////////////////////////////////////////////////////////////////////
export interface AlphaInfoCache {
    [details: string] : AlphaOrStrategyInfo;
} 

@ModelObject()
export class Alpha extends AlphaOrStrategy {
    @ModelClassId(Alpha, "Alpha") __dummy: string;
    

    static initInfoCache() {
        // gMarkets.map((minfo: MarketInfo) => {
        //     Alpha._infoCache[minfo.name] = new AlphaInfo(minfo.name, null);
        // });
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /// Not part of the Model
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    static _feed                : any = {};
    private static _infoCache   : AlphaInfoCache = {};
    _modified                   : boolean = false;
    _undoMgr                    : any = null;
    _subAlpha                   : Alpha = null;
    _superAlpha                 : Alpha = null;
    
    getRunStartNotificationId() { return PushNotificationType.alphaRun; }
    getRunFinishedNotificationId() { return PushNotificationType.alphaRun_Finished; }
    getRunStatusNotificationId() { return PushNotificationType.alphaRun_Status; }

/// Fully loads the alpha from the DB
    load(needSource: boolean, needsKeyStats: boolean = false): Promise<Alpha> {
        /// Source is the biggest indication that we have the alpha loaded
        // if ((needSource && this.source) || (!needSource && this.keyStats && typeof this.keyStats.ir !== undefined)) {
        //     return PromiseUtil.resolvedPromise(this);
        // }

        Debug.assert(this.containerId, "No containerId for this alpha!");
        
        if (this._didLoad && ((needSource && this.source) || (!needSource)))
            return PromiseUtil.resolvedPromise(this);

        let query: any = {
            alphaId: this._id.toString(),
            dbName: this.containerId,
            configType: this.configType
        };

        if (needsKeyStats)
            query.keyStatsTable = "LT";

        if (needSource === false)
            query.noSource = true;

        return this.get('[apiRoot][apiVersion]/alpha', query).then((alpha) => { 
            /// Copy everything to our object and then send that back
            this.copyFrom(alpha, new CopyOptions(SecrecyLevel.all));
            this._didLoad = true;

            return this;
        })
    }

    addOperation(operation) {
        return this.put('[apiRoot][apiVersion]/alpha/op', {
            alphaId: this._id.toString(),
            body: {
                data: JSON.stringify(operation.cleanClone())
            }
        }).then((alpha) => this.copyFrom(alpha));
    }

    deleteOperation(uuid) {
        return CObject["delete"]('[apiRoot][apiVersion]/alpha/op', {
            alphaId: this._id.toString(),
            uuid: uuid.toString()
        }).then((alpha) => this.copyFrom(alpha));
    }

    delete() {
        return CObject["delete"]('[apiRoot][apiVersion]/alpha', {
            alphaId: this._id.toString(),
        });
    }

    prepareRun() {
        super.prepareRun();
        this._subAlpha = null;
        this._superAlpha = null;
    }

    // publish(ws, callback, finishedCallback) {
    //     console.log(`[Alpha] Publishing the alpha: ${this._id}`);
    //     this.prepareRun();
        
    //     return this.put(`[apiRoot][apiVersion]/${this.getEP()}/publish`, this.fixId({
    //         _id_: this._id.toString()
    //     })).then((info) => {
    //         if (info.subAlphaId) {
    //             console.log(`[Alpha] Tracking sub-alpha: ${this._id}`);
    //             this._subAlpha = (new SubAlpha()).copyFrom(this);
    //             this._subAlpha.prepareRun();
    //             this._subAlpha.trackStatus(ws, callback, finishedCallback);
    //         }
            
    //         if (info.superAlphaId) {
    //             console.log(`[Alpha] Tracking super-alpha: ${this._id}`);
    //             this._superAlpha = (new SuperAlpha()).copyFrom(this);
    //             this._superAlpha.prepareRun();
    //             this._superAlpha.trackStatus(ws, callback, finishedCallback);
    //         }

    //         console.log(`[Alpha] Tracking alpha: ${this._id}`);
    //         return this.trackStatus(ws, callback, finishedCallback);
    //     });
    // }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /// Static methods
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    static create(category: string, type: AlphaSubType, name: string, market: string, universeId: string, delay: number): Promise<Alpha> {
        return CObject["put"]('[apiRoot][apiVersion]/alpha', {
            category: category,
            name: name,
            type: type,
            market: market,
            universeId: universeId,
            delay: delay
        }).then((alpha) => {
            Alpha._infoCache[market].add(alpha);
            return alpha;
        });
    }

    static getAllAlphas(market: string): Promise<AlphaOrStrategyInfo> {
        if (!market) 
            return PromiseUtil.resolvedPromise(new AlphaOrStrategyInfo("", []));

        if (Alpha._infoCache[market]) 
            return PromiseUtil.resolvedPromise(Alpha._infoCache[market]);

        return CObject["get"]('[apiRoot][apiVersion]/alpha/all', { market: market })
            .then((alphas) => {
                let alphaInfo: AlphaOrStrategyInfo = new AlphaOrStrategyInfo(market, alphas);
                Alpha._infoCache[market] = alphaInfo;
                return alphaInfo;
            });
    }

    static loadIds(ids: ObjectID[], needSource: boolean, needsKeyStats: boolean = false): Promise<AlphaOrStrategyInfo> {
        let promises = new Array(ids.length);

        ids.forEach((id: ObjectID, i) => {
            let alpha: Alpha = new Alpha();
            alpha._id = id;
            alpha.configType = ConfigType.any;
            promises[i] = alpha.load(needSource, needsKeyStats);
        });

        return Promise.all(promises).then((alphas: Alpha[]) => {
            let market = alphas.length ? alphas[0].market : "US";
            let alphaInfo: AlphaOrStrategyInfo = new AlphaOrStrategyInfo(market, alphas);
            return alphaInfo;
        }).catch(() => {
            return null;
        });
    }

    static getFeedId(ownerId, market, universeId, startId, count, sortBy, sortOrder) {
        return `${ownerId}_${market}_${universeId}_${startId}_${count}_${sortBy}_${sortOrder}`;
    }
}

// //////////////////////////////////////////////////////////////////////////////////////
// /// SubAlpha: Represents published alpha but on the SUB-Universe
// @ModelObject()
// export class SubAlpha extends Alpha {
//     @ModelClassId(SubAlpha, "SubAlpha") __dummy: string;
    
//     constructor() {
//         super();
//     }

//     isSubAlpha() { return true; }
//     isSuperAlpha() { return false; }

//     getRunStartNotificationId() { return PushNotificationType.subAlphaRun; }
//     getRunFinishedNotificationId() { return PushNotificationType.subAlphaRun_Finished; }
//     getRunStatusNotificationId() { return PushNotificationType.subAlphaRun_Status; }

//     updateFromInfo(info) {
//         return super.updateFromInfo(info);
//     }
// }

// /// SuperAlpha: Represents published alpha but on the SUPER-Universe
// @ModelObject()
// export class SuperAlpha extends Alpha {
//     @ModelClassId(SuperAlpha, "SuperAlpha") __dummy: string;
    
//     constructor() {
//         super();
//     }

//     isSubAlpha() { return false; }
//     isSuperAlpha() { return true; }

//     getRunStartNotificationId() { return PushNotificationType.superAlphaRun; }
//     getRunFinishedNotificationId() { return PushNotificationType.superAlphaRun_Finished; }
//     getRunStatusNotificationId() { return PushNotificationType.superAlphaRun_Status; }

//     updateFromInfo(info) {
//         return super.updateFromInfo(info);
//     }
// }

export default function init() {
    console.info("[Model] Init Alpha");
    new ConfigSection();
    new Alpha();
    new PublishTests();
}

Alpha.initInfoCache();
