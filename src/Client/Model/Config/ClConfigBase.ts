import { Debug, PromiseUtil } from 'Shared/Core/ShUtil';
import { ShAlpha, AlphaType, ConfigType } from 'Shared/Model/Config/ShAlpha';
import { ObjectID } from "Shared/Core/ObjectID";
import { PushNotificationType } from 'Shared/Core/PushNotificationType';
import { TaskStatus } from 'Shared/Model/ShTask';
import { ShStrategy, RawStatsType } from 'Shared/Model/Config/ShStrategy';
import { Stats } from 'Shared/Model/Stats/Stats';
import { LTStats } from 'Shared/Model/Stats/LTStats';
import { ModelObject, ModelClassId } from 'Shared/Core/BaseObject';
import { CObject } from 'Shared/Core/CObject';
import { User } from 'Client/Model/User/ClUser';
import { ClPositions } from '../Strats/ClPositions';
import { Cl } from 'Client/Core/ClCore';
import { AlphaFeed } from 'Shared/Model/Config/AlphaFeed';
import { SortOrder } from 'Shared/Model/ShModel';
import { ConfigSection } from 'Shared/Model/Config/ConfigSection';

let Fuse = require('fuse.js');

export class Category<T extends ShAlpha> {
    name                : string = "";
    objects             : T[] = [];

    constructor(name?: string, objects?: T[]) {
        if (name)
            this.name   = name;
        if (objects)
            this.objects= objects;
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// Alpha/Strategy: Container
////////////////////////////////////////////////////////////////////////////////////////////////////////////////
export class InfoCache<T extends ShAlpha> {
    objects             : T[] = [];
    categories          : any = {};
    idMap               : any = {};
    nameMap             : any = {};
    iedit               : number = 0;
    market              : string = "";
    searchString        : string = "";

    private _searchSet  : any = null;

    constructor(market: string, objects: T[]) {
        this.market = market;
        if (objects) {
            objects.map((alpha, i) => this.add(alpha));
        }
    }

    // static getEP() { 
    //     return 'alpha'; 
    // }
    get length() {
        return this.objects.length;
    }

    makeSearchSet(): any {
        if (this._searchSet)
            return this._searchSet;

        let options = {
            keys: ['name', 'category', 'bucket']
        };
        this._searchSet = new Fuse(this.objects, options);
        return this._searchSet;
    }

    getCategories(): Category<T>[] {
        let categories: Category<T>[] = [];
        let categoryMap: any = {};

        this.objects.map((object: T) => {
            let category: string = object.category;
            if (!categoryMap[category]) {
                let cat = new Category<T>();
                cat.name = category;
                cat.objects.push(object);
                categories.push(cat);
                categoryMap[category] = cat;
            }
            else {
                categoryMap[category].objects.push(object);
            }
        });

        return categories;
    }

    search(searchString: string): T[] {
        if (!searchString) {
            this.searchString = "";
            return [];
        }

        let results: T[] = [];
        let searchSet = this.makeSearchSet();
        let searchResults = searchSet.search(searchString);
        this.searchString = searchString;

        if (searchResults && searchResults.length) {
            results = new Array<T>(searchResults.length);
            searchResults.map((searchResult, index) => {
                results[index] = searchResult;
            });
        }

        return results;
    }

    doesExist(id: ObjectID): boolean {
        return !!this.idMap[id.toString()];
    }

    findIndex(id: ObjectID): number {
        let idStr = id.toString();

        for (let i = 0; i < this.objects.length; i++) { 
            if (this.objects[i] && this.objects[i]._id && this.objects[i]._id.toString() == idStr)
                return i;
        }

        return -1;
    }

    delete(index: number): boolean {
        Debug.assert(index >= 0 && index < this.objects.length, `[Alpha] Invalid index given for removal from the array: ${index} [0, ${this.objects.length})`);
        this.objects.splice(index, 1);
        return true;
    }

    deleteAlpha(alpha: T): boolean {
        let index = this.findIndex(alpha._id);
        if (index < 0)
            return false;
        return this.delete(index);
    }

    add(alpha: T) {
        /// If it already exists then don't add it
        if (this.doesExist(alpha._id))
            return;

        /// invalidate the search set
        this._searchSet = null;

        this.objects.push(alpha);

        this.idMap[alpha._id.toString()] = alpha;
        this.nameMap[alpha.name] = alpha;
        let category = alpha.category;

        if (!this.categories[category])
            this.categories[category] = [alpha];
        else
            this.categories[category].push(alpha);
    }

    findByName(name: string): T {
        return this.nameMap[name];
    }

    findById(id: ObjectID): T {
        return this.idMap[id.toString()];   
    }

    findFirstUnpublishedObject() {
        for (let i = 0; i < this.objects.length; i++) {
            if (!this.objects[i].isPublished)
                return i;
        }

        return 0;
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// Alpha: Client side Alpha class
////////////////////////////////////////////////////////////////////////////////////////////////////////////////
@ModelObject()
export abstract class AlphaOrStrategy extends ShStrategy {
    @ModelClassId(AlphaOrStrategy, "AlphaOrStrategy") __dummy: string;

    _positions          : any = {};
    _dates              : number[];
    _liveStatsIndex     : number = 0;
    _liveStats          : LTStats;
    _stats              : LTStats;
    _rawStats           : LTStats = null;
    _preoptStats        : LTStats = null;
    _didLoad            : boolean = true;
    _isStrategy         : boolean = false;
    _alphaSec           : ConfigSection = null;     /// Alpha section in case this alpha is embedded within a strategy
    get alphaSec() { return this._alphaSec; }
    set alphaSec(value: ConfigSection) { this._alphaSec = value; }
    
    _waitForTask(resolve, reject, endPointName, durationId, task, tickCallback) {
        let self = this; 

        // if (tickCallback) {
            User.user.wsClient.signal(PushNotificationType.taskUpdate).add((ws, notification) => {
                if (tickCallback)
                    tickCallback(notification.percent, notification.statusMessage);
            }, task);
        // }

        User.user.wsClient.signal(PushNotificationType.taskFinished).add((ws, notification) => {
            /// Ok, now that the task is finished, we remove all the bindings ...
            User.user.wsClient.signal(PushNotificationType.taskUpdate).removeAllForContext(task);
            User.user.wsClient.signal(PushNotificationType.taskFinished).removeAllForContext(task);

            /// ... then we analyse the result
            let result = notification.result;

            /// If the task finished properly then we send a success notification with the data ...
            if (notification.status == TaskStatus.finished) {
                resolve(notification.result);
                return;
            }

            /// Otherwise we send a rejection with the result ...
            reject(notification.result);
        }, task);
    }

    _sendPositions(resolve, calculateIC, durationId, positions) {
        if (!calculateIC)
            positions.ic = null; /// This is not really valid unless we're getting ICS!
        this._positions[durationId] = positions;
        resolve(positions);
    }

    abstract getRunStartNotificationId();
    abstract getRunFinishedNotificationId();
    abstract getRunStatusNotificationId();
    abstract load(...args);
    
    get reversion(): boolean {
        if (this._alphaSec)
            return this._alphaSec.map["reversion"] == "true" ? true : false;
        return false;
    }

    set reversion(value: boolean) {
        if (this._alphaSec)
            this._alphaSec.map["reversion"] = value ? "true" : "false";
    }

    get weight(): string {
        if (this._alphaSec)
            return this._alphaSec.map["weight"] || "1.0";
        return "0.0";
    }

    set weight(value: string) {
        if (this._alphaSec)
            this._alphaSec.map["weight"] = value;
    }

    save(market: string, universeId: string, source: string): Promise<AlphaOrStrategyInfo> {
        this.market = market;
        this.setUniverseId(universeId);
        this.source = source;

        return this.patch('[apiRoot][apiVersion]/alpha', {
            alphaId: this._id.toString(),
            market: market,
            universeId: universeId,
            startDate: this.startDate,
            bucket: this.bucket,

            body: {
                data: source
            }
        })
        // .then((alpha: Alpha) => {
        //     // Alpha._infoCache[market].add(alpha);
        //     // // Alpha._alphaInfo.add(alpha);
        //     // return Alpha._infoCache[market];
        //     return AlphaOrStrategy._infoCache[market];
        // });
    } 

    static duplicate(market: string, alphaId: string, name: string, category: string): Promise<AlphaOrStrategy> {
        return this.put('[apiRoot][apiVersion]/alpha/duplicate', {
            alphaId: alphaId,
            name: name,
            category: category,
            market: market
        }).then((alpha: AlphaOrStrategy) => { 
            return alpha;
        });
    }

    publish() {
        /// Alpha and strategies get published the same way ...
        return this.put(`[apiRoot][apiVersion]/alpha/publish`, {
            alphaId: this._id.toString(),
            dbName: this.containerId,
            type: this.type
        }).then((result: any) => {
            /// Assume some values, these will be modified 
            this.isPublished = true;
            this.publishedDate = result.publishedDate || new Date();
        })
    }

    updateFromInfo(info: any) {
        this.isFinished     = info.isFinished;
        this.isCancelled    = info.isCancelled;
        this.isFailed       = info.isFailed;
        this.percent        = info.percent;
        this.exitStatus     = info.exitStatus;
        this.log            = this.log.concat(info.log);

        if (info.livePnl && info.livePnl.length) {
            console.log(`[Config] Num dates: ${info.dates.length} - Num points: ${info.livePnl.length}`);

            if (info.dates && info.dates.length && !this._liveStats.points.length) {
                let numPoints = Math.min(info.dates.length, info.livePnl.length);
                this._liveStats.points = new Array(info.dates.length);
                
                info.dates.map((date, i) => {
                    let pt = new Stats();
                    pt.date = date;
                    pt.pnl = 0.0;
                    pt.bookSize = this.getBookSize();
                    this._liveStats.points[i] = pt;
                });
            }

            for (let i = this._liveStatsIndex; i < info.livePnl.length; i++)
                this._liveStats.points[i].pnl = info.livePnl[i];

                this._liveStatsIndex = info.livePnl.length;

            if (this._liveStats.points.length)
                this._liveStats.startDate = this._liveStats.points[0].date;
        }
    }

    onFinish(wsClient, info) {
    }

    isSubAlpha() { return false; }
    isSuperAlpha() { return false; }

    trackStatus(ws, callback, finishedCallback) {
        ws.signal(this.getRunStatusNotificationId()).add((wsClient, info) => {
            this.updateFromInfo(info);
            callback(this, info.log);
        }, this);
        
        /// Finished signal should only happen once anyway
        ws.signal(this.getRunFinishedNotificationId()).addOnce((wsClient, info) => {
            console.log("Got: alphaRun_Finished");
            this.updateFromInfo(info);
            ws.signal(this.getRunStatusNotificationId()).removeAllForContext(this);
            finishedCallback(this, info.log);
        }, this);

        return this;
    }

    fixId(object) {
        if (object._id_) {
            object[`${this.getEP()}Id`] = object._id_;
            delete object._id_;
        }

        return object;
    }

    prepareRun() {
        this._liveStats = new LTStats();
        this._liveStats._id = this.name;
        this._liveStatsIndex = 0;

        /// If you run a strategy, we just invalidate all the stats ...
        this._stats = null; 
    }

    getEP(): string { 
        return !this._isStrategy ? this.type.toLowerCase() : AlphaType.strategy.toLowerCase();
    }

    prune(): Promise<boolean> {
        // return PromiseUtil.resolvedPromise(true);
        return this.put(`[apiRoot][apiVersion]/alpha/prune`, {
            alphaId: this._id.toString(),
        }).then(() => {
            this.pruned = true;
            return true;
        });
    }

    saveSettings(operations: any, settings: any): Promise<AlphaOrStrategy> {
        console.log(`[Alpha] Saving operations on alpha-${this._id}: ${operations}`);

        return this.put(`[apiRoot][apiVersion]/alpha/settings`, {
            alphaId: this._id.toString(),
            body: {
                operations: operations,
                settings: settings
            }
        }).then((alpha: AlphaOrStrategy) => this.copyFrom(alpha));
    }

    saveOperations(operations: any) {
        console.log(`[Alpha] Saving operations on alpha-${this._id}: ${operations}`);

        return this.put(`[apiRoot][apiVersion]/${this.getEP()}/op`, {
            alphaId: this._id.toString(),
            body: {
                operations: operations
            }
        }).then((alpha: AlphaOrStrategy) => this.copyFrom(alpha));
    }

    run(ws, callback, finishedCallback) {
        console.log(`Running the alpha: ${this._id}`);
        this.prepareRun();

        return this.put(`[apiRoot][apiVersion]/${this.getEP()}/run`, this.fixId({
            _id_: this._id.toString(),
            dbName: this.containerId,
            configType: this.configType
        })).then(() => {
            return this.trackStatus(ws, callback, finishedCallback);
        });
    }

    cancel() {
        return CObject.delete(`[apiRoot][apiVersion]/${this.getEP()}/run`, this.fixId({
            _id_: this._id.toString()
        }));
    }

    hasRawStats(): boolean {
        return this._isStrategy || this.type == AlphaType.strategy;
    }

    getDates() {
        if (this._dates)
            return PromiseUtil.resolvedPromise(this._dates);

        return this.getAllStats()
            .then(() => {
                this._dates = new Array(this._stats.points.length);
                this._stats.points.map((pt, ii) => this._dates[ii] = pt.date);
                return this._dates;
            });
    }

    getRawStats(): Promise<LTStats> {
        if (this._rawStats)
            return PromiseUtil.resolvedPromise(this._rawStats);

        return this.get(`[apiRoot][apiVersion]/strategy/getStats`, this.fixId({
            _id_: this._id.toString(),
            rawStatsType: RawStatsType.raw
        })).then((rawStats) => {
            let rstats = new LTStats();
            rstats.loadObject(rawStats, true);
            this._rawStats = rstats;
            return rstats;
        });
    }

    getPreOptStats(): Promise<LTStats> {
        if (this._preoptStats)
            return PromiseUtil.resolvedPromise(this._preoptStats);

        return this.get(`[apiRoot][apiVersion]/strategy/getStats`, this.fixId({
            _id_: this._id.toString(),
            rawStatsType: RawStatsType.preopt
        })).then((preoptStats) => {
            let rstats = new LTStats();
            rstats.loadObject(preoptStats, true);
            this._preoptStats = rstats;
            return rstats;
        });
    }

    getStats() {
        return this.get(`[apiRoot][apiVersion]/${this.getEP()}/getStats`, this.fixId({
            _id_: this._id.toString(),
            dbName: this.containerId,
            configType: this.configType
        })).then((stats) => {
            this._stats = new LTStats();
            this._stats.loadObject(stats);
            // this._stats = stats;
            this._stats._id = this.name;
            // this._stats.name = this.name;
            return this._stats;
        });
    }

    getAllStats(...args): Promise<LTStats> { 
        /// If the stats have already been loaded then we don't need to do anything (this is an expensive operation) ...
        if (this._stats)
            return PromiseUtil.resolvedPromise(this._stats);

        return this.load(...args) 
            .then(() => this.getStats())
            .catch((err) => {
                console.error(JSON.stringify(err));
                Cl.ui.alert(`No stats - ${this._id}`, `Unable to get stats for: ${this.name}. This could be because the strategy/alpha has never been run.`);
            });
    }

    getPositionsOnDate(date) {
        return this.get(`[apiRoot][apiVersion]/${this.getEP()}/getPositions`, this.fixId({
            _id_: this._id.toString(),
            market: this.market || "US",
            dbName: this.containerId,
            configType: this.configType,
            date: date,
            loadPss: true
        })).then((positions) => {
            this._positions[date] = positions;
            return positions;
        });
    }

    
    getPositions(date: string, tickCallback: any = null) {
        /// First of all we need to get the config
        return this.getPositionsOnDate(date);
    }

    getPositionsDurationDirect(psInfo) {
        let { duration, startDate, endDate, endPointName, tickCallback, calculatePos, calculateIC, resolve, reject } = psInfo;

        let posIndexes = this._stats.getPlotIndexes(duration, 0, "", startDate, endDate);
        posIndexes = this._stats.adjustPlotIndexes(posIndexes.istart, posIndexes.iend);
        
        startDate = this._dates[posIndexes.istart];
        endDate = this._dates[posIndexes.iend];
        
        let durationId = ShAlpha.durationId(duration, startDate, endDate, calculatePos, calculateIC);

        if (this._positions[durationId]) {
            // return Promise.resolvedPromise(this._positions[durationId]); 
            resolve(this._positions[durationId]);
            return;
        }

        return CObject.get(`[apiRoot][apiVersion]/${this.getEP()}/getPositionsNet`, this.fixId({
            _id_: this._id.toString(),
            market: this.market || "US",
            startDate: startDate,
            endDate: endDate,
            dbName: this.containerId,
            configType: this.configType,
            calculatePos: calculatePos,
            calculateIC: calculateIC,
            noAsync: true
        })).then((positions) => {
            Object.setPrototypeOf(positions, ClPositions.prototype);

            if (!positions.isTask()) 
                return this._sendPositions(resolve, calculateIC, durationId, positions);

            this._waitForTask(resolve, reject, endPointName, durationId, positions, tickCallback);
        }).catch((err) => {
            reject(err);
        });
    }

    getPositionsWithICS(duration, startDate, endDate, tickCallback) {
        return new Promise((resolve, reject) => {
            return this.getPositionsDurationDirect({
                duration: duration, 
                startDate: startDate, 
                endDate: endDate, 
                calculatePos: true,
                calculateIC: true,
                resolve: resolve,
                reject: reject,
                tickCallback: tickCallback
            });
        });
    }

    getPositionsDuration(duration, startDate, endDate, tickCallback) {
        return new Promise((resolve, reject) => {
            return this.getPositionsDurationDirect({
                duration: duration, 
                startDate: startDate, 
                endDate: endDate, 
                calculatePos: true,
                calculateIC: true,
                resolve: resolve,
                reject: reject,
                tickCallback: tickCallback
            });
        });
    }
    
    static getFeed(ownerId: ObjectID, market: string, universeId: string, startIndex: number, count: number, sortBy: string, sortOrder: SortOrder, 
        dbName: string, configType: ConfigType, type: AlphaType, delay: number, searchView: string, advSearches: any[]): Promise<AlphaFeed<AlphaOrStrategy>>  {
        // let feedId = Alpha.getFeedId(ownerId, market, universeId, startIndex, count, sortBy, sortOrder);

        // if (Alpha._feed[feedId]) {
        //     return PromiseUtil.resolvedPromise(Alpha._feed[feedId]);
        // }

        let args = {
            ownerId: ownerId,
            market: market,
            universeId: (universeId && universeId != "ALL" ? universeId : ""),
            startId: startIndex,
            count: count,
            sortBy: sortBy,
            sortOrder: sortOrder,
            dbName: dbName,
            type: type,
            configType: configType,
            delay: delay,
            searchView: searchView,
            isPublished: true,
            body: {
                advSearches: advSearches
            }
        };

        return CObject.post('[apiRoot][apiVersion]/alpha/feed', args).then((feed: AlphaFeed<AlphaOrStrategy>) => {
            return feed;
        });
    }
};

export class AlphaOrStrategyInfo extends InfoCache<AlphaOrStrategy> {
    get alphas(): Array<AlphaOrStrategy> { return this.objects; }
    get strategies(): Array<AlphaOrStrategy> { return this.objects; }
}

