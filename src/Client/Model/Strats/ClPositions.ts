import { eido } from '../../../Shared/Core/ShCore';
import { Model } from '../../../Shared/Model/ShModel';
import { Debug, PromiseUtil } from '../../../Shared/Core/ShUtil';
import { ICPoint, InformationCoefficient, EarningsInfo, MiniPnl, PositionPerf, PositionPnl, Position } from '../../../Shared/Model/Stats/Position';
import { Quantile } from '../../../Shared/Model/Stats/Quantile';
import { Positions, MarketCapType } from '../../../Shared/Model/Stats/Positions';
import { ModelObject, ModelClassId } from '../../../Shared/Core/BaseObject';
import { BaseObject } from 'Shared/Core/BaseObject';
import { CObject } from 'Shared/Core/CObject';

@ModelObject()
export class ClPositions extends Positions {
    @ModelClassId(ClPositions, "Positions") __dummy: string;

    _mcapTypes              : string[] = [];

    getDataGeneric(funcName, key, dataId) {
        if (this[key] && this[key].length == this.positions.length)
            return PromiseUtil.resolvedPromise(this[key]);

        return ClPositions[funcName](this.getCFigis(), this.date, this.market, dataId, this.universeId)
            .then((values) => {
                this[key] = values;
                return this[key];
            });
    }

    getPrices(key, dataId) {
        return this.getDataGeneric("getPricesFor", key, dataId || "baseData.adjClosePrice");
    }

    getEarnings(key, dataId) {
        if (this.earnings && this.earnings.length)
            return PromiseUtil.resolvedPromise(this.earnings);

        return ClPositions.getData("getEarningsInfo", this.getCFigis(), 0, this.market, dataId || "wsh.Next_ED", this.universeId, {
            startDate: this.getStartDate(),
            endDate: this.getEndDate()
        }).then((earnings) => {
            this.earnings = earnings;
            return this.earnings;
        });
    }

    getClassificationGeneric(key, dataId) {
        if (this[key] && this[key].length == this.positions.length)
            return PromiseUtil.resolvedPromise(this[key]);

        return eido.dataCache.getUniverseClassifications(this.market, this.universeId, dataId)
            .then((dataMap) => {
                this[key] = new Array(this.positions.length);
                this.positions.map((position, index) => {
                    if (dataMap[position.cfigi]) 
                        this[key][index] = dataMap[position.cfigi];
                    else
                        console.warn(`Unable to find figi: ${position.cfigi}`);
                });

                return this[key];
            });
    }

    getCountries() {
        return this.getClassificationGeneric("countries", this.market == "EU" ? "secData.countryCode3" : "secData.CountryOfRisk");
    }

    getMCapTypes(): string[] {
        if (this._mcapTypes && this._mcapTypes.length == this.positions.length)
            return PromiseUtil.resolvedPromise(this._mcapTypes);

        return this.getMarketCaps()
            .then((mcaps) => {
                this._mcapTypes = new Array(this.positions.length);

                this.positions.map((position, index) => {
                    let mcap = mcaps[index];

                    /// Market caps are in millions in bloomberg dat!
                    if (mcap <= 1000) // This is in millions
                        this._mcapTypes[index] = MarketCapType.small;
                    else if (mcap > 1000 && mcap <= 10000)
                        this._mcapTypes[index] = MarketCapType.mid;
                    else if (mcap > 10000)
                        this._mcapTypes[index] = MarketCapType.large;
                    else
                        this._mcapTypes[index] = MarketCapType.unknown;
                });

                return this._mcapTypes;
            });
    }

    getSectors() {
        return this.getClassificationGeneric("sectors", "secData.sector");
    }

    getIndustry() {
        return this.getClassificationGeneric("industries", "secData.industry");
    }

    getClosePrices() {
        return this.getPrices("closePrices", "baseData.adjClosePrice");
    }

    getVWAPs() {
        return this.getPrices("vwaps", "baseData.adjVWAP");
    }

    getMarketCaps() {
        return this.getPrices("mcaps", "baseData.adjMCap");
    }

    getOpenPrices() {
        return this.getPrices("openPrices", "baseData.adjOpenPrice");
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /// Static methods
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    static sortRestData(data, field, cfigis, indexes) {
        let symbols = data["SYMBOL"];
        let dataValues = data[field];
        let retData = new Array(cfigis.length);

        dataValues.map((value, index) => {
            let figi = symbols[index];
            let dstIndex = indexes[figi];
            if (dstIndex >= 0 && dstIndex < cfigis.length) {
                retData[dstIndex] = value;
            }
        });

        return retData;
    }

    static getData(endPoint: string, info: any, date: number, market: string, dataId: string, universeId: string, params: any = {}) {
        params.figis        = info.values.join(",");
        params.market       = market || "US";
        params.universeId   = universeId || "";
        params.dataId       = dataId;

        if (date)
            params.date         = date;

        return CObject["post"](`[apiRoot][apiVersion]/dataAPI/${endPoint}`, params)
            .then((data) => {
                // return ClPositions.sortRestData(data, field, info.values, info.indexes);
                return data;
            });
    }

    static getPricesFor(info: any, date: number, market: string, dataId: string, universeId: string) {
        return ClPositions.getData("getPrices", info, date, market, dataId || "baseData.adjClosePrice", universeId, {
            unadjusted: "false"
        });
    }

    static getSectorsFor(info: any, date: number, market: string, dataId: string) {
        return ClPositions.getData("getClassification", info, date, market, dataId || "classification.bbSectors", "");
    }
}

export default function init() {
    console.info("[Model] Init ClPositions");

    // initialize the model!
    new ICPoint();
    new InformationCoefficient();
    new EarningsInfo();
    new MiniPnl();
    new PositionPerf();
    new PositionPnl();
    new Position();
    new Quantile();
    new ClPositions();
}
