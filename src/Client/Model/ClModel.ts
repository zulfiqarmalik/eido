import initCore from 'Client/Core/ClCore';

import initClData from './Data/ClData';
import initUser from './User/ClUser';
import initAlpha from './Config/ClAlpha';
import initStrategy from './Config/ClStrategy';
import initConfig from '../../Shared/Model/Config/Config';
import initStats from '../../Shared/Model/Stats/Stats';
import initCumStats from '../../Shared/Model/Stats/CumStats';
import initLTStats from '../../Shared/Model/Stats/LTStats';
import initPositions from './Strats/ClPositions';
import initNotifications from '../../Shared/Model/Notification';
import initTask from './ClTask';
import initGroupPnl from '../../Shared/Model/GroupPnl';
import initShData from 'Shared/Model/ShData'
import initTrade from 'Client/Model/Trade/ClTrade';

export function initModels() {
    console.info("Initialising client side models!");

    initCore();
    initClData();

    initUser();
    initAlpha();
    initStrategy();

    initConfig();
    initPositions();
    initStats();
    initCumStats();
    initLTStats();

    initNotifications();
    initTask();

    initGroupPnl();
    initShData();

    initTrade();
}
