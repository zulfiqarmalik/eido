import { ModelObject, ModelClassId, ModelValue } from "Shared/Core/BaseObject";
import { ShTrade, ShWave, TradeType } from "Shared/Model/Trade/ShTrade"
import { CObject } from "Shared/Core/CObject";

////////////////////////////////////////////////////////////////////////////////////
/// Trade: The main trade object
////////////////////////////////////////////////////////////////////////////////////
@ModelObject()
export class Trade extends ShTrade {
    @ModelClassId(Trade, "Trade") __dummy: string;

    constructor() {
        super();
    }

    static getTrade(market: string, type: TradeType = TradeType.UAT, date: Date = null) {
        return CObject.get('[apiRoot][apiVersion]/trade', {
            market: market,
            type: type,
            date: date
        }).then((trade: Trade) => {
            return trade;
        });
    }
};

////////////////////////////////////////////////////////////////////////////////////
/// Wave: The wave
////////////////////////////////////////////////////////////////////////////////////
@ModelObject()
export class Wave extends ShWave {
    @ModelClassId(Wave, "Wave") __dummy: string;

    constructor() {
        super();
    }

    static getWave(waveId: string) {
        return CObject.get('[apiRoot][apiVersion]/wave', {
            waveId: waveId
        }).then((trade: Trade) => {
            return trade;
        });
    }
};

export default function init() {
    console.info("[Model] Init Trade");
    new Wave();
    new Trade();
}

