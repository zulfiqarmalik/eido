import React from 'react';
import MainNav from "./Routes/MainNav";
import BrowserApp from "./BrowserApp";
import { RouteConfig } from 'react-router-config/index';

export class Route implements RouteConfig {

};

export class Routes {
    static routes: Route[] = [{
        path: "/dk/main",
        exact: true,
        component: {MainNav}
    }, {
        path: "/dk/login",
        exact: true,
        component: "Login"
    }]
}

export let routesInfo = [{
    component: BrowserApp,
    routes:  Routes.routes
}];
