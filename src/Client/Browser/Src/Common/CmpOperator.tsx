import React from 'react';

import MenuItem from 'material-ui/MenuItem';
import SelectField from 'material-ui/SelectField';
import Divider from 'material-ui/Divider';

interface Props {
    onOpChange    : (sortBy: string) => void;
    op              : string;
    label           ?: string;
    width           ?: number;
}

export default class CmpOperator extends React.Component<Props, {}> {
    constructor(props: Props) {
        super(props);
    }

    onOpChange = (event: any, index: number, sortBy: string) => {
        if (this.props.onOpChange) {
            this.props.onOpChange(sortBy);
        }
    }

    render() {
        return (
            <SelectField 
                floatingLabelText={this.props.label ? this.props.label : undefined}
                value={this.props.op} 
                onChange={this.onOpChange} 
                style={{width: this.props.width || 75}}
            >
                <MenuItem key="$gt" value="$gt" primaryText=">" />
                <MenuItem key="$gte" value="$gte" primaryText=">=" />
                <MenuItem key="$lt" value="$lt" primaryText="<" />
                <MenuItem key="$lte" value="$lte" primaryText="<=" />

            </SelectField>
        );
    }
}
