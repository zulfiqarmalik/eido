import React from 'react';
import { RouteComponentProps } from 'react-router';
import { PromiseUtil } from "Shared/Core/ShUtil";
import { ObjectID } from "Shared/Core/ObjectID";
import PnlGraph from './PnlGraph';
import StatsTable_Daily from './StatsTable_Daily';
import StatsTable_Cum from './StatsTable_Cum';
import {Toolbar, ToolbarGroup, ToolbarTitle} from 'material-ui/Toolbar';
import DurationSelect from './DurationSelect';
import FlatButton from 'material-ui/FlatButton';
import * as Colors from 'material-ui/styles/colors';

import {Tabs, Tab} from 'material-ui/Tabs';

import LongShort_Graph from '../Analysis/LongShort_Graph';
import IndustryGraph from '../Analysis/IndustryGraph';
import EarningsGraph from '../Analysis/EarningsGraph';
import ICGraph from '../Analysis/ICGraph';
import QuantileGraph from '../Analysis/QuantileGraph';
import PosGraph from './PosGraph';

import IconMenu from 'material-ui/IconMenu';
import MenuItem from 'material-ui/MenuItem';
import NavigationExpandMoreIcon from 'material-ui/svg-icons/navigation/expand-more';

import {Card, CardHeader, CardMedia, CardTitle, CardText} from 'material-ui/Card';

import { Strategy } from 'Client/Model/Config/ClStrategy';

import Divider from 'material-ui/Divider';
import { LTStats } from 'Shared/Model/Stats/LTStats';
import { StatsDuration, PnlSpace } from 'Shared/Model/Stats/CumStats';
import { AlphaOrStrategyInfo, AlphaOrStrategy } from 'Client/Model/Config/ClConfigBase';

const buttonStyles = {
    height: 55,
    // marginBottom: 5,
    // marginTop: 5,
    // marginLeft: 15,
    // marginRight: 15,
};

const bgColor = "#0097A7";

export interface Props extends RouteComponentProps<any>, React.Props<any> {
    alphaInfo           ?: AlphaOrStrategyInfo;
    alphaId             ?: ObjectID;
    market              ?: string;
    space               ?: string;
    hideDuration        ?: boolean;
    hidePnlSpace        ?: boolean;
    graphColor          ?: string;
    isEmbedded          ?: boolean;
}

export interface State {
    alphaId             : ObjectID;
    alpha               : AlphaOrStrategy;
    stats               : LTStats;
    rawStats            : LTStats;
    preoptStats         : LTStats;
    statsDates          : number[];
    alphaInfo           : AlphaOrStrategyInfo;

    graphColor          : string;
    tabIndex            : number;

    mainStats           ?: boolean;
    analysis            ?: boolean;
    dailyStats          ?: boolean;
    space               ?: PnlSpace;

    activeKey           ?: string;
    title               ?: string;
    positionFiles       ?: string[];
    positionFile        ?: string;
    showPositionSelect  ?: boolean;

    duration            ?: StatsDuration;
    showPnlSpace        ?: boolean;
    startDate           ?: number;
    endDate             ?: number;
}

export default class ViewAlphaResults extends React.Component<Props, State> {
    tabOrder            : string[] = ["mainStats", "analysis", "dailyStats"];

    constructor(props: Props) {
        super(props);

        this.state = {
            alphaId         : this.props.alphaId,
            alpha           : null,
            stats           : null,
            rawStats        : null,
            preoptStats     : null,
            statsDates      : [],
            alphaInfo       : this.props.alphaInfo,

            graphColor      : this.props.graphColor ? this.props.graphColor : Colors.blue400,

            tabIndex        : 0,

            mainStats       : true,
            analysis        : false,
            dailyStats      : false,
            space           : PnlSpace.returns,

            activeKey       : "pnl_full",
            title           : "PnL Graph",
            positionFiles   : [],

            duration        : StatsDuration.LT,
            showPnlSpace    : false,
            startDate       : 0,
            endDate         : 0
        };
    }

    onPositionChange(value) {
        console.log("Additional Graph: ", value);
        this.setState({
            positionFile: value
        });
        // this.updatePosGraph();
    }

    getPositionFile() {
        return this.state.showPositionSelect ? this.state.positionFile : "";
    }

    onSpaceChange = (value: PnlSpace) => {
        this.setState({
            space: value
        });
    }

    onDurationChange = (value: StatsDuration) => {
        this.setState({
            duration: value
        });
    }

    onDatesChanged = (startDate: number, endDate: number) => {
        this.setState({
            startDate: startDate,
            endDate: endDate
        });
    }

    componentDidMount() {
        let alpha = this.state.alphaInfo.findById(this.state.alphaId);

        if (alpha) {
            let stateObj: any = {};

            alpha.getAllStats()
                .then(() => alpha.getDates())
                .then(() => {
                    stateObj = {
                        alpha: alpha,
                        stats: alpha._stats,
                        statsDates: alpha._dates,
                        positionFiles: alpha._dates.map(String),
                        startDate: alpha._stats.getStartDate(),
                        endDate: alpha._stats.getEndDate()
                    };

                    if (alpha.hasRawStats()) {
                        let strategy: Strategy = alpha as Strategy;
                        return Promise.all([strategy.getRawStats(), strategy.getPreOptStats()]);
                    }

                    return PromiseUtil.resolvedPromise([]);
                })
                .then((rawStats: LTStats[]) => {
                    if (rawStats && rawStats.length == 2) {
                        stateObj.rawStats = rawStats[0];
                        stateObj.preoptStats = rawStats[1];
                    }
                    this.setState(stateObj);
                });
        }
    }

    handleChange = (value: number) => {
        let stateObj = {
            tabIndex: value
        };

        this.setState(stateObj);
    };

    getBkColor(key: string): string {
        if (this.state.activeKey == key)
            return bgColor;
        return "";
    }

    renderTitleButton(key: string, name: string) {
        return (
            <FlatButton 
                label={name} 
                backgroundColor={this.getBkColor(key)} 
                hoverColor={bgColor}
                style={buttonStyles} 
                onClick={() => this.setState({ activeKey: key, title: name })}
            />
        );
    }

    renderSubMenuItem(key: string, name: string, hidden: boolean = false) {
        let style: any = {};

        if (this.state.activeKey == key)
            style.backgroundColor = bgColor;

        return (
            <MenuItem 
                value={key} 
                primaryText={name} 
                onClick = {() => this.setState({ activeKey: key, title: name })} 
                style={style}
                hidden={!!hidden}
            />
        );
    }

    renderNavButtons() {
        return (
            <div>
                <IconMenu
                    iconButtonElement={
                        <FlatButton 
                            style={buttonStyles} 
                            label="PnL"
                            hoverColor={bgColor}
                            backgroundColor={this.state.activeKey.startsWith("pnl_") ? bgColor : ""}
                            icon={<NavigationExpandMoreIcon />}
                        />
                    }

                    targetOrigin={{ horizontal: 'left', vertical: 'top' }}
                    anchorOrigin={{ horizontal: 'left', vertical: 'bottom' }}
                >
                    {this.renderSubMenuItem("pnl_full", "PnL Graph")}
                    {this.renderSubMenuItem("pnl_daily", "Daily PnL Table")}
                    {this.renderSubMenuItem("pnl_longShort", "Long/Short PNL")}
                </IconMenu>

                <IconMenu
                    iconButtonElement={
                        <FlatButton 
                        style={buttonStyles} 
                        label="Distributions"
                        hoverColor={bgColor}
                        backgroundColor={this.state.activeKey.startsWith("dist_") ? bgColor : ""}
                        icon={<NavigationExpandMoreIcon />}
                        />
                    }
                    
                    targetOrigin={{ horizontal: 'left', vertical: 'top' }}
                    anchorOrigin={{ horizontal: 'left', vertical: 'bottom' }}
                >
                    {this.renderSubMenuItem("dist_pos", "Positions")}
                    {this.renderSubMenuItem("dist_qnt", "Quantiles")}
                    {this.renderSubMenuItem("dist_ics", "Information Coefficient")}

                    <Divider/>

                    {this.renderSubMenuItem("dist_mcap", "Market Cap")}
                    {this.renderSubMenuItem("dist_countries", "Countries")}
                    {this.renderSubMenuItem("dist_sector", "Sectors")}
                    {this.renderSubMenuItem("dist_industry", "Industry")}
                </IconMenu>

                <IconMenu
                    iconButtonElement={
                        <FlatButton 
                        style={buttonStyles} 
                        label="Details"
                        hoverColor={bgColor}
                        backgroundColor={this.state.activeKey.startsWith("misc_") ? bgColor : ""}
                        icon={<NavigationExpandMoreIcon />}
                        />
                    }
                    
                    targetOrigin={{ horizontal: 'left', vertical: 'top' }}
                    anchorOrigin={{ horizontal: 'left', vertical: 'bottom' }}
                >
                    {this.renderSubMenuItem("misc_earn", "Earnings Performance")}

                    {/* {this.renderSubMenuItem("misc_pub", "Publish Tests", (this.state.alpha && !this.state.alpha.isPublished) || !this.state.alpha)} */}
                </IconMenu>

            </div>
        );
    }

    getKey(name: string): string {
        return `${name}_${this.state.stats ? this.state.alphaId : ""}_${this.state.space}_${this.state.duration}_${this.state.startDate}_${this.state.endDate}`;
    }

    renderKeyStats(type: string, iid: string, stats: LTStats) {
        return (
            <StatsTable_Cum 
                key={`${iid}-${type}`} 
                stats={stats} 
            />
        );
    }

    renderAllKeyStats() {
        return (
            <StatsTable_Cum 
                key={`KeyStatsTable-${this.state.alphaId}`} 
                stats={this.state.stats} 
                rawStats={this.state.rawStats} 
                preoptStats={this.state.preoptStats} 
            />
        );
        // if (!this.state.rawStats || !this.state.preoptStats) {
        //     return this.renderKeyStats("overall", this.state.alphaId.toString(), this.state.stats);
        // }

        // return (
        //     <Tabs>
        //         <Tab label="Overall Stats" value="overall" key={`overall`}>
        //             {this.renderKeyStats("overall", this.state.alphaId.toString(), this.state.stats)}
        //         </Tab>
                
        //         <Tab label="Raw Stats" value="raw" key={`raw`}>
        //             {this.renderKeyStats("raw", this.state.alphaId.toString(), this.state.rawStats)}
        //         </Tab>

        //         <Tab label="Pre-OPT Stats" value="preopt" key={`preopt`}>
        //             {this.renderKeyStats("preopt", this.state.alphaId.toString(), this.state.preoptStats)}
        //         </Tab>
        //     </Tabs>
        // )
    }

    renderPnlGraph() {
        return (
            <Card>
                <CardHeader title="PnL Graph"/>

                <CardMedia>
                    <PnlGraph
                        key={this.getKey("pnl")}
                        stats={this.state.stats}
                        rawStats={this.state.rawStats}
                        preoptStats={this.state.preoptStats}
                        mainColor={this.state.graphColor}
                        space={this.state.space}
                        duration={this.state.duration}
                        startDate={this.state.startDate}
                        endDate={this.state.endDate}
                        useIndividualPlotIndexes={true}
                    />
                </CardMedia>

                <CardTitle title="Key Stats" />

                <CardText>
                    {this.renderAllKeyStats()}
                </CardText>
            </Card>
        );
    }

    renderLongShortGraph() {
        return (
            <LongShort_Graph
                key={this.getKey('lsGraph')}
                location={this.props.location}
                match={this.props.match}
                history={this.props.history}
                stats={this.state.stats}
                duration={this.state.duration}
                positionFile={this.state.positionFile}
                startDate={this.state.startDate}
                endDate={this.state.endDate}
            />
        );
    }

    renderDailyPnl() {
        return (
            <StatsTable_Daily
                key={this.getKey("dpnl")}
                stats={this.state.stats}
                duration={this.state.duration}
                startDate={this.state.startDate}
                endDate={this.state.endDate}
            />
        );
    }

    renderPosGraph() {
        return (
            <PosGraph
                location={this.props.location}
                history={this.props.history}
                match={this.props.match}
                key={this.getKey('positions')}
                strategy={this.state.alpha}
                stats={this.state.stats}
                duration={this.state.duration}
                positionFile={this.state.positionFile}
                startDate={this.state.startDate}
                endDate={this.state.endDate}
            />
        );
    }

    renderEarnings() {
        return (
            <EarningsGraph
                location={this.props.location}
                history={this.props.history}
                match={this.props.match}
                key={this.getKey('earnings')}
                strategy={this.state.alpha}
                stats={this.state.stats}
                type="earnings"
                duration={this.state.duration}
                positionFile={this.state.positionFile}
                startDate={this.state.startDate}
                endDate={this.state.endDate}
            />
        );
    }

    renderQuantiles() {
        return (
            <QuantileGraph
                location={this.props.location}
                match={this.props.match}
                history={this.props.history}
                key={this.getKey('quantiles')}
                strategy={this.state.alpha}
                type="quantile"
                duration={this.state.duration}
                positionFile={this.state.positionFile}
                startDate={this.state.startDate}
                endDate={this.state.endDate}
            />
        );
    }

    renderDistribution(name) {
        name = name.replace("dist_", "");

        return (
            <IndustryGraph
                location={this.props.location}
                match={this.props.match}
                history={this.props.history}
                key={this.getKey(name)}
                strategy={this.state.alpha}
                type={name}
                duration={this.state.duration}
                positionFile={this.state.positionFile}
                startDate={this.state.startDate}
                endDate={this.state.endDate}
            />
        );
    }

    renderICS() {
        return (
            <ICGraph
                location={this.props.location}
                match={this.props.match}
                history={this.props.history}
                key={this.getKey('ics')}
                strategy={this.state.alpha}
                stats={this.state.stats}
                type="ics"
                duration={this.state.duration}
                positionFile={this.state.positionFile}
                startDate={this.state.startDate}
                endDate={this.state.endDate}
            />
        );
    }

    renderChild() {
        if (this.state.activeKey == "pnl_full")
            return this.renderPnlGraph();
        else if (this.state.activeKey == "pnl_daily")
            return this.renderDailyPnl();
        else if (this.state.activeKey == "pnl_longShort")
            return this.renderLongShortGraph();
        else if (this.state.activeKey == "misc_earn")
            return this.renderEarnings();
        else if (this.state.activeKey == "dist_ics")
            return this.renderICS();
        else if (this.state.activeKey == "dist_pos")
            return this.renderPosGraph();
        else if (this.state.activeKey == "dist_qnt")
            return this.renderQuantiles();
        else if (this.state.activeKey.startsWith("dist_")) 
            return this.renderDistribution(this.state.activeKey);
    }

    render() {
        return (
            <div>
                <div className="results">
                    <Toolbar>
                        {this.renderNavButtons()}

                        <ToolbarGroup>
                            <ToolbarTitle text={this.state.title} className="dev-alpha-title" />
                        </ToolbarGroup>

                        <DurationSelect 
                            defaultValue={StatsDuration.LT}
                            defaultSpaceValue={PnlSpace.returns}
                            onDurationChange={this.onDurationChange} 
                            stats={this.state.stats} 
                            showPnlSpace={false}
                            onSpaceChange={this.onSpaceChange}
                            onDatesChanged={this.onDatesChanged}
                            allowIndividualDate={false}
                            allowCustomDate={true}
                        />
                   </Toolbar >
                </div>

                <div className="v-scrollable">
                    {this.renderChild()}
                </div>
            </div>
        );
    }
}
