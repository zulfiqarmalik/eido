// import React from 'react';
// import ReactDOM from 'react-dom';
// import { eido } from "Shared/Core/ShCore";
// import { Debug, ShUtil } from "Shared/Core/ShUtil";
// import ViewAlphaModal from './ViewAlpha';
// import { Redirect, withRouter, RouteComponentProps } from 'react-router';
// import { Link } from 'react-router-dom';
// import { Table } from 'material-ui/Table';

// import { Strategy } from 'Client/Model/Config/ClStrategy';
// import { LTStats } from 'Shared/Model/Stats/LTStats';

// interface TableProps extends RouteComponentProps<any>, React.Props<any> {
//     stats           : LTStats;
//     strategies      : Strategy[];
// }

// export default class ChildStrategiesTable extends React.Component<TableProps, {}> {
//     constructor(props: TableProps) {
//         super(props);
//     }

//     render() {
//         return (
//             (this.props.strategies && this.props.strategies.length) ?
//             <Table key={this.props.strategies && this.props.strategies.length ? this.props.strategies[0].map.uuid : "AlphasTables"} >
//                 <thead>
//                     <tr>
//                         <th><b>Index</b></th>
//                         <th><b>UUID</b></th>
//                         <th><b>Weight</b></th>
//                         <th><b>Universe ID</b></th>
//                     </tr>
//                 </thead>
//                 <tbody>
//                     {
//                         (this.props.strategies ? this.props.strategies : []).map((strategyConfig, index) => {
//                             return <ChildStrategyItem 
//                                 key={strategyConfig.map.uuid} 
//                                 strategyConfig={strategyConfig} 
//                                 index={index} 
//                                 strategy={this.props.strategy}
//                             />;
//                         }
//                     )}
//                 </tbody>
//             </Table>
//             : null
//         );
//     }
// }

// interface ItemState {
//     childStrategy   : Strategy;
// }

// interface ItemProps extends RouteComponentProps<any>, React.Props<any> {
//     key             : string;
//     stats           ?: LTStats;
//     strategies      ?: Strategy[];
//     strategy        ?: Strategy;
//     strategyConfig  ?: any;
// }

// export class ChildStrategyItem extends React.Component<ItemProps, ItemState> {
//     constructor(props: ItemProps) {
//         super(props);

//         this.state = {
//             childStrategy: null
//         };
//     }

//     handleViewStrat(e) {
//         eido._cached.strategy = this.props.strategyConfig;
//     }

//     componentDidMount() {
//         if (!this.strategy || !this.strategy._stratCategory)
//             return;

//         /// We need to find the child strategy config from the same category as the parent/master strategy
//         let strategy = Strategy.findStrategy(this.strategy._stratCategory.strategies, this.props.strategyConfig.map.uuid);
//         this.setState({
//             childStrategy: strategy
//         });
//     }

//     render() {
//         return (
//             <tr>
//                 <td>{this.props.index}</td>
//                 <td>
//                     <Link 
//                         to={this.props.strategy && this.props.strategyConfig ? '/dk/vcat/' + this.props.strategy._stratCategory.title + "/vstrat/" + this.props.strategyConfig.map.uuid : ""} 
//                         onClick={::this.handleViewStrat}>
//                         <b>{this.props.strategyConfig ? this.props.strategyConfig.map.uuid : ""}</b>
//                     </Link>
//                 </td>
//                 <td>{(this.props.strategyConfig && typeof this.props.strategyConfig.map.weight !== 'undefined' ? this.props.strategyConfig.map.weight : 1.0)}</td>
//                 <td>{this.props.strategyConfig ? this.props.strategyConfig.map.universeId : ""}</td>
//             </tr >
//         );
//     }
// }
