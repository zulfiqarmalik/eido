import React from 'react';

import { RouteComponentProps } from 'react-router';
import AlphaOp_Normalise from './AlphaOp_Normalise';
import AlphaOp_Winsorise from './AlphaOp_Winsorise';
import AlphaOp_Neutralise from './AlphaOp_Neutralise';
import AlphaOp_Rank from './AlphaOp_Rank';
import AlphaOp_ZScoreNormalise from './AlphaOp_ZScoreNormalise';
import AlphaOp_Decay from './AlphaOp_Decay';

interface State {
    opData                  : any;
    operation               : string;
}

interface Props extends RouteComponentProps<any>, React.Props<any> {
    className               ?: string;
    key                     : string;
    onOperationChange       ?: (index: number, operation: string) => any;
    operation               ?: string;
    canDelete               ?: boolean;
    canEdit                 ?: boolean;
    onDelete                ?: (index: number) => void;
    opData                  ?: any;
    index                   ?: number;
    mandatory               ?: boolean;
}

export default class AlphaOp extends React.Component<Props, State> {
    constructor(props: Props) {
        super(props);

        this.state = {
            opData: null,
            operation: this.props.opData ? this.props.opData.id : "OpNormalise"
        };
    }

    isNormalise         = () => this.state.operation == "OpNormalise"
    isNeut              = () => this.state.operation == "OpNeutralise"
    isWinsor            = () => this.state.operation == "OpWinsorise"
    isZScoreNorm        = () => this.state.operation == "OpZScoreNormalise"
    isRank              = () => this.state.operation == "OpRank"
    isDecay             = () => this.state.operation == "OpDecay"
    
    onOperationChange   = (operation: string) => {
        this.setState({ 
            operation: operation,
            opData: this.props.onOperationChange(this.props.index, operation)
        });

    }

    onDelete = () => {
        this.props.onDelete(this.props.index);
    }

    renderRow() {
        if (this.isNormalise()) {
            return (
                <AlphaOp_Normalise 
                    location={this.props.location}
                    match={this.props.match}
                    history={this.props.history}
                    onOperationChange={this.onOperationChange} 
                    canDelete={this.props.canDelete}
                    canEdit={this.props.canEdit}
                    onDelete={this.onDelete}
                    mandatory={this.props.mandatory}
                />
            );
        }
        else if (this.isWinsor()) {
            return (
                <AlphaOp_Winsorise 
                    location={this.props.location}
                    match={this.props.match}
                    history={this.props.history}
                    onOperationChange={this.onOperationChange} 
                    opData={this.props.opData}
                    canDelete={this.props.canDelete}
                    canEdit={this.props.canEdit}
                    onDelete={this.onDelete}
                    mandatory={this.props.mandatory}
                />
            );
        }
        else if (this.isNeut()) {
            return (
                <AlphaOp_Neutralise
                    location={this.props.location}
                    match={this.props.match}
                    history={this.props.history}
                    onOperationChange={this.onOperationChange} 
                    opData={this.props.opData}
                    canDelete={this.props.canDelete}
                    canEdit={this.props.canEdit}
                    onDelete={this.onDelete}
                    mandatory={this.props.mandatory}
                />
            );
        }
        else if (this.isZScoreNorm()) {
            return (
                <AlphaOp_ZScoreNormalise
                    location={this.props.location}
                    match={this.props.match}
                    history={this.props.history}
                    onOperationChange={this.onOperationChange} 
                    opData={this.props.opData}
                    canDelete={this.props.canDelete}
                    canEdit={this.props.canEdit}
                    onDelete={this.onDelete}
                    mandatory={this.props.mandatory}
                />
            );
        }
        else if (this.isRank()) {
            return (
                <AlphaOp_Rank
                    location={this.props.location}
                    match={this.props.match}
                    history={this.props.history}
                    onOperationChange={this.onOperationChange} 
                    opData={this.props.opData}
                    canDelete={this.props.canDelete}
                    canEdit={this.props.canEdit}
                    onDelete={this.onDelete}
                    mandatory={this.props.mandatory}
                />
            );
        }
        else if (this.isDecay()) {
            return (
                <AlphaOp_Decay
                    location={this.props.location}
                    match={this.props.match}
                    history={this.props.history}
                    onOperationChange={this.onOperationChange} 
                    opData={this.props.opData}
                    canDelete={this.props.canDelete}
                    canEdit={this.props.canEdit}
                    onDelete={this.onDelete}
                    mandatory={this.props.mandatory}
                />
            );
        }
    }

    render() {
        return (
            <div>
                <table className="alpha-op-title">
                    <colgroup>
                        <col width="300px" />
                        <col width="200px" />
                        <col width="200px" />
                        <col width="200px" />
                        <col width="200px" />
                        <col width="48px" />
                    </colgroup>

                    <tbody>
                        {this.renderRow()}
                    </tbody>
                </table>
            </div>
        );
    }
}
