import React from 'react';

import { eido } from 'Shared/Core/ShCore';
import { Redirect, withRouter, RouteComponentProps } from 'react-router';
import TextField from 'material-ui/TextField';
import Menu from 'material-ui/Menu';
import MenuItem from 'material-ui/MenuItem';
import Divider from 'material-ui/Divider';
import Subheader from 'material-ui/Subheader';
import Checkbox from 'material-ui/Checkbox';
import { AlphaOps as gAlphaOps } from 'Shared/Model/Config/ShAlpha';
import SelectField from 'material-ui/SelectField';

import PropTypes from 'prop-types';

const style = {
    paddingTop: 10
};

export interface State {
    ops                 : {}
}

export interface Props extends RouteComponentProps<any>, React.Props<any> {
    operation           : string;
    canEdit             : boolean;
    onOperationChange   : (value: string) => void;
}

export default class AlphaOpsMenu extends React.Component<Props, State> {
    constructor(props: Props) {
        super(props);

        this.state = {
            ops: {}
        };

        gAlphaOps.map((opCat) => {
            opCat.items.map((op) => this.state.ops[op.name] = op.defaultValue);
        });
    }

    renderHeader(opCat) {
        return (<Subheader>{opCat.header}</Subheader>);
    }

    renderOp(opCat) {
        return (
            <div>
                <Subheader>{opCat.header}</Subheader>
                {
                    opCat.items.map((op) => {
                        return <MenuItem primaryText=""><Checkbox label={op.text} checked={this.state.ops[op.name]} style={style} /></MenuItem>
                    })
                }
            </div>
        )
    }

    render() {
        return (
            <Menu>
                {/* {
                    gAlphaOps.map((opCat) => {
                        return this.renderOp(opCat);
                    })
                } */}
                <div>
                    <Subheader>Shift</Subheader>
                    <MenuItem primaryText="">
                        <Checkbox label="Normalise [Long/Short]" style={{paddingTop: 10}} />
                    </MenuItem>
                </div>

                <Subheader>Clamp</Subheader>

                <MenuItem primaryText=""><Checkbox label="Winsorise [Clamp]" style={{paddingTop: 10}} /></MenuItem>
                <MenuItem primaryText=""><Checkbox label="Winsorise [Median]" style={{paddingTop: 10}} /></MenuItem>
                <MenuItem primaryText=""><Checkbox label="Winsorise [NaN]" style={{paddingTop: 10}} /></MenuItem>
                <MenuItem primaryText=""><Checkbox label="Z-Score Normalise" style={{paddingTop: 10}} /></MenuItem>

                <Subheader>Neutralise Factors</Subheader>
                <MenuItem primaryText=""><Checkbox label="Beta" style={{paddingTop: 10}} /></MenuItem>
                <MenuItem primaryText=""><Checkbox label="Value" style={{paddingTop: 10}} /></MenuItem>
                <MenuItem primaryText=""><Checkbox label="Momentum" style={{paddingTop: 10}} /></MenuItem>
                <MenuItem primaryText=""><Checkbox label="Size" style={{paddingTop: 10}} /></MenuItem>

                <Subheader>Miscellaneous</Subheader>
                <MenuItem primaryText=""><Checkbox label="Rank" style={{paddingTop: 10}} /></MenuItem>
                <MenuItem primaryText=""><Checkbox label="Decay" style={{paddingTop: 10}} /></MenuItem>
                <MenuItem primaryText=""><Checkbox label="Hump" style={{paddingTop: 10}} /></MenuItem>
                <MenuItem primaryText=""><Checkbox label="Sector Neutralise" style={{paddingTop: 10}} /></MenuItem>
                <MenuItem primaryText=""><Checkbox label="Industry Neutralise" style={{paddingTop: 10}} /></MenuItem>
                <MenuItem primaryText=""><Checkbox label="Sub-Industry Neutralise" style={{paddingTop: 10}} /></MenuItem>
            </Menu>
        );
    }
}
