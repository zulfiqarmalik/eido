import React from 'react';

import { eido } from 'Shared/Core/ShCore';
import { withRouter, RouteComponentProps } from 'react-router';
import IconButton from 'material-ui/IconButton';
import RemoveCircle from 'material-ui/svg-icons/content/remove-circle';
import SelectField from 'material-ui/SelectField';
import TextField from 'material-ui/TextField';
import MenuItem from 'material-ui/MenuItem';
import FontIcon from 'material-ui/FontIcon';
import Toggle from 'material-ui/Toggle';
import Checkbox from 'material-ui/Checkbox';
import AlphaOpSelect from './AlphaOpSelect';
import AlphaOpDelete from './AlphaOpDelete';
import { Props } from './AlphaOp_Common';

export interface State {
}

export default class AlphaOp_Normalise extends React.Component<Props, State> {
    constructor(props: Props) {
        super(props);
    }

    render() {
        return (
            <tr>
                <AlphaOpSelect 
                    location={this.props.location}
                    match={this.props.match}
                    history={this.props.history}
                    onOperationChange={this.props.onOperationChange} 
                    operation="OpNormalise" 
                    canEdit={this.props.canEdit}
                    mandatory={this.props.mandatory}
                />

                <td className="alpha-op-cell" />
                <td className="alpha-op-cell" />
                <td className="alpha-op-cell" />
                <td className="alpha-op-cell" />

                <AlphaOpDelete 
                    location={this.props.location}
                    match={this.props.match}
                    history={this.props.history}
                    canDelete={this.props.canDelete}
                    onDelete={this.props.onDelete}
                />
            </tr>
        );
    }
}
