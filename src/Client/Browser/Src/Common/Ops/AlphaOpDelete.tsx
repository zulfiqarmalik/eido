import React from 'react';

import { eido } from 'Shared/Core/ShCore';
import { withRouter, RouteComponentProps } from 'react-router';
import IconButton from 'material-ui/IconButton';
import RemoveCircle from 'material-ui/svg-icons/content/remove-circle';
import FontIcon from 'material-ui/FontIcon';
import Toggle from 'material-ui/Toggle';
import Checkbox from 'material-ui/Checkbox';
import * as Colors from 'material-ui/styles/colors';

const deleteOpStyle = {
    width: 32, 
    height: 32, 
    textAlign: "center" as any, 
    paddingLeft: 0
};

export interface State {
}

export interface Props extends RouteComponentProps<any>, React.Props<any> {
    canDelete               : boolean;
    onDelete                : () => void;
}

export default class AlphaOpDelete extends React.Component<Props, State> {
    constructor(props: Props) {
        super(props);
    }

    render() {
        return (
            <td className="alpha-settings-title-button">
                <IconButton style={deleteOpStyle} onClick={this.props.onDelete} className={this.props.canDelete ? "" : "hidden"}>
                    <FontIcon color={Colors.red500} className="material-icons">delete</FontIcon>
                </IconButton>
            </td>
        );
    }
}

// <div className="alpha-settings-title-button">
//     <IconButton style={{width: 32, height: 32}} iconStyle={{color: 'red', width: 32, height: 32}}>
//         <RemoveCircle />
//     </IconButton>
//     <span className="alpha-op-title">{this.props.title}</span>
// </div>

// <div>
//     <ReactGridLayout
//         style={{ height: "100%" }}
//         className="layout"
//         autoSize={false}
//         margin={[2, 2]}
//         verticalCompact={true}
//         rowHeight={48}
//     >
//         <div 
//             key="1.1"
//             data-grid={{ x: 0, y: 0, w: 1, h: 1, static: true }}
//         >
//             <IconButton style={{width: 32, height: 32}} iconStyle={{color: 'red', width: 32, height: 32}}>
//                 <RemoveCircle />
//             </IconButton>
//         </div>
//         <div 
//             key="1.2" 
//             data-grid={{ x: 1, y: 0, w: 4, h: 10, static: true }}
//             className="main-content" 
//         >
//             <span>{this.props.title}</span>
//         </div>
//     </ReactGridLayout>
// </div>
