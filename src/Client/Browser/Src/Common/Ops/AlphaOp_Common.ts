import React from 'react';
import { withRouter, RouteComponentProps } from 'react-router';

export interface Props extends RouteComponentProps<any>, React.Props<any> {
    onOperationChange       ?: (value: string) => void;
    canDelete               ?: boolean;
    canEdit                 ?: boolean;
    onDelete                ?: () => void;
    opData                  ?: any;
    mandatory               : boolean;
}
