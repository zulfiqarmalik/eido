import React from 'react';

import { eido } from 'Shared/Core/ShCore';
import { withRouter, RouteComponentProps } from 'react-router';
import IconButton from 'material-ui/IconButton';
import RemoveCircle from 'material-ui/svg-icons/content/remove-circle';
import SelectField from 'material-ui/SelectField';
import TextField from 'material-ui/TextField';
import MenuItem from 'material-ui/MenuItem';
import FontIcon from 'material-ui/FontIcon';
import Toggle from 'material-ui/Toggle';
import Checkbox from 'material-ui/Checkbox';

import AlphaOpSelect from './AlphaOpSelect';
import AlphaOpDelete from './AlphaOpDelete';

import { Props } from './AlphaOp_Common';

const dayStyle = {width: 150, paddingLeft: 10};
const expStyle = {width: 150, paddingLeft: 10};

export interface State {
    opData              : any;
    days                : string;
    exp                 : string;
}

export default class AlphaOp_Decay extends React.Component<Props, State> {
    static daysArray    : number[] = Array.apply(null, {length: 300}).map(Number.call, Number);

    constructor(props: Props) {
        super(props);

        this.state = {
            opData: this.props.opData,
            days: this.props.opData.days,
            exp: this.props.opData.exp
        };
    }

    onDaysChange = (e: any, index: number, value: string) => {
        this.setState({ days: value });
        this.props.opData.days = value;
    }
    
    onExpChange = (e, value: string) => {
        this.props.opData.exp = value;
        this.setState({ exp: value });
    }

    render() {
        return (
            <tr>
                <AlphaOpSelect 
                    location={this.props.location}
                    match={this.props.match}
                    history={this.props.history}
                    onOperationChange={this.props.onOperationChange} 
                    operation="OpDecay" 
                    canEdit={this.props.canEdit}
                    mandatory={this.props.mandatory}
                />

                <td>
                    <SelectField
                        floatingLabelText="Days"
                        value={this.state.days}
                        onChange={this.onDaysChange}
                        style={dayStyle}
                    >
                        {
                            AlphaOp_Decay.daysArray.map((day, index) => {
                                return <MenuItem key={day} value={day.toString()} primaryText={day.toString()} />
                            })
                        }
                    </SelectField>
                </td>

                <td>
                    <TextField
                        floatingLabelText="Exponent"
                        value={this.state.exp}
                        onChange={this.onExpChange}
                        style={expStyle}
                    />
                </td>

                <td/>
                <td/>
                
                <AlphaOpDelete 
                    location={this.props.location}
                    match={this.props.match}
                    history={this.props.history}
                    canDelete={this.props.canDelete}
                    onDelete={this.props.onDelete}
                />
            </tr>
        );
    }
}