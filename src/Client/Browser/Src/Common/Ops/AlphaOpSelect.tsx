import React from 'react';

import { eido } from 'Shared/Core/ShCore';
import { withRouter, RouteComponentProps } from 'react-router';
import IconButton from 'material-ui/IconButton';
import RemoveCircle from 'material-ui/svg-icons/content/remove-circle';
import SelectField from 'material-ui/SelectField';
import TextField from 'material-ui/TextField';
import MenuItem from 'material-ui/MenuItem';
import FontIcon from 'material-ui/FontIcon';
import Toggle from 'material-ui/Toggle';
import Checkbox from 'material-ui/Checkbox';
import { AlphaOpList, MandatoryAlphaOpList, AlphaOpsLUT } from 'Shared/Model/Config/ShAlpha';

import RGL, { WidthProvider } from "react-grid-layout";
const ReactGridLayout = WidthProvider(RGL);
import * as Colors from 'material-ui/styles/colors';

const opSelectStyle = {};

export interface State {
}

export interface Props extends RouteComponentProps<any>, React.Props<any> {
    operation           ?: string;
    canEdit             ?: boolean;
    onOperationChange   ?: (value: string) => void;
    mandatory           : boolean;
}

export default class AlphaOpSelect extends React.Component<Props, State> {
    constructor(props: Props) {
        super(props);
    }

    onOperationChange   = (e: any, index: number, value: string) => this.props.onOperationChange(value)

    renderSelect() {
        return (
            <td className="alpha-op-cell">
                <SelectField
                    floatingLabelText="Operation Type"
                    value={this.props.operation}
                    onChange={this.onOperationChange}
                    style={opSelectStyle}
                >
                    {
                        (!this.props.mandatory ? AlphaOpList : MandatoryAlphaOpList).map((op) => {
                            return <MenuItem key={op.id} value={op.id} primaryText={op.label} />
                        })
                    }
                </SelectField>
            </td>
        );
    }

    renderText() {
        return (
            <td>
                <h4 className="alpha-read-only-title">{AlphaOpsLUT[this.props.operation].label}</h4>
            </td>
        )
    }

    render() {
        if (this.props.canEdit)
            return this.renderSelect();
        return this.renderText();
    }
}