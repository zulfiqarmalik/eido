import React, { ChangeEventHandler, ChangeEvent } from 'react';

import { eido } from 'Shared/Core/ShCore';
import { withRouter, RouteComponentProps } from 'react-router';
import IconButton from 'material-ui/IconButton';
import RemoveCircle from 'material-ui/svg-icons/content/remove-circle';
import SelectField from 'material-ui/SelectField';
import TextField from 'material-ui/TextField';
import MenuItem from 'material-ui/MenuItem';
import FontIcon from 'material-ui/FontIcon';
import Toggle from 'material-ui/Toggle';
import Checkbox from 'material-ui/Checkbox';

import AlphaOpSelect from './AlphaOpSelect';
import AlphaOpDelete from './AlphaOpDelete';
import { Props } from './AlphaOp_Common';

const style = {
    paddingTop: 10
};

const typeStyle = {width: 150, paddingLeft: 10};
const useMeanStyle = {width: 150, paddingLeft: 10};
const useGroupNeutraliseStyle = {width: 200, paddingLeft: 10};

export interface State {
    opData              : any;
    neutType            : string;
    neutUseMean         : boolean;
    neutGroupNormalise  : boolean;
}

export default class AlphaOp_Neutralise extends React.Component<Props, State> {
    constructor(props: Props) {
        super(props);

        this.state = {
            opData: this.props.opData,
            neutType: this.props.opData.dataId,
            neutUseMean: this.props.opData.useMean === "true",
            neutGroupNormalise: this.props.opData.groupNormalise === "true"
        };
    }

    // Winsorise
    onNeutTyepChanged = (e: React.SyntheticEvent<{}>, index: number, value: string) => {
        this.setState({ neutType: value });
        this.props.opData.dataId = value;
    }

    onNeutUseMeanToggle = (e: ChangeEvent<{}>, value?: boolean) => {
        this.setState({ neutUseMean: value });
        this.props.opData.neutUseMean = value ? "true" : "false";
    }

    onNeutGroupNormalise = (e: ChangeEvent<{}>, value?: boolean) => {
        this.setState({ neutGroupNormalise: value });
        this.props.opData.groupNormalise = value ? "true" : "false";
    }

    render() {
        return (
            <tr>
                <AlphaOpSelect 
                    location={this.props.location}
                    match={this.props.match}
                    history={this.props.history}
                    onOperationChange={this.props.onOperationChange} 
                    operation="OpNeutralise" 
                    canEdit={this.props.canEdit}
                    mandatory={this.props.mandatory}
                />

                <td>
                    <SelectField
                        floatingLabelText="Type"
                        value={this.state.neutType}
                        onChange={this.onNeutTyepChanged}
                        style={typeStyle}
                    >
                        <MenuItem key="sector" value="classification.sector" primaryText="Sector" />
                        <MenuItem key="industry" value="classification.industry" primaryText="Industry" />
                        <MenuItem key="subindustry" value="classification.industryGroup" primaryText="Sub-Industry" />
                    </SelectField>
                </td>

                <td>
                    <Checkbox
                        label="Use Mean"
                        defaultChecked={this.state.neutUseMean}
                        onChange={this.onNeutUseMeanToggle}
                        style={useMeanStyle}
                    />
                </td>

                <td>
                    <Checkbox
                        label="Group Normalise"
                        defaultChecked={this.state.neutGroupNormalise}
                        onChange={this.onNeutGroupNormalise}
                        style={useGroupNeutraliseStyle}
                    />
                </td>

                <td/>
                
                <AlphaOpDelete 
                    location={this.props.location}
                    match={this.props.match}
                    history={this.props.history}
                    canDelete={this.props.canDelete}
                    onDelete={this.props.onDelete}
                />
            </tr>
        );
    }
}
