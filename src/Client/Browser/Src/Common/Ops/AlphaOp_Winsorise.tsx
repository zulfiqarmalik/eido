import React from 'react';

import { eido } from 'Shared/Core/ShCore';
import { withRouter } from 'react-router';
import IconButton from 'material-ui/IconButton';
import RemoveCircle from 'material-ui/svg-icons/content/remove-circle';
import SelectField from 'material-ui/SelectField';
import TextField from 'material-ui/TextField';
import MenuItem from 'material-ui/MenuItem';
import FontIcon from 'material-ui/FontIcon';
import Toggle from 'material-ui/Toggle';
import Checkbox from 'material-ui/Checkbox';

import AlphaOpSelect from './AlphaOpSelect';
import AlphaOpDelete from './AlphaOpDelete';
import { Props } from './AlphaOp_Common';

const shrinkStyle = {width: 150, paddingLeft: 10};
const stdStyle = {width: 150, paddingLeft: 10};

interface State {
    opData          : any;
    winsorShrink    : string;
    winsorStd       : number;
}

export default class AlphaOp_Winsorise extends React.Component<Props, State> {
    constructor(props: Props) {
        super(props);

        this.state = {
            opData: this.props.opData,
            winsorShrink: this.props.opData.shrink,
            winsorStd: this.props.opData.std
        };
    }

    // Winsorise
    onWinsorShrinkChange = (e: React.SyntheticEvent<{}>, index: number, value: string) => {
        this.setState({ winsorShrink: value });
        this.props.opData.shrink = value;
    }
    
    onWinsorStdChange = (e: React.FormEvent<{}>, newValue: string) => {
        let value: number = parseFloat(newValue);
        this.props.opData.std = value;
        this.setState({ winsorStd: value });
    }

    render() {
        return (
            <tr>
                <AlphaOpSelect 
                    location={this.props.location}
                    match={this.props.match}
                    history={this.props.history}
                    onOperationChange={this.props.onOperationChange} 
                    operation="OpWinsorise" 
                    canEdit={this.props.canEdit}
                    mandatory={this.props.mandatory}
                />

                <td>
                    <SelectField
                        floatingLabelText="Shrink Type"
                        value={this.state.winsorShrink}
                        onChange={this.onWinsorShrinkChange}
                        style={shrinkStyle}
                    >
                        <MenuItem key="clamp" value="clamp" primaryText="Clamp" />
                        <MenuItem key="median" value="median" primaryText="Median" />
                        <MenuItem key="nan" value="nan" primaryText="NaN" />
                    </SelectField>
                </td>

                <td>
                    <TextField
                        floatingLabelText="Std. Dev."
                        value={this.state.winsorStd}
                        onChange={this.onWinsorStdChange}
                        style={stdStyle}
                    />
                </td>

                <td/>
                <td/>
                
                <AlphaOpDelete 
                    location={this.props.location}
                    match={this.props.match}
                    history={this.props.history}
                    canDelete={this.props.canDelete}
                    onDelete={this.props.onDelete}
                />
            </tr>
        );
    }
}
