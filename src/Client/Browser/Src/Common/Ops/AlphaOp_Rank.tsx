import React, { ChangeEvent } from 'react';

import { eido } from 'Shared/Core/ShCore';
import { withRouter } from 'react-router';
import IconButton from 'material-ui/IconButton';
import RemoveCircle from 'material-ui/svg-icons/content/remove-circle';
import SelectField from 'material-ui/SelectField';
import TextField from 'material-ui/TextField';
import MenuItem from 'material-ui/MenuItem';
import FontIcon from 'material-ui/FontIcon';
import Checkbox from 'material-ui/Checkbox';

import PropTypes from 'prop-types';

import AlphaOpSelect from './AlphaOpSelect';
import AlphaOpDelete from './AlphaOpDelete';
import { Props } from './AlphaOp_Common';

const style = {
    paddingTop: 10
};

const editStyle = {width: 150, paddingLeft: 10};
const useShrinkStyle = {width: 150, paddingLeft: 10};
const useDescStyle = {width: 150, paddingLeft: 10};

interface State {
    opData          : any;
    skew            : number;
    power           : number;
    shrink          : boolean;
    desc            : boolean;
}

export default class AlphaOp_Rank extends React.Component<Props, State> {
    constructor(props: Props) {
        super(props);

        this.state = {
            opData: this.props.opData,
            skew: this.props.opData.skew,
            power: this.props.opData.power,
            shrink: this.props.opData.shrink === "true",
            desc: this.props.opData.shrink === "true",
        };
    }

    onShrinkToggle = (e: ChangeEvent<{}>, value?: boolean) => {
        this.setState({ shrink: value });
        this.props.opData.shrink = value ? "true" : "false";
    }

    onDescToggle = (e: ChangeEvent<{}>, value?: boolean) => {
        this.setState({ desc: value });
        this.props.opData.desc = value ? "true" : "false";
    }

    onSkewChange = (e: React.FormEvent<{}>, newValue: string) => {
        let value: number = parseFloat(newValue);
        this.props.opData.skew = value;
        this.setState({ skew: value });
    }

    onPowerChange = (e: React.FormEvent<{}>, newValue: string) => {
        let value: number = parseFloat(newValue);
        this.props.opData.power = value;
        this.setState({ power: value });
    }

    render() {
        return (
            <tr>
                <AlphaOpSelect 
                    location={this.props.location}
                    match={this.props.match}
                    history={this.props.history}
                    onOperationChange={this.props.onOperationChange} 
                    operation="OpRank" 
                    canEdit={this.props.canEdit}
                    mandatory={this.props.mandatory}
                />

                <td>
                    <TextField
                        floatingLabelText="Skew"
                        value={this.state.skew}
                        onChange={this.onSkewChange}
                        style={editStyle}
                    />
                </td>

                <td>
                    <TextField
                        floatingLabelText="Power"
                        value={this.state.power}
                        onChange={this.onPowerChange}
                        style={editStyle}
                    />
                </td>

                <td>
                    <Checkbox
                        label="Shrink"
                        defaultChecked={this.state.shrink}
                        onChange={this.onShrinkToggle}
                        style={useShrinkStyle}
                    />
                </td>

                <td>
                    <Checkbox
                        label="Descending"
                        defaultChecked={this.state.desc}
                        onChange={this.onDescToggle}
                        style={useDescStyle}
                    />
                </td>

                <AlphaOpDelete 
                    location={this.props.location}
                    match={this.props.match}
                    history={this.props.history}
                    canDelete={this.props.canDelete}
                    onDelete={this.props.onDelete}
                />
            </tr>
        );
    }
}
