import React, { ChangeEvent } from 'react';

import { eido } from 'Shared/Core/ShCore';
import { withRouter } from 'react-router';
import IconButton from 'material-ui/IconButton';
import RemoveCircle from 'material-ui/svg-icons/content/remove-circle';
import SelectField from 'material-ui/SelectField';
import TextField from 'material-ui/TextField';
import MenuItem from 'material-ui/MenuItem';
import FontIcon from 'material-ui/FontIcon';
import Toggle from 'material-ui/Toggle';
import Checkbox from 'material-ui/Checkbox';

import AlphaOpSelect from './AlphaOpSelect';
import AlphaOpDelete from './AlphaOpDelete';
import { Props } from './AlphaOp_Common';

const style = {
    paddingTop: 10
};

const useMeanStyle = {width: 150, paddingLeft: 10};

interface State {
    opData          : any;
    useMean         : boolean;
}

export default class AlphaOp_ZScoreNormalise extends React.Component<Props, State> {
    
    constructor(props: Props) {
        super(props);

        this.state = {
            opData: this.props.opData,
            useMean: this.props.opData.useMean === "true"
        };
    }

    onMeanToggle = (e: ChangeEvent<{}>, value?: boolean) => {
        this.setState({ useMean: value });
        this.props.opData.useMean = value ? "true" : "false";
    }

    render() {
        return (
            <tr>
                <AlphaOpSelect 
                    location={this.props.location}
                    match={this.props.match}
                    history={this.props.history}
                    onOperationChange={this.props.onOperationChange} 
                    operation="OpZScoreNormalise" 
                    canEdit={this.props.canEdit}
                    mandatory={this.props.mandatory}
                />

                <td>
                    <Checkbox
                        label="Use Mean"
                        defaultChecked={this.state.useMean}
                        onChange={this.onMeanToggle}
                        style={useMeanStyle}
                    />
                </td>

                <td/>
                <td/>
                <td/>
                
                <AlphaOpDelete 
                    location={this.props.location}
                    match={this.props.match}
                    history={this.props.history}
                    canDelete={this.props.canDelete}
                    onDelete={this.props.onDelete}
                />
            </tr>
        );
    }
}
