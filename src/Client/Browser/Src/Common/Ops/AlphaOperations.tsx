import React from 'react';

import { eido } from 'Shared/Core/ShCore';
import { Redirect, withRouter, RouteComponentProps } from 'react-router';
import TextField from 'material-ui/TextField';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import { AlphaOpList, AlphaOpsLUT } from 'Shared/Model/Config/ShAlpha';
import MenuItem from 'material-ui/MenuItem';
import SelectField from 'material-ui/SelectField';
import { AlphaSubType } from 'Shared/Model/Config/ShAlpha';
import {Tabs, Tab} from 'material-ui/Tabs';
import MoreVertIcon from 'material-ui/svg-icons/navigation/more-vert';
import IconButton from 'material-ui/IconButton';
import FontIcon from 'material-ui/FontIcon';
import AddCircle from 'material-ui/svg-icons/content/add-circle';
import * as Colors from 'material-ui/styles/colors';
import AlphaOp from "./AlphaOp";

import PropTypes from 'prop-types';
import { Alpha } from 'Client/Model/Config/ClAlpha';
import { Strategy } from 'Client/Model/Config/ClStrategy';

const mediumIcon = {
    width: 32,
    height: 32,
};

const dlgStyle = {
    width: '1200px',
    maxWidth: 'none',
};

const addOpStyle = {color: 'green', width: 32, height: 32};

interface State {
    operations          : any[];
}

interface Props extends RouteComponentProps<any>, React.Props<any> {
    alpha               : Alpha | Strategy;
}

export default class AlphaOperations extends React.Component<Props, State> {
    constructor(props: Props) {
        super(props);

        this.state = {
            operations: []
        }
    }

    getOpData(op: string = "OpNormalise") {
        let opData = AlphaOpsLUT[op].opData;
        let opDataCopy = Object.assign({}, opData);
        opDataCopy.id = op;

        return opDataCopy;
    }

    onOperationChange = (index: number, op: string): any => {
        let opData = this.getOpData(op);
        this.state.operations[index] = opData;
        this.setState({
            operations: this.state.operations
        });
        return opData;
    }

    addOperation = (e) => {
        let opData = this.getOpData();
        opData.index = this.state.operations.length;
        this.state.operations.push(opData);

        this.setState({
            operations: this.state.operations
        });
    }

    onDelete = (index: number) => {
        this.state.operations.splice(index, 1);

        this.state.operations.map((op, index) => op.index = index);

        this.setState({
            operations: this.state.operations
        });
    }

    render() {
        return (
            <div>
                <div>
                    <span className="alpha-settings-title">Optional Operations</span>
                    <IconButton style={mediumIcon} iconStyle={addOpStyle} onClick={this.addOperation}>
                        <AddCircle />
                    </IconButton>

                    {
                        this.state.operations.map((opData, index) => {
                            return <AlphaOp 
                                location={this.props.location}
                                match={this.props.match}
                                history={this.props.history}
                                canDelete={true} 
                                canEdit={true}
                                onDelete={this.onDelete} 
                                opData={opData}
                                key={opData.id}
                                index={index}
                                onOperationChange={this.onOperationChange}
                            />
                        })
                    }
                </div>
                
                <br/>
                <div className = {this.props.alpha.isEquities() ? "" : "hidden"}>
                    <span className="alpha-settings-title">Mandatory Operations</span>
                    <AlphaOp 
                        location={this.props.location}
                        match={this.props.match}
                        history={this.props.history}
                        key="-1"
                        index={-1}
                        canDelete={false} 
                        canEdit={false} 
                        mandatory={true}
                        className="disabled"
                    />
                </div>
            </div>
        );
    }
}
