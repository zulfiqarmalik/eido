// import React from 'react';
// import ReactDOM from 'react-dom';
// import { eido } from "Shared/Core/ShCore";
// import { Debug, ShUtil } from "Shared/Core/ShUtil";
// import ViewAlphaModal from './ViewAlpha';
// import ChildStrategiesTable from './ChildStrategiesTable';
// import { Redirect, withRouter, RouteComponentProps } from 'react-router';

// import PropTypes from 'prop-types';
// import { Strategy } from 'Client/Model/Config/ClStrategy';
// import { LTStats } from 'Shared/Model/Stats/LTStats';

// interface Props extends RouteComponentProps<any>, React.Props<any> {
//     stats           : LTStats;
//     strategies      : Strategy[];
//     strategy        : Strategy;
// }

// export default class AlphasTable extends React.Component<Props, {}> {
//     viewAlphaModal          : any = null;
//     constructor(props: Props) {
//         super(props);
//     }

//     render() {
//         return (
//             <div>
//                 <ViewAlphaModal 
//                     key={this.props.strategy ? this.props.strategy._id : ""}
//                     ref={(c) => this.viewAlphaModal = c} 
//                     strategy={this.props.strategy}
//                     index={0} 
//                 />

//                 <ChildStrategiesTable strategies={this.props.strategies} strategy={this.props.strategy} />

//                 <Table hover condensed key={this.props.alphas && this.props.alphas.length ? this.props.alphas[0].map.uuid : "AlphasTables"} >
//                     <thead>
//                         <tr>
//                             <th><b>Index</b></th>
//                             <th><b>UUID</b></th>
//                             <th><b>Weight</b></th>
//                             <th><b>More Info</b></th>
//                         </tr>
//                     </thead>
//                     <tbody>
//                         {
//                             (this.props.alphas ? this.props.alphas : []).map((alpha, index) => {
//                                 return <AlphaItem 
//                                     key={alpha.map.uuid} 
//                                     alpha={alpha} 
//                                     index={index} 
//                                     viewAlphaModal={this.viewAlphaModal}
//                                     strategy={this.props.strategy}
//                                 />;
//                             }
//                         )}
//                     </tbody>
//                 </Table>
//             </div>
//         );
//     }
// }

// export class AlphaItem extends React.Component {
//     static propTypes = {
//         strategy: PropTypes.object,
//     };

//     constructor() {
//         super(...arguments);
//     }

//     handleViewAlpha(e) {
//         // eido._cached.strategy.currentAlpha = this.props.alpha;
//         if (this.props.viewAlphaModal)
//             this.props.viewAlphaModal.open(this.props.index);
//     }

//     render() {
//         return (
//             <tr>
//                 <td>{this.props.index + 1}</td>
//                 <td>
//                     <a href="#" onClick={ ::this.handleViewAlpha }><b>{ this.props.alpha.map.uuid }</b></a> 
//                     {/*<Button outlined bsStyle='primary' onClick={::this.handleViewAlpha}  uuid={this.props.alpha.map.uuid}>{ this.props.alpha.map.uuid }</Button>*/}
//                 </td>
//                 <td>{(this.props.alpha && typeof this.props.alpha.map.weight !== 'undefined' ? this.props.alpha.map.weight : 1.0)}</td>
//                 <td className="fixed-width-font">{this.props.alpha.map.pyPath ? this.props.alpha.map.pyPath + " - " + this.props.alpha.map.pyClassName : this.props.alpha.map.expr}</td>
//             </tr >
//         );
//     }
// }
