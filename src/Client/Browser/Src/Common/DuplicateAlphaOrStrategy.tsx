import React from 'react';

import { RouteComponentProps } from 'react-router';
import TextField from 'material-ui/TextField';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import { Category, AlphaOrStrategy } from 'Client/Model/Config/ClConfigBase';
import { Alpha } from 'Client/Model/Config/ClAlpha';

interface Props extends RouteComponentProps<any>, React.Props<any> {
    onOk                : (name: string, category: string) => void;
    onCancel            : () => void;
    categories          : Category<AlphaOrStrategy>[];
    open                : boolean;
}

interface State {
    name                : string;
    category            : string;
}

export default class DuplicateAlphaOrStrategy extends React.Component<Props, State> {
    constructor(props: Props) {
        super(props);

        let dataSource: string[] = new Array<string>(this.props.categories.length);
        this.props.categories.map((category: Category<Alpha>, index: number) => dataSource[index] = category.name);

        this.state = {
            name: "Duplicated",
            category: "Research"
        }
    }

    componentDidMount() {
    }

    onOk = () => {
        this.props.onOk(this.state.name, this.state.category);
    }

    onCancel = () => {
        this.props.onCancel();
    }

    onClose = () => {this
        this.props.onCancel();
    }

    onNameChange = (e: any, name: string) => {
        this.setState({
            name: name
        });
    }

    onCategoryChange = (e: any, category: string) => {
        this.setState({
            category: category
        });
    }

    getActions() {
        return [
            <FlatButton
                label="Ok"
                secondary={true}
                keyboardFocused={true}
                onClick={this.onOk}
            />,

            <FlatButton
                label="Cancel"
                primary={true}
                keyboardFocused={false}
                onClick={this.onCancel}
            />
          ];
    }

    render() {
        return (
            <Dialog
                title="Duplicate Alpha/Strategy"
                actions={this.getActions()}
                modal={true}
                open={this.props.open}
                onRequestClose={this.onClose}
            >
                <TextField
                    value={this.state.name}
                    onChange={this.onNameChange}
                    floatingLabelText="Name"
                />
                <br/>
                <TextField
                    floatingLabelText="Category"
                    hintText="Name of the category here"
                    value={this.state.category}
                    onChange={this.onCategoryChange}
                />
            </Dialog>
        );
    }
}
