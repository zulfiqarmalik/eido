import React from 'react';

import { RouteComponentProps } from 'react-router';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import { AlphaOpsLUT, DefaultOptimise } from 'Shared/Model/Config/ShAlpha';
import { AlphaSubType } from 'Shared/Model/Config/ShAlpha';
import {Tabs, Tab} from 'material-ui/Tabs';
import IconButton from 'material-ui/IconButton';
import AddCircle from 'material-ui/svg-icons/content/add-circle';
import AlphaOp from "./Ops/AlphaOp";

import { Strategy } from 'Client/Model/Config/ClStrategy';
import { Alpha } from 'Client/Model/Config/ClAlpha';
import TextField from 'material-ui/TextField';
import FontIcon from 'material-ui/FontIcon';
import * as Colors from 'material-ui/styles/colors';
import AlphaSettings_Section from './AlphaSettings_Section';
import { AlphaOrStrategy } from 'Client/Model/Config/ClConfigBase';
import RaisedButton from 'material-ui/RaisedButton';

const mediumIcon = {
    width: 32,
    height: 32,
};

const dlgStyle = {
    width: '1200px',
    maxWidth: 'none',
};

const addOpStyle = {color: 'green', width: 32, height: 32};

interface State {
    name                : string;
    category            : string;
    alphaType           : AlphaSubType;
    operations          : any[];
    refConfig           : string;
    portfolioJson       : string;
    optimiseIndex       : number;
    optimiseJson        : string;
}

interface Props extends RouteComponentProps<any>, React.Props<any> {
    key                 ?: string;
    alpha               ?: AlphaOrStrategy;
    onSave              ?: () => void;
    onCancel            ?: () => void;
    onOk                ?: () => void;
    canCancel           ?: boolean;
    open                ?: boolean;
    showOperations      ?: boolean;
    showOptimiser       ?: boolean;
    market              : string;
}

const stdStyle = {width: 150, paddingLeft: 10};

const deleteOpStyle = {
    width: 32, 
    height: 32, 
    textAlign: "center" as any, 
    paddingLeft: 0
};

export default class AlphaSettings extends React.Component<Props, State> {
    constructor(props: Props) {
        super(props);

        let optimiseIndex = this.props.alpha ? this.props.alpha.portfolio.findChildIndexWithName("Optimise") : -1;
        let optimiseJson = "";

        if (this.props.alpha && optimiseIndex >= 0) {
            optimiseJson = JSON.stringify(this.props.alpha.portfolio.children[optimiseIndex], null, 4);
        }

        this.state = {
            name: "MyAlpha",
            category: "Research",
            alphaType: AlphaSubType.python,
            operations: [],
            refConfig: this.props.alpha ? this.props.alpha.refConfig : "",
            portfolioJson: this.props.alpha ? JSON.stringify(this.props.alpha.portfolio.map, null, 4) : "{}",
            optimiseIndex: optimiseIndex,
            optimiseJson: optimiseJson
        }
    }

    componentDidMount() {
        let edOps = [];

        if (this.props.alpha) {
            // let ops = this.props.alpha.operations;
            // if (ops.length) {
            //     edOps = new Array(ops.length - 1); /// We don't include the mandatory Normalise
        
            //     for (let i = 0; i < ops.length - 1; i++)
            //         edOps[i] = ops[i].map;
            // }
        }

        this.setState({
            operations: edOps
        });
    }

    onSaveOperations = () => {
        let settings: any = {
            refConfig: this.state.refConfig,
            portfolio: JSON.parse(this.state.portfolioJson),
            optimise: this.state.optimiseJson ? JSON.parse(this.state.optimiseJson) : null
        };

        this.props.alpha.saveSettings(this.state.operations, settings)
            .then(() => {
                if (this.props.onOk)
                    this.props.onOk();
            })

        // this.props.alpha.saveOperations(this.state.operations)
        //     .then(() => {
        //         if (this.props.onOk)
        //             this.props.onOk();
        //     });
    }

    onCancel = () => this.props.onCancel()
    onClose = () => this.props.onCancel()

    getActions() {
        return [
            <FlatButton
                label="Save"
                secondary={true}
                keyboardFocused={true}
                onClick={this.onSaveOperations}
                disabled={this.props.alpha && this.props.alpha.isPublished}
            />,

            <FlatButton
                label="Cancel"
                primary={true}
                keyboardFocused={false}
                onClick={this.onCancel}
            />,
        ];
    }

    getOpData(op: string = "OpNormalise") {
        let opData = AlphaOpsLUT[op].opData;
        let opDataCopy = Object.assign({}, opData);
        opDataCopy.id = op;

        return opDataCopy;
    }

    onOperationChange = (index: number, op: string) => {
        let opData = this.getOpData(op);
        this.state.operations[index] = opData;
        this.setState({
            operations: this.state.operations
        });
        return opData;
    }

    addOperation = () => {
        let opData = this.getOpData();
        opData.index = this.state.operations.length;
        this.state.operations.push(opData);

        this.setState({
            operations: this.state.operations
        });
    }

    onDelete = (index: number) => {
        this.state.operations.splice(index, 1);
        this.state.operations.map((op, index) => op.index = index);
        this.setState({
            operations: this.state.operations
        });
    }

    renderOperations() {
        return (
            <Tab label="Operations" value="ops">
                <div>
                    <span className="alpha-settings-title">Optional Operations</span>
                    <IconButton 
                        style={mediumIcon} 
                        iconStyle={addOpStyle} 
                        onClick={this.addOperation}
                        disabled={this.props.alpha && this.props.alpha.isPublished}
                    >
                        <AddCircle />
                    </IconButton>

                    {
                        this.state.operations.map((opData, index) => {
                            return <AlphaOp 
                                location={this.props.location}
                                match={this.props.match}
                                history={this.props.history}
                                canDelete={!(this.props.alpha && this.props.alpha.isPublished)} 
                                canEdit={true}
                                onDelete={this.onDelete} 
                                opData={opData}
                                key={`${opData.id}-${index}`}
                                index={index}
                                onOperationChange={this.onOperationChange}
                            />
                        })
                    }
                </div>
                
                <br/>
                <div>
                    <span className="alpha-settings-title">Mandatory Operations</span>
                    <AlphaOp 
                        location={this.props.location}
                        match={this.props.match}
                        history={this.props.history}
                        key="-1"
                        index={-1}
                        canDelete={false} 
                        canEdit={false} 
                        className="disabled"
                    />
                </div>
            </Tab>
        );
    }

    onRefConfigChange = (event, value: string) => {
        this.setState({
            refConfig: value
        });
    }

    onPortfolioChange = (event, value: string) => {
        this.setState({
            portfolioJson: value
        });
    }

    renderPortfolio() {
        if (!this.props.alpha || !this.props.showOptimiser)
            return (<div/>);

        return (
            <Tab label="Portfolio" value="ops" className="v-scrollable">
                <TextField
                    floatingLabelText="Ref. Config"
                    style={{width: "100%"}}
                    value={this.state.refConfig}
                    onChange={this.onRefConfigChange}
                    disabled={this.props.alpha && this.props.alpha.isPublished}
                />

                <TextField 
                    multiLine={true}
                    value={this.state.portfolioJson}
                    onChange={this.onPortfolioChange}
                    style={{width: "100%"}}
                    rowsMax={12}
                    className="json-content"
                />
            </Tab>
        );
    }

    onOptimiseChange = (event, value: string) => {
        this.setState({
            optimiseJson: value
        });
    }

    onResetOptimisation = () => {
        this.setState({
            optimiseJson: JSON.stringify(DefaultOptimise[this.props.market], null, 4)
        });
    }

    renderOptimise() {
        if (!this.props.alpha || !this.props.showOptimiser)
            return (<div/>);

        return (
            <Tab label="Optimisation" value="opt" className="v-scrollable">
                <TextField 
                    multiLine={true}
                    value={this.state.optimiseJson}
                    onChange={this.onOptimiseChange}
                    style={{width: "100%"}}
                    rowsMax={18}
                    className="json-content"
                    hintText="No optimisation config exists. Use the Create/Reset button to create a default config"
                />

                <RaisedButton 
                    label="Create/Reset Optimisation Config" 
                    secondary={true}
                    onClick={this.onResetOptimisation}
                />

            </Tab>
        );
    }

    renderConstraints() {
        if (!this.props.alpha || !this.props.showOptimiser)
            return (<div/>);

        return (
            <Tab label="Opt. Constraints" value="ops" className="v-scrollable">
                <AlphaSettings_Section
                    sectionName="Risk Model Factor Exposure Limits"
                    sectionId="RiskModelFactorExposureLimits"
                    alpha={this.props.alpha}
                    section={this.props.alpha.portfolio.findChildWithName("Optimise") ? this.props.alpha.portfolio.findChildWithName("Optimise").findChildWithName("RiskModelFactorExposureLimits") : null}
                />

                <AlphaSettings_Section
                    sectionName="Constraint Resolution"
                    sectionId="ConstraintResolution"
                    alpha={this.props.alpha}
                    section={this.props.alpha.portfolio.findChildWithName("Optimise") ? this.props.alpha.portfolio.findChildWithName("Optimise").findChildWithName("ConstraintResolution") : null}
                />
            </Tab>
        );
    }

    renderConfigTab() {
        if (!this.props.alpha)
            return (<div/>);

        return (
            <Tab label="Config" value="ops" className="v-scrollable">
                <TextField
                    floatingLabelText="Ref. Config"
                    style={{width: 400}}
                    value={this.props.alpha ? this.props.alpha.refConfig : ""}
                    onChange={this.onRefConfigChange}
                    disabled={this.props.alpha && this.props.alpha.isPublished}
                />

                <AlphaSettings_Section
                    sectionName="Portfolio"
                    sectionId="Portfolio"
                    alpha={this.props.alpha}
                    section={this.props.alpha.portfolio}
                />

                <AlphaSettings_Section
                    sectionName="Optimise"
                    sectionId="Optimise"
                    alpha={this.props.alpha}
                    section={this.props.alpha.portfolio.findChildWithName("Optimise")}
                />

                <AlphaSettings_Section
                    sectionName="Risk Model Factor Exposure Limits"
                    sectionId="RiskModelFactorExposureLimits"
                    alpha={this.props.alpha}
                    section={this.props.alpha.portfolio.findChildWithName("Optimise") ? this.props.alpha.portfolio.findChildWithName("Optimise").findChildWithName("RiskModelFactorExposureLimits") : null}
                />

                <AlphaSettings_Section
                    sectionName="Constraint Resolution"
                    sectionId="ConstraintResolution"
                    alpha={this.props.alpha}
                    section={this.props.alpha.portfolio.findChildWithName("Optimise") ? this.props.alpha.portfolio.findChildWithName("Optimise").findChildWithName("ConstraintResolution") : null}
                />
            </Tab>
        );
    }

    renderTabs() {
        if (this.props.showOptimiser) {
            return (
                <Tabs>
                    {this.renderPortfolio()}
                    {this.renderOptimise()}
                    {/* {this.renderConstraints()} */}
                    {/* {this.renderOperations()} */}
                </Tabs>
            );
        }

        return (
            <Tabs>
                {/* {this.renderOperations()} */}
            </Tabs>
        );
    }

    render() {
        return (
            <Dialog
                actions={this.getActions()}
                modal={true}
                open={this.props.open}
                contentStyle={dlgStyle}
                onRequestClose={this.onClose}
            >
                {this.renderTabs()}
            </Dialog>
        );
    }
}
