import React from 'react';
import ReactDOM from 'react-dom';
import { eido } from "Shared/Core/ShCore";
import { Debug, ShUtil } from "Shared/Core/ShUtil";
import StatsRow_Cum from './StatsRow_Cum';
import * as Colors from 'material-ui/styles/colors';

import {
    Table,
    TableBody,
    TableHeader,
    TableHeaderColumn,
    TableRow,
    TableRowColumn,
} from 'material-ui/Table';
  
import PropTypes from 'prop-types';
import { Strategy} from 'Client/Model/Config/ClStrategy';
import { LTStats } from 'Shared/Model/Stats/LTStats';
import { CumStats } from 'Shared/Model/Stats/CumStats';

interface Props {
    key             ?: string;
    stats           : LTStats;
    rawStats        ?: LTStats;
    preoptStats     ?: LTStats;
    miniView        ?: boolean;
}

interface State { 
    statsToShow     : CumStats[];
}

export default class StatsTable_Cum extends React.Component<Props, State> {
    constructor(props: Props) {
        super(props);

        this.state = {
            statsToShow: []
        };

        if (this.props.stats) {
            this.state.statsToShow.push(this.props.stats.D5);
            this.state.statsToShow.push(this.props.stats.D10);
            this.state.statsToShow.push(this.props.stats.D21);
            this.state.statsToShow.push(this.props.stats.D63);
            this.state.statsToShow.push(this.props.stats.D120);
            this.state.statsToShow.push(this.props.stats.D250);
        }
    }

    renderLTRowGeneric(title: string, name: string, stats: LTStats, color: string) {
        return (
            <StatsRow_Cum key={stats ? name : "_LT"} stats={stats ? stats : null} color={color} title={title}/>
        )
    }

    renderLTRow() {
        return this.renderLTRowGeneric("LT", this.props.stats ? this.props.stats.name : "OVERALL_LT", this.props.stats, Colors.blue400);
    }

    renderRawLTRow() {
        if (!this.props.rawStats)
            return (<div/>);

        return this.renderLTRowGeneric("RAW_LT", this.props.rawStats.name, this.props.rawStats, Colors.red400);
    }

    renderPreoptLTRow() {
        if (!this.props.preoptStats)
            return (<div/>);

        return this.renderLTRowGeneric("PRE_OPT_LT", this.props.preoptStats.name, this.props.preoptStats, Colors.yellow400);
    }

    renderStatsRow(stats: CumStats) {
        if (!stats)
            return (<div/>);

        return (<StatsRow_Cum title={stats.name} key={stats.name} stats={stats} color={Colors.yellow400} />);
    }

    render() {
        return (
            <Table
                selectable={false}
                fixedFooter={false}
            >
                <TableHeader displaySelectAll={false} adjustForCheckbox={false}>
                    <TableRow>
                        <TableHeaderColumn><b>Type</b></TableHeaderColumn>
                        <TableHeaderColumn><b>Long / Short</b></TableHeaderColumn>
                        <TableHeaderColumn><b>PNL</b></TableHeaderColumn>
                        <TableHeaderColumn><b>Margin BPS</b></TableHeaderColumn>
                        <TableHeaderColumn><b>IR/Sharpe</b></TableHeaderColumn>
                        <TableHeaderColumn><b>Volatility</b></TableHeaderColumn>
                        <TableHeaderColumn><b>Returns %</b></TableHeaderColumn>
                        <TableHeaderColumn><b>TVR %</b></TableHeaderColumn>
                        <TableHeaderColumn><b>MaxDD %</b></TableHeaderColumn>
                        <TableHeaderColumn><b>MaxDH</b></TableHeaderColumn>
                        <TableHeaderColumn><b>Hit Ratio %</b></TableHeaderColumn>
                    </TableRow>
                </TableHeader>
                <TableBody displayRowCheckbox={false}>
                    {this.renderLTRow()}
                    {this.renderRawLTRow()}
                    {this.renderPreoptLTRow()}
                    {/* <StatsRow_Cum key={this.props.stats ? this.props.stats.name : "_LT"} stats={this.props.stats ? this.props.stats : null} color={Colors.blue400} title="LT"/> */}
                    <StatsRow_Cum  
                        title="OS"
                        className={this.props.stats && !this.props.stats.outSample ? 'hidden' : ''}
                        key={this.props.stats && this.props.stats.outSample ? this.props.stats.name + "_OS" : "_OS"} 
                        stats={this.props.stats ? this.props.stats.outSample : null} 
                        color={Colors.deepOrange500}
                    />

                    <StatsRow_Cum 
                        title="IS"
                        className={this.props.stats && !this.props.stats.inSample ? 'hidden' : ''}
                        key={this.props.stats && this.props.stats.inSample ? this.props.stats.name + "_IS" : "_IS"} 
                        stats={this.props.stats && this.props.stats.inSample ? this.props.stats.inSample : null} 
                        color={Colors.cyan400}
                    />

                    { this.renderStatsRow(this.props.stats ? this.props.stats.D5 : null) }
                    { this.renderStatsRow(this.props.stats ? this.props.stats.D10 : null) }
                    { this.renderStatsRow(this.props.stats ? this.props.stats.D21 : null) }
                    { this.renderStatsRow(this.props.stats ? this.props.stats.D63 : null) }
                    { this.renderStatsRow(this.props.stats ? this.props.stats.D120 : null) }
                    { this.renderStatsRow(this.props.stats ? this.props.stats.D250 : null) }

                    {
                        (this.props.stats && !this.props.miniView ? this.props.stats.annual : []).sort((a, b) => b.date - a.date).map((astats) => {
                            return <StatsRow_Cum title={astats.name} key={this.props.stats.name + astats.startDate} stats={astats} color={Colors.pink400} />
                        }
                    )}
                    {/* {
                        (!this.props.miniView ? this.state.statsToShow : []).map((stats) => {
                            return <StatsRow_Cum title={stats.name} key={stats.name} stats={stats} />
                        }
                    )} */}
                </TableBody>
            </Table>
        );
    }
}

// export default createContainer(() => {
//     // let shell = Meteor.npmRequire('shelljs');
//     console.log("Getting strats in path: ${path}");
//     // let strats = shell.ls(path);

//     return {
//         strats: ["Test1", "Test2"],
//         name: "US"
//     };
// }, PnlGraphs);
