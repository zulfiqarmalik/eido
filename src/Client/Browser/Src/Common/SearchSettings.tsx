import React from 'react';

import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import { ConfigType } from 'Shared/Model/Config/ShAlpha';
import IconButton from 'material-ui/IconButton';
import AddCircle from 'material-ui/svg-icons/content/add';
import RemoveCircle from 'material-ui/svg-icons/content/delete-sweep';

import TextField from 'material-ui/TextField';
import { AlphaOrStrategy } from 'Client/Model/Config/ClConfigBase';
import ConfigTypeSelect from './ConfigTypeSelect';
import SearchViewType from './SearchViewType';
import { ObjectID } from 'Shared/Core/ObjectID';
import AlphaSort from './AlphaSort';
import CmpOperator from './CmpOperator';
import FontIcon from 'material-ui/FontIcon';
import * as Colors from 'material-ui/styles/colors';

const mediumIcon = {
    width: 32,
    height: 32,
};

const dlgStyle = {
    width: '600px',
    maxWidth: 'none',
};

const addButtonStyle = {color: Colors.green500, width: 32, height: 32};
const deleteAllStyle = {color: Colors.red500, width: 32, height: 32};

interface State {
    searchView          : string;
    configType          : ConfigType;
    advSearches         : any[];
}

export class AdvSearchOptions {
    searchView          : string;
    configType          : ConfigType;
    advSearches         : any[];
}

interface Props {
    key                 ?: string;
    alpha               ?: AlphaOrStrategy;
    onCancel            ?: () => void;
    onOk                : (opt: AdvSearchOptions) => void;
    open                ?: boolean;

    searchView          : string;
    configType          : ConfigType;
    hideType            : boolean;
    advSearches         : any[]
}

const deleteOpStyle = {
    width: 16, 
    height: 16, 
    textAlign: "center" as any, 
    paddingLeft: 0,
    paddingBottom: 10
};

export default class SearchSettings extends React.Component<Props, State> {
    constructor(props: Props) {
        super(props);

        this.state = {
            searchView  : this.props.searchView,
            configType  : this.props.configType,
            advSearches : this.props.advSearches
        }
    }

    componentDidMount() {
    }

    onCancel = () => this.props.onCancel()
    onClose = () => this.props.onCancel()

    onUpdateSearch = () => {
        let opt: AdvSearchOptions = new AdvSearchOptions();
        opt.searchView = this.state.searchView;
        opt.configType = this.state.configType;
        opt.advSearches = this.state.advSearches;

        this.props.onOk(opt);
    }

    onConfigTypeChange = (configType: ConfigType) => {
        this.setState({
            configType: configType
        });
    }

    onSearchViewChange = (searchView: string) => {
        this.setState({
            searchView: searchView
        });
    }

    renderSearchViewType() {
        return (
            <SearchViewType
                label="Search View"
                key={"SearchView"}
                onSearchViewChange={this.onSearchViewChange}
                searchView={this.state.searchView} 
                width={200}
            />
        );
    }

    renderConfigType() {
        if (!this.props.hideType) {
            return (
                <ConfigTypeSelect
                    label="Type"
                    key={`ConfigType`}
                    onConfigTypeChange={this.onConfigTypeChange}
                    configType={this.state.configType}
                    readOnly={false}
                    width={200}
                />
            );
        }
        return (<span/>);
    }

    onAddAdvSearch = () => {
        let advSearches = this.state.advSearches;
        advSearches.push({
            name: "ir",
            op: "$gt",
            value: "0.05",
            iid: ObjectID.create().toString()
        });

        this.setState({
            advSearches: advSearches
        })
    }

    iid(): string {
        let iid: string = "";

        this.state.advSearches.map((advSearch) => {
            iid += `${advSearch.iid}_${advSearch.name}_`
        });

        iid += `${this.state.advSearches.length}`;

        return iid;
    }

    deleteAdvSearch(advSearch: any) {
        let index: number = -1;

        for (let i = 0; i < this.state.advSearches.length && index < 0; i++) {
            if (this.state.advSearches[i].iid == advSearch.iid) 
                index = i;
        }

        if (index >= 0 && index < this.state.advSearches.length) {
            this.state.advSearches.splice(index, 1);
            this.setState({
                advSearches: this.state.advSearches
            });
        }
    }

    renderAdvSearchItem(advSearch: any) {
        return (
            <table className="alpha-op-title" key={`AdvSearch-${advSearch.iid}`}>
                <colgroup>
                    <col width="300px" />
                    <col width="100px" />
                    <col width="100px" />
                    <col width="48px" />
                </colgroup>

                <tbody>
                    <tr>
                        <td>
                            <AlphaSort
                                key={`Stat-${advSearch.iid}`}
                                onSortChange={(name: string) => { 
                                    advSearch.name = name; 
                                    this.setState({ advSearches: this.state.advSearches });
                                }}
                                sortBy={advSearch.name} 
                                width={300}
                            />
                        </td>

                        <td>
                            <CmpOperator
                                key={`Op-${advSearch.iid}`}
                                onOpChange={(op: string) => { 
                                    advSearch.op = op; 
                                    this.setState({ advSearches: this.state.advSearches });
                                }}
                                op={advSearch.op} 
                                width={100}
                            />
                        </td>

                        <td>
                            <TextField
                                value={advSearch.value}
                                style={{width: 100, height: 36}}
                                key={`Value-${advSearch.iid}`}
                                onChange={(event: any, value: string) => {
                                    advSearch.value = value; 
                                    this.setState({ advSearches: this.state.advSearches });
                                }}
                            />
                        </td>
                        
                        <td className="alpha-settings-title-button">
                            <IconButton style={deleteOpStyle} onClick={() => {
                                this.deleteAdvSearch(advSearch);
                            }}>
                                <FontIcon color={Colors.red500} className="material-icons">delete</FontIcon>
                            </IconButton>
                        </td>
                    </tr>
                </tbody>
            </table>
        );
    }

    onResetAdvSearch = () => {
        this.setState({
            advSearches: []
        });
    }

    renderAdvSearch() {
        return (
            <div>
                <span className="alpha-settings-title">Limits</span>
                {/* <FlatButton
                    label="Add"
                    primary={true}
                    keyboardFocused={true}
                    onClick={this.onAddAdvSearch}
                />

                &nbsp;
                &nbsp;

                <FlatButton
                    label="Reset"
                    secondary={true}
                    onClick={() => {
                        this.setState({
                            advSearches: []
                        });
                    }}
                /> */}

                <IconButton 
                    style={mediumIcon} 
                    iconStyle={addButtonStyle} 
                    onClick={this.onAddAdvSearch}
                    tooltip="Add"
                >
                    <AddCircle />
                </IconButton>

                &nbsp;
                &nbsp;

                <IconButton 
                    style={mediumIcon} 
                    iconStyle={deleteAllStyle} 
                    onClick={this.onResetAdvSearch}
                    tooltip="Delete All"
                    color={Colors.red500}
                >
                    <RemoveCircle />
                </IconButton>

                <table className="alpha-op-title" key={`AdvSaerchs-${this.iid()}`}>
                    <colgroup>
                        <col width="300px" />
                        <col width="100px" />
                        <col width="100px" />
                        <col width="48px" />
                    </colgroup>

                    <tbody>
                        {this.state.advSearches.map((advSearch, index) => {
                            return this.renderAdvSearchItem(advSearch)
                        })}
                    </tbody>
                </table>
            </div>
        );
    }

    getActions() {
        return [
            <FlatButton
                label="Search"
                secondary={true}
                keyboardFocused={true}
                onClick={this.onUpdateSearch}
                disabled={this.props.alpha && this.props.alpha.isPublished}
            />,

            <FlatButton
                label="Cancel"
                primary={true}
                keyboardFocused={false}
                onClick={this.onCancel}
            />,
        ];
    }

    onDelete = (index: number) => {
    }

    render() {
        return (
            <Dialog
                actions={this.getActions()}
                title="Advaned Search Settings"
                modal={true}
                open={this.props.open}
                contentStyle={dlgStyle}
                onRequestClose={this.onClose}
            >
                {this.renderSearchViewType()}
                &nbsp; &nbsp;
                {this.renderConfigType()}
                <br/>
                {this.renderAdvSearch()}
            </Dialog>
        );
    }
}
