import React from 'react';

import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';

interface Props {
    onOk            : () => void;
    open            : boolean;
    title           : string;
    message         : string;
    modal           : boolean;
}

interface State {
}
    
export default class Alert extends React.Component<Props, State> {
    constructor(props: Props) {
        super(props);

        this.state = {
        }
    }

    componentDidMount() {
    }

    onOk = () => {
        this.props.onOk();
    }

    onClose = () => {
        this.props.onOk();
    }

    getActions() {
        return [
            <FlatButton
                label="Ok"
                secondary={true}
                keyboardFocused={true}
                onClick={this.onOk}
            />,
          ];
    }

    render() {
        return (
            <Dialog
                title={this.props.title}
                actions={this.getActions()}
                modal={this.props.modal}
                open={this.props.open}
                onRequestClose={this.onClose}
                autoScrollBodyContent={!this.props.modal}
            >
                {this.props.message}
            </Dialog>
        );
    }
}

