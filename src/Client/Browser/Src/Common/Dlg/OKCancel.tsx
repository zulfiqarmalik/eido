import React, { ChangeEventHandler, ChangeEvent } from 'react';

import { eido } from 'Shared/Core/ShCore';
import { Redirect, withRouter, RouteComponentProps } from 'react-router';
import TextField from 'material-ui/TextField';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import Checkbox from 'material-ui/Checkbox';

import PropTypes from 'prop-types';

interface Props {
    onOk            : () => void;
    onCancel        : () => void;
    open            : boolean;

    title           : string;
    message         : string;
}

interface State {
}
    
export default class OKCancelDialog extends React.Component<Props, State> {
    constructor(props: Props) {
        super(props);

        this.state = {
        }
    }

    componentDidMount() {
    }

    onOk = () => {
        this.props.onOk();
    }

    onCancel = () => {
        this.props.onCancel();
    }

    onClose = () => {
        this.props.onCancel();
    }

    getActions() {
        return [
            <FlatButton
                label="Ok"
                secondary={true}
                keyboardFocused={true}
                onClick={this.onOk}
            />,

            <FlatButton
                label="Cancel"
                primary={true}
                keyboardFocused={false}
                onClick={this.onCancel}
            />,
          ];
    }

    render() {
        return (
            <Dialog
                title={this.props.title}
                actions={this.getActions()}
                modal={true}
                open={this.props.open}
                onRequestClose={this.onClose}
            >
                {this.props.message}
            </Dialog>
        );
    }
}

