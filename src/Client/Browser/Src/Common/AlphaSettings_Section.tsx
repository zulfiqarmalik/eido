import React from 'react';

import { RouteComponentProps } from 'react-router';
import IconButton from 'material-ui/IconButton';
import AddCircle from 'material-ui/svg-icons/content/add-circle';

import TextField from 'material-ui/TextField';
import FontIcon from 'material-ui/FontIcon';
import * as Colors from 'material-ui/styles/colors';
import { AlphaOrStrategy } from 'Client/Model/Config/ClConfigBase';
import { ConfigSection } from 'Shared/Model/Config/ConfigSection';

const mediumIcon = {
    width: 32,
    height: 32,
};

const dlgStyle = {
    width: '1200px',
    maxWidth: 'none',
};

const addOpStyle = {color: 'green', width: 32, height: 32};

interface State {
    value: string;
}

interface Props {
    alpha               : AlphaOrStrategy;
    section             : ConfigSection;
    sectionId           : string;
    sectionName         : string;
}

const stdStyle = {width: 150, paddingLeft: 10};

const deleteOpStyle = {
    width: 32, 
    height: 32, 
    textAlign: "center" as any, 
    paddingLeft: 0
};

export default class AlphaSettings_Section extends React.Component<Props, State> {
    constructor(props: Props) {
        super(props);

        this.state = {
            value: JSON.stringify(this.props.section ? this.props.section.map : {}, null, 4)
        }
    }

    componentDidMount() {
    }

    onDelete = () => {

    }

    renderDelete() {
        return (
            <td className="alpha-settings-title-button">
                <IconButton style={deleteOpStyle} onClick={this.onDelete}>
                    <FontIcon color={Colors.red500} className="material-icons">delete</FontIcon>
                </IconButton>
            </td>
        )
    }

    renderTableRow(parent: any, key: string) {
        return (
            <tr>
                <td>
                    <TextField
                        floatingLabelText="Key"
                        value={key}
                        style={stdStyle}
                    />
                </td>

                <td>
                    <TextField
                        floatingLabelText="Value"
                        value={parent[key]}
                        style={stdStyle}
                    />
                </td>

                <td className="alpha-settings-title-button">
                    <IconButton style={deleteOpStyle} onClick={this.onDelete} className={this.props.alpha.isPublished ? "" : "hidden"}>
                        <FontIcon color={Colors.red500} className="material-icons">delete</FontIcon>
                    </IconButton>
                </td>
            </tr>
        )
    }

    onChange = (event: any, value: string) => {
        this.setState({
            value: value
        });
    }

    renderSection() {
        return (
            <TextField 
                hintText={this.props.sectionName}
                multiLine={true}
                value={this.state.value}
                onChange={this.onChange}
                style={{width: "100%"}}
                rowsMax={12}
                className="json-content"
            />
        );
    }

    render() {
        return (
            <div>
                <span className="json-content-title">{this.props.sectionName}</span>
                <br/>

                {/* {this.renderTable()} */}
                {this.renderSection()}
            </div>
        );
    }
}
