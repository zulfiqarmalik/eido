import React from 'react';
import ReactDOM from 'react-dom';
import { eido } from "Shared/Core/ShCore";
import { Debug, ShUtil } from "Shared/Core/ShUtil";
import LinearProgress from 'material-ui/LinearProgress';

import PropTypes from 'prop-types';
import { Strategy } from 'Client/Model/Config/ClStrategy';

interface Props {
    statusMessage       ?: string;
}

interface State {
    statusMessage       : string;
    progress            : number;
}

export default class TaskProgress extends React.Component<Props, State> {
    constructor(props: Props) {
        super(props);

        this.state = {
            statusMessage: this.props.statusMessage || "Loading ...",
            progress: 0
        };
    }

    taskUpdate(percent: number, statusMessage: string) {
        let pvalue = Math.round(percent * 100.0);
        console.log(pvalue);
        this.setState({
            progress: pvalue,
            statusMessage: statusMessage || this.state.statusMessage
        });
    }

    taskFinished(statusMessage) {
        this.setState({
            statusMessage: statusMessage || this.state.statusMessage,
            progress: 100
        });
    }

    setStatusMessage(statusMessage) {
        this.setState
    }

    render() {
        return (
            <div>
                <h3>{this.state.statusMessage}</h3>
                <LinearProgress value={this.state.progress} min={0} max={100} />
            </div>
        );
    }
}

// export default createContainer(() => {
//     // let shell = Meteor.npmRequire('shelljs');
//     console.log("Getting strats in path: ${path}");
//     // let strats = shell.ls(path);

//     return {
//         strats: ["Test1", "Test2"],
//         name: "US"
//     };
// }, PnlGraphs);
