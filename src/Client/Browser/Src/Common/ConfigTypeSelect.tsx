import React from 'react';
import MenuItem from 'material-ui/MenuItem';
import { ConfigType } from 'Shared/Model/Config/ShAlpha';
import SelectField from 'material-ui/SelectField';

interface Props {
    onConfigTypeChange  : (origin: ConfigType) => void;
    label               : string;
    configType          : ConfigType;
    readOnly            ?: boolean
    width               ?: number;
}


export default class ConfigTypeSelect extends React.Component<Props, {}> {
    constructor(props: Props) {
        super(props);
    }

    onConfigTypeChange = (event: any, index: number, configType: string) => {
        if (configType == "ALL")
            configType = ConfigType.any;

        if (this.props.onConfigTypeChange) {
            this.props.onConfigTypeChange(configType as ConfigType);
        }
    }

    render() {
        return (
            <SelectField 
                floatingLabelText={this.props.label ? this.props.label : undefined}
                value={this.props.configType == ConfigType.any ? "ALL" : this.props.configType} 
                onChange={this.onConfigTypeChange} 
                style={{width: this.props.width || 100}}
                disabled={this.props.readOnly}
            >
                <MenuItem key={"ALL"} value={"ALL"} primaryText="ALL" />
                <MenuItem key={ConfigType.user} value={ConfigType.user} primaryText="User" />
                <MenuItem key={ConfigType.auto} value={ConfigType.auto} primaryText="Auto" />
            </SelectField>
        );
    }
}
