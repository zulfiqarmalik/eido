import React from 'react';
import * as Colors from 'material-ui/styles/colors';

declare global {
    namespace JSX {
        interface IntrinsicElements {
            font: any
        }
    }
}
// import DateTimeDisplay from '../../Common/DateTimeDisplay';

import {
    TableRow,
    TableRowColumn,
} from 'material-ui/Table';
  
import { CumStats } from 'Shared/Model/Stats/CumStats';

interface Props {
    className       ?: string;
    key             ?: string;
    stats           : CumStats;
    title           : string;
    color           ?: string;
}

export default class StatsRow_Cum extends React.Component<Props, {}> {
    constructor(props: Props) {
        super(props);
    }

    render() {
        return (
            <TableRow>
                <TableRowColumn><font color={ this.props.color ? this.props.color : '' }><b>{this.props.title}</b></font></TableRowColumn>
                <TableRowColumn>{ this.props.stats && this.props.stats.numPoints > 1 ? `${this.props.stats.longCount.toFixed(0)} / ${this.props.stats.shortCount.toFixed(0)}` : "\u2014" }</TableRowColumn>
                <TableRowColumn><b><font color={ this.props.stats && this.props.stats.pnl >= 0.0 && this.props.stats.numPoints > 1 ? Colors.green700 : Colors.red700 }>{ this.props.stats && this.props.stats.numPoints > 1 ? this.props.stats.pnl.toExponential(2) : "\u2014" }</font></b></TableRowColumn>
                <TableRowColumn><b><font color={ this.props.stats && this.props.stats.pnl >= 0.0 && this.props.stats.numPoints > 1 ? Colors.green700 : Colors.red700 }>{ this.props.stats && this.props.stats.numPoints > 1 ? this.props.stats.marginBps.toFixed(2) : "\u2014" }</font></b></TableRowColumn>
                <TableRowColumn>{ this.props.stats && this.props.stats.ir && this.props.stats.numPoints > 1 ? this.props.stats.ir.toFixed(2).toString() + " / " + (this.props.stats.ir * Math.sqrt(252)).toFixed(2).toString() : "\u2014" }</TableRowColumn>
                <TableRowColumn>{ this.props.stats && this.props.stats.volatility && this.props.stats.numPoints > 1 ? this.props.stats.volatility.toFixed(2).toString() : "\u2014" }</TableRowColumn>
                <TableRowColumn>{ (this.props.stats && this.props.stats.returns && this.props.stats.numPoints > 1 ? this.props.stats.returns.toFixed(4) : "\u2014") }</TableRowColumn>
                <TableRowColumn>{ this.props.stats && this.props.stats.tvr && this.props.stats.numPoints > 1 ? this.props.stats.tvr.toFixed(2) : "\u2014" }</TableRowColumn>
                <TableRowColumn>{ this.props.stats && this.props.stats.maxDD && this.props.stats.numPoints > 1 ? this.props.stats.maxDD.toFixed(2) : "\u2014" }</TableRowColumn>
                <TableRowColumn>{ this.props.stats && this.props.stats.maxDH && this.props.stats.numPoints > 1 ? this.props.stats.maxDH : "\u2014" }</TableRowColumn>
                <TableRowColumn>{ this.props.stats && this.props.stats.hitRatio && this.props.stats.numPoints > 1 ? (this.props.stats.hitRatio * 100.0).toFixed(2) : "\u2014" }</TableRowColumn>
            </TableRow>
        );
    }
}
