import React from 'react';
import MenuItem from 'material-ui/MenuItem';
import SelectField from 'material-ui/SelectField';
import { SortOrder } from 'Shared/Model/ShModel';

interface Props {
    onSortOrderChange   : (sortOrder: SortOrder) => void;
    label               : string;
    sortOrder           : SortOrder;
}


export default class SortOrderSelect extends React.Component<Props, {}> {
    constructor(props: Props) {
        super(props);
    }

    onSortOrderChange = (event: any, index: number, sortOrder: string) => {
        if (this.props.onSortOrderChange) {
            this.props.onSortOrderChange(sortOrder as SortOrder);
        }
    }

    render() {
        return (
            <SelectField 
                floatingLabelText={this.props.label ? this.props.label : undefined}
                value={this.props.sortOrder} 
                onChange={this.onSortOrderChange} 
                style={{width: 150}}
            >
                <MenuItem key={SortOrder.desc} value={SortOrder.desc} primaryText={"Descending"} />
                <MenuItem key={SortOrder.asc} value={SortOrder.asc} primaryText={"Ascending"} />
            </SelectField>
        );
    }
}
