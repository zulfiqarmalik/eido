import React from 'react';
import SecGraphFromPosModal from './SecGraphFromPos';
import { Positions } from 'Shared/Model/Stats/Positions';
import { GraphProps, GraphState } from './GraphCommon';

interface RowProps {
    cfigi           : string;
    positions       : Positions;
}

class PositionRow extends React.Component<RowProps, {}> {
    viewSecModal    : any = null;

    constructor(props: RowProps) {
        super(props);
    }

    
    handleViewSec = (e) => {
        this.viewSecModal.open(this.props.cfigi);
    }

    render() {
        return (
            <div>
                <a href="#" onClick={this.handleViewSec}><b>{this.props.cfigi}</b></a> 
                <SecGraphFromPosModal 
                    key={this.props.cfigi}
                    ref={(c) => this.viewSecModal = c} 
                    cfigi={this.props.cfigi} 
                    positions={this.props.positions} 
                />
            </div>
        );
    }
}

///////////////////////////////////////////////////////////////////////////////////////
export default class PosTable extends React.Component<GraphProps, {}> {
    viewSecModal    : any = null;
    viewSecCFigi    : any = null;

    constructor(props: GraphProps) {
        super(props);
    }

    intFormatter(cell, row) {
        let colorTag = "<b><font color='red'>"
        let ivalue = parseInt(cell);
        if (ivalue > 0.0)
            colorTag = "<b><font color='green'>"
        return colorTag + ivalue.toFixed(0) + "</b>";
    }

    floatFormatter(cell, row) {
        let colorTag = "<b><font color='red'>"
        let fvalue = parseFloat(cell);
        if (fvalue > 0.0)
            colorTag = "<b><font color='green'>"
        return colorTag + fvalue.toFixed(2) + "</b>";
    }

    priceFormatter(cell, row) {
        let fvalue = parseFloat(cell);
        return fvalue.toFixed(2);
    }

    pnlFormatter(cell, row) {
        let colorTag = "<b><font color='red'>"
        let fvalue = parseFloat(cell);
        if (fvalue > 0.0)
            colorTag = "<b><font color='green'>"
        return colorTag + fvalue.toFixed(2) + "</b>";
    }

    positionRowFormatter(cell, row) {
        return (
            <PositionRow
                key={cell}
                cfigi={cell} 
                positions={this.props.positions} 
            />
        );
    }

    render() {
        // return (
        //     <BootstrapTable
        //         className={this.props.positions ? "" : "hidden"} 
        //         options={this.options}
        //         key={this.props.positions ? this.props.positions.positions.length : 0} 
        //         data={this.props.positions ? this.props.positions.positions : []}
        //         hover condensed search>

        //         <TableHeaderColumn dataField='bbTicker' isKey={true}>BB-Ticker</TableHeaderColumn>
        //         <TableHeaderColumn dataField='cfigi' dataFormat={::this.positionRowFormatter} dataSort={true}>CFIGI</TableHeaderColumn>
        //         <TableHeaderColumn dataField='numShares' dataSort={true} dataFormat={::this.intFormatter}>Num Shares</TableHeaderColumn>
        //         <TableHeaderColumn dataField='notional' dataSort={true} dataFormat={::this.floatFormatter}>Notional</TableHeaderColumn>
        //         <TableHeaderColumn dataField='price' dataFormat={::this.priceFormatter}>Price</TableHeaderColumn>
        //         <TableHeaderColumn dataField='_pnl' dataSort={true} dataFormat={::this.floatFormatter}>PnL</TableHeaderColumn>
        //     </BootstrapTable>
        // );
        return (<div/>);
    }
}

            // <Table hover condensed>
            //     <thead>
            //         <tr>
            //             <th><b>BB-Ticker</b></th>
            //             <th><b>CFIGI</b></th>
            //             <th><b>Shares</b></th>
            //             <th><b>$ Alloc</b></th>
            //             <th><b>$ Price</b></th>
            //         </tr>
            //     </thead>
            //     <tbody>
            //         {
            //             (this.props.positions ? this.props.positions.positions : []).map((pos) => {
            //                 return <tr key = {pos.cfigi}>
            //                         <td><b><font color="black">{pos.bbTicker}</font></b></td>
            //                         <td>{pos.cfigi}</td>
            //                         <td><b><font color={pos.notional >= 0.0 ? 'green' : 'red'}>{pos.numShares}</font></b></td>
            //                         <td><b><font color={pos.notional >= 0.0 ? 'green' : 'red'}>{pos.notional.toFixed(2)}</font></b></td>
            //                         <td>{pos.price.toFixed(2)}</td>
            //                     </tr>
            //             }
            //         )}
            //     </tbody>
            // </Table>
// export default createContainer(() => {
//     // let shell = Meteor.npmRequire('shelljs');
//     console.log("Getting strats in path: ${path}");
//     // let strats = shell.ls(path);

//     return {
//         strats: ["Test1", "Test2"],
//         name: "US"
//     };
// }, PnlGraphs);
