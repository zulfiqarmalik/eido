import React from 'react';

import { eido } from 'Shared/Core/ShCore';

import MenuItem from 'material-ui/MenuItem';
import Menu from 'material-ui/Menu';
import DropDownMenu from 'material-ui/DropDownMenu';
import { Universes as gUniverses } from 'Shared/Model/Config/ShAlpha';
import PropTypes from 'prop-types';
import Subheader from 'material-ui/Subheader';
import Divider from 'material-ui/Divider';

const ttPlacement = "bottom";

interface Props {
    onMarketChange  : (market: string) => void;
    market          : string;
    style           : any;
}

interface State {
    market          : string;
}

export default class MarketSelect extends React.Component<Props, State> {
    constructor(props: Props) {
        super(props);

        this.state = {
            market: this.props.market
        };
    }

    onMarketChange = (event: any, index: number, market: string) => {
        this.setState({
            market: market
        });

        if (this.props.onMarketChange)
            this.props.onMarketChange(market);
    }

    render() {
        return (
            <DropDownMenu value={this.state.market} onChange={this.onMarketChange} style={this.props.style}>
                <Subheader>America</Subheader>
                <MenuItem value="US" primaryText="U.S.A." />
                <MenuItem value="CA" primaryText="Canada" />
                <MenuItem value="LAT" primaryText="Latin America" />

                <Divider />

                <Subheader>Europe</Subheader>
                <MenuItem value="EU" primaryText="Western Europe" />
                <MenuItem value="EEU" primaryText="Eastern Europe" />

                <Divider />

                <Subheader>Asia-Pacific</Subheader>
                <MenuItem value="JP" primaryText="Japan" />
                <MenuItem value="AS" primaryText="Asia (No Japan)" />
                <MenuItem value="AU" primaryText="Australia" />
            </DropDownMenu>
        );
    }
}
