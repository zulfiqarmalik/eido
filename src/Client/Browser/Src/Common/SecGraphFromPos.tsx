import React from 'react';
import ReactDOM from 'react-dom';
import { eido } from "Shared/Core/ShCore";
import { Debug, ShUtil } from "Shared/Core/ShUtil";
import { Strategy } from 'Client/Model/Config/ClStrategy';
import { GraphProps, GraphState } from './GraphCommon';
import { Positions } from 'Shared/Model/Stats/Positions';
import { Position } from 'Shared/Model/Stats/Position';

let Line = require('react-chartjs-2').Line;

export class SecGraphFromPos extends React.Component<GraphProps, GraphState> {
    constructor(props: GraphProps) {
        super(props);

        this.state = {
            isLoading: false,
            loadingText: "Loading data. Please wait ...",
            chartData: {},
            chartOptions: {
                animation: {
                    duration: 0
                },
                scales: {
                    yAxes: [{
                        scaleLabel: {
                            display: true,
                            labelString: 'P&L',
                            fontSize: 40
                        },
                        ticks: {
                            // Create scientific notation labels
                            callback: function (value, index, values) {
                                return value.toFixed(2);
                            }
                        }
                    }],
                    xAxes: [{
                        display: true,
                        scaleLabel: {
                            display: true,
                            labelString: 'Dates',
                            fontSize: 40
                        },
                    }],
                }
            }
        };
    }

    updateGraph() {
        let positions = this.props.positions;
        if (!positions || !this.props.position)
            return;

        console.log("[SecGraphFromPos] Updating graph!");
        let posLUT = this.props.positions.getPositionsLUT();
        let index = posLUT[this.props.position.cfigi];
        let posPnl = this.props.positions.posPnls[index];
        let pnls = new Array(posPnl.pnls.length).fill(0.0);
        let totalPnl = 0.0;

        posPnl.pnls.map((pnl, index) => {
            pnls[index] = totalPnl;
            totalPnl += pnl;
        });

        let chartData = {
            labels: this.props.positions.dates,
            xAxisID: "Dates",
            yAxisID: "Cum-PnL",
            datasets: [{
                label: "Cum P&L",
                borderWidth: 1,
                lineTension: 0,
                pointRadius: pnls.length < 100 ? 3 : 0,
                backgroundColor: 'transparent',
                borderColor: pnls.length && pnls[pnls.length - 1] > 0.0 ? 'green' : 'red',
                steppedLine: false,
                data: pnls,
            }]
        };

        this.setState({
            isLoading: false,
            chartData: chartData
        });
    }

    componentDidMount() {
        this.updateGraph();
    }

    render() {
        return (
            <Line data={this.state.chartData} options={this.state.chartOptions} />
        );
    }
}

///////////////////////////////////////////////////////////////////////////////////////////////////
interface Props {
    key             ?: string;
    ref             ?: any;
    cfigi           ?: string;
    uuid            ?: string;
    positions       ?: Positions;
}

interface State {
    showModal       : boolean;
    uuid            : string;
    position        : Position;
    index           : number;
    canGoNext       : boolean;
    canGoPrev       : boolean;
}

export default class SecGraphFromPosModal extends React.Component<Props, State> {
    constructor(props: Props) {
        super(props);

        let posLUT = this.props.positions.getPositionsLUT();
        let index = posLUT[this.props.cfigi];
        let position = this.props.positions.positions[index];

        this.state = {
            showModal: false,
            uuid: position.cfigi,
            position: position,
            index: index,
            canGoNext: (index < this.props.positions.positions.length - 1),
            canGoPrev: (index > 0)
        };
    }

    close() {
        this.setState({ showModal: false });
    }

    open(cfigi) {
        let index = this.state.index;
        if (cfigi) {
            let posLUT = this.props.positions.getPositionsLUT();
            index = posLUT[this.props.cfigi];
        }

        this.updateView(index);
    }
    
    updateView(index) {
        let position = this.props.positions.positions[this.state.index];
        this.setState({
            uuid: position.cfigi,
            canGoNext: (index < this.props.positions.positions.length - 1),
            canGoPrev: (index > 0),
            index: index,
            position: position,
            showModal: true
        });
    }

    onNext() {
        let index = this.state.index + 1;
        if (index >= this.props.positions.positions.length) 
            return;

        this.updateView(index);
    }

    onPrev() {
        let index = this.state.index - 1;
        if (index < 0)
            return;

        this.updateView(index);
    }

    render() {
        return (
            <div/>
            // <Modal show={this.state.showModal} onHide={::this.close} lg>
            //     <Modal.Header closeButton>
            //         <Modal.Title><b>[{this.state.index + 1} / {this.props.positions.positions.length}] - {this.state.position.cfigi} | {this.state.position.bbTicker}</b></Modal.Title>
            //         <div className="modal-header-buttons">
            //             <Button className={!this.state.canGoPrev ? "hidden" : ""} onClick={::this.onPrev} bsStyle='warning'>Prev</Button>
            //             <Button className={!this.state.canGoNext ? "hidden" : ""} onClick={::this.onNext} bsStyle='info'>Next</Button>
            //             {/*<Button bsStyle='danger'>DELETE</Button>*/}
            //         </div>

            //     </Modal.Header>

            //     <Modal.Body>
            //         <Grid>
            //             <Row>
            //                 <SecGraphFromPos
            //                     key={this.state.position.cfigi}
            //                     positions={this.props.positions}
            //                     position={this.state.position}
            //                     index={this.state.index}
            //                 />
            //             </Row>
            //         </Grid>

            //     </Modal.Body>
            // </Modal>
        );
    }
}

