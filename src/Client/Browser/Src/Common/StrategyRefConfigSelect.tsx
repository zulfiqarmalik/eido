import React from 'react';

import MenuItem from 'material-ui/MenuItem';
import { Universes as gUniverses, SecMasterId, RefConfigInfo } from 'Shared/Model/Config/ShAlpha';
import SelectField from 'material-ui/SelectField';

export interface Props { 
    onRefConfigChange   : (refConfigfilename: string) => void;
    refConfigs          : RefConfigInfo[];
    label               : string;
    readOnly            : boolean;
}

export interface State {
    refConfigfilename   : string;
}

export default class StrategyRefConfigSelect extends React.Component<Props, State> {
    constructor(props: Props) {
        super(props);

        this.state = {
            refConfigfilename: ""
        };
    }

    onRefConfigChange = (event, index, refConfigfilename) => {
        this.setState({
            refConfigfilename: refConfigfilename
        });

        if (this.props.onRefConfigChange)
            this.props.onRefConfigChange(refConfigfilename);
    }

    render() {
        return (
            <SelectField 
                floatingLabelText={this.props.label ? this.props.label : undefined}
                value={this.state.refConfigfilename} 
                onChange={this.onRefConfigChange} 
                style={{width: "100%" }}
                disabled={this.props.readOnly}
            >
                {(this.props.refConfigs).map((rci, i) => {
                    return <MenuItem key={rci.filename} value={rci.filename} primaryText={rci.filename} />
                })}
            </SelectField>
        );
    }
}
