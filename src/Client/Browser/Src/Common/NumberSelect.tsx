import React from 'react';
import MenuItem from 'material-ui/MenuItem';
import SelectField from 'material-ui/SelectField';
import { SortOrder } from 'Shared/Model/ShModel';

interface Props {
    onNumberSelectChange: (value: number) => void;
    label               : string;
    start               : number;
    end                 : number;
    incr                : number;
    value               : number;
    prefix              : string;
    suffix              : string;
}

interface State {
    numbers             : Array<number>;
}

export default class NumberSelect extends React.Component<Props, State> {
    constructor(props: Props) {
        super(props);

        this.state = {
            numbers: []
        }
    }

    componentDidMount() {
        let numbers = [];
        for (let i = this.props.start; i < this.props.end; i += this.props.incr) 
            numbers.push(i);

        this.setState({
            numbers: numbers
        });
    }

    onNumberSelectChange = (event: any, index: number, num: string) => {
        if (this.props.onNumberSelectChange) {
            this.props.onNumberSelectChange(parseInt(num));
        }
    }

    render() {
        return (
            <SelectField 
                floatingLabelText={this.props.label ? this.props.label : undefined}
                value={`${this.props.value}`} 
                onChange={this.onNumberSelectChange} 
                style={{width: 100}}
            >
                {
                    this.state.numbers.map((n: number) => {
                        return <MenuItem key={`${n}`} value={`${n}`} primaryText={`${this.props.prefix}${n}${this.props.suffix}`} />
                    })
                }
            </SelectField>
        );
    }
}
