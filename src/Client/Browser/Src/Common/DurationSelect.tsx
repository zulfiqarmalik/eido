import React from 'react';
import ReactDOM from 'react-dom';
import { eido } from "Shared/Core/ShCore";
import { Debug, ShUtil } from "Shared/Core/ShUtil";

import MenuItem from 'material-ui/MenuItem';
import Menu from 'material-ui/Menu';
import DropDownMenu from 'material-ui/DropDownMenu';
import Subheader from 'material-ui/Subheader';
import Divider from 'material-ui/Divider';

import IconButton from 'material-ui/IconButton';
import FontIcon from 'material-ui/FontIcon';
import * as Colors from 'material-ui/styles/colors';

import TextField from 'material-ui/TextField';
import SelectField from 'material-ui/SelectField';
import {Toolbar, ToolbarGroup, ToolbarSeparator, ToolbarTitle} from 'material-ui/Toolbar';

import PropTypes from 'prop-types';
import { LTStats } from 'Shared/Model/Stats/LTStats';
import { PnlSpace, StatsDuration } from 'Shared/Model/Stats/CumStats';

interface Props {
    onDurationChange    : (duration: StatsDuration) => void;
    onDatesChanged      ?: (startDate: number, endDate: number) => void;
    onPositionChange    ?: (position: string) => void;
    onSpaceChange       ?: (space: PnlSpace) => void;
    show                ?: boolean;
    showPnlSpace        ?: boolean;
    defaultValue        ?: StatsDuration;
    defaultSpaceValue   ?: PnlSpace;
    stats               ?: LTStats;
    allowIndividualDate ?: boolean;
    allowCustomDate     ?: boolean;
    positionFiles       ?: string[];
    hide                ?: boolean;
}

interface State {
    positionFile        ?: string;
    showPositionSelect  ?: boolean;
    showDateRange       ?: boolean;
    duration            ?: StatsDuration;
    space               ?: PnlSpace;
    startDate           ?: number;
    endDate             ?: number;
    startDateValid      ?: boolean;
    endDateValid        ?: boolean;
}

export default class DurationSelect extends React.Component<Props, State> {
    static contextTypes = {
        onDurationChange: PropTypes.func
    };

    static childContextTypes = {
        onDurationChange: PropTypes.func.isRequired
    };

    constructor(props: Props) {
        super(props);

        this.state = {
            positionFile: this.props.positionFiles ? this.props.positionFiles[0] : "",
            showPositionSelect: false,
            showDateRange: false,

            duration: this.props.defaultValue ? this.props.defaultValue : StatsDuration.LT,
            space: this.props.defaultSpaceValue ? this.props.defaultSpaceValue : PnlSpace.pnl,

            startDate: this.props.stats ? this.props.stats.getOSStartDate() : 0,
            endDate: this.props.stats ? this.props.stats.getEndDate() : 0,
            startDateValid: true,
            endDateValid: true
        }
    }

    onDurationChange = (e: any, index: number, value: string) => {
        let state = {
            showPositionSelect: (value as StatsDuration) === StatsDuration.singleDate ? true : false,
            showDateRange: (value as StatsDuration) === StatsDuration.customDate ? true : false,
            duration: (value as StatsDuration)
        };

        if (this.props.stats) {
            if (!this.state.startDate)
                state["startDate"] = this.props.stats.getStartDate();
            if (!this.state.endDate)
                state["endDate"] = this.props.stats.getEndDate();
        }

        this.setState(state);

        this.props.onDurationChange(value as StatsDuration);
    }

    onDatesRefresh = () => {
        if (this.props.onDatesChanged) {
            this.props.onDatesChanged(this.state.startDate, this.state.endDate);
        }
    }

    onStartDateChange = (e: any, value: string) => this.setState({ startDate: parseInt(value) })
    onEndDateChange = (e: any, value: string) => this.setState({ endDate: parseInt(value) })

    onPositionChange = (e) => {
        if (this.props.onPositionChange)
            this.props.onPositionChange(e);
    }

    onSpaceChange = (e, index, value: string) => {
        this.setState({
            space: value as PnlSpace
        });

        if (this.props.onSpaceChange)
            this.props.onSpaceChange(value as PnlSpace);
    }

    componentDidMount() {
        if (this.props.stats) {
            this.setState({
                startDate: this.props.stats.getOSStartDate(),
                endDate: this.props.stats.getEndDate()
            });
        }
    }

    render() {
        return (
            <ToolbarGroup>
                <TextField
                    hintText="YYYYMMDD"
                    floatingLabelText="Start Date"
                    className={this.state.showDateRange ? "" : "hidden"}
                    value={this.state.startDate.toString()}
                    style={{width: 100}}
                    onChange={this.onStartDateChange}
                />

                <TextField
                    hintText="YYYYMMDD"
                    floatingLabelText="End Date"
                    className={this.state.showDateRange ? "" : "hidden"}
                    value={this.state.endDate.toString()}
                    style={{width: 100}}
                    onChange={this.onEndDateChange}
                />

                <IconButton onClick={this.onDatesRefresh} className={this.state.showDateRange ? "" : "hidden"}>
                    <FontIcon color={Colors.grey400} className="material-icons">refresh</FontIcon>
                </IconButton>                    


                <SelectField
                    floatingLabelText="Date"
                    onChange={this.onPositionChange}
                    className={this.state.showPositionSelect ? "" : "hidden"}
                >
                    {
                        (this.props.positionFiles ? this.props.positionFiles : []).map((positionFile, index) => {
                            return <MenuItem key={index} value={index} primaryText={positionFile} />;
                        }
                    )}
                </SelectField>

                <SelectField
                    floatingLabelText="Pnl Space"
                    value={this.state.space}
                    onChange={this.onSpaceChange}
                    className={this.props.showPnlSpace ? "" : "hidden"}
                    style={{width: 150}}
                >
                    <MenuItem key={PnlSpace.pnl} value={PnlSpace.pnl} primaryText={`P&L`} />
                    <MenuItem key={PnlSpace.returns} value={PnlSpace.returns} primaryText="Returns" />
                </SelectField>

                <SelectField
                    floatingLabelText="Duration"
                    className={this.props.hide ? "hidden" : ""}
                    value={this.state.duration}
                    onChange={this.onDurationChange}
                    style={{width: 200}}
                >
                        <MenuItem key="LT" value="LT" primaryText="Lifetime" />
                        <MenuItem key="IS" value="IS" primaryText="In Sample" /> 
                        <MenuItem key="OS" value="OS" primaryText="Out Sample" /> 
                        <MenuItem key="PS" value="PS" primaryText="Trading" /> 

                        <Divider />

                        <MenuItem key="D1" value="D1" primaryText="1 Day" />
                        <MenuItem key="D5" value="D5" primaryText="5 Days" />
                        <MenuItem key="D10" value="D10" primaryText="10 Days" />
                        <MenuItem key="D21" value="D21" primaryText="21 Days" />
                        <MenuItem key="D63" value="D63" primaryText="63 Days" />
                        <MenuItem key="D120" value="D120" primaryText="120 Days" />
                        <MenuItem key="D250" value="D250" primaryText="250 Days" />

                        { <Divider /> }
                        {
                            (this.props.stats && this.props.stats.annual ? this.props.stats.annual : []).sort((a, b) => b.date - a.date).slice(0).map((astats, index) => {
                                return <MenuItem key={`Y${astats.name}`} value={`Y${astats.name}`} primaryText={astats.name} />;
                            }
                        )}

                        { <Divider /> }
                        {
                            (this.props.allowCustomDate ? [""] : []).map(() => {
                                return <MenuItem key="CD" value="CD" primaryText="Custom Date" />;
                            }
                        )}

                        {
                            (this.props.allowIndividualDate ? [""] : []).map(() => {
                                return <MenuItem key="SD" value="SD" primaryText="Single Date"/>;
                            }
                        )}

                </SelectField>
            </ToolbarGroup>
        );
    }
}

// export default createContainer(() => {
//     // let shell = Meteor.npmRequire('shelljs');
//     console.log("Getting strats in path: ${path}");
//     // let strats = shell.ls(path);

//     return {
//         strats: ["Test1", "Test2"],
//         name: "US"
//     };
// }, PnlGraphs);
