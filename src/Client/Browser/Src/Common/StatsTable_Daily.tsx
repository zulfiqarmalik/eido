import React from 'react';
import ReactDOM from 'react-dom';
import { eido } from "Shared/Core/ShCore";
import { Debug, ShUtil } from "Shared/Core/ShUtil";
import StatsRow_Daily from './StatsRow_Daily';

import {
    Table,
    TableBody,
    TableHeader,
    TableHeaderColumn,
    TableRow,
    TableRowColumn,
} from 'material-ui/Table';
  
// import DateTimeDisplay from '../../Common/DateTimeDisplay';

import PropTypes from 'prop-types';
import { Strategy } from 'Client/Model/Config/ClStrategy';
import { CumStats } from 'Shared/Model/Stats/CumStats';
import { Stats } from 'Shared/Model/Stats/Stats';

interface Props {
    stats           : CumStats;
    duration        : string;
    startDate       : number;
    endDate         : number;
}

interface State {
    points          : Stats[];
}

export default class StatsTable_Daily extends React.Component<Props, State> {
    constructor(props: Props) {
        super(props);

        this.state = {
            points: [],
        };
    }

    updatePoints() {
        if (!this.props.stats)
            return;

        this.setState({
            points: this.props.stats.getPoints(this.props.duration, this.props.startDate, this.props.endDate)
        });
    }

    componentDidMount() {
        this.updatePoints();
    }

    render() {
        return (
            <Table
                selectable={false}
                fixedFooter={false}
            >
                <TableHeader displaySelectAll={false} adjustForCheckbox={false}>
                    <TableRow>
                        <TableHeaderColumn><b>Date</b></TableHeaderColumn>
                        {/* <TableHeaderColumn><b>C-PL</b></TableHeaderColumn> */}
                        <TableHeaderColumn><b>D-PnL</b></TableHeaderColumn>
                        <TableHeaderColumn><b>D-Returns</b></TableHeaderColumn>
                        {/* <TableHeaderColumn><b>Margin</b></TableHeaderColumn> */}
                        <TableHeaderColumn><b>D-Turnover</b></TableHeaderColumn>
                        <TableHeaderColumn><b>$-Traded</b></TableHeaderColumn>
                        <TableHeaderColumn><b>$-Held</b></TableHeaderColumn>
                        {/* <TableHeaderColumn><b>$-Long</b></TableHeaderColumn> */}
                        {/* <TableHeaderColumn><b>$-Short</b></TableHeaderColumn> */}
                        {/* <TableHeaderColumn><b>$-Liq</b></TableHeaderColumn> */}
                        <TableHeaderColumn><b>N-Long</b></TableHeaderColumn>
                        <TableHeaderColumn><b>N-Short</b></TableHeaderColumn>
                        <TableHeaderColumn><b>N-Liquidate</b></TableHeaderColumn>
                        <TableHeaderColumn><b>Cost</b></TableHeaderColumn>
                        <TableHeaderColumn><b>L-Pnl</b></TableHeaderColumn>
                        <TableHeaderColumn><b>S-PnL</b></TableHeaderColumn>
                    </TableRow>
                </TableHeader>
                <TableBody displayRowCheckbox={false}>
                    {
                        this.state.points.map((ptStats, index) => {
                            return <StatsRow_Daily key={ index.toString() } ptStats={ ptStats } index={ index } />;
                        }
                    )}
                </TableBody>
            </Table>
        );
    }
}

// export default createContainer(() => {
//     // let shell = Meteor.npmRequire('shelljs');
//     console.log("Getting strats in path: ${path}");
//     // let strats = shell.ls(path);

//     return {
//         strats: ["Test1", "Test2"],
//         name: "US"
//     };
// }, PnlGraphs);
