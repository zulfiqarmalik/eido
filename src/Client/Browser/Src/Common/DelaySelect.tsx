import React from 'react';
import MenuItem from 'material-ui/MenuItem';
import SelectField from 'material-ui/SelectField';

interface Props {
    onDelayChange   : (delay: number) => void;
    label           : string;
    delay           : number;
    readOnly        ?: boolean;
}

export default class DelaySelect extends React.Component<Props, {}> {
    constructor(props: Props) {
        super(props);
    }

    onDelayChange = (event: any, index: number, delay: string) => {
        if (this.props.onDelayChange) {
            this.props.onDelayChange(parseInt(delay));
        }
    }

    render() {
        return (
            <SelectField 
                floatingLabelText={this.props.label ? this.props.label : undefined}
                value={`${this.props.delay}`} 
                onChange={this.onDelayChange} 
                style={{width: 100}}
                disabled={this.props.readOnly}
            >
                <MenuItem key={"0"} value={"0"} primaryText={"0 Day"} />
                <MenuItem key={"1"} value={"1"} primaryText={"1 Day"} />
                <MenuItem key={"-1"} value={"-1"} primaryText={"Intraday"} />
            </SelectField>
        );
    }
}
