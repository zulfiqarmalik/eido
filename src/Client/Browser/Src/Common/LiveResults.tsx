import React from 'react';
import PropTypes from 'prop-types';

import { eido } from 'Shared/Core/ShCore';
import { Strategy } from 'Client/Model/Config/ClStrategy';
import { User } from 'Client/Model/User/ClUser';
import LinearProgress from 'material-ui/LinearProgress';
import FloatingActionButton from 'material-ui/FloatingActionButton';
import Divider from 'material-ui/Divider';

import { DefMarket as gDefMarket, ShAlpha } from 'Shared/Model/Config/ShAlpha';
import PnlGraph from '../Common/PnlGraph';

import RGL, { WidthProvider } from "react-grid-layout";
import { Alpha } from 'Client/Model/Config/ClAlpha';
import { LiveStats } from 'Shared/Model/Config/BaseConfig';
import { LTStats } from 'Shared/Model/Stats/LTStats';
import * as Colors from 'material-ui/styles/colors';
import { PnlSpace } from 'Shared/Model/Stats/CumStats';

const ReactGridLayout = WidthProvider(RGL);

let BottomSheet = require('material-ui-bottom-sheet').BottomSheet;
let CloseIcon = require('mui-icons/fontawesome/angle-double-down').default;
let OpenIcon = require('mui-icons/fontawesome/angle-double-up').default;

interface Props {
    alpha               : ShAlpha;
    onShowOutputWindow  ?: () => void;
    onHideOutputWindow  ?: () => void;
    showOutputWindow    ?: boolean;

    numStats            ?: number;
    numSubStats         ?: number;
    numSuperStats       ?: number;

    liveStats           ?: LTStats;
    subLiveStats        ?: LTStats;
    superLiveStats      ?: LTStats;

    isRunning           ?: boolean;
    isSubRunning        ?: boolean;
    isSuperRunning      ?: boolean;

    showSub             ?: boolean;
    showSuper           ?: boolean;

    runPercent          ?: number;
    subRunPercent       ?: number;
    superRunPercent     ?: number;

    logMsgs             ?: string[];
}

interface State {
    showOutputWindow    : boolean;
}

export default class LiveResults extends React.Component<Props, State> {
    constructor(props: Props) {
        super(props);

        this.state = {
            showOutputWindow: this.props.showOutputWindow
        };
    }

    onShowOutputWindow = () => {
        this.setState({showOutputWindow: true});

        if (this.props.onShowOutputWindow)
            this.props.onShowOutputWindow();
    }

    onHideOutputWindow = () => {
        this.setState({showOutputWindow: false});

        if (this.props.onHideOutputWindow)
            this.props.onHideOutputWindow();
    }

    formatLogMsg(logMsg, i) {
        let type = parseInt(logMsg[0]);
        let msg = logMsg.substr(2); 
        let color = "green";

        if (type == 1 || type == 2 || type == 3)
            color = "red";
        else if (type == 4 || type == 5)
            color = "yellow";
        else if (type == 6)
            color = "silver";
        else if (type == 7)
            color = "lime";
        // else if (type == 8)
        //     color = "silver";

        return (<div key={i}><span style={{color: color}}>{msg}</span> <br/></div>);
    }

    getGraphKey(): string {
        if (!this.props.showSub || !this.props.showSuper)
            return `${this.props.numStats}`;

        return `${this.props.numStats}_${this.props.numSubStats}_${this.props.numSuperStats}`;
    }

    render() {
        return (
            <div className="live-results">
                <BottomSheet
                    action={
                        <FloatingActionButton secondary={true} onClick={this.onHideOutputWindow}>
                            <CloseIcon />
                        </FloatingActionButton>
                    }

                    onRequestClose={this.onHideOutputWindow}
                    open={this.state.showOutputWindow}
                >
                    <h4 style={{ marginLeft: 40, color: Colors.white }}>{this.getOutputString()}</h4>

                    <div className="output-window">
                        <ReactGridLayout
                            className="layout"
                            style={{ height: "100%" }}
                            autoSize={false}
                            margin={[2, 2]}
                            verticalCompact={true}
                            rowHeight={15}
                        >
                            <div
                                key="1"
                                data-grid={{ x: 0, y: 0, w: 12, h: 1, static: true }}
                            >
                                <LinearProgress 
                                    mode="determinate" 
                                    style={{height: this.props.isRunning ? 10 : 0}}
                                    value={this.props.runPercent} 
                                    color={Colors.blue400}
                                /> 

                                <LinearProgress 
                                    mode="determinate" 
                                    style={{height: this.props.isRunning && this.props.showSub ? 10 : 0}}
                                    value={this.props.subRunPercent} 
                                    color={Colors.pink400}
                                /> 

                                <LinearProgress 
                                    mode="determinate" 
                                    style={{height: this.props.isRunning && this.props.showSuper ? 10 : 0}}
                                    value={this.props.superRunPercent} 
                                    color={Colors.yellow400}
                                /> 
                                <Divider/>
                            </div>

                            <div
                                key="2"
                                data-grid={{ x: 0, y: 1, w: 12, h: 20, static: true }}
                                className="main-content"
                            >
                                <ReactGridLayout
                                    className="layout"
                                    style={{ height: "100%" }}
                                    autoSize={false}
                                    margin={[2, 2]}
                                    verticalCompact={true}
                                    rowHeight={400}
                                >
                                    <div
                                        key="2.1"
                                        data-grid={{ x: 0, y: 0, w: 8, h: 1, static: true }}
                                    >
                                        <div className="dev-output-window" id="dev-output">
                                            { (this.props.logMsgs ? this.props.logMsgs : []).map((logMsg, i) => {
                                                if (logMsg)
                                                return this.formatLogMsg(logMsg, i);
                                            })}
                                        </div>
                                    </div>

                                    <div
                                        key="2.2"
                                        data-grid={{ x: 8, y: 0, w: 4, h: 2, static: true }}
                                    >
                                        <a 
                                            className="pnl-graph-anchor"
                                            href={`#p=developer&market=${this.props.alpha ? this.props.alpha.market : gDefMarket}&alphaId=${this.props.alpha ? this.props.alpha._id : ""}&viewResults=true`}>
                                            <div id="live-pnl-graph">
                                                <PnlGraph
                                                    stats={this.props.liveStats}
                                                    mainColor={Colors.blue400}

                                                    rawStats={this.props.subLiveStats}
                                                    rawStatsLabel="Sub-Universe"
                                                    rawColor={Colors.pink400}
                                                    showRaw={this.props.showSub}
                                                    
                                                    preoptStats={this.props.superLiveStats}
                                                    preoptStatsLabel="Super-Universe"
                                                    preoptColor={Colors.yellow400}
                                                    showPreOpt={this.props.showSuper}

                                                    key={this.getGraphKey()}
                                                    hidePoints={true}
                                                    numPoints={this.props.numStats}
                                                    disableAnimation={true}
                                                    space={PnlSpace.returns}
                                                    lineWidth={1}
                                                    useIndividualPlotIndexes={true}
                                                />
                                            </div>
                                        </a>
                                    </div>
                                </ReactGridLayout>
                            </div>
                        </ReactGridLayout>
                    </div>
                </BottomSheet>


                {/* <OverlayTrigger placement="top" tooltip={TT("Show Output")}> */}
                    <FloatingActionButton 
                        className={!this.props.showOutputWindow ? "dev-open-output standout-button" : "hidden"} 
                        secondary={true}
                        onClick={this.onShowOutputWindow}
                    >
                        <OpenIcon />
                    </FloatingActionButton>
                {/* </OverlayTrigger> */}

            </div>
        );
    }

    getOutputString() {
        if (this.props.isRunning)
            return "Running Simulation [" + this.props.runPercent.toString() + " %]";
        return "Output";
    }

    noOp() {}
}
