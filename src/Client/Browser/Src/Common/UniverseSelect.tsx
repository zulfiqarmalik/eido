import React from 'react';

import MenuItem from 'material-ui/MenuItem';
import { Universes as gUniverses, SecMasterId } from 'Shared/Model/Config/ShAlpha';
import SelectField from 'material-ui/SelectField';

export interface Props { 
    onUniverseChange    : (universeId: string) => void;
    label               ?: string;
    universeId          : string;
    market              : string;
    extraUniverses      ?: string[];
    readOnly            : boolean;
    showSecMaster       ?: boolean;
    width               ?: number;
}

export interface State {
    universeId          : string;
}

export default class UniverseSelect extends React.Component<Props, State> {
    constructor(props: Props) {
        super(props);

        this.state = {
            universeId: this.props.universeId
        };
    }

    onUniverseChange = (event, index, universeId) => {
        this.setState({
            universeId: universeId
        });

        if (this.props.onUniverseChange)
            this.props.onUniverseChange(universeId);
    }

    render() {
        return (
            <SelectField 
                floatingLabelText={this.props.label ? this.props.label : undefined}
                value={this.state.universeId} 
                onChange={this.onUniverseChange} style={{width: !this.props.showSecMaster ? 125 : 150 }}
                disabled={this.props.readOnly}
            >
                {(this.props.showSecMaster ? [SecMasterId] : []).map((uname, i) => {
                    return <MenuItem key={uname} value={uname} primaryText={uname} />
                })}

                {(this.props.extraUniverses ? this.props.extraUniverses : []).map((uname, i) => {
                    return <MenuItem key={uname} value={uname} primaryText={uname} />
                })}

                {gUniverses[this.props.market].map((uname, i) => {
                    return <MenuItem key={uname} value={uname} primaryText={uname} />
                })}
            </SelectField>
        );
    }
}
