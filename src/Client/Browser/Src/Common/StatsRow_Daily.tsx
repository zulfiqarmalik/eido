import React from 'react';
import ReactDOM from 'react-dom';
import { eido } from "Shared/Core/ShCore";
import { Debug, ShUtil } from "Shared/Core/ShUtil";
import * as Colors from 'material-ui/styles/colors';

// import DateTimeDisplay from '../../Common/DateTimeDisplay';

import {
    Table,
    TableBody,
    TableHeader,
    TableHeaderColumn,
    TableRow,
    TableRowColumn,
} from 'material-ui/Table';
  
import PropTypes from 'prop-types';
import { Strategy } from 'Client/Model/Config/ClStrategy';
import { Stats } from 'Shared/Model/Stats/Stats';

interface Props {
    ptStats         : Stats;
    key             ?: string;
    index           ?: number;
}

export default class StatsRow_Daily extends React.Component<Props, {}> {
    constructor(props: Props) {
        super(props);
    }

    render() {
        return (
            <TableRow>
                <TableRowColumn>{this.props.ptStats ? this.props.ptStats.date : "0"}</TableRowColumn>
                {/* <TableRowColumn><b><font color={this.props.ptStats && this.props.ptStats.cumPnl >= 0.0 ? Colors.green700 : Colors.red700}>{this.props.ptStats ? this.props.ptStats.cumPnl.toExponential(2) : "0"}</font></b></TableRowColumn> */}
                <TableRowColumn><b><font color={this.props.ptStats && this.props.ptStats.pnl >= 0.0 ? Colors.green700 : Colors.red700}>{this.props.ptStats ? this.props.ptStats.pnl.toExponential(2) : "0"}</font></b></TableRowColumn>
                <TableRowColumn><b><font color={this.props.ptStats && this.props.ptStats.returns >= 0.0 ? Colors.green700 : Colors.red700}>{this.props.ptStats ? this.props.ptStats.returns.toFixed(2) : "0"}</font></b></TableRowColumn>
                {/* <TableRowColumn><b><font color={this.props.ptStats && this.props.ptStats.marginBps >= 0.0 ? Colors.green700 : Colors.red700}>{this.props.ptStats ? this.props.ptStats.marginBps.toFixed(2) : "0"}</font></b></TableRowColumn> */}
                <TableRowColumn>{this.props.ptStats ? this.props.ptStats.tvr.toFixed(2) : "0"}</TableRowColumn>
                <TableRowColumn>{this.props.ptStats ? this.props.ptStats.tradedNotional.toExponential(2) : "0"}</TableRowColumn>
                <TableRowColumn>{this.props.ptStats ? this.props.ptStats.heldNotional.toExponential(2) : "0"}</TableRowColumn>
                {/* <TableRowColumn>{this.props.ptStats ? this.props.ptStats.longNotional.toExponential(2) : "0"}</TableRowColumn> */}
                {/* <TableRowColumn>{this.props.ptStats ? this.props.ptStats.shortNotional.toExponential(2) : "0"}</TableRowColumn> */}
                {/* <TableRowColumn>{this.props.ptStats ? this.props.ptStats.liquidateNotional.toExponential(2) : "0"}</TableRowColumn> */}
                <TableRowColumn>{this.props.ptStats ? Math.round(this.props.ptStats.longCount) : "0"}</TableRowColumn>
                <TableRowColumn>{this.props.ptStats ? Math.round(this.props.ptStats.shortCount) : "0"}</TableRowColumn>
                <TableRowColumn>{this.props.ptStats ? Math.round(this.props.ptStats.liquidateCount) : "0"}</TableRowColumn>
                <TableRowColumn>{this.props.ptStats ? this.props.ptStats.cost.toFixed(2) : "0"}</TableRowColumn>
                <TableRowColumn><b><font color={this.props.ptStats && this.props.ptStats.longPnl >= 0.0 ? Colors.green700 : Colors.red700}>{this.props.ptStats ? this.props.ptStats.longPnl.toExponential(2) : "0"}</font></b></TableRowColumn>
                <TableRowColumn><b><font color={this.props.ptStats && this.props.ptStats.shortPnl >= 0.0 ? Colors.green700 : Colors.red700}>{this.props.ptStats ? this.props.ptStats.shortPnl.toExponential(2) : "0"}</font></b></TableRowColumn>
            </TableRow>
        );
    }
}

