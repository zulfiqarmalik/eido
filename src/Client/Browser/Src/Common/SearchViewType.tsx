import React from 'react';

import MenuItem from 'material-ui/MenuItem';
import SelectField from 'material-ui/SelectField';

import Divider from 'material-ui/Divider';

interface Props {
    onSearchViewChange  : (searchView: string) => void;
    searchView          : string;
    label               ?: string;
    width               ?: number;
}

export default class SearchViewType extends React.Component<Props, {}> {
    sortLUT         : any = {};

    constructor(props: Props) {
        super(props);
    }

    onSearchViewChange = (event: any, index: number, searchView: string) => {
        if (this.props.onSearchViewChange) {
            this.props.onSearchViewChange(searchView);
        }
    }

    render() {
        return (
            <SelectField 
                floatingLabelText={this.props.label ? this.props.label : undefined}
                value={this.props.searchView} 
                onChange={this.onSearchViewChange} 
                style={{width: this.props.width || 200}}
            >
                <MenuItem key="LT" value="LT" primaryText="Lifetime" />
                <MenuItem key="IS" value="IS" primaryText="In-Sample" />
                <MenuItem key="OS" value="OS" primaryText="Out-Sample" />

                <Divider />

                <MenuItem key="2018" value="2018" primaryText="2018" />
                <MenuItem key="2017" value="2017" primaryText="2017" />
                <MenuItem key="2016" value="2016" primaryText="2016" />
                <MenuItem key="2015" value="2015" primaryText="2015" />

                <Divider />

                <MenuItem key="D250" value="D250" primaryText="250 Days" />
                <MenuItem key="D120" value="D120" primaryText="120 Days" />
                <MenuItem key="D63" value="D63" primaryText="63 Days" />
                <MenuItem key="D21" value="D21" primaryText="21 Days" />
                <MenuItem key="D10" value="D10" primaryText="10 Days" />
                <MenuItem key="D5" value="D5" primaryText="5 Days" />

            </SelectField>
        );
    }
}
