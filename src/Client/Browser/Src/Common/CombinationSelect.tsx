import React from 'react';

import { eido } from 'Shared/Core/ShCore';

import MenuItem from 'material-ui/MenuItem';
import Menu from 'material-ui/Menu';
import DropDownMenu from 'material-ui/DropDownMenu';
import { Universes as gUniverses } from 'Shared/Model/Config/ShAlpha';
import Subheader from 'material-ui/Subheader';
import Divider from 'material-ui/Divider';
import SelectField from 'material-ui/SelectField';

const ttPlacement = "bottom";

const style = {
    paddingLeft: 10,
    width: 200
};

interface State {
    combo               : string;
}

interface Props {
    onComboChange       : (combo: string) => void;
    combo               : string;
}

export default class CombinationSelect extends React.Component<Props, State> {
    constructor(props: Props) {
        super(props);

        this.state = {
            combo: this.props.combo
        };
    }

    onComboChange = (event: any, index: number, combo: string) => {
        this.setState({
            combo: combo
        });

        if (this.props.onComboChange)
            this.props.onComboChange(combo);
    }

    render() {
        return (
            <SelectField 
                value={this.state.combo} 
                onChange={this.onComboChange} style={style}
                floatingLabelText="Combination"
            >
                <Subheader>Basic Combination</Subheader>
                <MenuItem value="BasicCombination_Add" primaryText="Add" />
                <MenuItem value="BasicCombination_Subtract" primaryText="Subtract" />
                <MenuItem value="BasicCombination_Multiply" primaryText="Multiply" />
                <MenuItem value="BasicCombination_Divide" primaryText="Divide" />
                
                <Subheader>Smart Combination</Subheader>
                <MenuItem value="SmartCombination_ST" primaryText="Short Term" />
                <MenuItem value="SmartCombination_MT" primaryText="Medium Term" />
                <MenuItem value="SmartCombination_LT" primaryText="Long Term" />
                
                <Subheader>Rank</Subheader>
                <MenuItem value="SimpleRank" primaryText="Daily Ranking" />
                <MenuItem value="SmartRank" primaryText="Historical Ranking" />
            </SelectField>
        );
    }
}
