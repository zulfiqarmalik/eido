import { Strategy } from 'Client/Model/Config/ClStrategy';
import { LTStats } from 'Shared/Model/Stats/LTStats';
import { Positions } from 'Shared/Model/Stats/Positions';
import { Position } from 'Shared/Model/Stats/Position';
import { AlphaOrStrategy } from 'Client/Model/Config/ClConfigBase';
import { RouteComponentProps } from 'react-router-dom';

export interface GraphProps extends RouteComponentProps<any>, React.Props<any> {
    strategy            ?: AlphaOrStrategy;
    positionFiles       ?: string[];
    showTable           ?: boolean;
    duration            ?: string;
    positionFile        ?: string;
    startDate           ?: number;
    endDate             ?: number;
    positions           ?: Positions;
    position            ?: Position;
    stats               ?: LTStats;
}

export interface GraphState {
    isLoading           : boolean;
    loadingText         : string;
    statusMessage       ?: string;
    percent             ?: number;
    chartData           : any;
    chartOptions        : any;
    positions           ?: Positions;
}

