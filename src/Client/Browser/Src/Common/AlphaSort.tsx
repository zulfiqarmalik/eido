import React from 'react';

import MenuItem from 'material-ui/MenuItem';
import { AlphaSortOrders as gAlphaSortOrders } from 'Shared/Model/Config/ShAlpha';
import SelectField from 'material-ui/SelectField';

interface Props {
    onSortChange    : (sortBy: string) => void;
    sortBy          : string;
    label           ?: string;
    width           ?: number;
}

export default class AlphaSort extends React.Component<Props, {}> {
    sortLUT         : any = {};

    constructor(props: Props) {
        super(props);
    }

    onSortChange = (event: any, index: number, sortBy: string) => {
        if (this.props.onSortChange) {
            this.props.onSortChange(sortBy);
        }
    }

    render() {
        return (
            <SelectField 
                floatingLabelText={this.props.label ? this.props.label : undefined}
                value={this.props.sortBy} 
                onChange={this.onSortChange} 
                style={{width: this.props.width || 200}}
            >
                {gAlphaSortOrders.items.map((order, i) => {
                    return <MenuItem key={order.name} value={order.name} primaryText={order.text} />
                })}
            </SelectField>
        );
    }
}
