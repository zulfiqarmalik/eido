// import React from 'react';
// import ReactDOM from 'react-dom';
// import Loader from 'react-loader-advanced';
// import { eido } from "Shared/Core/ShCore";
// import { Debug, ShUtil } from "Shared/Core/ShUtil";
// import { withRouter, Link } from 'react-router';
// import PnlGraph from './PnlGraph';
// import StatsTable_Cum from './StatsTable_Cum';
// import DurationSelect from './DurationSelect';

// // import DateTimeDisplay from '../../Common/DateTimeDisplay';

// import {
//     Row,
//     Col,
//     Grid,
//     Form,
//     Panel,
//     PanelHeader,
//     Table,
//     Alert,
//     Button,
//     Checkbox,
//     PanelBody,
//     FormGroup,
//     FormControl,
//     ControlLabel,
//     PanelContainer,
//     Tabs,
//     Tab,
//     Nav,
//     NavItem,
//     PanelTabContainer,
//     Modal
// } from '@sketchpixy/rubix';

// import PropTypes from 'prop-types';
// import { Strategy } from 'Client/Model/Config/ClStrategy';

// // export class ViewAlpha extends React.Component {
// //     static propTypes = {
// //         stats: PropTypes.object,
// //         uuid: PropTypes.string,
// //         duration: PropTypes.string
// //     };

// //     constructor(props) {
// //         super(...arguments);
// //     }

// //     render() {
// //         return (
// //             <Grid>
// //                 <Row>
// //                     <PnlGraph 
// //                         key={this.props.stats ? this.props.stats._id + this.props.duration : "PnlGraph"} 
// //                         hideDuration={true} 
// //                         duration={this.props.duration} 
// //                         stats={this.props.stats}
// //                     />
// //                 </Row>
// //                 <Row>
// //                     <StatsTable_Cum key={this.props.stats ? this.props.stats._id : "StatsTable_Cum"} stats={this.props.stats} />
// //                 </Row>
// //             </Grid>
// //         );
// //     }
// // }

// export default class ViewAlphaModal extends React.Component {
//     static propTypes = {
//         uuid: PropTypes.string
//     };

//     constructor(props) {
//         super(...arguments);

//         this.state = {
//             showModal: false,
//             isLoading: false,
//             duration: "LT",
//             stats: null,
//             uuid: this.props.uuid,
//             index: this.props.index,
//             alphas: [],
//             canGoNext: false,
//             canGoPrev: false
//         };
//     }

//     close() {
//         this.setState({ showModal: false });
//     }

//     open(index) {
//         if (typeof index === 'undefined')
//             index = this.props.index;

//         this.updateView(index);
//     }
    
//     onAlphaDurationChange(e) {
//         this.setState({
//             duration: e.target.value
//         });
//     }

//     componentDidMount() {
//         if (!this.props.strategy)
//             return;

//         this.props.strategy.getConfig()
//             .then((config) => {
//                 if (!config)
//                     return;

//                 let alphas = config.getAllAlphas();
//                 let index = this.props.index;

//                 this.setState({
//                     alphas: alphas,
//                     uuid: alphas[index].map.uuid,
//                     canGoNext: (index < alphas.length - 1),
//                     canGoPrev: (index > 0)
//                 });
//             });
//     }

//     updateView(index, uuid) {
//         if (!this.props.strategy)
//             return;

//         if (!uuid)
//             uuid = this.state.alphas[index].map.uuid;

//         console.log("[ViewAlpha] Getting alpha stats: %s", uuid);

//         this.props.strategy.getAlphaStats(uuid)
//             .then((stats) => {
//                 console.log("[ViewAlpha] Got alpha stats: %s", uuid);

//                 this.setState({
//                     index: index,
//                     stats: stats,
//                     uuid: uuid,
//                     showModal: true,
//                     canGoNext: (index < this.state.alphas.length - 1),
//                     canGoPrev: (index > 0)
//                 });
//             });
//     }

//     onNext() {
//         let index = this.state.index + 1;
//         if (index >= this.state.alphas.length) 
//             return;

//         this.updateView(index, this.state.alphas[index].map.uuid);
//     }

//     onPrev() {
//         let index = this.state.index - 1;
//         if (index < 0)
//             return;

//         let uuid = this.state.alphas[index].map.uuid;
        
//         this.updateView(index, this.state.alphas[index].map.uuid);
//     }

//     render() {
//         return (
//             <Modal show={this.state.showModal} onHide={::this.close} lg>
//                 <Modal.Header closeButton>
//                     <Modal.Title><b>[{this.state.index + 1} / {this.state.alphas.length}] - {this.state.uuid}</b></Modal.Title>
//                     <div className="modal-header-buttons">
//                         <Button className={!this.state.canGoPrev ? "hidden" : ""} onClick={::this.onPrev} bsStyle='warning'>Prev</Button>
//                         <Button className={!this.state.canGoNext ? "hidden" : ""} onClick={::this.onNext} bsStyle='info'>Next</Button>
//                         {/*<Button bsStyle='danger'>DELETE</Button>*/}
//                     </div>

//                     <div>
//                         <Form inline className="pnl-graph-header">
//                             <DurationSelect 
//                                 onDurationChange={::this.onAlphaDurationChange} 
//                                 showPnlSpace={false}
//                                 stats={this.state.stats} 
//                             />
//                         </Form>
//                     </div>
//                 </Modal.Header>

//                 <Modal.Body>
//                     {/*<ViewAlpha 
//                         key={this.state.uuid + this.state.duration} 
//                         uuid={this.state.uuid} 
//                         stats={this.state.stats}
//                         duration={this.state.duration}
//                         index={this.state.index} 
//                         alpha={this.state.alphas[this.state.index]} 
//                     />*/}

//                     <Grid>
//                         <Row>
//                             <PnlGraph
//                                 key={this.state.uuid + this.state.duration}
//                                 hideDuration={true}
//                                 hidePnlSpace={true}
//                                 duration={this.state.duration}
//                                 stats={this.state.stats}
//                             />
//                         </Row>
//                         <Row>
//                             <StatsTable_Cum 
//                                 key={this.state.uuid + this.state.duration} 
//                                 stats={this.state.stats} 
//                             />
//                         </Row>
//                     </Grid>

//                 </Modal.Body>
//             </Modal>
//         );
//     }
// }

