import React from 'react';
import ReactDOM from 'react-dom';
import { eido } from "Shared/Core/ShCore";
import { Debug, ShUtil } from "Shared/Core/ShUtil";
import * as Colors from 'material-ui/styles/colors';

let Line = require('react-chartjs-2').Line;
require("chartjs-plugin-annotation");

import PropTypes from 'prop-types';
import { Strategy } from 'Client/Model/Config/ClStrategy';
import { LTStats } from 'Shared/Model/Stats/LTStats';
import { PnlSpace } from 'Shared/Model/Stats/CumStats';

interface Props {
    key                 ?: string;
    stats               : LTStats;
    statsLabel          ?: string;
    rawStats            ?: LTStats;
    rawStatsLabel       ?: string;
    preoptStats         ?: LTStats;
    preoptStatsLabel    ?: string;
    space               ?: PnlSpace;
    duration            ?: string;
    numPoints           ?: number;
    disableAnimation    ?: boolean;
    mainColor           ?: string;
    rawColor            ?: string;
    preoptColor         ?: string;
    hidePoints          ?: boolean;
    lineWidth           ?: number;
    showRaw             ?: boolean;
    showPreOpt          ?: boolean;
    useIndividualPlotIndexes?: boolean;
    startDate           ?: number;
    endDate             ?: number;
    hidePnlSpace        ?: boolean;
}

interface State {
    chartData           ?: any;
    chartOptions        ?: any;
    showPnlSpace        ?: boolean;
    mainColor           ?: string;
    rawColor            ?: string;
    preoptColor         ?: string;
    startDate           ?: number;
    endDate             ?: number;
    lineWidth           ?: number;
    statsLabel          ?: string;
    rawStatsLabel       ?: string;
    preoptStatsLabel    ?: string;
    hidePnlSpace        ?: boolean;
}

export default class PnlGraph extends React.Component<Props, State> {
    renderFinal        : boolean = true;
    renderRaw          : boolean = false;
    renderPreOpt       : boolean = false;
    additionalGraph    : string = "NONE";
    space              : string = this.props.space || PnlSpace.returns;


    constructor(props: Props) {
        super(props);

        this.state = {
            chartData:          {},
            chartOptions:       this.getChartOptions(),
            showPnlSpace:       this.props.hidePnlSpace ? false : true,
            mainColor:          this.props.mainColor || Colors.blue400,
            rawColor:           this.props.rawColor || Colors.red400,
            preoptColor:        this.props.preoptColor || Colors.yellow400,
            startDate:          this.getStartDate(),
            endDate:            this.getEndDate(),
            lineWidth:          this.props.lineWidth ? this.props.lineWidth : 2,
            statsLabel:         this.props.statsLabel ? this.props.statsLabel : "",
            rawStatsLabel:      this.props.rawStatsLabel ? this.props.rawStatsLabel : "Raw",
            preoptStatsLabel:   this.props.preoptStatsLabel ? this.props.preoptStatsLabel : "Pre-Opt",
        };
    }

    getStartDate(): number {
        if (!this.props.stats)
            return 0;

        if (this.props.duration == "CD" && this.props.startDate)
            return this.props.startDate;
        return this.props.stats.getStartDate();
    }

    getEndDate() {
        if (!this.props.stats)
            return 0;

        if (this.props.duration == "CD" && this.props.endDate)
            return this.props.endDate;
        return this.props.stats.getEndDate();
    }

    getChartOptions() {
        let self = this;

        let options: any = {
            bezierCurve: false,
            responsive: true,
            // maintainAspectRatio: false,
            scales: {
                yAxes: [{
                    scaleLabel: {
                        display: true,
                        labelString: this.space != PnlSpace.returns ? 'P&L' : "% Returns",
                        fontSize: 40,
                        fontColor: 'white'
                    },
                    ticks: {
                        fontColor: 'rgba(255, 255, 255, 0.7)',
                        // Create scientific notation labels
                        callback: function (value, index, values) {
                            return self.space != PnlSpace.returns ? value.toExponential(2) : value.toFixed(2) + "%";
                        }
                    },
                    gridLines: {
                        zeroLineColor: 'white',
                        color: 'rgba(255, 255, 255, 0.1)'
                    },
                }],
                xAxes: [{
                    ticks: {
                        fontColor: 'rgba(255, 255, 255, 0.7)',
                        fontSize: 10,
                    },
                    gridLines: {
                        zeroLineColor: 'white',
                        color: 'rgba(255, 255, 255, 0.1)'
                    }
                }],
            },
        }

        if (this.props.disableAnimation)
            options.animation = { duration: 0 };

        return options;
}

    addChartDataset(name, stats, colour, plotIndexes, plotData, hidden) {
        if (!plotData) {
            plotData = stats.getPnlPlotDataForRange(plotIndexes.start, plotIndexes.end, "", this.space == "RET" ? true : false, this.getEndPlotIndex(plotIndexes));
        }

        console.log(`[PnlGraph] Adding ${stats._id} - Num Points: ${plotData.pnls.length}`);

        // let pointBackgroundColors = [];
        // let colours = ["red", "green", "blue", "pink", "fuchisa"];
        // let cid = 0;
        // for (let i = 0; i < plotData.pnls.length; i++) {
        //     let colour = colours[cid];
        //     cid += 1;
        //     if (cid >= colours.length)
        //         cid = 0;
        //     pointBackgroundColors.push(colour);
        // }

        return {
            label: name || stats._id,
            borderWidth: this.state.lineWidth,
            lineTension: 0,
            hidden: hidden, 
            pointRadius: !this.props.hidePoints && plotData.pnls.length < 100 ? 3 : 0,
            backgroundColor: 'transparent',
            borderColor: colour,
            steppedLine: false,
            data: plotData.pnls,
            // pointBackgroundColor: pointBackgroundColors
        };
    }

    getDuration() {
        return this.props.duration;
    }

    getEndPlotIndex(plotIndexes) {
        let end = plotIndexes.end;
        if (this.props.numPoints && this.props.numPoints < plotIndexes.end)
            end = this.props.numPoints;
        return end;
    }

    updatePnlGraph() {
        Debug.assert(this.props.stats, "Stats not intialised properly!");

        let stats = this.props.stats;
        let rawStats = this.props.rawStats;
        let preoptStats = this.props.preoptStats;

        /// Normally the three graphs have the same date range so we just get indexes from the main graph and use the same
        /// for the RAW and PRE-OPT stats as well. However, the problem is when we're running a live publish session
        /// We may encounter different date ranges, while the strategy is running. In this case we use the 
        /// useIndividualPlotIndexes prop. This helps use make sure graphs come out correctly
        let plotIndexes = stats.getPlotIndexes(this.getDuration(), 0, "", this.state.startDate, this.state.endDate);
        let plotData = stats.getPnlPlotDataForRange(plotIndexes.istart, plotIndexes.iend, "", this.props.space == "RET" ? true : false, this.getEndPlotIndex(plotIndexes));

        let chartData = {
            labels: plotData.dates,
            xAxisID: "Dates",
            yAxisID: "Cum-PnL",
            datasets: []
        };

        if (this.renderFinal) {
            chartData.datasets.push(this.addChartDataset(`P&L`, stats, this.state.mainColor, plotIndexes, plotData, false));
        }

        if (rawStats) {
            if (this.props.useIndividualPlotIndexes) { 
                plotIndexes = rawStats.getPlotIndexes(this.getDuration(), 0, "", this.state.startDate, this.state.endDate);
                plotData = rawStats.getPnlPlotDataForRange(plotIndexes.istart, plotIndexes.iend, "", this.props.space == "RET" ? true : false, this.getEndPlotIndex(plotIndexes));
            }
            else
                plotData = null;

            chartData.datasets.push(this.addChartDataset(`Pre-COST`, rawStats, this.state.rawColor, plotIndexes, plotData, !this.props.showRaw));
        }

        if (preoptStats) {
            if (this.props.useIndividualPlotIndexes) { 
                plotIndexes = preoptStats.getPlotIndexes(this.getDuration(), 0, "", this.state.startDate, this.state.endDate);
                plotData = preoptStats.getPnlPlotDataForRange(plotIndexes.istart, plotIndexes.iend, "", this.props.space == "RET" ? true : false, this.getEndPlotIndex(plotIndexes));
            }
            else
                plotData = null;

            chartData.datasets.push(this.addChartDataset(`Pre-OPTIMISATION`, preoptStats, this.state.preoptColor, plotIndexes, plotData, !this.props.showPreOpt)); 
        }

        let chartOptions = this.getChartOptions();
        chartOptions["annotation"] = {
            annotations: [{
                type: "line",
                mode: "vertical",
                scaleID: "x-axis-0",
                value: stats.osDate,
                borderColor: Colors.pink500,
                label: {
                    content: "OS Start",
                    enabled: true,
                    position: "bottom"
                }
            }]
        };

        this.setState({
            chartData: chartData,
            chartOptions: chartOptions
        });
    }

    componentDidMount() {
        if (!this.props.stats)
            return;

        this.updatePnlGraph();
    }

    onFinalToggle(e) {
        this.renderFinal = !this.renderFinal;
        this.updatePnlGraph();
    }

    onRawToggle(e) {
        this.renderRaw = !this.renderRaw;
        this.updatePnlGraph();
    }

    onPreOptToggle(e) {
        this.renderPreOpt = !this.renderPreOpt;
        this.updatePnlGraph();
    }

    render() {
        return (
            <Line data={this.state.chartData} options={this.state.chartOptions} />
        );
    }
}

// export default createContainer(() => {
//     // let shell = Meteor.npmRequire('shelljs');
//     console.log("Getting strats in path: ${path}");
//     // let strats = shell.ls(path);

//     return {
//         strats: ["Test1", "Test2"],
//         name: "US"
//     };
// }, PnlGraphs);
