import React from 'react';
import * as Colors from 'material-ui/styles/colors';
import { Positions } from 'Shared/Model/Stats/Positions';
import { GraphProps, GraphState } from './GraphCommon';
import PosTable from './PosTable';

let Line = require('react-chartjs-2').Line;

export default class PosGraph extends React.Component<GraphProps, GraphState> {
    taskProgress        : any = null;

    constructor(props: GraphProps) {
        super(props);

        this.state = {
            isLoading: false,
            loadingText: "Loading data. Please wait ...",
            statusMessage: "",
            percent: 0.0,
            chartData: {},
            positions: null,
            chartOptions: {
                scales: {
                    yAxes: [{
                        scaleLabel: {
                            display: true,
                            fontSize: 40,
                            fontColor: 'white',
                            labelString: 'Notional'
                        },
                        ticks: {
                            fontColor: 'rgba(255, 255, 255, 0.7)',
                            // Create scientific notation labels
                            callback: function (value, index, values) {
                                return value.toExponential(2);
                            }
                        },
                        gridLines: {
                            zeroLineColor: 'white',
                            color: 'rgba(255, 255, 255, 0.1)'
                        },
                    }],
                    xAxes: [{
                        display: false
                    }],
                }
            }
        };
    }

    updatePosGraph() {
        if (!this.props.strategy)
            return;

        console.log("[PosGraph] Updating graph!");
        this.setState({ 
            isLoading: true
        });

        if (this.taskProgress)
            this.taskProgress.taskUpdate(0, "Loading ...");

        let promise =       this.props.duration === "SD" && this.props.positionFile ? 
                            this.props.strategy.getPositions(this.props.positionFile) : 
                            this.props.strategy.getPositionsDuration(this.props.duration, this.props.startDate, this.props.endDate, (percent, statusMessage) => {
                                if (this.taskProgress) 
                                    this.taskProgress.taskUpdate(percent, statusMessage);
                            });

        promise
            .then((pos: Positions) => {
                if (!pos) {
                    this.setState({
                        loadingText: "Unable to load position data!"
                    });
                    return;
                }

                if (this.taskProgress)
                    this.taskProgress.taskFinished("Finalising results ...");

                let sortedPositions = pos.sortPositions();
                let tickers = [];
                let notionals = [];
                let pointBackgroundColor = [];
                let borderColor = [];
                let transitionValue = 0;
                let transitionTicker = "";

                for (let i = 0; i < sortedPositions.length; i++) {
                    let color = sortedPositions[i].notional < 0.0 ? Colors.red700 : Colors.green700;

                    if (Math.abs(sortedPositions[i].notional) > 0.001) {
                        tickers.push(sortedPositions[i].bbTicker);
                        notionals.push(Math.round(Math.abs(sortedPositions[i].notional)));
                        pointBackgroundColor.push(color);
                        borderColor.push(color);

                        if (!transitionValue && i > 0 && sortedPositions[i].notional > 0.0 && sortedPositions[i - 1].notional <= 0.0) {
                            if (transitionValue == 0.0) {
                                transitionValue = sortedPositions[i].pnl.pnlClose; ///Math.round(Math.abs(sortedPositions[i].notional)); 
                                transitionTicker = sortedPositions[i].bbTicker;
                            }
                        }
                    }
                }

                let chartOptions = this.state.chartOptions;
                if (transitionValue) {
                    chartOptions["annotation"] = {
                        annotations: [{
                            type: "line",
                            mode: "vertical",
                            scaleID: "x-axis-0",
                            value: transitionValue,
                            borderColor: "red",
                            label: {
                                content: "<<< Short | Long >>>",
                                enabled: true,
                                position: "bottom"
                            }
                        }]
                    };
                }
                else {
                    delete chartOptions["annotation"];
                }
                
                let chartData = {
                    labels: tickers,
                    xAxisID: "Dates",
                    yAxisID: "Cum-PnL",
                    datasets: [{
                        label: "Positions",
                        borderWidth: 1,
                        lineTension: 0,
                        pointRadius: 3, //plotData.pnls.length < 100 ? 5 : 0,
                        backgroundColor: 'transparent',
                        borderColor: borderColor,
                        steppedLine: false,
                        pointBackgroundColor: pointBackgroundColor,
                        data: notionals
                    }]
                };

                this.setState({
                    isLoading: false,
                    positions: pos,
                    chartData: chartData,
                    chartOptions: chartOptions
                });
            });
    }

    componentDidMount() {
        this.updatePosGraph();
    }

    render() {
        return (
            <div>
                <Line data={ this.state.chartData } options={ this.state.chartOptions } />
                <PosTable 
                    location={this.props.location}
                    history={this.props.history}
                    match={this.props.match}
                    key={this.props.duration + this.props.positionFile} 
                    strategy={this.props.strategy}
                    positions={this.state.positions} 
                />
            </div>
        );
    }
}
