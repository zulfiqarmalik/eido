import React from 'react';
import { RouteComponentProps } from 'react-router';

import { Toolbar, ToolbarTitle } from 'material-ui/Toolbar';
import { SysViewType, SysViewDesc } from './SysDefs';
import { AppPages } from '../AppState';
let SideNav = require('react-sidenav').default;
let { Nav, NavIcon, NavText } = require('react-sidenav');
let lineChart = require('react-icons-kit/fa/lineChart').lineChart;
let database = require('react-icons-kit/fa/database').database;
let asterisk = require('react-icons-kit/fa/asterisk').asterisk;
let gripVertical = require('react-icons-kit/fa/th').th;

let SvgIcon = require('react-icons-kit').default;
const Icon20 = props => <SvgIcon size={props.size || 20} icon={props.icon} />;
// const tooltip = (
//     <Tooltip id='tooltip'><strong>Holy guacamole!</strong> Check this info.</Tooltip>
// );

// function TT(msg, title) {
//     if (title)
//         return <Tooltip ><strong>{title}</strong>{msg}</Tooltip>
//     return <Tooltip >{msg}</Tooltip>
// }

const buttonStyles = {
    height: 52,
    width: 32,
    minWidth: 0,
    padding: 0,
    textAlign: "center" as any
};

interface Props extends RouteComponentProps<any>, React.Props<any> {
    view: SysViewType;
    market: string;
}

interface State {
}

export default class SysSideBar extends React.Component<Props, State> {
    constructor(props: Props) {
        super(props);

        this.state = {
        };
    }

    componentDidMount() {
    }

    componentDidUpdate() {
    }

    // onSearch = (event: any, value: string) => {
    //     if (!value) {
    //         /// This will clear out the search string in alphaInfo ...

    //         this.setState({
    //             alphaInfo: new AlphaInfo(this.props.alphaInfo.market, this.state.category.objects)
    //         });

    //         return;
    //     }

    //     let searchAlphas = this.props.alphaInfo.search(value);
    //     let alphaInfo = new AlphaInfo(this.props.alphaInfo.market, searchAlphas);

    //     alphaInfo.searchString = value;

    //     this.setState({
    //         alphaInfo: alphaInfo
    //     });
    // }

    getSelected() {
        return this.props.view.toString();
    }

    renderItem(view: SysViewType, icon: any) {
        return (
            <Nav key={view.toString()} id={view.toString()}>
                <NavIcon><Icon20 icon={icon} /></NavIcon>
                <NavText>
                    <a
                        className="menu-link"
                        href={`#p=${AppPages.system}&market=${this.props.market}&view=${view}`}
                        style={{ color: "white" }}
                    >
                        {SysViewDesc.get(view)}
                    </a>
                </NavText>
            </Nav>
        );
    }

    render() {
        return (
            <div className='full-side-bar'>
                <Toolbar>
                    <ToolbarTitle text="System" className="dev-alpha-title" />
                </Toolbar>

                {/* <TextField
                    hintText="Search ..."
                    style={{ width: 270, marginLeft: 20 }}
                    value={this.state.alphaInfo.searchString}
                    onChange={this.onSearch}
                /> */}

                <div>
                    <SideNav
                        highlightBgColor="#0097A7"
                        hoverBgColor="#0097A7"
                        defaultSelected={this.getSelected()}
                        selected={this.getSelected()}
                        key="Sys-SideBar"
                    >
                        {this.renderItem(SysViewType.jobQueue, asterisk)}
                        {this.renderItem(SysViewType.prices, lineChart)}
                        {this.renderItem(SysViewType.universe, gripVertical)}
                        {/* {this.renderItem(SysViewType.securityMaster, asterisk)} */}
                        {this.renderItem(SysViewType.corporateActions, database)}
                    </SideNav>
                </div>
            </div>
        );
    }
}
