import React from 'react';
import { RouteComponentProps } from 'react-router';

import { SysViewType, SysViewDesc } from './SysDefs';
import {
    Table,
    TableBody,
    TableHeader,
    TableHeaderColumn,
    TableRow,
    TableRowColumn,
} from 'material-ui/Table';
import DataViewer from './DataViewer';
import Toolbar, { ToolbarGroup } from 'material-ui/Toolbar';
import TextField from 'material-ui/TextField';
  

interface Props {
    market          : string;
    secUuids        : string[];
    universeId      : string;
    startDate       : string;
    endDate         : string;
}

interface State {
    datasets        : string[];
}

export default class PricesViewer extends React.Component<Props, State> {
    constructor(props: Props) {
        super(props);

        this.state = {
            // datasets : ["baseData.adjClosePrice", "baseData.adjVolume", "baseData.adjMCap", "baseData.returns"]
            datasets : ["baseData.adjClosePrice"]
        };
    }

    componentDidMount() {
    }

    componentDidUpdate() {
    }

    // renderToolbar() {
    //     return (
    //         <div className="results">
    //             <Toolbar>
    //                 <ToolbarGroup>
    //                     {this.state.datasets.map((ds: string, index: number) => {
    //                         return (
    //                             <TextField
    //                                 floatingLabelText={`Data-${index}`}
    //                                 floatingLabelFixed={true}
    //                                 value={this.state.datasets[index]}
    //                                 onChange={(event: any, value: string) => this.setState}
    //                                 style={{ width: 100 }}
    //                                 disabled={false}
    //                             />
    //                         )
    //                     })}
    //                 </ToolbarGroup>
    //             </Toolbar>

    //         </div>
    //     );
    // }

    renderDataViewer() {
        return (
            <DataViewer
                market={this.props.market}
                secUuids={this.props.secUuids}
                key={`${this.props.secUuids.join(',')}`}
                universeId={this.props.universeId}
                startDate={this.props.startDate}
                endDate={this.props.endDate}
                showSecInfo={true}
                heading="Daily Data"
                datasets={this.state.datasets}
                datasetLabels={this.state.datasets}
                formatting={['d', 'd', 'd', 'd']}
            />
        );
    }

    render() {
        return (
            <div>
                {this.renderDataViewer()}
            </div>
        );
    }
}
