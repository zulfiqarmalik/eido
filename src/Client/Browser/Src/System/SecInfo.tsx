import React from 'react';
import { RouteComponentProps } from 'react-router';

import { SysViewType, SysViewDesc } from './SysDefs';
import {
    Table,
    TableBody,
    TableHeader,
    TableHeaderColumn,
    TableRow,
    TableRowColumn,
} from 'material-ui/Table';
import { Sec, SecMaster } from 'Shared/Model/ShData';
import { eido } from 'Shared/Core/ShCore';
  

interface Props {
    market          : string;
    secUuids        : string[];
    universeId      : string;
}

interface State {
    securities      : Sec[]
}

export default class SecInfo extends React.Component<Props, State> {
    constructor(props: Props) {
        super(props);

        this.state = {
            securities: []
        };
    }

    componentDidMount() {
        if ((!this.state.securities || !this.state.securities.length) && this.props.secUuids.length) {
            return eido.dataCache.getSecMaster(this.props.market).then((secMaster: SecMaster) => {
                /// Now get the securities
                let securities: Sec[] = new Array<Sec>(this.props.secUuids.length);

                this.props.secUuids.map((secUuid, ii) => {
                    let security: Sec = secMaster.find(secUuid);
                    securities[ii] = security;
                })

                this.setState({ 
                    securities: securities
                })
            });
        }
    }

    componentDidUpdate() {
    }

    renderRow(security: Sec, index: number) {
        if (!security) {
            return (<div/>);
        }

        return (
            <TableRow key={`SecInfo-${security.uuid}`}>
                <TableRowColumn key={`Ticker-${security.uuid}`}>{security.ticker}</TableRowColumn>
                <TableRowColumn key={`UUID-${security.uuid}`}>{security.uuid}</TableRowColumn>
                <TableRowColumn key={`FIGI-${security.uuid}`}>{security.figi}</TableRowColumn>
                <TableRowColumn key={`ISIN-${security.uuid}`}>{security.isin}</TableRowColumn>
                <TableRowColumn key={`PermId-${security.uuid}`}>{security.permId}</TableRowColumn>
                <TableRowColumn key={`PerfId-${security.uuid}`}>{security.perfId}</TableRowColumn>
                <TableRowColumn key={`Name-${security.uuid}`}>{security.name}</TableRowColumn>
            </TableRow>
        );
    }

    render() {
        return (
            <Table
                selectable={false}
                fixedFooter={false}
            >
                <TableHeader displaySelectAll={false} adjustForCheckbox={false}>
                    <TableRow>
                        <TableHeaderColumn key="Ticker"><b>Ticker</b></TableHeaderColumn>
                        <TableHeaderColumn key="UUID"><b>UUID</b></TableHeaderColumn>
                        <TableHeaderColumn key="FIGI"><b>FIGI</b></TableHeaderColumn>
                        <TableHeaderColumn key="ISIN"><b>ISIN</b></TableHeaderColumn>
                        <TableHeaderColumn key="PermId"><b>PermId</b></TableHeaderColumn>
                        <TableHeaderColumn key="PerfId"><b>PerfId</b></TableHeaderColumn>
                        <TableHeaderColumn key="Name"><b>Name</b></TableHeaderColumn>
                    </TableRow>
                </TableHeader>
                <TableBody displayRowCheckbox={false}>
                    {
                        this.state.securities.map((security: Sec, ii) => {
                            return this.renderRow(security, ii)
                        })
                    }
                </TableBody>
            </Table>
        );
    }
}
