import React from 'react';
import DataViewer from './DataViewer';
// import Toolbar, { ToolbarGroup, ToolbarTitle } from 'material-ui/Toolbar';
  

interface Props {
    market          : string;
    universeId      : string;
}

interface State {
    // datasets        : string[];
    columns         : any[];
    rows            : any[]
}

import Paper from '@material-ui/core/Paper'; import {
    SearchState,
    IntegratedFiltering,
} from '@devexpress/dx-react-grid';
import {
    Grid,
    Table,
    Toolbar,
    SearchPanel,
    TableHeaderRow,
} from '@devexpress/dx-react-grid-material-ui';

import {
    SortingState,
    IntegratedSorting,
  } from '@devexpress/dx-react-grid';

export default class JobQueueViewer extends React.Component<Props, State> {
    constructor(props: Props) {
        super(props);

        this.state = {
            columns: [
                { name: 'name', title: 'Name' },
                { name: 'sex', title: 'Sex' },
                { name: 'city', title: 'City' },
                { name: 'car', title: 'Car' },
            ],
            rows: [{
                name: "Zulfi", sex: "male", city: "London", car: "BMW"
            }, {
                name: "Ammara", sex: "female", city: "Reigate", car: "Audi"
            }]
        };
    }

    componentDidMount() {
    }

    componentDidUpdate() {
    }

    // renderTitle() {
    //     return (
    //         <ToolbarGroup>
    //             <ToolbarTitle text="Job Queue" className="dev-alpha-title" />
    //         </ToolbarGroup>
    //     );
    // }
    
    // renderToolbar() {
    //     return (
    //         <div className="results">
    //             <Toolbar>
    //                 {this.renderTitle()}
    //             </Toolbar>

    //         </div>
    //     );
    // }

    render() {
        const { rows, columns } = this.state;

        return (
            <Paper 
                style={{height: "100%"}}
            >
                <Grid
                    rows={rows}
                    columns={columns}
                >
                    <SearchState defaultValue="" />
                    <IntegratedFiltering />
                    <SortingState
                        defaultSorting={[{ columnName: 'city', direction: 'asc' }]}
                    />
                    <IntegratedSorting />
                    <Table />
                    <TableHeaderRow showSortingControls />
                    <Toolbar />
                    <SearchPanel />
                </Grid>
            </Paper>
        );
    }
}
{/* <div>
    {this.renderToolbar()}
 </div> */}
