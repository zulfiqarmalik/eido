import React from 'react';
import { RouteComponentProps } from 'react-router';

import RGL, { WidthProvider } from "react-grid-layout";
import SysSideBar from './SysSideBar';
import { SysViewType, SysViewDesc } from './SysDefs';
const ReactGridLayout = WidthProvider(RGL);
import { DefMarket as gDefMarket, getDefUniverse, AssetClass, SecMasterId } from 'Shared/Model/Config/ShAlpha';
import { AppPages } from '../AppState';
import * as qs from 'query-string';
import {Toolbar, ToolbarGroup, ToolbarSeparator, ToolbarTitle} from 'material-ui/Toolbar';
import UniverseSelect from '../Common/UniverseSelect';
import TextField from 'material-ui/TextField';
import RaisedButton from 'material-ui/RaisedButton';
let RunIcon = require('material-ui-icons/Refresh').default;
import * as Colors from 'material-ui/styles/colors';
import { eido } from 'Shared/Core/ShCore';
import { SecMaster, Sec } from 'Shared/Model/ShData';
import AutoComplete from 'material-ui/AutoComplete';
import IconButton from 'material-ui/IconButton';
import FontIcon from 'material-ui/FontIcon';
import PricesViewer from './PricesViewer';
import { ShUtil } from 'Shared/Core/ShUtil';
import { User } from '../../../Model/User/ClUser';
import JobQueueViewer from './JobQueueViewer';

export interface Props extends RouteComponentProps<any>, React.Props<any> {
}

export interface State {
    children            : any[];
    view                : SysViewType;
    market              : string;
    universeId          : string;
    secUuid             : string;
    secName             : string;
    startDate           : string;
    endDate             : string;
    secMaster           : SecMaster;
    searchStrings       : string[];
    keySuffix           : string;
}    

const btStyle = {
    marginRight: 10,
    marginLeft: 10
};

export default class System extends React.Component<Props, State> {
    view                : SysViewType = SysViewType.prices;
    market              : string = gDefMarket;
    secIdInput          : any = null;

    constructor(props: Props) {
        super(props);

        let today = new Date();

        this.state = {
            children        : [],
            view            : this.view,
            market          : this.market,
            universeId      : SecMasterId,
            secUuid         : "",
            secName         : "",
            startDate       : "20180101",
            endDate         : ShUtil.currentDateToString(""),
            secMaster       : null,
            searchStrings   : [],
            keySuffix       : "",
        };    
    }

    updateState() {
        this.setState({
            market: this.market,
            view: this.view
        });

        return true;
    }

    checkArgs() {
        const args = qs.parse(this.props.location.hash);

        if (args.p != AppPages.system)
            return false;

        if (!args || !args.view || (args.view == this.view && args.market == this.market))
            return false;

        this.market = args.market as string;
        this.view = args.view as SysViewType;

        return this.updateState();
    }

    componentDidMount() {
        if (this.secIdInput)
            this.secIdInput.focus();

        if (!this.state.secMaster && this.state.market) {
            eido.dataCache.getSecMaster(this.state.market).then((secMaster: SecMaster) => {
                this.setState({ 
                    secMaster: secMaster, 
                    searchStrings: secMaster.searchStrings() 
                });

                User.current.loadSettings(["sys.secName", "sys.startDate", "sys.endDate"], ["", "20180101", ShUtil.currentDateToString("")])
                    .then((values: string[]) => {
                        let secName: string = values[0];
                        let startDate: string = values[1];
                        let endDate: string = values[2];
                        let sec = secMaster.findSearchString(secName);

                        if (sec)
                            this.refresh(secName, sec.uuid, startDate, endDate);

                        this.setState({
                            secName: sec ? secName : "",
                            secUuid: sec ? sec.uuid : "",
                            startDate: startDate,
                            endDate: endDate
                        });
                    });
            });
        }

        return this.updateState();
    }

    componentDidUpdate() {
        this.checkArgs();
    }

    renderView() {
        return (
            <div/>
        );
    }

    onUniverseChange = (universeId: string) => {
        this.setState({
            universeId: universeId,
        });
    }

    onStartDateChanged = (event: any, value: string) => {
        this.setState({
            startDate: value
        });
    }
    
    onEndDateChanged = (event: any, value: string) => {
        this.setState({
            endDate: value
        });
    }

    refresh(secName: string, secUuid: string, startDate: string, endDate: string) {
        this.setState({
            keySuffix: `${secUuid}-${startDate}-${endDate}`
        });

        User.current.saveSetting("sys.startDate", startDate);
        User.current.saveSetting("sys.endDate", endDate);
        User.current.saveSetting("sys.secName", secName);
    }

    onRun = () => {
        this.refresh(this.state.secName, this.state.secUuid, this.state.startDate, this.state.endDate);
    }

    onInput = (searchText: string, ...args) => {
        let stateObj = {
            secName: searchText
        };

        if (args.length == 2 && args[1].source == "click") {
            let sec: Sec = this.state.secMaster.findSearchString(searchText);

            if (sec) {
                stateObj["secUuid"] = sec.uuid;
            }
        }

        this.setState(stateObj);
    }

    renderUniverses() {
        return (
            <ToolbarGroup>
                <UniverseSelect
                    market={this.state.market}
                    universeId={this.state.universeId}
                    onUniverseChange={this.onUniverseChange}
                    readOnly={false}
                    showSecMaster={true}
                    width={150}
                    label="Universe"
                />
            </ToolbarGroup>
        );
    }

    renderOptions() {
        return (
            <ToolbarGroup>
                {/* <TextField
                    hintText="Ticker/FIGI"
                    value={this.state.secUuid}
                    onChange={this.onSecIdChanged}
                    style={{ width: 150 }}
                    disabled={false}
                    ref={(input) => { this.secIdInput = input; }}
                /> */}

                <AutoComplete
                    floatingLabelText="Security"
                    floatingLabelFixed={true}
                    key={`SearchTicker-${this.state.searchStrings.length}`}
                    filter={AutoComplete.fuzzyFilter}
                    dataSource={this.state.searchStrings}
                    dataSourceConfig={{text: 'text', value: 'value'}}
                    maxSearchResults={10}
                    listStyle={{width: 450}}
                    onUpdateInput={this.onInput}
                    searchText={this.state.secName}
                    ref={(input) => this.secIdInput = input}
                />

                &emsp;

                <TextField
                    floatingLabelText="Start Date"
                    floatingLabelFixed={true}
                    value={this.state.startDate}
                    onChange={this.onStartDateChanged}
                    style={{ width: 100 }}
                    disabled={false}
                />

                &emsp;

                <TextField
                    floatingLabelText="End Date"
                    floatingLabelFixed={true}
                    value={this.state.endDate}
                    onChange={this.onEndDateChanged}
                    style={{ width: 100 }}
                    disabled={false}
                />

                &emsp;

                {/* <RaisedButton
                    secondary={true}
                    icon={<RunIcon color={Colors.deepOrange500} />}
                    style={btStyle}
                    onClick={this.onRun}
                /> */}
                <IconButton onClick={this.onRun}>
                    <FontIcon color={Colors.deepOrange500} className="material-icons">refresh</FontIcon>
                </IconButton>                    

            </ToolbarGroup>
        );
    }

    renderTitle() {
        return (
            <ToolbarGroup>
                <ToolbarTitle text={SysViewDesc.get(this.state.view)} className="dev-alpha-title" />
            </ToolbarGroup>
        );
    }
    
    renderToolbar() {
        if (this.state.view == SysViewType.jobQueue) {
            return <div/>
        }

        return (
            <div className="results">
                <Toolbar>
                    {this.renderUniverses()}
                    {this.renderTitle()}
                    {this.renderOptions()}
                </Toolbar>

            </div>
        );
    }

    renderPrices() {
        if (!this.state.keySuffix || !this.state.secUuid || !this.state.secName)
            return (<div/>);

        return (
            <PricesViewer
                market={this.state.market}
                secUuids={[this.state.secUuid]}
                universeId={this.state.universeId}
                startDate={this.state.startDate}
                endDate={this.state.endDate}
                key={`PricesViewer-${this.state.keySuffix}`}
            />
        )
    }

    renderJobQueue() {
        return (
            <JobQueueViewer
                market={this.state.market}
                universeId={this.state.universeId}
                key={`JobQueue-${this.state.keySuffix}`}
            />
        )
    }

    renderChild() {
        if (this.state.view == SysViewType.jobQueue) 
            return this.renderJobQueue();

        if (!this.state.keySuffix) // || !this.state.secUuid || !this.state.secName)
            return (<div/>);

        if (this.state.view == SysViewType.prices)
            return this.renderPrices();
    }
    
    renderMainView() {
        return (
            <div>
                {this.renderToolbar()}
                {this.renderChild()}
            </div>
        );
    }

    render() {
        return (
            <ReactGridLayout
                className="layout full-side-bar"
                style={{height: "100%"}}
                autoSize={false}
                margin={[2, 2]}
                verticalCompact={true}
                rowHeight={(window.innerHeight - 100.0) / 10.0}
            >
                <div 
                    key="1"
                    data-grid={{ x: 0, y: 0, w: 2, h: 10, static: true }}
                >
                    <SysSideBar
                        location={this.props.location}
                        match={this.props.match}
                        history={this.props.history}
                        key={`SysSideBar-Main-${this.state.view}`} 
                        view={this.state.view}
                        market={this.state.market}
                    /> 
                </div>
                <div 
                    key="2" 
                    data-grid={{ x: 2, y: 0, w: 10, h: 10, static: true }}
                    className="main-content" 
                >
                    <div className="full-side-bar">
                        {this.renderMainView()}
                    </div>
                </div>
            </ReactGridLayout>
        );
    }
}