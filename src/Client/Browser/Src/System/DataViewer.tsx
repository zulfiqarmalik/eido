import React from 'react';

import {
    Table,
    TableBody,
    TableHeader,
    TableHeaderColumn,
    TableRow,
    TableRowColumn,
} from 'material-ui/Table';
import SecInfo from './SecInfo';
import { eido } from 'Shared/Core/ShCore';
import { SecMaster } from 'Shared/Model/ShData';
import RefreshIndicator from 'material-ui/RefreshIndicator';
  
interface Props {
    market          : string;
    secUuids        : string[];
    universeId      : string;
    startDate       : string;
    endDate         : string;

    datasets        : string[];
    datasetLabels   : string[];
    formatting      : string[];

    showSecInfo     ?: boolean;
    heading         ?: string;
}

interface State {
    results         : any;
    loadingState    : string;
}

export default class DataViewer extends React.Component<Props, State> {
    constructor(props: Props) {
        super(props);

        this.state = {
            loadingState: "ready",
            results : {
                dates: [],
                data: {},
            }
        };
    }

    componentDidMount() {
        this.setLoading();

        eido.dataCache.getSecMaster(this.props.market)
            .then((secMaster: SecMaster) => {
                if (!this.state.results || !this.state.results.dates.length) {
                    return eido.dataCache.getFloatData(this.props.market, this.props.universeId, this.props.secUuids, this.props.datasets,
                        parseInt(this.props.startDate), parseInt(this.props.endDate));
                }
                return Promise.resolve(this.state.results);
            })
            .then((results: any) => {
                this.setState({
                    results: results
                });
            })
            .finally(() => {
                this.clearLoading();
            });
    }

    componentDidUpdate() {
    }

    renderSecInfo(secUuid: string, ii: number) {
        if (!this.props.showSecInfo)
            return (<div/>);

        return (
            <div key={`SecInfo-${secUuid}`}>
                <h2 className="para-heading">Security Info</h2>
                <SecInfo
                    market={this.props.market}
                    secUuids={[secUuid]}
                    universeId={this.props.universeId}
                /> 
                <br/>
                <br/>
            </div>
        )
    }

    format(value: any, dataIndex: number): any {
        let format: string = this.props.formatting[dataIndex];

        if (format[0] == 'd') {
            if (value > 1e6) 
                return value.toExponential(4);
            return value.toFixed(4);
        }
        if (format[0] == 'f')
            return value.toFixed(parseInt(format[1]));
        else if (format[0] == 'e')
            return value.toExponential(parseInt(format[1]));
        return value.toString();
    }

    renderRow(date: number, di: number, secUuid: string, ii: number) {
        let secData: any = this.state.results.data[secUuid];

        if (!secData)
            return (<div/>);

        return (
            <TableRow key={`Data-${date}-${secUuid}`}>
                <TableRowColumn key={`Date-${date}-${secUuid}`}>{date}</TableRowColumn>

                {this.props.datasets.map((dataId, dataIndex) => {
                    return (<TableRowColumn key={`${date}-${dataId}-${secUuid}`}>
                        {secData.values[dataId][di] ? this.format(secData.values[dataId][di], dataIndex) : "<NONE>"}
                    </TableRowColumn>)
                })}
            </TableRow>
        );
    }

    renderResults(secUuid: string, ii: number) {
        return(
            <TableBody displayRowCheckbox={false}>
                {this.state.results.dates.map((date, di) => {
                    return this.renderRow(date, di, secUuid, ii)
                })}
            </TableBody>
        );
    }

    renderDataTable(secUuid: string, ii: number) {
        return (
            <Table
                selectable={false}
                fixedFooter={false}
                key={`Table-${secUuid}-${this.state.results.dates.length}`}
            >
                <TableHeader displaySelectAll={false} adjustForCheckbox={false}>
                    <TableRow>
                        <TableHeaderColumn><b>Date</b></TableHeaderColumn>

                        {
                            this.props.datasetLabels.map((label) => {
                                return <TableHeaderColumn key={label}><b>{label}</b></TableHeaderColumn>
                            })
                        }
                    </TableRow>
                </TableHeader>

                {this.renderResults(secUuid, ii)}
            </Table>
        );
    }

    renderUuid(secUuid: string, ii: number) {
        return (
            <div key={`Div-${secUuid}-${this.state.results.dates.length}`}>
                <br/>
                {this.renderSecInfo(secUuid, ii)}
                <h2 className={this.props.heading ? "para-heading" : "hidden"}>{this.props.heading}</h2>
                {this.renderDataTable(secUuid, ii)}
            </div>
        );
    }

    renderLoadingIndicator() {
        return (
            <div className={this.state.loadingState == "loading" ? "" : "hidden"}>
                <RefreshIndicator
                    size={100}
                    left={0}
                    top={0}
                    status={this.state.loadingState as any}
                    loadingColor='cyan'
                    style={{marginLeft: '50%', marginTop: '25%', position: 'relative' as any, visibility: `${this.state.loadingState === 'loading' ? 'visible' : 'hidden'}` as any}}
                />            
            </div>
        );
    }

    setLoading = () => this.setState({ loadingState: "loading" })
    clearLoading = () => this.setState({ loadingState: "hide" })

    render() {
        return (
            <div>
                {this.renderLoadingIndicator()}

                <div className={this.state.loadingState != "loading" ? "" : "hidden"}>
                    {this.props.secUuids.map((secUuid, ii) => {
                        return this.renderUuid(secUuid, ii);
                    })}
                </div>
            </div>
        );
    }
}
