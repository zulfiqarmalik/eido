import React from 'react';

import {
    Table,
    TableBody,
    TableHeader,
    TableHeaderColumn,
    TableRow,
    TableRowColumn,
} from 'material-ui/Table';
import SecInfo from './SecInfo';
import { eido } from 'Shared/Core/ShCore';
import { SecMaster } from 'Shared/Model/ShData';
import { Cl } from 'Client/Core/ClCore';
  
interface Props {
    market          : string;
    universeId      : string;
    startDate       : string;
    endDate         : string;
}

interface State {
}

export default class UniverseViewer extends React.Component<Props, State> {
    constructor(props: Props) {
        super(props);

        this.state = {
        };
    }

    componentDidMount() {
    }

    componentDidUpdate() {
    }

    render() {
        return (
            <div>
            </div>
        );
    }
}
