export const enum SysViewType {
    jobQueue            = "jobs",
    prices              = "prices",
    universe            = "universe",
    corporateActions    = "cax",
    adj                 = "adj",
}

export class SysViewDesc {
    static get(view: SysViewType) : string {
        switch (view) {
        case SysViewType.jobQueue:
            return "Job Queue";
        case SysViewType.prices: 
            return "Daily Data";
        case SysViewType.universe:
            return "Universe";
        // case SysViewType.securityMaster:
        //     return "Security Master";
        case SysViewType.corporateActions:
            return "Corporate Actions";
        case SysViewType.adj:
            return "Adjustment Factors";
        }

        return "Unknown";
    }
}
