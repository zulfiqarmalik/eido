import React from 'react';
import { Debug } from "Shared/Core/ShUtil";
import * as Colors from 'material-ui/styles/colors';
import { Props, State } from './Analysis_Common';

let Doughnut = require('react-chartjs-2').Doughnut;

export default class LongShort_Pie extends React.Component<Props, State> {
    constructor(props: Props) {
        super(props);

        this.state = {
            chartData: {}
        };
    }

    static getPercentages(stats, istartOrg, iendOrg) {
        let { istart, iend } = stats.adjustPlotIndexes(istartOrg, iendOrg)

        let numPosConverge = 0;
        let numNegConverge = 0;
        let numDiverge = 0;
        let numNeutral = 0;
        
        for (let i = istart; i < iend; i++) {
            let point = stats.points[i];
            let longPnl = point.longPnl;
            let shortPnl = point.shortPnl;

            if (longPnl > 0 && shortPnl > 0)
                numPosConverge += 1;
            else if (longPnl < 0 && shortPnl < 0)
                numNegConverge += 1;
            else if ((longPnl < 0 && shortPnl > 0) || (longPnl > 0 && shortPnl < 0))
                numDiverge += 1;
            else
                numNeutral += 1;
        }

        let numPoints = iend - istart + 1;
        return {
            labels: ["Diverge", "+ve Converge", "-ve Converge"],
            percentages: [
                ((numDiverge / numPoints) * 100.0).toFixed(2),
                ((numPosConverge / numPoints) * 100.0).toFixed(2),
                ((numNegConverge / numPoints) * 100.0).toFixed(2),
                // ((numNeutral / numPoints) * 100.0).toFixed(2)
            ]
        };
    }

    updateLSPie() {
        Debug.assert(this.props.stats, "Stats not intialised properly!");

        let stats = this.props.stats;
        let plotIndexes = stats.getPlotIndexes(this.props.duration, 0, this.props.positionFile, this.props.startDate, this.props.endDate);
        let gdata = LongShort_Pie.getPercentages(stats, plotIndexes.istart, plotIndexes.iend);

        let chartData = {
            labels: gdata.labels,
            datasets: [{
                data: gdata.percentages,
                backgroundColor: [Colors.purple700, Colors.green700, Colors.red700]
            }]
        };

        this.setState({
            chartData: chartData
        });
    }

    componentDidMount() {
        if (!this.props.stats)
            return;

        console.log("[LSPie] Updating graph ...");
        this.updateLSPie();
    }

    render() {
        return (
            <Doughnut data={this.state.chartData} />
        );
    }
}

// export default createContainer(() => {
//     // let shell = Meteor.npmRequire('shelljs');
//     console.log("Getting strats in path: ${path}");
//     // let strats = shell.ls(path);

//     return {
//         strats: ["Test1", "Test2"],
//         name: "US"
//     };
// }, PnlGraphs);
