import React from 'react';
import * as Colors from 'material-ui/styles/colors';
import { Props, State } from './Analysis_Common';
import { Positions } from 'Shared/Model/Stats/Positions';

let Bar = require('react-chartjs-2').Bar;
require("chartjs-plugin-annotation");

export default class QuantileGraph extends React.Component<Props, State> {
    taskProgress            : any = null;

    constructor(props: Props) {
        super(props);

        this.state = {
            positions: null,
            chartData: {},
            chartOptions: {
                scales: {
                    yAxes: [{
                        scaleLabel: {
                            display: true,
                            labelString: 'Returns BPS',
                            fontSize: 40,
                            fontColor: 'white'
                        },
                        ticks: {
                            beginAtZero: true,
                            fontColor: 'rgba(255, 255, 255, 0.7)',
                            // Create scientific notation labels
                            callback: function (value, index, values) {
                                return value.toFixed(2);
                            }
                        },
                        gridLines: {
                            zeroLineColor: 'white',
                            color: 'rgba(255, 255, 255, 0.1)'
                        },
                    }],
                    xAxes: [{
                        stacked: false,
                        beginAtZero: true,
                        scaleLabel: {
                            display: true,
                            labelString: "Quantiles",
                            fontSize: 40,
                            fontColor: 'white'
                        },
                        ticks: {
                            fontColor: 'rgba(255, 255, 255, 0.7)',
                            stepSize: 1,
                            min: 0,
                            autoSkip: false
                        },
                        gridLines: {
                            zeroLineColor: 'white',
                            color: 'rgba(255, 255, 255, 0.1)'
                        }
                    }],
                }
            }
        };
    }

    updateGraph() {
        if (!this.props.strategy)
            return;

        if (this.taskProgress)
            this.taskProgress.taskUpdate(0, "Loading ...");

        this.setState({
            isLoading: true
        });

        let promise =       this.props.duration === "SD" && this.props.positionFile ?
                            this.props.strategy.getPositions(this.props.positionFile) : 
                            this.props.strategy.getPositionsDuration(this.props.duration, this.props.startDate, this.props.endDate, (percent, statusMessage) => {
                                if (this.taskProgress) 
                                    this.taskProgress.taskUpdate(percent, statusMessage);
                            });

        promise
            .then((pos: Positions) => {
                if (!pos.quantiles || !pos.quantiles.length) {
                    this.setState({
                        loadingText: "Unable to load position data!"
                    });
                    return;
                }

                let qnotional = new Array(pos.quantiles.length);
                let qpnlClose = new Array(pos.quantiles.length);
                let qreturns = new Array(pos.quantiles.length);
                // let qpnlHigh = new Array(pos.quantiles.length);
                // let qpnlLow = new Array(pos.quantiles.length);

                for (let qi = 0; qi < pos.quantiles.length; qi++) {
                    qnotional[qi]   = pos.quantiles[qi].notional;
                    qpnlClose[qi]   = Math.round(pos.quantiles[qi].pnlClose);
                    // qpnlHigh[qi]    = pos.quantiles[qi].pnlHigh;
                    // qpnlLow[qi]     = pos.quantiles[qi].pnlLow;
                    qreturns[qi]    = (pos.quantiles[qi].returns) * 1e4;
                }

                let chartData = {
                    labels: ["Quantile-1", "Quantile-2", "Quantile-3", "Quantile-4", "Quantile-5"],
                    xAxisID: "Quantiles",
                    yAxisID: "Notional/P&L",
                    datasets: [
                    // {
                    //     label: "Abs. Notional",
                    //     backgroundColor: 'orange',
                    //     fillColor: "orange",
                    //     data: qnotional
                    // }, 
                    {
                        label: "Returns",
                        borderColor: 'white',
                        borderWidth: 2,
                        backgroundColor: Colors.blue400,
                        data: qreturns
                    }, 
                    // {
                    //     label: "High P&L",
                    //     backgroundColor: 'green',
                    //     fillColor: "green",
                    //     data: qpnlHigh
                    // }, 
                    // {
                    //     label: "Low P&L",
                    //     backgroundColor: 'red',
                    //     fillColor: "Red",
                    //     data: qpnlLow
                    // }
                    ]
                };

                this.setState({
                    isLoading: false,
                    positions: pos,
                    chartData: chartData
                });
            });
    }

    componentDidMount() {
        this.updateGraph();
    }

    render() {
        return (
            <Bar data={ this.state.chartData } options={ this.state.chartOptions } />
        );
    }
}

// export default createContainer(() => {
//     // let shell = Meteor.npmRequire('shelljs');
//     console.log("Getting strats in path: ${path}");
//     // let strats = shell.ls(path);

//     return {
//         strats: ["Test1", "Test2"],
//         name: "US"
//     };
// }, PosGraphs);
