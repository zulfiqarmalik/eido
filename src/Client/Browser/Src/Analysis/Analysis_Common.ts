import { RouteComponentProps } from 'react-router';
import { LTStats } from 'Shared/Model/Stats/LTStats';
import { AlphaOrStrategy } from 'Client/Model/Config/ClConfigBase';

export interface State {
    positions           ?: any;
    maxDrift            ?: number;
    chartData           ?: any;
    driftChartData      ?: any;
    isLoading           ?: boolean;
    loadingText         ?: string;
    chartOptions        ?: any;
    driftChartOptions   ?: any;
}

export interface Props extends RouteComponentProps<any>, React.Props<any> {
    strategy            ?: AlphaOrStrategy;
    type                ?: string;
    duration            ?: string;
    stats               ?: LTStats;
    positionFile        ?: string;
    startDate           ?: number;
    endDate             ?: number;
}
