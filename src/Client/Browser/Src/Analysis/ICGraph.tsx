import React from 'react';
import { Props, State } from './Analysis_Common';
import * as Colors from 'material-ui/styles/colors';

let Line = require('react-chartjs-2').Line;

export default class ICGraph extends React.Component<Props, State> {
    isUpdating              : boolean = false;
    taskProgress            : any = null;

    constructor(props: Props) {
        super(props);

        this.state = {
            chartData: {},
            chartOptions: {
                bezierCurve: true,
                responsive: true,
                scales: {
                    yAxes: [{
                        scaleLabel: {
                            display: true,
                            labelString: 'Correlation',
                            fontSize: 40,
                            fontColor: 'white'
                        },
                        ticks: {
                            // Create scientific notation labels
                            callback: function (value, index, values) {
                                return value.toFixed(2);
                            }
                        },
                        gridLines: {
                            zeroLineColor: 'white',
                            color: 'rgba(255, 255, 255, 0.3)'
                        }
                    }],
                    xAxes: [{
                        scaleLabel: {
                            display: true,
                            labelString: "∆ Days",
                            fontSize: 40,
                            fontColor: 'white'
                        },
                        gridLines: {
                            zeroLineColor: 'white',
                            color: 'rgba(255, 255, 255, 0.3)'
                        }
                    }],
                }
            },
        };
    }

    updateGraph() {
        if (!this.props.strategy)
            return;

        if (this.isUpdating)
            return;

        this.isUpdating = true;

        this.setState({
            isLoading: true
        });

        if (this.taskProgress)
            this.taskProgress.taskUpdate(0, "Loading ...");

        let promise =       this.props.duration === "SD" && this.props.positionFile ? 
        this.props.strategy.getPositions(this.props.positionFile) : 
        this.props.strategy.getPositionsDuration(this.props.duration, this.props.startDate, this.props.endDate, (percent, statusMessage) => {
            if (this.taskProgress) 
                this.taskProgress.taskUpdate(percent, statusMessage);
        });

        promise
            .then((pos) => {
                if (!pos) {
                    this.setState({
                        loadingText: "Unable to load position data!"
                    });
                    return;
                }

                this.setState({
                    positions: pos
                });

                if (!pos.ic) 
                    return;

                let ic = pos.ic;

                let values = (new Array(ic.points.length + 1)).fill(0.0);
                let stddevUp = (new Array(ic.points.length + 1)).fill(0.0);
                let stddevDown = (new Array(ic.points.length + 1)).fill(0.0);
                let deltas = new Array(ic.points.length + 1);
                deltas[0] = "D0";

                ic.points.map((point, index) => { 
                    deltas[index + 1] = "D" + (index + 1).toString();
                    values[index + 1] = point.value;
                    stddevUp[index + 1] = point.value + point.stddev;
                    stddevDown[index + 1] = point.value - point.stddev;
                });

                let chartData = {
                    labels: deltas,
                    datasets: [{
                        label: "Corr",
                        borderWidth: 2,
                        pointRadius: 3,
                        backgroundColor: 'transparent',
                        borderColor: Colors.blue400,
                        steppedLine: false,
                        data: values
                    }, {
                        label: "Corr + SD",
                        borderWidth: 2,
                        pointRadius: 3,
                        backgroundColor: 'transparent',
                        borderColor: Colors.green400,
                        hidden: true,
                        steppedLine: false,
                        data: stddevUp
                    }, {
                        label: "Corr - SD",
                        borderWidth: 2,
                        pointRadius: 3,
                        backgroundColor: 'transparent',
                        hidden: true,
                        borderColor: Colors.red400,
                        steppedLine: false,
                        data: stddevDown
                    }]
                };

                this.setState({
                    chartData: chartData,
                    isLoading: false
                });

                this.isUpdating = false;
            });
    }

    componentDidMount() {
        this.updateGraph();
    }

    render() {
        return (
            <Line data={this.state.chartData} options={this.state.chartOptions} />
        );
    }
}

// export default createContainer(() => {
//     // let shell = Meteor.npmRequire('shelljs');
//     console.log("Getting strats in path: ${path}");
//     // let strats = shell.ls(path);

//     return {
//         strats: ["Test1", "Test2"],
//         name: "US"
//     };
// }, PosGraphs);
