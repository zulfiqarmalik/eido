import React from 'react';
import { Props, State } from './Analysis_Common';
import * as Colors from 'material-ui/styles/colors';
import { Positions } from 'Shared/Model/Stats/Positions';
import {Toolbar, ToolbarGroup} from 'material-ui/Toolbar';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import Divider from 'material-ui/Divider';

let Bar = require('react-chartjs-2').Bar;
let Line = require('react-chartjs-2').Line;

export interface IGState extends State {
    pnlChartData            : any;
    sectorPnlChartData      : any;
    sectors                 : string[];
    sector                  : string;
    sectorPnls              : any[];
    tooltips                : any;
    pnlChartOptions         : any;
    sectorPnlChartOptions   : any;
}

export default class IndustryGraph extends React.Component<Props, IGState> {
    isUpdating              : boolean = false;
    positions               : Positions = null;
    taskProgress            : any = null;

    constructor(props: Props) {
        super(props);

        this.state = {
            positions: null,
            chartData: {},
            pnlChartData: {},
            sectorPnlChartData: {},
            sector: "ALL",
            sectors: [],
            sectorPnls: [],
            isLoading: false,
            loadingText: "Loading data. Please wait ...",
            // tooltipTemplate: "<%= value.toFixed(2) + ' %' %>",
            // tooltipTemplate: function (valueObj) {
            //     return valueObj.value.toFixed(2);
            // },
            tooltips: {
                callbacks: {
                    label: function (tooltipItem, data) {
                        var datasetLabel = data.datasets[tooltipItem.datasetIndex].label || 'Other';
                        var label = data.labels[tooltipItem.index];
                        return datasetLabel + ': ' + label;
                    }
                }
            },
            chartOptions: {
                scales: {
                    yAxes: [{
                        scaleLabel: {
                            display: true,
                            labelString: '% of BookSize',
                            fontSize: 40,
                            fontColor: 'white'
                        },
                        gridLines: {
                            color: 'rgba(255, 255, 255, 0.3)'
                        },
                        ticks: {
                            // Create scientific notation labels
                            callback: function (value, index, values) {
                                return value.toFixed(2) + " %";
                            }
                        }
                    }],
                    xAxes: [{
                        stacked: false,
                        beginAtZero: true,
                        scaleLabel: {
                            display: false,
                            labelString: this.getLabel(),
                            fontSize: 40,
                            fontColor: 'white'
                        },
                        gridLines: {
                            color: 'rgba(255, 255, 255, 0.3)'
                        },
                        ticks: {
                            stepSize: 1,
                            min: 0,
                            autoSkip: false
                        }
                    }],
                }
            },
            pnlChartOptions: {
                scales: {
                    yAxes: [{
                        scaleLabel: {
                            display: true,
                            labelString: 'BPS',
                            fontSize: 40,
                            fontColor: 'white'
                        },
                        gridLines: {
                            color: 'rgba(255, 255, 255, 0.3)'
                        },
                    }],
                    xAxes: [{
                        stacked: false,
                        beginAtZero: true,
                        scaleLabel: {
                            display: false,
                            labelString: this.getLabel(),
                            fontSize: 40,
                            fontColor: 'white'
                        },
                        gridLines: {
                            color: 'rgba(255, 255, 255, 0.3)'
                        },
                        ticks: {
                            stepSize: 1,
                            min: 0,
                            autoSkip: false
                        }
                    }],
                }
            },
            sectorPnlChartOptions: {
                bezierCurve: false,
                responsive: true,
                legend: {
                    display: false
                },
                scales: {
                    yAxes: [{
                        scaleLabel: {
                            display: true,
                            labelString: 'P&L',
                            fontSize: 40,
                            fontColor: 'white'
                        },
                        gridLines: {
                            color: 'rgba(255, 255, 255, 0.3)'
                        },
                        ticks: {
                            // Create scientific notation labels
                            callback: function (value, index, values) {
                                return value.toExponential(2);
                            }
                        }
                    }]
                }
            }
        };
    }

    updateSectorGraph(sectors, sectorPnls, sector) {
        let usedSectorPnls = sectorPnls;
        let usedSectorNames = sectors;

        if (sector != "ALL") { 
            usedSectorNames = [sector];
            usedSectorPnls = null;

            for (let i = 0; i < sectors.length && !usedSectorPnls; i++) {
                if (sectors[i] == sector)
                    usedSectorPnls = [sectorPnls[i]];
            }
        }

        let colours = [Colors.blue400, Colors.red400, Colors.green400, Colors.orange400, Colors.yellow400, Colors.indigo400, 
            Colors.cyan400, Colors.pink400, Colors.purple400, Colors.deepPurple400, Colors.lime400, Colors.deepOrange400, 
            Colors.brown400, Colors.grey400, Colors.blueGrey400, Colors.amber400
        ];
        let chartData = {
            labels: this.positions.dates && this.positions.dates.length ? this.positions.dates : [0, this.positions.date],
            datasets: []
        };

        let colourIndex = 0;
        console.log("[Industry] Sectors: %s", usedSectorNames.toString());

        if (usedSectorNames.length > 10) 
            this.state.sectorPnlChartOptions.legend.display = false;
        else
            this.state.sectorPnlChartOptions.legend.display = true;

        usedSectorNames.map((usedSectorName, index) => {
            chartData.datasets.push({
                label: usedSectorName,
                borderWidth: 2,
                lineTension: 0,
                pointRadius: usedSectorPnls[index].length < 100 ? 3 : 0,
                backgroundColor: 'transparent',
                borderColor: colours[colourIndex],
                steppedLine: false,
                data: usedSectorPnls[index]
            });

            colourIndex += 1;
            if (colourIndex >= colours.length)
                colourIndex = 0;
        });
        
        this.setState({
            sectorPnlChartData: chartData,
            sectorPnls: sectorPnls,
            sectors: sectors,
            sector: sector
        });
    }

    getBorderWidth() {
        return this.props.type == "industry" ? 0 : 2;
    }

    updateGraph() {
        if (!this.props.strategy)
            return;

        if (this.isUpdating)
            return;

        this.isUpdating = true;
        this.setState({ 
            isLoading: true
        });

        if (this.taskProgress)
            this.taskProgress.taskUpdate(0, "Loading ...");

        let promise =       this.props.duration === "SD" && this.props.positionFile ? 
                            this.props.strategy.getPositions(this.props.positionFile) : 
                            this.props.strategy.getPositionsDuration(this.props.duration, this.props.startDate, this.props.endDate, (percent, statusMessage) => {
                                if (this.taskProgress) 
                                    this.taskProgress.taskUpdate(percent, statusMessage);
                            });

        promise
            .then((pos) => {
                if (!pos) {
                    this.setState({
                        loadingText: "Unable to load position data!"
                    });
                    return;
                }

                this.positions = pos;
                this.setState({
                    positions: pos
                });

                if (this.props.type == "country")
                    return pos.getCountries();
                else if (this.props.type == "industry") 
                    return pos.getIndustry();
                else if (this.props.type == "mcap")
                    return pos.getMCapTypes();
                return pos.getSectors();
            })
            .then((sectors) => {
                let sectorIndexes = {};
                let uniqueSectors = [];
                let absNotionals = [];
                let longNotionals = [];
                let shortNotionals = [];
                let pnls = [];
                let pnlsPercentOfNotional = [];
                let pnlsPercentOfBooksize = [];
                let sectorPnls = [];
                let sectorCumPnls = [];

                this.positions.positions.map((position, index) => {
                    let sector = sectors[index];

                    if (typeof sector != "string") 
                        return;
                        
                    if (this.props.type == "country") 
                        sector = sector.toUpperCase();

                    if (sectorIndexes[sector] == undefined) {
                        sectorIndexes[sector] = {
                            index: absNotionals.length,
                            count: 1
                        };
                        uniqueSectors.push(sector);
                        absNotionals.push(Math.abs(position.notional))

                        if (position.notional > 0.0) {
                            longNotionals.push(position.notional);
                            shortNotionals.push(0.0);
                        }
                        else {
                            longNotionals.push(0.0);
                            shortNotionals.push(Math.abs(position.notional));
                        }

                        if (position.pnl) 
                            pnls.push(position.pnl.pnlClose);

                        if (this.positions.posPnls) {
                            sectorPnls.push(this.positions.posPnls[index].pnls.slice());
                            sectorPnls[sectorPnls.length - 1][0] = 0.0;
                        }
                        else if (position.pnl) 
                            sectorPnls.push([0, 0]);
                    }
                    else { 
                        let sectorIndex = sectorIndexes[sector].index;
                        sectorIndexes[sector].count++;
                        absNotionals[sectorIndex] += Math.abs(position.notional);

                        if (position.notional > 0.0)
                            longNotionals[sectorIndex] += position.notional;
                        else
                            shortNotionals[sectorIndex] += Math.abs(position.notional);

                        if (position.pnl)
                            pnls[sectorIndex] += position.pnl.pnlClose;

                        if (this.positions.posPnls) {
                            // Debug.assert(sectorPnls[sectorIndex].length == this.positions.posPnls[index].length,
                            //     "Length mismatch with daily positions. Expecting: %d", sectorPnls[sectorIndex].length, " - Found: %d", this.positions.posPnls[index].length);
                            for (let i = 1; i < this.positions.posPnls[index].pnls.length; i++) {
                                let ppnl = this.positions.posPnls[index].pnls[i];
                                sectorPnls[sectorIndex][i] += ppnl;
                            }
                        }
                        else if (position.pnl) 
                            sectorPnls[sectorIndex][1] += position.pnl.pnlClose;
                    }
                });

                sectorPnls.map((sectorPnl) => {
                    if (!sectorPnl.length)
                        return;
                    let cumPnl = sectorPnl[0];
                    for (let jj = 1; jj < sectorPnl.length; jj++) {
                        cumPnl += sectorPnl[jj];
                        sectorPnl[jj] = cumPnl;
                    }
                });

                let totalNotional = 0.0;
                absNotionals.map((n) => totalNotional += n);

                let bookSize = totalNotional * 0.01;
                pnlsPercentOfNotional = new Array(pnls.length).fill(0);
                pnlsPercentOfBooksize = new Array(pnls.length).fill(0);

                absNotionals.map((n, index) => {
                    pnlsPercentOfNotional[index] = (pnls[index] / absNotionals[index]) * 10000.0;
                    pnlsPercentOfBooksize[index] = (pnls[index] / bookSize) * 100.0; /// booksize is already multiplied by 0.01
                    
                    absNotionals[index]     = (absNotionals[index] / bookSize);
                    longNotionals[index]    = (longNotionals[index] / bookSize);
                    shortNotionals[index]   = (shortNotionals[index] / bookSize);
                });

                uniqueSectors.map((uniqueSector, index) => {
                    let info = sectorIndexes[uniqueSector];
                    uniqueSectors[index] += " [" + info.count.toString() + "]";
                });

                let chartData = {
                    labels: uniqueSectors,
                    xAxisID: this.getLabel(),
                    yAxisID: "Notional",
                    datasets: [{
                        label: "Total",
                        backgroundColor: Colors.blue400,
                        borderWidth: this.getBorderWidth(),
                        borderColor: 'white',
                        fillColor: "blue",
                        data: absNotionals
                    }, {
                        label: "Long",
                        backgroundColor: Colors.green400,
                        borderWidth: this.getBorderWidth(),
                        borderColor: 'white',
                        fillColor: "green",
                        data: longNotionals
                    }, {
                        label: "Short",
                        backgroundColor: Colors.red400,
                        borderWidth: this.getBorderWidth(),
                        borderColor: 'white',
                        fillColor: "red",
                        data: shortNotionals
                    }]
                };

                let pnlChartData = null;

                if (pnls.length == longNotionals.length) {
                    pnlChartData = {
                        labels: uniqueSectors,
                        xAxisID: chartData.xAxisID,
                        yAxisID: chartData.yAxisID,
                        datasets: [{
                            label: "P&L / BookSize",
                            borderWidth: this.getBorderWidth(),
                            borderColor: 'white',
                            backgroundColor: Colors.red400,
                            fillColor: "red",
                            data: pnlsPercentOfBooksize
                        }, {
                            label: "P&L / Notional",
                            borderWidth: this.getBorderWidth(),
                            borderColor: 'white',
                            backgroundColor: Colors.blue400,
                            fillColor: "blue",
                            data: pnlsPercentOfNotional
                        }]
                    };
                }

                this.updateSectorGraph(uniqueSectors, sectorPnls, this.state.sector);

                this.setState({
                    chartData: chartData,
                    pnlChartData: pnlChartData,
                    isLoading: false
                });

                this.isUpdating = false;
            });
    }

    componentDidMount() {
        this.updateGraph();
    }

    onSectorChange = (e, index, value) => {
        this.updateSectorGraph(this.state.sectors, this.state.sectorPnls, value);
    }

    getLabel() {
        if (this.props.type == "industry")
            return "Industry";
        else if (this.props.type == "sector")
            return "Sector";
        else if (this.props.type == "mcap")
            return "Market Cap";
        return "Country";
    }

    render() {
        return (
            <div>
                <Bar className={!this.state.pnlChartData ? "hidden" : ""} data={this.state.pnlChartData ? this.state.pnlChartData : {}} options={this.state.pnlChartOptions} />
                <div>
                    <Toolbar>
                        <ToolbarGroup>
                        </ToolbarGroup>
                        
                        <ToolbarGroup>
                            <h2 className="para-heading">Individual {this.getLabel()} Total and Long/Short Exposures </h2>
                        </ToolbarGroup>

                        <ToolbarGroup>
                            <SelectField
                                floatingLabelText={this.getLabel()}
                                value={this.state.sector}
                                onChange={this.onSectorChange}
                                style={{width: 400}}
                            >
                                <MenuItem key="ALL" value="ALL" primaryText="ALL" />
                                <Divider />
                                {
                                    (this.state.sectors).map((sector, index) => {
                                        return <MenuItem key={index} value={sector} primaryText={sector} />;
                                    })
                                }                
                            </SelectField>
                        </ToolbarGroup>
                   </Toolbar >

                    <Line key={this.state.sector} data={this.state.sectorPnlChartData} options={this.state.sectorPnlChartOptions} />

                    <h2 className="para-heading">Individual {this.getLabel()} Total and Long/Short Exposures </h2>
                    <Bar data={this.state.chartData} options={this.state.chartOptions} />

                </div>
            </div>
        );
    }
}
