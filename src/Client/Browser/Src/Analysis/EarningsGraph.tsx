import React from 'react';
import * as Colors from 'material-ui/styles/colors';
import { Positions } from 'Shared/Model/Stats/Positions';

let Bar = require('react-chartjs-2').Bar;
let Line = require('react-chartjs-2').Line;
import { Props, State } from './Analysis_Common';

export default class EarningsGraph extends React.Component<Props, State> {
    isUpdating          : boolean = false;
    positions           : Positions = null;
    taskProgress        : any = null;

    constructor(props: Props) {
        super(props);

        this.isUpdating = false;
        this.state = {
            positions: null,
            maxDrift: 5,
            chartData: {},
            driftChartData: {},
            isLoading: false,
            loadingText: "Loading earnings data. Please wait ...",
            chartOptions: {
                bezierCurve: false,
                responsive: true,
                scales: {
                    yAxes: [{
                        scaleLabel: {
                            display: true,
                            labelString: 'P&L',
                            fontColor: 'white',
                            fontSize: 40,
                        },
                        ticks: {
                            // Create scientific notation labels
                            callback: function (value, index, values) {
                                return value.toExponential(2);
                            }
                        },
                        gridLines: {
                            zeroLineColor: 'white',
                            color: 'rgba(255, 255, 255, 0.3)'
                        }
                    }],
                    xAxes: [{
                        stacked: false,
                        beginAtZero: true,
                        scaleLabel: {
                            display: false,
                            labelString: "Dates",
                            fontColor: 'white',
                            fontSize: 40,
                        },
                        gridLines: {
                            zeroLineColor: 'white',
                            color: 'rgba(255, 255, 255, 0.3)'
                        }
                    }],
                },
                annotation: {
                    annotations: [{
                        type: "line",
                        mode: "vertical",
                        scaleID: "x-axis-0",
                        value: this.props.stats ? this.props.stats.osDate : 0,
                        borderColor: "magenta",
                        label: {
                            content: "OS Start",
                            enabled: true,
                            position: "middle"
                        }
                    }]
                }
            },
            driftChartOptions: {
                bezierCurve: true,
                responsive: true,
                scales: {
                    yAxes: [{
                        scaleLabel: {
                            display: true,
                            labelString: 'Avg. Accum. Returns BPS',
                            fontColor: 'white',
                            fontSize: 40,
                        },
                        ticks: {
                            // Create scientific notation labels
                            callback: function (value, index, values) {
                                return value.toFixed(2);
                            }
                        },
                        gridLines: {
                            zeroLineColor: 'white',
                            color: 'rgba(255, 255, 255, 0.3)'
                        }
                    }],
                    xAxes: [{
                        scaleLabel: {
                            display: true,
                            labelString: "Days",
                            fontColor: 'white',
                            fontSize: 40
                        },
                        gridLines: {
                            zeroLineColor: 'white',
                            color: 'rgba(255, 255, 255, 0.3)'
                        }
                    }],
                },
                annotation: {
                    annotations: [{
                        type: "line",
                        mode: "vertical",
                        scaleID: "x-axis-0",
                        value: 0,
                        borderColor: Colors.red400,
                        label: {
                            content: "Earnings Announcement",
                            enabled: true,
                            position: "top"
                        }
                    }]
                }
            },
        };
    }

    updateGraph() {
        if (!this.props.strategy)
            return;

        if (this.isUpdating)
            return;

        this.isUpdating = true;

        this.setState({ 
            isLoading: true
        });

        if (this.taskProgress)
            this.taskProgress.taskUpdate(0, "Loading ...");

        let promise =       null;

        promise
            .then((pos) => {
                if (!pos) {
                    this.setState({
                        loadingText: "Unable to load position data!"
                    });
                    return;
                }

                if (this.taskProgress)
                    this.taskProgress.taskFinished("Finalising results ...");

                this.positions = pos;
                this.setState({
                    loadingText: "Loading earnings dates!",
                    positions: pos
                });

                return pos.getEarnings();
            })
            .then((earnings) => {
                if (!earnings) {
                    this.setState({
                        loadingText: "This strategy does not support earnings! Please enable it within the Stats section of the strategy!"
                    });
                    return;
                }

                let dates = this.positions.dates;
                let pnls = new Array(dates.length).fill(0.0);
                let shortPnls = new Array(dates.length).fill(0.0);
                let longPnls = new Array(dates.length).fill(0.0);
                let maxDrift = this.state.maxDrift;
                let earningsDrift = new Array(maxDrift * 2 + 1).fill(0.0);
                let earningsDriftCount = new Array(maxDrift * 2 + 1).fill(0);
                let pnlAfter = new Array(maxDrift).fill(0.0);
                let datesLookup = this.positions.getDateLookup();

                earnings.map((earning, posIndex) => {
                    earning.dates.map((earningDate) => {
                        let pnlIndex = datesLookup[earningDate];

                        if (typeof pnlIndex === 'undefined')
                            return;

                        let pinfoPrev = this.positions.getPnlOnDate(posIndex, pnlIndex - 1);
                        let pinfo = this.positions.getPnlOnDate(posIndex, pnlIndex);
                        let pinfoNext = this.positions.getPnlOnDate(posIndex, pnlIndex + 1);

                        if (pinfoPrev.notional > 0.0)
                            longPnls[pnlIndex] += pinfo.pnl;
                        else if (pinfoPrev.notional < 0.0)
                            shortPnls[pnlIndex] += pinfo.pnl;

                        if (pinfo.notional > 0.0)
                            longPnls[pnlIndex] += pinfoNext.pnl;
                        else if (pinfo.notional < 0.0)
                            shortPnls[pnlIndex] += pinfoNext.pnl;

                        pnls[pnlIndex] += (pinfo.pnl + pinfoNext.pnl);

                        for (let i = -maxDrift; i <= maxDrift; i++) {
                            let driftIndex = i + maxDrift;
                            let dpnlIndex = pnlIndex + i;
                            if (dpnlIndex <= 0)
                                continue;

                            let pinfoPrev = this.positions.getPnlOnDate(posIndex, dpnlIndex - 1);
                            let pinfo = this.positions.getPnlOnDate(posIndex, dpnlIndex);

                            if (pinfo.pnl != 0.0 && pinfoPrev.notional != 0.0) {
                                let returns = (pinfo.pnl / Math.abs(pinfoPrev.notional)) * 10000.0;
                                earningsDrift[driftIndex] += returns;
                                earningsDriftCount[driftIndex] += 1;
                            }
                        }
                    });
                });

                pnls[0] = 0.0;
                longPnls[0] = 0.0;
                shortPnls[0] = 0.0; 
                for (let i = 1; i < pnls.length; i++) {
                    pnls[i] += pnls[i - 1];
                    longPnls[i] += longPnls[i - 1];
                    shortPnls[i] += shortPnls[i - 1];
                }
                
                let driftLabels = new Array(earningsDrift.length).fill(0);
                for (let i = 0; i < earningsDrift.length; i++) {
                    if (earningsDriftCount[i] > 0)
                        earningsDrift[i] /= earningsDriftCount[i];
                    if (i > 0)
                        earningsDrift[i] += earningsDrift[i - 1];
                    driftLabels[i] = (i - maxDrift);
                }

                // let middlePoint = earningsDrift[this.state.maxDrift];
                // for (let i = 0; i < earningsDrift.length; i++) 
                //     earningsDrift[i] -= middlePoint;
                

                let chartData = {
                    labels: dates,
                    xAxisID: "Dates",
                    yAxisID: "P&L",
                    datasets: [{
                        label: "Total",
                        borderWidth: 2,
                        lineTension: 0,
                        pointRadius: pnls.length < 100 ? 3 : 0,
                        backgroundColor: 'transparent',
                        borderColor: Colors.blue400,
                        steppedLine: false,
                        data: pnls
                    }, {
                        label: "Long",
                        borderWidth: 2,
                        lineTension: 0,
                        pointRadius: pnls.length < 100 ? 3 : 0,
                        backgroundColor: 'transparent',
                        borderColor: Colors.green400,
                        steppedLine: false,
                        data: longPnls
                    }, {
                        label: "Short",
                        borderWidth: 2,
                        lineTension: 0,
                        pointRadius: pnls.length < 100 ? 3 : 0,
                        backgroundColor: 'transparent',
                        borderColor: Colors.red400,
                        steppedLine: false,
                        data: shortPnls
                    }]
                };

                let driftChartData = {
                    labels: driftLabels,
                    xAxisID: "Days",
                    yAxisID: "Avg. BPS Returns",
                    datasets: [{
                        label: "Average Returns",
                        borderWidth: 3,
                        pointRadius: 3,
                        backgroundColor: 'transparent',
                        borderColor: Colors.blue400,
                        steppedLine: false,
                        data: earningsDrift
                    }]
                };

                this.setState({
                    chartData: chartData,
                    driftChartData: driftChartData,
                    isLoading: false
                });

                this.isUpdating = false;
            });
    }

    componentDidMount() {
        this.updateGraph();
    }

    render() {
        return (
            <div>
                <Line data={this.state.chartData} options={this.state.chartOptions} />

                <hr/>
                <h3 className="para-heading">Average Returns in -{this.state.maxDrift} to +{this.state.maxDrift} day window from Earnings date</h3>

                <Line data={this.state.driftChartData} options={this.state.driftChartOptions} />
            </div>
        );
    }
}

// export default createContainer(() => {
//     // let shell = Meteor.npmRequire('shelljs');
//     console.log("Getting strats in path: ${path}");
//     // let strats = shell.ls(path);

//     return {
//         strats: ["Test1", "Test2"],
//         name: "US"
//     };
// }, PosGraphs);
