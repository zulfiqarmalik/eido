import React from 'react';
import { Debug } from "Shared/Core/ShUtil";
import { Props, State } from './Analysis_Common';
import * as Colors from 'material-ui/styles/colors';
import LongShort_Pie from './LongShort_Pie';
import { PlotIndex } from 'Shared/Model/Stats/CumStats';

let Line = require('react-chartjs-2').Line;
require("chartjs-plugin-annotation");

export default class LongShort_Graph extends React.Component<Props, State> {
    constructor(props) {
        super(props);

        this.state = {
            chartData: {},
            chartOptions: {
                bezierCurve: false,
                responsive: true,
                // maintainAspectRatio: false,
                scales: {
                    yAxes: [{
                        scaleLabel: {
                            display: true,
                            labelString: '% Returns',
                            fontSize: 40,
                            fontColor: 'white'
                        },
                        ticks: {
                            // Create scientific notation labels
                            callback: function (value, index, values) {
                                return value.toFixed(2) + "%";
                            }
                        }
                    }],
                    xAxes: [{
                        ticks: {
                            fontSize: 10,
                        },
                    }],
                }
            }
        };
    }

    static addChartDataset(name: string, colour: any, plotIndexes: PlotIndex, plotData: any, hidden: boolean = false, key: string = "") {
        return {
            label: name,
            borderWidth: 2,
            lineTension: 0,
            hidden: hidden, 
            pointRadius: plotData.pnls.length < 100 ? 3 : 0,
            backgroundColor: 'transparent', ///'rgba(' + colour + ',0.5)',
            borderColor: colour,
            steppedLine: false,
            data: plotData.pnls,
            // pointBackgroundColor: pointBackgroundColors
        };
    }

    updateAnalysis_LongShortGraph() {
        Debug.assert(this.props.stats, "Stats not intialised properly!");

        let stats = this.props.stats;
        let plotIndexes = stats.getPlotIndexes(this.props.duration, 0, this.props.positionFile, this.props.startDate, this.props.endDate);
        let plotDataLong = stats.getPnlPlotDataForRange(plotIndexes.istart, plotIndexes.iend, "longPnl", true);
        let plotDataShort = stats.getPnlPlotDataForRange(plotIndexes.istart, plotIndexes.iend, "shortPnl", true);
        let plotData = stats.getPnlPlotDataForRange(plotIndexes.istart, plotIndexes.iend, "pnl", true);

        let chartData = {
            labels: plotDataLong.dates,
            datasets: []
        };

        chartData.datasets.push(LongShort_Graph.addChartDataset("Net", Colors.blue400, plotIndexes, plotData, false));
        chartData.datasets.push(LongShort_Graph.addChartDataset("Long", Colors.green400, plotIndexes, plotDataLong, false));
        chartData.datasets.push(LongShort_Graph.addChartDataset("Short", Colors.red400, plotIndexes, plotDataShort, false));

        let chartOptions = this.state.chartOptions;
        chartOptions["annotation"] = {
            annotations: [{
                type: "line",
                mode: "vertical",
                scaleID: "x-axis-0",
                value: stats.osDate,
                borderColor: "magenta",
                label: {
                    content: "OS Start",
                    enabled: true,
                    position: "middle"
                }
            }]
        };

        this.setState({
            chartData: chartData,
            chartOptions: chartOptions
        });
    }

    // componentDidMount() {
    //     if (!this.props.stats)
    //         return;

    //     let cstats = this.props.stats;
    //     if (this.props.duration == "IS")
    //         cstats = this.props.stats.inSample;
    //     else if (this.props.duration == "OS")
    //         cstats = this.props.stats.outSample;
    //     else if (this.props.duration)
    //         cstats = this.props.stats[this.props.duration];

    //     this.setState({
    //         analysis: cstats.runAnalysis()
    //     });
    // }

    componentDidMount() {
        if (!this.props.stats)
            return;

        console.log("[LSGraph] Updating graph ...");
        this.updateAnalysis_LongShortGraph();
    }

    handleActiveState(eventKey) {
    }

    render() {
        return (
            <div>
                <Line data={this.state.chartData} options={this.state.chartOptions} />

                <h2 className="para-heading">{`Long/Short P&L Convergence/Divergence`}</h2>
                <LongShort_Pie
                    location={this.props.location}
                    match={this.props.match}
                    history={this.props.history}
                    stats={this.props.stats}
                    duration={this.props.duration}
                    positionFile={this.props.positionFile}
                />
            </div>
        );
    }
}

// export default createContainer(() => {
//     // let shell = Meteor.npmRequire('shelljs');
//     console.log("Getting strats in path: ${path}");
//     // let strats = shell.ls(path);

//     return {
//         strats: ["Test1", "Test2"],
//         name: "US"
//     };
// }, PnlGraphs);

