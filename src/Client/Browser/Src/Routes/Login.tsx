import React from 'react';
import { withRouter, RouteComponentProps } from 'react-router';
import { User } from 'Client/Model/User/ClUser';
import PropTypes from 'prop-types';
import TextField from 'material-ui/TextField';
import Paper from 'material-ui/Paper';
import RaisedButton from 'material-ui/RaisedButton';
import * as Colors from 'material-ui/styles/colors';
import { Cl } from 'Client/Core/ClCore';

export interface State { 
    email       : string;
    password    : string;
    error       : string;
    success     : string;
}

interface Props extends RouteComponentProps<any>, React.Props<any> {
    onLogin     ?: () => void;
}

const buttonStyle = {
    margin: 12,
};

const style = {
    // height: 100,
    width: 600,
    padding: 40,
    textAlign: 'center' as any,
    display: 'inline-block',
};
  
export default class Login extends React.Component<Props, State> {
    constructor(props: Props) {
        super(props);
        
        this.state = { 
            email: "",
            password: "",
            error: "",
            success: ""
        }
    }

    onLogin = (e) => {
        this.setState({
            success: "",
            error: ""
        });

        User.login(this.state.email, this.state.password)
            .then((user) => {
                console.info(`[Login] User authenticated: ${user.email}`);
                // browserHistory.replace("/");
                // window.location.href = "/";
            })
            .catch((err) => {
                console.log(err);
            });

    }

    onEmailChange = (e, value) => {
        this.setState({
            email: value
        });
    }

    onPasswordChange = (e, value) => {
        this.setState({
            password: value
        });
    }

    onRegister = () => {
        this.setState({
            success: "",
            error: ""
        });

        User.register(this.state.email, this.state.password)
            .then(() => {
                this.setState({
                    success: "User registered successfully. You can now login!"
                });
            })
            .catch((err) => {
                this.setState({
                    error: err.response.data.message
                });
                // Cl.ui.alert("Error", err.response.data.message);
            });
    }

    render() {
        return (
            <div className="login">
                <form className="login-form" onSubmit={this.onLogin}>
                    <Paper style={style} zDepth={1}>

                        <h1>QuanVolve Login/Register</h1>

                        <TextField
                            autoFocus
                            id="email"
                            floatingLabelText="Email Address"
                            type="email"
                            onChange={this.onEmailChange}
                            autoComplete="off"
                            fullWidth
                        />

                        <TextField
                            id="password"
                            floatingLabelText="Password"
                            type="password"
                            onChange={this.onPasswordChange}
                            autoComplete="off"
                            fullWidth
                        />

                        <RaisedButton 
                            label="Login" 
                            type="submit"
                            style={buttonStyle} 
                            backgroundColor={Colors.cyan700}
                        />

                        <RaisedButton 
                            label="Register" 
                            style={buttonStyle} 
                            backgroundColor={Colors.pink700}
                            onClick={this.onRegister}
                        />

                    </Paper>
                </form>

                <h3 className={this.state.error ? "error-text" : "hidden"}>{this.state.error}</h3>
                <h3 className={this.state.success ? "success-text" : "hidden"}>{this.state.success}</h3>
            </div>
        );
    }
}