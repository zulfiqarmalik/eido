import React from 'react';
import PropTypes from 'prop-types';
import { RouteComponentProps } from 'react-router';

import { Markets as gMarkets, MarketInfo } from 'Shared/Model/Config/ShAlpha';

import AppBar from 'material-ui/AppBar';
import FlatButton from 'material-ui/FlatButton';

import IconMenu from 'material-ui/IconMenu';
import MenuItem from 'material-ui/MenuItem';
import MoreVertIcon from 'material-ui/svg-icons/navigation/more-vert';
import NotificationIcon from 'material-ui/svg-icons/social/notifications';
import ArrowDropRight from 'material-ui/svg-icons/navigation-arrow-drop-right';

import Home from "../Home/Home"
import Port from "../Portfolio/Port";
import Dev from "../Developer/Dev";
import Docs from "../Main/Docs";
import Blog from "../Main/Blog";
import System from "../System/Sys";
import * as qs from 'query-string';
import { AppPages } from '../AppState';
import { ViewType } from 'Client/Browser/Src/Portfolio/PortDefs';
import { Divider } from 'material-ui';
import { User } from 'Client/Model/User/ClUser';

const buttonStyles = {
    height: 65,
};

const notificationsButtonStyles = {
    height: 65,
    width: 50,
    minWidth: 0
};

export interface State { 
    activePage          : string;
    market              : string;
    anchorEl            ?: any;
}

export interface Props extends RouteComponentProps<any>, React.Props<any> {
    onLogout            ?: () => void;
    classes             ?: any;
}

export default class MainNav extends React.Component<Props, State> {
    static propTypes = {
        onLogout: PropTypes.func
    };

    activePage          : string = AppPages.home;
    market              : string = "US";

    constructor(props: Props) {
        super(props);

        this.state = {
            activePage: this.activePage,
            market: this.market
        };
    }

    componentDidMount() {
        const args = qs.parse(this.props.location.hash);
        this.activePage = (args.p as string) || (AppPages.home as string);

        if (User.current) {
            User.current.loadSettings(["global.market"], [this.state.market])
                .then((values: string[]) => {
                    let market = values[0];
    
                    this.setState({
                        activePage: this.activePage
                    });
    
                    this.market = market;
                    this.changeMarket(market);
                });
        }
        else {
            this.setState({
                activePage: this.activePage
            });
        }
    }

    componentDidUpdate(location) {
        // console.log("YEEEEESSS");
        const args = qs.parse(this.props.location.hash);

        if (!args || !args.p || (args.p == this.activePage && args.market == this.market))
            return;

        this.activePage = args.p as string;
        this.market = args.market as string;

        this.setState({
            activePage: this.activePage,
            market: this.market
        });
    }

    getActivePage() {
        if (this.state.activePage == AppPages.home) 
            return (<Home location={this.props.location} match={this.props.match} history={this.props.history} />);
        else if (this.state.activePage == AppPages.portfolio) 
            return (<Port location={this.props.location} match={this.props.match} history={this.props.history} />);
        else if (this.state.activePage == AppPages.developer) 
            return (<Dev location={this.props.location} match={this.props.match} history={this.props.history} />);
        else if (this.state.activePage == AppPages.docs) 
            return (<Docs location={this.props.location} match={this.props.match} history={this.props.history} />);
        else if (this.state.activePage == AppPages.blog) 
            return (<Blog location={this.props.location} match={this.props.match} history={this.props.history} />);
        else if (this.state.activePage == AppPages.system) 
            return (<System location={this.props.location} match={this.props.match} history={this.props.history} />);
        return (<Home location={this.props.location} match={this.props.match} history={this.props.history} />);
    }
    
    getTitle() {
        return (
            <div>
                <a href={`#p=home&market=${this.state.market}`}>
                    <img src='/imgs/logo_web.png' alt='QuanVolve' width='238' height='60' />
                </a>
            </div>
        );
    }

    getBkColor(name) {
        if (this.state.activePage == name)
            return "#001f3f";
        return "";
    }

    onNavClick(name: string, additionalArgs: any) {
        let url = `#p=${name.toLowerCase()}&market=${this.state.market}`;

        for (let key in additionalArgs)
            url += `&${key}=${additionalArgs[key]}`;

        this.props.history.push(url);
    }

    renderTitleButton(name: string, additionalArgs: any = null) {
        let lcName = name.toLowerCase();
        return (
            <FlatButton 
                label={name} 
                backgroundColor={this.getBkColor(lcName)} 
                hoverColor="#001f3f" 
                style={buttonStyles} 
                onClick={() => this.onNavClick(lcName, additionalArgs)}
            />
        );
    }

    changeMarket = (market: string) => {
        const currLocation = this.props.location.hash;
        const args = qs.parse(currLocation);
        let newLocation = currLocation.replace(`market=${args.market}`, `market=${market}`);

        if (args.p) {
            if (args.p == AppPages.portfolio) {
                let useDefault: boolean = true;

                if (args.view) {
                    if (args.view == ViewType.strategyFeed) {
                        newLocation = `#p=${AppPages.portfolio}&market=${market}&view=${ViewType.strategyFeed}`;
                        useDefault = false;
                    }
                }
                
                if (useDefault) 
                    newLocation = `#p=${AppPages.portfolio}&market=${market}&view=${ViewType.alphaFeed}`;
            }
            else if (args.p == AppPages.developer)
                newLocation = `#p=${AppPages.developer}&market=${market}`;
        }

        User.current.saveSetting("global.market", market);

        this.setState({
            market: market
        });

        // let newLocation = `#p=${this.state.activePage}&market=${market}`;
        console.log(`[MainNav] Going to location: ${newLocation}`);
        this.props.history.push(newLocation);
    }

    renderMarketItem(minfo: MarketInfo) {
        return (
            <MenuItem 
                primaryText={minfo.title} 
                checked={this.market == minfo.name}
                onClick={() => this.changeMarket(minfo.name)}
                // href={`#p=${this.state.activePage}&market=${minfo.name}`}
            />
        );
    }

    renderAssetClass(category: string, items: MarketInfo[]) {
        return (
            <MenuItem 
                primaryText={category}
                rightIcon={<ArrowDropRight />}
                menuItems={this.renderAssetClassItems(items)}
            />
        );
    }

    renderAssetClassItems(items: MarketInfo[]) {
        let menuItems = [];

        items.map((minfo: MarketInfo) => {
            menuItems.push(this.renderMarketItem(minfo));
        });

        return menuItems;
    }

    renderMarketItems() {
        let menuItems = [];
        let assetClasses = {};

        gMarkets.map((minfo: MarketInfo) => {
            if (!assetClasses[minfo.assetClass]) {
                assetClasses[minfo.assetClass] = [];
            }

            assetClasses[minfo.assetClass].push(minfo);
            // menuItems.push(this.renderMarketItem(minfo));
        });

        for (let assetClass in assetClasses) { 
            menuItems.push(this.renderAssetClass(assetClass, assetClasses[assetClass]));
        }

        return menuItems;
    }

    renderNavButtons() {
        return (
            <div>
                {this.renderTitleButton(AppPages.home)}
                {this.renderTitleButton(AppPages.portfolio, { view: ViewType.alphaFeed })}
                {this.renderTitleButton(AppPages.developer)}
                {this.renderTitleButton(AppPages.system)}
                {this.renderTitleButton(AppPages.blog)} 

                <FlatButton 
                    label={name} 
                    hoverColor="#001f3f" 
                    style={notificationsButtonStyles} 
                    icon={<NotificationIcon />}
                />

                <IconMenu
                    iconButtonElement={
                        <FlatButton 
                            style={notificationsButtonStyles} 
                            icon={<MoreVertIcon />}
                        />
                    }
                    targetOrigin={{ horizontal: 'left', vertical: 'top' }}
                    anchorOrigin={{ horizontal: 'left', vertical: 'bottom' }}
                >
                    <MenuItem primaryText="Settings" />
                    <MenuItem 
                        primaryText="Market"
                        rightIcon={<ArrowDropRight />}
                        menuItems={this.renderMarketItems()}
                    />

                    <Divider />
                    
                    <MenuItem primaryText="Logout" onClick={this.onLogout} />

                </IconMenu>

            </div>
        );
    }

    onLogout = () => {
        if (this.props.onLogout)
            this.props.onLogout();
    }

    render() {
        return (
            <div className="desktop-app">
                <AppBar
                    title={this.getTitle()}
                    showMenuIconButton={false}
                >
                    {this.renderNavButtons()}
                </AppBar>

                 {this.getActivePage()}
            </div>
        );
    }
}
