import React from 'react';
import { Route, withRouter, RouteComponentProps } from 'react-router';

export interface State {
}

export interface Props extends RouteComponentProps<any>, React.Props<any> {
}

export default class Docs extends React.Component<Props, State> {
    constructor(props: Props) {
        super(props);
    }

    render() {
        return (
            <div>Hello Docs!</div>
        );
    }
}
