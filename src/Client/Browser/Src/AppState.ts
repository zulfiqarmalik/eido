export const enum AppPages {
    home        = "home",
    portfolio   = "portfolio",
    developer   = "developer",
    docs        = "docs",
    blog        = "blog",
    system      = "system",
}

export class AppState {
}
