import React from 'react';

import Login from './Routes/Login'
import { User } from 'Client/Model/User/ClUser';

import MainNav from "./Routes/MainNav";
import { RouteComponentProps } from "react-router-dom";
import { Cl, ClientUI } from 'Client/Core/ClCore';

import OKCancel from './Common/Dlg/OKCancel';
import Alert from './Common/Dlg/Alert';
import { CObject } from 'Shared/Core/CObject';

import RefreshIndicator from 'material-ui/RefreshIndicator';

import { SnackbarProvider, withSnackbar, InjectedNotistackProps } from 'notistack';

export interface State { 
    stratCategories         : string[];
    children                : any[];
    isLoggedIn              : boolean;
    isLoggingIn             : boolean;

    title                   : string;
    message                 : string;
    showAlert               : boolean;
    showOKCancel            : boolean;

    loading                 : boolean;
    loadingState            : any;

    onOk                    : () => void;
    onCancel                : () => void;
}

export interface Props extends RouteComponentProps<any>, React.Props<any> {
}

export default class BrowserApp extends React.Component<Props, State> implements ClientUI {
    container       : any = null;

    constructor(props: Props) {
        super(props);

        Cl.ui = this;
        CObject.RESTErrorCallback = this.onRESTError;
        
        this.state = {
            stratCategories: [],
            children: [],
            isLoggedIn: false,
            isLoggingIn: true,
            showAlert: false,
            showOKCancel: false,
            title: "",
            message: "",
            onOk: null,
            onCancel: null,
            loading: false,
            loadingState: "loading"
        };
    }

    isLoading() {
        return this.state.loading;
    }

    setLoading(loading: boolean, loadingState: any = "loading") {
        this.setState({
            loading: loading,
            loadingState: loading ? loadingState : 'ready'
        });
    }

    onRESTError = (err: any) => {
        let res = err.response;

        if (res && res.data && res.data.message) {
            this.hintError("Server Error", res.data.message);
        }
        else {
            this.alert("Generic Server Error", `Server threw a generic error with status: ${res.statusText} [Code: ${res.statusCode}]. Response Text: ${res.request.responseText}`);
        }
    }

    hideDlg() {
        this.setState({
            showAlert: false,
            showOKCancel: false,
            onOk: null,
            onCancel: null
        });
    }

    onOk = () => {
        let callback = this.state.onOk;
        this.hideDlg();

        if (callback)
            callback();
    }

    onCancel = () => {
        let callback = this.state.onCancel;
        this.hideDlg();

        if (callback)
            callback();
    }

    alert(title: string, message: string) {
        this.setState({
            showAlert: true,
            title: title,
            message: message
        });
    }

    okCancel(title: string, message: string, onOk?: () => void, onCancel?: () => void) {
        this.setState({
            showOKCancel: true,
            title: title,
            message: message,
            onOk: onOk,
            onCancel: onCancel
        });
    }

    hintSuccess(title: string, message: string) {
        // if (!this.container)
        //     return;

        // this.container.success(
        //      message, title, {
        //         timeOut: 500,
        //         extendedTimeOut: 500,
        //         showAnimation: 'animated fadeIn',
        //         hideAnimation: 'animated fadeOut'
        // });

        // this.props.enqueueSnackbar(message, { variant: 'success' })
    }

    hintError(title: string, message: string) {
        // if (!this.container)
        //     return;

        // this.container.error(
        //      message, title, {
        //         timeOut: 5000,
        //         extendedTimeOut: 1000,
        //         showAnimation: 'animated fadeIn',
        //         hideAnimation: 'animated fadeOut'
        // });
        // this.props.enqueueSnackbar(message, { variant: 'error' })
    }
    
    checkUser() {
        console.log("[ClApp] Auto logging in ...");
        return User.autoLogin();
    }

    onLogin = () => {
        this.setState({
            isLoggedIn: true,
            isLoggingIn: false
        });

        // this.getAllStrategies();
    }

    onLogout = () => {
        if (User.current) {
            User.current.logout();
        }

        this.setState({
            isLoggedIn: false,
            isLoggingIn: false
        });
    }

    componentDidMount() {
        console.log("[ClApp] Initialising Application!");
        this.checkUser()
            .then((user) => {
                this.setState({
                    isLoggedIn: true
                });

                // return this.getAllStrategies();
            })
            .catch((err) => {
                console.error(err);

                if (!User.user) {
                    this.setState({
                        isLoggedIn: false,
                        isLoggingIn: false
                    });

                    this.props.history.replace("/dk/login");
                }
            });
    }

    renderAlert() {
        return (
            <Alert open={this.state.showAlert} onOk={this.onOk} title={this.state.title} message={this.state.message} modal={false} />
        )
    }

    renderOKCancel() {
        return (
            <OKCancel open={this.state.showOKCancel} onOk={this.onOk} onCancel={this.onCancel} title={this.state.title} message={this.state.message} />
        )
    }

    // renderToaster() {
    //     var ReactToastr = require("react-toastr");
    //     var {ToastContainer} = ReactToastr; // This is a React Element.
    //     var ToastMessageFactory = React.createFactory(ReactToastr.ToastMessage.animation);

    //     return (
    //         <ToastContainer 
    //             ref={(input) => { this.container = input; }}
    //             toastMessageFactory={ToastMessageFactory}
    //             className="toast-bottom-right" 
    //         />
    //     );
    // }

    renderLoadingIndicator() {
        return (
            <div className={this.state.loading ? "" : "hidden"}>
                <RefreshIndicator
                    size={100}
                    left={0}
                    top={0}
                    status={this.state.loadingState}
                    loadingColor='cyan'
                    style={{
                        marginLeft: '50%', 
                        marginTop: '25%', 
                        position: 'absolute' as any, 
                        visibility: `${this.state.loadingState === 'loading' && this.state.loading === true ? 'visible' : 'hidden'}` as any
                    }}
                />            
            </div>
        );
    }

    renderMainNav() {
        return (
            <SnackbarProvider maxSnack={3}
                anchorOrigin={{vertical: 'top', horizontal: 'right'}}
            >
                <div className="desktop-app">
                    {this.renderAlert()}
                    {this.renderOKCancel()}
                    {/* {this.renderToaster()} */}

                    <MainNav 
                        onLogout={this.onLogout} 
                        location={this.props.location} 
                        history={this.props.history}
                        match={this.props.match}
                    />
                </div>
            </SnackbarProvider>
        );
    }

    renderLogin() {
        return (
            <Login 
                location={this.props.location}
                match={this.props.match}
                history={this.props.history}
                onLogin={this.onLogin} 
            />
        );
    }

    render() {
        if (this.state.isLoggedIn || this.state.isLoggingIn)
            return this.renderMainNav();

        return this.renderLogin();
    }
}

// export default withSnackbar(BrowserApp);
