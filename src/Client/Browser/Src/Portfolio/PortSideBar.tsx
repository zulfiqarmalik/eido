import React from 'react';

import { ObjectID } from "Shared/Core/ObjectID";

import { RouteComponentProps } from 'react-router';

import ContentAdd from 'material-ui/svg-icons/content/add';
import FloatingActionButton from 'material-ui/FloatingActionButton';
import PortMenuMain from './PortMenuMain';
import {Toolbar, ToolbarTitle} from 'material-ui/Toolbar';
import { Alpha } from 'Client/Model/Config/ClAlpha';
import { User } from 'Client/Model/User/ClUser';
import TextField from 'material-ui/TextField';
import { AlphaOrStrategyInfo } from 'Client/Model/Config/ClConfigBase';
import { ViewType } from './PortDefs';

// const tooltip = (
//     <Tooltip id='tooltip'><strong>Holy guacamole!</strong> Check this info.</Tooltip>
// );

// function TT(msg, title) {
//     if (title)
//         return <Tooltip ><strong>{title}</strong>{msg}</Tooltip>
//     return <Tooltip >{msg}</Tooltip>
// }

interface Props extends RouteComponentProps<any>, React.Props<any> {
    key             ?: string;
    strategyInfo    : AlphaOrStrategyInfo;
    strategyId      : ObjectID;
    view            : ViewType;
    market          : string;
    createStrategy  : () => void;
}

interface State {
    unpublishedAlphas: Alpha[];
    publishedAlphas : Alpha[];
    allowEditing    : boolean;
    allowGroupPnl   : boolean;
    isEditing       : boolean;
    strategyInfo    : AlphaOrStrategyInfo;
}
    
export default class PortSideBar extends React.Component<Props, State> {
    constructor(props: Props) {
        super(props);

        this.state = {
            unpublishedAlphas: [],
            publishedAlphas: [],
            allowEditing: false,
            allowGroupPnl: false,
            isEditing: false,
            strategyInfo: this.props.strategyInfo
        };

    }

    componentDidMount() {
        let user = User.user;
        if (user && user.isAdminOrBetter()) {
            this.setState({
                allowEditing: true,
                allowGroupPnl: true
            });
        }
        else {
            this.setState({
                allowEditing: false,
                allowGroupPnl: false
            });
        }
    }

    componentDidUpdate() {
        if (this.state.isEditing) {
            this.setState({
                isEditing: false
            });
        }
    }

    onSearch = (event: any, value: string) => {
        if (!value) {
            /// This will clear out the search string in strategyInfo ...
            this.props.strategyInfo.search(value);

            this.setState({
                strategyInfo: this.props.strategyInfo
            });
            return;
        }

        let searchAlphas = this.props.strategyInfo.search(value);
        this.setState({
            strategyInfo: new AlphaOrStrategyInfo(this.props.strategyInfo.market, searchAlphas)
        });
    }

    
    render() {
        return (
            <div className="full-side-bar">
                <Toolbar>
                    <ToolbarTitle text={"Alpha Feed & Strategies"} className="dev-alpha-title" />
                </Toolbar>

                <TextField
                    hintText="Search ..." 
                    style={{width: 270, marginLeft: 20}}
                    value={this.props.strategyInfo.searchString}
                    onChange={this.onSearch}
                />

                <PortMenuMain
                    key={`PortMenuMain-${this.props.strategyInfo.searchString}-${this.props.strategyInfo.length}`}
                    location={this.props.location}
                    history={this.props.history}
                    match={this.props.match}
                    strategyInfo={this.state.strategyInfo}
                    strategyId={this.props.strategyId}
                    view={this.props.view}
                    market={this.props.market}
                />

                <FloatingActionButton className="dev-create-alpha" secondary={true} onClick={ () => { if (this.props.createStrategy) this.props.createStrategy(); } }>
                    <ContentAdd />
                </FloatingActionButton>
            </div>
        );
    }
}
