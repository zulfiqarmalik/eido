import React from 'react';

import { RouteComponentProps } from 'react-router';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';

import { Alpha } from 'Client/Model/Config/ClAlpha';

import ViewAlphaResults from '../Common/ViewAlphaResults';
import { AlphaOrStrategyInfo } from 'Client/Model/Config/ClConfigBase';

interface Props extends RouteComponentProps<any>, React.Props<any> {
    onOk        : () => void;
    open        : boolean;
    alphaInfo   : AlphaOrStrategyInfo;
    alpha       : Alpha;
    market      : string;
}

interface State {
    name        : string;
    id          : string;
    alphaInfo   : AlphaOrStrategyInfo;
}
    
export default class PortShowAlphaDlg extends React.Component<Props, State> {
    constructor(props: Props) {
        super(props);

        this.state = {
            id: this.props.alpha ? this.props.alpha._id.toString() : "",
            name: this.props.alpha ? this.props.alpha.name : "",
            alphaInfo: new AlphaOrStrategyInfo(this.props.market, [])
        };

        if (this.props.alpha)
            this.state.alphaInfo.add(this.props.alpha);
    }

    componentDidMount() {
    }

    getActions() {
        return [
            <FlatButton
                label="Close"
                secondary={true}
                keyboardFocused={true}
                onClick={this.props.onOk}
            />,
          ];
    }

    render() {
        return (
            <Dialog
                title={`${this.state.name} - ${this.state.id}`}
                actions={this.getActions()}
                modal={false}
                open={this.props.open}
                onRequestClose={this.props.onOk}
                autoDetectWindowHeight={true}
                autoScrollBodyContent={true}
                repositionOnUpdate={true}
                // style={{maxHeight: '200%'}}
                contentStyle={{width: '80%', maxWidth: 'none'}}
                contentClassName="v-scrollable"
                className="v-scrollable"
            >
                <ViewAlphaResults
                    location={this.props.location}
                    match={this.props.match}
                    history={this.props.history}
                    alphaId={this.props.alpha ? this.props.alpha._id : null}
                    alphaInfo={this.props.alphaInfo}
                    isEmbedded={true}
                />
            </Dialog>
        );
    }
}
