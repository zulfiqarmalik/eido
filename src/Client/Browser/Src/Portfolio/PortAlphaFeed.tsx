import React from 'react';
import { RouteComponentProps } from 'react-router';

import PortAlphaFeedItem from './PortAlphaFeedItem';

import {Toolbar, ToolbarGroup} from 'material-ui/Toolbar';
import UniverseSelect from '../Common/UniverseSelect';

import PortShowAlphaDlg from './PortShowAlphaDlg';
import AlphaSort from '../Common/AlphaSort';
import { Alpha } from 'Client/Model/Config/ClAlpha';
import RefreshIndicator from 'material-ui/RefreshIndicator';
import ConfigTypeSelect from '../Common/ConfigTypeSelect';
import { ConfigType, ShAlpha, AlphaType } from 'Shared/Model/Config/ShAlpha';
import DelaySelect from '../Common/DelaySelect';
import { User } from 'Client/Model/User/ClUser';
import { SortOrder } from 'Shared/Model/ShModel';
import SortOrderSelect from '../Common/SortOrder';
import { AlphaFeed } from 'Shared/Model/Config/AlphaFeed';
import NumberSelect from '../Common/NumberSelect';
import { AlphaOrStrategyInfo, AlphaOrStrategy } from 'Client/Model/Config/ClConfigBase';
import { Port } from './Port';
import IconButton from 'material-ui/IconButton';
import FontIcon from 'material-ui/FontIcon';

import SearchIcon from 'material-ui/svg-icons/action/search';

import * as Colors from 'material-ui/styles/colors';
import SearchViewType from '../Common/SearchViewType';
import IconMenu from 'material-ui/IconMenu';
import FlatButton from 'material-ui/FlatButton';
import Popover from 'material-ui/Popover';
import SearchSettings, { AdvSearchOptions } from '../Common/SearchSettings';

const mediumIcon = {
    width: 64,
    height: 64,
};

interface Props extends RouteComponentProps<any>, React.Props<any> {
    view                ?: string;
    key                 ?: string;
    market              : string;
    strategyInfo        : AlphaOrStrategyInfo;
    createStrategy      : () => void;
    type                : AlphaType;
    hideDelay           ?: boolean;
    hideType            ?: boolean;
    prefix              : string;

    portView            : Port;
}

interface State {
    pageId              : number;
    ppCount             : number;
    numPages            : number;
    universeId          : string;
    feed                : AlphaFeed<AlphaOrStrategy>;
    alphaInfo           : AlphaOrStrategyInfo;
    currAlpha           : Alpha;
    showAlphaDlg        : boolean;

    sortBy              : string;
    sortOrder           : SortOrder;
    configType          : ConfigType;
    delay               : number;
    loadingState        : string;
    bottomToolbar       : boolean;

    searchView          : string;
    openAdvSearch       : boolean;
    advSearches         : any[];
}
    
export default class PortAlphaFeed extends React.Component<Props, State> {
    constructor(props: Props) {
        super(props);

        this.state = {
            pageId: 0,
            ppCount: 20,
            numPages: 1,
            universeId: "ALL",
            feed: new AlphaFeed<AlphaOrStrategy>(this.props.market),
            alphaInfo: new AlphaOrStrategyInfo(this.props.market, null),
            currAlpha: null,
            showAlphaDlg: false,

            sortBy: "ir",
            sortOrder: SortOrder.desc,
            loadingState: 'loading',
            delay: 1,
            configType: ConfigType.any,
            searchView: "LT",
            bottomToolbar: false,

            openAdvSearch: false,
            advSearches: []
        };
    }

    componentDidMount() {
        if (!User.current)
            return;

        User.current.loadSettings(
            [`${this.props.prefix}.feed.universeId`, `${this.props.prefix}.feed.sortBy`, `${this.props.prefix}.feed.sortOrder`, `${this.props.prefix}.feed.delay`, 
            `${this.props.prefix}.feed.configType`, `${this.props.prefix}.feed.pageId`, `${this.props.prefix}.feed.searchView`, `${this.props.prefix}.feed.advSearches`], 
            [this.state.universeId, this.state.sortBy, this.state.sortOrder, `${this.state.delay}`, this.state.configType, this.state.pageId.toString(), this.state.searchView, JSON.stringify(this.state.advSearches)])
                .then((values) => {
                    let universeId: string  = values[0];
                    let sortBy: string      = values[1];
                    let sortOrder: SortOrder= values[2] as SortOrder;
                    let delay: number       = parseInt(values[3]);
                    let configType: ConfigType = values[4] as ConfigType;
                    let pageId: number      = parseInt(values[5]);
                    let searchView: string  = values[6];
                    let advSearches: any[]  = values[7] ? JSON.parse(values[7]) : [];

                    this.setState({
                        universeId: universeId,
                        sortBy: sortBy,
                        sortOrder: sortOrder,
                        delay: delay,
                        configType: this.props.type == AlphaType.alpha ? configType : ConfigType.any,
                        pageId: pageId,
                        searchView: searchView,
                        advSearches: advSearches
                    });

                    this.updateAlphaFeed(universeId, sortBy, sortOrder, configType, delay, pageId, searchView, advSearches);
                });
    }

    onUniverseChange = (universeId: string) => {
        /// We reset the page as the universe is being reset
        this.setState({
            loadingState: 'loading',
            pageId: 0,
            universeId: universeId
        });

        User.current.saveSetting(`${this.props.prefix}.feed.universeId`, universeId);

        this.updateAlphaFeed(universeId, this.state.sortBy, this.state.sortOrder, this.state.configType, this.state.delay, 0, this.state.searchView, this.state.advSearches);
    }

    onSortChange = (sortBy: string) => {
        this.setState({
            sortBy: sortBy,
            pageId: 0,
            loadingState: 'loading'
        });

        User.current.saveSetting(`${this.props.prefix}.feed.sortBy`, sortBy);

        this.updateAlphaFeed(this.state.universeId, sortBy, this.state.sortOrder, this.state.configType, this.state.delay, 0, this.state.searchView, this.state.advSearches);
    }

    onSearchViewChange = (searchView: string) => {
        this.setState({
            searchView: searchView,
            pageId: 0,
            loadingState: 'loading'
        });

        User.current.saveSetting(`${this.props.prefix}.feed.searchView`, searchView);

        this.updateAlphaFeed(this.state.universeId, this.state.sortBy, this.state.sortOrder, this.state.configType, this.state.delay, 0, searchView, this.state.advSearches);
    }

    onSortOrderChange = (sortOrder: SortOrder) => {
        this.setState({
            sortOrder: sortOrder,
            pageId: 0,
            loadingState: 'loading',
        });

        User.current.saveSetting(`${this.props.prefix}.feed.sortOrder`, sortOrder);

        this.updateAlphaFeed(this.state.universeId, this.state.sortBy, sortOrder, this.state.configType, this.state.delay, 0, this.state.searchView, this.state.advSearches);
    }

    onConfigTypeChange = (configType: ConfigType) => {
        this.setState({
            configType: configType,
            pageId: 0,
            loadingState: 'loading',
        });

        User.current.saveSetting(`${this.props.prefix}.feed.configType`, configType);

        this.updateAlphaFeed(this.state.universeId, this.state.sortBy, this.state.sortOrder, configType, this.state.delay, 0, this.state.searchView, this.state.advSearches);
    }

    onDelayChange = (delay: number) => {
        this.setState({
            delay: delay,
            pageId: 0,
            loadingState: "loading"
        });

        User.current.saveSetting(`${this.props.prefix}.feed.delay`, `${delay}`);

        this.updateAlphaFeed(this.state.universeId, this.state.sortBy, this.state.sortOrder, this.state.configType, delay, 0, this.state.searchView, this.state.advSearches);
    }

    updateAlphaFeed(universeId: string, sortBy: string, sortOrder: SortOrder, configType: ConfigType, delay: number, pageId: number, searchView: string, advSearches) {
        let dbName: string = ShAlpha.getDbName(this.props.market, configType);

        let startIndex: number = pageId * this.state.ppCount;

        AlphaOrStrategy.getFeed(null, this.props.market, universeId, startIndex, this.state.ppCount, sortBy, sortOrder, dbName, configType, this.props.type, delay, searchView, advSearches)
            .then((feed: AlphaFeed<AlphaOrStrategy>) => {
                console.log(`[Feed] Number of alphas for the feed: ${feed.count}`); 
                let numPages: number = Math.ceil(feed.totalCount / this.state.ppCount);

                this.setState({
                    feed: feed,
                    numPages: numPages,
                    bottomToolbar: feed.totalCount >= 4,
                    alphaInfo: new AlphaOrStrategyInfo(feed.market, feed.alphas),
                    loadingState: 'ready'
                });
            });
    }

    onPageChange = (value: number) => {
        value = Math.max(value - 1, 0);

        this.setState({
            pageId: value,
            loadingState: "loading"
        });

        User.current.saveSetting(`${this.props.prefix}.feed.pageId`, value.toString());

        this.updateAlphaFeed(this.state.universeId, this.state.sortBy, this.state.sortOrder, this.state.configType, this.state.delay, value, this.state.searchView, this.state.advSearches);
    }

    onQuickShowAlpha = (alpha: Alpha) => {
        console.log(`[Feed] Showing alpha: ${alpha._id}`);
        this.setState({
            currAlpha: alpha,
            showAlphaDlg: true
        });
    }

    getAlphaKey(alpha) {
        return `${this.props.market}_${this.state.universeId}_${alpha._id}_${this.props.strategyInfo.length}`;
    }

    getKey(prefix: string): string {
        return `${prefix}-${this.state.sortBy}-${this.state.sortOrder}-${this.state.delay}-${this.state.configType}-${this.state.numPages}-${this.state.pageId}`;
    }

    renderUniverseSelect(toolbarType: string) {
        return (
            <UniverseSelect 
                label="Search Universe"
                key={this.getKey(`Universe-${toolbarType}`)}
                market={this.props.market} 
                universeId={this.state.universeId} 
                onUniverseChange={this.onUniverseChange} 
                extraUniverses={["ALL"]}
                readOnly={false}
            />
        );
    }

    renderSearchViewType(toolbarType: string) {
        return (
            <SearchViewType
                label="Search View"
                key={this.getKey(`SearchView-${toolbarType}`)}
                onSearchViewChange={this.onSearchViewChange}
                searchView={this.state.searchView} 
            />
        );
    }

    renderAlphaSort(toolbarType: string) {
        return (
            <AlphaSort
                label="Sort"
                key={this.getKey(`Sort-${toolbarType}`)}
                onSortChange={this.onSortChange}
                sortBy={this.state.sortBy} 
            />
        );
    }

    renderSortOrder(toolbarType: string) {
        return (
            <SortOrderSelect
                label="Order"
                key={this.getKey(`Order-${toolbarType}`)}
                onSortOrderChange={this.onSortOrderChange}
                sortOrder={this.state.sortOrder} 
            />
        );
    }

    renderConfigType(toolbarType: string) {
        if (!this.props.hideType) {
            return (
                <ConfigTypeSelect
                    label="Type"
                    key={this.getKey(`Type-${toolbarType}`)}
                    onConfigTypeChange={this.onConfigTypeChange}
                    configType={this.props.type == AlphaType.alpha ? this.state.configType : ConfigType.any}
                    readOnly={this.props.type != AlphaType.alpha}
                />
            );
        }
        return (<span/>);
    }

    renderDelay(toolbarType: string) {
        if (!this.props.hideDelay) {
            return (
                <DelaySelect
                    label="Delay"
                    key={this.getKey(`Delay-${toolbarType}`)}
                    onDelayChange={this.onDelayChange}
                    delay={this.state.delay}
                />
            );
        }
        return (<span/>);
    }

    onNextPage = () => {
        if (this.state.pageId >= this.state.numPages - 1)
            return;

        /// +2 is because we are going to the next page (+1) and the pageId internally 
        /// is 0 indexed, but the UI is 1 indexed (+1 to ensure that UI is in sync)
        this.onPageChange(this.state.pageId + 2);
    }

    renderNextPage(toolbarType: string) {
        return (
            <IconButton tooltip="Next Page" style={mediumIcon} onClick={this.onNextPage}>
                <FontIcon color={Colors.grey500} className="material-icons">play_arrow</FontIcon
            ></IconButton>
        );
    }

    renderPageId(toolbarType: string) {
        return (
            <NumberSelect
                label="Page"
                key={this.getKey(`Page-${toolbarType}-${this.state.pageId}`)}
                onNumberSelectChange={this.onPageChange}
                start={1}
                end={this.state.numPages + 1}
                incr={1}
                value={this.state.pageId + 1}
                prefix={""}
                suffix={""}
            />
        );
    }

    renderToolbar(toolbarType: string) {
        return (
            <Toolbar>
                <ToolbarGroup>
                    {this.renderUniverseSelect(toolbarType)}
                    &nbsp;

                    {this.renderAlphaSort(toolbarType)}
                    &nbsp;

                    {this.renderSortOrder(toolbarType)}
                    &nbsp;

                    {/* {this.renderSearchViewType(toolbarType)}
                    &nbsp;

                    {this.renderConfigType(toolbarType)}
                    &nbsp; */}

                    {this.renderDelay(toolbarType)}
                    &nbsp;

                    {this.renderPageId(toolbarType)}
                    {this.renderNextPage(toolbarType)}
                    {this.renderAdvancedSearch(toolbarType)}
                </ToolbarGroup>
            </Toolbar>
        );
    }

    renderLoadingIndicator() {
        return (
            <div className={this.state.loadingState == "loading" ? "" : "hidden"}>
                <RefreshIndicator
                    size={100}
                    left={0}
                    top={0}
                    status={this.state.loadingState as any}
                    loadingColor='cyan'
                    style={{marginLeft: '50%', marginTop: '25%', position: 'relative' as any, visibility: `${this.state.loadingState === 'loading' ? 'visible' : 'hidden'}` as any}}
                />            
            </div>
        );
    }

    renderAlphas() {
        return (
            <div className={this.state.loadingState != "loading" ? "" : "hidden"}>
                { 
                    this.state.alphaInfo.alphas.map((alpha, ai) => { 
                        return (
                            <PortAlphaFeedItem 
                                location={this.props.location}
                                match={this.props.match}
                                history={this.props.history}
                                key={this.getAlphaKey(alpha)} 
                                alpha={alpha as Alpha} 
                                strategyInfo={this.props.strategyInfo}
                                strategy={null}
                                createStrategy={this.props.createStrategy}
                                onQuickShowAlpha={this.onQuickShowAlpha} 
                                dbName={alpha.containerId}
                                addText={this.props.type == AlphaType.alpha ? "Add To Strategy" : "Add To MASTER"}
                                disableCreateNew={this.props.type == AlphaType.strategy}
                                portView={this.props.portView}
                            />
                        );
                    }) 
                }
            </div>
        );
    }

    renderAlphaDlg() {
        return (
            <PortShowAlphaDlg
                key={this.state.currAlpha ? this.state.currAlpha._id.toString() : "PortShowDlg"}
                location={this.props.location}
                match={this.props.match}
                history={this.props.history}
                market={this.props.market}
                onOk={() => this.setState({showAlphaDlg: false})}
                alphaInfo={this.state.alphaInfo}
                alpha={this.state.currAlpha}
                open={this.state.showAlphaDlg}
            />
        );
    }

    renderTopToolbar() {
        return this.renderToolbar("Top");
    }
    
    renderBottomToolbar() {
        if (!this.state.bottomToolbar)
            return (<div/>);
        return this.renderToolbar("Bottom");
    }

    onCloseAdvSearch = () => {
        this.setState({
            openAdvSearch: false
        });
    }

    onAdvSearchOpen = (event) => {
        this.setState({
            openAdvSearch: true
        });
    }

    onSearchSettingsChanged = (opt: AdvSearchOptions) => {
        this.setState({
            searchView: opt.searchView,
            configType: opt.configType,
            advSearches: opt.advSearches,
            pageId: 0,
            openAdvSearch: false,
            loadingState: "loading"
        });

        User.current.saveSetting(`${this.props.prefix}.feed.configType`, opt.configType);
        User.current.saveSetting(`${this.props.prefix}.feed.searchView`, opt.searchView);
        User.current.saveSetting(`${this.props.prefix}.feed.advSearches`, JSON.stringify(opt.advSearches));

        this.updateAlphaFeed(this.state.universeId, this.state.sortBy, this.state.sortOrder, opt.configType, this.state.delay, 0, opt.searchView, opt.advSearches);
    }

    renderSearchSettings() {
        return (
            <SearchSettings
                key={`SearchSettings-${this.state.configType}-${this.state.searchView}-${this.state.advSearches.length}`}
                open={this.state.openAdvSearch}
                onCancel={this.onCloseAdvSearch}
                searchView={this.state.searchView}
                hideType={this.props.hideType}
                configType={this.state.configType}
                onOk={this.onSearchSettingsChanged}
                advSearches={this.state.advSearches}
            />
        );
    }

    renderAdvancedSearch(toolbarType: string) {
        return (
            <IconButton 
                tooltip="Search Settings" 
                style={mediumIcon} 
                onClick={this.onAdvSearchOpen}
            >
                <FontIcon color={!this.state.advSearches.length ? Colors.deepOrange500 : Colors.yellow500} className="material-icons">build</FontIcon
            ></IconButton>
        );
    }
    
    render() {
        return (
            <div className="port-feed">
                {this.renderTopToolbar()}
                {this.renderAlphas()}
                {this.renderAlphaDlg()}
                {this.renderLoadingIndicator()}
                {this.renderBottomToolbar()}
                {this.renderSearchSettings()}
            </div>
        );
    }
}

