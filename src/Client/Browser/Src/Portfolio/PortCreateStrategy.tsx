import React, { ChangeEvent } from 'react';

import { RouteComponentProps } from 'react-router';
import TextField from 'material-ui/TextField';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import Checkbox from 'material-ui/Checkbox';

import UniverseSelect from '../Common/UniverseSelect';
import { DefUniverse, getRefConfigs, RefConfigInfo } from 'Shared/Model/Config/ShAlpha';
import StrategyRefConfigSelect from '../Common/StrategyRefConfigSelect';
import { Debug } from 'Shared/Core/ShUtil';
import DelaySelect from '../Common/DelaySelect';

export class StrategyCreateInfo {
    category        : string;
    name            : string;
    market          : string;
    refConfigfilename: string
    universeId      : string;
    isMaster        : boolean;
    delay           : number;
}

interface Props extends RouteComponentProps<any>, React.Props<any> {
    onOk            : (sci: StrategyCreateInfo) => void;
    onCancel        : () => void;
    canCancel       : boolean;
    open            : boolean;
    market          : string;
}

interface State {
    name            : string;
    category        : string;
    isMaster        : boolean;
    universeId      : string;
    refConfigs      : RefConfigInfo[];
    refConfigfilename: string;
    delay           : number;
}
    
export default class PortCreateStrategy extends React.Component<Props, State> {
    constructor(props: Props) {
        super(props);

        let universeId = DefUniverse;
        let refConfigs = getRefConfigs(this.props.market, universeId);
        Debug.assert(refConfigs.length, `Unable to get any ref configs for: ${this.props.market}@${universeId}`);

        this.state = {
            name: "MyStrategy",
            category: "Portfolio",
            isMaster: false,
            universeId: DefUniverse,
            refConfigs: refConfigs,
            refConfigfilename: refConfigs[0].filename,
            delay: 1
        }
    }

    componentDidMount() {
    }

    onOk = () => {
        let sci = new StrategyCreateInfo();
        sci.market = this.props.market;
        sci.category = this.state.category;
        sci.name = this.state.name;
        sci.refConfigfilename = this.state.refConfigfilename;
        sci.universeId = this.state.universeId;
        sci.isMaster = this.state.isMaster;
        sci.delay = this.state.delay;

        this.props.onOk(sci);
    }

    onCancel = () => {
        this.props.onCancel();
    }

    onClose = () => {
        this.props.onCancel();
    }

    onNameChange = (e: any, name: string) => {
        this.setState({
            name: name
        });
    }

    onCategoryChange = (e: any, category: string) => {
        this.setState({
            category: category
        });
    }

    onUniverseChange = (universeId: string) => {
        let refConfigs = getRefConfigs(this.props.market, universeId);
        Debug.assert(refConfigs.length, `Unable to get any ref configs for: ${this.props.market}@${universeId}`);

        this.setState({
            universeId: universeId,
            refConfigs: refConfigs,
            refConfigfilename: refConfigs[0].filename
        });
    }

    onRefConfigChange = (refConfigfilename: string) => {
        this.setState({
            refConfigfilename: refConfigfilename
        });
    }

    getActions() {
        return [
            <FlatButton
                label="Ok"
                secondary={true}
                keyboardFocused={true}
                onClick={this.onOk}
            />,

            <FlatButton
                label="Cancel"
                primary={true}
                keyboardFocused={false}
                onClick={this.onCancel}
            />,
          ];
    }

    onMasterToggle = (e: ChangeEvent<{}>, value?: boolean) => {
        this.setState({ isMaster: value });
    }

    onDelayChange = (delay: number) => {
        this.setState({
            delay: delay
        });
    }

    render() {
        return (
            <Dialog
                title="Create your Strategy"
                actions={this.getActions()}
                modal={true}
                open={this.props.open}
                onRequestClose={this.onClose}
            >
                <UniverseSelect
                    onUniverseChange={this.onUniverseChange}
                    universeId={this.state.universeId}
                    market={this.props.market}
                    readOnly={false}
                    label={"Target Universe"}
                />
                <br/>

                <DelaySelect
                    onDelayChange={this.onDelayChange}
                    delay={this.state.delay}
                    label={"Delay"}
                />
                <br/>

                <StrategyRefConfigSelect
                    key={`${this.state.universeId}-RefConfig}`}
                    label={"Basic Configuration"}
                    readOnly={false}
                    refConfigs={this.state.refConfigs}
                    onRefConfigChange={this.onRefConfigChange}
                />
                <br/>

                <TextField
                    defaultValue={this.state.name}
                    onChange={this.onNameChange}
                    floatingLabelText="Strategy Name"
                />
                <br/>

                <TextField
                    defaultValue={this.state.category}
                    onChange={this.onCategoryChange}
                    floatingLabelText="Strategy Category"
                />
                <br/>
                
                <Checkbox
                    label="Is Master?"
                    defaultChecked={this.state.isMaster}
                    onChange={this.onMasterToggle}
                />

            </Dialog>
        );
    }
}

