import React from 'react';
import { RouteComponentProps } from 'react-router';

import PortAlphaFeedItem from './PortAlphaFeedItem';
import { ObjectID } from "Shared/Core/ObjectID";

import {Toolbar, ToolbarGroup, ToolbarTitle} from 'material-ui/Toolbar';
import CombinationSelect from '../Common/CombinationSelect';

import PortShowAlphaDlg from './PortShowAlphaDlg';
import { Alpha } from 'Client/Model/Config/ClAlpha';
import { Strategy } from 'Client/Model/Config/ClStrategy';
import { LTStats } from 'Shared/Model/Stats/LTStats';
import RefreshIndicator from 'material-ui/RefreshIndicator';

import RaisedButton from 'material-ui/RaisedButton';
import IconButton from 'material-ui/IconButton';
import FontIcon from 'material-ui/FontIcon';
import * as Colors from 'material-ui/styles/colors';

import TextField from 'material-ui/TextField';
import LiveResults from "../Common/LiveResults";
import AlphaSettings from "../Common/AlphaSettings";
import { User } from 'Client/Model/User/ClUser';
import { AppPages } from '../AppState';
import { ViewType } from './PortDefs';
import UniverseSelect from '../Common/UniverseSelect';
import { AlphaOrStrategyInfo, AlphaOrStrategy, Category } from 'Client/Model/Config/ClConfigBase';

import CopyIcon from 'material-ui/svg-icons/content/content-copy';
import DuplicateAlphaOrStrategy from '../Common/DuplicateAlphaOrStrategy';
import { Port } from './Port';
import { Cl } from 'Client/Core/ClCore';

let RunIcon = require('material-ui-icons/PlayArrow').default;
let CancelIcon = require('material-ui-icons/Cancel').default;

const tbStyle = {
    marginLeft: 30,
};

const tbStyle2 = {
    marginRight: 150,
};

const btStyle = {
    marginRight: 10,
    marginLeft: 10
};

const smallIcon = {
    width: 36,
    height: 36,
};

const mediumIcon = {
    width: 48,
    height: 48,
};

const ttPlacement = "bottom";

interface Props extends RouteComponentProps<any>, React.Props<any> {
    key                 ?: string;
    strategyInfo        : AlphaOrStrategyInfo;
    strategyId          : ObjectID;
    market              : string;
    portView            : Port;
}

interface State {
    comboId             : string;
    isRunning           : boolean;
    alphaInfo           : AlphaOrStrategyInfo;
    showAlphaDlg        : boolean;
    showOutputWindow    : boolean;
    showPortOps         : boolean;
    logMsgs             : string[];
    liveStats           : LTStats;
    numStats            : number;
    startDate           : string;
    showSettings        : boolean;
    strategy            : Strategy;
    currAlpha           : Alpha;
    anchorEl            ?: any;

    runPercent          : number;
    subRunPercent       : number;
    superRunPercent     : number;
    
    subLiveStats        ?: LTStats;
    numSubStats         ?: number;
    superLiveStats      ?: LTStats;
    numSuperStats       ?: number;

    loadingState        : string
    modifiedBookSize    : boolean;
    bookSize            : string;
}
    
export default class PortStrategyView extends React.Component<Props, State> {
    constructor(props: Props) {
        super(props);

        let strategy = this.props.strategyInfo.findById(this.props.strategyId);

        this.state = {
            comboId: "BasicCombination_Add",
            isRunning: false,
            alphaInfo: new AlphaOrStrategyInfo(this.props.market, null),
            showAlphaDlg: false,
            showOutputWindow: false,
            showPortOps: false,
            logMsgs: [],
            runPercent: 0,
            subRunPercent: 0,
            superRunPercent: 0,
            liveStats: new LTStats(),
            numStats: 0,
            startDate: strategy ? strategy.startDate : "20120101",
            showSettings: false,
            currAlpha: null,
            strategy: strategy as Strategy,
            loadingState: "hide",
            modifiedBookSize: false,
            bookSize: strategy ? strategy.getBookSize().toExponential(2) : "0",
        };
    }

    buildAlphaInfo() {
        this.setState({
            loadingState: "loading"
        });

        if (this.state.strategy) {
            this.state.strategy.getAllAlphas()
                .then((alphas) => {
                    this.setState({
                        alphaInfo: new AlphaOrStrategyInfo(this.state.strategy.market, alphas),
                        loadingState: "hide"
                    });
                });
        }
    }

    componentDidMount() {
        this.buildAlphaInfo();
    }

    formatLogMsg(logMsg: string, i: number) {
        let type = parseInt(logMsg[0]);
        let msg = logMsg.substr(2); 
        let color = "green";

        if (type == 1 || type == 2 || type == 3)
            color = "red";
        else if (type == 4 || type == 5)
            color = "yellow";
        else if (type == 6)
            color = "silver";
        else if (type == 7)
            color = "lime";
        // else if (type == 8)
        //     color = "silver";

        return (<div key={i}><span style={{color: color}}>{msg}</span> <br/></div>);
    }
    
    appendLog(log: string[]) {
        if (log && log.length) {
            let existingLog = this.state.logMsgs;

            if (this.state.logMsgs.length > 100)
                existingLog = ["4><<< Truncated log. Use the download option to download the entire log file >>>"];

            this.setState({
                logMsgs: existingLog.concat(log)
            });
    
        }
    }

    onRunTick = (alpha: Alpha, log: string[]) => {
        this.appendLog(log);

        let runPercent = Math.round(alpha.percent * 100.0);
        
        this.setState({
            runPercent: runPercent,
            liveStats: alpha._liveStats,
            numStats: alpha._liveStatsIndex
        });

        let elem = document.getElementById('dev-output');
        elem.scrollTop = elem.scrollHeight;
    }

    onFinished = (alpha: Alpha, log: string[]) => {
        this.setState({
            isRunning: false
        });

        this.appendLog(log);
    }

    onRun = () => {
        console.log("Running the strategy ...");
        this.setState({
            showOutputWindow: true,
            isRunning: true,
            runPercent: 0,
            logMsgs: []
        });

        this.state.strategy.run(User.user.wsClient, this.onRunTick, this.onFinished)
            .catch(() => { 
                this.setState({
                    isRunning: false
                });
            });
    }
    
    onCancel = () => {
        this.setState({
            isRunning: false
        });

        this.state.strategy.cancel();
    }

    onShowOutputWindow = () => this.setState({showOutputWindow: true});
    onHideOutputWindow = () => this.setState({showOutputWindow: false});

    onQuickShowAlpha = (alpha) => {
        console.log(`[Feed] Showing alpha: ${alpha._id}`);
        this.setState({
            currAlpha: alpha,
            showAlphaDlg: true
        });
    }

    onComboChange(comboId) {
        this.setState({
            comboId: comboId
        });
    }

    getKey(alpha) {
        if (this.state.strategy && this.state.alphaInfo)
            return `${this.state.strategy._id}_${this.state.comboId}_${alpha._id}_${this.state.alphaInfo.alphas.length}`;
        return "FeedItem";
    }

    onShowPortOps = (event) => {
        // This prevents ghost click.
        // event.preventDefault();
        
        this.setState({
            showPortOps: true,
            anchorEl: event.currentTarget,
        });
    };
    
    onHidePortOps = () => {
        this.setState({
            showPortOps: false,
        });
    };

    getTitleLabel() {
        if (!this.state.strategy)
            return "";

        return `${this.state.strategy.name} : [${this.state.alphaInfo.alphas.length}]`;
    }

    onSave = (alpha: Alpha, weight: number, reversion: boolean) => {
        return this.state.strategy.updateAlphaWeight(alpha._id, weight, reversion)
            .then(() => {
                this.props.portView.successAlert("Save", "Successfully saved!");
                return this.state.strategy;
            })
            .catch((err) => {
                this.props.portView.errorAlert("Save", `Save failed: ${err}`);
                throw err;
            });
    }

    onDeleteAlpha = (alpha: Alpha) => {
        return this.state.strategy.removeAlpha(alpha)
            .then(() => {
                this.state.alphaInfo.deleteAlpha(alpha);
                this.setState({
                    alphaInfo: this.state.alphaInfo
                });
                this.props.portView.successAlert("Delete", "Successfully deleted!");
                return this.state.strategy;
                // return this.buildAlphaInfo();
                // return this.state.strategy;
            })
            .catch((err) => {
                this.props.portView.errorAlert("Delete", `Delete failed: ${err}`);
                throw err;
            });
    }

    onDelete = () => {
        
    }

    onPublish = () => {
        Cl.ui.okCancel("Publish", `Are you sure you want to publish strategy: ${this.state.strategy.name}? This is an irreversible process.`, () => {
            this.state.strategy.publish()
                .then(() => {
                    this.props.portView.successAlert("Publish", "Successfully published!");
                    return this.buildAlphaInfo();
                    // return this.state.strategy;
                })
                .catch((err) => {
                    this.props.portView.errorAlert("Publish", `Publishing failed: ${err}`);
                    throw err;
                });
        });
    }

    onSaveBookSize = () => {
        this.state.strategy.saveBookSize(parseFloat(this.state.bookSize))
            .then(() => {
                this.setState({
                    modifiedBookSize: false
                });
                this.props.portView.successAlert("BookSize", `Successfully changed to: ${this.state.strategy.getBookSize().toExponential(2)}`);
            })
            .catch(() => {
                this.props.portView.errorAlert("BookSize", `Error setting booksize!`);
            });
    }

    onBookSizeChanged = (event: any, value: string) => {
        this.setState({
            bookSize: value,
            modifiedBookSize: true
        });
    } 

    onDuplicate = () => {
        if (this.props.portView && this.state.strategy)
            this.props.portView.onDuplicate(this.state.strategy._id.toString());
    }

    renderToolbar() {
        return (
            <Toolbar>
                <ToolbarGroup style={tbStyle}>
                    <TextField
                        hintText="Start Date" 
                        floatingLabelText="Start Date"
                        value={this.state.startDate}
                        style={{width: 100}}
                        disabled={true}
                    />  

                    &nbsp;

                    <CombinationSelect 
                        combo={this.state.comboId} 
                        onComboChange={this.onComboChange} 
                    />

                    &nbsp;
                    &nbsp;

                    <UniverseSelect
                        universeId={this.state.strategy && !this.state.strategy.isMaster ? this.state.strategy.getUniverseId() : "Inferred"}
                        market={this.props.market}
                        readOnly={true}
                        onUniverseChange={null}
                        label="Universe"
                        extraUniverses={this.state.strategy && this.state.strategy.isMaster ? ["Inferred"] : []}
                    />
                    
                    &nbsp;
                    &nbsp;
    
                    {/* <DelaySelect
                        label="Delay"
                        delay={this.state.strategy.getDelay()}
                        onDelayChange={null}
                        readOnly={true}
                    />

                    &nbsp;
                    &nbsp; */}
     
                    <TextField
                        hintText="Book Size" 
                        floatingLabelText="Book Size"
                        value={this.state.bookSize}
                        style={{width: 100}}
                        className={this.state.strategy && !this.state.strategy.isMaster ? "hidden" : ""}
                        onChange={this.onBookSizeChanged}
                    />  

                    <IconButton 
                        style={mediumIcon}
                        onClick={() => this.setState({showSettings: true})}
                    >
                        <FontIcon color={Colors.deepOrange500} className="material-icons">build</FontIcon>
                    </IconButton>

                </ToolbarGroup>

                <ToolbarGroup>
                    <ToolbarTitle 
                        text={`${this.getTitleLabel()}`} 
                        className="dev-alpha-title" 
                    />
                </ToolbarGroup>

                <ToolbarGroup>
                    <RaisedButton
                        secondary={true}
                        className={!this.state.isRunning ? "" : "hidden"}
                        icon={<RunIcon color={"#ff0066"} />}
                        style={btStyle}
                        onClick={this.onRun}
                    />

                    <RaisedButton
                        className={this.state.isRunning ? "" : "hidden"}
                        backgroundColor="red"
                        icon={<CancelIcon color={"white"} />}
                        style={btStyle}
                        onClick={this.onCancel}
                    />

                    <IconButton 
                        key={`BooksizeSave_${this.state.modifiedBookSize}`}
                        style={mediumIcon} 
                        onClick={this.onSaveBookSize}
                        disabled={!this.state.strategy || !this.state.strategy.isMaster || !this.state.modifiedBookSize}
                        className={this.state.strategy.isMaster ? "" : "hidden"}
                    >
                        <FontIcon color={Colors.green700} className="material-icons">save</FontIcon>
                    </IconButton>                    
    
                    <IconButton style={mediumIcon} href={`#p=${AppPages.portfolio}&market=${this.props.market}&view=${ViewType.strategyResults}&strategyId=${this.props.strategyId}`}>
                        <FontIcon color={Colors.grey400} className="material-icons">trending_up</FontIcon
                    ></IconButton>

                    <IconButton style={mediumIcon} onClick={this.onDuplicate}>
                        <FontIcon color={Colors.grey400} className="material-icons">file_copy</FontIcon
                    ></IconButton>

                    <IconButton style={mediumIcon} onClick={this.onPublish} className={this.state.strategy && !this.state.strategy.isPublished && !this.state.strategy.isMaster ? "" : "hidden"}>
                        <FontIcon color={Colors.limeA400} className="material-icons">publish</FontIcon>
                    </IconButton>

                    <IconButton style={mediumIcon} onClick={this.onDelete} className={this.state.strategy && !this.state.strategy.isPublished && !this.state.strategy.isMaster ? "" : "hidden"}>
                        <FontIcon color={Colors.deepOrange500} className="material-icons">delete</FontIcon>
                    </IconButton>

                </ToolbarGroup>
            </Toolbar>
        );
    }

    renderAlphas() {
        return (
            <div>
                { 
                    this.state.alphaInfo.alphas.map((alpha, ai) => { 
                        return (
                            <PortAlphaFeedItem 
                                location={this.props.location}
                                match={this.props.match}
                                history={this.props.history}
                                key={this.getKey(alpha)} 
                                alpha={this.state.strategy && !this.state.strategy.isMaster ? alpha as Alpha : alpha as Strategy} 
                                strategyInfo={this.props.strategyInfo}
                                onQuickShowAlpha={this.onQuickShowAlpha} 
                                strategy={this.state.strategy}
                                onSave={this.onSave}
                                onDeleteAlpha={this.onDeleteAlpha} 
                                dbName={null}
                                portView={this.props.portView}
                            />
                        );
                    }) 
                }
            </div>
        )
    }

    renderLoadingIndicator() {
        return (
            <RefreshIndicator
                size={100}
                left={0}
                top={0}
                status={this.state.loadingState as any}
                loadingColor='cyan'
                style={{marginLeft: '50%', marginTop: '25%', position: 'relative' as any, visibility: `${this.state.loadingState === 'loading' ? 'visible' : 'hidden'}` as any}}
            />            
        );
    }
    
    render() {
        return (
            <div className="port-feed">
                {this.renderToolbar()}
                {this.renderAlphas()}
                {/* {this.renderLiveResults()} */}
                {this.renderAlphaSettings()}
                {this.renderAlphaDlg()}
                {this.renderLoadingIndicator()}
            </div>
        );
    }

    onSaveSettings = () => {
        this.props.portView.successAlert("Save", "Settings saved successfully");
        this.setState({
            showSettings: false
        });
    }

    onCancelSettings = () => {
        this.setState({
            showSettings: false
        });
    }

    renderAlphaDlg() {
        return (
            <PortShowAlphaDlg
                key={`PortShowDlg-${this.state.currAlpha ? this.state.currAlpha._id : ""}`}
                location={this.props.location}
                match={this.props.match}
                history={this.props.history}
                market={this.props.market}
                onOk={() => this.setState({showAlphaDlg: false, currAlpha: null})}
                alphaInfo={this.state.alphaInfo}
                alpha={this.state.currAlpha}
                open={this.state.showAlphaDlg}
            />
        );
    }
    
    renderAlphaSettings() {
        return (
            <AlphaSettings
                location={this.props.location}
                match={this.props.match}
                history={this.props.history}
                key={this.state.strategy ? `AlphaSetting-${this.state.strategy._id}` : "AlphaSettings"}
                alpha={this.state.strategy ? this.state.strategy : null}
                onOk={this.onSaveSettings}
                onCancel={this.onCancelSettings}
                canCancel={true}
                open={this.state.showSettings}
                showOperations={false}
                showOptimiser={true}
                market={this.props.market}
            />
        );
    }

    renderLiveResults() {
        return (
            <LiveResults
                showOutputWindow={this.state.showOutputWindow}
                onShowOutputWindow={this.onShowOutputWindow}
                onHideOutputWindow={this.onHideOutputWindow}
                logMsgs={this.state.logMsgs}
                isRunning={this.state.isRunning}
                
                runPercent={this.state.runPercent}
                liveStats={this.state.liveStats}
                numStats={this.state.numStats}
                
                subRunPercent={this.state.subRunPercent}
                subLiveStats={this.state.subLiveStats}
                numSubStats={this.state.numSubStats}
                showSub={this.state.subLiveStats ? true : false}

                superRunPercent={this.state.superRunPercent}
                superLiveStats={this.state.superLiveStats}
                numSuperStats={this.state.numSuperStats}
                showSuper={this.state.subLiveStats ? true : false}

                alpha={this.state.strategy}
                key={`${this.state.isRunning}_${this.state.logMsgs.length}`}
            />
        );
    }

    getOutputString() {
        if (this.state.isRunning)
            return "Running Simulation [" + this.state.runPercent.toString() + " %]";
        return "Output";
    }

    noOp() {}
}
