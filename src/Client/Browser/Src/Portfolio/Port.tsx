import React from 'react';
import { RouteComponentProps } from 'react-router';

import { ObjectID } from "Shared/Core/ObjectID";
import { Strategy } from 'Client/Model/Config/ClStrategy';
import PortSideBar from "./PortSideBar";
import PortCreateStrategy, { StrategyCreateInfo } from './PortCreateStrategy';
import * as qs from 'query-string';

import { DefMarket as gDefMarket, AlphaType } from 'Shared/Model/Config/ShAlpha';

import PortAlphaFeed from './PortAlphaFeed';
import PortStrategyView from './PortStrategyView';

import ViewAlphaResults from '../Common/ViewAlphaResults';

import RGL, { WidthProvider } from "react-grid-layout";
import { AppPages, AppState } from 'Client/Browser/Src/AppState';
import { ViewType } from './PortDefs';
import { Debug, PromiseUtil } from 'Shared/Core/ShUtil';
import { AlphaOrStrategyInfo, AlphaOrStrategy } from 'Client/Model/Config/ClConfigBase';
import DuplicateAlphaOrStrategy from '../Common/DuplicateAlphaOrStrategy';
import { Cl } from 'Client/Core/ClCore';
const ReactGridLayout = WidthProvider(RGL);

import { withSnackbar, InjectedNotistackProps } from 'notistack';
import PortTrades from './PortTrades';
import PortLivePnl from './PortLivePnl';

export interface Props extends RouteComponentProps<any>, React.Props<any>, InjectedNotistackProps {
}

export interface State {
    children            : any[];
    showCreateDialog    : boolean;
    view                : ViewType;
    market              : string;
    strategyInfo        : AlphaOrStrategyInfo;
    alphaStrategies     : AlphaOrStrategyInfo;
    masterStrategies    : AlphaOrStrategyInfo;
    strategyId          : ObjectID;
    dupId               : string;
}
    
export class Port extends React.Component<Props & RouteComponentProps<any>, State> {
    view                : ViewType = null;
    market              : string = gDefMarket;
    strategyId          : ObjectID = null;
    container           : any = null;

    constructor(props: Props) {
        super(props);
        
        this.state = {
            children        : [],
            showCreateDialog: false,
            view            : this.view,
            market          : this.market,
            strategyInfo    : new AlphaOrStrategyInfo(this.market, null),
            alphaStrategies : new AlphaOrStrategyInfo(this.market, null),
            masterStrategies : new AlphaOrStrategyInfo(this.market, null),

            strategyId      : null,
            dupId           : ""
        };
    }

    refreshState(strategyInfo: AlphaOrStrategyInfo) {
        let alphaStrategies: AlphaOrStrategy[] = [];
        let masterStrategies: AlphaOrStrategy[] = [];

        strategyInfo.strategies.map((strategy: AlphaOrStrategy) => {
            if (strategy.isMaster)
                masterStrategies.push(strategy);
            else if (!strategy.isPublished)
                alphaStrategies.push(strategy);
        });

        this.setState({
            market: this.market,
            view: this.view,
            strategyInfo: strategyInfo,
            alphaStrategies: new AlphaOrStrategyInfo(this.state.market, alphaStrategies),
            masterStrategies: new AlphaOrStrategyInfo(this.state.market, masterStrategies),
            strategyId: this.strategyId
        });
    }

    updateState() {
        this.getAllStrategies()
            .then((strategyInfo: AlphaOrStrategyInfo) => {
                this.refreshState(strategyInfo);
            });

        return true;
    }

    checkArgs(strategyInfo: AlphaOrStrategyInfo) {
        if (!strategyInfo)
            return false;

        const args = qs.parse(this.props.location.hash);

        if (args.p != AppPages.portfolio)
            return false;

        if (!args || !args.view || (args.view == this.view && args.market == this.market && ((args.strategyId && args.strategyId == this.strategyId.toString()) || (!args.strategyId && !this.strategyId))))
            return false;

        this.market = args.market as string;
        this.view = args.view as ViewType;
        if (args.strategyId)
            this.strategyId = ObjectID.create(args.strategyId);
        else
            this.strategyId = null;

        // return this.refreshState(this.state.strategyInfo);
        return this.updateState();
    }

    getStrategy(): Promise<AlphaOrStrategy> {
        if (this.strategyId)
            return Strategy.getStrategy(this.strategyId);
        return PromiseUtil.resolvedPromise(null);
    }

    getAllStrategies(): Promise<AlphaOrStrategyInfo> {
        console.log(`[Port] Getting all strategies for market: ${this.state.market}`);
        let sinfo: AlphaOrStrategyInfo;
        const args: any = qs.parse(this.props.location.hash);
        let market: string = args.market as string || this.market;

        return Strategy.getUnPublished(market)
            .then((strategyInfo: AlphaOrStrategyInfo) => {
                console.log(`[Dev] Strategies count: ${strategyInfo.strategies.length}`);
                sinfo = strategyInfo;
                return this.getStrategy();
            })
            .then((strategy: AlphaOrStrategy) => {
                /// If the market doesn't match then just go to the alpha feed
                // if (strategy && strategy.market != this.market) {
                //     this.props.location.push(`#p=${AppPages.portfolio}&market=${this.market}&view=${ViewType.alphaFeed}`)
                //     return sinfo;
                // }

                if (strategy && !sinfo.doesExist(strategy._id) && strategy.market == market)
                    sinfo.add(strategy);
                return sinfo;
            });
    }

    componentDidMount() {
        return this.updateState();
    }

    componentDidUpdate() {
        this.checkArgs(this.state.strategyInfo);
    }

    onCreateStrategy = (sci: StrategyCreateInfo) => {
        if (!sci.market) 
            sci.market = this.state.market;

        console.log("[Port] Creating strategy: %s/%s", sci.category, sci.name);

        const args = qs.parse(this.props.location.hash);

        Strategy.create(sci.category, sci.name, sci.market, sci.universeId, sci.refConfigfilename, sci.isMaster, sci.delay)
            .then((strategyInfo) => {
                this.setState({
                    showCreateDialog: false,
                    strategyInfo: strategyInfo
                });
            });
    }

    onCancelCreateStrategy = () => {
        this.setState({
            showCreateDialog: false
        });
    } 

    renderAlphaFeed() {
        return (
            <PortAlphaFeed
                view={this.view as string}
                location={this.props.location}
                type={AlphaType.alpha}
                match={this.props.match}
                history={this.props.history}
                key={`AlphaFeed_${this.state.alphaStrategies.length}_${this.state.strategyId}_${this.state.market}`} 
                market={this.state.market}
                strategyInfo={this.state.alphaStrategies}
                createStrategy={() => this.setState({ showCreateDialog: true })}
                portView={this}
                prefix={`alpha.${this.market}`}
            />
        );
    }

    renderStrategyFeed() {
        return (
            <PortAlphaFeed
                view={this.view as string}
                location={this.props.location}
                type={AlphaType.strategy}
                match={this.props.match}
                history={this.props.history}
                key={`StrategyFeed_${this.state.masterStrategies.length}_${this.state.strategyId}_${this.state.market}`} 
                market={this.state.market}
                strategyInfo={this.state.masterStrategies}
                createStrategy={() => this.setState({ showCreateDialog: true })}
                hideDelay={false}
                hideType={true}
                portView={this}
                prefix={`strategy.${this.market}`}
            />
        );
    }

    successAlert(title, message) {
        // if (!this.container)
        //     return;

        // this.container.success(
        //      message, title, {
        //         timeOut: 500,
        //         extendedTimeOut: 500,
        //         showAnimation: 'animated fadeIn',
        //         hideAnimation: 'animated fadeOut'
        //     });
        // Cl.ui.hintSuccess(title, message);
        this.props.enqueueSnackbar(message, { variant: 'success' })
    }

    errorAlert(title, message) {
        // if (!this.container)
        //     return;

        // this.container.error(
        //      message, title, {
        //         timeOut: 5000,
        //         extendedTimeOut: 1000,
        //         showAnimation: 'animated fadeIn',
        //         hideAnimation: 'animated fadeOut'
        //     });
        // Cl.ui.hintError(title, message);

        this.props.enqueueSnackbar(message, { variant: 'error' })
    }

    renderStrategy() {
        return (
            <PortStrategyView
                key={`PortStrategyView-${this.state.strategyId}`}
                location={this.props.location}
                match={this.props.match}
                history={this.props.history}
                market={this.state.market}
                strategyId={this.state.strategyId}
                strategyInfo={this.state.strategyInfo}
                portView={this}
            />
        );
    }

    renderStrategyResults() {
        return (
            <ViewAlphaResults
                key={`ViewAlphaResults-${this.state.strategyId}`}
                location={this.props.location}
                match={this.props.match}
                history={this.props.history}
                market={this.state.market}
                alphaInfo={this.state.strategyInfo}
                alphaId={this.state.strategyId}
            />
        );
    }

    renderTrades() {
        return (
            <PortTrades 
                key={`PortTrades-${this.state.market}`}
                location={this.props.location}
                match={this.props.match}
                history={this.props.history}
                market={this.state.market}
                portView={this}
            />
        )
    }

    renderLivePnl() {
        return (
            <PortLivePnl 
                key={`PortLivePnl-${this.state.market}`}
                location={this.props.location}
                match={this.props.match}
                history={this.props.history}
                market={this.state.market}
                portView={this}
            />
        )
    }

    renderMaster() {
        return (<div/>);
    }

    renderMain() {
        if (!this.view || !this.state.strategyInfo)
            return;

        if ((this.view == ViewType.strategy || this.view == ViewType.strategyResults) && !this.state.strategyId)
            return;

        if (this.view == ViewType.alphaFeed) {
            return this.renderAlphaFeed();
        }
        else if (this.view == ViewType.strategyFeed) {
            return this.renderStrategyFeed();
        }
        else if (this.view == ViewType.strategy) {
            return this.renderStrategy();
        }
        else if (this.view == ViewType.strategyResults) {
            return this.renderStrategyResults();
        }
        else if (this.view == ViewType.master) {
            return this.renderMaster();
        }
        else if (this.view == ViewType.trades) {
            return this.renderTrades();
        }
        else if (this.view == ViewType.livePnl) {
            return this.renderLivePnl();
        }
        else {
            Debug.assert(false, `[Port] Unsupported view: ${this.view}`);
        }
    }    

    onDuplicate = (dupId: string) => {
        this.setState({
            dupId: dupId
        });
    }
    
    okDuplicate = (name: string, category: string) => {
        AlphaOrStrategy.duplicate(this.state.market, this.state.dupId, name, category)
            .then((obj: AlphaOrStrategy) => {
                this.successAlert("Duplicate", `Duplicate created with id: ${obj._id}`);

                this.setState({
                    dupId: ""
                })
            })
            .catch((err) => {
                this.errorAlert(err.response.statusText, `Error duplicating. ${err.response.status}: ${err.response.data}`);

                this.setState({
                    dupId: ""
                })
            });
    }  

    cancelDuplicate = () => {
        this.setState({
            dupId: ""
        });
    }
    
    createDuplicateDialog() {
        return (
            <DuplicateAlphaOrStrategy
                key={`DuplicateAlphaOrStrategy-${this.state.dupId}`}
                location={this.props.location}
                match={this.props.match}
                history={this.props.history}
                onOk={this.okDuplicate}
                onCancel={this.cancelDuplicate}
                categories={[]}
                open={this.state.dupId ? true : false}
            />
        );
    }

    createDialog() {
        return (
            <PortCreateStrategy
                key="PortCreateDialog"
                location={this.props.location}
                match={this.props.match}
                history={this.props.history}
                onOk={this.onCreateStrategy}
                onCancel={this.onCancelCreateStrategy}
                canCancel={true}
                open={this.state.showCreateDialog}
                market={this.state.market}
            />
        );
    }

    createUI() {
        return (
            <div>
                {this.renderMain()}
                {this.createDialog()}
                {this.createDuplicateDialog()}
            </div>
        );
    }

    children() {
        // if (!this.state.showCreateDialog) {
        //     return this.createDialog();
        // }

        /// Otherwise we show just the normal UI
        return this.createUI();
    }

    render() {
        return (
            <div>
                <ReactGridLayout
                    className="layout"
                    style={{height: "100%"}}
                    autoSize={false}
                    margin={[1, 1]}
                    verticalCompact={true}
                    rowHeight={(window.innerHeight - 100.0) / 10.0}
                >
                    <div 
                        key="1"
                        data-grid={{ x: 0, y: 0, w: 2, h: 10, static: true }}
                    >
                        <PortSideBar
                            location={this.props.location}
                            match={this.props.match}
                            history={this.props.history}
                            key={`PortSideBar_${this.state.strategyInfo.length}_${this.state.strategyId}_${this.state.market}`}
                            strategyInfo={this.state.strategyInfo}
                            strategyId={this.state.strategyId}
                            view={this.state.view}
                            market={this.state.market}
                            createStrategy={() => this.setState({ showCreateDialog: true })}
                        /> 
                    </div>
                    <div 
                        key="2" 
                        data-grid={{ x: 2, y: 0, w: 10, h: 10, static: true }}
                        className="main-content v-scrollable" 
                    >
                        {this.children()}
                    </div>
                </ReactGridLayout>

        {/* <PortSideBar
                    location={this.props.location}
                    key={`PortSideBar_${this.state.strategyInfo.length}_${this.state.strategyId}_${this.state.market}`} 
                    strategyInfo={this.state.strategyInfo}
                    strategyId={this.state.strategyId}
                    view={this.state.view}
                    market={this.state.market}
                    createStrategy={() => this.setState({ showCreateDialog: true })}
                /> 

                <div 
                    className="main-content" 
                    id='body' 
                    key={`PortMain-${this.state.view}`}
                >
                    {this.children()}
                </div> */}

            </div>
        );
    }
}

export default withSnackbar(Port);
