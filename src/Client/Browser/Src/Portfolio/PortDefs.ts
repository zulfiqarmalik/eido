export const enum ViewType {
    alphaFeed           = "alphaFeed",
    strategyFeed        = "strategyFeed",
    strategy            = "strategy",
    strategyResults     = "strategyResults",
    alphaResults        = "alphaResults",
    master              = "master",
    trades              = "trades",
    livePnl             = "livePnl"
}
