import React from 'react';
  
import Paper from '@material-ui/core/Paper'; import {
    SearchState,
    IntegratedFiltering,
} from '@devexpress/dx-react-grid';
import {
    Grid,
    Table,
    Toolbar,
    SearchPanel,
    TableHeaderRow,
} from '@devexpress/dx-react-grid-material-ui';

import {
    SortingState,
    IntegratedSorting,
  } from '@devexpress/dx-react-grid';
import { RouteComponentProps } from 'react-router-dom';
import { Port } from './Port';

export interface Props extends RouteComponentProps<any>, React.Props<any> {
    key                 ?: string;
    market              : string;
    portView            : Port;
}

export interface State {
    columns             : any[];
    rows                : any[]
}

export default class PortLivePnl extends React.Component<Props, State> {
    constructor(props: Props) {
        super(props);

        this.state = {
            columns: [
                { name: 'name', title: 'Name' },
                { name: 'sex', title: 'Sex' },
                { name: 'city', title: 'City' },
                { name: 'car', title: 'Car' },
            ],
            rows: [{
                name: "Zulfi", sex: "male", city: "London", car: "BMW"
            }, {
                name: "Ammara", sex: "female", city: "Reigate", car: "Audi"
            }]
        };
    }

    componentDidMount() {
    }

    componentDidUpdate() {
    }

    render() {
        const { rows, columns } = this.state;

        return (
            <Paper 
                style={{height: "100%"}}
            >
                <Grid
                    rows={rows}
                    columns={columns}
                >
                    <SearchState defaultValue="" />
                    <IntegratedFiltering />
                    <SortingState
                        defaultSorting={[{ columnName: 'city', direction: 'asc' }]}
                    />
                    <IntegratedSorting />
                    <Table />
                    <TableHeaderRow showSortingControls />
                    <Toolbar />
                    <SearchPanel />
                </Grid>
            </Paper>
        );
    }
}
