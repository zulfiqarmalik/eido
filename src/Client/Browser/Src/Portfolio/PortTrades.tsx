import React from 'react';
  
import Paper from '@material-ui/core/Paper'; import {
    SearchState,
    IntegratedFiltering,
    FilteringState,
    SelectionState,
    IntegratedSelection,
    DataTypeProvider,
} from '@devexpress/dx-react-grid';

import {
    Grid,
    Table,
    VirtualTable,
    Toolbar,
    SearchPanel,
    TableHeaderRow,
    TableFilterRow,
    TableSelection,
    TableColumnVisibility,
    ColumnChooser,
} from '@devexpress/dx-react-grid-material-ui';

import {
    SortingState,
    IntegratedSorting,
  } from '@devexpress/dx-react-grid';

import { RouteComponentProps } from 'react-router-dom';
import { Port } from './Port';
import { Trade, Wave } from 'Client/Model/Trade/ClTrade';
import { PromiseUtil } from 'Shared/Core/ShUtil';
import { withStyles, Select, Input, MenuItem, Chip } from '@material-ui/core';
import { fade } from 'material-ui/utils/colorManipulator';
import { colors } from 'material-ui/styles';

export interface Props extends RouteComponentProps<any>, React.Props<any> {
    key                 ?: string;
    market              : string;
    portView            : Port;
}

export interface State {
    columns             : any[];
    rows                : any[];
    selection           : any[];
    sideColumns         : string[];
    hiddenColumns       : string[];

    trade               : Trade;
    wave                : Wave;
}

const Root = props => <Grid.Root {...props} style={{ height: '100%' }} />;

const styles = theme => ({
    tableStriped: {
        '& tbody tr:nth-of-type(odd)': {
            backgroundColor: colors.blueGrey900,
        },
    },

    tableDark: {
        backgroundColor: '#272822'
    }
});

const TableComponentBase = ({ classes, ...restProps }) => (
    <Table.Table
        {...restProps}
        className={classes.tableDark}
    />
);

export const TableComponent = withStyles(styles, { name: 'TableComponent' })(TableComponentBase);

const SideFormatter = (props) => {
    let { column, row, value } = props;

    if (row.side === 'Buy') {
        return (
            <span style={{ color: 'green' }}>
                {value}
            </span>
        );
    }
    else {
        return (
            <span style={{ color: 'red' }}>
                {value}
            </span>
        );
    }
}

const SideFormatterProvider = props => (
    <DataTypeProvider
        formatterComponent={SideFormatter}
        {...props}
    />
);

// const SideEditor = ({ value, onValueChange }) => (
//     <Select
//         input={<Input />}
//         value={value}
//         onChange={event => onValueChange(event.target.value)}
//         style={{ width: '100%' }}
//     >
//         <MenuItem value="">ALL</MenuItem>
//         <MenuItem value="Buy">Buy</MenuItem>
//         <MenuItem value="Sell">Sell</MenuItem>
//         <MenuItem value="Short">SellShort</MenuItem>
//     </Select>
// );

// const SideTypeProvider = props => (
//     <DataTypeProvider
//         formatterComponent={SideFormatter}
//         editorComponent={SideEditor}
//         {...props}
//     />
// );
  

export default class PortTrades extends React.Component<Props, State> {
    constructor(props: Props) {
        super(props);

        this.state = {
            columns: [
                { name: 'ticker', title: 'Ticker' },
                { name: 'sedol', title: 'Sedol' },
                { name: 'assetClass', title: 'Asset Class' },
                { name: 'mic', title: 'Exchange' },
                { name: 'region', title: 'Region' },
                { name: 'side', title: 'Side' },
                { name: 'quantity', title: 'Quantity' },
                { name: 'country', title: 'Country' },
                { name: 'currency', title: 'Currency' },
                { name: 'algo', title: 'Algo' }
            ],
            rows: [],
            selection: [],
            sideColumns: ['side', 'quantity'],
            hiddenColumns: ['assetClass', 'mic', 'region', 'country', 'currency', 'algo'],

            trade: new Trade(),
            wave: new Wave()
        };
    }

    changeSelection = selection => this.setState({ selection })

    getTrade() {
        return Trade.getTrade(this.props.market);
    }

    getWave(trade: Trade, waveId: string = "") {
        if (!waveId) {
            if (!trade.waves.length)
                return PromiseUtil.resolvedPromise(new Wave()); /// return an empty wave

            waveId = trade.waves[trade.waves.length - 1];
        }

        return Wave.getWave(waveId);
    }

    waveToRows(wave: Wave): any[] {
        let rows: any[] = Array<any>(wave.marketTrades.length);

        for (let i = 0; i < wave.marketTrades.length; i++) {
            let marketTrade = wave.marketTrades[i];
            let sec = marketTrade.security;
            rows[i] = {
                ticker      : sec.bloomberg.tickerAndExchangeCode,
                sedol       : sec.sedol,
                assetClass  : sec.assetClass,
                mic         : sec.mic,
                region      : sec.region,
                side        : marketTrade.side,
                quantity    : marketTrade.quantity,
                country     : sec.country,
                currency    : sec.currency,
                algo        : marketTrade.algoName
            };
        }

        return rows;
    }

    componentDidMount() {
        let _trade: Trade = null;

        return this.getTrade()
            .then((trade: Trade) => {
                _trade = trade;
                return this.getWave(trade)
            })
            .then((wave: Wave) => {
                let rows = this.waveToRows(wave);

                this.setState({
                    trade: _trade,
                    wave: wave,
                    rows: rows
                });
            });
    }

    componentDidUpdate() {
    }

    hiddenColumnNamesChange = (hiddenColumns) => {
        this.setState({  hiddenColumns });
    };    

    render() {
        return (
            <div>
                <Paper 
                    style={{height: `${window.innerHeight - 100}px`, backgroundColor: '#272822'}}
                    className='table-header'
                >
                    <Grid
                        rows={this.state.rows}
                        columns={this.state.columns} 
                        rootComponent={Root}
                    >
                        <SelectionState
                            selection={this.state.selection}
                            onSelectionChange={this.changeSelection}
                        />
                        <IntegratedSelection />

                        {/* <SideTypeProvider for={this.state.sideColumns} /> */}
                        <SideFormatterProvider for={this.state.sideColumns}/>

                        {/* <SearchState defaultValue="" /> */}

                        <FilteringState defaultFilters={[]} />
                        <IntegratedFiltering />
                        <SortingState />
                        <IntegratedSorting />

                        <VirtualTable 
                            height={`${window.innerHeight - 120}`}
                            tableComponent={TableComponent}
                        />

                        <TableHeaderRow 
                            showSortingControls 
                        />
                        <TableFilterRow />
                        <TableSelection showSelectAll />

                        <TableColumnVisibility
                            hiddenColumnNames={this.state.hiddenColumns}
                            onHiddenColumnNamesChange={this.hiddenColumnNamesChange}
                        />
                        <Toolbar />
                        <ColumnChooser />

                        {/* <SearchPanel /> */}
                    </Grid>
                </Paper>
            </div>
        );
    }
}
