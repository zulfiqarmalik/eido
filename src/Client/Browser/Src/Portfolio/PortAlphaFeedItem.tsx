import React from 'react';
import { RouteComponentProps } from 'react-router';

import {Card, CardActions, CardHeader, CardMedia, CardTitle, CardText} from 'material-ui/Card';
import Paper from 'material-ui/Paper';

import MenuItem from 'material-ui/MenuItem';
import FontIcon from 'material-ui/FontIcon';

import {Toolbar, ToolbarGroup, ToolbarSeparator, ToolbarTitle} from 'material-ui/Toolbar';
import Divider from 'material-ui/Divider';
import IconButton from 'material-ui/IconButton';
import FavouriteIcon from 'material-ui/svg-icons/action/grade';
import DeleteIcon from 'material-ui/svg-icons/action/delete';
import Popover from 'material-ui/Popover';
import Menu from 'material-ui/Menu';
import Checkbox from 'material-ui/Checkbox';
import NavigationExpandMoreIcon from 'material-ui/svg-icons/navigation/expand-more';
import TextField from 'material-ui/TextField';
import * as Colors from 'material-ui/styles/colors';

import { Alpha } from 'Client/Model/Config/ClAlpha';
import { Strategy } from 'Client/Model/Config/ClStrategy';
import { Debug, ShUtil } from 'Shared/Core/ShUtil';
import { ObjectID } from 'Shared/Core/ObjectID';
import { Cl } from 'Client/Core/ClCore';
import { ConfigType, AlphaType } from 'Shared/Model/Config/ShAlpha';
import { Toggle } from 'material-ui';
import { AlphaOrStrategyInfo, AlphaOrStrategy } from 'Client/Model/Config/ClConfigBase';
import { colors } from 'material-ui/styles';
import { User } from 'Client/Model/User/ClUser';
import DuplicateAlphaOrStrategy from '../Common/DuplicateAlphaOrStrategy';
import { Port } from './Port';
import { ViewType } from './PortDefs';
  
let CancelIcon = require('material-ui-icons/Cancel').default;
let CommentIcon = require('material-ui-icons/Comment').default;

const tbStyle = {
    marginLeft: 15,
};

const mediumIcon = {
    width: 48,
    height: 48,
};

const styles = {
    block: {
        maxWidth: 100,
    },
    toggle: {
        marginBottom: 16,
    },
    thumbOff: {
        backgroundColor: '#39CCCC',
    },
    trackOff: {
        backgroundColor: '#399d9d',
    },
    thumbSwitched: {
        backgroundColor: 'red',
    },
    trackSwitched: {
        backgroundColor: '#ff9d9d',
    },
    labelStyle: {
        // color: 'red',
    },
};

interface Props extends RouteComponentProps<any>, React.Props<any> {
    alpha               : AlphaOrStrategy;
    onQuickShowAlpha    : (alpha: AlphaOrStrategy) => void;
    createStrategy      ?: () => void;
    strategy            : Strategy;
    strategyInfo        : AlphaOrStrategyInfo;
    onSave              ?: (alpha: AlphaOrStrategy, weight: number, reversion: boolean) => any;
    onDeleteAlpha       ?: (alpha: AlphaOrStrategy) => any;

    addText             ?: string;
    disableCreateNew    ?: boolean;

    dbName              : string;
    portView            : Port;
}

interface State {
    pageId              : number;
    ppCount             : number;
    isActive            : boolean;
    isFavourite         : boolean;
    showStrategyOps     : boolean;
    anchorEl            ?: any;
    modified            : boolean;
    defaultImg          : boolean;
    staticImg           : boolean;
    pruned              : boolean;
}
    
export default class PortAlphaFeedItem extends React.Component<Props, State> {
    constructor(props: Props) {
        super(props);

        this.state = {
            pageId: 0,
            ppCount: 5,
            isActive: false,
            isFavourite: false,
            showStrategyOps: false,
            modified: false,
            defaultImg: false,
            staticImg: true,
            pruned: false
        };
    }

    componentDidMount() {
    }

    onQuickShowAlpha = (e: any) => {
        e.preventDefault();
        
        if (this.props.onQuickShowAlpha)
            this.props.onQuickShowAlpha(this.props.alpha);
    }

    get alpha() {
        return this.props.alpha;
    }

    get keyStats() {
        return this.props.alpha.keyStats;
    }

    onAddToStrategy(event, index, universeId) {
        if (index == 0) {
            if (this.props.createStrategy)
                this.props.createStrategy();
                
            return;
        }
    }

    onShowStrategyOps = (event: any) => {
        // This prevents ghost click.
        // event.preventDefault();
        
        this.setState({
            showStrategyOps: true,
            anchorEl: event.currentTarget,
        });
    }
    
    onHideStrategyOps = () => {
        this.setState({
            showStrategyOps: false,
        });
    }

    onCreateNew() {
        this.onHideStrategyOps(); 
        if (this.props.createStrategy)
            this.props.createStrategy();
    }

    addRemoveAlpha(strategy: Strategy, alpha: Alpha, checkStatus: boolean) {
        if (checkStatus) 
            strategy.addAlpha(this.props.alpha as Alpha);
        else 
            strategy.removeAlpha(this.props.alpha as Alpha);
    }

    renderStatsTable() {
        return (
            <table className={"port-alpha-feed-item-table-title"}>
                <colgroup>
                    <col width="50px" />
                    <col width="50px" />
                    <col width="50px" />
                    <col width="50px" />
                    <col width="50px" />
                    <col width="50px" />
                    <col width="50px" />
                    <col width="50px" />
                    <col width="50px" />
                </colgroup>

                <thead>
                    <tr>
                        <th className="left"><b><font color={Colors.teal200}>Margin BPS</font></b></th>
                        <th className="left"><b><font color={Colors.teal200}>IR/Sharpe</font></b></th>
                        <th className="left"><b><font color={Colors.teal200}>Volatility</font></b></th>
                        <th className="left"><b><font color={Colors.teal200}>Returns %</font></b></th>
                        <th className="left"><b><font color={Colors.teal200}>Turnover %</font></b></th>
                        <th className="left"><b><font color={Colors.teal200}>MaxDD %</font></b></th>
                        <th className="left"><b><font color={Colors.teal200}>MaxDH</font></b></th>
                        <th className="left"><b><font color={Colors.teal200}>Hit Ratio</font></b></th>
                        <th className="left"><b><font color={Colors.teal200}>Long / Short</font></b></th>
                    </tr>
                </thead>
                <tbody>
                        <tr>
                            <td className="left"><b><font color={ this.keyStats && this.keyStats.pnl >= 0.0 ? 'green' : 'red' }>{ this.keyStats && this.keyStats.marginBps ? this.keyStats.marginBps.toFixed(2) : "0" }</font></b></td>
                            <td className="left">{ this.keyStats && this.keyStats.ir ? this.keyStats.ir.toFixed(4).toString() + " / " + (this.keyStats.ir * Math.sqrt(252)).toFixed(4).toString() : "" }</td>
                            <td className="left">{ this.keyStats && this.keyStats.volatility ? this.keyStats.volatility.toFixed(2) : "0" }</td>
                            <td className="left">{ this.keyStats && this.keyStats.returns ? this.keyStats.returns.toFixed(2) : "0" }</td>
                            <td className="left">{ this.keyStats && this.keyStats.tvr ? this.keyStats.tvr.toFixed(2) : "0" }</td>
                            <td className="left">{ this.keyStats && this.keyStats.maxDD ? this.keyStats.maxDD.toFixed(2) : "0" }</td>
                            <td className="left">{ this.keyStats && this.keyStats.maxDH ? this.keyStats.maxDH : "0" }</td>
                            <td className="left">{ this.keyStats && this.keyStats.hitRatio ? this.keyStats.hitRatio.toFixed(2) : "0" }</td>
                            <td className="left">{ this.keyStats && this.keyStats.longCount ? `${this.keyStats.longCount.toFixed(0)} / ${this.keyStats.shortCount.toFixed(0)}` : "0" }</td>
                        </tr>
                </tbody>
            </table>    
        );
    }

    onDelete = () => {
        Cl.ui.okCancel("Delete", `Are you sure you want to remove alpha: ${this.props.alpha.name} from strategy: ${this.props.strategy.name}`, () => {
            if (this.props.onDeleteAlpha)
                this.props.onDeleteAlpha(this.props.alpha);
        });
    }

    onDuplicate = () => {
        if (this.props.portView && this.props.alpha)
            this.props.portView.onDuplicate(this.props.alpha._id.toString());
    }

    onPrune = () => {
        Cl.ui.okCancel("Prune", `Are you sure you want to prune alpha: ${this.props.alpha._id} - ${this.props.alpha.name}`, () => {
            this.props.alpha.prune().then(() => {
                this.setState({
                    pruned: true
                });
    
                this.props.portView.successAlert("Prune", `Successfully pruned: ${this.props.alpha._id}`);
            });
        });
    }

    renderFooterRHS() {
        return (
            <ToolbarGroup style={tbStyle}>
                <IconButton style={mediumIcon} tooltip="View Strategy"  touch={true} tooltipPosition="top-center" 
                    className={this.props.alpha.type == AlphaType.strategy ? "" : "hidden"}
                    href={`#p=portfolio&market=${this.props.alpha.market}&view=${ViewType.strategy}&strategyId=${this.props.alpha._id}`}>
                    <FontIcon color={Colors.green400} className="material-icons">forward</FontIcon
                ></IconButton>

                <IconButton style={mediumIcon} tooltip="View Result" touch={true} tooltipPosition="top-center" href={`#p=portfolio&view=${ViewType.strategyResults}&market=${this.props.alpha.market}&strategyId=${this.props.alpha._id}&viewResults=true`}>
                    <FontIcon color={Colors.grey400} className="material-icons">trending_up</FontIcon
                ></IconButton>

                <IconButton style={mediumIcon} tooltip="Duplicate" touch={true} tooltipPosition="top-center" onClick={this.onDuplicate}>
                    <FontIcon color={Colors.grey400} className="material-icons">file_copy</FontIcon
                ></IconButton>

                <IconButton tooltip="Delete" touch={true} tooltipPosition="top-center" onClick={this.onDelete} className={this.props.strategy && !this.props.strategy.isPublished ? "" : "hidden"}>
                    <DeleteIcon color={Colors.red500} />
                </IconButton>

                <IconButton tooltip="Add to favourites" touch={true} tooltipPosition="top-center" onClick={() => this.setState({ isFavourite: !this.state.isFavourite })}>
                    <FavouriteIcon color={this.state.isFavourite ? Colors.yellow500 : ""} />
                </IconButton>

                <IconButton 
                    tooltip="Prune" 
                    touch={true} 
                    tooltipPosition="top-center" 
                    onClick={this.onPrune}
                    className={!this.props.strategy ? "" : "hidden"}
                >
                    <CancelIcon color={Colors.orange500} />
                </IconButton>

                <IconButton tooltip="Discuss" touch={true} tooltipPosition="top-center">
                    <CommentIcon color={Colors.blue500} />
                </IconButton>
            </ToolbarGroup>
        );
    }

    renderFooterFeedOptions() {
        return (
            <ToolbarGroup style={tbStyle}>
                <ToolbarTitle text={this.props.addText} className="port-alpha-feed-item-add-strat-title" />
                <ToolbarSeparator />

                <IconButton style={mediumIcon} onClick={this.onShowStrategyOps}>
                    <NavigationExpandMoreIcon />
                </IconButton>

                <Popover
                    open={this.state.showStrategyOps}
                    anchorEl={this.state.anchorEl}
                    anchorOrigin={{ horizontal: 'right', vertical: 'top' }}
                    targetOrigin={{ horizontal: 'right', vertical: 'top' }}
                    onRequestClose={this.onHideStrategyOps}
                >
                    <Menu>
                        <MenuItem key={0} value={0}  primaryText="Create New ..." onClick={() => this.onCreateNew()} 
                            className={this.props.disableCreateNew ? "hidden" : ""}
                        />
                        {
                            this.props.strategyInfo.strategies.map((strategy, i) => {
                                return <MenuItem key={strategy._id.toString()} primaryText="">
                                    <Checkbox 
                                        label={`[${strategy.getUniverseId()}] - ${strategy.name}`} 
                                        style={{paddingTop: 10}} 
                                        defaultChecked={strategy.hasAlpha(this.props.alpha._id) >= 0}
                                        labelStyle={{color: strategy.isMaster ? colors.yellowA200 : colors.white}}
                                        onCheck={(e, c) => this.addRemoveAlpha(strategy as Strategy, this.props.alpha as Alpha, c)}
                                    />
                                </MenuItem>
                            })
                        }
                    </Menu>
                </Popover>

                
            </ToolbarGroup>
        )
    }

    onWeightChanged = (event: any, value: string) => {
        (this.props.alpha as Alpha).weight = value;
        this.setState({
            modified: true
        });
    } 
    
    onReversionChanged = () => {
        this.props.alpha.reversion = !this.props.alpha.reversion;
        this.setState({
            modified: true
        });
    }

    onSave = () => {
        let weight = parseFloat(this.props.alpha.weight);

        this.props.onSave(this.props.alpha, weight, this.props.alpha.reversion)
            .then(() => {
                this.setState({
                    modified: false
                });
            });
    }

    renderWeight() {
        let alphaSec = this.props.alpha.alphaSec;
        Debug.assert(alphaSec, `[FeedItem] No alpha sec for alpha: ${this.props.alpha._id}`);

        return (
            <ToolbarGroup style={tbStyle}>
                <ToolbarTitle text="Weight" className="port-alpha-feed-item-add-strat-title" />
                <TextField
                    hintText="Weight" 
                    value={this.props.alpha.weight}
                    onChange={this.onWeightChanged}
                    style={{width: 50}}
                    disabled={this.props.strategy && this.props.strategy.isPublished}
                />
            </ToolbarGroup>
        );
    }

    renderReversion() {
        let alphaSec = this.props.alpha.alphaSec;
        Debug.assert(alphaSec, `[FeedItem] No alpha sec for alpha: ${this.props.alpha._id}`);

        return (
            <ToolbarGroup style={tbStyle}>
                {/* <Checkbox label="Reversion" checked={this.props.alpha.reversion} onCheck={this.onReversionChanged} /> */}
                <Toggle 
                    label="Reversion"
                    thumbStyle={styles.thumbOff}
                    trackStyle={styles.trackOff}
                    thumbSwitchedStyle={styles.thumbSwitched}
                    trackSwitchedStyle={styles.trackSwitched}
                    labelStyle={styles.labelStyle}
                    toggled={this.props.alpha.reversion}
                    onToggle={this.onReversionChanged}
                    disabled={this.props.strategy && this.props.strategy.isPublished}
                />                
            </ToolbarGroup>
        );
    }

    renderStrategyOptions() {
        return (
            <ToolbarGroup>
                {this.renderWeight()}
                {this.renderReversion()}

                <IconButton 
                    style={mediumIcon} 
                    onClick={this.onSave}
                    className={this.props.strategy && !this.props.strategy.isPublished && this.state.modified ? "" : "hidden"}
                >
                    <FontIcon color={Colors.green700} className="material-icons">save</FontIcon>
                </IconButton>                    
            </ToolbarGroup>
        );
    }

    get strategyId(): ObjectID {
        if (this.props.strategy)
            return this.props.strategy._id;
        return null;
    }

    renderFooterLHS() {
        if (!this.strategyId)
            return this.renderFooterFeedOptions();

        return this.renderStrategyOptions();
    }

    renderFooter() {
        return (
            <Toolbar className="port-alpha-item-footer">
                {this.renderFooterLHS()}
                {this.renderFooterRHS()}
            </Toolbar>
        );
    }

    onStaticImgError = (e) => {
        this.setState({
            staticImg: false
        });
    }

    onImgError = (e) => {
        this.setState({
            defaultImg: true
        });
    }

    renderStaticThm() {
        return (
            <img 
                style={{ width: 250, color: Colors.blue400 }} 
                src={`${ShUtil.getThmPath(this.props.alpha._id)}`} 
                onClick={this.onQuickShowAlpha}
                onError={this.onStaticImgError}
            />
        );
    }

    renderThm() {
        return (
            <img 
                style={{ width: 250, color: Colors.blue400 }} 
                src={`/eido/v1/alpha/thm?alphaId=${this.props.alpha._id}&sampleName=LT&market=${this.props.alpha.market}&dbName=${this.props.dbName || this.props.alpha.containerId}&configType=${this.props.alpha.configType}`} 
                onClick={this.onQuickShowAlpha}
                onError={this.onImgError}
            />
        );
    }

    renderDefThm() {
        return (
            <img 
                style={{ width: 250, color: Colors.blue400 }} 
                src='/imgs/no_image.png' alt="" 
                onClick={this.onQuickShowAlpha}
            />
        );
    }

    renderImg() {
        if (this.state.staticImg)
            return this.renderStaticThm();
        else if (!this.state.defaultImg)
            return this.renderThm();
        return this.renderDefThm();
    }

    render() {
        return (
            <div 
                className={((this.props.alpha && this.props.alpha.pruned) || this.state.pruned) ? "hidden" : "port-alpha-item"} 
                onMouseEnter={() => this.setState({isActive: true})} 
                onMouseLeave={() => this.setState({isActive: false})
            }>
                <Paper zDepth={5}>
                    <Card>
                        <table className="alpha-op-title">
                            <colgroup>
                                <col width="250px" />
                                <col width="100%" />
                            </colgroup>

                            <tbody>
                                <tr>
                                    <td className="port-alpha-item-media">
                                        <a href="#" onClick={this.onQuickShowAlpha} key={`img-${this.alpha._id}-${this.state.defaultImg}`}>
                                            { this.renderImg() }
                                        </a>
                                    </td>
                                    <td>
                                        <Card>
                                            <CardHeader
                                                className={this.state.isActive ? "port-alpha-item-header-active" : "port-alpha-item-header"}
                                                title={`[${this.props.alpha.getUniverseId()}] - [Delay: ${this.props.alpha.getDelay()}] : ${this.props.alpha._id} - ${this.props.alpha.name}`}
                                                titleColor={Colors.cyan500}
                                                subtitle={`by ${User.current.email}`}
                                                avatar="/imgs/me.png"
                                            />
                                            {/* <CardTitle title={`${this.alpha.name} - Key Stats`} className="alpha-title" /> */}
                                            <CardText>
                                                {this.renderStatsTable()}
                                            </CardText>

                                            <CardActions>
                                                <Divider />
                                                {this.renderFooter()}
                                            </CardActions>
                                        </Card>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </Card>
                </Paper>
            </div> 
        );
    }
}
