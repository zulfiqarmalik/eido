import React from 'react';

import { ObjectID } from "Shared/Core/ObjectID";

import { RouteComponentProps } from 'react-router';

import * as qs from 'query-string';
import { ViewType } from './PortDefs';
import { AlphaOrStrategyInfo } from 'Client/Model/Config/ClConfigBase';
import { User } from 'Client/Model/User/ClUser';
import { colors } from 'material-ui/styles';
import { AlphaType } from 'Shared/Model/Config/ShAlpha';

let SvgIcon = require('react-icons-kit').default;
const Icon20 = props => <div style={{color: colors.grey300}}><SvgIcon size={props.size || 20} icon={props.icon} /></div>;
let SideNav = require('react-sidenav').default;
let { Nav, NavIcon, NavText } = require('react-sidenav');

let list = require('react-icons-kit/fa/bars').bars;
let lineChart = require('react-icons-kit/fa/lineChart').lineChart;
let dollar = require('react-icons-kit/fa/dollar').dollar;
let barChart = require('react-icons-kit/fa/barChart').barChart;
let anchor = require('react-icons-kit/fa/anchor').anchor;
let at = require('react-icons-kit/fa/at').at;
let pencil = require('react-icons-kit/fa/pencil').pencil;

interface Props extends RouteComponentProps<any>, React.Props<any> {
    strategyId      : ObjectID;
    view            : ViewType;
    market          : string;
    strategyInfo    : AlphaOrStrategyInfo;
}

interface State {
}
    
export default class PortMenuMain extends React.Component<Props, State> {
    constructor(props: Props) {
        super(props);

        this.state = {
            // market: gDefMarket,
        };
    }

    componentDidMount() {
    }

    getSelected() {
        if (this.props.view == ViewType.alphaFeed)
            return ViewType.alphaFeed;
        if (this.props.view == ViewType.strategyFeed)
            return ViewType.strategyFeed;
        else if (this.props.view == ViewType.master)
            return ViewType.master;
        else if (this.props.view == ViewType.trades)
            return ViewType.trades;
        else if (this.props.view == ViewType.livePnl)
            return ViewType.livePnl;
        else if (!this.props.strategyId)
            return ViewType.alphaFeed;
        return this.props.strategyId.toString();
    }

    onMarketChange(market) {
        this.setState({
            market: market
        });

        const args = qs.parse(this.props.location.hash);
        let url = `#p=portfolio&view=${args.view}&market=${market}`;
        this.props.history.push(url);
    }

    renderStrategyText(text: string, isMaster: boolean, currentUser: boolean) {
        if (!isMaster) {
            return currentUser ? text : `* ${text}`;
        }

        return `* ${text}`;
    }

    renderStrategies(isMaster: boolean, currentUser: boolean) {
        let strategies = this.props.strategyInfo.strategies;
        strategies = strategies.filter((strategy) => strategy.isMaster == isMaster);
        strategies = strategies.filter((strategy) => strategy.type == AlphaType.strategy);
        
        if (currentUser)
            strategies = strategies.filter((strategy) => strategy.ownerId.toString() == User.current._id.toString());
        else
            strategies = strategies.filter((strategy) => strategy.ownerId.toString() != User.current._id.toString());

        return (
            strategies
                .map((strategy, ai) => { 
                return (
                    <Nav key={`SideMenu-${strategy._id}`} id={strategy._id.toString()}>
                        <NavIcon><Icon20 icon={!isMaster ? pencil : barChart} /></NavIcon>
                        <NavText>
                            <a 
                                className="menu-link" 
                                href={`#p=portfolio&market=${this.props.market}&view=${ViewType.strategy}&strategyId=${strategy._id}`} 
                                style={{color: isMaster || !currentUser ? colors.yellowA200 : colors.white}}
                            >
                                {this.renderStrategyText(strategy.name, isMaster, currentUser)}
                            </a>
                        </NavText>
                    </Nav>
                    
                );
            })
        );
    }

    render() {
        return (
            <SideNav 
                highlightBgColor="#0097A7"
                hoverBgColor="#0097A7"
                defaultSelected={this.getSelected()}
                selected={this.getSelected()}
            >
                
                {/* <MarketSelect 
                    market={this.props.market} 
                    onMarketChange={::this.onMarketChange} 
                    style={{width: 250}}
                /> */}

                <Nav key={`AlphaFeed-${this.props.market}`} id="alphaFeed">
                    <NavIcon><Icon20 icon={list} /></NavIcon>
                    <NavText>
                        <a 
                            className="menu-link" 
                            href={`#p=portfolio&market=${this.props.market}&view=${ViewType.alphaFeed}`} 
                            style={{color: colors.white}}
                        >
                            Alpha Feed
                        </a>
                    </NavText>
                </Nav>

                <Nav key={`StrategyFeed-${this.props.market}`} id="strategyFeed">
                    <NavIcon><Icon20 icon={list} /></NavIcon>
                    <NavText>
                        <a 
                            className="menu-link" 
                            href={`#p=portfolio&market=${this.props.market}&view=${ViewType.strategyFeed}`} 
                            style={{color: colors.white}}
                        >
                            Strategy Feed
                        </a>
                    </NavText>
                </Nav>

                <Nav key={`Trades-${this.props.market}`} id="trades">
                    <NavIcon><Icon20 icon={lineChart} /></NavIcon>
                    <NavText>
                        <a 
                            className="menu-link" 
                            href={`#p=portfolio&market=${this.props.market}&view=${ViewType.trades}`} 
                            style={{color: colors.orange400}}
                        >
                            Trades/Orders
                        </a>
                    </NavText>
                </Nav>

                <Nav key={`LivePnl-${this.props.market}`} id="livePnl">
                    <NavIcon><Icon20 icon={dollar} /></NavIcon>
                    <NavText>
                        <a 
                            className="menu-link" 
                            href={`#p=portfolio&market=${this.props.market}&view=${ViewType.livePnl}`} 
                            style={{color: colors.orange500}}
                        >
                            Live PnL
                        </a>
                    </NavText>
                </Nav>

                { 
                    this.renderStrategies(true, false)
                }

                { 
                    this.renderStrategies(false, true)
                }

                { 
                    this.renderStrategies(false, false)
                }
            </SideNav>
        );
    }
}
