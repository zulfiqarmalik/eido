/// Before importing anything, we must initialise the models!
// console.log('!!!INIT CLIENT!!!');
import { initModels } from 'Client/Model/ClModel';

initModels();

import React from 'react';
import ReactDOM from 'react-dom';

import {render} from 'react-dom';

import { BrowserRouter } from 'react-router-dom';
import { renderRoutes } from 'react-router-config';
import { routesInfo } from './Routes';
import V0MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import baseTheme from 'material-ui/styles/baseThemes/darkBaseTheme';
import * as Colors from 'material-ui/styles/colors';

import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles'; // v1.x

import purple from '@material-ui/core/colors/purple';
import cyan from '@material-ui/core/colors/cyan';
import pink from '@material-ui/core/colors/pink';
import teal from '@material-ui/core/colors/teal';
import red from '@material-ui/core/colors/red';
import CssBaseline from '@material-ui/core/CssBaseline';

// The same Promise API, everywhere.
import * as Promise from 'bluebird'
(global as any).Promise = Promise

const getTheme = () => {
    let overwrites = {
        "palette": {
            "accent2Color": "#001f3f",
            "accent3Color": Colors.tealA100,
            "accent1Color": Colors.tealA200
        }
    };

    return getMuiTheme(baseTheme, overwrites);
}

const theme = createMuiTheme({
    palette: {
        type: 'dark',
        primary: purple,
        secondary: cyan,
        error: red,
        // Used by `getContrastText()` to maximize the contrast between the background and
        // the text.
        contrastThreshold: 3,
        // Used to shift a color's luminance by approximately
        // two indexes within its tonal palette.
        // E.g., shift from Red 500 to Red 300 or Red 700.
        tonalOffset: 0.2
    },

    // overrides: {
    //     MuiTable: {
    //         root: {
    //             backgroundColor: 'red'
    //         }
    //     },
    //     MuiTableCell: {
    //         root: {
    //             backgroundColor: 'red'
    //         }
    //     }
    // }
});

// render(routes, () => {
//     console.log('Completed rendering!');
// });

// if (module.hot) {
//     module.hot.accept('./routes', () => {
//         // reload routes again
//         require('./routes').default;
//         render(routes);
//     });
// }

const AppRouter = () => {
    return (
        <BrowserRouter>
            <React.Fragment>
                <MuiThemeProvider theme={theme}>
                    <CssBaseline />
                    <V0MuiThemeProvider muiTheme={getTheme()}>
                        <div style={{fontFamily: 'Roboto, sans-serif'}}>
                            {renderRoutes(routesInfo)}
                        </div>
                    </V0MuiThemeProvider>
                </MuiThemeProvider>
            </React.Fragment>
        </BrowserRouter>
    )
}
  
render(<AppRouter />, document.querySelector('#app-container'));

