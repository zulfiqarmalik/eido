import React from 'react';

import { eido } from 'Shared/Core/ShCore';
import { Redirect, withRouter, RouteComponentProps } from 'react-router';
import TextField from 'material-ui/TextField';
import AutoComplete from 'material-ui/AutoComplete';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import MenuItem from 'material-ui/MenuItem';
import SelectField from 'material-ui/SelectField';
import { AlphaSubType } from 'Shared/Model/Config/ShAlpha';
import { Category, AlphaOrStrategy } from 'Client/Model/Config/ClConfigBase';
import { Alpha } from 'Client/Model/Config/ClAlpha';
import DelaySelect from '../Common/DelaySelect';

interface Props extends RouteComponentProps<any>, React.Props<any> {
    onOk                : (category: string, alphaType: AlphaSubType, name: string, market: string, universeId: string, delay: number) => void;
    onCancel            : () => void;
    canCancel           : boolean;
    open                : boolean;
    market              : string;
    categories          : Category<AlphaOrStrategy>[];
}

interface State {
    name                : string;
    category            : string;
    alphaType           : AlphaSubType;
    dataSource          : string[];
    delay               : number;
}

export default class DevCreateAlpha extends React.Component<Props, State> {
    constructor(props: Props) {
        super(props);

        let dataSource: string[] = new Array<string>(this.props.categories.length);
        this.props.categories.map((category: Category<Alpha>, index: number) => dataSource[index] = category.name);

        this.state = {
            name: "MyAlpha",
            category: "Research",
            alphaType: AlphaSubType.python,
            dataSource: dataSource,
            delay: 1
        }
    }

    componentDidMount() {
    }

    onOk = () => {
        this.props.onOk(this.state.category, this.state.alphaType, this.state.name, this.props.market, "", this.state.delay);
    }

    onCancel = () => {
        this.props.onCancel();
    }

    onClose = () => {this
        this.props.onCancel();
    }

    onNameChange = (e: any, name: string) => {
        this.setState({
            name: name
        });
    }

    onCategoryChange = (e: any, category: string) => {
        this.setState({
            category: category
        });
    }

    onAlphaTypeChange = (e: any, index: number, type_: string) => {
        let type: AlphaSubType = type_ as AlphaSubType;
        this.setState({
            alphaType: type
        });
    }

    getActions() {
        return [
            <FlatButton
                label="Ok"
                secondary={true}
                keyboardFocused={true}
                onClick={this.onOk}
            />,

            <FlatButton
                label="Cancel"
                primary={true}
                keyboardFocused={false}
                onClick={this.onCancel}
                disabled={!this.props.canCancel}
            />
          ];
    }

    onDelayChange = (delay: number) => {
        this.setState({
            delay: delay
        });
    }

    render() {
        return (
            <Dialog
                title="Create your Alpha"
                actions={this.getActions()}
                modal={true}
                open={this.props.open}
                onRequestClose={this.onClose}
            >
                <TextField
                    value={this.state.name}
                    onChange={this.onNameChange}
                    floatingLabelText="Alpha Name"
                />
                <br/>
                <TextField
                    floatingLabelText="Alpha Category"
                    hintText="Name of the category here"
                    value={this.state.category}
                    onChange={this.onCategoryChange}
                />
                <br/>
                
                <SelectField
                    floatingLabelText="Alpha Type"
                    value={this.state.alphaType}
                    onChange={this.onAlphaTypeChange}
                >
                    <MenuItem value={AlphaSubType.python}  primaryText="Python" />
                    <MenuItem value={AlphaSubType.expr}    primaryText="Expression" />
                </SelectField>
                <br/>

                <DelaySelect 
                    label="Delay"
                    delay={this.state.delay}
                    onDelayChange={this.onDelayChange}
                />
            </Dialog>
        );
    }
}
