import React from 'react';
import { RouteComponentProps } from 'react-router';

import { ObjectID } from "Shared/Core/ObjectID";
import { User } from 'Client/Model/User/ClUser';

import IconButton from 'material-ui/IconButton';
import FontIcon from 'material-ui/FontIcon';
import MenuItem from 'material-ui/MenuItem';
import RaisedButton from 'material-ui/RaisedButton';
import {Toolbar, ToolbarGroup, ToolbarTitle} from 'material-ui/Toolbar';
import CodeEditor from "../Common/CodeEditor"
import * as Colors from 'material-ui/styles/colors';

import { AlphaBucket, Universes as gUniverses, DefMarket as gDefMarket, getDefUniverse, AssetClass } from 'Shared/Model/Config/ShAlpha';
import UniverseSelect from '../Common/UniverseSelect';
import { LTStats } from 'Shared/Model/Stats/LTStats';
import TextField from 'material-ui/TextField';
import SelectField from 'material-ui/SelectField';
import LiveResults from "../Common/LiveResults";
import AlphaSettings from "../Common/AlphaSettings";

import { Alpha } from 'Client/Model/Config/ClAlpha';
import { PromiseUtil } from 'Shared/Core/ShUtil';
import { Cl } from 'Client/Core/ClCore';
import { AlphaOrStrategyInfo } from 'Client/Model/Config/ClConfigBase';

let RunIcon = require('material-ui-icons/PlayArrow').default;
let CancelIcon = require('material-ui-icons/Cancel').default;

const tbStyle = {
    marginLeft: 15,
};

const btStyle = {
    marginRight: 10,
    marginLeft: 10
};

const smallIcon = {
    width: 36,
    height: 36,
};

const mediumIcon = {
    width: 48,
    height: 48,
};

interface Props extends RouteComponentProps<any>, React.Props<any> {
    key             : string;
    alphaInfo       : AlphaOrStrategyInfo;
    alphaId         : ObjectID;
    market          : string;
}

export interface State {
    universes       : string[];
    universeId      : string;
    showAlphaOps    : boolean;
    alpha           : Alpha;
    isRunning       : boolean;
    logMsgs         : string[];

    runPercent      : number;
    subRunPercent   : number;
    superRunPercent : number;
    
    startDate       : number;
    bucket          : AlphaBucket;

    liveStats       : LTStats;
    numStats        : number;

    subLiveStats    ?: LTStats;
    numSubStats     ?: number;
    superLiveStats  ?: LTStats;
    numSuperStats   ?: number;

    showSettings    : boolean;
    showOutputWindow?: boolean;

    anchorEl        ?: any;

    iedit           ?: number;
    alphas          ?: Alpha[];
    market          ?: string;

    modified        : boolean;
    source          : string;

    isMounted       : boolean;
}

export default class DevEdit extends React.Component<Props, State> {
    ace             : any = null;
    aceOut          : any = null;
    commands        : any[];
    container       : any = null;
    
    constructor(props: Props) {
        super(props);
        
        this.ace            = null;
        this.aceOut         = null;
        let alpha           = this.props.alphaId ? this.props.alphaInfo.findById(this.props.alphaId) : null;
        let market          = !alpha ? gDefMarket : alpha.market;
        let assetClass      = !alpha ? AssetClass.Equities : alpha.getAssetClass();

        this.state = { 
            universes       : gUniverses[market],
            universeId      : !alpha ? getDefUniverse(assetClass) : alpha.getUniverseId(),
            showAlphaOps    : false,
            alpha           : alpha as Alpha,
            isRunning       : false,
            logMsgs         : [],

            runPercent      : 0,
            subRunPercent   : 0,
            superRunPercent : 0,
            
            startDate       : 20150101,
            bucket          : AlphaBucket.PriceVolume,

            liveStats       : new LTStats(),
            numStats        : 0,

            subLiveStats    : null,
            numSubStats     : 0,
            superLiveStats  : null,
            numSuperStats   : 0,

            showSettings    : false,
            modified        : false,
            source          : "",

            isMounted       : false
        };

        this.commands       = [{
            name: 'runSim',
            bindKey: { win: 'alt+b', mac: 'command+b', sender: 'editor|cli' },
            exec: this.onRun
        }, {
            name: 'showOutput',
            bindKey: { win: 'Alt+2', mac: 'Command-2', sender: 'editor|cli' },
            exec: this.onShowOutputWindow
        }]
    }

    getEditedAlpha() {
        if (this.state.iedit >= 0 && this.state.iedit < this.state.alphas.length)
            return this.state.alphas[this.state.iedit];

        return null;
    }

    onShowOutputWindow = () => this.setState({showOutputWindow: true});
    onHideOutputWindow = () => this.setState({showOutputWindow: false});

    onShowAlphaOps = (event) => {
        // This prevents ghost click.
        // event.preventDefault();
        
        this.setState({
            showAlphaOps: true,
            anchorEl: event.currentTarget,
        });
    };
    
    onHideAlphaOps = () => {
        this.setState({
            showAlphaOps: false,
        });
    };

    onUniverseChange = (universeId: string) => {
        this.setState({
            universeId: universeId,
            modified: true
        });
    }
    
    onMarketChange = (market: string) => {
        this.setState({
            universes: gUniverses[market],
            universeId: gUniverses[market][0],
            market: market,
            showOutputWindow: false
        });
    }

    onReset() {
        // if (this.ace)
        //     this.ace.editor.setValue(this.originalXML, -1);

        // this.setState({
        //     configXML: this.originalXML
        // });
    }

    initExprMode() {
        if (!this.ace)
            return;

        this.ace.editor.getSession().on('change', (e) => {
            if (e && e.action == "insert" && e.end.row > e.start.row) {
                console.log('cancel event')
                // e.preventDefault() // doesnt work
                // e.stopPropagation()  // doesnt work
                this.ace.editor.find(String.fromCharCode(10))
                this.ace.editor.replaceAll(''); // this work
            }
        });        
    }

    resetUndo(alpha: Alpha) {
        if (!this.ace)
            return;

        if (!alpha._undoMgr) {
            let undoMgr = this.ace.editor.getSession().getUndoManager();
            undoMgr.reset();
            this.ace.editor.getSession().setUndoManager(undoMgr);
            alpha._undoMgr = undoMgr;
        }
        else {
            this.ace.editor.getSession().setUndoManager(alpha._undoMgr);
        }
    }

    onDelete = () => {
        if (!this.state.alpha || !this.props.alphaInfo)
            return;
        
        // this.state.alpha.delete();
        if (this.props.alphaInfo.deleteAlpha(this.state.alpha)) {
            /// Then delete the alpha
            let alphaId = this.state.alpha._id;
            this.state.alpha.delete()
                .then(() => { 
                    console.log(`[DevEdit] Deleted Alpha: ${alphaId}`);
                    this.setState({
                        alpha: null
                    });
                });
        }
    }

    onEditorSave = () => {
        this.onSave(false);
    }

    onSaveClick = () => {
        return this.onSave(false);
    }

    onSave(force: boolean = false) {
        if (!this.ace)
            return PromiseUtil.resolvedPromise(this);

        if (!this.state.alpha)
            return PromiseUtil.resolvedPromise(this);

        if (this.state.alpha.isPublished)
            return PromiseUtil.resolvedPromise(this);

        if (!this.state.modified && !force) {
            console.log(`[DevEdit] Nothing to save ...`);
            return PromiseUtil.resolvedPromise(this);
        }
    
        // console.log(this.ace.editor.getValue());
        let source = this.ace.editor.getValue();
        return this.state.alpha.save(this.props.market, this.state.universeId, source)
            .then(() => {
                this.setState({
                    modified: false,
                    source: source
                });
                this.successAlert("Save", "Successfully saved!");
                return this;
            })
            .catch((err) => {
                this.errorAlert("Save", `Save failed: ${err}`);
                return this;
            });
    }

    successAlert(title: string, message: string) {
        Cl.ui.hintSuccess(title, message);
    }

    errorAlert(title: string, message: string) {
        Cl.ui.hintError(title, message);
    }

    appendLog(log: string) {
        if (log && log.length) {
            let existingLog = this.state.logMsgs;

            if (this.state.logMsgs.length > 100)
                existingLog = ["4><<< Truncated log. Use the download option to download the entire log file >>>"];

            this.setState({
                logMsgs: existingLog.concat(log)
            });
    
        }
    }

    onRunTickSub(alpha, log) {
        let runPercent = Math.round(alpha.percent * 100.0);

        console.log(`[DevEdit] Num SUB alpha stats: ${alpha._liveStats.points.length} [Index: ${alpha._liveStatsIndex}]`);
        
        this.setState({
            subRunPercent: runPercent,
            subLiveStats: alpha._liveStats,
            numSubStats: alpha._liveStatsIndex
        });
    }

    onRunTickSuper(alpha, log) {
        let runPercent = Math.round(alpha.percent * 100.0);
        
        console.log(`[DevEdit] Num SUPER alpha stats: ${alpha._liveStats.points.length} [Index: ${alpha._liveStatsIndex}]`);

        this.setState({
            superRunPercent: runPercent,
            superLiveStats: alpha._liveStats,
            numSuperStats: alpha._liveStatsIndex
        });
    }

    onRunTick = (alpha: Alpha, log: string) => {
        if (!this.state.isMounted)
            return;

        if (alpha.isSubAlpha())
            return this.onRunTickSub(alpha, log);

        else if (alpha.isSuperAlpha())
            return this.onRunTickSuper(alpha, log);

        this.appendLog(log);

        let runPercent = Math.round((alpha.percent * 100.0));
        
        this.setState({
            runPercent: runPercent,
            liveStats: alpha._liveStats,
            numStats: alpha._liveStatsIndex
        });

        let elem = document.getElementById('dev-output');
        elem.scrollTop = elem.scrollHeight;
    }

    onFinished = (alpha, log) => {
        if (alpha.isSubAlpha()) {
            this.setState({
                subRunPercent: 100.0
            });
            return;
        }
        else if (alpha.isSuperAlpha()) {
            this.setState({
                superRunPercent: 100.0
            });
            return;
        }

        this.setState({
            isRunning: false
        });

        this.appendLog(log);
    }

    onPublish = () => {
        this.onSave()
            .then(() => { 
                this.setState({
                    showOutputWindow: true,
                    isRunning: true,
                    runPercent: 0,
                    subRunPercent: 0,
                    superRunPercent: 0,
                    subLiveStats: null,
                    superLiveStats: null,
                    logMsgs: []
                });
        
                this.state.alpha.publish()
                    .then(() => {
                        this.successAlert("Publish", `Successfully published alpha: ${this.state.alpha.name}`)
                    })
                    .catch((err) => { 
                        this.errorAlert("Publish", `Failed to publish alpha: ${this.state.alpha.name}. Error: ${err}`);
                        this.setState({
                            isRunning: false
                        });
                    });
            });
    }

    onRun = () => {
        if (!this.ace)
            return;

        if (!this.state.alpha)
            return;

        this.onSave()
            .then(() => {
                this.setState({
                    showOutputWindow: true,
                    isRunning: true,
                    runPercent: 0,
                    subRunPercent: 0,
                    superRunPercent: 0,
                    liveStats: null,
                    subLiveStats: null,
                    superLiveStats: null,
                    logMsgs: []
                });
        
                this.state.alpha.run(User.user.wsClient, this.onRunTick, this.onFinished)
                    .catch(() => { 
                        this.setState({
                            isRunning: false
                        });
                    });
            });
    }
    
    onCancel = () => {
        this.setState({
            isRunning: false
        });

        this.state.alpha.cancel();
    }

    onCancelSettings = () => {
        this.setState({
            showSettings: false
        });
    }

    componentWillUnmount() {
        this.setState({
            isMounted: false
        });
    }

    componentDidMount() {
        this.setState({
            isMounted: true
        });

        if (!this.props.alphaId)
            return;

        let alpha = this.props.alphaInfo.findById(this.props.alphaId);

        if (alpha) {
            alpha.load(false)
                .then((loadedAlpha: Alpha) => {
                    if (loadedAlpha) {
                        this.setState({
                            alpha:      loadedAlpha, 
                            market:     loadedAlpha.market,
                            universeId: loadedAlpha.getUniverseId(),
                            isRunning:  loadedAlpha.isRunning,
                            startDate:  parseInt(loadedAlpha.startDate),
                            bucket:     loadedAlpha.bucket,
                            modified:   loadedAlpha._modified,
                            source:     loadedAlpha.getSource()
                        });

                        this.resetUndo(loadedAlpha);
                    }
                });
        }
    }

    getSource(): string {
        return this.state.source;
        // if (this.state.alpha)
        //     return this.state.alpha.getSource();
        // return "";
    }

    canEditSource(): boolean {
        if (this.state.alpha && !this.state.alpha.isPublished)
            return true;
        return false;
    }

    isRunning(): boolean {
        return this.state.isRunning;
    }

    onStartDateChanged = (event: any, value: string) => {
        if (this.state.alpha) {
            this.state.alpha.startDate = value;

            this.setState({
                startDate: parseInt(value),
                modified: true
            });
        }
    }

    onAlphaBucketChanged = (e: any, index: number, bucket_: string) => {
        let bucket: AlphaBucket = bucket_ as AlphaBucket;
        if (this.state.alpha) {
            this.state.alpha.bucket = bucket;

            this.setState({
                bucket: bucket
            });
        }
    }

    onSaveSettings = () => {
        this.successAlert("Save", "Settings saved successfully");
        this.setState({
            showSettings: false
        });
    }

    onCodeChanged = (source: string) => {
        this.setState({
            modified: true,
            source: source
        });

        if (this.state.alpha) {
            this.state.alpha.setSource(source);
            if (this.ace)
                this.state.alpha._undoMgr = this.ace.editor.getSession().getUndoManager();
            this.state.alpha._modified = true;
        }
    }

    renderPythonEditor() {
        return (
            <CodeEditor
                className="editor"
                mode="python"
                theme="monokai"
                key={`PyEd-${this.props.alphaId}`}
                readOnly={!this.canEditSource()}
                value={this.getSource()}
                fontSize={12}
                width="99%"
                height={(window.innerHeight - 150).toString() + 'px'}
                showPrintMargin={false}
                onSave={this.onEditorSave}
                onChange={this.onCodeChanged}
                wrapEnabled
                ref={instance => { this.ace = instance; }}
                setOptions={{
                    enableBasicAutocompletion: false,
                    enableLiveAutocompletion: false,
                    enableSnippets: true,
                    showLineNumbers: true,
                    tabSize: 4,
                    useSoftTabs: true,
                }}
                commands={this.commands} 
            />
        );
    }

    renderExpressionEditor() {
        return (
            <CodeEditor
                className="editor"
                mode="python"
                theme="monokai"
                key={`ExprEd-${this.props.alphaId}`}
                readOnly={!this.canEditSource()}
                value={this.getSource()}
                fontSize={14}
                width="99%"
                height='150px'
                showPrintMargin={false}
                showGutter={false}
                wrapEnabled={true}
                onSave={this.onEditorSave}
                onChange={this.onCodeChanged}
                ref={instance => { this.ace = instance; this.initExprMode(); }}
                setOptions={{
                    enableBasicAutocompletion: true,
                    enableLiveAutocompletion: true,
                    enableSnippets: true,
                    showLineNumbers: true,
                    tabSize: 4,
                    useSoftTabs: true,
                }}
                commands={this.commands} 
            />
        );
    }

    renderCodeEditor() {
        if (!this.state.alpha || this.state.alpha.isPython()) 
            return this.renderPythonEditor();

        return this.renderExpressionEditor();
    }

    getTitleLabel() {
        if (!this.state.alpha)
            return "";

        // let prefix = "Python - ";
        // if (this.state.alpha.isExpression())
        //     prefix = "Expression - ";

        let modString = !this.state.modified ? "" : "* ";

        return modString + this.state.alpha.name;
    }

    renderToolbar() {
        return (
            <Toolbar key={`Toolbar-${this.state.alpha ? this.props.alphaId : ""}`}>
                <ToolbarGroup style={tbStyle}>
                    <UniverseSelect 
                        market={this.props.market} 
                        universeId={this.state.universeId} 
                        onUniverseChange={this.onUniverseChange} 
                        readOnly={this.state.alpha && this.state.alpha.isPublished}
                    />

                    <SelectField 
                        value={this.state.bucket} 
                        onChange={this.onAlphaBucketChanged} 
                        style={{width: 200}} 
                        disabled={this.state.alpha && this.state.alpha.isPublished}
                    >
                        {Object.keys(AlphaBucket).map((bucket) => {
                            return <MenuItem key={bucket} value={bucket}  primaryText={bucket} />
                        })}
                    </SelectField>

                    <TextField
                        hintText="Start Date" 
                        value={this.state.startDate}
                        onChange={this.onStartDateChanged}
                        style={{width: 100}}
                        disabled={this.state.alpha && this.state.alpha.isPublished}
                    />

                    <IconButton 
                        style={mediumIcon}
                        onClick={() => this.setState({showSettings: true})}
                    >
                        <FontIcon color={Colors.deepOrange500} className="material-icons">build</FontIcon>
                    </IconButton>

                </ToolbarGroup>
                
                <ToolbarGroup>
                    <ToolbarTitle 
                        text={this.getTitleLabel()} 
                        className={!this.state.modified ? "dev-alpha-title" : "dev-alpha-title-modified"}
                    />
                </ToolbarGroup>

                <ToolbarGroup>
                    <RaisedButton
                        secondary={true}
                        className={!this.isRunning() ? "" : "hidden"}
                        icon={<RunIcon color={Colors.deepOrange500} />}
                        style={btStyle}
                        onClick={this.onRun}
                    />

                    <RaisedButton
                        className={this.isRunning() ? "" : "hidden"}
                        backgroundColor={Colors.deepOrange500}
                        icon={<CancelIcon color={"white"} />}
                        style={btStyle}
                        onClick={this.onCancel}
                    />

                    <IconButton 
                        style={mediumIcon} 
                        onClick={this.onSaveClick}
                        className={this.state.alpha && !this.state.alpha.isPublished ? "" : "hidden"}
                    >
                        <FontIcon color={Colors.grey400} className="material-icons">save</FontIcon>
                    </IconButton>                    

                    <IconButton style={mediumIcon} href={`#p=developer&market=${this.state.market}&alphaId=${this.props.alphaId}&viewResults=true`}>
                        <FontIcon color={Colors.grey400} className="material-icons">trending_up</FontIcon
                    ></IconButton>

                    <IconButton style={mediumIcon} onClick={this.onPublish} className={this.state.alpha && !this.state.alpha.isPublished ? "" : "hidden"}>
                        <FontIcon color={Colors.limeA400} className="material-icons">publish</FontIcon>
                    </IconButton>

                    <IconButton style={mediumIcon} onClick={this.onDelete} className={this.state.alpha && !this.state.alpha.isPublished ? "" : "hidden"}>
                        <FontIcon color={Colors.deepOrange500} className="material-icons">delete</FontIcon>
                    </IconButton>

                </ToolbarGroup>
            </Toolbar>
        );
    }
    
    render() {
        // var ReactToastr = require("react-toastr");
        // var {ToastContainer} = ReactToastr; // This is a React Element.
        // // For Non ES6...
        // // var ToastContainer = ReactToastr.ToastContainer;
        // var ToastMessageFactory = React.createFactory(ReactToastr.ToastMessage.animation);

        return (
            <div>
{/*                 
                <ToastContainer 
                    ref={(input) => { this.container = input; }}
                    toastMessageFactory={ToastMessageFactory}
                    className="toast-bottom-right" 
                /> */}

                {this.renderToolbar()}
                {this.renderCodeEditor()}
                {/* {this.renderLiveResults()} */}
                {this.renderAlphaSettings()}
            </div>
        );
    }

    renderAlphaSettings() {
        return (
            <AlphaSettings
                location={this.props.location}
                match={this.props.match}
                history={this.props.history}
                key={this.state.alpha ? `AlphaSetting-${this.state.alpha._id}` : "AlphaSettings"}
                alpha={this.state.alpha ? this.state.alpha : null}
                onOk={this.onSaveSettings}
                onCancel={this.onCancelSettings}
                canCancel={true}
                open={this.state.showSettings}
                showOperations={true}
                showOptimiser={false}
                market={this.props.market}
            />
        );
    }

    renderLiveResults() {
        return (
            <LiveResults
                showOutputWindow={this.state.showOutputWindow}
                onShowOutputWindow={this.onShowOutputWindow}
                onHideOutputWindow={this.onHideOutputWindow}
                logMsgs={this.state.logMsgs}
                isRunning={this.state.isRunning}
                
                runPercent={this.state.runPercent}
                liveStats={this.state.liveStats}
                numStats={this.state.numStats}
                
                subRunPercent={this.state.subRunPercent}
                subLiveStats={this.state.subLiveStats}
                numSubStats={this.state.numSubStats}
                showSub={this.state.subLiveStats ? true : false}

                superRunPercent={this.state.superRunPercent}
                superLiveStats={this.state.superLiveStats}
                numSuperStats={this.state.numSuperStats}
                showSuper={this.state.subLiveStats ? true : false}

                alpha={this.state.alpha}
                key={`${this.state.isRunning}_${this.state.logMsgs.length}`}
            />
        );
    }

    getOutputString() {
        if (this.state.isRunning)
            return "Running Simulation [" + this.state.runPercent.toString() + " %]";
        return "Output";
    }

    noOp() {}
}
