import React from 'react';

import { ObjectID } from "Shared/Core/ObjectID";

import { RouteComponentProps } from 'react-router';

let pencil = require('react-icons-kit/fa/pencil').pencil;

import { Category, AlphaOrStrategy, AlphaOrStrategyInfo } from 'Client/Model/Config/ClConfigBase';
import { AppPages } from 'Client/Browser/Src/AppState';

let SvgIcon = require('react-icons-kit').default;
const Icon20 = props => <SvgIcon size={props.size || 20} icon={props.icon} />;
let SideNav = require('react-sidenav').default;
let { Nav, NavIcon, NavText } = require('react-sidenav');

export interface Props extends RouteComponentProps<any>, React.Props<any> {
    onShowAlpha     ?: () => void;
    onShowResults   ?: () => void;
    alphaInfo       : AlphaOrStrategyInfo;
    category        : Category<AlphaOrStrategy>;
    title           : string;
    selected        ?: string;
    showPublished   : boolean;
    alphaId         ?: ObjectID;
    viewResults     ?: boolean;
}

export default class DevMenuAlphas extends React.Component<Props, {}> {
    constructor(props: Props) {
        super(props);
    }

    getSelected(): string {
        if (this.props.alphaId)
            return this.props.alphaId.toString();
        else if (this.props.category && this.props.category.objects.length)
            return this.props.category.objects[0]._id.toString();
        return "";
    }

    render() {
        return (
            <SideNav 
                highlightBgColor="#0097A7"
                hoverBgColor="#0097A7"
                defaultSelected={this.getSelected()}
                selected={this.getSelected()}
                key={this.props.alphaInfo ? this.props.alphaInfo.objects.length.toString() : "0"}
            >
                { 
                    (this.props.alphaInfo ? this.props.alphaInfo.objects : [])
                        .filter((alpha) => alpha.isPublished == this.props.showPublished)
                        // .filter((alpha) => alpha.category == this.props.title)
                        .map((alpha, ai) => { 
                            return (
                                <Nav key={alpha._id.toString()} id={alpha._id.toString()}>
                                    <NavIcon><Icon20 icon={pencil} /></NavIcon>
                                    <NavText>
                                        <a 
                                            className="menu-link" 
                                            href={`#p=${AppPages.developer}&market=${alpha.market}&alphaId=${alpha._id}&viewResults=${alpha.isPublished}`} 
                                            style={{color: "white"}}
                                        >
                                            {alpha.name}
                                        </a>
                                    </NavText>
                                </Nav>
                                
                            );
                        }) 
                }
            </SideNav>
        );
    }
}
