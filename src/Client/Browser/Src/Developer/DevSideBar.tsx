import React from 'react';

import { ObjectID } from "Shared/Core/ObjectID";

import { RouteComponentProps } from 'react-router';

import ContentAdd from 'material-ui/svg-icons/content/add';
import FloatingActionButton from 'material-ui/FloatingActionButton';
import DevMenuAlphas from './DevMenuAlphas';

import IconMenu from 'material-ui/IconMenu';
import MenuItem from 'material-ui/MenuItem';
import FlatButton from 'material-ui/FlatButton';
import MoreVertIcon from 'material-ui/svg-icons/navigation/more-vert';

import {Toolbar, ToolbarTitle} from 'material-ui/Toolbar';
import { Alpha } from 'Client/Model/Config/ClAlpha';
import { User } from 'Client/Model/User/ClUser';
import TextField from 'material-ui/TextField';
import { Category, AlphaOrStrategyInfo, AlphaOrStrategy } from 'Client/Model/Config/ClConfigBase';

import { Divider } from 'material-ui';

// const tooltip = (
//     <Tooltip id='tooltip'><strong>Holy guacamole!</strong> Check this info.</Tooltip>
// );

// function TT(msg, title) {
//     if (title)
//         return <Tooltip ><strong>{title}</strong>{msg}</Tooltip>
//     return <Tooltip >{msg}</Tooltip>
// }

const buttonStyles = {
    height: 52,
    width: 32,
    minWidth: 0,
    padding: 0,
    textAlign: "center" as any
};

interface Props extends RouteComponentProps<any>, React.Props<any> {
    alphaInfo           : AlphaOrStrategyInfo;
    alphaId             : ObjectID;
    viewResults         : boolean;
    createAlpha         : () => void;
    categories          : Category<AlphaOrStrategy>[];

    onCategoryChange    : (category: Category<AlphaOrStrategy>, alphaInfo: AlphaOrStrategyInfo, alpha: AlphaOrStrategy) => void;
}

interface State {
    showPublished       : boolean;
    title               : string;
    allowEditing        : boolean;
    allowGroupPnl       : boolean;
    isEditing           : boolean;
    alphaInfo           : AlphaOrStrategyInfo;
    category            : Category<AlphaOrStrategy>;
}

export default class DevSideBar extends React.Component<Props, State> {
    constructor(props: Props) {
        super(props);

        this.state = {
            showPublished: false,
            allowEditing: false,
            allowGroupPnl: false,
            isEditing: false,
            alphaInfo: this.props.alphaInfo,
            title: "",
            category: null
        };
    }

    componentDidMount() {
        let user = User.user;
        let stateObj = {};

        if (user && user.isAdminOrBetter()) {
            stateObj = {
                allowEditing: true,
                allowGroupPnl: true
            };
        }
        else {
            stateObj = {
                allowEditing: false,
                allowGroupPnl: false
            };
        }

        let categories = this.props.categories;
        let category = null;

        if (categories.length && this.props.alphaId && this.props.alphaInfo) {
            let alpha: AlphaOrStrategy = this.props.alphaInfo.findById(this.props.alphaId);

            for (let i = 0; alpha && i < categories.length && !category; i++) {
                if (categories[i].name == alpha.category) 
                    category = categories[i];
            }
        }

        if (!category) {
            category = categories.length ? categories[0] : new Category<Alpha>("Research");
        }

        stateObj["title"] = category.name;
        stateObj["category"] = category;
        stateObj["alphaInfo"] = new AlphaOrStrategyInfo(this.props.alphaInfo.market, category.objects);

        this.setState(stateObj);
    }

    componentDidUpdate() {
        if (this.state.isEditing) {
            this.setState({
                isEditing: false
            });
        }
    }

    onSearch = (event: any, value: string) => {
        if (!value) {
            /// This will clear out the search string in alphaInfo ...

            this.setState({
                alphaInfo: new AlphaOrStrategyInfo(this.props.alphaInfo.market, this.state.category.objects)
            });

            return;
        }

        let searchAlphas = this.props.alphaInfo.search(value);
        let alphaInfo = new AlphaOrStrategyInfo(this.props.alphaInfo.market, searchAlphas);

        alphaInfo.searchString = value;

        this.setState({
            alphaInfo: alphaInfo
        });
    }

    onCategoryChange = (category: Category<AlphaOrStrategy>) => {
        this.setState({ 
            category: category, 
            title: category.name,
            alphaInfo: new AlphaOrStrategyInfo(this.props.alphaInfo.market, category.objects)
        });

        if (this.props.onCategoryChange) {
            this.props.onCategoryChange(category, this.props.alphaInfo, category.objects.length ? category.objects[0] : null);
        }
    }

    render() {
        return (
            <div className='full-side-bar'>
                <Toolbar>
                    <ToolbarTitle text={this.state.title} className={!this.state.showPublished ? "dev-alpha-title" : "dev-alpha-pubtitle"} />

                    {/* <IconMenu
                        menuStyle={{ width: 250 }}
                        iconButtonElement={
                            <FlatButton 
                                style={buttonStyles} 
                                icon={<SearchIcon />}
                            />
                        }
                        targetOrigin={{ horizontal: 'left', vertical: 'top' }}
                        anchorOrigin={{ horizontal: 'left', vertical: 'bottom' }}
                    >
                        <TextField
                            hintText="Search ..." 
                            style={{width: "100%"}}
                        />
                    </IconMenu> */}

                    <IconMenu
                        iconButtonElement={
                            <FlatButton 
                                style={buttonStyles} 
                                icon={<MoreVertIcon />}
                            />
                        }
                        targetOrigin={{ horizontal: 'left', vertical: 'top' }}
                        anchorOrigin={{ horizontal: 'left', vertical: 'bottom' }}
                    >
                        {
                            this.props.categories.map((category: Category<Alpha>) => {
                                return (
                                    <MenuItem 
                                        primaryText={category.name} 
                                        key={category.name}
                                        onClick={() => this.onCategoryChange(category)} 
                                        checked={this.state.title == category.name}
                                    />
                                );
                            })
                        }

                        <Divider />

                        {/* <MenuItem primaryText="Development" onClick={() => this.setState({ title: "Development Alphas", showPublished: false })} /> */}
                        <MenuItem 
                            primaryText="Published" 
                            key="Published"
                            onClick={() => this.setState({ showPublished: !this.state.showPublished })} 
                            checked={this.state.showPublished}
                        />
                    </IconMenu>

                </Toolbar>

                <TextField
                    hintText="Search ..." 
                    style={{width: 270, marginLeft: 20}}
                    value={this.state.alphaInfo.searchString}
                    onChange={this.onSearch}
                />

                <DevMenuAlphas 
                    location={this.props.location}
                    match={this.props.match}
                    history={this.props.history}
                    alphaId={this.props.alphaId}
                    viewResults={this.props.viewResults}
                    alphaInfo={this.state.alphaInfo}
                    category={this.state.category}
                    showPublished={this.state.showPublished}
                    key={`DevMenuAlphas-${this.state.category ? this.state.category.objects.length : 0}-${this.state.category ? this.state.category.name : ""}-${this.props.alphaInfo.searchString}`}
                    title={this.state.title}
                />

                <FloatingActionButton className="dev-create-alpha" secondary={true} onClick={ () => { if (this.props.createAlpha) this.props.createAlpha(); } }>
                    <ContentAdd />
                </FloatingActionButton>

            </div>
        );
    }
}