import React from 'react';
import { RouteComponentProps } from 'react-router';

import { ObjectID } from "Shared/Core/ObjectID";
import { Alpha } from 'Client/Model/Config/ClAlpha';
import DevSideBar from "./DevSideBar";
import DevEdit from "./DevEdit";
import DevCreateAlpha from './DevCreateAlpha';
import ViewAlphaResults from '../Common/ViewAlphaResults';
import { DefMarket as gDefMarket, AlphaSubType, getDefUniverse, ShAlpha } from 'Shared/Model/Config/ShAlpha';
import * as qs from 'query-string';

import RGL, { WidthProvider } from "react-grid-layout";
import { Debug, PromiseUtil } from 'Shared/Core/ShUtil';
import { AppPages } from '../AppState';
import { Category, AlphaOrStrategyInfo, AlphaOrStrategy } from 'Client/Model/Config/ClConfigBase';
import { Cl } from 'Client/Core/ClCore';
const ReactGridLayout = WidthProvider(RGL);

export interface Props extends RouteComponentProps<any>, React.Props<any> {
}

export interface State {
    children            : any[];
    showCreateDialog    : boolean;
    alphaId             : ObjectID;
    viewResults         : boolean;
    market              : string;
    alphaInfo           : AlphaOrStrategyInfo;
    categories          : Category<AlphaOrStrategy>[];
}    

export default class Dev extends React.Component<Props, State> {
    alphaId             : ObjectID = null;
    market              : string = "";
    viewResults         : boolean = false;

    constructor(props: Props) {
        super(props);

        this.state = {
            children        : [],
            showCreateDialog: false,
            alphaId         : null,
            viewResults     : this.viewResults,
            market          : this.market,
            categories      : [],
            alphaInfo       : new AlphaOrStrategyInfo(gDefMarket, null)
        };    
    }

    getAllAlphas(market: string): Promise<AlphaOrStrategyInfo> {
        console.log("[Dev] Getting all alphas for the user ...");

        return Alpha.getAllAlphas(market)
            .then((alphaInfo) => {
                console.log("[Dev] Alpha count: %d", alphaInfo.alphas.length);
                return alphaInfo;
            });
    }

    updateState() {
        let promise = null;

        // if (this.alphaId) {
        //     let alpha = new Alpha();
        //     alpha._id = this.alphaId;
        //     promise = Alpha.loadIds([this.alphaId], true, true);
        // }
        // else {
        //     promise = this.getAllAlphas(this.market)
        // }

        let ainfo: AlphaOrStrategyInfo = null;

        this.getAllAlphas(this.market)
            .then((alphaInfo: AlphaOrStrategyInfo) => {
                ainfo = alphaInfo;

                if (this.alphaId && !alphaInfo.findById(this.alphaId))
                    return Alpha.loadIds([this.alphaId], true, true);
                return PromiseUtil.resolvedPromise(new AlphaOrStrategyInfo("", [])); 
            })
            .then((iinfo: AlphaOrStrategyInfo) => {
                if (iinfo) {
                    iinfo.objects.map((info: AlphaOrStrategy) => {
                        ainfo.add(info);
                    });
                }

                return ainfo;
            })
            .then((alphaInfo: AlphaOrStrategyInfo) => {
                this.setState({
                    alphaInfo: alphaInfo,
                    market: this.market,
                    viewResults: this.viewResults,
                    categories: alphaInfo ? alphaInfo.getCategories() : []
                });

                if (alphaInfo.length) {
                    if (this.alphaId) {
                        let alpha = alphaInfo.findById(this.alphaId);
                    
                        if (alpha) {
                            alpha.load(true)
                                .then((loadedAlpha) => {
                                    if (loadedAlpha)
                                        this.setState({ alphaId: loadedAlpha._id })
                                    else
                                        this.setState({ alphaId: null });
                                });
                
                            return true;
                        }
                        else {
                            this.gotoAlpha(null, null, false);
                            // this.setState({
                            //     showCreateDialog: true
                            // });
                        }
                    }
                    else {
                        let firstAlphaIndex = alphaInfo.findFirstUnpublishedObject();
                        this.gotoAlpha(alphaInfo, alphaInfo.alphas[firstAlphaIndex]._id, this.viewResults);
                    }
                }
                else {
                    this.gotoAlpha(null, null, false);
                    // this.setState({
                    //     showCreateDialog: true
                    // });
                }
            });

        return true;
    }

    checkArgs(alphaInfo: AlphaOrStrategyInfo): boolean {
        if (!alphaInfo)
            return false;

        const args = qs.parse(this.props.location.hash);
        let viewResults = args.viewResults == 'true';

        if (args.p != AppPages.developer)
            return false;

        if (!args || (((this.alphaId && args.alphaId == this.alphaId.toString()) || !args.alphaId) && viewResults == this.viewResults && args.market == this.market))
            return false;
    
        this.alphaId = args.alphaId ? ObjectID.create(args.alphaId) : null;
        this.viewResults = viewResults;
        this.market = args.market as string;

        return this.updateState();
    }

    componentDidMount() {
        return this.updateState();
    }

    componentDidUpdate() {
        this.checkArgs(this.state.alphaInfo);
    }

    onCategoryChange = (category: Category<Alpha>, alphaInfo: AlphaOrStrategyInfo, alpha: Alpha) => {
        if (alpha) {
            this.alphaId = alpha._id;
            this.viewResults = alpha.isPublished;

            // this.setState({
            //     alphaId: this.alphaId,
            //     viewResults: this.viewResults
            // });

            this.gotoAlpha(alphaInfo, this.alphaId, this.viewResults);
            this.updateState();
        }
    }

    gotoAlpha(alphaInfo: AlphaOrStrategyInfo, alphaId: ObjectID, viewResults: boolean) {
        if (!alphaInfo || !alphaInfo.length)
            return;

        let baseUrl = `/dk/main#p=${AppPages.developer}`;
        let url;

        if (alphaId) {
            url = `${baseUrl}&market=${this.market}&alphaId=${alphaId}&viewResults=`;
            
            if (viewResults)
               url += 'true';
            else
                url += 'false';
        }
        else {
            url = baseUrl;
        }

        console.log("[Dev] Going to: %s", url);
        this.props.history.push(url);
    }

    hasAlphas() {
        return this.state.alphaInfo.length > 0;
    }    

    onCreateAlpha = (category: string, alphaType: AlphaSubType, name: string, market: string, universeId: string, delay: number) => {
        if (!market) {
            Cl.ui.hintError("Invalid Market", "Invalid market specified while creating a new alpha!");
            return;
        }
        if (!category) {
            Cl.ui.hintError("Invalid Category", "Invalid category specified while creating a new alpha!");
            return;
        }
        
        Debug.assert(market, 'Invalid market specified while creating a new alpha!');

        if (!universeId)
            universeId = getDefUniverse(ShAlpha.getAssetClassForMarket(market));

        console.log("[Dev] Creating alpha: %s/%s [%s/%s]", category, name, market, universeId);

        const args = qs.parse(this.props.location.hash);

        Alpha.create(category, alphaType, name, market, universeId, delay)
            .then((alpha: Alpha) => {
                this.setState({
                    showCreateDialog: false
                });

                this.gotoAlpha(this.state.alphaInfo, alpha._id, this.state.viewResults);
            });
    }

    onCancelCreateAlpha = () => {
        this.setState({
            showCreateDialog: false
        });
    } 

    get alphaIdKey(): string {
        if (this.state.alphaId)
            return this.state.alphaId.toString();
        return "";
    }

    getMainView() {
        const args = qs.parse(this.props.location.hash);
        let viewResults = args.viewResults == 'true';

        if (!viewResults) {
            return (
                <DevEdit
                    key={`DevEdit-${this.alphaIdKey}`}
                    location={this.props.location}
                    match={this.props.match}
                    history={this.props.history}
                    alphaId={this.state.alphaId}
                    alphaInfo={this.state.alphaInfo}
                    market={this.state.market}
                />
            );
        }
        if (this.state.alphaId) {
            return (
                <ViewAlphaResults
                    key={`ViewAlphaResults-${this.alphaIdKey}`}
                    location={this.props.location}
                    match={this.props.match}
                    history={this.props.history}
                    alphaId={this.state.alphaId}
                    alphaInfo={this.state.alphaInfo}
                    market={this.state.market}
                />
            );
        }

        return (
            <div/>
        );
    }    

    createDialog() {
        return (
            <DevCreateAlpha
                location={this.props.location}
                match={this.props.match}
                categories={this.state.categories}
                history={this.props.history}
                key={this.state.alphaInfo.length}
                onOk={this.onCreateAlpha}
                onCancel={this.onCancelCreateAlpha}
                canCancel={true}
                open={this.state.showCreateDialog}
                market={this.state.market}
            />
        );
    }

    createUI() {
        return (
            <div>
                {this.getMainView()}
                {this.createDialog()}
            </div>
        );
    }

    children() {
        if (!this.state.alphaInfo.alphas.length) {
            return this.createDialog();
        }

        /// Otherwise we show just the normal UI
        return this.createUI();
    }

    render() {
        return (
            <ReactGridLayout
                className="layout"
                style={{height: "100%"}}
                autoSize={false}
                margin={[1, 1]}
                verticalCompact={true}
                rowHeight={(window.innerHeight - 100.0) / 10.0}
            >
                <div 
                    key="1"
                    data-grid={{ x: 0, y: 0, w: 2, h: 10, static: true }}
                >
                    <DevSideBar 
                        location={this.props.location}
                        match={this.props.match}
                        history={this.props.history}
                        key={`DevSideBar-${this.state.alphaInfo.length}`} 
                        alphaInfo={this.state.alphaInfo} 
                        categories={this.state.categories}
                        alphaId={this.alphaId}
                        viewResults={this.state.viewResults}
                        createAlpha={() => this.setState({ showCreateDialog: true })}
                        onCategoryChange={this.onCategoryChange}
                    /> 
                </div>
                <div 
                    key="2" 
                    data-grid={{ x: 2, y: 0, w: 10, h: 10, static: true }}
                    className="main-content v-scrollable" 
                >
                    {this.children()}
                </div>
            </ReactGridLayout>
        );
    }
}