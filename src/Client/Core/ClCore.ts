import { Env, eido, IError, CoreUtil as ShCoreUtil, IUser } from "Shared/Core/ShCore";
import { Debug } from "Shared/Core/ShUtil";

export interface ClientUI {
    alert(title: string, message: string);
    okCancel(title: string, message: string, onOk?: () => void, onCancel?: () => void);
    hintSuccess(title: string, message: string);
    hintError(title: string, message: string);

    setLoading(loading: boolean, loadingState: any);
    isLoading();
}

class DefaultClientUI implements ClientUI {
    private _loading: boolean = false;
    
    isLoading() {
        return this._loading
    }

    get loading(): boolean {
        return this._loading;
    }

    setLoading() {
        this._loading = true;
    }

    alert(title: string, message: string) {
        alert(message);
    }

    okCancel(title: string, message: string) {
        alert(message);
    }

    hintSuccess(title: string, message: string) {
        alert(message);
    }

    hintError(title: string, message: string) {
        alert(message);
    }
}

export class ClientInfo {
    remote          : string = "http://localhost:4000/";
    apiRoot         : string = "/eido";
    apiVersion      : string = "/v1";
    sessionId       : string = "";

    ui              : ClientUI = new DefaultClientUI();
}

export let Cl = new ClientInfo();

export class CoreUtil implements ShCoreUtil {
    apiRoot(): string {
        return Cl.apiRoot;
    }

    apiVersion(): string {
        return Cl.apiVersion;
    }

    createError(code: number, msg: string): IError {
        // return new ServerError(code, msg);
        return null;
    }

    user(): IUser {
        return null;
    }
}

export default function initCore() {
}

eido.coreUtil = new CoreUtil();

if (Env.release || Env.prod) {
    console.log     = function() {}
    console.debug   = function() {}
    console.info    = function() {}
    console.warn    = function() {}
    console.error   = function() {}

    Debug.assert    = function() {}
    Debug.throwIf   = function() {}
}

