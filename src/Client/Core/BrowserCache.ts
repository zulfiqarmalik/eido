// import { eido } from '../../Shared/Core/ShCore';
// import { Debug, PromiseUtil } from '../../Shared/Core/ShUtil';

// ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// /// General purpose client cache that uses local storage
// ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// export class Cache {
//     name            : string = "";
//     container       : any = window.localStorage;

//     constructor(name: string, container: any = window.localStorage) {
//         this.name = name;
//         this.container = container;
//     }
    
//     getUid(id: string) {
//         return `${this.name}.${id}`;
//     }
    
//     put(object: CObject) {
//         var id = object._id || (object.getId ? object.getId() : null);
        
//         Debug.assert(id, 'Invalid object that has neither an _id nor a valid getId implementation!');
        
//         // var uid = this.getUid(id);
//         return this.set(id, object.cleanClone({
//             level: SecrecyLevel.all
//         }));
//     }

//     set(id: string, value: any, noMarshal: boolean = false) {
//         id = this.getUid(id);

//         var valueStr;
//         try {
//             if (!noMarshal && (eido.sh.Object.isModelObject(value) || value instanceof Array)) {
//                 value = value.cleanClone({
//                     level: eido.SecrecyLevel.all
//                 });
//             }

//             valueStr = JSON.stringify(value);
//         }
//         catch (e) {
//             valueStr = value;
//         }
        
//         this.container.setItem(id, valueStr);
        
//         return true;
//     }
    
//     get(id, noMarshal) {
//         id = this.getUid(id);
//         var objectStr = this.container[id];
        
//         if (!objectStr)
//             return undefined;
            
//         var object = objectStr;
            
//         try {
//             object = JSON.parse(objectStr);
//         }
//         catch (e) {
//             object = objectStr;
//         }
        
//         if (typeof object === 'object' && !noMarshal) {
//             // if (object instanceof Array) {
//             //     for (var i = 0; i < object.length; i++) {
//             //         var item = object[i];
//             //         if (item.__classId) { // this is for a normal non-array object
//             //             var cls = eido.Model.getClassFromClassId(item.__classId);
//             //             if (cls) {
//             //                 // convert the data into that model ...
//             //                 item = (new cls()).copyFrom(cls, {
//             //                     level: eido.SecrecyLevel.all
//             //                 });
                            
//             //                 object[i] = item;
//             //             }
//             //         }
//             //     }
//             // }
//             // else if (object.__classId) { // this is for a normal non-array object
//                 var classId = object.__classId;

//                 if (!classId && object instanceof Array)
//                     classId = 'array';

//                 if (classId) {
//                     var cls = eido.Model.getClassFromClassId(classId);
//                     if (cls) {
//                         // convert the data into that model ...
//                         object = (new cls()).copyFrom(object, {
//                             level: eido.SecrecyLevel.all
//                         });
                        
//                     }
//                 }
//             // }
//         }
            
//         return object;
//     }

//     initCachedVariable(self, name, defaultValue) {
//         Object.defineProperty(self, name, {
//             enumerable: true,
//             configurable: false,

//             get() {
//                 var value = this.get(name);

//                 if (value === undefined)
//                     return defaultValue;

//                 return value;
//             }.bind(this),

//             set(value) {
//                 this.set(name, value, true);
//             }.bind(this)
//         });
//     }
// }

// export default function initCache() {
//     if (typeof Cache.general === 'undefined') {
//         Cache.general = new eido.Cache('general');
//         Cache.events = new eido.Cache('events');
//     }
// }

