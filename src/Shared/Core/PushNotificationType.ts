export enum PushNotificationType { 
    RESPONSE            = 'RESPONSE',           // The response to a websocket request
    TIMEOUT             = 'TIMEOUT',            // A timeout has occured
    
    INIT_HANDSHAKE      = 'INIT_HANDSHAKE',     // The initial handshake to signify that the websocket session has beek created
    
    taskQueued          = 'taskQueued',         // The task has been queued
    taskUpdate          = 'taskUpdate',         // The task has been updated
    taskFinished        = 'taskFinished',       // The task has finished
    
    alphaRun            = 'alphaRun',           // Run the alpha
    alphaRun_Status     = 'alphaRun_Status',    // A status update to the alpha while it was running log messages and percentage completion
    alphaRun_Cancel     = 'alphaRun_Cancel',    // Cancel the alpha run
    alphaRun_Finished   = 'alphaRun_Finished',  // The alpha run finished with exitStatus of the simulation
    
    subAlphaRun         = 'subAlphaRun',           // Run the sub-alpha
    subAlphaRun_Status  = 'subAlphaRun_Status',    // A status update to the sub-alpha while it was running log messages and percentage completion
    subAlphaRun_Finished= 'subAlphaRun_Finished',  // The sub-alpha run finished with exitStatus of the simulation
    
    superAlphaRun       = 'superAlphaRun',           // Run the super-alpha
    superAlphaRun_Status= 'superAlphaRun_Status',    // A status update to the super-alpha while it was running log messages and percentage completion
    superAlphaRun_Finished= 'superAlphaRun_Finished',  // The super-alpha run finished with exitStatus of the simulation
    
    strategyRun         = 'strategyRun',           // Run the strategy
    strategyRun_Status  = 'strategyRun_Status',    // A status update to the alpha while it was running log messages and percentage completion
    strategyRun_Cancel  = 'strategyRun_Cancel',    // Cancel the strategy run
    strategyRun_Finished= 'strategyRun_Finished',  // The strategy run finished with exitStatus of the simulation
};    

