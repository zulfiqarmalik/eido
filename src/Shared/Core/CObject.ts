import { BaseObject, CopyOptions } from "./BaseObject";
import { SecrecyLevel } from "Shared/Model/SecrecyLevel";
import { ObjectID } from 'Shared/Core/ObjectID';
import './Array';
import { ModelProperties, Model, ModelProperty } from "../Model/ShModel";
import { Debug, PromiseUtil } from "./ShUtil";
import { Client } from "./Client";
require('./Array.js');

export class CObject extends BaseObject {
    static RESTErrorCallback: (err: any) => void

    constructor() {
        super();
    }

    /////////////////////////////////////////////////////////////////////////////////////////
    /// Client side
    /////////////////////////////////////////////////////////////////////////////////////////
    static url(path: string): string {
        return new Client().url(path);
    }
    
    static wssUrl(): string {
        return new Client().wssUrl();
    }
    
    url(path: string): string {
        return CObject.url(path);
    }
    
    wssUrl(): string {
        return CObject.wssUrl();
    }
    
    isTask() {
        return false;
    }
    
    updateObject(src: any) {
        this.copyFrom(src, new CopyOptions(SecrecyLevel.all));
        return this;
    }
    
    static callMethod(method, path: string, params: any, noConvertData?: boolean): Promise<any> { 
        Debug.assert(path, 'Must specify a remote path to call!');
    
        let deferred = PromiseUtil.defer();
        
        let url = new Client().url(path);
        let dataType = 'json';
        let data = {};
    
        if (!params || typeof params !== 'object')
            params = {};
    
        // params.appId = eido.appId;
        // params.sessionId = eido.sessionId;
        
        // if its a get or we're given a query string
        if (method === 'get' || (params && params.qs)) {
            dataType = 'post';
            if (params && params.qs)
                params = params.qs;
        }
    
        if (dataType !== 'get' && params && params.body) {
            data = params.body;
        }
    
        console.log(`Ajax URL: ${url} [Method: ${method}]`);
    
        let ajaxLib = require('axios');
    
        ajaxLib({
            method: method,
            url: url,
            params: params,
            // dataType: dataType,
            contentType: 'application/json; charset=utf-8',
            accepts: 'application/json',
            accept: 'application/json',
            timeout: 300 * 1000,
            data: data
            // headers: {
            //     "x-access-token": accessToken
            // },
        }).then(function(response) {
            let data = BaseObject.deseriliseResponse(response.data);
            this.resolve(data);
        }.bind(deferred))
        .catch(function(err) {
            console.error("Error:", err);

            // if (CObject.RESTErrorCallback)
            //     CObject.RESTErrorCallback(err);
                
            this.reject(err);
        }.bind(deferred));
        
        return deferred.promise;
    }

    copyAll(src: any, props: ModelProperties, options: CopyOptions) {
        let dst = this;
    
        if (!options)
            options = new CopyOptions(SecrecyLevel.all);
    
        if (!props)
            props = this.getModelProperties();
    
        for (let k in src) {
            if (k.charAt(0) === '_' && k !== '_id')
                continue;
    
            //Debug.assert(src[k] !== undefined, 'Expecting source to have property: ' + k);
            let prop = props.get(k);
            let v = src[k];
    
            if (!prop && options.level !== SecrecyLevel.all)
                continue;
            else 
                prop = new ModelProperty(typeof v, k, "");
    
            if (typeof v === 'function' || k === '_super')
                continue;
    
            if (typeof v === 'undefined' || v === null) {
                dst[k] = v;
                continue;
            }
    
            if (prop.type === 'array' && typeof dst[k] === 'undefined')
                dst[k] = new Array();
    
            // see if its a model object .. if it is then copy is recursively!
            if (typeof dst[k] !== 'undefined' && dst[k] !== null && typeof v == 'object' && 
                (BaseObject.isModelObject(dst[k]) || v instanceof Array) && typeof dst[k].copyFrom == 'function') {
                options.__prop = prop;
                dst[k].copyFrom(v, options);
                delete options.__prop;
                continue;
            }
            else if (typeof v === 'object' && v.__classId) {
                let cls = Model.getClass(v.__classId);
                if (cls) {
                    dst[k] = (new cls()).copyFrom(v, options);
                    continue;
                }
            }
            
            if (!options.noMarshal && prop) {
                if (typeof v === 'string' && prop.type !== 'string') {
                    let parser = Model.getParser(prop.type);
                    if (parser)
                        v = parser.parse(v, prop);
                }
            }
    
            if (prop && prop.type === 'ObjectID') 
                v = ObjectID.create(v.toString());
    
            dst[k] = v;
        }
    
        return this;
    }
    
    static post(path: string, params: any, noConvertData?: boolean): Promise<any> {
        return CObject.callMethod('post', path, params, noConvertData);
    }
    
    static put(path: string, params: any, noConvertData?: boolean): Promise<any> {
        return CObject.callMethod('put', path, params, noConvertData);
    }
    
    static patch(path: string, params: any, noConvertData?: boolean): Promise<any> {
        return CObject.callMethod('patch', path, params, noConvertData);
    }
    
    static get(path: string, params: any, noConvertData?: boolean): Promise<any> {
        return CObject.callMethod('get', path, params, noConvertData);
    }
    
    static delete(path: string, params: any, noConvertData?: boolean): Promise<any> {
        return CObject.callMethod('delete', path, params, noConvertData);
    }

    post(path: string, params: any, noConvertData?: boolean) {
        return CObject.callMethod('post', path, params, noConvertData);
    }
    
    put(path: string, params: any, noConvertData?: boolean) {
        return CObject.callMethod('put', path, params, noConvertData);
    }
    
    patch(path: string, params: any, noConvertData?: boolean) {
        return CObject.callMethod('patch', path, params, noConvertData);
    }
    
    get(path: string, params: any, noConvertData?: boolean) {
        return CObject.callMethod('get', path, params, noConvertData);
    }
    
    delete(path: string, params: any, noConvertData?: boolean) {
        return CObject.callMethod('delete', path, params, noConvertData);
    }
    
        
    /////////////////////////////////////////////////////////////////////////////////////////
    /// SERVER side
    /////////////////////////////////////////////////////////////////////////////////////////
    
    // get dbName(): string            { return "pesa"; }
    // get collectionName(): string    { return "none"; }

    getCollectionName(): string {
        if (this.collectionName)
            return this.collectionName;
        else if (this.constructor["collectionName"])
            return this.constructor["collectionName"];
        Debug.assert(false, '[CObject] Every object MUST return a valid collection name!');
        return "NONE";
    }

    getDBName(): string {
        if (this.dbName)
            return this.dbName;
        else if (this.constructor["dbName"])
            return this.constructor["dbName"];
        return 'pesa';
    }

    static makeDataFromRows(cls: any, rows: any[]) {  }
    static getCollection(qinfo: any = null): any { return null; }
    static dbRef(ref) {  }
    static parseSet(set, object) {  }
    static combineSet(parts) {  }
    static makeSubsetDataFromRows(cls, rows, parts, isBulk) {  }
    static getNestedObject(src, set, object) {  }
    static find(info, collectionName, callback) {  }
    static convertResults(qinfo, rows) {  }
    static loadAll(qinfo): Promise<any> { return null; }
    static dbExec(qinfo): Promise<any> { return null; }
    static advancedSearch(qinfo): Promise<any> { return null; }
    static search(qinfo): Promise<any> { return null; }
    static dbLoad(qinfo): Promise<any> { return null; }
    static dbLoadOne(qinfo?: any): Promise<any> { return null; }
    static dbBulkDelete(qinfo): Promise<any> { return null; }
    static dbBulkInsert(qinfo): Promise<any> { return null; }
    static dbInsert(qinfo): Promise<any> { return null; }
    static dbUpdate(qinfo): Promise<any> { return null; }
    static send(args, input, status, level) {  }
    static sendClientRaw(args: any, status: number, data: any) {  }
    static sendClient(args: any, status: number, input: BaseObject | Array<any>, clsId: string, level: SecrecyLevel = SecrecyLevel.public) {  }
    static loadFromIds(ids, cls): Promise<any> { return null; }
    static loadFromId(id: ObjectID, cls: any = null): Promise<any> { return null; }
    static getBulkKey(): string { return ""; };
}
