/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// StringUtil: Utility string functions
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
interface String { 
    lpad(padString: string, length: number): string;
    replaceAt(index: number, character: string): string;
};

if (!String.prototype.lpad) {
    String.prototype.lpad = function (padString: string, length: number): string {
        let str = this;
        while (str.length < length)
            str = padString + str;
        return str;
    }
}

if (!String.prototype.replaceAt) {
    String.prototype.replaceAt = function(index, character) {
        return this.substr(0, index) + character + this.substr(index + character.length);
    }
}
