////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// Date: Some static utility definitions
////////////////////////////////////////////////////////////////////////////////////////////////////////////////
export declare module Date {
    export var Sunday;
    export var Monday;
    export var Tuesday;
    export var Wednesday;
    export var Thursday;
    export var Friday;
    export var Saturday;

    export var January;
    export var February;
    export var March;
    export var April;
    export var May;
    export var June;
    export var July;
    export var August;
    export var September;
    export var October;
    export var November;
    export var December;

    export var asUTC;
    export var utcNow;
    export var rand;
    export var yearsInPast;
};

////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// Days of the week - JS number convention
////////////////////////////////////////////////////////////////////////////////////////////////////////////////
Date.Sunday         = 0;
Date.Monday         = 1;
Date.Tuesday        = 2;
Date.Wednesday      = 3;
Date.Thursday       = 4;
Date.Friday         = 5;
Date.Saturday       = 6;

////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// Months of the year - JS number convention
////////////////////////////////////////////////////////////////////////////////////////////////////////////////
Date.January        = 0;
Date.February       = 1;
Date.March          = 2;
Date.April          = 3;
Date.May            = 4;
Date.June           = 5;
Date.July           = 6;
Date.August         = 7;
Date.September      = 8;
Date.October        = 9;
Date.November       = 10;
Date.December       = 11;
