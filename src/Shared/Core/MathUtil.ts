import { eido } from './ShCore';

export default class MathUtil {
    static mean(arr: number[]): number {
        if (!arr.length)
            return 0.0;

        let sum = 0.0;
        arr.map((v) => sum += v);
        return sum / arr.length;
    }

    static variance(arr: number[], mean: number): number {
        if (!mean)
            mean = MathUtil.mean(arr);

        let variance = 0.0;

        arr.map((v) => {
            let d = v - mean;
            variance += (d * d);
        });

        return variance / arr.length;
    }

    static stddev(arr: number[], mean: number): number {
        return Math.sqrt(MathUtil.variance(arr, mean));
    }
};
