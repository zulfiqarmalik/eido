//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// eido Guid definition
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
export default class Guid {
    guid: string;

    constructor(value?: string) {
        if (!value)
            this.guid = this.generate();
        else
            this.guid = value;
    }

    toString(): string {
        return this.guid;
    }

    generate(): string {
        return 'xxxxxxxxxxxx4xxxyxxxxxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
            let r = Math.random() * 16 | 0,
                v = c == 'x' ? r : (r & 0x3 | 0x8);
            return v.toString(16);
        });
    }
}
