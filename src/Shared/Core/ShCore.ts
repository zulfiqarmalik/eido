import { ObjectID } from "./ObjectID";
import "./CObject";

export interface IObject {
};

export interface IError {
}

export interface IUser {
    loadAccessToken()           : string;
}

export function applyMixins(derivedCtor: any, baseCtors: any[]) {
    baseCtors.forEach(baseCtor => {
        Object.getOwnPropertyNames(baseCtor.prototype).forEach(name => {
            derivedCtor.prototype[name] = baseCtor.prototype[name];
        });
    });
}

export interface CoreUtil {
    apiRoot()                               : string;
    apiVersion()                            : string;
    createError(code: number, msg: string)  : IError;
    user()                                  : IUser;
}

export class Env {
    static type                 : string = "development";
    static local                : boolean = true;
    static test                 : boolean = false;
    static dev                  : boolean = true;
    static debug                : boolean = true;
    static release              : boolean = false;
    static preProd              : boolean = false;
    static prod                 : boolean = false;
    static linux                : boolean = false;
    static windows              : boolean = false;

    static get server()         : boolean {
        /// A bit hacky, but the best I have for the time being 
        if (typeof window === undefined)
            return true;
        return false;
    }

    static isDev()              : boolean { return Env.type == 'development'; }
    static isPreProd()          : boolean { return Env.type == 'pre_production'; }
    static isProd()             : boolean { return Env.type == 'production'; }
}

export class Eido {
    appId                       : string = "5drathii23406jaysthh6y960gk";
    DBContainerObject           : any;
    private _appServer          : any = null;
    dataCache                   : any = null;
    coreUtil                    : CoreUtil = null;
    remote                      : string = "";

    DB                          : any = null;
    
    _cached                     : {
        signals : {
        }
    }

    get appServer(): any {
        // if (!Env.server || !this._appServer) {
        //     console.error("Can only call eido.appServer from server side!");
        //     debugger;
        //     return
        // }

        return this._appServer;
    }

    set appServer(value: any) {
        if (this._appServer) {
            console.error("AppServer already set!");
            return;
        }

        this._appServer = value;
    }
}

export let eido = new Eido();

export default function initCore() {
}

// export declare module eido {
//     export var appId       : string = "5drathii23406jaysthh6y960gk";
// }

// export let eido = {
//     appId: "5drathii23406jaysthh6y960gk",
//     ObjectID: any,
//     _cached: {
//         signals: {
//         }
//     }
// };

// String.prototype.lpad = function (padString, length) {
//     var str = this;
//     while (str.length < length)
//         str = padString + str;
//     return str;
// }

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// eido unit test specific
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// eido.TestPriority = {
//     max: 1,
//     high: 25,
//     normal: 50,
//     low: 75,
//     min: 100
// };

// eido.sh = eido.sh || {};

// eido.appId = "5drathii23406jaysthh6y960gk";

// if (typeof global == 'undefined' && window) {
//     window.__DEBUG__ = __DEBUG__;
//     window.__RELEASE__ = __RELEASE__;
//     window.__TEST__ = false;
// }
// else if (typeof global != 'undefined') {
//     global.__DEBUG__ = __DEBUG__;
//     global.__RELEASE__ = __RELEASE__;
//     global.__TEST__ = false;
// }

// eido.noImpl = function(message) {
//     message = message || "Not implemented"
//     eido.Debug.assert(false, message);
// }
