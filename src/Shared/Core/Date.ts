/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// Date: Utility functions for Date object
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
interface Date { 
    isValid(): boolean;
    offsetTime(hours: number, minutes: number, seconds: number, milliseconds: number): Date;
    offsetDays(offset: number): Date;
    offsetWeeks(weeks: number): Date;
    endOfDay(): Date;
    copyTime(date: Date): Date
    changeTime(hours: number, minutes: number, seconds: number, milliseconds: number): Date;
    asUTC(): Date
    rand(start: Date, end: Date): Date;
};

Date.prototype.isValid = function(): boolean {
    var d = this;
    return  !isNaN(d.getUTCFullYear()) &&
            !isNaN(d.getUTCMonth()) &&
            !isNaN(d.getUTCDate()) &&
            !isNaN(d.getUTCHours()) &&
            !isNaN(d.getUTCMinutes()) &&
            !isNaN(d.getUTCSeconds());
};

Date.prototype.offsetTime = function(hours, minutes, seconds, milliseconds) {
    if (hours !== undefined && hours !== null)
        this.setHours(this.getHours() + hours);

    if (minutes !== undefined && minutes !== null)
        this.setMinutes(this.getMinutes() + minutes);

    if (seconds !== undefined && seconds !== null)
        this.setSeconds(this.getSeconds() + seconds);

    if (milliseconds !== undefined && milliseconds !== null)
        this.setMilliseconds(this.getMilliseconds() + milliseconds);

    return this;
};

Date.prototype.offsetDays = function(offset) {
    this.setDate(this.getDate() + offset);
    return this;
};

Date.prototype.offsetWeeks = function(weeks) {
    return this.offsetDays(weeks * 7);
};

Date.prototype.endOfDay = function() {
    this.setHours(23);
    this.setMinutes(59);
    this.setSeconds(59);
    // this.setMilliseconds(999);
    return this;
};

Date.prototype.copyTime = function(date) {
    return this.changeTime(date.getHours(), date.getMinutes(), date.getSeconds(), date.getMilliseconds());
};

Date.prototype.changeTime = function(hours, minutes, seconds, milliseconds) {
    if (hours !== undefined && hours !== null)
        this.setHours(hours);

    if (minutes !== undefined && minutes !== null)
        this.setMinutes(minutes);

    if (seconds !== undefined && seconds !== null)
        this.setSeconds(seconds);

    if (milliseconds !== undefined && milliseconds !== null)
        this.setMilliseconds(milliseconds);

    return this;
};

Date.prototype.asUTC = function() {
    return new Date();
};

