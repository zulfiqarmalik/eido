export default class Timer {
    static setInterval(interval, callback) {
        return setInterval(callback, interval);
    }

    static clearInterval(timer) {
        clearInterval(timer);
    }

    static setTimeout(interval, callback) {
        setTimeout(callback, interval);
    }

    static getTime() {
        return (new Date).getTime();
    }

    static getSeconds() {
        return Timer.getMilliseconds() * 0.001;
    }

    static getMilliseconds() {
        return (new Date).getTime();
    }
};

