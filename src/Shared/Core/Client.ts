import { Debug } from "Shared/Core/ShUtil";
import { Env, eido } from "Shared/Core/ShCore";

export class Client {
    static __url            : string = "";
    static __wssUrl         : string = "";

    url(path: string) {
        if (Client.__url)
            return this.finaliseUrl(Client.__url, path);

        if (eido.remote) {
            Client.__url = eido.remote;
            return this.finaliseUrl(Client.__url, path);
        }

        // otherwise try to construct from window location ...
        if (window && window.location) {
            if (window.location.origin) {
                Client.__url = window.location.origin;
                return this.finaliseUrl(Client.__url, path);
            }
        }

        // if that fails then we just use the default eido path!
        Client.__url = "https://www.eido.com";

        return this.finaliseUrl(Client.__url, path);
    }

    wssUrl() {
        if (Client.__wssUrl)
            return this.finaliseUrl(Client.__wssUrl, "");

        var url = this.url("");
        var parts = url.split("/");
        var protocol = "ws://";

        Debug.assert(parts.length >= 2, `Invalid url: ${url}`);

        if (parts[0] == "https:")
            protocol = "wss://";

        var host = parts[2];
        Client.__wssUrl = protocol + host;

        return this.finaliseUrl(Client.__wssUrl, "");
    }

    finaliseUrl(baseUrl, path) {
        var url = (baseUrl + path).replace("[apiRoot]", eido.coreUtil.apiRoot()).replace("[apiVersion]", eido.coreUtil.apiVersion());

        var accessToken = "";
        let user = eido.coreUtil.user();
        if (user)
            accessToken = user.loadAccessToken();

        url += "?appId=" + eido.appId; // + "&accessToken=" + accessToken;
        return url;
    }

    // exec(method, path, config) {
    //     var deferred = Q.defer();

    //     Client.ajax({
    //         type: method,
    //         url: this.url(path),
    //         data: config.fields || config.params,
    //         dataType: config.type || 'json',

    //         success(result, code, response) {
    //             try {
    //                 if (!response.responseJSON) {
    //                     if (response.responseText.length)
    //                         response.responseJSON = JSON.parse(response.responseText);
    //                     deferred.resolve(response.responseJSON);
    //                 }
    //                 else {
    //                     deferred.resolve(response.responseJSON);
    //                 }
    //             }
    //             catch (e) {
    //                 deferred.reject(e);
    //             }
    //         },

    //         error(err) {
    //             deferred.reject(err);
    //         }
    //     });

    //     return deferred.promise;
    // }

    // get(path, config) {
    //     return this.exec('GET', path, config);
    // }

    // post(path, config, $http) {
    //     return this.exec('POST', path, config);
    // }

    // delete(path, config) {
    //     return this.exec('DELETE', path, config);
    // }

    // put(path, config) {
    //     return this.exec('PUT', path, config);
    // }

    // patch(path, config) {
    //     return this.exec('PATCH', path, config);
    // }
};
