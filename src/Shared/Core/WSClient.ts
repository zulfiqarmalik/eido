//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// WebSocket "Client"
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

import Signal from "./Signals";
import { PushNotificationType } from "./PushNotificationType";
import { eido, Env } from './ShCore';
import { ShUtil, Debug, PromiseUtil } from './ShUtil';
import { BaseObject } from './BaseObject';

let g_wsClientId = 0;

export class WSOptions {
    autoReconnect           : boolean   = true;
    reconnect               : boolean   = true;
}

export default class WSClient {
    // we use global signals for ease of use ...
    static s_WSConnect      = new Signal();
    static s_WSDisconnect   = new Signal();
    static s_WSMessage      = new Signal();
    static get maxReconnect() { return 100; }

    s_WSHandshake           : any       = null;
    s_WSConnect             : any       = null;
    s_WSDisconnect          : any       = null;
    s_WSMessage             : any       = null;
    s_WSResponse            : any       = null;


    ws                      : any       = null;
    maxWSReconnects         : number    = 1000000;
    reconnectInterval       : number    = 5;
    reconnect               : boolean   = true;
    wsClientId              : number    = g_wsClientId++;
    signals                 : object    = {};
    numWSReconnects         : number    = 0;

    url                     : string    = "";
    options                 : WSOptions = new WSOptions();
    deferred                : any       = null;

    constructor() {
    }
    
    isConnected(): boolean {
        return this.ws ? true : false;
    }
    
    initSignals(): void {
        this.s_WSHandshake  = new Signal();
        this.s_WSConnect    = new Signal();
        this.s_WSDisconnect = new Signal();
        this.s_WSMessage    = new Signal();
        this.s_WSResponse   = new Signal();
    }
    
    freeSignals(): void {
        this.s_WSHandshake.dispose();
        this.s_WSHandshake = null;

        this.s_WSConnect.dispose();
        this.s_WSConnect = null;

        this.s_WSDisconnect.dispose();
        this.s_WSDisconnect = null;

        this.s_WSMessage.dispose();
        this.s_WSMessage = null;

        this.s_WSResponse.dispose();
        this.s_WSResponse = null;
    }

    connect(url: string, options: WSOptions = null): any {
        Debug.assert(url, 'Invalid URL passed for connection!');
        Debug.assert(!this.ws, 'Trying to reconnect a connected socket!');
        
        if (this.ws)
            return PromiseUtil.resolvedPromise(this);

        if (options)
            this.options = options;
        this.url = url;

        if (typeof this.options.autoReconnect === 'undefined')
            this.options.autoReconnect = true;

        this.options.reconnect = true;

        this.initSignals();

        if (!Env.server) {
            this.ws = new WebSocket(url);

            this.ws.onopen = this.onConnect.bind(this);
            this.ws.onmessage = this.onMessage.bind(this);
            this.ws.onclose = this.onDisconnect.bind(this);
        }
        else {
            // this.ws = new WebSocket(url, null, {
            //     headers: {
            //         'Cookie': options.cookie,
            //         'x-access-token': options.accessToken
            //     }
            // });
            this.ws = new WebSocket(url);

            this.ws.on('open', this.onConnect.bind(this));
            this.ws.on('message', this.onMessage.bind(this));
            this.ws.on('close', this.onDisconnect.bind(this));
        }

        this.deferred = PromiseUtil.defer();

        return this.deferred.promise;
    }

    disconnect(): void {
        this.options.reconnect = false;

        if (this.ws) {
            console.log('[WSClient] Disconnecting ...');
            this.ws.close();
            this.ws = null;
        }
    }

    send(data: object): void {
        Debug.assert(this.ws, 'Web socket is not connected!');
        let dataJson;

        if (typeof data === 'string')
            dataJson = data;
        else
            dataJson = JSON.stringify(data);
        
        this.ws.send(dataJson);
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /// Event Listeners
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    onConnect(ws: WebSocket): void {
        WSClient.s_WSConnect.dispatch(this);
        this.s_WSConnect.dispatch(this);
        
        // reset the number of reconnections ...
        this.numWSReconnects = 0;

        if (this.deferred) {
            this.deferred.resolve(this);
            this.deferred = null;
        }
    }

    onDisconnect(ws: WebSocket): void {
        this.ws = null;

        // disconnect all the signals
        this.freeSignals();
        
        if (this.options.reconnect === true && 
            this.numWSReconnects < this.maxWSReconnects) {

            setTimeout(() => {
                this.numWSReconnects++;
                if (this.numWSReconnects > WSClient.maxReconnect) {
                    console.error('[WSClient] Max reconnect hit:', WSClient.maxReconnect);
                    return;
                }
                
                console.error('[WSClient] Websocket disconnected erratically. Reconnecting counter:', this.numWSReconnects);
                this.connect(this.url, this.options);
            }, this.reconnectInterval * 1000);
        }
    }

    onMessage(event: MessageEvent) {
        try {
            let data: string | Buffer = event.data;
            let ws: WebSocket = event.target as WebSocket;

            Debug.assert(data, 'Invalid message with no data.');

            let notificationStr = "";

            if (typeof(data) == 'string')
                notificationStr = data.toString();
            else if (data instanceof Buffer)
                notificationStr = data.toString('ascii');
            else {
                Debug.assert(false, `Un-supported data format!`);
            }

            let notification = JSON.parse(notificationStr);

            if (!notification.id) {
                console.log(`Invalid notification without an id: ${notificationStr}`);
                return;
            }

            // console.trace('[WSClient] WSClient Notification:', notificationStr);

            if (notification.id === PushNotificationType.INIT_HANDSHAKE) { // the initial handshake is sent
                console.log("[WSClient] INIT_HANDSHAKE!");
                if (this.deferred) {
                    this.deferred.resolve(this);
                    this.deferred = null;
                }

                this.s_WSHandshake.dispatch(this, notification, notificationStr);
            }
            else if (notification.id === PushNotificationType.RESPONSE) { // response to a request is sent
                console.log("[WSClient] RESPONSE!");
                this.s_WSResponse.dispatch(this, notification, notificationStr);
            }
            else { // some other notification is sent
                let signal = this.signals[notification.id];
                // console.log("[WSClient] Notification: %s", notification.id);

                if (signal) {
                    /// Deserialise the object
                    let data = BaseObject.deseriliseResponse(notification.data);
                    signal.dispatch(this, data, notificationStr);
                }
                else {
                    this.s_WSMessage.dispatch(this, notification, notificationStr);
                    // this.s_WSMessage.dispatch(this, notification, notificationStr);
                }
            }
        }
        catch (e) {
            console.error(e);
            
            // if there is an error parsing the data then we just pass the raw message in the event ...
            // s_WSMessage.dispatch(this, undefined, message);
        }
    }

    signal(name) {
        if (!this.signals[name])
            this.signals[name] = new Signal();

        return this.signals[name];
    }
}

