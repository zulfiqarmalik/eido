import { BaseObject } from './BaseObject';
import { Model } from '../Model/ShModel';
import { CObject } from './CObject';
import { SecrecyLevel } from 'Shared/Model/SecrecyLevel';
import { Debug, PromiseUtil } from './ShUtil';

Array.prototype.cleanClone = function() {
    let clone = new Array(this.length);

    for (let i = 0; i < this.length; i++) {
        if (BaseObject.isModelObject(this[i]))
            clone[i] = this[i].cleanClone(...arguments);
        else
            clone[i] = this[i];
    }
    
    clone.__classId = 'Array';

    return clone;
};

Array.prototype.embedClone = function(level) {
    if (!this.length)
        return [];
        
    if (level & SecrecyLevel.embeddedRef) {
        return this.embedCloneRef();
    }

    return this.cleanClone(...arguments);
};

Array.prototype.minimalClone = function() {
    return this.cleanClone(SecrecyLevel.minimal);
};

Array.prototype.embedCloneRef = function(level) {
    if (!this.length)
        return [];

    let clone = new Array(this.length);
    let classId = this[0].constructor.__classId;

    Debug.assert(classId, 'Invalid class retrieved from embedded object!');

    for (let i = 0; i < this.length; i++) {
        Debug.assert(this[i]._id, 'Cannot have an object without an id as embedded ref!');
        Debug.assert(this[i].constructor.__classId === classId, 'Array elements must be homogenous. Expecting:', classId, '- Found:', this[i].constructor.__classId);
        clone[i] = this[i]._id; //this[i].embedCloneRef.apply(this[i], arguments);
    }
    
    clone._dbRef = true;
    clone.__classId = classId;

    return clone;
};

Array.prototype.copyFrom = function(src, options) {
    if (!src.length) {
        this.length = 0;
        return this;
    }

    /// Reset the array. This is important to ensure that object is the exact copy of the source
    this.length = 0;

    Debug.assert(src instanceof Array, 'Invalid array passed as input');
    let promises = [];
    let classId = src.__classId || this.__classId;
    
    for (let i = 0; i < src.length; i++) {
        let srcObj = src[i];
        if (typeof srcObj != 'object' || !srcObj) 
            this.push(srcObj);
        else if (typeof srcObj == 'object' && !srcObj.__classId)
            this.push(srcObj);
        else {
            classId = srcObj.__classId || classId;

            if (typeof srcObj !== 'object') {
                this.push(srcObj);
            }
            else if (classId === "ObjectID") {
                this.push(srcObj);
            }
            else {
                // Debug.assert(options && options.prop, 'Invalid options passed!');
                if (options && options.prop)
                    classId = options.prop.items.type;
                    
                Debug.assert(classId, 'Invalid classId for array item. All array items must be model objects!');

                let cls = Model.getClass(classId);
                if (cls) {
                    let instance = new cls();
                    Debug.assert(typeof instance.copyFrom === 'function', 'Invalid model object instance!');
                    
                    let ret = instance.copyFrom(srcObj, {
                        level: options.level
                    });
                    
                    if (PromiseUtil.isPromise(ret)) 
                        promises.push(ret);
                        
                    this.push(instance);
                }
            }
        }
    };

    if (classId)
        this.__classId = classId;
    
    return promises.length ? PromiseUtil.allPromises(promises) : this;
};

Array.prototype.toClass = function() {
    let clone = new Array(this.length);

    for (let i = 0; i < this.length; i++)
        clone[i] = this[i].toClass.apply(this[i], arguments);

    return clone;
}

let baseCopyFrom = Array.prototype.copyFrom;

Array.prototype.copyFrom = function(src, options) {
    if (!src.length)
        return baseCopyFrom.apply(this, arguments);

    if (!options.prop || 
        options.deserialize === true || 
        // options.level === SecrecyLevel.REST ||
        (options.prop.level & SecrecyLevel.embeddedRef) === 0)
        return baseCopyFrom.apply(this, arguments);

    if (typeof src[0]._bsontype !== 'string' || src[0]._bsontype !== 'ObjectID')
        return baseCopyFrom.apply(this, arguments);


    let classId = src.__classId || options.prop.items.type;
    Debug.assert(classId, 'Expecting class id to be embedded within the source array. Could not found!');

    // If its just object ids that we're copying then we just copy them verbatim ...
    if (classId === "ObjectID") {
        for (let i = 0; i < src.length; i++) 
            this[i] = src[i];
        return this;
    }
    
    let cls = Model.getClass(classId);
    Debug.assert(cls, 'Invalid class id:', classId);

    let ids = src; // the source array should be our ids!
    this.__classId = classId;

    // otherwise we load the object from the server
    return cls.loadFromIds(ids, cls)
        .then((results) => {
            this.length = results.length;
            for (let i = 0; i < results.length; i++)
                this[i] = results[i];
        });

    // // if one of the elements is a dbRef then we shall assume the entire array to be DBRefs
    // let ref = CObject.dbRef(src[0]);

    // // if its not a DBRef then just call the base implementation
    // if (!ref)
    //     return baseCopyFrom.apply(this, arguments);

    // let ids = new Array(src.length);
    // ids[0] = ref._id;

    // for (let i = 1; i < src.length; i++) {
    //     let dref = CObject.dbRef(src[i]);
    //     Debug.assert(dref, 'Invalid source array with mixed dbRefs and non-refs. This is not allowed!');
    //     Debug.assert(dref.cls.__classId === ref.cls.__classId, 'Different classIds found. Expecting:', ref.cls.__classId, '- Found:', dref.cls.__classId);
    //     ids[i] = dref._id;
    // }

    // // otherwise we load the object from the server
    // return ref.cls.loadFromIds(ids, ref.cls)
    //     .then(function(results) {
    //         this.length = results.length;
    //         for (let i = 0; i < results.length; i++)
    //             this[i] = results[i];
    //     }.bind(this));
};

Array.prototype.sendClient = function(args, status, level) {
    Debug.assert(args, `Invalid server args while trying to send array!`);
    return CObject.sendClient(args, status, this, "array", level);
};


Array.copyFrom = function(other) {
    let clone = new Array(other.length);

    for (let i = 0; i < other.length; i++) {
        if (BaseObject.isModelObject(other[i])) 
            clone[i] = other[i].cleanClone(...arguments);
        else
            clone[i] = other[i];
    }
    
    clone.__classId = 'Array';

    return clone;
}

// export default class ArrayContainer<T extends any> extends Array<T> {
//     _dbRef                  : boolean = false;
//     __classId               : string;

//     static copyFrom(other: any[]) {
//         let clone = new ArrayContainer(other.length);

//         for (let i = 0; i < other.length; i++) {
//             if (BaseObject.isModelObject(this[i])) 
//                 clone[i] = other[i].cleanClone(...arguments);
//             else
//                 clone[i] = other[i];
//         }
        
//         clone.__classId = 'Array';
    
//         return clone;
//     }

//     cleanClone(options: CopyOptions | SecrecyLevel): any {
//         return ArrayContainer.copyFrom(this);
//     }

//     embedClone(level: SecrecyLevel = SecrecyLevel.public) {
//         if (!this.length)
//             return new ArrayContainer<T>();
            
//         if (level & SecrecyLevel.embeddedRef) {
//             return this.embedCloneRef(SecrecyLevel.embeddedRef);
//         }
    
//         return this.cleanClone(level);
//     };
    
//     minimalClone() {
//         return this.cleanClone(SecrecyLevel.minimal);
//     };
    
//     sendClient(args: any, status: number, level: SecrecyLevel = SecrecyLevel.public) {
//         return CObject.sendClient(args, status, this, "array", level);
//     }

//     embedCloneRef(level: SecrecyLevel) {
//         if (!this.length)
//             return new ArrayContainer<T>();
    
//         let clone = new ArrayContainer(this.length);
//         let classId = this[0].constructor.__classId;
    
//         Debug.assert(classId, 'Invalid class retrieved from embedded object!');
    
//         for (let i = 0; i < this.length; i++) {
//             Debug.assert(this[i]._id, 'Cannot have an object without an id as embedded ref!');
//             Debug.assert(this[i].constructor.__classId === classId, `Array elements must be homogenous. Expecting: ${classId} - Found: ${this[i].constructor.__classId}`);
//             clone[i] = this[i]._id; //this[i].embedCloneRef.apply(this[i], arguments);
//         }
        
//         clone._dbRef = true;
//         clone.__classId = classId;
    
//         return clone;
//     };

//     copyFrom(src: ArrayContainer<T>, options: any) {
//         if (!src.length) {
//             this.length = 0;
//             return this;
//         }
    
//         // TODO: remove this!
//         // Debug.assert(src.length < 100, "WTF!");
            
//         Debug.assert(src instanceof Array, 'Invalid array passed as input');
//         let promises = [];
//         let classId = src.__classId || this.__classId;
        
//         for (let i = 0; i < src.length; i++) {
//             let srcObj = src[i];
//             if (typeof srcObj != 'object' || !srcObj) 
//                 this.push(srcObj);
//             else if (typeof srcObj == 'object' && !srcObj.__classId)
//                 this.push(srcObj);
//             else {
//                 classId = srcObj.__classId || classId;
    
//                 if (typeof srcObj !== 'object') {
//                     this.push(srcObj);
//                 }
//                 else if (classId === "ObjectID") {
//                     this.push(srcObj);
//                 }
//                 else {
//                     // Debug.assert(options && options.prop, 'Invalid options passed!');
//                     if (options && options.prop)
//                         classId = options.prop.items.type;
                        
//                     Debug.assert(classId, 'Invalid classId for array item. All array items must be model objects!');
    
//                     let cls = Model.getClass(classId);
//                     if (cls) {
//                         let instance = new cls();
//                         Debug.assert(typeof instance.copyFrom === 'function', 'Invalid model object instance!');
                        
//                         let ret = instance.copyFrom(srcObj, {
//                             level: options.level
//                         });
                        
//                         if (PromiseUtil.isPromise(ret)) 
//                             promises.push(ret);
                            
//                         this.push(instance);
//                     }
//                 }
//             }
//         };
    
//         if (classId)
//             this.__classId = classId;
        
//         return promises.length ? Promise.all(promises) : this;
//     };
// }        


