import { Env, eido } from './ShCore';
import './String';
import './DateUtil';
import './Date';
import { ObjectID } from './ObjectID';

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// Debug helper class
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
export class Debug {
    static assert(exp: any, message: string) {
        if (!exp) {
            if (!Env.release) {
                console.error(`Assert :  ${message}`);
                console.error(`Call stack : ${(new Error()).stack}`);
                debugger;
            }
            
            // throw (new Error(500, message));
        }
    }

    static getCallStack() {
        return (new Error()).stack;
    }

    static throwIf(exp: boolean, message: string) {
        if (exp) {
            throw new Error(message);
        }
    }
};

export class ShUtil {
    static getValue(object: object, index: string, lessIndex: number, errorValue = undefined) {
        let parts = index.split('.');
        let parent = object;

        if (!lessIndex)
            lessIndex = 0;

        for (let i = 0; i < parts.length - lessIndex; i++) {
            parent = parent[parts[i]];

            if (parent === null || parent === undefined)
                return typeof errorValue === undefined ? parent : errorValue;
        }

        return parent;
    }

    static splitMongoId(id: string | ObjectID) {
        return id.toString().split(/(.{2})/).filter(O=>O);
    }

    static getThmPath(iid: string | ObjectID): string {
        let dirId = ShUtil.splitMongoId(iid);
        return `/static_thm/${dirId.join('/')}/${iid}`;
    }

    /// DO NOT DELETE. THIS IS ANOTHER RANK FUNCTION THAT MAY BECOME FASTER IN THE FUTURE. 
    // static rank(arr, sortFunc) {
    //     if (!sortFunc)
    //         sortFunc = (a, b) => a - b;

    //     return arr
    //         .map((x, i) => [x, i])
    //         .sort((a, b) => sortFunc(a[0], b[0]))
    //         .reduce((a, x, i, s) => (a[x[1]] =
    //             i > 0 && sortFunc(s[i - 1][0], x[0]) === 0 ? a[s[i - 1][1]] : i + 1, a), []);
    // }

    static rank(arr: number[], sortFunc: (a: number, b: number) => number = null): number[] {
        if (!sortFunc)
            sortFunc = (a, b) => a - b;

        let sorted = arr.slice().sort(sortFunc);
        return arr.slice().map(function (v) { return sorted.indexOf(v) + 1; });
    }

    static noImpl(message: string = "") {
        message = message || "Not implemented"
        Debug.assert(false, message);
    }

    static setValue(object: object, index: string, value: string, indexPrefix: string) {
        let parts = index.split('.');
        let parent = object;

        for (let i = 0; i < parts.length - 1; i++) {
            parent = parent[parts[i]];

            if (parent === null || parent === undefined)
                return parent;
        }

        let pindex = parts[parts.length - 1];
        if (indexPrefix)
            pindex = indexPrefix + pindex;

        parent[pindex] = value;

        return parent;
    }

    static isPowerOfTwo(value: number) {
        return (value & (value - 1)) === 0 ? true : false;
    }
    
    static swap(value1: any, value2: any) {
        let tmp = value1;
        value1 = value2;
        value2 = tmp;
    }

    static getYearsBetweenIntDates(startDate: number, endDate: number) {
        let startYear = Math.floor(startDate / 10000);
        let endYear = Math.floor(endDate / 10000);
        let years = [];

        while (startYear <= endYear) {
            years.push(startYear);
            startYear++;
        }

        return years;
    }

    static splitIntDate(date: number) {
        let year = Math.floor(date / 10000);
        date %= 10000;
        let month = Math.floor(date / 100);
        let day = date % 100;

        return { year: year, month: month, day: day };
    }

    static dateInfoToString(year: number, month: number, day: number, sep: string): string {
        if (typeof sep === 'undefined')
            sep = '-';
        return `${year.toString()}${sep}${month.toString().lpad("0", 2)}${sep}${day.toString().lpad("0", 2)}`;
    }

    static intDateToString(idate: number, sep: string = '-'): string {
        let dinfo = ShUtil.splitIntDate(idate);
        return ShUtil.dateInfoToString(dinfo.year, dinfo.month, dinfo.day, sep);
    }

    static strDateToIntDate(sdate: string, sep: string = '-'): number {
        if (typeof sep === 'undefined')
            sep = '-';

        let parts = sdate.split(sep);
        let idate = 0;

        if (sep && parts.length == 3)
            idate = parseInt(parts[0]) * 10000 + parseInt(parts[1]) * 100 + parseInt(parts[2]);
        else
            idate = parseInt(parts[0]);

        return idate;
    }

    static applyTimezone(date: Date): Date {
        var newDate = new Date(date.getTime()+date.getTimezoneOffset()*60*1000);

        var offset = date.getTimezoneOffset() / 60;
        var hours = date.getHours();
    
        newDate.setHours(hours - offset);
    
        return newDate;        
    }

    static dateToString(date: Date, sep: string): string {
        return ShUtil.dateInfoToString(date.getFullYear(), date.getMonth() + 1, date.getDate(), sep);
    }

    static currentDateToString(sep: string = "-"): string {
        let today = new Date();
        return ShUtil.dateToString(today, sep);
    }

    static startOfDayToday(): Date {
        let today = new Date();

        today.setHours(0);
        today.setMinutes(0);
        today.setSeconds(0);
        today.setMilliseconds(0);

        return today;
    }

    static randDates(start: Date, end: Date): Date {
        return new Date(start.getTime() + Math.random() * (end.getTime() - start.getTime()));
    };
    
    static yearsInPast(years): Date {
        var now = new Date();
        return new Date(now.getFullYear() - years, now.getMonth(), now.getDate());
    };
        

    static datedCorrelation(correlationFunc: any, arr1: any[], dates1: any[], arr2: any[], dates2: any[]): number {
        let s1 = dates2.indexOf(dates1[0]);
        let s2 = dates1.indexOf(dates2[0]);

        if (s1 < 0)
            s1 = 0;
        if (s2 < 0)
            s2 = 0;

        return ShUtil.correlation(correlationFunc, arr1, arr2, s1, 0, s2, 0);
    }

    static correlation(correlationFunc: (a: number[], b: number[]) => number, arr1: any[], arr2: any[], s1: number = 0, e1: number = 0, s2: number = 0, e2: number = 0): number {
        if (!s1) s1 = 0;
        if (!s2) s2 = 0;
        if (!e1) e1 = arr1.length;
        if (!e2) e2 = arr2.length;

        let count1 = e1 - s1;
        let count2 = e2 - s2;

        /// Common case
        if (count1 == count2 && s1 == 0 && s2 == 0 && e1 == e2)
            return correlationFunc(arr1, arr2);

        let count = Math.min(count1, count2);

        /// Otherwise we'll need to splice the array
        let sarr1 = arr1.slice(s1, s1 + count);
        let sarr2 = arr2.slice(s2, s2 + count);

        return correlationFunc(sarr1, sarr2);
    }
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// Promise helper
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
export class PromiseUtil {
    static defer(): any {
        var resolve, reject;
        var d = {
            resolve: null, 
            reject: null,
            promise: null
        };

        d.promise = new Promise(function(resolve, reject) {
            this.resolve = resolve;
            this.reject = reject;
        }.bind(d));

        return d;
        // return eido.Promise.defer();
    }

    static isPromise(promise: any): boolean {
        return typeof promise.then === 'function';
    }

    static serial(funcs) {
        return funcs.reduce((cur, next) => {
            return cur.then(next);
        }, Promise.resolve());

        // return funcs.reduce((promise, func) =>
        //     promise.then(result => func().then(Array.prototype.concat.bind(result))),
        //     eido.Promise.resolve([]));

        // let final = [];

        // return funcs.reduce((promise, func) => {
        //     return promise
        //         .then((result) => {
        //             return func().then(result => final.push(result));
        //         });
        // }, eido.Promise.resolve());
    } 

    static resolvedPromise(val): any {
        // let d = Promise.defer();
        // d.resolve(val);
        // return d.promise;
        return new Promise(function(resolve) {
            resolve(val);
        });
    }

    static failedPromise(e): any {
        // let d = Promise.defer();
        // d.reject(e);
        // return d.promise;
        return new Promise(function(resolve, reject) {
            reject(e);
        });
    }
}

console.exception = function(e: any) {
    if (e.stack)
        console.error(`[Exeption] ${e.stack}`);
    else
        console.error(e.toString());
}


