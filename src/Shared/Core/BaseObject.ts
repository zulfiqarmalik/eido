import { SecrecyLevel } from 'Shared/Model/SecrecyLevel';
import { TypeParser, Model, ModelProperties, ModelProperty } from 'Shared/Model/ShModel';
import { Debug, PromiseUtil } from './ShUtil';
import { eido } from './ShCore';
import { ObjectID } from 'Shared/Core/ObjectID';
import 'reflect-metadata';

export function ModelValue(desc: string, level: SecrecyLevel = SecrecyLevel.public, nullable: boolean = false): any {
    return function (target: any, propertyKey: string) {
        var targetType = (Reflect as any).getMetadata("design:type", target, propertyKey);

        let cls = target.constructor;
        let mprops: ModelProperties = Model.register(cls.__name, cls);
        Debug.assert(mprops, `Unable to find model properties for class: ${cls.__name || cls.__classId || target.name || target.__classId}`);

        let type: string = targetType ? targetType.name : "ObjectID";
        // console.trace(`    => ${propertyKey} : ${type}`);
        let props: ModelProperty = new ModelProperty(type, propertyKey, desc, level);

        props.nullable = nullable;
        mprops.add(props.name, props);
    }
}

export function ModelClassId(cls: any, classId: string): any {
    cls.__name = classId;
    Model.register(classId, cls);
    return function (target: any, propertyKey: string) {
        /// No-op
    }
}

export function ModelObject(desc: string = ""): any {
    return function (target: any, propertyKey: string) {
        // Debug.assert(target.__classId, `Invalid ModelObject declaration without declaring @ModelClass(class, name) at the top!`);

        let name: string = target.__name;

        if (!desc)
            desc = name;

        Debug.assert(name && desc, `[BaseObject] Invalid class info - Name: ${name} - Desc: ${desc}`);

        target.__classId = name;
        let mprops: ModelProperties = Model.register(name, target);

        let cls = target;
        mprops.name = target.__classId;
        mprops.desc = desc;
    }
}

export class CopyOptions {
    level                   : SecrecyLevel      = SecrecyLevel.public;
    noMarshal               : boolean           = false;
    isDB                    : boolean           = false;
    __prop                  : ModelProperty     = null;

    constructor(level: SecrecyLevel = SecrecyLevel.public, isDB: boolean = false) {
        this.level          = level;
        this.isDB           = isDB;
    }
}

// Base eido Object class that is shared between the client as well as the server
@ModelObject()
export class BaseObject implements TypeParser {
    @ModelClassId(BaseObject, "BaseObject") __dummy: string;

    static __defaultOptions : CopyOptions       = new CopyOptions(); /// The default set of options
    static __classId        : string            = "";       /// What is the classId of this object

    constructor() {
        // if (rhs && typeof rhs === 'object') {
        //     this.copyFrom(rhs, new CopyOptions(SecrecyLevel.all));
        // }
    }

    getDefaultOptions(): CopyOptions {
        return BaseObject.__defaultOptions;
    }

    getModelProperties(): ModelProperties {
        return Model.getClassProperties(this.constructor);
    }

    getProps(level: SecrecyLevel): object {
        let props = this.getModelProperties();

        if (!props) {
            // this is a normal object ... make props on the fly ...
            props = ModelProperties.createFromObject(this);
        }

        return props.getProps(level);
    }

    copyFromProps(src: any, mprops: ModelProperties, options?: CopyOptions): any {
        let dst = this;
        
        if (!options) {
            options = this.getDefaultOptions();
        }

        let promises = [];
        let props = mprops.getAllProps();
        
        for (let k in props) {
            let prop: ModelProperty = props[k];

            let v = src[k];

            // ObjectIDs get a special treatment
            if (props[k] && props[k].property === true && props[k].readOnly === true)
                continue;
                
            // the corresponding key must exist in dst
            if (typeof dst[k] === 'undefined' || v === null || v === undefined)
                continue;
            
            // see if its a model object .. if it is then copy is recursively!
            if (typeof dst[k] !== 'undefined' && dst[k] !== null && typeof v == 'object' && 
                (Model.getClass(dst[k].constructor.__classId) || v instanceof Array) && typeof dst[k].copyFrom == 'function') {
                options.__prop = props[k];
                let ret = dst[k].copyFrom(v, options);

                if (PromiseUtil.isPromise(ret)) {
                    ret.then(() => delete options.__prop);
                    promises.push(ret);
                }
                else 
                    delete options.__prop;
                continue;
            }
            
            if (options.level === SecrecyLevel.db && props[k].dbCopy && typeof props[k].dbCopy == 'function')
                v = props[k].dbCopy(v);
            else if (!options.noMarshal) {
                if (typeof v === 'string' && prop.type !== 'string') {
                    let parser: TypeParser = Model.getParser(prop.type);
                    if (parser)
                        v = parser.parse(v, prop);
                }
            }

            if (props[k] && props[k].type === 'ObjectID') 
                v = ObjectID.create(v.toString());

            dst[k] = v;
        }

        // are there any promises that we are waiting for?
        if (promises.length > 0) {
            return Promise.all(promises).then(function() {
                return this;
            }.bind(this));
        }

        return this;
    }

    getApiDocs() {
        return this.getModelProperties().getApiDocs();
    }
    
    clone(options?: CopyOptions) {
        let cls = this.constructor;
        let clone = new (<any>cls());
        clone.copyFrom(this, options);
        
        return clone;
    }

    parse(value: string, prop?: ModelProperty): BaseObject {
        let cls = this.constructor;
        let clone = new (<any>cls());
        let obj = JSON.parse(value);
        return clone.copyFrom(obj);
    }
    
    copyFrom(src: object, options?: CopyOptions): any {
        let dst = this;
        let props = this.getModelProperties(); //Model.getProperties(this);

        // This model object has no props, so nothing to copy over here.
        if (!props)
            return this;

        return this.copyFromProps(src, props, options);
    }

    static isModelContainer(value: any): boolean {
        return typeof value == 'object' && typeof value.__modelContainer !== 'undefined' && value.__modelContainer == true;
    }

    static isModelObject(value) {
        return value != null && value !== undefined && typeof value == 'object' && typeof value.constructor !== 'undefined' && typeof value.cleanClone === 'function';
    }

    static cleanClone(props: ModelProperties, src: object, clone: object, options?: CopyOptions) {
        if (!clone)
            clone = {};
            
        if (!options) {
            options = BaseObject.__defaultOptions;
        }
        else if (typeof options.level === 'undefined')
            options.level = SecrecyLevel.public;
            
        let allProps = props.getAllProps();

        for (let name in allProps) {
            let value = src[name];
            let prop: ModelProperty = allProps[name]; 

            Debug.assert(prop, `Unable to find prop: ${name}`);
            Debug.assert(prop.level, 'Invalid level for the property. Model definition error!');
            // let clLevel = prop.level & SecrecyLevel.clLevels;

            // secret properties are NEVER sent!
            if (!src.hasOwnProperty(name) || typeof value == 'function' || 
                ((prop.level > options.level) && !(prop.level & options.level))) {
                	continue;
				}
                
            // stuff that should not be written to the DB is not put in ... 
            if (prop.noDB === true && options.level === SecrecyLevel.db)
                continue;

            if (value !== undefined && value !== null && typeof value == 'object' && 
                (BaseObject.isModelObject(value) || value instanceof Array)) {

                let didClone = false;

                // if the original property type is embedded then we just copy the embedded stuff
                if (options.level === SecrecyLevel.db || 
                    options.level === SecrecyLevel.embedded || 
                    options.level === SecrecyLevel.embeddedRef) {
                    if ((prop.level & SecrecyLevel.embedded) ||
                        (prop.level & SecrecyLevel.embeddedRef) ||
                        (prop.level <= SecrecyLevel.embedded)) {
                        didClone = true;
                        value = value.embedClone(prop.level);
                    }
                    else if (prop.level & SecrecyLevel.minimal) {
                        didClone = true;
                        value = value.minimalClone();
                    }
                }

                if (!didClone) {
                    if (value instanceof Array) 
                        value = (Array as any).copyFrom(value);
                    else
                        value = value.cleanClone(options);
                }
            }
            else if (value instanceof ObjectID)
                value = value.toString();

            clone[name] = value;
        }
        
        // if we are not writing to the DB then include the class id in there
//        if (!(options.level & SecrecyLevel.db) && (options.level < SecrecyLevel.db)) {
            if (src.constructor && src.constructor["__classId"])
                clone["__classId"] = src.constructor["__classId"];
            else
                clone["__classId"] = 'Object';
//        }

        return clone;
    }
    
    static getNestedParam(object, param) {
        if (!object || !param)
            return null;
        Debug.assert(typeof param === 'string', 'Invalid type of param. Expecting string ...');
        let parts = param.split('.');
        
        // most common scenario
        if (parts.length === 1)
            return object[param];
        
        for (let i = 0; i < parts.length; i++) {
            object = object[parts[i]];
            
            if (object === null || object === undefined)
                return undefined;
        }
        
        return object;
    }
    
    static setNestedParam(object: any, param: any, value: any, onlyIfNone: boolean = false) {
        Debug.assert(object && param && typeof param === 'string', 'Invalid input to setNestedParam');
        let parts = param.split('.');
        
        // most common scenario
        if (parts.length === 1) {
            if (onlyIfNone && (object[parts[parts.length - 1]] == null || object[parts[parts.length - 1]] == undefined) || 
               !onlyIfNone) {
                    object[param] = value;
            }
            return value;
        }
        
        for (let i = 0; i < parts.length - 1; i++) {
            let nobject = object[parts[i]];
            
            if (nobject === null || nobject === undefined) {
                nobject = {};
                object[parts[i]] = nobject;
            }
            
            Debug.assert(typeof nobject == 'object', `Nested type is not an object: ${parts[i]}`);
            object = nobject;
        }
        
        // assign the value at last
        if (onlyIfNone && (object[parts[parts.length - 1]] == null || object[parts[parts.length - 1]] == undefined) || 
           !onlyIfNone)
            object[parts[parts.length - 1]] = value;
        
        
        return value;
    }

    embedClone(level) {
        return this.cleanClone(level);
    }

    minimalClone() {
        return this.cleanClone(SecrecyLevel.minimal);
    }

    // creates a clean shallow clone
    cleanClone(options: CopyOptions | SecrecyLevel): any {
        if (!(options instanceof CopyOptions))
            options = new CopyOptions(options);

        // if (options && 
        //     ((options.level & SecrecyLevel.embedded) || 
        //     (options.level & SecrecyLevel.embeddedRef)))
        //     Debug.assert(false, 'Change this method to use embedClone!');

        let props: ModelProperties = this.getModelProperties();

        if (!props) {
            // this is a normal object ... make props on the fly ...
            props = ModelProperties.createFromObject(this);
        }

        return BaseObject.cleanClone(props, this, {}, options);
    }
    
    toClass(cls: any, options: CopyOptions) {
        let props: ModelProperties = Model.getClassProperties(cls);
        let clone = new (<any>cls());
        return BaseObject.cleanClone(props, this, clone, options);
    }

    static validateData(self: BaseObject, cls?: any) {
        if (!cls)
            cls = Model.getClass((self.constructor as any).__classId);

        let props = self.getModelProperties();
        let names = props.getNames();

        for (let i = 0; i < names.length; i++) {
            let name = names[i];
            let prop = props.getProp(name);

            // if the property is nullable then we do not really care
            if (prop.nullable === true)
                continue;

            // if the property is not nullable then we must check
            let v = self[name];
            if (v === null || v === undefined)
                return false;

            // if its a model class object then validate that using the validation function as well
            if (typeof v == 'object' && Model.getClass(v.constructor.__classId) && typeof v.validateData === 'function') {
                if (!v.validateData())
                    return false;
            }
            // else if (Object.prototype.toString.call(v) === '[object Array]') {
            //     // is the array allowed to be empty
            // }
            
            return true;
        }
    }

    validateData() {
        return BaseObject.validateData(this);
    }

    static getDiff(current: BaseObject, update: boolean, prefix: string, diff: object, props?: ModelProperties) {
        if (!props)
            props = current.getModelProperties();

        for (let k in props) {
            if (k === '_id')
                continue;

            //Debug.assert(src[k] !== undefined, 'Expecting source to have property: ' + k);
            let prop = props[k];

            let currentValue = current[k];

            if (props[k] && props[k].property === true && props[k].readOnly === true)
                continue;

            let updateValue = update[k];

            // if the update value is undefined or the same as the current value then nothing has changed ...                
            if (updateValue === undefined || updateValue === currentValue)
                continue;

            if (currentValue instanceof Date && !(currentValue > updateValue || currentValue < updateValue))
                continue;
            
            // see if its a model object .. if it is then copy is recursively!
            if (typeof updateValue == 'object' && Model.getClass(updateValue.constructor.__classId) && typeof updateValue.copyFrom == 'function') {
                let newPrefix;

                if (prefix !== undefined) {
                    if (prefix)
                        newPrefix = prefix + "." + k;
                    else
                        newPrefix = k;
                }

                let newDiff = newPrefix ? diff : {};
                BaseObject.getDiff(currentValue, updateValue, newPrefix, newDiff);
                continue;
            }

            if (prefix) {
                diff[prefix + '.' + k] = updateValue;
            }
            else {
                diff[k] = updateValue;
            }
        }

        return diff;
    }

    getDiff(other) {
        return BaseObject.getDiff(this, other, "", {});
    }

    static deseriliseResponse(response: any) {
        // now that we have a response we need to convert it instantiate the appropriate class 
        // if the said response contains a __classId then we try to desezilize the response 
        // into that
        
        if (response instanceof Array)
            (response as any).__classId = 'array';
        
        if (response && response.__classId && response.__classId !== "Object") {
            // see whether we have a model of that type ...
            let cls = Model.getClass(response.__classId);
            Debug.assert(cls, 'Invalid classId from server!');
            if (cls) {
                // convert the data into that model ...
                let instance = new cls();
                
                // do a shallow copy ...
                instance.copyFrom(response, {
                    level: SecrecyLevel.all,
                    deserialize: true // this is just for server side tests ...
                });
                
                // we're done!
                return instance;
            }
        }
        
        // if nothing was found then we just return the response as is ... this is not for us
        return response;
    }

    static clone(obj) {
        if (obj === null || typeof(obj) !== 'object' || 'isActiveClone' in obj)
            return obj;

        let temp = obj.constructor(); // changed

        for (let key in obj) {
            if (Object.prototype.hasOwnProperty.call(obj, key)) {
                obj['isActiveClone'] = null;
                temp[key] = BaseObject.clone(obj[key]);
                delete obj['isActiveClone'];
            }
        }

        return temp;
    }
}

