import { eido } from 'Shared/Core/ShCore';
import { SecrecyLevel } from 'Shared/Model/SecrecyLevel';
import { Model } from 'Shared/Model/ShModel';
import { Debug } from 'Shared/Core/ShUtil';
import { ModelObject, ModelValue, ModelClassId } from 'Shared/Core/BaseObject';
import { CObject } from 'Shared/Core/CObject';
import * as path from 'path';

////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// PnlPoint - One PNL point (repnresenting 1 day)
////////////////////////////////////////////////////////////////////////////////////////////////////////////////
@ModelObject()
export class GroupPnlPoint extends CObject {
    @ModelClassId(GroupPnlPoint, "GroupPnlPoint") __dummy: string;

    @ModelValue("The date for the data.")
    date: number;
    
    @ModelValue("The pnl.")
    pnl: number;
    
    @ModelValue("The book size at this point.")
    bookSize: number;
    
    constructor(date: number, pnl: number, bookSize: number) {
        super();

        this.date = date;
        this.pnl = pnl;
        this.bookSize = bookSize;
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// GroupStrategy - One strategy for the group!
////////////////////////////////////////////////////////////////////////////////////////////////////////////////
@ModelObject()
export class GroupStrategy extends CObject {
    @ModelClassId(GroupStrategy, "GroupStrategy") __dummy: string;

    @ModelValue("The name of this Group strategy.")
    name: string;
    
    @ModelValue("The starting date of the strategy.")
    startDate: number = 0;
    
    @ModelValue("The ending date of the strategy.")
    endDate: number = 0;
    
    @ModelValue("An array of all the pnl points.",)
    pnls: Array<GroupPnlPoint> = new Array<GroupPnlPoint>();

    constructor(name: string = "") {
        super();
        this.name = name;
    }

    add(date: number, pnl: number, bookSize: number) {
        if (!this.startDate)
            this.startDate = date;

        if (this.endDate < date)
            this.endDate = date;

        this.pnls.push(new GroupPnlPoint(date, pnl, bookSize));

        return this;
    }

    loadString(parts: string[], date: number, pnlIndex: number, bookSizeIndex: number) {
        if (parts.length <= pnlIndex || parts.length <= bookSizeIndex)
            return this;

        if (parts[pnlIndex] && parts[bookSizeIndex]) {
            let pnl = parseFloat(parts[pnlIndex].trim());
            let bookSize = parseFloat(parts[bookSizeIndex].trim());
            return this.add(date, pnl, bookSize);
        }

        return this;
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// GroupInfo - All the strategiens
////////////////////////////////////////////////////////////////////////////////////////////////////////////////
@ModelObject()
export class GroupInfo extends CObject {
    @ModelClassId(GroupInfo, "GroupInfo") __dummy: string;

    @ModelValue("All the strategies in the group.")
    strategies: Array<GroupStrategy> = new Array<GroupStrategy>();

    constructor() {
        super();
    }

    loadString(str: string) {
        let lines = str.split(/\n/); 
        if (!lines.length)
            return;

        let eqUS = new GroupStrategy("US Equity");
        let eqEU = new GroupStrategy("EU Equity");
        let futures = new GroupStrategy("Futures");
        let fx = new GroupStrategy("FX");

        this.strategies = new Array<GroupStrategy>();
        this.strategies.push(eqUS);
        this.strategies.push(eqEU);
        this.strategies.push(futures);
        this.strategies.push(fx);

        for (let li: number = 1; li < lines.length; li++) {
            let line = lines[li];
    
            let parts = line.split(',');
            if (!parts.length)
                return false; 

            let date = parseInt(parts[0].trim());

            futures.loadString(parts, date, 1, 5);
            fx.loadString(parts, date, 2, 6);
            eqUS.loadString(parts, date, 3, 7);
            eqEU.loadString(parts, date, 4, 8);
        }

        return this;
    }
}

export default function init() {
    console.info("[Model] Init GroupPnl");

    // initialize the model!
    new GroupPnlPoint(0, 0, 0);
    new GroupStrategy();
    new GroupInfo();
}
