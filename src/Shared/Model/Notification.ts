import { eido } from 'Shared/Core/ShCore';
import { ObjectID } from "Shared/Core/ObjectID";
import { SecrecyLevel } from 'Shared/Model/SecrecyLevel';
import { PushNotificationType } from 'Shared/Core/PushNotificationType';
import { ModelObject, ModelValue, ModelClassId } from 'Shared/Core/BaseObject';
import { CObject } from '../Core/CObject';

////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// Notification - Base notification class (no server side implementation available)
////////////////////////////////////////////////////////////////////////////////////////////////////////////////
@ModelObject()
export class Notification extends CObject {
    @ModelClassId(Notification, "Notification") __dummy: string;
    
    @ModelValue("The ID of the notification.")
    id              : string;

    @ModelValue("The time when this was sent.")
    dispatchTime    : Date;

    static get id(): string { return ""; }

    constructor(id?: string, ...args) {
        super();
        this.id = id || (this.constructor as typeof Notification).id;
    }

    wsSend(userIds: ObjectID[], excludeIds: ObjectID[] = null) {
        if (!eido.appServer || !eido.appServer.wsServer)
            return;

        return eido.appServer.wsServer.sendData(require('util').isArray(userIds) ? userIds : [userIds], this, excludeIds);
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// TaskUpdate - Task update notification
////////////////////////////////////////////////////////////////////////////////////////////////////////////////
@ModelObject()
export class TaskUpdate extends Notification {
    @ModelClassId(TaskUpdate, "TaskUpdate") __dummy: string;

    static get id(): string { return PushNotificationType.taskUpdate; }

    @ModelValue("The percentage completion.")
    percent             : number;
    
    @ModelValue("The status message.")
    statusMessage       : string;
    
    constructor(percent: number = 0.0, statusMessage: string = "") {
        super();

        this.percent = percent;
        this.statusMessage = `Math.round(percent * 100)% - ${statusMessage}`;
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// TaskFinished - Task update notification
////////////////////////////////////////////////////////////////////////////////////////////////////////////////
@ModelObject()
export class TaskFinished extends Notification {
    @ModelClassId(TaskFinished, "TaskFinished") __dummy: string;

    static get id() { return PushNotificationType.taskFinished; }

    @ModelValue("The completion status.")
    status          : number;
    
    @ModelValue("The status message.")
    statusMessage   : String;
    
    @ModelValue("The actual result object.")
    result          : Object;
        
    constructor(status: number = 0, statusMessage: string = "", result: object = null) {
        super();

        this.status = status;
        this.statusMessage = statusMessage;
        this.result = result;
    }
}

export default function init() {
    console.info("[Model] Init Notification");

    // initialize the model!
    new Notification();
    new TaskUpdate();
    new TaskFinished();
}
