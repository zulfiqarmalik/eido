import { ConfigSection } from "./ConfigSection";
import { ShAlpha } from "./ShAlpha";
import { ShStrategy } from "./ShStrategy";

export class Config extends ShStrategy {
    constructor() {
        super();
    }
}

export default function init() {
    console.info("[Model] Init Config");

    // initialize the model!
    new ConfigSection();
    new ShAlpha();
    new ShStrategy();
    new Config();
}
