import { ModelObject, ModelValue, ModelClassId } from 'Shared/Core/BaseObject';
import { CObject } from 'Shared/Core/CObject';

@ModelObject()
export class ConfigSection extends CObject {
    @ModelClassId(ConfigSection, "ConfigSection") __dummy: string;

    @ModelValue("The name of the section.")
    m_name: string = "";
    
    @ModelValue("The alpha details.")
    map: any = {};
    
    @ModelValue("The child sections.")
    children: ConfigSection[] = [];
        
    constructor(name: string = "", map: any = null, children: ConfigSection[] = []) {
        super();

        if (name && map && children) {
            this.m_name = name;
            this.map = map;

            // this.children = new Array<ConfigSection>(children.length);
            this.children = [];

            children.map((child, ci) => {
                if (child)
                    this.children.push(new ConfigSection(child.m_name, child.map, child.children || []));
            });
        }
    }

    uuid()          { return this.map["uuid"];     }
    id()            { return this.map["id"];       }
    type()          { return this.map["type"];     }
    name()          { return this.m_name;       }
    
    getString(name: string, def: string): string  { return this.map[name] || def; }

    findChildWithId(id: string): ConfigSection {
        if (!this.children || !this.children.length)
            return null;

        for (let i = 0; i < this.children.length; i++) {
            let child = this.children[i];
            if (child.id() == id)
                return child;
        }

        return null;
    }

    findAllChildrenWithName(name: string): ConfigSection[] {
        if (!this.children || !this.children.length)
            return [];

        let filteredChildren: ConfigSection[] = [];

        for (let i = 0; i < this.children.length; i++) {
            let child = this.children[i];
            if (child.m_name == name)
                filteredChildren.push(child);
        }

        return filteredChildren;
    }

    getAllOperations(): ConfigSection[] {
        return this.findAllChildrenWithName("Operation");
    }

    findChildIndexWithName(name: string): number {
        if (!this.children || !this.children.length)
            return -1;

        for (let i = 0; i < this.children.length; i++) {
            let child = this.children[i];
            if (child.m_name == name)
                return i;
        }

        return -1;
    }

    findChildWithName(name: string): ConfigSection {
        let index: number = this.findChildIndexWithName(name);
        return index < 0 ? null : this.children[index];
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /// Static methods
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    static loadFromXmlElt(xmlDoc: XMLDocument, elt: any): ConfigSection {
        let sec = new ConfigSection();
        sec.m_name = elt.nodeName;
        sec.map = {};
        sec.children = [];

        if (elt.attributes && elt.attributes.length) {
            for (let i = 0; i < elt.attributes.length; i++) {
                let attrib = elt.attributes[i];
                sec.map[attrib.name] = attrib.value;
            }
        }

        if (elt.childNodes && elt.childNodes.length) {
            for (let i = 0; i < elt.childNodes.length; i++) {
                let childNode = elt.childNodes[i];
                if (childNode.nodeName[0] != '#') {
                    let childSec = ConfigSection.loadFromXmlElt(xmlDoc, childNode);
                    sec.children.push(childSec);
                }
            }
        }

        return sec;
    }
}

