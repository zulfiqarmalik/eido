import { eido } from 'Shared/Core/ShCore';
import { ObjectID } from "Shared/Core/ObjectID";
import { Model } from 'Shared/Model/ShModel';
import { Debug } from 'Shared/Core/ShUtil';
import { ConfigSection } from "./ConfigSection";
import { ShAlpha, AlphaType } from "./ShAlpha";
import { ModelObject, ModelValue, ModelClassId } from 'Shared/Core/BaseObject';

export const enum RawStatsType {
    normal      = "",
    raw         = "RAW",
    preopt      = "PRE_OPT"
}

@ModelObject()
export class ShStrategy extends ShAlpha {
    @ModelClassId(ShStrategy, "ShStrategy") __dummy: string; 

    static embeddedStrategyTypes: string[] = ["PosStrategyDB", "PosStrategy", "Strategy"];

    @ModelValue("The imports section.")
    imports: ConfigSection[] = [];
    
    @ModelValue("The modules section.")
    modules: ConfigSection = null;
        
    @ModelValue("Whether this is a master strategy or not.")
    isMaster: boolean = false;
        
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /// Not part of the Model
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    _allAlphas              : ConfigSection[]   = null;         /// All the alphas of this strategy

    constructor() {
        super();
    }

    getAllAlphasFromSection(alphasSec: ConfigSection, alphasList: ConfigSection[]): ConfigSection[] {
        /// This alphas section needs to be added too
        alphasSec.children.map((alphaSec) => {
            if (ShAlpha.embeddedAlphaTypes.indexOf(alphaSec.m_name) >= 0)
                alphasList.push(alphaSec);
            // if (alphaSec.m_name === 'Alpha')
        });

        return alphasList;
    }

    getNumAlphas(): number {
        return this._allAlphas ? this._allAlphas.length : 0;
    }

    getFirstAlpha(): ConfigSection {
        let allAlphas = this.getAllAlphas();
        return allAlphas.length ? allAlphas[0] : null;
    }

    getAllAlphas(): ConfigSection[] {
        if (this._allAlphas)
            return this._allAlphas;

        this._allAlphas = [];
        let mainAlphasSec = this.portfolio.findChildWithName("Alphas");

        if (!mainAlphasSec) 
            return this._allAlphas;

        this.getAllAlphasFromSection(mainAlphasSec, this._allAlphas);
        return this._allAlphas;
    }

    getAllStrategies(): ConfigSection[] {
        let mainAlphasSec = this.portfolio.findChildWithName("Alphas");

        if (!mainAlphasSec)
            return [];

        return mainAlphasSec.findAllChildrenWithName(AlphaType.strategy);
    }

    hasStrategy(strategyId: ObjectID, types: string[] = ShStrategy.embeddedStrategyTypes): number {
        return this.hasAlpha(strategyId, types);
    }

    getDefaultBookSize(): number {
        return 100e6;
    }

    getAlphaIndex(alphaId_: ObjectID, types: string[] = ShAlpha.embeddedAlphaTypes): number {
        if (!alphaId_)
            return -1;
            
        let alphasSec = this.portfolio.children[0];
        let alphaId = alphaId_.toString();
        // let filter = alphasSec.children.filter((alphaSec: ConfigSection, index: number) => types.indexOf(alphaSec.m_name) >= 0 && alphaSec.map["iid"] == alphaId).length > 0;

        for (let i = 0; i < alphasSec.children.length; i++) { 
            let alphaChild = alphasSec.children[i];
            if (types.indexOf(alphaChild.m_name) >= 0 && alphaChild.map["iid"] == alphaId)
                return i;
            // if ((alphaChild.m_name == "PosAlphaDB" || alphaChild.m_name == "PosAlpha" || alphaChild.m_name == "Alpha") && 
            //     alphaChild.map["iid"] == alphaId)
            //     return i;
        }

        return -1;
    }

    hasAlpha(alphaId: ObjectID, types: string[] = ShAlpha.embeddedAlphaTypes): number {
        return alphaId ? this.getAlphaIndex(alphaId, types) : -1;
    }
    
    loadImports(xmlDoc: XMLDocument): ConfigSection[] {
        this.imports = [];

        let elts = xmlDoc.getElementsByTagName("Import");
        if (!elts || elts.length == 0)
            return;

        // Debug.assert(elts, "Unable to load element: ", name);
        // Debug.assert(elts.length > index, "In sufficient number of elements: ", elts.length, " - Requested index: ", index);

        this.imports = new Array(elts.length);
        for (let i = 0; i < elts.length; i++) {
            this.imports[i] = ConfigSection.loadFromXmlElt(xmlDoc, elts[i]);
        }

        return this.imports;
    }

    loadXML(xmlString: string): ShStrategy {
        let xmlDoc = super.loadXML(xmlString);

        this.loadImports(xmlDoc);
        let { Config } = require('./Config');
        this.modules = Config.loadSection(xmlDoc, null, 'Modules', 0);

        return this;
    }
    

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /// Static methods
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
}

