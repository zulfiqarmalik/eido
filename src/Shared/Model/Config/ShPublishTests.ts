import { Env, eido } from 'Shared/Core/ShCore';
import { ObjectID } from "Shared/Core/ObjectID";
import { Model } from 'Shared/Model/ShModel';
import { Debug } from 'Shared/Core/ShUtil';
import { BaseConfig } from "./BaseConfig";
import { ModelObject, ModelValue, ModelClassId } from 'Shared/Core/BaseObject';
import { CObject } from 'Shared/Core/CObject';

export const Thresholds = {
    corrISOS: {
        max: 0.65
    },

    corrSub: {
        min: 0.65
    },

    corrSuper: {
        min: 0.65
    },

    alpha: {
        min: -0.65,
        max: 0.65
    }
}

@ModelObject()
export class ShPublishTests extends CObject {
    @ModelClassId(ShPublishTests, "ShPublishTests") __dummy: string; 

    @ModelValue("The unique id. This is the same as alpha/strategy id")
    _id: ObjectID = ObjectID.create();
    
    @ModelValue("The in-sample correlation with out sample.")
    corrISOS: number = 0.0;
    
    @ModelValue("The sample correlation with sub sample.")
    corrSub: number = 0.0;
    
    @ModelValue("The sample correlation with super sample.")
    corrSuper: number = 0.0;
    
    @ModelValue("Top 5 correlated alphas.")
    topCorrelations: ObjectID[] = [];
    
    @ModelValue("Bottom 5 correlated alphas.")
    bottomCorrelations: ObjectID[] = [];
        
    constructor() {
        super();
    }
};

