import { ModelObject, ModelValue, ModelClassId } from "Shared/Core/BaseObject";
import { CObject } from "Shared/Core/CObject";
import { SecrecyLevel } from "../SecrecyLevel";

@ModelObject()
export class AlphaFeed<T> extends CObject { 
    @ModelClassId(AlphaFeed, "AlphaFeed") __dummy: string; 

    @ModelValue("The market.", SecrecyLevel.public)
    market                  : string = "US";
    
    @ModelValue("The universe.", SecrecyLevel.public)
    universeId              : string;
    
    @ModelValue("The start index.", SecrecyLevel.public)
    startId                 : number = 0;
    
    @ModelValue("How many alphas are in this feed payload.", SecrecyLevel.public)
    count                   : number = 0;

    @ModelValue("Total count of the alphas in the feed matching the original search.", SecrecyLevel.public)
    totalCount              : number = 0;
    
    @ModelValue("The alphas sent back in the feed.", SecrecyLevel.public)
    alphas                  : T[] = [];              /// The alphas sent in THIS alpha feed

    constructor(market: string = "US", universeId: string = "ALL") {
        super();
        this.market         = market;
        this.universeId     = universeId;
    }
};

