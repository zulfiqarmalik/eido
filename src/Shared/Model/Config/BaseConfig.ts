import { eido } from 'Shared/Core/ShCore';
import { ObjectID } from "Shared/Core/ObjectID";
import { Model } from 'Shared/Model/ShModel';
import { SecrecyLevel } from 'Shared/Model/SecrecyLevel';
import { ModelObject, ModelValue, ModelClassId } from 'Shared/Core/BaseObject';
import { CObject } from 'Shared/Core/CObject';

@ModelObject()
export class BaseConfig extends CObject {
    @ModelClassId(BaseConfig, "BaseConfig") __dummy: string;

    @ModelValue("The unique id of this strategy", SecrecyLevel.minimal)
    _id: ObjectID = ObjectID.create();
    
    @ModelValue("The id of the user who owns this config.", SecrecyLevel.minimal)
    ownerId: ObjectID = ObjectID.create();
    
    @ModelValue("Whether this strategy is on the filesystem or DB.", SecrecyLevel.public)
    isFilesystem: boolean = false;
    
    @ModelValue("The user friendly name of this config.", SecrecyLevel.minimal)
    name: string = "";
    
    @ModelValue("The path of this strategy on the filesystem or the DB path in case it's from a database.", SecrecyLevel.public)
    path: string = "";
    
    @ModelValue("When was this created.", SecrecyLevel.minimal)
    creationDate: Date = new Date();
    
    @ModelValue("Last time this was modified.", SecrecyLevel.minimal)
    modifiedDate: Date = new Date();
    
    @ModelValue("The date that this config was published.", SecrecyLevel.minimal)
    publishedDate: Date = new Date();
            
    @ModelValue("Whether the config is pruned or not.", SecrecyLevel.minimal)
    pruned: boolean = false;
            
    constructor() {
        super();
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /// Static methods
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
}

export class LiveStats {
    pnls: number[] = [];
    dates: number[] = [];
}

