import { ObjectID } from "Shared/Core/ObjectID";
import { Debug } from 'Shared/Core/ShUtil';
import { BaseConfig } from "./BaseConfig";
import { ConfigSection } from "./ConfigSection";
import { ModelObject, ModelValue, BaseObject, ModelClassId } from 'Shared/Core/BaseObject';

import { LTStats } from '../Stats/LTStats';
import { SecrecyLevel } from '../SecrecyLevel';

let path = require('path');

export enum AlphaType {
    any = "",
    alpha = "Alpha",
    strategy = "Strategy"
}

export enum AlphaSubType {
    none = "",
    python = 'python',
    expr = 'expr',
    auto = 'auto'
}

export enum ConfigType {
    any = "",
    user = 'user',
    auto = 'auto',
}

export enum AlphaBucket {
    PriceVolume = 'PriceVolume',
    Event = 'Event',
    Fundamental = 'Fundamental',
    BalanceSheet = 'BalanceSheet',
    Sentiment = 'Sentiment',
    Analyst = 'Analyst',
    Sector = 'Sector',
    Industry = 'Industry',
    Other = 'Other',
}

export enum SampleType {
    LT = 'LT',
    IS = 'IS',
    OS = 'OS',
    PS = 'PS',
}

export enum AssetClass {
    Equities = 'Equities',
    Futures = 'Futures'
}

@ModelObject()
export class AlphaParentInfo {
    @ModelClassId(AlphaParentInfo, "AlphaParentInfo") __dummy: string;

    @ModelValue("The iid of the parent.", SecrecyLevel.minimal)
    iid: ObjectID;

    @ModelValue("The name of the parent.", SecrecyLevel.minimal)
    name: string;
}


@ModelObject()
export class ShAlpha extends BaseConfig {
    @ModelClassId(ShAlpha, "ShAlpha") __dummy: string;

    static embeddedAlphaTypes: string[] = ["Strategy", "PyPosAlphaDB", "PosAlpha", "Alpha"];
 
    @ModelValue("The market that this belongs to.", SecrecyLevel.minimal)
    market: string = "";
    
    @ModelValue("The category of this alpha.", SecrecyLevel.minimal)
    category: string = "";
    
    @ModelValue("What is the type of this alpha. Is it a python or just an expression.", SecrecyLevel.minimal)
    subType: AlphaSubType = AlphaSubType.auto;
    
    @ModelValue("What is the type of this alpha. Is it a python or just an expression.", SecrecyLevel.minimal)
    type: AlphaType = AlphaType.alpha;
    
    @ModelValue("What type of config is it.", SecrecyLevel.minimal)
    configType: string = ConfigType.any;
    
    @ModelValue("Whether this has been published or not.", SecrecyLevel.minimal)
    isPublished: boolean = false;
    
    @ModelValue("Is the alpha currently running or not.", SecrecyLevel.minimal)
    isRunning: boolean = false;
    
    @ModelValue("Has it finished running.", SecrecyLevel.minimal)
    isFinished: boolean = false;
    
    @ModelValue("Has the process been cancelled.", SecrecyLevel.minimal)
    isCancelled: boolean = false;
    
    @ModelValue("The exit status of the last run.", SecrecyLevel.minimal)
    exitStatus: number = 0;
    
    @ModelValue("The percent run.", SecrecyLevel.minimal)
    percent: number = 0.0;
    
    @ModelValue("DBName", SecrecyLevel.minimal)
    containerId: string = "pesa";

    @ModelValue("The source code of the alpha.")
    source: string = "";
    
    @ModelValue("The name of this alpha.")
    refConfig: string = "";
    
    @ModelValue("All the log messages.")
    log: string[] = [];
    
    @ModelValue("The key stats of the alpha.", SecrecyLevel.minimal)
    keyStats: LTStats = null;
    
    @ModelValue("The defs section.", SecrecyLevel.minimal)
    defs: ConfigSection = new ConfigSection();
    
    @ModelValue("The portfolio section.", SecrecyLevel.minimal)
    portfolio: ConfigSection = new ConfigSection();
    
    @ModelValue("The parent info.", SecrecyLevel.minimal)
    parentInfo: AlphaParentInfo[] = [];
    
    @ModelValue("The version number of the alpha/strategy")
    version: number = 0;
    
    @ModelValue("If there were any errors while publishing or otherwise")
    lastError: string = "";
    
    @ModelValue("The sub-alpha id")
    subAlphaId: ObjectID = ObjectID.create();
    
    @ModelValue("The super-alpha id")
    superAlphaId: ObjectID = ObjectID.create();

    @ModelValue("Exit code")
    exitCode: number = 0;

    @ModelValue("Has it failed or not")
    isFailed: boolean = false;

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /// Not part of the Model
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    _posIndex: number = -1;
    _statsIndex: number = -1;
    
    constructor() {
        super();
    }

    create(userId: ObjectID, name: string, category: string, market: string, universeId: string, subType: AlphaSubType, type: AlphaType) {
        this._id            = ObjectID.create();
        this.ownerId        = userId;
        this.name           = name;
        this.market         = market;
        this.category       = category;
        this.subType        = subType || AlphaSubType.python;
        this.type           = type;
        this.bucket         = AlphaBucket.PriceVolume;

        this.defs.m_name    = "Defs";
        this.defs.map       = {
            exactStartDate  : "true",
            isUserConfig    : "true"
        };
        
        this.defs.children  = [];

        this.portfolio.m_name = "Portfolio";
        this.portfolio.map  = {
            uuid            : "MyPort",
            iid             : this._id.toString(),
        };

        return this;
    }

    isPython(): boolean { return this.subType == AlphaSubType.python; }
    isExpression(): boolean { return !this.subType || this.subType == AlphaSubType.expr || this.subType == AlphaSubType.auto; }

    loadXMLDoc(xmlString: string): XMLDocument {
        let parser = new DOMParser();
        let xmlDoc = parser.parseFromString(xmlString, "text/xml");

        return xmlDoc;
    }

    loadXML(xmlString: string): any {
        let xmlDoc = this.loadXMLDoc(xmlString);

        let { Config } = require('./Config');
        this.defs = Config.loadSection(xmlDoc, null, 'Defs', 0);
        this.portfolio = Config.loadSection(xmlDoc, null, 'Portfolio', 0);

        return xmlDoc;
    }

    static getDbName(market: string, origin: ConfigType): string {
        // if (origin == AlphaOrigin.user)
        //     return `pesa_${market.toLowerCase()}`;
        // return `pesa_${market.toLowerCase()}_auto`;
        return "pesa";
    }

    static durationId(duration, startDate, endDate, calcPos, calcIC) {
        if (startDate && endDate) {
            return `${duration}_${startDate}-${endDate}-${calcPos}-${calcIC}`;
        }

        return duration;
    }
    
    get isUserConfig(): boolean { return this.defs  ["isUserConfig"] == 'true'; }

    getDelay(): number {
        if (this.portfolio && this.portfolio.map && typeof this.portfolio.map.delay !== 'undefined') 
            return parseInt(this.portfolio.map.delay);
        return 0;
    }

    setDelay(delay: number) {
        this.portfolio.map.delay = delay == 0 ? "0" : "1";
    }

    getUniverseId(): string { 
        if (this.portfolio && this.portfolio.map) 
            return this.portfolio.map.universeId;
        else if (this.portfolio && this.portfolio.children && this.portfolio.children.length)
            return this.portfolio.children[0].map.universeId;
        return getDefUniverse(this.getAssetClass()); 
    }

    setUniverseId(universeId: string) {
        this.portfolio.map.universeId = universeId;

        if (this.type == AlphaType.alpha)
            this.portfolio.children[0].map.universeId = universeId;
    }

    get startDate(): string { return this.defs.map["startDate"]; }
    set startDate(value: string) { this.defs.map["startDate"] = value; }

    get bucket(): AlphaBucket { return this.defs.map["bucket"] as AlphaBucket; }
    set bucket(value) { this.defs.map["bucket"] = value; }

    get expr(): string { 
        if (!this.isExpression())
            return "";
        return this.portfolio.children && this.portfolio.children.length ? this.portfolio.children[0].map["expr"] : "";
    }

    set expr(value) {
        if (!this.isExpression())
            return;
        this.portfolio.children[0].map["expr"] = value;
    }

    get operations(): ConfigSection[] {
        return this.portfolio.children[0].getAllOperations();
    }

    findPortfolioSection(name: string): number {
        for (let i = 0; i < this.portfolio.children.length; i++) {
            if (this.portfolio.children[i].m_name == name)
                return i;
        }
    }

    get posSection(): ConfigSection {
        if (this._posIndex < 0) 
            this._posIndex = this.findPortfolioSection("Pos");

        if (this._posIndex >= 0)
            return this.portfolio.children[this._posIndex];

        return null;
    }

    get statsSection(): ConfigSection {
        if (this._statsIndex < 0) 
            this._statsIndex = this.findPortfolioSection("Stats");

        if (this._statsIndex >= 0)
            return this.portfolio.children[this._statsIndex];

        return null;
    }

    getDefaultBookSize(): number {
        return 20e6;
    }

    getBookSize(): number {
        if (this.portfolio.map["bookSize"]) {
            return parseFloat(this.portfolio.map["bookSize"]);
        }

        return this.getDefaultBookSize();
    }

    setBookSize(bookSize: number) {
        this.portfolio.map.bookSize = bookSize.toExponential(2);
    }

    setSource(source: string) {
        if (this.isPython()) {
            this.source = source;
            return this;
        }

        this.portfolio.children[0].map["expr"] = source;
        return this;
    }

    getSource(): string {
        if (this.isPython())
            return this.source;
        return this.portfolio.children[0].map["expr"];
    }

    getStatsPath(): string {
        let stats = this.statsSection;
        Debug.assert(stats, `[Alpha] Unable to load stats section: ${this._id}`);
        return path.join(stats.map["fileDir"], this._id.toString());
    }

    getPosPath(): string {
        let pos = this.posSection;
        Debug.assert(pos, `[Alpha] Unable to load pos section: ${this._id}`);
        return path.join(pos.map["dir"], this._id.toString());
    }

    getAssetClass(): AssetClass {
        return ShAlpha.getAssetClassForMarket(this.market);
    }

    isEquities(): boolean {
        return this.getAssetClass() == AssetClass.Equities;
    }

    isFutures(): boolean {
        return this.getAssetClass() == AssetClass.Futures;
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /// Static methods
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // static loadSectionFromElt(xmlDoc: XMLDocument, elt: Node): ConfigSection {
    //     let sec = new ConfigSection();
    // }
    static getAssetClassForMarket(market: string) {
        if (market.endsWith("Fut"))
            return AssetClass.Futures;
        return AssetClass.Equities;
    }

    static loadSection(xmlDoc: XMLDocument, parentElt: Node, name: string, index: number): ConfigSection {
        if (!parentElt) 
            parentElt = xmlDoc.documentElement;

        /// If the element does not exist then just return an empty one
        let elts = xmlDoc.getElementsByTagName(name);
        if (!elts || elts.length == 0)
            return new ConfigSection();

        // Debug.assert(elts, "Unable to load element: ", name);
        // Debug.assert(elts.length > index, "In sufficient number of elements: ", elts.length, " - Requested index: ", index);

        return ConfigSection.loadFromXmlElt(xmlDoc, elts[index]);
    }
}

export class MarketInfo {
    name: string;
    assetClass: AssetClass;
    title: string;
}

export const Markets: MarketInfo[] = [{
        name: "US",
        assetClass: AssetClass.Equities,
        title: "North America"
    }, {
        name: "EU",
        assetClass: AssetClass.Equities,
        title: "Western Europe"
    }, {
        name: "JP",
        assetClass: AssetClass.Equities,
        title: "Japan"
    }, {
        name: "AP",
        assetClass: AssetClass.Equities,
        title: "Asia-Pacific (ex. JP)"
    }, {
        name: "USFut",
        assetClass: AssetClass.Futures,
        title: "US Commodity/Index Futures"
    }
];

export const SecMasterId = "SecurityMaster"

export const Universes = { 
    "US": ["Top100", "Top200", "Top500", "Top1000", "Top1500", "Top3000", "Tech"], 
    "CA": ["Top100", "Top200", "Top400"], 
    "LAT": ["Top100", "Top200", "Top400"], 
    "EU": ["Top100", "Top200", "Top400", "Top800", "Top1000", "Top1200"], 
    "EEU": ["Top100"], 
    "JP": ["Top100", "Top200", "Top500", "Top1000", "Top1200"], 
    "AS": ["Top100", "Top200", "Top300"], 
    "AU": ["Top100", "Top200"], 
    "USFut": ["Cmdt", "Energy", "Treasuries", "Indices", "ALL"]
}

export class RefConfigInfo {
    name: string;
    filename: string;

    constructor(name: string, filename: string) {
        this.name = name;
        this.filename = filename;
    }
}

export const StrategyRefConfigs = {
    "Default": [
        new RefConfigInfo("High Turnover / High Factor", "{$APP}/Configs/Misc/{$market}/minimal_ref_{$universeId}_high_tvr_high_factor.xml"),
        new RefConfigInfo("High Turnover / Low Factor", "{$APP}/Configs/Misc/{$market}/minimal_ref_{$universeId}_high_tvr_low_factor.xml"),
        new RefConfigInfo("Very High Turnover / Default Factor", "{$APP}/Configs/Misc/{$market}/minimal_ref_{$universeId}_very_high_tvr.xml"),
        new RefConfigInfo("Low Turnover / Low Factor", "{$APP}/Configs/Misc/{$market}/minimal_ref_{$universeId}_low_tvr_low_factor.xml"),
        new RefConfigInfo("Low Turnover / High Factor", "{$APP}/Configs/Misc/{$market}/minimal_ref_{$universeId}_low_tvr_high_factor.xml"),
        new RefConfigInfo("Very Low Turnover / Low Factor", "{$APP}/Configs/Misc/{$market}/minimal_ref_{$universeId}_very_low_tvr_low_factor.xml")
    ],

    "US": {
    }, 
    "EU": {
    },
    "JP": {
    }
}

export const DerivedUniverses = {
    "US": {
        "Top100": ["Top100_Sub", "Top100_Super"],
        "Top200": ["Top200_Sub", "Top200_Super"],
        "Top500": ["Top500_Sub", "Top500_Super"],
        "Top1000": ["Top1000_Sub", "Top1000_Super"],
        "Top1500": ["Top1500_Sub", "Top1500_Super"],
        "Top3000": ["Top3000_Sub", "Top3000_Super"],
        "Tech": [null, null],
        "SnP500": [null, null],
    }
}

export const DefMarket = "US";
export const DefUniverse = "Top200";

export function getRefConfigs(market: string, universeId: string): RefConfigInfo[] {
    Debug.assert(market, "Invalid market passed for getting ref configs!");
    Debug.assert(universeId, "Invalid universeId passed for getting ref configs!");

    let refConfigsSrc: RefConfigInfo[] = StrategyRefConfigs["Default"];

    if (StrategyRefConfigs[market] && StrategyRefConfigs[market][universeId])
        refConfigsSrc = refConfigsSrc.concat(StrategyRefConfigs[market][universeId]);

    let refConfigs: RefConfigInfo[] = new Array<RefConfigInfo>(refConfigsSrc.length);

    refConfigsSrc.map((refConfig: RefConfigInfo, i) => {
        refConfigs[i] = new RefConfigInfo(refConfig.name.slice(0), refConfig.filename.slice(0));
        refConfigs[i].filename = refConfigs[i].filename.replace("{$universeId}", universeId).replace("{$market}", market);
    });

    return refConfigs;
}

export function getDefUniverseForMarket(market: string) {
    if (market == "US")
        return "Top200";
    else if (market == "EU")
        return "Top500";
    return "Top100";
}

export function getDefUniverse(assetClass: AssetClass): string {
    if (assetClass == AssetClass.Equities)
        return "Top200";
    return "Cmdt";
}

export const AlphaOpsShift = {
    heading: "Shift",
    items: [{
        name: "Normalise", text: "Normalise [Long/Short]", defaultValue: false
    }]
};

export const Normalise_Def = {
    id: "OpNormalise",
    label: "Normalise [Long/Short]",
    opData: {}
};

export const ZScoreNormalise_Def = {
    id: "OpZScoreNormalise",
    label: "Z-Score Normalise",
    opData: {
        useMean: "false"
    }
};

export const Winsorise_Def = {
    id: "OpWinsorise",
    label: "Winsorise",
    opData: {
        shrink: 'clamp',
        std: "3.0"
    }
};

export const Neutralise_Def = {
    id: "OpNeutralise",
    label: "Neutralise",
    opData: {
        dataId: "classification.sector",
        useMean: "false",
        groupNormalise: "true"
    }
};

export const Rank_Def = {
    id: "OpRank",
    label: "Rank",
    opData: {
        shrink: "false",
        desc: "false",
        skew: "0.0",
        power: "1.0",
    }
};

export const Decay_Def = {
    id: "OpDecay",
    label: "Decay",
    opData: {
        days: "5",
        exp: "0.0"
    }
};

export const AlphaOpList = [
    Normalise_Def,
    ZScoreNormalise_Def,
    Winsorise_Def, 
    Neutralise_Def,
    Rank_Def,
    Decay_Def
];

export const MandatoryAlphaOpList = [
    Normalise_Def,
    ZScoreNormalise_Def,
    Neutralise_Def
];

export const DefaultOpObj = {
    m_name      : "Operation",
    map         : {
        id      : "OpNormalise",
        uuid    : "OpNormalise"
    },
    children    : []
};

export const EquitiesDefaultOp = new ConfigSection(DefaultOpObj.m_name, DefaultOpObj.map, DefaultOpObj.children);
export function getDefaultOp(assetClass: AssetClass): ConfigSection {
    if (assetClass == AssetClass.Equities)
        return EquitiesDefaultOp;
    return null;
}


export const AlphaOpsLUT = function() {
    let lut = {};
    AlphaOpList.map((op) => lut[op.id] = op);
    return lut;
}();

export const AlphaOpsClamp = {
    heading: "Clamp", 
    items: [{
        name: "Winsorise_Clamp", text: "Winsorise [Clamp]", defaultValue: false
    }, {
        name: "Winsorise_Median", text: "Winsorise [Median]", defaultValue: false
    }, {
        name: "Winsorise_Nan", text: "Winsorise [NaN]", defaultValue: false
    }, {
        name: "ZScore_Normalise", text: "ZScore Normalise", defaultValue: false
    }]
};

export const AlphaOpsNeuterFactors = {
    heading: "Neutralise Factors",
    items: [{
        name: "Neuter_Beta", text: "Beta", defaultValue: false
    }, {
        name: "Neuter_Value", text: "Value", defaultValue: false
    }, {
        name: "Neuter_Size", text: "Size", defaultValue: false
    }, {
        name: "Neuter_Momentum", text: "Momentum", defaultValue: false
    }]
};

export const AlphaOps = [AlphaOpsShift, AlphaOpsClamp, AlphaOpsNeuterFactors];

export const AlphaSortOrders = {
    items: [{
        name: "ir", text: "IR/Sharpe", order: "desc"
    }, {
        name: "returns", text: "Returns", order: "desc"
    }, {
        name: "marginBps", text: "Margin", order: "desc"
    }, {
        name: "volatility", text: "Volatility", order: "desc"
    }, {
        name: "hitRatio", text: "Hit Ratio", order: "asc"
    }, {
        name: "tvr", text: "Turnover", order: "asc"
    }, {
        name: "maxDD", text: "Max. Drawdown", order: "asc"
    }, {
        name: "longPnl", text: "Long P&L", order: "asc"
    }, {
        name: "shortPnl", text: "Short P&L", order: "asc"
    }]
};

export const MarketTimezones = {
    "US": "America/New_York",
    "EU": "Europe/London",
    "JP": "Asia/Tokyo"
}

export const DefaultOptimise_US = {
    "m_name": "Optimise",
    "map": {
        "id": "PyOptimiser",
        "uuid": "PyMOSEK",
        "pyPath": "optimiserMOSEK.py",
        "pyClassName": "PyMOSEK",
        
        "annualisedVol": "-1",
        "delay": "1",
        "maxPosPctBook": "0.005",
        "maxTradePctADV": "0.05",
        "maxTurnover": "0.10",
        "moduleId": "PyImpl",
        "objectiveFunction": "maxret",
        "useSqConstraint": "true",
        "sqConstraintParam": "0.65",
        "l1normMaxretParam": "1",
        "overrideMaxPosPctBook": "3.5",
        "optimise": "true",
        "preserveAlphaSign": "false",
        "smallMarketCapPctLimit": "0.2",
        "smallMarketCapSize": "1000000000",
        "smallMarketCapNetPctLimit": "0.05",
        "largeMarketCapNetPctLimit": "0.05",
        "useShortHardLiquidate": "1",
        "maxSmallMarketCapTurnover": "0.1",
        "maxSmallMarketCapMaxPosStartMult": "20",
        "maxSmallMarketCapMaxPosPctBook": "0.005",
        "maxSmallMarketCapMaxPosDollar": "250000",
        "riskModel": "axioma_mh",
        "riskModelFactors": "f_Value, f_Leverage, f_Growth, f_Size, f_ShortTermMomentum, f_MediumTermMomentum, f_Volatility, f_Liquidity, f_ExchangeRateSensitivity, f_DiversifiedFinancials, f_Insurance, f_RealEstate, f_InternetSoftware_and_Services, f_ITServices, f_Software, f_CommunicationsEquipment, f_Computers_and_Peripherals, f_ElectronicEquipment_Instruments_and_Components, f_Semiconductors_and_SemiconductorEquipment, f_TelecommunicationServices, f_Utilities, f_EnergyEquipment_and_Services, f_Oil_Gas_and_ConsumableFuels, f_MaterialsexMetals_and_Mining, f_Metals_and_MiningexGold, f_Gold, f_Aerospace_and_Defense, f_ElectricalEquipment, f_Industrial_and_Machinery, f_Commercial_and_ProfessionalServices, f_Transportation, f_Automobiles_and_Components, f_HouseholdDurables, f_Leisure_and_Apparel, f_ConsumerServices, f_Media, f_Retailing, f_Food_and_StaplesRetailing, f_Food_Beverage_and_Tobacco, f_Household_and_PersonalProducts, f_HealthCareProviders_and_Services, f_HealthCareEquipment_and_Technology, f_Pharmaceuticals, f_Biotechnology_and_LifeSciences, f_CommercialBanks, f_Thrifts_and_MortgageFinance, f_UnitedStates, f_Canada, f_USD, f_CAD, f_COP, f_JPY, f_TWD, f_MUR, f_NGN, f_EGP, f_IDR, f_BGN, f_ISK, f_PEN, f_ILS, f_GBP, f_LKR, f_DKK, f_PKR, f_LBP, f_HUF, f_VND, f_MAD, f_KWD, f_RON, f_CZK, f_NAD, f_MYR, f_ZAR, f_THB, f_UAH, f_JOD, f_GHS, f_QAR, f_SAR, f_SEK, f_SGD, f_JMD, f_TTD, f_BWP, f_AUD, f_CHF, f_KRW, f_CNY, f_TRY, f_TND, f_HRK, f_NZD, f_CLP, f_ZMW, f_EUR, f_VEF, f_ARS, f_KZT, f_NOK, f_RSD, f_RUB, f_BHD, f_INR, f_MXN, f_OMR, f_BRL, f_HKD, f_PLN, f_BDT, f_PHP, f_KES, f_AED"
    },

    "children": [
        {
            "m_name": "RiskModelFactorExposureLimits",
            "map": {
                "f_Value": "0.025" ,
                "f_Leverage": "0.025" ,
                "f_Growth": "0.025" ,
                "f_Size": "0.025" ,
                "f_Liquidity": "0.025" ,
                "f_MediumTermMomentum": "0.025" ,
                "f_ExchangeRateSensitivity": "0.025" ,
                "f_Volatility": "0.025" ,
                "r_predictedBeta": "0.025" ,
                "r_historicalBeta": "0.025" 
            }
        },
        // {
        //     "m_name": "GroupLimits",
        //     "map": {
        //         "classification.sector": "0.025"
        //     }
        // },
        {
            "m_name": "ConstraintResolution",
            "map": {},
            "children": [
                {
                    "m_name": "Relax",
                    "map": {"maxTurnover": "1.1"}
                },
                {
                    "m_name": "Relax",
                    "map": {"maxTurnover": "1.25"}
                },
                {
                    "m_name": "Relax",
                    "map": {"maxTradePctADV": "1.5"}
                },
                {
                    "m_name": "Relax",
                    "map": {"maxTradePctBook": "1.3"}
                },
                {
                    "m_name": "Relax",
                    "map": {"maxTurnover": "1.4"}
                },
                {
                    "m_name": "Relax",
                    "map": {"maxTurnover": "1.5"}
                },
                {
                    "m_name": "Relax",
                    "map": {"maxTradePctADV": "1.3"}
                },
                {
                    "m_name": "Relax",
                    "map": {"maxTradePctADV": "1.3"}
                },
                {
                    "m_name": "Relax",
                    "map": {"maxTradePctADV": "2.0"}
                },
                {
                    "m_name": "Relax",
                    "map": {"maxTradePctADV": "2.0"}
                }
            ]
        }
    ]
}

///////////////////////////////////////////////////////////////////////////////
export const DefaultOptimise_JP = {
    "m_name": "Optimise",
    "map": {
        "id": "PyOptimiser",
        "uuid": "PyMOSEK",
        "pyPath": "optimiserMOSEK.py",
        "pyClassName": "PyMOSEK",
        "annualisedVol": "-1",
        "delay": "1",
        "maxPosPctBook": "0.008",
        "maxTradePctADV": "0.05",
        "maxTurnover": "0.10",
        "moduleId": "PyImpl",
        "objectiveFunction": "maxret",
        "useSqConstraint": "true",
        "sqConstraintParam": "0.65",
        "l1normMaxretParam": "1",
        "overrideMaxPosPctBook": "3.5",
        "optimise": "true",
        "preserveAlphaSign": "false",
        "smallMarketCapPctLimit": "0.25",
        "smallMarketCapSize": "1000000000",
        "smallMarketCapNetPctLimit": "0.05",
        "largeMarketCapNetPctLimit": "0.05",
        "useShortHardLiquidate": "1",
        "maxSmallMarketCapTurnover": "0.1",
        "maxSmallMarketCapMaxPosStartMult": "20",
        "maxSmallMarketCapMaxPosPctBook": "0.008",
        "maxSmallMarketCapMaxPosDollar": "250000",
        "riskModel": "axioma_mh",
        "riskModelFactors": "f_Asian_Market, f_Value, f_Leverage, f_Growth, f_Size, f_Short-Term_Momentum, f_Medium-Term_Momentum, f_Volatility, f_Liquidity, f_Exchange_Rate_Sensitivity, f_Domestic_China, f_Energy, f_Materials, f_Capital_Goods, f_Commercial_and_Professional_Services, f_Transportation, f_Automobiles_and_Components, f_Consumer_Durables_and_Apparel, f_Consumer_Services, f_Media, f_Retailing, f_Food_and_Staples_Retailing, f_Food_Beverage_and_Tobacco, f_Household_and_Personal_Products, f_Health_Care_Equipment_and_Services, f_Pharmaceuticals_Biotechnology_and_Life_Sciences, f_Banks, f_Diversified_Financials, f_Insurance, f_Real_Estate, f_Software_and_Services, f_Technology_Hardware_and_Equipment, f_Semiconductors_and_Semiconductor_Equipment, f_Telecommunication_Services, f_Utilities, f_Taiwan, f_Vietnam, f_Australia, f_China, f_Hong_Kong, f_India, f_Indonesia, f_Korea_Republic_of, f_Malaysia, f_New_Zealand, f_Pakistan, f_Philippines, f_Singapore, f_Thailand, f_Japan, f_Sri_Lanka, f_Bangladesh, f_PKR, f_VND, f_AUD, f_TWD, f_THB, f_MYR, f_IDR, f_KRW, f_INR, f_CNY, f_JPY, f_BDT, f_NZD, f_PHP, f_LKR, f_SGD, f_HKD, f_COP, f_USD, f_NAD, f_MUR, f_NGN, f_EGP, f_BGN, f_ISK, f_PEN, f_ILS, f_GBP, f_DKK, f_CAD, f_BWP, f_LBP, f_HUF, f_ZAR, f_KWD, f_RON, f_CZK, f_GHS, f_UAH, f_JOD, f_QAR, f_SAR, f_SEK, f_JMD, f_TTD, f_CHF, f_TRY, f_TND, f_HRK, f_CLP, f_ZMW, f_EUR, f_VEF, f_ARS, f_KZT, f_NOK, f_RSD, f_RUB, f_BHD, f_MXN, f_OMR, f_BRL, f_MAD, f_PLN, f_KES, f_AED"
    },

    "children": [
        {
            "m_name": "RiskModelFactorExposureLimits",
            "map": {
                "f_Asian_Market": "0.025",
                "f_Value": "0.025",
                "f_Leverage": "0.025",
                "f_Growth": "0.025",
                "f_Size": "0.025",
                "f_Liquidity": "0.025",
                "f_Short-Term_Momentum": "0.1",
                "f_Medium-Term_Momentum": "0.025",
                "f_Exchange_Rate_Sensitivity": "0.025",
                "f_Volatility": "0.025",
                "r_predictedBeta": "0.025",
                "r_historicalBeta": "0.025"
            }
        },
        {
            "m_name": "ConstraintResolution",
            "map": {},
            "children": [
                {
                    "m_name": "Relax",
                    "map": {"maxTurnover": "1.1"}
                },
                {
                    "m_name": "Relax",
                    "map": {"maxTurnover": "1.25"}
                },
                {
                    "m_name": "Relax",
                    "map": {"maxTradePctADV": "1.5"}
                },
                {
                    "m_name": "Relax",
                    "map": {"maxTradePctBook": "1.3"}
                },
                {
                    "m_name": "Relax",
                    "map": {"maxTurnover": "1.4"}
                },
                {
                    "m_name": "Relax",
                    "map": {"maxTurnover": "1.5"}
                },
                {
                    "m_name": "Relax",
                    "map": {"maxTradePctADV": "1.3"}
                },
                {
                    "m_name": "Relax",
                    "map": {"maxTradePctADV": "1.3"}
                },
                {
                    "m_name": "Relax",
                    "map": {"maxTradePctADV": "2.0"}
                },
                {
                    "m_name": "Relax",
                    "map": {"maxTradePctADV": "2.0"}
                }
            ]
        }
    ]
}

export let DefaultOptimise: any = {
    "US": DefaultOptimise_US,
    "JP": DefaultOptimise_JP
}
