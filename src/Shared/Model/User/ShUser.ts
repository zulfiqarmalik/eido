import { eido } from 'Shared/Core/ShCore';
import { ObjectID } from "Shared/Core/ObjectID";
import { Model } from 'Shared/Model/ShModel';
import { SecrecyLevel } from 'Shared/Model/SecrecyLevel';
import { ModelObject, ModelValue, ModelClassId } from 'Shared/Core/BaseObject';
import { UserPreferences } from './ShUserPreferences';
import { CObject } from 'Shared/Core/CObject';

export const enum AccountType {
    eido = 'eido',
    facebook = 'facebook',
    gplus = 'gplus',
    twitter = 'twitter',
};

export const enum AccountStatus {
    auto = 'auto',       	// normal status of the account
    normal = 'normal',     	// normal status of the account
    banned = 'banned',     	// account has been banned
    suspended = 'suspended',// account has been suspended
    locked = 'locked',     	// account has been locked
};
    
export const enum Gender {
    male = 'male',
    female = 'female',
};
    
export const enum UserLevel {
    superUser = 0,          // The super user
    admin = 1,              // One of the admins
    cio = 1,                // Whether it's a CIO or not
    portfolioManager = 2,   // Account is a portfolio manager
    normal = 5,             // Normal account level
};

@ModelObject()
export class ShUser extends CObject {
    @ModelClassId(ShUser, "ShUser") __dummy: string;

    @ModelValue("The users unique identifier.", SecrecyLevel.minimal)
    _id: ObjectID = ObjectID.create();
    
    @ModelValue("The user email address.", SecrecyLevel.private)
    email: string = "";
    
    @ModelValue("The level of this account.", SecrecyLevel.public)
    level: UserLevel = UserLevel.normal;
    
    @ModelValue("The JWT access token returned by the server.", SecrecyLevel.private)
    accessToken: string = "";
    
    @ModelValue("Other emails linked with this account. These could have come from various third party oauth logins.", SecrecyLevel.private)
    linkedEmails: string[] = [];
    
    @ModelValue("The name for the user.", SecrecyLevel.minimal)
    name: string = "";
    
    @ModelValue("The country code for the user.", SecrecyLevel.private)
    countryCode: string = "";
    
    @ModelValue("The status of the account.", SecrecyLevel.embedded)
    accountStatus: AccountStatus = AccountStatus.normal;
    
    @ModelValue("The user is currently online (application is running and actively connected).", SecrecyLevel.public)
    isOnline: boolean = false;
    
    @ModelValue("Date of birth.", SecrecyLevel.embedded)
    dateOfBirth: Date = new Date();
    
    @ModelValue('The gender of the user.', SecrecyLevel.embedded)
    gender: Gender = Gender.male;
    
    @ModelValue("The date this account was created.", SecrecyLevel.public)
    creationDate: Date = new Date();
    
    @ModelValue("The date this user was last seen.", SecrecyLevel.public)
    lastSeen: Date = new Date();
    
    @ModelValue('Email address verification code.', SecrecyLevel.private)
    verificationCode: string = "";
    
    @ModelValue('Password reset code.', SecrecyLevel.private)
    passwordResetCode: string = "";
    
    @ModelValue('Password reset code expiry date.', SecrecyLevel.private)
    passwordResetExpiryDate: Date = new Date();
    
    // @ModelValue("User preferences.", SecrecyLevel.private)
    // prefs: UserPreferences = new UserPreferences();
            
    printId() {
        return `${this.email} (${this.name})`;
    }

    constructor() {
        super();
    }
    
    age() {
        return 23;
    }
    
    getName(): string {
        return this.name ? this.name : this.email.split('@')[0];
    }

    initCreate(args: any): any {
        this._id            = ObjectID.create();
        this.email          = args.email;
        this.name           = args.name;
        this.accountStatus  = AccountStatus.normal;
        this.dateOfBirth    = args.dateOfBirth;
        this.creationDate   = new Date();
        this.lastSeen       = new Date();
        this.gender         = args.gender || Gender.male;

        return this;
    }

    hasLevel(level: UserLevel): boolean {
        return this.level <= level ? true : false;
    }

    isPortfolioManagerOrBetter(): boolean { 
        return this.hasLevel(UserLevel.portfolioManager);
    }

    isAdminOrBetter(): boolean {
        return this.hasLevel(UserLevel.admin);
    }

    isSuperUser(): boolean {
        return this.hasLevel(UserLevel.superUser);
    }
    
    isCIO(): boolean {
        return this.hasLevel(UserLevel.cio);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /// Static methods
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
}

