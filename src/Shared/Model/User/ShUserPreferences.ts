import { eido } from 'Shared/Core/ShCore';
import { ObjectID } from "Shared/Core/ObjectID";
import { Model } from 'Shared/Model/ShModel';
import { ModelObject, ModelValue, ModelClassId } from 'Shared/Core/BaseObject';
import { SecrecyLevel } from 'Shared/Model/SecrecyLevel';
import { CObject } from 'Shared/Core/CObject';

@ModelObject()
export class UserPreferences extends CObject {
    @ModelClassId(UserPreferences, "UserPreferences") __dummy: string;

    @ModelValue("The users unique identifier.", SecrecyLevel.minimal)
    _id: ObjectID = ObjectID.create();

    constructor() {
        super();
    }
}
