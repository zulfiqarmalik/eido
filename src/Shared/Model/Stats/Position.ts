import { eido } from 'Shared/Core/ShCore';
import { SecrecyLevel } from 'Shared/Model/SecrecyLevel';
import { Debug } from 'Shared/Core/ShUtil';
import { ModelObject, ModelValue, ModelClassId } from 'Shared/Core/BaseObject';
import { CObject } from 'Shared/Core/CObject';

////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// Information coefficient - The information coefficient for a certain stock
////////////////////////////////////////////////////////////////////////////////////////////////////////////////
@ModelObject()
export class ICPoint extends CObject {
    @ModelClassId(ICPoint, "ICPoint") __dummy: string;

    @ModelValue("IC Value.")
    value: number = 0.0;
    
    @ModelValue("IC stddev.")
    stddev: number = 0.0;

    _points: number[] = [];
    
    constructor() {
        super();
    }

    add(pt: any) {
        if (pt && pt.value != 0.0) {
            this.value += pt.value;
            if (!this._points.length) 
                this._points.push(this.value);
            this._points.push(pt.value);
        }
    }

    average() {
        let count = this._points.length;

        if (!count || count == 1) {
            this.stddev = 0.0;
            return;
        }

        let mean = this.value / count;
        let variance = 0.0;

        this._points.map((point) => {
            let distance = (point - mean);
            variance += (distance * distance);
        });

        variance /= count;
        this.value = mean;
        this.stddev = Math.sqrt(variance);
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// Information coefficient - The information coefficient for everything
////////////////////////////////////////////////////////////////////////////////////////////////////////////////
@ModelObject()
export class InformationCoefficient extends CObject {
    @ModelClassId(InformationCoefficient, "InformationCoefficient") __dummy: string;

    @ModelValue("21 day ICS.")
    points: Array<ICPoint> = new Array<ICPoint>();

    static get maxDays(): number { return 21; }

    constructor() {
        super();
    }

    init() {
        if (this.points && this.points.length)
            return;

        this.points = new Array<ICPoint>(InformationCoefficient.maxDays);
        for (let i = 0; i < this.points.length; i++)
            this.points[i] = new ICPoint();
    }

    add(rhs: InformationCoefficient) {
        this.init();

        rhs.points.map((point, index) => {
            if (point)
                this.points[index].add(point);
        });

        return this;
    }

    average() {
        this.points.map((point) => {
            point.average();
        });
    }

    calculate(positions, returns, index, correlationFunction) {
        this.init();
        
        this.points.map((point, pindex) => { 
            point.value = InformationCoefficient.calculateCorrelationAt(positions, returns, index + pindex + 1, correlationFunction);
        });
    }

    static calculateCorrelationAt(positions, returns, index, correlationFunction) {
        if (index >= returns.length)
            return 0.0;
            
        return returns.length > index ? correlationFunction(positions._notionalRank, returns[index].rank) : 0.0;
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// EarningsInfo - MiniPnl to track very long term pnl information!
////////////////////////////////////////////////////////////////////////////////////////////////////////////////
@ModelObject()
export class EarningsInfo extends CObject {
    @ModelClassId(EarningsInfo, "EarningsInfo") __dummy: string;

    @ModelValue("The array of earnings dates.")
    dates: number[] = [];

    constructor() {
        super();
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// MiniPnl - MiniPnl to track very long term pnl information!
////////////////////////////////////////////////////////////////////////////////////////////////////////////////
@ModelObject()
export class MiniPnl extends CObject {
    @ModelClassId(MiniPnl, "MiniPnl") __dummy: string;
    
    @ModelValue("The array of pnls.")
    pnls: number[] = [];

    @ModelValue("The array of pnls.")
    notionals: number[] = [];

    constructor(length: number = 0) {
        super();

        if (length && length > 0) {
            this.pnls = (new Array(length)).fill(0.0);
            this.notionals = (new Array(length)).fill(0.0);
        }
    }

    push(ppnl: number, notional: number) {
        if (isNaN(ppnl))
            ppnl = 0.0;

        if (typeof notional === 'undefined' || isNaN(notional))
            notional = 0.0;

        this.pnls.push(ppnl);
        this.notionals.push(notional);
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// PositionPerf - The performance info of a position!
////////////////////////////////////////////////////////////////////////////////////////////////////////////////
@ModelObject()
export class PositionPerf extends CObject {
    @ModelClassId(PositionPerf, "PositionPerf") __dummy: string;

    @ModelValue("The index of the position.")
    index: number = 0;

    @ModelValue("The performance value.")
    value: number = 0.0;

    constructor(index: number = -1, value: number = 0.0) {
        super();
        
        this.index = index;
        this.value = value;
    }

    net(count: number) {
        this.value  /= count;
    }

    add(rhsPerf: PositionPerf) {
        this.value  += rhsPerf.value;
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /// Static methods
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    static sort(perfs: PositionPerf[]) {
        perfs.sort((a, b) => {
            return a.value - b.value;
        });
    }

    static rsort(perfs: PositionPerf[]) {
        perfs.sort((a, b) => {
            return b.value - a.value;
        });
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// PositionPnl - The pnl information of a position!
////////////////////////////////////////////////////////////////////////////////////////////////////////////////
@ModelObject()
export class PositionPnl extends CObject {
    @ModelClassId(PositionPnl, "PositionPnl") __dummy: string;

    @ModelValue("The CFIGI.")
    cfigi: string = "";
    
    @ModelValue("The cost of holding this position.")
    cost: number = 0.0;
    
    @ModelValue("The closing pnl of this position.")
    pnlClose: number = 0.0;
    
    @ModelValue("The next day return for this position.")
    nextDayReturn: number = 0.0;
    
    // @ModelValue("The opening pnl.")
    // pnlOpen: number = 0.0;
    
    // @ModelValue("Intraday PNL high.")
    // pnlHigh: number = 0.0;
    
    // @ModelValue("Intraday PNL low.")
    // pnlLow: number = 0.0;
    
    @ModelValue("The turnover for this stock")
    tvr: number = 0.0;

    notional: number = 0.0;
    
    constructor() {
        super();
    }

    net(count: number): PositionPnl {
        this.notional   /= count;
        this.pnlClose   /= count;
        this.cost       /= count;
        this.tvr        /= count;
        // this.pnlOpen    /= count;
        // this.pnlHigh    /= count;
        // this.pnlLow     /= count;
        this.nextDayReturn/= count;

        return this;
    }

    add(rhsPnl: PositionPnl): PositionPnl {
        this.notional   += rhsPnl.notional;
        this.pnlClose   += rhsPnl.pnlClose;
        this.cost       += rhsPnl.cost;
        this.tvr        += rhsPnl.tvr;
        // this.pnlOpen    += rhsPnl.pnlOpen;
        // this.pnlHigh    += rhsPnl.pnlHigh;
        // this.pnlLow     += rhsPnl.pnlLow;
        this.nextDayReturn += rhsPnl.nextDayReturn;

        return this;
    }


    loadObjectPSS(data: any, ii: number): PositionPnl {
        this.notional   = data.pssNotionals ? data.pssNotionals[ii] : 0.0;
        this.pnlClose   = data.pssClosePnls ? data.pssClosePnls[ii] : 0.0;
        this.tvr        = data.pssTurnovers ? data.pssTurnovers[ii] : 0.0;
        this.cost       = data.pssCosts ? data.pssCosts[ii] : 0.0;
        // this.pnlOpen    = 0.0;
        // this.pnlHigh    = 0.0;
        // this.pnlLow     = 0.0;

        if (isNaN(this.notional))
            this.notional = 0.0;

        return this;
    }

    loadString(str) {
        if (!str.length)
            return false;

        let parts = str.split(',');
        if (!parts.length || parts.length != 10)
            return false;

        let bbTicker    = parts[0].trim(); 
        this.cfigi      = parts[1].trim();
        this.notional   = parseFloat(parts[2].trim());
        this.pnlClose   = parseFloat(parts[3].trim());
        this.cost       = parseFloat(parts[4].trim());
        this.tvr        = parseFloat(parts[5].trim());
        // this.pnlOpen    = parseFloat(parts[7].trim());
        // this.pnlHigh    = parseFloat(parts[8].trim());
        // this.pnlLow     = parseFloat(parts[9].trim());

        if (isNaN(this.notional))
            this.notional = 0.0;

        return true;
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// Position - Single position for a single stock on a particular day
////////////////////////////////////////////////////////////////////////////////////////////////////////////////
@ModelObject()
export class Position extends CObject {
    @ModelClassId(Position, "Position") __dummy: string;
    
    @ModelValue("The CFIGI.")
    cfigi: string = "";
    
    // @ModelValue("The FIGI.")
    // figi: string = "";
    
    @ModelValue("The BB ticker.")
    bbTicker: string = "";
    
    // @ModelValue("The trade ticker.")
    // tradeTicker: string = "";
    
    @ModelValue("The number of shares.")
    numShares: number = 0;
    
    @ModelValue("The dollar notional.")
    notional: number = 0.0;
    
    @ModelValue("The price of the stock.")
    price: number = 0.0;
    
    // @ModelValue("The pre-opt notional.")
    // preOptNotional: number = 0.0;
    
    @ModelValue("The pnl info for this position. This is only calculated if you get the detailed positions.")
    pnl: PositionPnl = new PositionPnl();

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /// Not part of the Model
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    _returns: number = 0.0;

    constructor() {
        super();
    }

    net(count: number): Position {
        this.notional               /= count;
        this.numShares              = Math.round(this.numShares / count);
        this.price                  /= count;
        // this.preOptNotional         /= count;

        return this;
    }

    add(rhsPos: Position): Position {
        /// Add the PNLs together
        if (this.pnl && rhsPos.pnl)
            this.pnl.add(rhsPos.pnl);

        this.notional               += rhsPos.notional;
        this.numShares              += rhsPos.numShares;
        this.price                  += rhsPos.price;
        // this.preOptNotional         += rhsPos.preOptNotional;

        return this;
    }

    loadObject(data: any, ii: number): Position {
        this.bbTicker               = data.info.tickers[ii];
        this.cfigi                  = data.info.cfigis[ii];
        this.numShares              = data.shares[ii];
        this.notional               = data.notionals[ii];
        this.price                  = data.prices[ii];
        // this.figi                   = data.info.cfigis[ii];
        // this.tradeTicker            = data.info.tickers[ii];

        if (isNaN(this.notional))
            this.notional           = 0.0;

        if (isNaN(this.numShares))
            this.numShares          = 0;

        return this;
    }

    loadString(str: string): boolean {
        if (!str.length)
            return false;

        let parts = str.split('|');
        if (!parts.length)
            return false;

        this.bbTicker               = parts[0].trim();
        this.cfigi                  = parts[1].trim();
        this.numShares              = parseInt(parts[2].trim());
        this.notional               = parseFloat(parts[3].trim());
        this.price                  = parseFloat(parts[4].trim());
        // this.figi                   = parts[5].trim();
        // this.tradeTicker            = parts[6].trim();
        // this.preOptNotional         = parseFloat(parts[7].trim());

        if (isNaN(this.notional))
            this.notional           = 0.0;

        // if (isNaN(this.preOptNotional))
        //     this.preOptNotional     = 0.0;

        if (isNaN(this.numShares))
            this.numShares          = 0;

        return true;
    }
    
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /// Static methods
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
}
