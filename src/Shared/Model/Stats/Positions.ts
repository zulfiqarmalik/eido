import { eido } from 'Shared/Core/ShCore';
import { Model } from 'Shared/Model/ShModel';
import { SecrecyLevel } from 'Shared/Model/SecrecyLevel';
import { ShUtil, Debug } from 'Shared/Core/ShUtil';
import { ICPoint, InformationCoefficient, EarningsInfo, MiniPnl, PositionPerf, PositionPnl, Position } from './Position';
import { Quantile } from './Quantile';
import { ModelObject, ModelValue, ModelClassId } from 'Shared/Core/BaseObject';
import { CObject } from 'Shared/Core/CObject';

import * as path from 'path';

export const enum MarketCapType {
    unknown         = 'Unknown',
    small           = 'Small',
    mid             = 'Mid',
    large           = 'Large'
}

class PositionValues {
    values: string[] = [];
    indexes: object = {};
}

class ReturnsInfo {
    sum: number = 0.0;
    array: number[] = [];

    constructor(array: number[] = [], sum: number = 0.0) {
        this.array = array;
        this.sum = sum;
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// Positions - All the positions for a particular day
////////////////////////////////////////////////////////////////////////////////////////////////////////////////
@ModelObject()
export class Positions extends CObject {
    @ModelClassId(Positions, "Positions") __dummy: string;
    
    @ModelValue("The universeId.")
    universeId: string = "";
    
    @ModelValue("The book size.")
    bookSize: number = 0.0;
    
    @ModelValue("The timestamp when this was generated.")
    genTimestamp: string = "";
    
    @ModelValue("The version of PSIM used to generate this position.")
    version: string = "";
    
    @ModelValue("The portfolio section.")
    positions: Position[] = [];
    
    @ModelValue("The countries for each of the CFIGIs.")
    countries: number[] = [];
    
    @ModelValue("The sectors for each of the CFIGIs.")
    sectors: number[] = [];
    
    @ModelValue("The market cap types (small, medium and large) for each of the CFIGIs.")
    mcapTypes: number[] = [];
    
    @ModelValue("The industry for each of the CFIGIs.")
    industries: number[] = [];
    
    @ModelValue("The overall information coefficient of this bunch of positions.")
    ic: InformationCoefficient = null;
    
    @ModelValue("The closing prices for each of the FIGIs for the day of these positions.")
    closePrices: number[] = [];
    
    @ModelValue("The opening prices for each of the FIGIs for the day of these positions.")
    openPrices: number[] = [];
    
    @ModelValue("The VWAPs for each of the FIGI for the day of these positions.")
    vwaps: number[] = [];
    
    @ModelValue("The market caps for each of the FIGIs.")
    mcaps: number[] = [];
    
    @ModelValue("The earnings information for each of the FIGIs.")
    earnings: number[] = [];
    
    @ModelValue("The integer date.")
    date: number = 0;
    
    @ModelValue("The market that this belongs to.")
    market: string = "";
    
    @ModelValue("Pnl information divided into quantiles.")
    quantiles: Quantile[] = [];
    
    @ModelValue("Pnl performance index. Sorted from worse to best pnl.")
    pnlPerf: PositionPerf[] = [];
    
    @ModelValue("The starting date of the positions. This is valid only in the case when multiple positions are net together.")
    startDate: number = 0;
    
    @ModelValue("The ending date of the positions. This is valid only in the case when multiple positions are net together.")
    endDate: number = 0;
    
    @ModelValue("Mini PnL streams.")
    posPnls: MiniPnl[] = [];
    
    @ModelValue("The list of source dates.")
    dates: number[] = [];

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /// Not part of the Model
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    _returnsCalculated: boolean         = false;        /// Whether returns have been calculated or not
    _returns: number[]                  = null;         /// What are the values of those returns
    _returnsInfo: Array<ReturnsInfo>    = null;         /// What are the values of those returns
    _nextDayReturns: number[]           = null;         /// What are the next day returns (looks into the future)
    _notionalRank: number[]             = null;         /// What is the rank of the notionals
    _cfigis: PositionValues             = null;         /// The CFIGIs in this set of positions
    _figis: PositionValues              = null;         /// The FIGIs in this set of positions
    _posCounts: number[]                = null;         /// The position counts
    _count: number                      = 0;            /// The overall count
    _sortedPositions: Position[]        = null;         /// Sorted positions (absolute)
    _signedSortedPositions: Position[]  = null;         /// Sorted positions (signed)
    _positionsMap: object               = null;         /// Positions map
    _positionsLUT: object               = null;         /// Positions look up table
    _cumReturns: Array<ReturnsInfo>     = null;         /// The cumulative returns
    _averageReturns: Array<any>         = null;         /// The average returns
        
    constructor() {
        super();
    } 

    getDateLookup(): object {
        let dateLookup = {};
        this.dates.map((date, index) => dateLookup[date] = index);
        return dateLookup;
    }

    getStartDate(): number {
        return this.dates && this.dates.length ? this.dates[0] : this.date;
    }

    getEndDate(): number {
        return this.dates && this.dates.length ? this.dates[this.dates.length - 1] : this.date;
    }

    getPnlOnDate(posIndex: number, pnlIndex: number): any {
        if (!this.posPnls || posIndex > this.posPnls.length) {
            return {
                pnl: 0.0,
                notional: 0.0
            };
        }

        let posPnl = this.posPnls[posIndex];

        if (posPnl.pnls.length && pnlIndex < posPnl.pnls.length && posPnl.pnls.length == this.dates.length) {
            return {
                pnl: posPnl.pnls[pnlIndex],
                notional: posPnl.notionals[pnlIndex]
            }
        }

        return {
            pnl: 0.0,
            notional: 0.0
        };
    }

    add(rhsPositions: Positions): Positions {
        let positionsMap = this.getPositionsMap();

        if (!this._posCounts) {
            this._count = 1;
            this._posCounts = (new Array(this.positions.length)).fill(1);
            this.dates = [this.date];

            if (this.hasPnls()) {
                this.posPnls = new Array(this.positions.length);

                this.positions.map((position, index) => {
                    this.posPnls[index] = new MiniPnl(1);
                    this.posPnls[index].pnls[0] = position.pnl.pnlClose;
                    this.posPnls[index].notionals[0] = position.notional;
                });
            }
            else 
                this.posPnls = null;
        }

        this._count += 1;

        /// First of all we net all the positions together!
        rhsPositions.positions.map((rhsPosition, index) => {
            let myIndex = this._positionsLUT[rhsPosition.cfigi];

            /// If a position already exists, then we net the positions together!
            if (positionsMap[rhsPosition.cfigi]) {
                positionsMap[rhsPosition.cfigi].add(rhsPosition);
                this._posCounts[myIndex] += 1;

                if (this.posPnls)
                    this.posPnls[myIndex].push(rhsPosition.pnl.pnlClose, rhsPosition.notional);
            }
            else {
                /// Otherwise we just add the position
                if (this.posPnls) {
                    this.posPnls.push(new MiniPnl(this.dates.length - 1));
                    this.posPnls[this.posPnls.length - 1].push(rhsPosition.pnl.pnlClose, rhsPosition.notional);
                }

                this.positions.push(rhsPosition);
                this._posCounts.push(1);
                positionsMap[rhsPosition.cfigi] = rhsPosition;
                this._positionsLUT[rhsPosition.cfigi] = this.positions.length - 1;
            }
        });

        /// Now we add the main object!
        let quantiles = this.calculateQuantiles();
        let rhsQuantiles = rhsPositions.calculateQuantiles();

        if (quantiles && rhsQuantiles) {
            quantiles.map((quantile, index) => {
                let rhsQuantile = rhsQuantiles[index];
                quantile.add(rhsQuantile);
            });
        }

        if (this.ic && rhsPositions.ic)
            this.ic.add(rhsPositions.ic);

        if (rhsPositions.date < this.startDate)
            this.startDate = rhsPositions.date;
        else if (rhsPositions.date > this.endDate)
            this.endDate = rhsPositions.date;

        this.dates.push(rhsPositions.date);

        this.bookSize += rhsPositions.bookSize;

        return this;
    }

    hasPnls(): boolean {
        return this.positions.length && this.positions[0].pnl ? true : false;
    }

    calculatePerfs(): void {
        this.pnlPerf = null;
        return;
        // if (this.pnlPerf && this.pnlPerf.length == this.positions.length)
        //     return; 

        // /// If there are no positions, or pnl has not been calculated then we don't do any of this!
        // if (!this.hasPnls())
        //     return;

        // /// Pnl performance index is initialised ...
        // this.pnlPerf = new Array(this.positions.length);
        // this.positions.map((position, index) => {
        //     this.pnlPerf[index] = new PositionPerf(index, position.pnl.pnlClose);
        // });

        // PositionPerf.sort(this.pnlPerf);
    }

    finaliseNet(): Positions {
        this.bookSize /= this._count;

        this.calculatePerfs();

        if (this.quantiles) {
            this.quantiles.map((quantile, index) => {
                quantile.net(this._count);
            });
        }

        if (this._posCounts && this._posCounts.length) {
            this.positions.map((position, index) => {
                let count = this._posCounts[index];
                position.net(count);
                position.numShares = Math.round(position.notional / position.price);
            });
        }

        if (this.ic) 
            this.ic.average();

        return this;
    }

    getPositionsLUT(): object {
        if (this._positionsLUT)
            return this._positionsLUT;

        this.getPositionsMap();
        return this._positionsLUT;
    }

    getPositionsMap(): object {
        if (this._positionsMap)
            return this._positionsMap;

        this._positionsMap = {};
        this._positionsLUT = {};
        this.positions.map((position, index) => {
            this._positionsMap[position.cfigi] = position;
            this._positionsLUT[position.cfigi] = index;
        });

        return this._positionsMap;
    }

    getPositionsValues(key: string): PositionValues {
        let pv: PositionValues = new PositionValues();
        pv.values = new Array<string>(this.positions.length);

        this.positions.map((position, index) => {
            pv.values[index] = position[key];
            pv.indexes[position[key]] = index;
        });


        return pv;
    }

    getCFigis() { 
        if (this._cfigis && this._cfigis.values.length === this.positions.length)
            return this._cfigis;

        this._cfigis = this.getPositionsValues("cfigi");
        return this._cfigis;
    }

    getFigis()  { 
        if (this._figis && this._figis.values.length === this.positions.length)
            return this._figis;

        this._figis = this.getPositionsValues("figi");
        return this._figis;
    }

    sortPositions(): Position[] {
        if (this._sortedPositions)
            return this._sortedPositions;

        this._sortedPositions = this.positions.slice();
        this._sortedPositions.sort((a, b) => {
            if (a.notional < 0 && b.notional < 0)
                return Math.abs(a.notional) - Math.abs(b.notional);
            else if (a.notional > 0 && b.notional > 0)
                return b.notional - a.notional;
            return a.notional - b.notional;
        });

        return this._sortedPositions;
    }

    remapFigis(figiInfo) {
        let posMap = this.getPositionsMap();
        let posLUT = this.getPositionsLUT();

        // this._positionsMap = {};
        // this._positionsLUT = {};
        // this.positions.map((position, index) => {
        //     this._positionsMap[position.cfigi] = position;
        //     this._positionsLUT[position.cfigi] = index;
        // });

        /// We'll need to recreate these at the end se we might as well do this as we're iterating!
        this._positionsMap = {};
        this._positionsLUT = {};

        let desiredLength = figiInfo.figis.length;
        this.positions = new Array(desiredLength);

        figiInfo.figis.map((figi, index) => {
            let existingPosition = posMap[figi];
            if (existingPosition) {
                this.positions[index] = existingPosition;
            }
            else {
                let newPosition = new Position();
                newPosition.cfigi = figi;
                newPosition.pnl = new PositionPnl();
                this.positions[index] = newPosition;
            }

            let position = this.positions[index];
            this._positionsMap[position.cfigi] = position;
            this._positionsLUT[position.cfigi] = index;
        });

        this._cfigis = figiInfo.figis;
    }

    calculateReturns(prevPositions) {
        Debug.assert(prevPositions, "Must supply valid previous positions!");
        this._positionsMap = {};
        this._positionsLUT = {};
        let prevPositionsMap = prevPositions.getPositionsMap();
        this._returns = new Array<number>(this.positions.length).fill(0.0);

        this.positions.map((position, index) => {
            position._returns = 0.0;
            this._positionsMap[position.cfigi] = position;
            this._positionsLUT[position.cfigi] = index;

            if (prevPositionsMap[position.cfigi]) {
                let notional = prevPositionsMap[position.cfigi].notional;
                let pnl = prevPositionsMap[position.cfigi].pnl.pnlClose;
                position._returns = pnl / Math.abs(notional);
            }

            this._returns[index] = position._returns;
        });

        /// Calculate the returns rank
        // this._returnsRank.sort((a, b) => {
        //     return this.positions[a]._returns - this.positions[b]._returns;
        // });
    }

    rankNotionals() {
        if (this._notionalRank)
            return this._notionalRank;

        console.trace("[Pos] Ranking notionals: %d [%d]", this.positions.length, this.date);
        let positions = this.positions.map((n) => n.notional);
        this._notionalRank = ShUtil.rank(positions);

        return this._notionalRank;
    }

    getNextDayReturns() {
        // if (this._nextDayReturns)
        //     return this._nextDayReturns;

        let cfigis = this.getCFigis().values;
        let market = eido.dataCache.getMarket(this.market);
        let returnsData = market.getFloatData("baseData.returns", this.universeId);

        Debug.assert(cfigis.length == this.positions.length, `[Pos] CFIGIs array does not match with positions. Expecting: ${this.positions.length} - Found: ${cfigis.length}`);

        console.trace("[Pos] Calculating returns for: %d [Num FIGIS: %d]", this.date, cfigis.length);

        let di0 = returnsData.getDateIndex(this.date, false);

        if (di0 >= 0)
            this._nextDayReturns = returnsData.getDataForFigisAtIndex(di0 + 1, cfigis)
        else
            this._nextDayReturns = (new Array(cfigis.length).fill(0.0));

        if (!this._nextDayReturns || this._nextDayReturns.length != cfigis.length) {
            console.log("[Pos] Invalid next day returns for date: %d", this.date);
            this._nextDayReturns = (new Array(cfigis.length)).fill(0.0);
        }

        return this._nextDayReturns;
    }

    getPositionsReturns() {
        if (this._returnsCalculated)
            return; 

        let nextDayReturns = this.getNextDayReturns();
        if (!nextDayReturns) {
            this._returnsCalculated = true;
            return;
        }

        Debug.assert(nextDayReturns.length === this.positions.length, 
            `[Pos] Invalid returns array. Expecting size: ${this.positions.length} - Found size: ${nextDayReturns.length} - Date: ${this.date}`);

        this.positions.map((pos, index) => pos.pnl.nextDayReturn = nextDayReturns[index]);
        this._returnsCalculated = true;
    }

    getReturns(maxDays: number = 22): Array<ReturnsInfo> {
        let cfigis = this.getCFigis().values;
        let market = eido.dataCache.getMarket(this.market);
        let returnsData: any = market.getFloatData("baseData.returns", this.universeId);

        if (!this._returnsInfo)
            this._returnsInfo = new Array<ReturnsInfo>();

        console.trace("[Pos] Calculating returns for: %d [Num FIGIS: %d]", this.date, this.positions.length);

        let di0 = returnsData.getDateIndex(this.date, false);

        if (di0 < 0) {
            this._returnsInfo = [
                new ReturnsInfo((new Array<number>(cfigis.length)).fill(0.0))
            ];

            return this._returnsInfo;
        }

        this._returns = [];

        // this._returns = returnsData.getDataForFigis(this.date, cfigis);
        this._returnsInfo.push(new ReturnsInfo(returnsData.getDataForFigis(this.date, cfigis)));

        let diLast = returnsData.getLastDateIndex();

        for (let i = 1; i < maxDays && ((di0 + i) <= diLast); i++) {
            this._returnsInfo.push(new ReturnsInfo(returnsData.getDataForFigisAtIndex(di0 + i, cfigis)));
        }

        return this._returnsInfo;
    }

    calcAverageReturns(returns) {
        if (!returns)
            returns = this._returns;

        if (!returns || !returns.length) 
            return;

        console.trace("[Pos] Averaging returns for: %d [Num FIGIS: %d - Num Ret: %d]", this.date, this.positions.length, returns.length);

        this._averageReturns = new Array();
        let retCopy = returns[0].array.slice(0);
        this._averageReturns.push({
            sum: retCopy,
            array: retCopy
        });

        for (let i = 1; i < returns.length; i++) {
            let averageReturns = {
                sum: returns[i - 1].array.slice(0),
                array: (new Array(returns[i - 1].length)).fill(0.0)
            };

            returns[i].array.map((ret, retIndex) => {
                averageReturns.sum[retIndex] += ret;
                averageReturns.array[retIndex] = averageReturns.sum[retIndex] / (i + 1);
            });

            this._averageReturns.push(averageReturns);
        }

        return this._averageReturns;
    }

    accumulateReturns(returns) {
        if (!returns)
            returns = this._returns;

        if (!returns || !returns.length) 
            return;

        console.trace("[Pos] Averaging returns for: %d [Num FIGIS: %d - Num Ret: %d]", this.date, this.positions.length, returns.length);

        this._cumReturns = new Array();
        let retCopy = returns[0].array.slice(0);
        this._cumReturns.push({
            sum: retCopy,
            array: retCopy
        });

        for (let i = 1; i < returns.length; i++) {
            let cumReturns: ReturnsInfo = new ReturnsInfo(returns[i - 1].array.slice(0));

            returns[i].array.map((ret, retIndex) => {
                cumReturns.array[retIndex] += ret;
            });

            this._cumReturns.push(cumReturns);
        }

        return this._cumReturns;
    }

    calculateInformationCoefficient(correlationFunction) {
        // console.log("[Pos] Calculating IC: %d", this.date);

        this.rankNotionals();
        let returns = this.getReturns();
        let cumReturns = this.accumulateReturns(returns);

        Positions.rankReturns(cumReturns);

        this.ic = new InformationCoefficient();
        this.ic.calculate(this, cumReturns, 0, correlationFunction);

        return this.ic;
    }
    

    signedSortPositions() {
        if (this._signedSortedPositions)
            return this._signedSortedPositions;

        this._signedSortedPositions = this.positions.slice();
        this._signedSortedPositions.sort((a, b) => {
            return b.notional - a.notional;
        });

        return this._signedSortedPositions;
    }

    loadObject(dataArray) {
        dataArray.map((data, di) => {
            let dataInfo        = data.info;

            this.market         = dataInfo.market;
            this.universeId     = dataInfo.universeId;
            this.bookSize       = dataInfo.bookSize;
            this.genTimestamp   = dataInfo.genTimestamp;
            this.version        = dataInfo.version;

            this.date           = data.date;
            this.startDate      = this.date;
            this.endDate        = this.date;

            this.positions      = new Array(dataInfo.tickers.length);

            for (let ii = 0; ii < dataInfo.tickers.length; ii++) {
                this.positions[ii] = (new Position()).loadObject(data, ii);
            }
        });
        
    }

    loadString(str) {
        let lines = str.split(/\n/);
        this.positions = [];

        lines.map((line, li) => {
            line = line.trim();

            if (line[0] != '#') {
                let position = new Position();
                if (position.loadString(line)) {
                    this.positions.push(position);

                    if (!this.market) {
                        if (position.bbTicker.indexOf(" US") > 0) 
                            this.market = "US";
                        else if (position.bbTicker.indexOf(" LN") > 0)
                            this.market = "EU";
                    }
                }
            }
            else {
                let parts = line.substring(2).split(':');
                if (parts.length < 2)
                    return;
                // Debug.assert(parts.length >= 2, "Invalid comment in position file: ", line, " - Parts: ", parts);

                let lhs = parts[0].trim();
                
                if (lhs == 'Universe')
                    this.universeId = parts[1].trim();
                else if (lhs == 'BookSize')
                    this.bookSize = parseFloat(parts[1].trim());
                else if (lhs == 'Gen. Timestamp') {
                    parts.shift();
                    this.genTimestamp = (parts.join(':')).trim();
                }
                else if (lhs == 'Date') {
                    this.date = parseInt(parts[1].trim());
                    this.startDate = this.date;
                    this.endDate = this.date;
                }
                else if (lhs == 'Market')
                    this.market = parts[1].trim();
                else if (lhs == 'Version')
                    this.version = parts[1].trim();
            }
        });

        if (!this.market)
            this.market = "JP";

        // this.getPositionsReturns();

        return this;
    }

    loadObjectPSS(data) {
        let positionsPnls = [];
        let positionsMap = this.getPositionsMap();
        let nextDayReturns = this.getNextDayReturns();

        if (nextDayReturns) 
            Debug.assert(nextDayReturns.length === this.positions.length, 
                `[PSS-Load] Invalid returns array. Expecting size: ${this.positions.length} - Found size: ${nextDayReturns.length} - Date: ${this.date}`);

        this.positions.map((position, ii) => {
            position.pnl = new PositionPnl();
            position.pnl.loadObjectPSS(data, ii);
            // console.log(`[Pos] Ticker: ${position.bbTicker} = ${nextDayReturns[ii]}`);
            this.positions[ii].pnl.nextDayReturn = nextDayReturns[ii]; 
        });
    }

    loadStringPSS(str) {
        let lines = str.split(/\n/);
        let positionsPnls = [];
        let positionsMap = this.getPositionsMap();
        let nextDayReturns = this.getNextDayReturns();

        if (nextDayReturns) 
            Debug.assert(nextDayReturns.length === this.positions.length, 
                `[PSS-Load] Invalid returns array. Expecting size: ${this.positions.length} - Found size: ${nextDayReturns.length} - Date: ${this.date}`);

        for (let li = 1; li < lines.length; li++) {
            let line = lines[li].trim();

            if (line && line[0] != '#') {
                let posPnl = new PositionPnl();

                if (posPnl.loadString(line)) {
                    let cfigi = posPnl.cfigi;

                    if (posPnl.cfigi) {
                        if (positionsMap[posPnl.cfigi]) {
                            positionsMap[posPnl.cfigi].pnl = posPnl;
                            positionsMap[posPnl.cfigi].pnl.cfigi = ""; /// We don't really need this anymore!
                        }
                        else 
                            console.warn("[Positions] CFIGI: %s exists in PSS file but not in POS file!", posPnl.cfigi);
                    }

                    let posIndex = this._positionsLUT[posPnl.cfigi];
                    if (posIndex >= 0 && posIndex < this.positions.length) {
                        Debug.assert(this.positions[posIndex].pnl, `[Pos] No pnl object at index: ${posIndex}`);
                        this.positions[posIndex].pnl.nextDayReturn = nextDayReturns[posIndex]; 
                    }
                }
            }
        }


        return this;
    }

    calculateQuantiles() {
        /// If there is no associated pnl data then we cannot calculate the quantiles!
        if (!this.hasPnls())
            return this.quantiles;

        /// If we've already calculated the quantiles
        let numQuantiles = 5;
        if (this.quantiles && this.quantiles.length == numQuantiles)
            return this.quantiles;

        this.getPositionsReturns();

        this.quantiles = new Array(numQuantiles);

        let sortedPositions = this.signedSortPositions();
        let count = Math.ceil(sortedPositions.length / numQuantiles);
        let posIter = 0;

        for (let qi = 0; qi < numQuantiles; qi++) {
            let posStart = posIter;
            this.quantiles[qi] = new Quantile();

            while (posIter < Math.min(posStart + count, sortedPositions.length)) {
                let pos = sortedPositions[posIter];
                this.quantiles[qi].addPosition(pos);
                posIter++;
            }
        }

        return this.quantiles;
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /// Static methods
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // static getUnionOfFigis(positionsArray) {
    //     let figiLUT = {};
    //     let figis = [];

    //     positionsArray.map((positions, index) => {
    //         positions.positions.map((position, pindex) => {
    //             let figi = position.cfigi;
    //             if (!figiMap[figi]) {
    //                 figis.push(figi);
    //                 figiLUT[figi] = figis.length - 1;
    //             }
    //         });
    //     });

    //     return {
    //         figis: figis,
    //         figiLUT: figiLU
    //     };
    // }

    // static averageReturns(positionsArray, startIndex, endIndex) {
    //     let count = endIndex - startIndex;
    //     let returns = [{
    //         arrays: (new Array(positionsArray[0].positions.length)).fill(0.0),
    //         rank: (new Array(positionsArray[0].positions.length)).fill(0.0)
    //     }];

    //     for (let i = startIndex + 1; i < endIndex; i++) {
    //         let prevPositions   = positionsArray[i - 1];
    //         let currPositions   = positionsArray[i];
    //         let prevFigis       = prevPositions.getCFigis().values;
    //         let currFigis       = currPositions.getCFigis().values;
    //         let prevReturns     = returns[i - startIndex - 1];
    //         let currReturns     = {
    //             "array": new Array(currFigis.length)
    //         };

    //         currFigis.map((figi, index) => {
    //             if (prevReturns[figi]) {
    //                 currReturns[figi] = {
    //                     "sum": currPositions._returns[index] + prevReturns[figi].sum,
    //                     "count": prevReturns[figi].count + 1
    //                 }
    //             }
    //             else {
    //                 currReturns[figi] = {
    //                     "sum": currPositions._returns[index],
    //                     "count": 1
    //                 }
    //             }

    //             currReturns[figi]["returns"] = currReturns[figi].sum / currReturns[figi].count;
    //             currReturns.array[index] = currReturns[figi]["returns"];
    //         });

    //         returns.push(currReturns);
    //     }

    //     return returns;
    // }

    static rankReturns(returns) {
        returns.map((ret, index) => {
            ret.rank = ShUtil.rank(ret.array);
        });
    }

    // static calculateInformationCoefficient(positionsArray, returns, correlationFunction) {
    //     let ic = new InformationCoefficient();

    //     positionsArray.map((positions, index) => {
    //         positions.ic = new InformationCoefficient();
    //         positions.ic.calculate(positions, returns, index, correlationFunction);

    //         /// Add the ic to the average ic
    //         ic.add(positions.ic);
    //     });

    //     ic.average();

    //     return ic;
    // }

    static loadStringPSS(str: string): PositionPnl[] {
        let lines = str.split(/\n/);
        let positionsPnls = [];

        for (let li = 1; li < lines.length; li++) {
            let line = lines[li].trim();

            if (line && line[0] != '#') {
                let posPnl = new PositionPnl();
                if (posPnl.loadString(line))
                    positionsPnls.push(posPnl);
            }
        }

        return positionsPnls;
    }

    static getDateFromPosFilename(uuid: string, filename: string): string {
        let basename = path.basename(filename);
        let parts = basename.split("_");

        if (!parts.length) {
            return "";
        }

        let dateWithExt = parts[parts.length - 1].replace(uuid, "");
        parts = dateWithExt.split(".");
        return parts.length && parts[0].length == 8 ? parts[0] : "";
    }
}

export default function init() {
    console.info("[Model] Init Config");

    // initialize the model!
    new ICPoint();
    new InformationCoefficient();
    new EarningsInfo();
    new MiniPnl();
    new PositionPerf();
    new PositionPnl();
    new Position();
    new Quantile();
    new Positions();
}
