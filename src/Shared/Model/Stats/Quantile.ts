import { eido } from 'Shared/Core/ShCore';
import { Model } from 'Shared/Model/ShModel';
import { SecrecyLevel } from 'Shared/Model/SecrecyLevel';
import { Debug } from 'Shared/Core/ShUtil';
import { ModelObject, ModelValue, ModelClassId } from 'Shared/Core/BaseObject';
import { Position } from './Position';
import { CObject } from 'Shared/Core/CObject';

////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// PositionPnl - The pnl information of a position!
////////////////////////////////////////////////////////////////////////////////////////////////////////////////
@ModelObject()
export class Quantile extends CObject {
    @ModelClassId(Quantile, "Quantile") __dummy: string;

    @ModelValue("The dollar notional.")
    notional: number = 0.0; 
    
    @ModelValue( "The cost of holding this position.")
    cost: number = 0.0; 
    
    @ModelValue("The closing pnl of this position.")
    pnlClose: number = 0.0; 
    
    // @ModelValue( "The opening pnl.")
    // pnlOpen: number = 0.0; 
    
    // @ModelValue( "Intraday PNL high.")
    // pnlHigh: number = 0.0; 
    
    // @ModelValue( "Intraday PNL low.")
    // pnlLow: number = 0.0; 
    
    @ModelValue( "The quantile returns.")
    returns: number = 0.0; 
    
    constructor() {
        super();
    }

    addPosition(pos: Position): Quantile {
        this.notional   += Math.abs(pos.notional);
        this.cost       += Math.abs(pos.pnl.cost);
        this.pnlClose   += pos.pnl.pnlClose;
        // this.pnlOpen    += pos.pnl.pnlOpen;
        // this.pnlHigh    += pos.pnl.pnlHigh;
        // this.pnlLow     += pos.pnl.pnlLow;
        this.returns    += pos.pnl.nextDayReturn ? pos.pnl.nextDayReturn : 0.0;

        return this;
    }

    add(rhsQuantile: Quantile): Quantile {
        this.notional   += rhsQuantile.notional;
        this.cost       += rhsQuantile.cost;
        this.pnlClose   += rhsQuantile.pnlClose;
        // this.pnlOpen    += rhsQuantile.pnlOpen;
        // this.pnlHigh    += rhsQuantile.pnlHigh;
        // this.pnlLow     += rhsQuantile.pnlLow;
        this.returns    += rhsQuantile.returns ? rhsQuantile.returns : 0.0;

        return this;
    }


    net(count: number): Quantile {
        this.notional   /= count;
        this.cost       /= count;
        this.pnlClose   /= count;
        // this.pnlOpen    /= count;
        // this.pnlHigh    /= count;
        // this.pnlLow     /= count;
        this.returns    /= count;

        return this;
    }
}

