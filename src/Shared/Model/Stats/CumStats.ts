import { eido } from 'Shared/Core/ShCore';
import { Model } from 'Shared/Model/ShModel';
import { Stats } from './Stats';
import { Debug } from 'Shared/Core/ShUtil';
import { Positions } from 'Shared/Model/Stats/Positions'
import MathUtil from 'Shared/Core/MathUtil';
import { ModelObject, ModelValue, ModelClassId } from 'Shared/Core/BaseObject';
import { GroupStrategy } from 'Shared/Model/GroupPnl';
import { SecrecyLevel } from 'Shared/Model/SecrecyLevel';

export const enum StatsDuration {
    LT                  = "LT",
    IS                  = "IS",
    OS                  = "OS",
    PS                  = "PS",

    D1                  = "D1",
    D5                  = "D5",
    D10                 = "D10",
    D21                 = "D21",
    D63                 = "D63",
    D120                = "D120",
    D250                = "D250",

    singleDate          = "SD",
    customDate          = "CD"
}

export const enum PnlSpace {
    pnl                 = "PNL",
    returns             = "RET"
}

export class Analysis {
    bookSize: number = 0.0;
    totalLongPnl: number = 0.0;
    totalShortPnl: number = 0.0;
    longShortPnlDisparity: number = 0.0;
    bestDay: number = 0;
    worstDay: number = 0;
    tvrMean: number = 0.0;
    tvrSD: number = 0.0;
    avgCost: number = 0.0;
    avgLiquidationCost: number = 0.0;
    avgNumLiquidations: number = 0.0;
    avgNumLong: number = 0.0;
    avgNumShort: number = 0.0;
}

export class PlotIndex {
    istart: number = 0;
    iend: number = 0;

    constructor(istart: number = 0, iend: number = 0) {
        this.istart = istart;
        this.iend = iend;
    }
}

export class PlotData {
    dates: number[] = [];
    pnls: number[] = [];
}
 
@ModelObject()
export class CumStats extends Stats {
    @ModelClassId(CumStats, "CumStats") __dummy: string;

    @ModelValue("The name for these cum stats.", SecrecyLevel.minimal)
    name: string = "";
    
    @ModelValue("The starting date.", SecrecyLevel.minimal)
    startDate: number = 0;
    
    @ModelValue("The array of all the Stats within this.")
    points: Stats[] = [];
    
    @ModelValue("The hit ratio.", SecrecyLevel.minimal)
    hitRatio: number = 0.0;
    
    @ModelValue("The max drawdown.", SecrecyLevel.minimal)
    maxDD: number = 0.0;
    
    @ModelValue("The max days in drawdown.", SecrecyLevel.minimal)
    maxDH: number = 0.0;
    
    @ModelValue("The minReturns for this time period.")
    minReturns: number = 0.0;
    
    @ModelValue("The maxReturns for this time period.")
    maxReturns: number = 0.0;
    
    @ModelValue("The information ratio for this time period.", SecrecyLevel.minimal)
    ir: number = 0.0;

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /// Not part of the Model
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    _analysis               : Analysis = null;
        
    constructor() {
        super();
    }

    get numPoints() { return this.points && this.points.length > 0 ? this.points.length : 0; }

    initFromGroupStrategy(gstrat: GroupStrategy, startIndex: number, endIndex: number = 0) {
        if (!endIndex)
            endIndex = gstrat.pnls.length;

        if (startIndex < 0) 
            startIndex = Math.max(endIndex + startIndex, 0);

        let numPoints = endIndex - startIndex;
        Debug.assert(numPoints > 0, `Invalid array length: [${startIndex}, ${endIndex})`);
        this.points = new Array(numPoints);

        this.startDate = gstrat.pnls[startIndex].date;

        this.cumPnl = 0.0;
        let pnls = new Array(numPoints);
        this.minReturns = 0.0;
        this.maxReturns = 0.0;

        let numWon = 0;
        let returns = new Array(numPoints);
        let maxPnl = 0.0, minPnl = 0.0;
        // this.maxDD = 0.0;
        this.maxDH = 0.0;
        this.bookSize = 0.0;

        this.returns = 0.0;

        for (let i = startIndex; i < endIndex; i++) {
            let index = i - startIndex;
            let pointStats = new Stats();
            pointStats.initFromGroupStrategy(gstrat, i);
            this.points[index] = pointStats;

            this.cumPnl += pointStats.pnl;
            let returns = (pointStats.pnl * 100.0 / pointStats.bookSize) * 250.0;
            this.returns += returns;

            if (pointStats.pnl > 0.0) {
                numWon++;

                if (this.cumPnl > maxPnl) {
                    maxPnl = this.cumPnl;
                    this.maxDH = 0.0;
                }
                else
                    this.maxDH++;
            }
            else {
                if (this.cumPnl < minPnl)
                    minPnl = this.cumPnl;

                this.maxDH++;
            }

            if (i == 0 || returns < this.minReturns)
                this.minReturns = returns;

            if (i == 0 || returns > this.maxReturns)
                this.maxReturns = returns;

            this.bookSize += pointStats.bookSize;
            pnls[index] = pointStats.pnl;
        }

        this.returns /= numPoints;
        this.bookSize /= numPoints;
        this.pnl = this.cumPnl;
        this.hitRatio = numWon / numPoints;
        this.maxDD = ((maxPnl - minPnl) * 100.0) / this.bookSize;

        // let math = require('mathjs');
        let pnlMean = this.cumPnl / pnls.length; /// math.mean(pnls);
        let pnlStdDev = MathUtil.stddev(pnls, pnlMean);

        this.ir = 0.0;

        if (pnlStdDev != 0.0)
            this.ir = pnlMean / pnlStdDev;

        return this;
    }

    /// TODO: 
    _runAnalysis(): Analysis {
        return new Analysis();
    }

    runAnalysis() {
        if (this._analysis)
            return this._analysis;

        this._analysis = this._runAnalysis();
        return this._analysis;
    }

    getIndexForDate(date: number): number {
        if (!this.points || !this.points.length)
            return -1;

        /// Just check the extremes first ...
        if (date >= this.points[this.points.length - 1].date)
            return this.points.length - 1;
        else if (date <= this.points[0].date)
            return 0;

        let prevPoint = null;
        for (let i = 0; i < this.points.length; i++) {
            let point = this.points[i];

            if (point.date == date)
                return i;
            else if (prevPoint && prevPoint.date < date && point.date > date)
                return i;

            prevPoint = point;
        }

        return -1;
    }

    adjustPlotIndexes(istart: number, iend: number): PlotIndex {
        if (!this.points || !this.points.length)
            return { istart: 0, iend: 0 };

        if (typeof istart === 'undefined')
            istart = 0;
        else if (istart < 0)
            istart = Math.max((this.points.length + istart) - 1, 0);

        if (typeof iend === 'undefined')
            iend = this.points.length - 1;
        else if (iend < 0)
            iend = Math.max((this.points.length + iend) - 1, 0);

        iend = Math.max(0, Math.min(iend, this.points.length - 1));
        istart = Math.max(0, Math.min(istart, this.points.length - 1));

        Debug.assert(iend >= istart && istart >= 0 && istart < this.points.length - 1 && iend >= 0 && iend < this.points.length,
            `Invalid istart and iend: ${istart}, ${iend}`);
        
        return {
            istart: istart,
            iend: iend
        };
    }

    getPoints(duration: string, startDate: number, endDate: number): Stats[] {
        let plotIndexes = this.getPlotIndexes(duration, 0, "", startDate, endDate);
        let pi = this.adjustPlotIndexes(plotIndexes.istart, plotIndexes.iend);

        if (pi.istart < 0 || pi.iend < 0)
            return [];

        let length = pi.iend - pi.istart + 1;
        let points = new Array<Stats>(length);
        let di = 0;

        for (let i = pi.iend; i >= pi.istart; i--) {
            points[di] = this.points[i];
            di++;
        }

        return points;
    }

    getPlotIndexes(duration: string, osDate: number, posName: string, startDate: number, endDate: number): PlotIndex {
        let istart = 0;
        let iend = this.points.length - 1;

        if (duration != "LT") {
            if (duration == "D5")
                istart = -5;
            else if (duration == "D10")
                istart = -10;
            else if (duration == "D21")
                istart = -21;
            else if (duration == "D63")
                istart = -63;
            else if (duration == "D120")
                istart = -120;
            else if (duration == "D250")
                istart = -250;
            else if (duration == "D1")
                istart = -1;
            else if (duration == "IS") {
                if (osDate > 0)
                    iend = this.getIndexForDate(osDate);
            }
            else if (duration == "OS") {
                if (osDate > 0)
                    istart = this.getIndexForDate(osDate);
            }
            else if (duration == "SD") {
                Debug.assert(posName, "For a single date, you must give the pos name as well!");
                /// Get the date from the pos name
                let sdate = parseInt(Positions.getDateFromPosFilename("", posName));

                if (!sdate) /// This must be the final day (the same as D1)
                    istart = -1
                else {
                    let date = parseInt(Positions.getDateFromPosFilename("", posName));
                    iend = this.getIndexForDate(date);
                    if (iend != 0) {
                        if (iend < this.points.length - 1) {
                            istart = iend;
                            iend += 1;
                        }
                        else
                            istart = iend - 1;
                    }
                }
            }
            else if (duration == "CD") {
                istart = this.getIndexForDate(startDate);
                iend = this.getIndexForDate(endDate);
            }
        }

        return { istart: istart, iend: iend };
    }

    getPnlPlotDataForRange(istartOrg: number, iendOrg: number, key: string, returnSpace: boolean, endPlotIndex: number = 0): PlotData {
        if (!this.points || !this.points.length)
            return { dates: [], pnls: [] };

        let { istart, iend } = this.adjustPlotIndexes(istartOrg, iendOrg);

        if (!key)
            key = "pnl";

        let dates = [this.points[istart].date];
        let pnls = [0.0];
        let cumPnl = 0.0;

        for (let i = istart + 1; i <= iend; i++) {
            let point = this.points[i];
            dates.push(point.date);

            if (!returnSpace)
                cumPnl += point[key];
            else
                cumPnl += (point[key] / point.bookSize) * 100.0;

            if (!endPlotIndex || endPlotIndex > i)
                pnls.push(cumPnl);
        }

        return {
            dates: dates,
            pnls: pnls
        };
    }

    loadObject(data: any, parent: any) {
        if (!data)
            return null;

        let vec = [];
        if (!parent || data.__forceCopy)
            vec = data.vec || data.points;
        else {
            vec = new Array(data.indexes.length);
            data.indexes.map((index, i) => vec[i] = parent.vec[index]);
        }

        Debug.assert(vec, "Invalid vector of stat points!");

        let cumPnl = 0.0;

        this.points = new Array(vec.length);
        vec.map((pt, i) => {
            cumPnl += pt.pnl;
            this.points[i] = (new Stats()).loadObject(pt) 
            this.points[i].cumPnl = cumPnl;
        });

        this.loadObjectRange(data);

        this.pnl = this.cumPnl = cumPnl;

        return this;
    }
    
    loadObjectRange(data: any): CumStats {
        if (!data)
            return this;

        super.loadObject(data);

        /// For accumulated stats, this is the cumulative pnl
        this.name               = data.stype;
        this.startDate          = data.startDate;
        this.ir                 = data.ir;
        this.maxDD              = data.maxDD;
        this.maxDH              = data.maxDH;
        this.hitRatio           = data.hitRatio;
        this.marginBps          = data.marginBps;
        this.minReturns         = data.minReturns;
        this.maxReturns         = data.maxReturns;

        return this;
    }

    loadRangeString(str: string): boolean {
        if (!str)
            return false;

        let parts               = str.split(',');
        if (!parts.length)
            return false;

        this.date               = parseInt(parts[0].trim());
        this.bookSize           = parseFloat(parts[1].trim());
        this.longCount          = parseInt(parts[2].trim());
        this.shortCount         = parseInt(parts[3].trim());
        this.liquidateCount     = parseInt(parts[23].trim());
        this.pnl                = parseFloat(parts[4].trim());
        this.ir                 = parseFloat(parts[5].trim());
        this.returns            = parseFloat(parts[6].trim());
        this.tvr                = parseFloat(parts[7].trim());
        this.maxDD              = parseFloat(parts[8].trim());
        this.maxDH              = parseInt(parts[9].trim());
        this.hitRatio           = parseFloat(parts[10].trim());
        this.marginBps          = parseFloat(parts[11].trim());
        this.minReturns         = parseFloat(parts[13].trim());
        this.maxReturns         = parseFloat(parts[14].trim());
        this.tradedNotional     = parseFloat(parts[15].trim());
        this.heldNotional       = parseFloat(parts[16].trim());
        this.longNotional       = parseFloat(parts[17].trim());
        this.shortNotional      = parseFloat(parts[18].trim());
        this.liquidateNotional  = parseFloat(parts[19].trim());
        this.cost               = parseFloat(parts[24].trim());

        return true;
    }

    getStartDate(): number { 
        return this.points.length ? this.points[0].date : 0;
    }
    
    getEndDate(): number { 
        return this.points.length ? this.points[this.points.length - 1].date : 0;
    }

    getOSStartDate(): number {
        return this.getStartDate();
    }

    loadString(str: string): any {
        let lines = str.split(/\n/);
        this.points = [];

        /// We ignore the first line
        let li = 1;

        for (; li < lines.length; li++) {
            let line = lines[li];

            /// The first line that we encounter a comment then we break
            if (line[0] == '#')
                break;

            let point = new Stats();
            if (point.loadString(line))
                this.points.push(point);
        }

        return {
            lines: lines,
            li: li
        };
    }
    
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /// Static methods
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
}

export default function init() {
    console.info("[Model] Init CumStats");

    // initialize the model!
    // CumStats.__initModel = undefined;
    new CumStats();
}
