import { eido } from 'Shared/Core/ShCore';
import { Model } from 'Shared/Model/ShModel';
import { Stats } from './Stats';
import { PlotIndex, CumStats } from './CumStats';
import { Debug } from 'Shared/Core/ShUtil';
import { GroupStrategy } from 'Shared/Model/GroupPnl';
import { ModelObject, ModelValue, ModelClassId } from 'Shared/Core/BaseObject';
import { ObjectID } from 'Shared/Core/ObjectID';

@ModelObject()
export class LTStats extends CumStats {
    @ModelClassId(LTStats, "LTStats") __dummy: string;

    @ModelValue("The unique name of these stats. For Alphas this is the id of the alpha and for strategy this is the UUID of the portfolio.")
    _id: string = "";
    
    @ModelValue("The parent ObjectID.")
    iid: ObjectID = null;
    
    @ModelValue("The yearly stats for this strategy.")
    annual: CumStats[] = [];
    
    @ModelValue("The start date for the out sample")
    osDate: number = 0;
    
    @ModelValue("D5 stats.")
    D5: CumStats = new CumStats();
    
    @ModelValue("D10 stats.")
    D10: CumStats = new CumStats();
    
    @ModelValue("D21 stats.")
    D21: CumStats = new CumStats();
    
    @ModelValue("D63 stats.")
    D63: CumStats = new CumStats();
    
    @ModelValue("D120 stats.")
    D120: CumStats = new CumStats();
    
    @ModelValue("D250 stats.")
    D250: CumStats = new CumStats();
    
    @ModelValue("In-Sample stats.")
    inSample: CumStats = new CumStats();
    
    @ModelValue("Out-Sample stats.")
    outSample: CumStats = new CumStats();
        
    constructor() {
        super();
        this.name = "LT";
    }

    getOSStartDate(): number {
        return this.osDate ? this.osDate : this.getStartDate();
    }

    initFromGroupStrategy(gstrat: GroupStrategy, _no_op_: number) {
        super.initFromGroupStrategy(gstrat, 0, gstrat.pnls.length);

        this.D5         = (new CumStats()).initFromGroupStrategy(gstrat, -5);
        this.D10        = (new CumStats()).initFromGroupStrategy(gstrat, -10);
        this.D21        = (new CumStats()).initFromGroupStrategy(gstrat, -21);
        this.D63        = (new CumStats()).initFromGroupStrategy(gstrat, -63);
        this.D120       = (new CumStats()).initFromGroupStrategy(gstrat, -120);
        this.D250       = (new CumStats()).initFromGroupStrategy(gstrat, -250);

        this.inSample   = new CumStats();
        this.outSample  = (new CumStats()).initFromGroupStrategy(gstrat, 0, gstrat.pnls.length);

        return this;
    }

    loadObject(data: any, minimal: boolean = false) {
        super.loadObject(data.LT, null)

        if (!minimal) {
            this.D5.loadObject(data.D5, data.LT);
            this.D10.loadObject(data.D10, data.LT);
            this.D21.loadObject(data.D21, data.LT);
            this.D63.loadObject(data.D63, data.LT);
            this.D120.loadObject(data.D120, data.LT);
            this.D250.loadObject(data.D250, data.LT);
    
            this.inSample.loadObject(data.IS, data.LT);
            this.outSample.loadObject(data.OS, data.LT);
    
            this.annual = [];
    
            if (data.OS && data.OS.startDate)
                this.osDate = data.OS.startDate;
    
            if (data.annual, data.annual.length) {
                this.annual = new Array(data.annual.length);
                data.annual.map((annualData, ai) => this.annual[ai] = (new CumStats()).loadObject(annualData, data.LT) );
            }
        }

        return this;
    }

    loadString(str: string) {
        let status = super.loadString(str);
        if (!status)
            return;

        let lines = status.lines;
        let li = status.li;

        /// We ignore the first line
        for (; li < lines.length; li++) {
            let line = lines[li];

            /// The first line that we encounter a comment then we break
            if (line[0] == '#' && line[1] == '#')
                continue;
            else if (line.trim().startsWith("Date")) /// The header block
                continue;

            let name = line.substring(1).trim();
            if (!name)
                continue;
            
            /// go to the next line ...
            li++;
            let cumStats = null;
            if (name == 'D5' || name == 'D10' || name == 'D21' || name == 'D63' || name == "D120" || name == 'D250') 
                cumStats = this[name] = new CumStats();
            else if (name[0] == 'Y') {
                cumStats = null;
                name = name.substring(1); /// Remove the leading Y
                if (!this.annual)
                    this.annual = [];
                this.annual.push(cumStats);
            }
            else if (name == 'InSample')
                cumStats = this.inSample = new CumStats();
            else if (name.startsWith('OutSample')) {
                let parts = name.split('-');
                Debug.assert(parts.length == 2, `Parse error while getting out sample date: ${name}`);
                name = parts[0];
                this.osDate = parseInt(parts[1].trim());
                cumStats = this.outSample = new CumStats();
                this.outSample.startDate = this.osDate;
            }
            else if (name == 'LT')
                cumStats = this;
            else {
                console.log(`[Stats] Invalid cum stats block: ${name} Ignoring ...`);
                continue;
            }
            
            Debug.assert(cumStats, `Invalid cumStats for block: '${name}'`);
            cumStats.name = name;
            cumStats.loadRangeString(lines[li]);
        }

        return this;
    }

    getAnnualStats(year) {
        for (let i = 0; i < this.annual.length; i++) {
            let astats = this.annual[i];
            if (astats.name == year)
                return astats;
        }

        return null;
    }

    getPlotIndexes(duration: string, osDate: number, posFile: string, startDate: number, endDate: number): PlotIndex {
        /// If its a year
        if (duration && duration.startsWith("Y")) {
            /// remove the leading Y
            let year = parseInt(duration.slice(1));
            let startDate = year * 10000 + 101; /// Change to YYYYMMDD (e.g. Y2016 = 20160101)
            let endDate = (year + 1) * 10000 + 101; /// Get the end date, which is the start of the next year

            return {
                istart: this.getIndexForDate(startDate),
                iend: this.getIndexForDate(endDate)
            };
        }
         
        /// Otherwise we just get it from the cumstats
        return super.getPlotIndexes(duration, osDate ? osDate : this.osDate, posFile, startDate, endDate);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /// Static methods
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
}

export default function init() {
    console.info("[Model] Init LTStats");

    // initialize the model!
    // LTStats.__initModel = undefined;
    new LTStats();
}
