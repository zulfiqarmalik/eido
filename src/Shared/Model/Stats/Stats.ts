import { SecrecyLevel } from 'Shared/Model/SecrecyLevel';
import { ModelObject, ModelValue, ModelClassId } from 'Shared/Core/BaseObject';
import { GroupStrategy } from 'Shared/Model/GroupPnl';
import { CObject } from 'Shared/Core/CObject';

@ModelObject()
export class Stats extends CObject {
    @ModelClassId(Stats, "Stats") __dummy: string;

    @ModelValue("The date for this point.", SecrecyLevel.minimal)
    date: number = 0;
    
    @ModelValue("The book size.", SecrecyLevel.minimal)
    bookSize: number = 0;
    
    @ModelValue("The pnl at this point.", SecrecyLevel.minimal)
    cumPnl: number = 0;
    
    @ModelValue("The pnl at this point.", SecrecyLevel.minimal)
    pnl: number = 0;
    
    @ModelValue("The pnl from the long side.")
    longPnl: number = 0;
    
    @ModelValue("The pnl from the short side.")
    shortPnl: number = 0;
    
    @ModelValue("The number of long stocks.")
    longCount: number = 0;
    
    @ModelValue("The number of short stocks.")
    shortCount: number = 0;
    
    @ModelValue("The number of liquidate stocks.")
    liquidateCount: number = 0;
    
    @ModelValue("The returns.", SecrecyLevel.minimal)
    returns: number = 0;
    
    @ModelValue("The volatility.", SecrecyLevel.minimal)
    volatility: number = 0;
    
    @ModelValue("The margin in BPS.", SecrecyLevel.minimal)
    marginBps: number = 0;
    
    @ModelValue("The turnover.", SecrecyLevel.minimal)
    tvr: number = 0;
    
    @ModelValue("The trading notional.")
    tradedNotional: number = 0;
    
    @ModelValue("The held notional.")
    heldNotional: number = 0;
    
    @ModelValue("The held notional.")
    longNotional: number = 0;
    
    @ModelValue("The held notional.")
    shortNotional: number = 0;
    
    @ModelValue("The held notional.")
    liquidateNotional: number = 0;
    
    @ModelValue("The trading costs.")
    cost: number = 0;
    
    constructor() {
        super();
    }

    initFromGroupStrategy(gstrat: GroupStrategy, index: number, ...args) {
        let pnl             = gstrat.pnls[index];
        this.date           = pnl.date;
        this.bookSize       = pnl.bookSize;
        this.cumPnl         = pnl.pnl;
        this.pnl            = pnl.pnl;
        this.returns        = ((this.pnl * 100.0) / this.bookSize) * 250.0;

        return this;
    }

    loadObject(data: any, ...args) {
        if (!data)
            return this;

        this.date               = data.date;
        this.bookSize           = data.bookSize;
        this.cumPnl             = data.cumPnl;
        this.pnl                = data.pnl;
        this.returns            = data.returns;
        this.volatility         = data.volatility;
        this.tvr                = data.tvr;
        this.marginBps          = data.marginBps;

        this.tradedNotional     = data.tradedNotional;
        this.longNotional       = data.longNotional;
        this.shortNotional      = data.shortNotional;
        this.heldNotional       = (this.longNotional + this.shortNotional) - this.tradedNotional;
        this.liquidateNotional  = data.liquidateNotional;

        this.longPnl            = data.longPnl;
        this.shortPnl           = data.shortPnl;

        this.longCount          = data.longCount;
        this.shortCount         = data.shortCount;
        this.liquidateCount     = data.liquidateCount;
        this.cost               = data.cost;

        return this;
    }

    loadString(str: string): any {
        if (!str)
            return false;

        let parts               = str.split(',');
        if (!parts.length)
            return false;

        this.date               = parseInt(parts[0].trim());
        this.bookSize           = parseFloat(parts[1].trim());
        this.cumPnl             = parseInt(parts[2].trim());
        this.pnl                = parseInt(parts[3].trim());
        this.returns            = parseInt(parts[4].trim());
        this.tvr                = parseInt(parts[5].trim());
        this.marginBps          = parseInt(parts[6].trim());

        this.tradedNotional     = parseFloat(parts[8].trim());
        this.heldNotional       = parseFloat(parts[9].trim());
        this.longNotional       = parseFloat(parts[10].trim());
        this.shortNotional      = parseFloat(parts[11].trim());
        this.liquidateNotional  = parseFloat(parts[12].trim());

        if (parts.length > 20) {
            this.longPnl        = parseInt(parts[20].trim());
            this.shortPnl       = parseInt(parts[21].trim());
        }
        
        this.longCount          = parseInt(parts[16].trim());
        this.shortCount         = parseInt(parts[17].trim());
        this.liquidateCount     = parseInt(parts[18].trim());
        this.cost               = parseInt(parts[19].trim());

        return true;
    }
    
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /// Static methods
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
}

export default function init() {
    console.info("[Model] Init Stats");

    // initialize the model!
    new Stats();
}
