import { eido } from 'Shared/Core/ShCore';
import { ObjectID } from "Shared/Core/ObjectID";
import { ShUtil, Debug } from 'Shared/Core/ShUtil';
import { Model } from 'Shared/Model/ShModel';
import { SecrecyLevel } from 'Shared/Model/SecrecyLevel';
import { ModelObject, ModelValue, ModelClassId } from 'Shared/Core/BaseObject';
import { CObject } from '../Core/CObject';

export enum TaskStatus {
    queued = 1000,       // The task has been queued awaiting to be picked up by one of the servers
    working = 1,         // The task is being worked upon
    
    /// <= 0 means task has finished, one way or another 
    finished = 0,        // The task has finished without errors
    failed =  -666,      // The task has failed
    terminated = -1000  // The task has failed
};

@ModelObject()
export class ShTask extends CObject {
    @ModelClassId(ShTask, "ShTask") __dummy: string;

    @ModelValue("The unique ID of the task.")
    _id: string = "";
    
    @ModelValue("The time when this task was created.")
    creationDate: Date = new Date();
    
    @ModelValue("The time when this task was last updated.")
    lastUpdated: Date = new Date();
    
    constructor() {
        super();
    }

    isTask() {
        return true;
    }
}
