/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// SecrecyLevel
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
export enum SecrecyLevel {
    minimal =           1 << 0,         // Minimal information. Normally for display purposes. This normally consists of _id and name etc.
    embeddedRef =       1 << 1,         // Embedded $ref
    embedded =          1 << 2,         // Normally used when one object is embedded within another and data is duplicated. We only wanna retain minimal information. The whole purpose of this thing is be public
    verbatim =          1 << 3,         // DEFAULT: A clone of the object is embedded in the other object
    
    // Important Note: Above this line (dbLevels) are levels which are used when serilizing into the DB
    dbLevels =          0x000000FF,
    // Important Note: Below this line (dbLevels)are levels which are used when sending to client and other non-DB related functions 
     
    public =            1 << 8,         // DEFAULT: This is public, non-sensitive information
    verbose =           1 << 9,         // Information is detailed and should not be sent or copied across unless explicitly requested
    private =           1 << 14,        // Information private to the user. This is never sent across to other users
    server =            1 << 15,        // This is server owned. This is never copied across and not availble to anyone (except the DB. See below)! Even to the user! 

    // Important Note: Above this line (clLevels) are levels which are used when sending to clients
    clLevels =          0x0000FF00,
    // Important Note: Below this line are special levels that are used on the server side primarily
    
    // add more here ... above the server 
    db =                1 << 17,        // Data is being pushed into the DB. This has a special status as it allows server side data to be passed around
    noExpandDBRef =     1 << 18,        // DO not expand the DBRef. These items are late bound 
    
    svLevels =          0x00FF0000, 

    // NEVER GO ABOVE 20!

    // Special ... never use this in sendClient!
    REST =              0xEFFFFF00,     // REST get special treatment sometimes
    all =               0xFFFFFF00,     // Copy everything

    dbEmbedded =        (SecrecyLevel.db | SecrecyLevel.embedded),
    dbMinimal =         (SecrecyLevel.db | SecrecyLevel.minimal),
    publicEmbedded =    (SecrecyLevel.embedded | SecrecyLevel.public),
    publicEmbeddedRef = (SecrecyLevel.embeddedRef | SecrecyLevel.public),
    privateEmbeddedRef =(SecrecyLevel.embeddedRef | SecrecyLevel.private),
};
