import { ModelObject, ModelValue, ModelClassId } from 'Shared/Core/BaseObject';
import { CObject } from '../Core/CObject';
import { Debug } from 'Shared/Core/ShUtil';

@ModelObject()
export class Sec {
    @ModelClassId(Sec, "Sec") __dummy: string;

    @ModelValue("The uuid of the security.")
    uuid: string;
    
    @ModelValue("The figi of the security.")
    figi: string;

    @ModelValue("The isin of the security.")
    isin: string;

    @ModelValue("The name of the security.")
    name: string;

    @ModelValue("The permId of the security.")
    permId: string;

    @ModelValue("The perfId of the security.")
    perfId: string;

    @ModelValue("The ticker of the security.")
    ticker: string;

    // @ModelValue("All tickers including historical tickers.")
    // allTickers: string[] = [];
};

@ModelObject()
export class SecMaster extends CObject {
    @ModelClassId(SecMaster, "SecMaster") __dummy: string;

    @ModelValue("The securities within this secmaster!")
    securities      : Sec[] = [];
    _secLookup      : any = null;
    _tickers        : string[] = null;
    _searchStrings  : any[] = null;
    _searchStringsLUT: any;
    
    constructor(securities: Sec[]) {
        super();

        if (!securities)
            return;

        this.securities = securities;
        this.build();
    }

    build() {
        if (this._secLookup && this._tickers)
            return;

        this._tickers = new Array<string>(this.securities.length);
        this._searchStrings = new Array<any>(this.securities.length);
        this._secLookup = {};
        this._searchStringsLUT = {};

        this.securities.map((sec, ii) => {
            this._secLookup[sec.uuid] = ii;
            if (sec.figi)
                this._secLookup[sec.figi] = ii;
            if (sec.isin)
                this._secLookup[sec.isin] = ii;
            if (sec.permId)
                this._secLookup[sec.permId] = ii;
            if (sec.perfId)
                this._secLookup[sec.perfId] = ii;
            if (sec.ticker)
                this._secLookup[sec.ticker] = ii;

            this._tickers[ii] = sec.ticker;
            let searchString: string = `${sec.uuid} : ${sec.ticker} : ${sec.name}`;

            this._searchStrings[ii] = {
                text: searchString,
                value: ii
            };
            this._searchStringsLUT[searchString] = ii;
        });
    }

    at(ii: number): Sec {
        Debug.assert(ii >= 0 && ii < this.securities.length, `Invalid index requested: ${ii} [0, ${this.securities.length}]`);
        return this.securities[ii];
    }

    count(): number {
        return this.securities ? this.securities.length : 0;
    }

    findSearchString(searchString: string): Sec {
        if (this._searchStringsLUT[searchString])
            return this.securities[this._searchStringsLUT[searchString]];
        return null;
    }

    find(search: string): Sec {
        if (!this._secLookup)
            this.build();

        if (this._secLookup[search])
            return this.securities[this._secLookup[search]];
        return null;
    }

    searchStrings(): string[] {
        if (!this._searchStrings)
            this.build();

        return this._searchStrings;
    }

    tickers(): string[] {
        if (!this._tickers)
            this.build();

        return this._tickers;
    }
}

export default function init() {
    console.info("[Model] Init ShData");

    // initialize the model!
    new Sec();
    new SecMaster([]);
}
