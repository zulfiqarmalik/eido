import { ObjectID } from "Shared/Core/ObjectID";
import { SecrecyLevel } from 'Shared/Model/SecrecyLevel';
import { ModelObject, ModelValue, ModelClassId } from 'Shared/Core/BaseObject';
import { CObject } from 'Shared/Core/CObject';

export enum TradeType {
    UAT = "UAT",
    PROD = "PROD"
};

export enum OrderSide {
    none = "",
    buy = "Buy",
    sell = "Sell",
    sellShort = "SellShort"
}

export enum BookingType {
    none = "",
    cash = "Cash",
    swap = "Swap"
}

export enum WaveState {
    init = "init",
    submitted = "submitted",
    cancelled = "cancelled"
}

////////////////////////////////////////////////////////////////////////////////////
/// MarketSecurity: The market securuity that was internal to us
////////////////////////////////////////////////////////////////////////////////////
@ModelObject()
export class MarketSecurity extends CObject {
    @ModelClassId(MarketSecurity, "MarketSecurity") __dummy: string;

    @ModelValue("ticker.", SecrecyLevel.minimal)
    ticker: string = "";

    @ModelValue("sector.", SecrecyLevel.minimal)
    sector: string = "";
    
    @ModelValue("mic.", SecrecyLevel.minimal)
    mic: string = "";
    
    @ModelValue("numShares.", SecrecyLevel.minimal)
    numShares: string = "";
    
    @ModelValue("industry.", SecrecyLevel.minimal)
    industry: string = "";
    
    @ModelValue("instrumentId.", SecrecyLevel.minimal)
    instrumentId: string = "";
    
    @ModelValue("bbTicker.", SecrecyLevel.minimal)
    bbTicker: string = "";
    
    @ModelValue("uuid.", SecrecyLevel.minimal)
    uuid: string = "";
    
    @ModelValue("name.", SecrecyLevel.minimal)
    name: string = "";
    
    @ModelValue("countryCode.", SecrecyLevel.minimal)
    countryCode: string = "";
    
    @ModelValue("figi.", SecrecyLevel.minimal)
    figi: string = "";
    
    @ModelValue("sedol.", SecrecyLevel.minimal)
    sedol: string = "";
    
    @ModelValue("isin.", SecrecyLevel.minimal)
    isin: string = "";
    
    @ModelValue("price.", SecrecyLevel.minimal)
    price: string = "";
    
    @ModelValue("currencyStr.", SecrecyLevel.minimal)
    currencyStr: string = "";
};

////////////////////////////////////////////////////////////////////////////////////
/// MarketOrder: The market order that was internal to us
////////////////////////////////////////////////////////////////////////////////////
@ModelObject()
export class MarketOrder extends CObject {
    @ModelClassId(MarketOrder, "MarketOrder") __dummy: string;

    @ModelValue("position", SecrecyLevel.minimal)
    position: number = 0.0;

    @ModelValue("quantity", SecrecyLevel.minimal)
    quantity: number = 0.0;

    @ModelValue("tradeNotional", SecrecyLevel.minimal)
    tradeNotional: number = 0.0;

    @ModelValue("notional", SecrecyLevel.minimal)
    notional: number = 0.0;

    @ModelValue("target", SecrecyLevel.minimal)
    target: number = 0.0;

    @ModelValue("sec", SecrecyLevel.minimal)
    sec: MarketSecurity = null;
};

////////////////////////////////////////////////////////////////////////////////////
/// MarketTrade: The market order that was internal to us
////////////////////////////////////////////////////////////////////////////////////
@ModelObject()
export class MarketTrade extends CObject {
    @ModelClassId(MarketTrade, "MarketTrade") __dummy: string;

    @ModelValue("algoName", SecrecyLevel.minimal)
    algoName: string = "";

    @ModelValue("side", SecrecyLevel.minimal)
    side: OrderSide = OrderSide.none;

    @ModelValue("quantity", SecrecyLevel.minimal)
    quantity: number = 0;

    @ModelValue("bookingType", SecrecyLevel.minimal)
    bookingType: BookingType = BookingType.none;

    @ModelValue("security", SecrecyLevel.minimal)
    security: any = null;
};

////////////////////////////////////////////////////////////////////////////////////
/// Trade: The main trade object
////////////////////////////////////////////////////////////////////////////////////
@ModelObject()
export class ShTrade extends CObject {
    @ModelClassId(ShTrade, "ShTrade") __dummy: string;

    @ModelValue("The unique id for the trade.", SecrecyLevel.minimal)
    _id: string = "";

    @ModelValue("The type.", SecrecyLevel.minimal)
    type: TradeType = TradeType.UAT;

    @ModelValue("The waves that have been submitted.", SecrecyLevel.minimal)
    submittedWaves: string[] = [];

    @ModelValue("The waves that have been queued.", SecrecyLevel.minimal)
    waves: string[] = [];
};

////////////////////////////////////////////////////////////////////////////////////
/// Wave: The wave
////////////////////////////////////////////////////////////////////////////////////
@ModelObject()
export class ShWave extends CObject {
    @ModelClassId(ShWave, "ShWave") __dummy: string;

    @ModelValue("_id", SecrecyLevel.minimal)
    _id: string = "";

    @ModelValue("errorCode", SecrecyLevel.minimal)
    errorCode: number = 0;

    @ModelValue("submissionDate", SecrecyLevel.minimal)
    submissionDate: Date = null;

    @ModelValue("type", SecrecyLevel.minimal)
    type: TradeType = TradeType.UAT;

    @ModelValue("name", SecrecyLevel.minimal)
    name: string = "";

    @ModelValue("errorMessage", SecrecyLevel.minimal)
    errorMessage: string = "";

    @ModelValue("market", SecrecyLevel.minimal)
    market: string = "";

    @ModelValue("l_creationDate", SecrecyLevel.minimal)
    l_creationDate: Date = new Date();

    @ModelValue("creationDate", SecrecyLevel.minimal)
    creationDate: Date = new Date();

    @ModelValue("state", SecrecyLevel.minimal)
    state: WaveState = WaveState.init;

    @ModelValue("cancellationDate", SecrecyLevel.minimal)
    cancellationDate: Date = null;

    @ModelValue("marketTrades", SecrecyLevel.minimal)
    marketTrades: MarketTrade[] = [];

    @ModelValue("marketOrders", SecrecyLevel.minimal)
    marketOrders: MarketOrder[] = [];
};
