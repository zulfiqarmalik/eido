import { eido } from "Shared/Core/ShCore";
import { ObjectID } from "Shared/Core/ObjectID";
import { Debug } from "Shared/Core/ShUtil";
import 'Shared/Core/StringUtil';
import 'Shared/Core/Date';
import 'Shared/Core/DateUtil';
import { SecrecyLevel } from './SecrecyLevel';

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// Helper implementations
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
export declare module String {
    export var format: any;
}

export enum SortOrder {
    desc = "desc",
    asc = "asc"
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// ModelProperty: Defines the properties of a single model value
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
export class ModelProperty {
    name                    : string        = "";                   /// The name of this property
    type                    : string        = "";                   /// What is the typeof this object
    level                   : SecrecyLevel  = SecrecyLevel.public;  /// What is the level of this model property
    desc                    : string        = "";                   /// The description of this property
    readOnly                : boolean       = false;                /// Is this a readonly property or not
    nullable                : boolean       = false;                /// Whether this is a nullable property or not
    public noDB             : boolean       = false;                /// Don't put this in the DB

    constructor(type: string, name: string, desc: string, level: SecrecyLevel = SecrecyLevel.public) {
        this.type           = type;
        this.name           = name;
        this.desc           = desc;
        this.level          = level;
    }
}

interface ModelPropertyMap {
    [key: string]: ModelProperty;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// ModelProperties: All the properties of a ModelObject
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
export class ModelProperties {
    private props           : ModelPropertyMap = {};                /// The props of this object)
    private propsLevel      : Object        = {};                   /// Props according to level
    private names           : string[]      = [];                   /// The names of all the props
    public isFinialised     : boolean       = false;                /// Whether the model has been finalised or not
    public name             : string        = "";                   /// The name of the model object
    public desc             : string        = "";                   /// The description of this model object

    constructor(cls: any = null) {
        // this.constructor = cls;
        Object.keys(SecrecyLevel).map((secrecyValue) =>  this.propsLevel[secrecyValue] = {})
    }

    getNames(): string[] {
        return this.names;
    }

    getProp(name: string): ModelProperty {
        return this.props[name];
    }

    getAllProps(): Object {
        return this.props;
    }

    copyFrom(other: ModelProperties): ModelProperties {
        // if (!other)
        //     return this;
            
        for (let key in other.props) {
            let props: ModelProperty = other.props[key];
            this.add(props.name, props);
        }

        return this;
    }

    add(name: string, props: ModelProperty) {
        if (this.props[name])
            return;

        if (!this.propsLevel[props.level])
            this.propsLevel[props.level] = {};
            
        this.propsLevel[props.level][name] = props;
        
        this.props[name] = props;
        this.names.push(name);
    }

    getProps(level) {
        //if (!level || level >= SecrecyLevel.server)
            return this.props;
         
        //return this.propsLevel[level] ? this.propsLevel[level] : {}; 
    }

    get(name) {
        return this.props[name];
    }

    doesExist(name) {
        return this.props[name] ? true : false;
    }

    getApiDocs() {
        let docs = {
            type: "object",
            properties: {}
        };

        for (let key in this.props) {
            let prop: ModelProperty = this.props[key];
            docs.properties[key] = prop.type;
        }

        return docs;
    }

    static createFromObject(src) {
        var props = new ModelProperties(null);

        for (var name in src) {
            var value = src[name];

            if (!src.hasOwnProperty(name) || typeof value == 'function' || 
                name.indexOf('_') === 0)
                continue;

            props.add(name, new ModelProperty(typeof value, name, name));
        }

        return props;
    }
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// String to type parsers
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
export interface TypeParser {
    parse(value: string, prop?: ModelProperty): any;
}

class IntegerParser implements TypeParser {
    parse(value: string, prop?: ModelProperty): number {
        return parseInt(value);
    }
}

class NumberParser implements TypeParser {
    parse(value: string, prop?: ModelProperty): number {
        return parseFloat(value);
    }
}

class BooleanParser implements TypeParser {
    parse(value: string, prop?: ModelProperty): boolean {
        return value === "true" ? true : false;
    }
}

class StringParser implements TypeParser {
    parse(value: string, prop?: ModelProperty): string {
        return value;
    }
}

class ObjectParser implements TypeParser {
    parse(value: string | object, prop?: ModelProperty): object {
        try {
            return JSON.parse(value as string);
        }
        catch (e) {
            return value as object;
        }
    }
}

class ObjectIDParser implements TypeParser {
    parse(value: string, prop?: ModelProperty): ObjectID {
        return ObjectID.create(value);
    }
}

class DateParser implements TypeParser {
    parse(value: string, prop?: ModelProperty): Date {
        return new Date(value);
    }
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// ModelFactory: Encapsulates all the models within the system
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
export class ModelFactory {
    models              : any = {};              /// The models within the system
    parsers             : any = {};              /// The parsers available within the system
    modelProperties     : any = {};              /// The model properties of each of the objects

    constructor() {
        this.parsers["integer"]     = new NumberParser();
        this.parsers["number"]      = new NumberParser();
        this.parsers["Number"]      = new NumberParser();
        this.parsers["String"]      = new StringParser();
        this.parsers["string"]      = new StringParser();
        this.parsers["ObjectID"]    = new ObjectIDParser();
        this.parsers["Object"]      = new ObjectParser();
        this.parsers["object"]      = new ObjectParser();
        this.parsers["any"]         = new ObjectParser();
        this.parsers["Date"]        = new DateParser();
        this.parsers["Boolean"]     = new BooleanParser();
        this.parsers["boolean"]     = new BooleanParser();
        this.parsers["bool"]        = new BooleanParser();

        this.models['array']        = Array;
        this.models['Array']        = Array;
    }

    getClass(classId: string): any {
        return this.models[classId];
    }

    isClassRegistered(classId: string): boolean {
        let cls = this.getClass(classId);
        return !!cls;
    }

    getClassPropertiesFromName(classId: string): ModelProperties {
        return this.modelProperties[name];
    }

    getClassProperties(cls: any): ModelProperties {
        let name = cls.__name || cls.__classId;
        return this.modelProperties[name];
    }

    register(classId: string, cls: any): ModelProperties {
        let mprops: ModelProperties = this.modelProperties[classId];

        if (!mprops) {
            console.info(`- Class => ${classId}`);

            let parentClass = Object.getPrototypeOf(cls);
            mprops = new ModelProperties(cls);

            if (parentClass) {
                let parentMProps = this.getClassProperties(parentClass);
                if (parentMProps) 
                    mprops.copyFrom(parentMProps);
            }

            this.models[classId] = cls;
            this.parsers[classId] = cls;
            this.modelProperties[classId] = mprops;

            // if (cls.__classId && cls.__classId !== classId) {
            //     this.models[cls.__classId] = cls;
            //     this.parsers[cls.__classId] = cls;
            //     this.modelProperties[cls.__classId] = mprops;
            // }
        }

        return mprops;
    }

    getParser(type: string): TypeParser {
        return this.parsers[type];
    }
}

export let Model = new ModelFactory();
