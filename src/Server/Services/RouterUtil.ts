import { ShUtil, Debug, PromiseUtil } from 'Shared/Core/ShUtil';
import TaskHandler from 'Server/Core/Task/TaskHandler'
import { ServerArgs } from '../Core/ServerArgs';
import { ApiPermissions, ApiPermissionsCallbacks } from './Router';
import { Routes } from 'Server/Core/SvCore';
import 'reflect-metadata';
import { Model } from 'Shared/Model/ShModel';
import * as express from 'express';

// return {
//     "name": name,
//     "description": description,
//     "type": type,
//     "required": required,
//     "enum": allowableValuesEnum,
//     "defaultValue": defaultValue,
//     "paramType": "query",
//     "isUserClass": Model.getClass(type) ? true : false
// };


export namespace Rest {
    export class Param {
        name                : string = "";
        desc                : string = "";
        type                : string = "";
        required            : boolean = true;
        nested              : boolean = false;
        paramType           : string = "query";
        isUserContainerClass: boolean = false;
        enumValues          : string[] = [];
    }
    
    export class Spec {
        type                : string = "";
        content             : string = "application/json";
        parameters          : Param[] = [];
        hasUserContainerClasses: boolean = false;
    }

    export class PermissionsInfo {
        permission          : ApiPermissions = ApiPermissions.none;
        or                  : PermissionsInfo[] = [];
        and                 : PermissionsInfo[] = [];

        constructor(permission: ApiPermissions = ApiPermissions.login) {
            this.permission = permission;
        }

        static getPermissionsCallback(permission: ApiPermissions): any {
            let permissionCallback = ApiPermissionsCallbacks[permission];
            Debug.assert(permissionCallback && typeof permissionCallback == 'function', `[Router] Invalid permissions: ${permission}`);
            return permissionCallback;
        }

        static runPemmissions(args: ServerArgs, permission: ApiPermissions): Promise<boolean> {
            if (permission === ApiPermissions.none)
                return PromiseUtil.resolvedPromise(true);

            let permissionCallback: ((args: ServerArgs) => Promise<boolean>) = PermissionsInfo.getPermissionsCallback(permission);
            return permissionCallback(args);
        }

        runOr(args: ServerArgs): Promise<boolean> {
            if (!this.or.length)
                return PromiseUtil.resolvedPromise(true);
                
            let promises: Promise<boolean>[] = [];
            this.or.map((pinfo: PermissionsInfo) => promises.push(pinfo.run(args)));
            return (Promise as any).any(promises);
        }

        runAnd(args: ServerArgs): Promise<boolean> {
            if (!this.and.length)
                return PromiseUtil.resolvedPromise(true);

            let promises: Promise<boolean>[] = [];
            this.and.map((pinfo: PermissionsInfo) => promises.push(pinfo.run(args)));
            return (Promise as any).each(promises);
        }

        run(args: ServerArgs): Promise<any> {
            let promises = [];
            if (this.permission)
                promises.push(PermissionsInfo.runPemmissions(args, this.permission));
            if (this.or.length)
                promises.push(this.runOr(args));
            if (this.and.length)
                promises.push(this.runAnd(args));
    
            if (promises.length == 0)
                return PromiseUtil.resolvedPromise(true);
            else if (promises.length == 1) 
                return promises[0];
            
            return Promise.all(promises);
        }
    }
    
    export class Info {
        method              : string = "";
        path                : string = "";
        desc                : string = "";
        permissions         : PermissionsInfo = new PermissionsInfo();
        spec                : Spec = new Spec();

        middleWares         : ((req: express.Request, res: express.Response, next: any) => void)[] = [];

        __params            : any = {};
    }

    function createInfo(target: any) {
        let ctor = target;
        if (!ctor.info)
            ctor.info = new Info();
    }

    export function Def(method: string, path: string, desc: string) {
        Debug.assert(desc, "[Router] Invalid description for rest end-point!");
        Debug.assert(path, "[Router] Invalid path for rest end-point!");
        
        return function (target: any) {
            createInfo(target);

            let ctor = target;
            ctor.info.method    = method;
            ctor.info.path      = path;
            ctor.info.desc      = desc;

            // eido.appServer[method](target);
            Routes.add(target);
        }
    }

    export function MiddleWare(middleware: (req: express.Request, res: express.Response, next: any) => void) {
        return function (target: any) {
            createInfo(target);
            let ctor = target;
            let info: Info = ctor.info;
            info.middleWares.push(middleware);
        }
    }

    function ensureLoggedIn(options) {
        if (typeof options == 'string') {
            options = { redirectTo: options }
        }
        options = options || {};

        var url = options.redirectTo || '/login';
        var setReturnTo = (options.setReturnTo === undefined) ? true : options.setReturnTo;

        return function (req, res, next) {
            if (!req.isAuthenticated || !req.isAuthenticated()) {
                if (setReturnTo && req.session) {
                    req.session.returnTo = req.originalUrl || req.url;
                }
                // return res.redirect(url);
                return res.status(403).send({});
            }
            next();
        }
    }

    export function LoggedIn() {
        // let ensureLoggedIn: any = require('connect-ensure-login').ensureLoggedIn;
        return MiddleWare(ensureLoggedIn('/dk/login'));
    }

    export function Permissions(permissions: ApiPermissions | ApiPermissions[]) {
        return function (target: any) {
            createInfo(target);
            
            let ctor = target;
            let info: Info = ctor.info;

            if (permissions instanceof Array) {
                permissions.map((permission) => info.permissions.or.push(new PermissionsInfo(permission)));
            }
            else 
                info.permissions.permission = permissions;
        }
    }

    export function PermissionsEx(permissions: PermissionsInfo) {
        return function (target: any) {
            createInfo(target);
            
            let ctor = target;
            ctor.info.permissions = permissions;
        }
    }

    export function Content(content: string) {
        return function (target: any) {
            createInfo(target);
            let info: Info = target.info;
            info.spec.content = content;
        }
    }

    export function Returns<T>(Class: T | string) {
        return function (target: any) {
            createInfo(target);
            let info: Info = target.info;

            if (Class instanceof String)
                info.spec.type = Class.toString();
            else
                info.spec.type = Class["__classId"];
        }
    }

    function ParamDef(paramType: string, desc: string, EnumClass: any, required: boolean) {
        Debug.assert(desc, "[Router] Invalid description for field!");
        
        return function (target: any, propertyKey: string) {
            let Class = target.constructor;
            createInfo(Class);

            var targetType = (Reflect as any).getMetadata("design:type", target, propertyKey);
            let param: Param = new Param();

            param.name      = propertyKey;
            param.type      = targetType.name;
            param.desc      = desc;
            param.required  = required;
            param.isUserContainerClass = Model.getClass(targetType) ? true : false;
            param.paramType = paramType;

            if (EnumClass) {
                param["enum"] = Object.keys(EnumClass);
            }

            let info: Info  = Class.info;
            info.spec.parameters.push(param);
        }
    }

    export function Query(desc: string, EnumClass: any = null, required: boolean = true) {
        return ParamDef('query', desc, EnumClass, required);
    }

    export function Body(desc: string, EnumClass: any = null, required: boolean = true) {
        return ParamDef('body', desc, EnumClass, required);
    }

    export function Get(path: string, desc: string) {
        return Def("get", path, desc);
    }
    
    export function Put(path: string, desc: string) {
        return Def("put", path, desc);
    }
    
    export function Post(path: string, desc: string) {
        return Def("post", path, desc);
    }
    
    export function Patch(path: string, desc: string) {
        return Def("patch", path, desc);
    }
    
    export function Delete(path: string, desc: string) {
        return Def("delete", path, desc);
    }
    
    export function WS(path: string, desc: string) {
        return Def("ws", path, desc);
    }
    
    export class EndPoint extends TaskHandler {
        static info         : Info = null;
    
        constructor() { 
            super(); 
        }

        get args() : ServerArgs { return this as ServerArgs; } 
    
        preValidate() { 
            return PromiseUtil.resolvedPromise(true); 
        }
    
        validate() {
            return PromiseUtil.resolvedPromise(true);
        }
    
        postValidate() { 
            return PromiseUtil.resolvedPromise(true); 
        }
    
        action() { 
            return ShUtil.noImpl(); 
        }
    
        user() { 
            return this.args._cached.user; 
        }
    
        userId() { 
            return this.args._cached.user._id; 
        }
    }
}    
