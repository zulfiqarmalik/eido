import { Rest } from "Server/Services/RouterUtil";
import { ApiPermissions } from '../Router';
import { Trade, Wave } from "Server/Model/Trade/SvTrade";
import { TradeType } from "Shared/Model/Trade/ShTrade";

@Rest.Get('[apiRoot][apiVersion]/wave', "Get a specific wave for a trade.")
@Rest.LoggedIn()
@Rest.Permissions([ApiPermissions.cio])
@Rest.Returns(Trade)
class TradeGet extends Rest.EndPoint {
    @Rest.Query("The wave id")
    waveId: string = "";

    action() {
        // let trade: Trade = null;

        return (new Wave()).load(this.waveId)
            .then((wave: Wave) => {
                wave.sendClient(this, 200);
            });
    }
}
