import { Rest } from "Server/Services/RouterUtil";
import { ApiPermissions } from '../Router';
import { Trade } from "Server/Model/Trade/SvTrade";
import { TradeType } from "Shared/Model/Trade/ShTrade";

@Rest.Get('[apiRoot][apiVersion]/trade', "Get the current trade for a region.")
@Rest.LoggedIn()
@Rest.Permissions([ApiPermissions.cio])
@Rest.Returns(Trade)
class TradeGet extends Rest.EndPoint {
    @Rest.Query("Market Name")
    market: string = "";

    @Rest.Query("Trade type", TradeType)
    type: TradeType = TradeType.UAT;

    @Rest.Query("Date", null, false)
    date: Date = null;

    action() {
        // let trade: Trade = null;

        return (new Trade()).load(this.market, this.type, this.date)
            .then((trade: Trade) => {
                trade.sendClient(this, 200);
            });
    }
}
