import { eido } from 'Shared/Core/ShCore';
import initDataCache from "./DataCache";
import "./Data_GetClassification";
import "./Data_GetPrices";
import "./Data_GetEarningsInfo";
import "./Data_GetSecMaster";
import "./Data_GetFloat";
import "./Data_GetCheckpoint";
// import "./Data_GetGroupPnl";

export default function initRouter(app) {
	initDataCache(app);
};
