import { Debug } from 'Shared/Core/ShUtil';
import { Rest } from "Server/Services/RouterUtil";
import { DalAPI } from "./DalAPI";
import { GroupInfo } from 'Shared/Model/GroupPnl';
import { FloatData, Universe, SecurityMaster } from './DataCache';
import { eido } from 'Shared/Core/ShCore';
import { CObject } from 'Shared/Core/CObject';

@Rest.Post('[apiRoot][apiVersion]/dataAPI/getFloat', "Get floating point data for a range of dates.")
@Rest.LoggedIn()
@Rest.Returns(GroupInfo)
class Data_GetPrices extends DalAPI {
    @Rest.Query("Which market to get the data for.")
    market: string = "";
    
    @Rest.Query("[optional] Which market to get the data for.")
    universeId: string = "";
    
    @Rest.Query("The starting date of the data (inclusive). 0 means first date.")
    startDate: number = 0;
    
    @Rest.Query("The ending date for the data (inclusive). 0 means last date.")
    endDate: number = 0;
    
    @Rest.Query("Array of figis to fetch.")
    figis: string;
    
    @Rest.Query("A list of datasets to fetch.")
    dataIds: string;

    _diStart: number = -1;
    _diEnd: number = -1;
    _figis: string[];
    _ii: number[];
    _universe: Universe | SecurityMaster;
    _dataIds: string[];
    _data: FloatData[];

    validate() {
        this._figis = this.figis.split(',');
        this._dataIds = this.dataIds.split(',');

        this._error.assert(this.universeId, 400, "Invalid universeId specified");
        this._error.assert(this._dataIds.length, 400, "Invalid number of dataIds sent for retrieval");
        this._error.assert(this.figis.length, 400, "Invalid number of figis sent for retrieval");
        this._error.assert(this.startDate <= this.endDate, 400, `Invalid startDate: ${this.startDate} or endDate: ${this.endDate}`);

        this._market = eido.dataCache.getMarket(this.market);
        this._error.assert(this._market, 400, `Unable to fetch market: ${this.market}`);

        this._universe = this._market.getUniverse(this.universeId);
        this._error.assert(this._universe, 400, `Unable to fetch universe: ${this.universeId} from market: ${this.market}`);

        this._data = new Array<FloatData>(this._dataIds.length);

        this._dataIds.map((dataId, i) => {
            let data: FloatData = this._market.getFloatData(dataId, this.universeId);
            this._error.assert(data, 400, `Unable to load data source: ${dataId}`);

            this._data[i] = data;
        });

        this._ii = new Array<number>(this._figis.length);
        this._figis.map((figi, index) => {
            let ii: number = this._universe.getIndex(figi);
            this._error.assert(ii >= 0, 400, `Unable to find figi: ${figi}`);
            this._ii[index] = ii;
        });

        if (this._diStart < 0)
            this._diStart = this._data[0].getDateIndex(this.startDate, true);
        if (this._diEnd < 0)
            this._diEnd = this._data[0].getDateIndex(this.endDate, true);

        this._error.assert(this._diStart <= this._diEnd && this._diStart >= 0 && this._diEnd >= 0, 400,
            `Unable to map start and/or end date properly. Start Date: ${this.startDate} [di = ${this._diStart}] - End Date: ${this.endDate} [di = ${this._diEnd}]`);

        console.log(`[Data] Getting data in range: [${this.startDate}, ${this.endDate}] = [${this._diStart}, ${this._diEnd}]`);
    }

    action() {
        let results = {
            dates: new Array<number>(this._diEnd - this._diStart + 1),
            data: {}
        };

        for (let di: number = this._diStart; di <= this._diEnd; di++) {
            let date: number = this._data[0].getIndexDate(di);
            results.dates[di - this._diStart] = date;

            this._ii.map((ii, figiIndex) => {
                let figi: string = this._figis[figiIndex];
                let figiData = results.data[figi] || {};
                
                figiData.figi = figi;
                figiData.values = figiData.values || {};

                this._data.map((data, dataIndex) => {
                    let dataId = this._dataIds[dataIndex];
                    let value: number = data.getValue(di, ii);
                    let figiDataValues = figiData.values[dataId] || [];

                    figiDataValues.push(value);

                    figiData.values[dataId] = figiDataValues;
                });

                results.data[figi] = figiData;
            });
        }

        CObject.sendClientRaw(this as any, 200, results);
    }
}
