import { Debug } from 'Shared/Core/ShUtil';
import { Rest } from "Server/Services/RouterUtil";
import { DalAPI } from "./DalAPI";
import { GroupInfo } from 'Shared/Model/GroupPnl';
import { FloatData } from './DataCache';

@Rest.Post('[apiRoot][apiVersion]/dataAPI/getPrices', "Get prices for a list of FIGIs.")
@Rest.LoggedIn()
@Rest.Returns(GroupInfo)
class Data_GetPrices extends DalAPI {
    @Rest.Query("The FIGIs to fetch.")
    figis: string = "";
    
    @Rest.Query("The date for which the data is required.")
    date: number = 0;
    
    @Rest.Query("Which market to get the data for.")
    market: string = "";
    
    @Rest.Query("[optional] Which market to get the data for.")
    universeId: string = "";
    
    @Rest.Query("[optional] Which field to get.")
    dataId: string = "";

    _data: FloatData;

    validate() {
        super.validate();

        this._data = this._market.getFloatData(this.dataId, this.universeId);
        Debug.assert(this._data, `Unable to load data: ${this.dataId}`);
    }
}
