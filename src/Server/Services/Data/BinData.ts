import { eido } from 'Shared/Core/ShCore';
import { Debug, ShUtil } from 'Shared/Core/ShUtil';
import * as path from 'path';
let BinaryParser = require('binary-parser').Parser;
import { CacheInfo } from '../../App/ServerConfig';
import { ShAlpha, AssetClass } from 'Shared/Model/Config/ShAlpha';
import { Sec, SecMaster } from 'Shared/Model/ShData';

export let Size = new BinaryParser()
    .endianess('little')
    .uint32("hi")
    .uint32("lo")
    ;

export let DataDefinition = new BinaryParser()
    .endianess('little')
    .nest("version", { type: Size } )
    .uint32("tmp")
    .string("name", { length: 256, stripNull: true })
    .int32("itemType")
    .nest("itemSize", { type: Size })
    .uint32("frequency")
    .uint32("flags")
    .string("typeName", { length: 256, stripNull: true })
    .uint32("padding2")
    ;

export function fixDataDefinition(def) {
    def.version = def.version.lo;
}

export let MetadataItem = new BinaryParser()
    .endianess('little')
    .string("key", { length: 8, stripNull: true })
    .string("value", { length: 16, stripNull: true })
    ;

export let Metadata = new BinaryParser()
    .endianess('little')
    .array("items", { type: MetadataItem, length: 16 })
    .nest("numItems", { type: Size })
    ;

export let DataHeader = new BinaryParser()
    .endianess('little')
    .nest("version", { type: Size })
    .string("loaderId", { length: 128, stripNull: true })
    .uint32("startDate")
    .uint32("lastValidDataDate")
    .uint32("endDate")
    .uint32("updateDate")
    .uint32("updateTime")
    .nest("rowSize", { type: Size })
    .nest("numRows", { type: Size })
    .nest("metadata", { type: Metadata })
    .nest("def", { type: DataDefinition })
    ;

export let szDataHeader = 1112;

function fixDataHeader(hdr) {
    hdr.version = hdr.version.hi;
    hdr.rowSize = hdr.rowSize.lo;
    hdr.numRows = hdr.numRows.lo;

    fixDataDefinition(hdr.def);
}

export let DataRowHeader = new BinaryParser()
    .endianess('little')
    .uint32("date")
    .nest("size", { type: Size })
    .nest("cols", { type: Size })
    .nest("depth", { type: Size })
    .nest("writtenBytes", { type: Size })
    .uint32("padding")
    ;

export let szDataRowHeader = 40;

export let IntDataRow = new BinaryParser()
    .nest("rhdr", { type: DataRowHeader })
    .array("cols", {
        type: 'int32le',
        length: function () {
            return this.rhdr.size.lo / 4; /// 520;
        }
    })
    ;

//////////////////////////////////////////////////////////////////////////////////
/// FloatMatrixData
//////////////////////////////////////////////////////////////////////////////////
export let FloatDataRow = new BinaryParser()
    .endianess('little')
    .nest("rhdr", { type: DataRowHeader })
    .array("cols", {
        type: 'floatle',
        length: function () {
            return this.rhdr.size.lo / 4; /// 520;
        }
    })
    ;

export let FloatDataMatrix = new BinaryParser()
    .nest("header", { type: DataHeader })
    .array("rows", {
        type: FloatDataRow,
        length: function () {
            return this.header.numRows.lo;
        }
    })
    ;

//////////////////////////////////////////////////////////////////////////////////
/// UInt16MatrixData
//////////////////////////////////////////////////////////////////////////////////
export let UInt16DataRow = new BinaryParser()
    .endianess('little')
    .nest("rhdr", { type: DataRowHeader })
    .array("cols", {
        type: 'uint16le',
        length: function () {
            return this.rhdr.size.lo / 2; /// 520;
        }
    })
    ;

export let UInt16DataMatrix = new BinaryParser()
    .nest("header", { type: DataHeader })
    .array("rows", {
        type: UInt16DataRow,
        length: function () {
            return this.header.numRows.lo;
        }
    })
    ;

//////////////////////////////////////////////////////////////////////////////////
/// UInt16StaticDataMatrix
//////////////////////////////////////////////////////////////////////////////////
export let UInt16StaticDataRow = new BinaryParser()
    .endianess('little')
    .array("cols", {
        type: 'uint16le',
        readUntil: 'eof'                
    })
    ;

export let UInt16StaticDataMatrix = new BinaryParser()
    .nest("header", { type: DataHeader })
    .array("rows", {
        type: UInt16StaticDataRow,
        length: function () {
            return this.header.numRows.lo;
        }
    })
    ;

//////////////////////////////////////////////////////////////////////////////////
/// UIntMatrixData
//////////////////////////////////////////////////////////////////////////////////
export let UIntDataRow = new BinaryParser()
    .endianess('little')
    .nest("rhdr", { type: DataRowHeader })
    .array("cols", {
        type: 'uint32le',
        length: function () {
            return this.rhdr.size.lo / 4; /// 520;
        }
    })
    ;

export let UIntDataMatrix = new BinaryParser()
    .nest("header", { type: DataHeader })
    .array("rows", {
        type: UIntDataRow,
        length: function () {
            return this.header.numRows.lo;
        }
    })
    ;

//////////////////////////////////////////////////////////////////////////////////
/// UIntUniversalData
//////////////////////////////////////////////////////////////////////////////////
export let UIntStaticDataRow = new BinaryParser()
    .endianess('little')
    .array("cols", {
        type: 'uint32le',
        readUntil: 'eof'                
    })
    ;

export let UIntStaticDataMatrix = new BinaryParser()
    .nest("header", { type: DataHeader })
    .array("rows", {
        type: UIntStaticDataRow,
        length: function () {
            return this.header.numRows.lo;
        }
    })
    ;

//////////////////////////////////////////////////////////////////////////////////
/// StringData and variants
//////////////////////////////////////////////////////////////////////////////////
export let StringValue = new BinaryParser()
    .endianess('little')
    .nest("length", { type: Size })
    .string("value", {
        length: function() {
            return this.length.lo || this.length.hi;
        }
    })
    ;

export let StringDataRow = new BinaryParser()
    .endianess('little')
    // .nest("rhdr", { type: DataRowHeader })
    .nest("numRows", { type: Size })
    .nest("numCols", { type: Size })
    .nest("depth", { type: Size })
    .nest("vecSize", { type: Size })
    .array("cols", {
        type: StringValue,
        length: function() {
            return this.vecSize.hi;
            // return this.rhdr.depth.lo;
        }
    })
    ;

export let StringDataData = new BinaryParser()
    .nest("header", { type: DataHeader })
    .array("rows", {
        type: StringDataRow,
        length: function () {
            return this.header.numRows.lo;
        }
    })
    ;

export let StringDailyDataRow = new BinaryParser()
    .endianess('little')
    // .nest("rhdr", { type: DataRowHeader })
    .nest("vecSize", { type: Size })
    .array("cols", {
        type: StringValue,
        length: function() {
            return this.vecSize.hi;
        }
    })
    ;

export let StringDailyData = new BinaryParser()
    .nest("header", { type: DataHeader })
    .array("rows", {
        type: StringDailyDataRow,
        length: function () {
            return this.header.numRows.lo;
        }
    })
    ;

const   s_maxSymbol         = 32;
const   s_maxName           = 128;
const   s_maxUuid           = 24;
const   s_maxIssueType      = 32;
const   s_maxIssueClass     = 32;
const   s_maxCcy            = 4;

export let Security = new BinaryParser()
    .endianess('little')
    .nest("version", { type: Size })
    .uint32("index")
    .string("uuid", { stripNull: true, length: s_maxUuid })
    .string("parentUuid", { stripNull: true, length: s_maxUuid })
    .string("name", { stripNull: true, length: s_maxName })
    .uint32("uuidType")

    .string("bbTicker", { stripNull: true, length: s_maxSymbol })
    .string("ticker", { stripNull: true, length: s_maxSymbol })
    .string("exchange", { stripNull: true, length: s_maxSymbol })
    .string("instrument", { stripNull: true, length: s_maxSymbol })

    .string("cfigi", { stripNull: true, length: s_maxUuid })
    .string("isin", { stripNull: true, length: s_maxUuid })
    .string("figi", { stripNull: true, length: s_maxUuid })
    .string("perfId", { stripNull: true, length: s_maxUuid })

    .string("permId", { stripNull: true, length: s_maxUuid })
    .string("issuerPermId", { stripNull: true, length: s_maxUuid })
    .string("quotePermId", { stripNull: true, length: s_maxUuid })
    .string("ric", { stripNull: true, length: s_maxUuid })

    .string("issueClass", { stripNull: true, length: s_maxIssueClass })
    .string("issueType", { stripNull: true, length: s_maxIssueType })

    .string("currency", { stripNull: true, length: s_maxCcy })
    .string("countryCode2", { stripNull: true, length: 3 })
    .string("countryCode3", { stripNull: true, length: 4 })

    .uint8("isInactive")
    .uint32("ipoDate")
    .uint32("delistingDate")
    
    .uint32("padding")
    ;

////////////////////////////////////////////////////////////////////////////////////////////////////////
/// SecurityMaster
////////////////////////////////////////////////////////////////////////////////////////////////////////
export let SecMasterData = new BinaryParser()
    .endianess('little')
    .nest("header", { type: DataHeader })
    .array("securities", {
        type: Security,
        length: function() {
            return this.header.rowSize.lo / 600;
        }
    })
    ;

////////////////////////////////////////////////////////////////////////////////////////////////////////
/// CheckpointInfo
////////////////////////////////////////////////////////////////////////////////////////////////////////
export let CheckpointInfoData = new BinaryParser()
    .endianess('little')
    .uint32("version")
    .nest("timestamp", { type: Size })
    .uint32("di")
    ;

////////////////////////////////////////////////////////////////////////////////////////////////////////
/// Statistics
////////////////////////////////////////////////////////////////////////////////////////////////////////
export let StatisticsData = new BinaryParser()
    .endianess('little')
    .uint32('date')
    .floatle('pnl')
    .floatle('tvr')
    .floatle('longPnl')
    .floatle('shortPnl')
    .floatle('bookSize')
    .floatle('tradedNotional')
    .floatle('tradedShares')
    .floatle('tradingPnl')
    .floatle('shortNotional')
    .floatle('longNotional')
    .floatle('liquidateNotional')
    .floatle('longCount')
    .floatle('shortCount')
    .floatle('liquidateCount')
    .floatle('returns')
    .floatle('cost')
    .floatle('marginBps')

    ;

////////////////////////////////////////////////////////////////////////////////////////////////////////
/// CumStatistics
////////////////////////////////////////////////////////////////////////////////////////////////////////
export let CumStatisticsData = new BinaryParser()
    .endianess('little')
    .nest('st', { type: StatisticsData })
    .uint32('numDays')
    .uint32('numWon')
    .floatle('ddMaxPnl')
    .floatle('ddMinPnl')
    .floatle('ir')
    .floatle('hitRatio')
    .floatle('maxTvr')

    .floatle('maxDD')
    .floatle('dd')
    .uint32('dh')
    .uint32('maxDH')

    .floatle('maxDU')
    .floatle('du')
    .floatle('duh')
    .floatle('maxDUH')

    .floatle('volatility')
    .floatle('pnlMean')
    .floatle('pnlStdDev')
    .floatle('squaredPnl')

    .floatle('pnlSkew')
    .floatle('pnlKurtosis')
    .floatle('rsquared')

    .floatle('pnlNegMean')
    .floatle('squaredNegPnl')
    .floatle('pnlNegStdDev')

    .floatle('sortinoRatio')
    .floatle('calmar')

    .floatle('minReturns')
    .floatle('maxReturns')
    ;

////////////////////////////////////////////////////////////////////////////////////////////////////////
/// RangeStatistics
////////////////////////////////////////////////////////////////////////////////////////////////////////
export let RangeStatisticsData = new BinaryParser()
    .endianess('little')
    .uint32('startDate')
    .uint32('endDate')
    .nest('cumStats', { type: CumStatisticsData })
    .nest('maxLimit', { type: Size })
    .nest('vecSize', { type: Size })
    .array('vec', { 
        type: StatisticsData,
        length: function() {
            return this.vecSize.lo || this.vecSize.hi;
        }
    })
    ;

////////////////////////////////////////////////////////////////////////////////////////////////////////
/// LTStats
////////////////////////////////////////////////////////////////////////////////////////////////////////
export let LTStatsData = new BinaryParser()
    .endianess('little')
    .uint32('version')
    .nest('rs', { type: RangeStatisticsData })
    ;

////////////////////////////////////////////////////////////////////////////////////////////////////////
/// CalcData
////////////////////////////////////////////////////////////////////////////////////////////////////////
export let CalcDataData = new BinaryParser()
    .endianess('little')
    .nest('rows', { type: Size })
    .nest('cols', { type: Size })

    .nest('alphaDataSize', { type: Size })

    .array('alphaData', {
        type: 'floatle',
        length: function () {
            return (this.rows.lo || this.rows.hi) * (this.cols.lo || this.cols.hi);
        }
    })

    .nest('cumStats', { type: LTStatsData })
    ;

////////////////////////////////////////////////////////////////////////////////////////////////////////
/// AlphaData
////////////////////////////////////////////////////////////////////////////////////////////////////////
export let AlphaDataData = new BinaryParser()
    .endianess('little')
    .nest("uuid", { type: StringValue })
    .nest("calcData", { type: CalcDataData })
    ;
    
////////////////////////////////////////////////////////////////////////////////////////////////////////
/// AlphaData
////////////////////////////////////////////////////////////////////////////////////////////////////////
export let FloatArray = new BinaryParser()
    .endianess('little')
    .uint32("length")
    .array('data', {
        type: 'floatle',
        length: function () {
            return this.length;
        }
    });


export let PosData = new BinaryParser() 
    .endianess('little')
    .nest('posNotionals', { type: FloatArray })
    .nest('posPrices', { type: FloatArray })
    .nest('posShares', { type: FloatArray })
    .nest('pssNotionals', { type: FloatArray })
    .nest('pssClosePnls', { type: FloatArray })
    .nest('pssTurnovers', { type: FloatArray })
    .nest('pssCosts', { type: FloatArray })
    ;
