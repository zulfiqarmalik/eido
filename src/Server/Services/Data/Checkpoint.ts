import { ObjectID } from 'Shared/Core/ObjectID';
import { CheckpointInfoData, AlphaDataData } from './BinData';
import { ServerConfig } from 'Server/App/ServerConfig';
import { Util } from 'Server/Core/SvUtil';

export enum CheckpointType {
    alpha           = "alpha",
    portfolio       = "port",
    rawPortfolio    = "raw_port",
    preOptPortfolio = "pre_opt_port"
};


export class Checkpoint {
    _id             : ObjectID;
    type            : CheckpointType;
    alphaData       : any = null;

    constructor(id: ObjectID, type: CheckpointType) {
        this._id    = id;
        this.type   = type;
    }

    loadAlphaData(alphaDataBuffer: Buffer): Checkpoint {
        this.alphaData = AlphaDataData.parse(alphaDataBuffer);
        return this;
    }

    loadInfoBuffer(infoBuffer: Buffer): Checkpoint {
        let info    = CheckpointInfoData.parse(infoBuffer);
        console.log(`[Checkpoint] Checkpoint version: ${info.version}`);
        console.log(`[Checkpoint] Checkpoint di: ${info.di}`);
        return this;
    }

    static loadInfo(sconfig: ServerConfig, id: ObjectID, type: string): Promise<Buffer> {
        console.log(`[Checkpoint] Loading info: ${id}`);
        let suffix: string = type ? `_${type}` : "";
        return Util.getIdFileData(sconfig.checkpointContainerId, id, suffix);
    }

    static load(sconfig: ServerConfig, id: ObjectID, type: CheckpointType) {
        console.log(`[Checkpoint] Getting checkpoint: ${id}_${type}`);

        let cp: Checkpoint = new Checkpoint(id, type);

        return Checkpoint.loadInfo(sconfig, id, "")
            .then((info: Buffer) => {
                cp.loadInfoBuffer(info);
                return Checkpoint.loadInfo(sconfig, id, type);
            })
            .then((buffer: Buffer) => cp.loadAlphaData(buffer))
    }
}
