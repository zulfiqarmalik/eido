import { eido } from 'Shared/Core/ShCore';
import { Debug, PromiseUtil, ShUtil } from 'Shared/Core/ShUtil';
import { Rest } from "Server/Services/RouterUtil";
import { DalAPI } from "./DalAPI";
import { Params, Collections } from "Server/Model/SvModel";
import { Model } from 'Shared/Model/ShModel';
import { CObject } from 'Shared/Core/CObject';
import { StringData, Universe } from './DataCache';

@Rest.Post('[apiRoot][apiVersion]/dataAPI/getClassification', "Get classification of a list of FIGIs.")
@Rest.LoggedIn()
@Rest.Returns(Object)
class Data_GetClassification extends DalAPI {
    @Rest.Query("The FIGIs to fetch.")
    figis: string = "";
    
    @Rest.Query("Which market to get the data for.")
    market: string = "";
    
    @Rest.Query("[optional] Which market to get the data for.")
    universeId: string = "";
    
    @Rest.Query("[optional] Which field to get.")
    dataId: string = "";

    _data: StringData = null;
        
    validate() {
        super.validate();

        this._data = this._market.getStringData(this.dataId, this.universeId);
        this.date = 0;
        Debug.assert(this._data, `Unable to load data: ${this.dataId}`);
    }
}

@Rest.Post('[apiRoot][apiVersion]/dataAPI/getClassificationForUniverse', "Get classification for a universe.")
@Rest.LoggedIn()
@Rest.Returns(Object)
class Data_GetClassificationForUniverse extends DalAPI {
    @Rest.Query("Which market to get the data for.")
    market: string = "";
    
    @Rest.Query("[optional] Which market to get the data for.")
    universeId: string = "";
    
    @Rest.Query("[optional] Which field to get.")
    dataId: string = "";

    _data: StringData = null;
    _universe: Universe = null

    validate() {
        super.validate();

        console.info(`[DataCache] Loading classifications: ${this.universeId}.${this.dataId}`)

        this._data = this._market.getStringData(this.dataId, this.universeId);
        this.date = 0;
        Debug.assert(this._data, `Unable to load data: ${this.dataId}`);
    }

    action() {
        let uuids = this._universe.getAllFigis();
        let data = this._data.getDataForFigis(0, uuids);

        Debug.assert(uuids.length == data.length, `Data and figi lengths do not match! Num figis: ${uuids.length} - Num data points: ${data.length}`);
        let dataMap = {};
        uuids.map((uuid, index) => dataMap[uuid] = data[index]);

        CObject.sendClientRaw(this, 200, dataMap);
    }
}
