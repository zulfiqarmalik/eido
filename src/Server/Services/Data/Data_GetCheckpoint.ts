import { eido } from 'Shared/Core/ShCore';
import { Debug, PromiseUtil, ShUtil } from 'Shared/Core/ShUtil';
import { Rest } from "Server/Services/RouterUtil";
import { DalAPI } from "./DalAPI";
import { ApiPermissions } from '../Router';
import { SecMaster } from 'Shared/Model/ShData';
import { SecrecyLevel } from 'Shared/Model/SecrecyLevel';
import { ObjectID } from 'Shared/Core/ObjectID';
import { CObject } from 'Shared/Core/CObject';
import { db } from 'Server/Core/DB/DBManager';
import { CheckpointType, Checkpoint } from './Checkpoint';
import { AssetClass } from 'Shared/Model/Config/ShAlpha';

@Rest.Get('[apiRoot][apiVersion]/dataAPI/getCheckpoint', "Get the checkpoint of a certain data.")
@Rest.LoggedIn()
@Rest.Permissions([ApiPermissions.portfolioManager])
@Rest.Returns(Object)
class Data_GetCheckpoint extends Rest.EndPoint {
    @Rest.Query('The checkpoint id')
    id              : ObjectID = null;
    
    @Rest.Query('The type of the checkpoint')
    type            : CheckpointType = CheckpointType.alpha;
    
    @Rest.Query('The db from which to load the checkpoint')
    dbName          : string = "pesa_us";
    
    action() {
        return Checkpoint.load(this._appServer.config(AssetClass.Equities), this.id, this.type);
    }
}
