import { eido } from 'Shared/Core/ShCore';
import { Debug, PromiseUtil, ShUtil } from 'Shared/Core/ShUtil';
import { Rest } from "Server/Services/RouterUtil";
import { DalAPI } from "./DalAPI";
import { Params, Collections } from "Server/Model/SvModel";
import { Model } from 'Shared/Model/ShModel';
import { CObject } from 'Shared/Core/CObject';
import { StringData, Universe, Market } from './DataCache';
import { ApiPermissions } from '../Router';
import { SecMaster } from 'Shared/Model/ShData';
import { SecrecyLevel } from 'Shared/Model/SecrecyLevel';

@Rest.Post('[apiRoot][apiVersion]/dataAPI/getSecMaster', "Get the security master for a particular market.")
@Rest.LoggedIn()
@Rest.Permissions([ApiPermissions.portfolioManager])
@Rest.Returns(Object)
class Data_GetSecMaster extends DalAPI {
    @Rest.Query("Which market to get the security master for.")
    market: string = "";
    
    valdate() {
        /// Nothign to validate here 
        return true;
    }

    action() {
        let market: Market = eido.dataCache.getMarket(this.market);
        Debug.assert(market, `Unknown market: ${this.market}`);

        /// Now get the sec master
        let securities: SecMaster = market.secMaster.getAllSecurities();
        // CObject.sendClientRaw(this as any, 200, securities);
        return securities.sendClient(this as any, 200, SecrecyLevel.public);
    }
}
