import { eido } from 'Shared/Core/ShCore';
import { Debug, PromiseUtil, ShUtil } from 'Shared/Core/ShUtil';
import { Rest } from "Server/Services/RouterUtil";
import { DalAPI } from "./DalAPI";
import { Params, Collections } from "Server/Model/SvModel";
import { Model } from 'Shared/Model/ShModel';
import { CObject } from 'Shared/Core/CObject';
import { StringData, Universe, NextEDData } from './DataCache';
import { EarningsInfo } from 'Shared/Model/Stats/Position';
import { SecrecyLevel } from 'Shared/Model/SecrecyLevel';

@Rest.Post('[apiRoot][apiVersion]/dataAPI/getEarningsInfo', "Get earnings info for a list of FIGIs.")
@Rest.LoggedIn()
@Rest.Returns<EarningsInfo[]>([])
class Data_GetEarningsInfo extends DalAPI {
    @Rest.Query("The FIGIs to fetch.")
    figis: string = "";
    
    @Rest.Query("The start date for which the data is required.")
    startDate: number = 0;
    
    @Rest.Query("The end date for which the data is required.")
    endDate: number = 0;
    
    @Rest.Query("Which market to get the data for.")
    market: string = "";
    
    @Rest.Query("[optional] Which market to get the data for.")
    universeId: string = "";
    
    @Rest.Query("[optional] Which field to get.")
    dataId: string =  "wsh.Next_ED";

    _figis: string[] = [];
    _data: NextEDData = null;
    _startDateIndex: number = 0;
    _endDateIndex: number = 0;

    validate() {
        super.validate();

        this._data = this._market.getNextEDData(this.dataId, this.universeId);
        this.date = 0;
        Debug.assert(this._data, `Unable to load data: ${this.dataId}`);

        this._data.buildDateMap();

        this._startDateIndex = this._data.getDateIndex(this.startDate, true);
        this._error.assert(this._startDateIndex >= 0, 400, `Unable to find start date: ${this.startDate}`);

        this._endDateIndex = this._data.getDateIndex(this.endDate, true);

        if (this._endDateIndex < 0) {
            console.log(`[Earnings] Could not find date: ${this.endDate}. Clamping to the last date ... ${this.endDate}`);
            this.endDate = parseInt(this._data.getLastDate());
            this._endDateIndex = this._data.getDateIndex(this.endDate, true);
        }

        this._error.assert(this._endDateIndex >= 0, 400, `Unable to find end date: ${this.endDate}`);
    }

    action() {
        let retData = new Array<EarningsInfo>(this._figis.length);

        for (let di = this._startDateIndex; di <= this._endDateIndex; di++) {
            let date = this._data.getIndexDate(di);

            this._figis.map((figi, figiIndex) => {
                let ivalue = this._data.getValueFIGI(di, figi);
                if (!retData[figiIndex])
                    retData[figiIndex] = new EarningsInfo();

                if (ivalue) {
                    let obj = retData[figiIndex];
                    if (!obj.dates.length || obj.dates[obj.dates.length - 1] != ivalue) 
                        obj.dates.push(ivalue);
                }
            });
        }

        CObject.sendClientRaw(this, 200, (retData as any).cleanClone(SecrecyLevel.public));
    }
}
