import { eido } from 'Shared/Core/ShCore';
import { Rest } from "Server/Services/RouterUtil";
import { CObject } from 'Shared/Core/CObject';
import { ShUtil } from 'Shared/Core/ShUtil';
import { Market } from './DataCache';

export class DalAPI extends Rest.EndPoint {
    defFields           : any;

    date                : number = 0;
    _market             : Market = null;

    constructor(defFields: any) {
        super();
        this.defFields = defFields;
    }

    validate() { 
        let args = this as any;

        if (typeof args.figis !== 'undefined') {
            if (typeof args.figis.split === 'function')
                args._figis = args.figis.split(',');
            else
                args._figis = args.figis;

            console.log("Num Figis: %d", args._figis.length);
        }

        /// Preload the market
        if (args.market) {
            args._market = eido.dataCache.getMarket(args.market);
            args._universe = args._market.getUniverse(args.universeId);
        }
    }

    makeSymbols(symbols: string[], type = "'CFIGI'") {
        if (!type)
            type = "'CFIGI'";
        let symbolsStr = "dict(Symbology = 'CFIGI', IDs = (";
        for (let i = 0; i < symbols.length; i++)
            symbolsStr += "'" + symbols[i] + "',"; 

        symbolsStr += "))";

        return symbolsStr;
    }

    getSuffix(market) {
        if (market == "US")
            return "";

        return "NonUS";
    }

    executeUrl(args: any, parameters: any) {
        if (parameters.yml && parameters.yml.indexOf('\\') != 0) 
            parameters.yml = eido.appServer.config.data.ymlDir + parameters.yml;
        if (parameters.Fields) 
            parameters.Fields = "[" + parameters.Fields.join(",") + "]";

        if (parameters.StartDate && parameters.StartDate[0] != '\'')
            parameters.StartDate = "'" + ShUtil.intDateToString(parameters.StartDate) + "'";
        if (parameters.EndDate && parameters.EndDate[0] != '\'')
            parameters.EndDate = "'" + ShUtil.intDateToString(parameters.EndDate) + "'";

        if (args.field || (this.defFields && this.defFields.length))
            parameters.Fields = args.field ? ["'" + args.field + "'"] : this.defFields;

        if (typeof parameters.Symbols === 'undefined' && typeof args._figis !== 'undefined') 
            parameters.Symbols = this.makeSymbols(args._figis);

        parameters.ultrajson = "True";
        parameters.Keyword = (this as any).getKeyword(args);

        return new Promise((resolve, reject) => {
            // let ntlm = require('httpntlm');
            // let dataConfig = eido.appServer.config.data;
            // let opts = {
            //     url: dataConfig.url,
            //     username: dataConfig.username,
            //     password: dataConfig.password,
            //     domain: dataConfig.domain,
            //     workstation: dataConfig.workstation,
            //     parameters: parameters
            // };

            // console.log(opts);

            // ntlm.post(opts, (err, response) => {
            //     if (response.statusCode < 200 || response.statusCode >= 300) { 
            //         console.error("[DalAPI] Failure: ", response.statusCode, response.body);
            //         reject(new Error(response.statusCode, response.body));
            //         return;
            //     }

            //     resolve(JSON.parse(response.body));

            //     return;
            // });
        });
    }

    action() {
        let args = this as any;
        let data = args._data.getDataForFigis(args.date, args._figis); 
        CObject.sendClientRaw(args, 200, data);
    }
}

