import { eido } from 'Shared/Core/ShCore';
import { Debug, ShUtil } from 'Shared/Core/ShUtil';
import * as path from 'path';
let BinaryParser = require('binary-parser').Parser;
import { CacheInfo } from '../../App/ServerConfig';
import { ShAlpha, AssetClass } from 'Shared/Model/Config/ShAlpha';
import { Sec, SecMaster } from 'Shared/Model/ShData';
import { SecMasterData, UIntDataMatrix, UIntStaticDataMatrix, FloatDataMatrix, StringDataData, UInt16StaticDataMatrix, UInt16DataMatrix, szDataHeader, DataHeader, szDataRowHeader, DataRowHeader, StringDailyDataRow, StringDailyData } from './BinData';

export class Data {
    path:           string = "";
    data:           any = null;
    universe:       any = null;
    dateList:       string[] = [];
    dateMap:        any = {};
    dateIndexMap:   any = {};

    constructor() { 
    }

    load(path: string, ...args): any {
        return this.loadFilename(path, ...args);
    }

    loadFilename(path: string, ...args): any {
        console.log(`[DataCache] Loading file: ${path}`);
        let fs = require('fs');
        let ret = fs.readFileSync(path); 
        console.log(`[DataCache] File loaded: ${path}`);

        this.path = path;
        return ret;
    }

    buildDateMap(data: any = null) {
        if (this.dateList.length)
            return this.dateMap;

        console.log(`[DataCache] Building data map: ${this.path}`);
        if (!data)
            data = this.data;

        console.log(`[DataCache] Num rows: ${this.data.rows.length} [${this.path}]`);
        this.data.rows.map((row, index) => {
            this.dateList.push(row.rhdr.date);
            this.dateMap[row.rhdr.date] = index;
            this.dateIndexMap[index] = row.rhdr.date;
        });

        console.log(`[DataCache] Date map built: ${this.path}`);
        return this.dateMap;
    }

    getIndexDate(dateIndex) {
        if (this.dateIndexMap[dateIndex])
            return this.dateIndexMap[dateIndex];
        return 0;
    }

    getLastDate(): string {
        return this.dateList[this.dateList.length - 1];
    }

    getLastDateIndex(): number {
        return this.dateList.length - 1;
    }

    getDateIndex(date: any, closest: boolean = false) {
        /// This is universal data!
        if (this.data.rows.length == 1)
            return 0;

        if (this.dateMap[date] >= 0)
            return this.dateMap[date];

        if (!closest)
            return -1;

        if (!this.dateList.length)
            return -1;

        let count = this.dateList.length;

        if (date < this.dateList[0])
            return 0;
        else if (date > this.dateList[count - 1])
            return count - 1;

        let prevDate = this.dateList[0];
 
        for (let i = 1; i < count; i++) {
            let idate = this.dateList[i];

            if (idate > date && prevDate < date) 
                return this.dateMap[idate];

            prevDate = idate;
        }

        return -1;
    }

    getValue(di, ii) {
        return this.data.rows[di].cols[ii];
    }

    getValueFIGI(di: number, figi: string) {
        let ii = this.universe.getIndex(figi);
        return this.data.rows[di].cols[ii];
    }

    getDataForFigisAtIndex(dateIndex, figis) {
        if (dateIndex >= this.data.rows.length)
            return null;

        let retData = (new Array(figis.length)).fill(0.0);
        let index = null;

        figis.map((figi, index) => {
            let dataIndex = this.universe.getIndex(figi);
            let dataValue: any = 'NaN';
            if (dataIndex >= 0)
                dataValue = this.data.rows[dateIndex].cols[dataIndex];
            if (typeof dataValue != 'object')
                retData[index] = dataValue ? dataValue : NaN;
            else
                retData[index] = !dataValue.value ? dataValue : dataValue.value;
        });

        return retData;
    }

    getDataForFigis(date, figis) {
        Debug.assert(this.universe, "Invalid universe!");
        let di = this.getDateIndex(date);
        Debug.assert(di >= 0, `Unable to index for date: ${date} - Data: ${this.path}`);
        return this.getDataForFigisAtIndex(di, figis);
    }
}

export class SecurityMaster extends Data {
    uuids           : string[] = [];
    uuidMap         : any = {};

    _securities     : SecMaster;

    load(path): SecurityMaster {
        this.data = SecMasterData.parse(super.load(path));

        this.uuids = new Array(this.data.securities.length);
        this.data.securities.map((sec, index) => {
            let uuid = sec.uuid;
            this.uuids[index] = uuid;
            this.uuidMap[uuid] = index;
        });

        return this;
    }

    getAllFigis(): string[] {
        return this.uuids;
    }

    getIndex(uuid: string): number {
        return this.uuidMap[uuid] >= 0 ? this.uuidMap[uuid] : -1;
    }

    getSecurity(index) {
        Debug.assert(index >= 0 && index < this.data.securities.length, `Invalid index: ${index} - Max Securities in Sec Master: ${this.data.securities.length}`);
        return this.data.securities[index];
    }

    getAllSecurities(): SecMaster {
        /// If we have preparte a list of securities already, then just send that 
        if (this._securities)
            return this._securities;

        /// Otherwise, make a list
        let securities: Sec[] = new Array<Sec>(this.data.securities.length);

        this.data.securities.map((sec, ii) => {
            let rsec: Sec = new Sec();

            rsec.uuid   = sec.uuid;
            rsec.figi   = sec.figi;
            rsec.isin   = sec.isin;
            rsec.name   = sec.name;
            rsec.permId = sec.permId;
            rsec.perfId = sec.perfId;
            rsec.ticker = sec.ticker;

            securities[ii] = rsec;
        });

        this._securities = new SecMaster(securities);
        
        return this._securities;
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////////
/// Universe
////////////////////////////////////////////////////////////////////////////////////////////////////////
export class Universe extends Data {
    uuids           : string[] = [];
    tickers         : string[] = [];
    uuidMap         : any = {};
    imap            : any = null;

    load(name: string, path: string, secMaster: SecurityMaster): Universe {
        this.data = UIntDataMatrix.parse(super.load(path));
        return this;
    }

    getAllFigis(): string[] {
        return this.uuids;
    }

    getIndex(uuid: string): number {
        return this.uuidMap[uuid] >= 0 ? this.uuidMap[uuid] : -1;
    }

    loadIMap(path: string, secMaster: SecurityMaster) {
        this.imap = UIntStaticDataMatrix.parse(super.load(path));
        Debug.assert(this.imap.rows.length == 1, `Invalid number of rows for imap: ${path} - Expecting 1 found: ${this.imap.rows.length}`);

        this.imap.rows.map((row) => {
            row.cols.map((col, index) => {
                let sec = secMaster.getSecurity(col);
                this.uuidMap[sec.uuid] = index;
                this.uuidMap[sec.ticker] = index;
                this.uuids.push(sec.uuid);
                this.tickers.push(sec.ticker);
            });
        });
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////////
/// UIntData
////////////////////////////////////////////////////////////////////////////////////////////////////////
export class UIntData extends Data {
    load(path: string): UIntData {
        this.data = UIntDataMatrix.parse(super.load(path));
        this.buildDateMap();
        return this;
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////////
/// FloatData
////////////////////////////////////////////////////////////////////////////////////////////////////////
export class FloatData extends Data {
    load(path: string): FloatData {
        this.data = FloatDataMatrix.parse(super.load(path));
        this.buildDateMap();
        return this;
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////////
/// StringData
////////////////////////////////////////////////////////////////////////////////////////////////////////
export class StringData extends Data {
    constructor() { 
        super();
        this.data = null;
    }

    load(path: string): StringData {
        this.data = StringDataData.parse(super.load(path));
        return this;
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////////
/// IStringData: 16-bit indexed string
////////////////////////////////////////////////////////////////////////////////////////////////////////
export class IStringData extends StringData {
    stringData:         StringData = null;
    
    constructor() {
        super();
    }

    load(path: string): StringData {
        if (path.indexOf(",static") >= 0)
            this.data = UInt16StaticDataMatrix.parse(super.loadFilename(path)); 
        else
            this.data = UInt16DataMatrix.parse(super.loadFilename(path));

        let spath: string = path + ".meta";
        this.stringData = new StringData().load(spath);
        return this;
    }

    getValue(di, ii): string {
        let sii = super.getValue(di, ii);
        if (sii < 0 || sii >= 65536)
            return "";

        return this.stringData.getValue(0, sii);
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////////
/// LStringData: 32-bit indexed string
////////////////////////////////////////////////////////////////////////////////////////////////////////
export class LStringData extends Data {
    stringData:         StringData = null;
    
    constructor() {
        super();
    }

    load(path: string): StringData {
        if (path.indexOf(",static") >= 0)
            this.data = UIntDataMatrix.parse(super.loadFilename(path));
        else
            this.data = UIntStaticDataMatrix.parse(super.loadFilename(path));

        let spath: string = path + ".meta";
        this.stringData = new StringData().load(spath);
        return this;
    }

    getValue(di, ii): string {
        let sii = super.getValue(di, ii);
        if (sii < 0 || sii >= 1e9)
            return "";

        return this.stringData.getValue(0, sii);
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////////
/// StringDailyData
////////////////////////////////////////////////////////////////////////////////////////////////////////
export class NextEDData extends Data {
    constructor() { 
        super();
        this.data = null;
    }

    load(path): NextEDData {
        // console.log("[DataCache] Parsing StringDailyData: %s", path);
        // let data = StringDailyDataData.parse(super.load(path)); 
        // console.log("[DataCache] Parsed StringDailyData: %s", path);

        let buffer = super.load(path);

        this.data = {
            header: DataHeader.parse(buffer),
            rows: null
        };

        let offset = szDataHeader;
        let length = this.data.header.numRows.lo
        this.data.rows = new Array(length);

        for (let i = 0; i < length; i++) {
            let rhdrBuffer = Buffer.from(buffer.buffer, offset, szDataRowHeader);
            let rhdr = DataRowHeader.parse(rhdrBuffer);
            let row = {
                rhdr: {
                    date: rhdr.date
                },
                cols: []
            };

            let rowSizeInBytes = rhdr.writtenBytes.lo;
            offset += szDataRowHeader;

            let rowDataBuffer = Buffer.from(buffer.buffer, offset, rowSizeInBytes);
            let rowData = StringDailyDataRow.parse(rowDataBuffer);

            row.cols = new Array(rowData.cols.length);
            rowData.cols.map((col, ci) => row.cols[ci] = ShUtil.strDateToIntDate(col.value, "-"));

            offset += rowSizeInBytes;

            this.data.rows[i] = row; 
        }

        return this;
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////////
/// Market
////////////////////////////////////////////////////////////////////////////////////////////////////////
export class Market {
    marketId                : string = "";
    secMaster               : SecurityMaster = null;
    datasets                : any = {};
    universes               : any = {};
    cacheInfo               : CacheInfo = null;
    baseDir                 : string = null;
    assetClass              : AssetClass;
    simDates                : number[];

    constructor(marketId: string) {
        this.marketId       = marketId;
        this.assetClass     = ShAlpha.getAssetClassForMarket(marketId);
        this.cacheInfo      = eido.appServer.config(this.assetClass).cacheInfo;
        this.baseDir        = this.cacheInfo.root;

        console.info(`Market: ${marketId} - BaseDir: ${this.baseDir} [Asset Class: ${this.assetClass}]`);
    }

    getSimDatesBetween(startDate: number, endDate: number) {
        let simDates: number[] = [];

        for (let si = 0; si < this.simDates.length; si++) {
            if (this.simDates[si] >= startDate && this.simDates[si] <= endDate)
                simDates.push(this.simDates[si]);
        }

        return simDates;
    }

    loadSimDates() {
        let path = require('path');
        let fs = require('fs');
        let simDatesFilename = path.join(this.baseDir, this.marketId, 'sim_dates.txt');
        let data: string = fs.readFileSync(simDatesFilename, 'utf-8');
        let simDates: string[] = data.split('\n');

        this.simDates = new Array<number>(simDates.length);

        simDates.map((sdate: string, i: number) => this.simDates[i] = parseInt(sdate));
    }

    load(): Market {
        this.secMaster = (new SecurityMaster()).load(path.join(this.baseDir, this.marketId, "SecurityMaster", "Master,SecurityData,static"));
        this.loadSimDates();
        return this;
    }

    getUniverse(name: string): Universe | SecurityMaster {
        if (name == "SecurityMaster" || !name)
            return this.secMaster; 

        if (this.universes[name])
            return this.universes[name];

        let universe = (new Universe()).load(name, path.join(this.baseDir, this.marketId, name, "Universe,uint,static"), this.secMaster);
        universe.loadIMap(path.join(this.baseDir, this.marketId, name, "IMap,uint,static"), this.secMaster)
        this.universes[name] = universe;

        return universe;
    }

    getDataGeneric<T extends Data>(DataClass: { new(): T ;}, dataId: string, universeId: string, type: string = 'float', frequency: string = 'daily'): T {
        if (!frequency)
            frequency = "daily";

        let universe = null;
        if (!universeId || universeId == "SecurityMaster") {
            universeId = "SecurityMaster";
            universe = this.secMaster;
        }
        else {
            universe = this.getUniverse(universeId);
            Debug.assert("Unable to load universe: ", universeId);
        }

        let cacheId = universeId + "." + dataId;

        if (this.datasets[cacheId]) {
            /// First of all we want to see whether the data was loaded on the same date as today 
            let currentDate = new Date();
            let dataInfo = this.datasets[cacheId];
            if (currentDate.getFullYear() == dataInfo.time.getFullYear() && 
                currentDate.getMonth() == dataInfo.time.getMonth() && 
                currentDate.getDate() == dataInfo.time.getDate())
                return dataInfo.data;

            /// Otherwise, we need to invalidate the data in the cache ...
            // return this.datasets[cacheId];
        }

        let parts = dataId.split("."); 
        Debug.assert(parts.length == 2, `Invalid dataId: ${dataId}`);
        let subDir = parts[0];
        let dataName = parts[1];

        if (dataName.indexOf(",") < 0)
            dataName += "," + type + "," + frequency; 

        let dataFilename = path.join(this.baseDir, this.marketId, universeId, subDir, dataName);

        /// If the data file doesn't exist then just return
        if (!require('fs').existsSync(dataFilename))
            return null;

        let data = (new DataClass()).load(dataFilename);
        data.universe = universe;

        this.datasets[cacheId] = {
            "time": new Date(),
            "data": data
        };
        
        return data;
    }

    getFloatData(dataId: string, universeId: string, frequency: string = 'daily'): FloatData {
        return this.getDataGeneric(FloatData, dataId, universeId, "float", frequency);
    }

    getStringData(dataId: string, universeId: string, frequency: string = 'static'): StringData {
        let sdata: StringData = this.getDataGeneric(StringData, dataId, "", "string", frequency);

        /// If we manage to find the right data as a string then just load that
        if (sdata)
            return sdata;

        /// Otherwise we try to load an indexed string data
        return this.getIndexedStringData(dataId, universeId, frequency);
    }

    getIndexedStringData(dataId: string, universeId: string, frequency: string = 'static'): StringData {
        let sdata: StringData = this.getIStringData(dataId, universeId, frequency);

        /// If we get some data back then just load that ...
        if (sdata)
            return sdata;
        
        /// Otherwise try to load a long string data
        return this.getLStringData(dataId, universeId, frequency);
    }

    getIStringData(dataId: string, universeId: string, frequency:string = 'static'): StringData {
        return this.getDataGeneric(IStringData, dataId, "", "i_string", frequency);
    }

    getLStringData(dataId: string, universeId: string, frequency:string = 'static'): StringData {
        return this.getDataGeneric(LStringData, dataId, "", "l_string", frequency);
    }

    getStringDailyData(dataId: string, universeId: string) {
        return this.getDataGeneric(StringDailyData, dataId, "", "string", 'daily');
    }

    getNextEDData(dataId: string, universeId: string): NextEDData {
        return this.getDataGeneric(NextEDData, dataId, "", "string", 'daily');
    }

    getUIntData(dataId: string, universeId: string, frequency: string = 'daily'): UIntData {
        return this.getDataGeneric(UIntData, dataId, universeId, "uint", frequency);
    }
}

export class DataCache {
    private static s_instance   : DataCache = null;
    markets                     : any = {}

    static get instance()   : DataCache { return DataCache.s_instance; }
    static set instance(value) { 
        Debug.assert(!DataCache.s_instance, 'DataCache has already been initialised!');
        DataCache.s_instance = value; 
    }

    constructor() {
    }

    load() {
        // let cacheInfo       = eido.appServer.config.cache;
        // cacheInfo.markets.map((marketId: string) => {
        //     this.getMarket(marketId);
        // });

        // this.getMarket("EU");
        // let market = this.getMarket("US"); 
        // let universe = market.getUniverse("Top1000");
        // // let data1 = market.getStringData("secData.CountryOfRiskCode", "Top1000");
        // let data2 = market.getStringDailyData("wsh.Next_ED", "Top1000");
    }

    getMarket(marketId: string): Market { 
        /// TODO: Demo hack!
        marketId = marketId || "US";

        if (this.markets[marketId])
            return this.markets[marketId];

        console.info("[DataCache] Loading market: %s", marketId);
        let market = new Market(marketId);
        market.load();
        this.markets[marketId] = market;

        return market;
    }
}

export default function initCache(app) {
    console.info("[DataCache] Init data cache!");
    DataCache.instance = new DataCache();
    DataCache.instance.load();

    eido.dataCache = DataCache.instance;
};

