import { eido } from 'Shared/Core/ShCore';
import { Params, Collections, DB } from "Server/Model/SvModel";

import { Model } from 'Shared/Model/ShModel';
import { Debug, PromiseUtil } from 'Shared/Core/ShUtil';
import { Util } from 'Server/Core/SvUtil';
import { ShUser, AccountType, AccountStatus, Gender } from 'Shared/Model/User/ShUser';
import { Email } from 'Server/System/Email';
import { Error } from 'Server/Core/Error';
import { User } from "Server/Model/User/SvUser";
import { Rest } from '../RouterUtil';
import { ApiPermissions } from 'Server/Services/Router';
import { SecrecyLevel } from 'Shared/Model/SecrecyLevel';

import { Alpha } from 'Server/Model/Config/SvAlpha';
import { CObject } from 'Shared/Core/CObject';
import { Positions } from 'Shared/Model/Stats/Positions';
import { LTStats } from 'Shared/Model/Stats/LTStats';
import { SampleType, ConfigType, AlphaType } from 'Shared/Model/Config/ShAlpha';
import { ConfigSection } from 'Shared/Model/Config/ConfigSection';
import { ObjectID } from 'Shared/Core/ObjectID';
import { db } from 'Server/Core/DB/DBManager';
import { AlphaOrStrategy } from 'Server/Model/Config/SvConfigBase';

@Rest.Put('[apiRoot][apiVersion]/alpha/publish', "Publish an alpha!")
@Rest.LoggedIn()
@Rest.Permissions([ApiPermissions.alphaOwner, ApiPermissions.strategyOwner])
@Rest.Returns(Object)
class AlphaPublish extends Rest.EndPoint {
    @Rest.Query('Alpha ID')
    alphaId                 : ObjectID = null;

    @Rest.Query('DB Name')
    dbName                  : string = "pesa";

    @Rest.Query('Type')
    type                    : AlphaType;

    preValidate() { return AlphaOrStrategy.validate(this, this.alphaId, this.dbName, ConfigType.user, this.type); }

    validate() {
        this._error.assert(!this._cached.alpha.isPublished, 400, `[${this._cached.alpha.type}] Already published: ${this.alphaId}`);
        this._error.assert(!this._cached.alpha.isMaster, 400, `[${this._cached.alpha.type}] Cannot publish master: ${this.alphaId}`);
        return true;
    }

    action() {
        // /// We want to make this as atomic as possible
        // /// First of all we prepare the alpha for publishing ...
        // return this._cached.alpha.prepareForPublish(this)
        //     .then((alpha) => {
        //         /// OK, send the response to the client now. We don't want a long connection that possibly times out.
        //         /// The rest of the communication will be done over the websocket
        //         CObject.sendClientRaw(this, 200, { 
        //             _id: alpha._id,
        //             subAlphaId: alpha.subAlphaId,
        //             superAlphaId: alpha.superAlphaId
        //         });

        //         /// Ok, now that we have sent the response that tells about the sub and super alphas
        //         /// We start off with publishing the alpha ...
        //         return this._cached.alpha.publish(this);
        //     })
        //     .then((alpha) => {
        //         /// OK now, we just run the alpha, sub-alpha and super-universe alpha together
        //         return Promise.all([
        //             alpha.run(this, true),
        //             alpha.runSubAlpha(this),
        //             alpha.runSuperAlpha(this)
        //         ]);
        //         // Promise.resolvedPromise(alpha);
        //     })
        //     // .then(() => {
        //     //     return this._cached.alpha.runPostPublishTests();
        //     // })
        //     .catch((err) => {
        //         console.error(`[Alpha] Rolling back publish because of error while publishing alpha: ${JSON.stringify(err)}`);
        //         console.error(err.stack);
        //         this._cached.alpha.rollbackPublish(this, err.toString());
        //     });

        return this._cached.alpha.publishAny().then(() => CObject.sendClientRaw(this, 200, { modifiedDate: this._cached.alpha.modifiedDate }));
    }
}
