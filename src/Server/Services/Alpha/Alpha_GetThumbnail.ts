import { eido } from 'Shared/Core/ShCore';
import { Params, Collections, DB } from "Server/Model/SvModel";

import { Model } from 'Shared/Model/ShModel';
import { Debug, PromiseUtil } from 'Shared/Core/ShUtil';
import { Util } from 'Server/Core/SvUtil';
import { ShUser, AccountType, AccountStatus, Gender } from 'Shared/Model/User/ShUser';
import { Email } from 'Server/System/Email';
import { Error } from 'Server/Core/Error';
import { User } from "Server/Model/User/SvUser";
import { Rest } from '../RouterUtil';
import { ApiPermissions } from 'Server/Services/Router';
import { SecrecyLevel } from 'Shared/Model/SecrecyLevel';

import { Alpha } from 'Server/Model/Config/SvAlpha';
import { CObject } from 'Shared/Core/CObject';
import { ObjectID } from 'Shared/Core/ObjectID';
import { Positions } from 'Shared/Model/Stats/Positions';
import { LTStats } from 'Shared/Model/Stats/LTStats';
import { SampleType, ConfigType } from 'Shared/Model/Config/ShAlpha';

@Rest.Get('[apiRoot][apiVersion]/alpha/thm', "Get the thumbnail for a particular alpha.")
@Rest.Permissions([ApiPermissions.public])
@Rest.Returns(String)
@Rest.Content("image/png")
class AlphaGetThumbnail extends Rest.EndPoint {
    @Rest.Query('Alpha ID')
    alphaId     : ObjectID = null;

    @Rest.Query(`Sample name. Values = ${Object.keys(SampleType)}`, SampleType)
    sampleName  : SampleType = SampleType.IS;

    @Rest.Query("Market.")
    market      : string = "US";

    @Rest.Query("The db to use.")
    dbName      : string = "pesa_us";

    @Rest.Query("What type of config do we want to load.", ConfigType, false)
    configType: ConfigType = ConfigType.user;
    
    preValidate() { return Alpha.validate(this, this.alphaId, this.dbName, this.configType); }

    action() {
        console.log(`[Alpha] Getting thumbnail: ${this.dbName}@${this.alphaId} [Sample: ${this.sampleName}]`);
        return Util.sendThumbnail(this, this.alphaId, this._cached.alpha.getAssetClass())
            .catch(() => { 
                console.error(`[Alpha] Error getting thumbnail: ${this.dbName}@${this.alphaId} [Sample: ${this.sampleName}]`);
                this._res.status(404).send({});
            });
    }
}
