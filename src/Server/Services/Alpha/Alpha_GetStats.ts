import { Rest } from '../RouterUtil';
import { ApiPermissions } from 'Server/Services/Router';

import { Alpha } from 'Server/Model/Config/SvAlpha';
import { CObject } from 'Shared/Core/CObject';
import { ObjectID } from 'Shared/Core/ObjectID';
import { LTStats } from 'Shared/Model/Stats/LTStats';
import { ConfigType } from 'Shared/Model/Config/ShAlpha';

@Rest.Get('[apiRoot][apiVersion]/alpha/getStats', "Loads the stats for the said alpha.")
@Rest.LoggedIn()
@Rest.Permissions([ApiPermissions.alphaAccess, ApiPermissions.portfolioManager])
@Rest.Returns(LTStats)
class AlphaGetStats extends Rest.EndPoint {
    @Rest.Query('Alpha ID')
    alphaId                 : ObjectID = null;

    @Rest.Query("DB Name.")
    dbName: string;

    @Rest.Query("What type of config do we want to load.", ConfigType, false)
    configType: ConfigType = ConfigType.user;
    
    preValidate() { return Alpha.validate(this, this.alphaId, this.dbName, this.configType); }

    loadFromDisk() {
        return this._cached.alpha.loadLTStats().then((ltStats) => {
            console.log(`[AlphaStats] Sending stats for alpha: ${this.alphaId}`)
            // ltStats.sendClient(this, 200)
            CObject.sendClientRaw(this, 200, ltStats);
            console.log(`[AlphaStats] Sent stats for alpha: ${this.alphaId}`)
        });
    }

    action() {
        return this.loadFromDisk();
    }
}
