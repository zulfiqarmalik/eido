import { eido } from 'Shared/Core/ShCore';
import { Params, Collections, DB } from "Server/Model/SvModel";

import { Model } from 'Shared/Model/ShModel';
import { Debug, PromiseUtil } from 'Shared/Core/ShUtil';
import { Util } from 'Server/Core/SvUtil';
import { ShUser, AccountType, AccountStatus, Gender } from 'Shared/Model/User/ShUser';
import { Email } from 'Server/System/Email';
import { Error } from 'Server/Core/Error';
import { User } from "Server/Model/User/SvUser";
import { Rest } from '../RouterUtil';
import { ApiPermissions } from 'Server/Services/Router';
import { SecrecyLevel } from 'Shared/Model/SecrecyLevel';

import { Alpha } from 'Server/Model/Config/SvAlpha';
import { CObject } from 'Shared/Core/CObject';
import { Positions } from 'Shared/Model/Stats/Positions';
import { LTStats } from 'Shared/Model/Stats/LTStats';
import { SampleType, ConfigType } from 'Shared/Model/Config/ShAlpha';
import { ConfigSection } from 'Shared/Model/Config/ConfigSection';
import { ObjectID } from 'Shared/Core/ObjectID';

@Rest.Put('[apiRoot][apiVersion]/alpha/run', "Run a simulation of the alpha")
@Rest.LoggedIn()
@Rest.Permissions([ApiPermissions.alphaOwner, ApiPermissions.portfolioManager])
@Rest.Returns(Object)
class AlphaRun extends Rest.EndPoint {
    @Rest.Query('Alpha ID')
    alphaId                 : ObjectID = null;

    @Rest.Query("DB Name.")
    dbName: string;

    @Rest.Query("What type of config do we want to load.", ConfigType, false)
    configType: ConfigType = ConfigType.user;
    
    preValidate() { return Alpha.validate(this, this.alphaId, this.dbName, this.configType); }
    
    validate() {
        // args.assert(!args._cached.alpha.isRunning, 400, "Alpha is already running!");
        // let ws = args._appServer.wsServer.getWebSocket(args.userId().toString());
        // args.assert(ws, 500, "No websocket. Cannot start ")

        /// Cancel any existing run 
        // return args._cached.alpha.cancel();
        return this._cached.alpha.clearStatus();
    }

    action() {
        this._cached.alpha.run(this)
    }
}
