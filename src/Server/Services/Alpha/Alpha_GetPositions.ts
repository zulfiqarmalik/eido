import { eido } from 'Shared/Core/ShCore';
import { Params, Collections } from "Server/Model/SvModel";

import { Model } from 'Shared/Model/ShModel';
import { Debug, PromiseUtil } from 'Shared/Core/ShUtil';
import { Util } from 'Server/Core/SvUtil';
import { ShUser, AccountType, AccountStatus, Gender } from 'Shared/Model/User/ShUser';
import { Email } from 'Server/System/Email';
import { Error } from 'Server/Core/Error';
import { User } from "Server/Model/User/SvUser";
import { Rest } from '../RouterUtil';
import { ApiPermissions } from 'Server/Services/Router';
import { SecrecyLevel } from 'Shared/Model/SecrecyLevel';

import { Alpha } from 'Server/Model/Config/SvAlpha';
import { CObject } from 'Shared/Core/CObject';
import { Positions } from 'Shared/Model/Stats/Positions';
import { ObjectID } from 'Shared/Core/ObjectID';
import { ConfigType } from 'Shared/Model/Config/ShAlpha';

@Rest.Get('[apiRoot][apiVersion]/alpha/getPositions', "Gets the positions in a date range.")
@Rest.LoggedIn()
@Rest.Permissions([ApiPermissions.alphaAccess, ApiPermissions.portfolioManager])
@Rest.Returns(Positions)
class AlphaGetPositions extends Rest.EndPoint {
    @Rest.Query("The id of the positions to load.")
    alphaId: ObjectID = null;
    
    @Rest.Query("The name of the database (needed only for DB strategy).")
    market: string = "";
    
    @Rest.Query("The date for which the positions are needed.")
    date: number = 0;
    
    @Rest.Query("[Optional] The per stock date to load.")
    loadPss: boolean = false;

    @Rest.Query("DB Name.")
    dbName: string;

    @Rest.Query("What type of config do we want to load.", ConfigType, false)
    configType: ConfigType = ConfigType.user;
    
    ////////////////////////////////////////////////////////////////////////////////////
    /// Not part of the payload
    ////////////////////////////////////////////////////////////////////////////////////
    positions       : Positions = null;
    
    preValidate() { return Alpha.validate(this, this.alphaId, this.dbName, this.configType); }

    loadFromDisk() {
        this._cached.alpha.loadPositionsForDate(this.date)
            .then((positions) => {
                this.positions = positions;
                positions.sendClient(this, 200, SecrecyLevel.public);
            });
    }

    action() {
        return this.loadFromDisk();
    }
}
