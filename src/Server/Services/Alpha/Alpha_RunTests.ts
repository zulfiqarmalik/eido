import { eido } from 'Shared/Core/ShCore';
import { Params, Collections, DB } from "Server/Model/SvModel";

import { Model } from 'Shared/Model/ShModel';
import { Debug, PromiseUtil } from 'Shared/Core/ShUtil';
import { Util } from 'Server/Core/SvUtil';
import { ShUser, AccountType, AccountStatus, Gender } from 'Shared/Model/User/ShUser';
import { Email } from 'Server/System/Email';
import { Error } from 'Server/Core/Error';
import { User } from "Server/Model/User/SvUser";
import { Rest } from '../RouterUtil';
import { ApiPermissions } from 'Server/Services/Router';
import { SecrecyLevel } from 'Shared/Model/SecrecyLevel';

import { Alpha } from 'Server/Model/Config/SvAlpha';
import { CObject } from 'Shared/Core/CObject';
import { Positions } from 'Shared/Model/Stats/Positions';
import { LTStats } from 'Shared/Model/Stats/LTStats';
import { SampleType } from 'Shared/Model/Config/ShAlpha';
import { ConfigSection } from 'Shared/Model/Config/ConfigSection';
import { ObjectID } from 'Shared/Core/ObjectID';

@Rest.Put('[apiRoot][apiVersion]/alpha/test', "Run tests on an alpha")
@Rest.LoggedIn()
@Rest.Permissions([ApiPermissions.alphaOwner, ApiPermissions.portfolioManager])
@Rest.Returns(Alpha)
class AlphaRunTests extends Rest.EndPoint {
    @Rest.Query('Alpha ID')
    alphaId                 : ObjectID = null;

    action() {
        return this._cached.alpha.runPostPublishTests()
            .then((alpha) => alpha.sendClient(this, 200, SecrecyLevel.public));
    }
}
