import { Rest } from '../RouterUtil';
import { ApiPermissions } from 'Server/Services/Router';

import { Alpha } from 'Server/Model/Config/SvAlpha';
import { Positions } from 'Shared/Model/Stats/Positions';
import { ObjectID } from 'Shared/Core/ObjectID';
import { CObject } from 'Shared/Core/CObject';
import { ConfigType } from 'Shared/Model/Config/ShAlpha';

@Rest.Get('[apiRoot][apiVersion]/alpha/getPositionsNet', "Load a range of positions from all the positions of an alpha and net them in a certain way.")
@Rest.LoggedIn()
@Rest.Permissions([ApiPermissions.alphaAccess, ApiPermissions.portfolioManager])
@Rest.Returns(Positions)
export class AlphaGetPositionsNet extends Rest.EndPoint {
    @Rest.Query("Alpha ID")
    alphaId: ObjectID = null;
    
    @Rest.Query("Market Name")
    market: string = "";
    
    @Rest.Query("Start Date")
    startDate: number = 0;
    
    @Rest.Query("End Date")
    endDate: number = 0;
    
    @Rest.Query("Calculate Positions?")
    calculatePos: boolean = true;
    
    @Rest.Query("Calculate Information Coefficient?")
    calculateIC: boolean = false;
    
    @Rest.Query("Force it to not run as a task but as a normal REST API.")
    noAsync: boolean = false;

    @Rest.Query("What type of config do we want to load.", ConfigType, false)
    configType: ConfigType = ConfigType.user;
    
    @Rest.Query("DB Name.")
    dbName: string;

    ////////////////////////////////////////////////////////////////////////////////////
    /// Not part of the payload
    ////////////////////////////////////////////////////////////////////////////////////
    _posDir: string

    preValidate() { return Alpha.validate(this, this.alphaId, this.dbName, this.configType); }

    getId(): string { 
        return `${this._cached.user._id}_${this.alphaId}_[${this.startDate},${this.endDate}]_${this._posDir}`;
    }

    runTask() {
        return this._cached.alpha.loadAllPosInfo(this, this.startDate, this.endDate, this.calculatePos, this.calculateIC);
    }

    action() {
        if (this._noTask || this.noAsync) {
            return this.runTask().then((positions) => CObject.sendClientRaw(this, 200, positions)) //.sendClient(this, 200));
        }

        return this.createTask();
    }
}
