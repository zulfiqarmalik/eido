import { DB } from "Server/Model/SvModel";

import { Rest } from '../RouterUtil';
import { ApiPermissions } from 'Server/Services/Router';

import { Alpha } from 'Server/Model/Config/SvAlpha';
import { CObject } from 'Shared/Core/CObject';
import { AlphaBucket, ConfigType } from 'Shared/Model/Config/ShAlpha';
import { ObjectID } from 'Shared/Core/ObjectID';

@Rest.Patch('[apiRoot][apiVersion]/alpha', "Update an alpha")
@Rest.LoggedIn()
@Rest.Permissions([ApiPermissions.alphaOwner])
@Rest.Returns(Object)
class AlphaUpdate extends Rest.EndPoint {
    @Rest.Query("The friendly name of this alpha.")
    alphaId: ObjectID = null;
    
    @Rest.Query("The name of the market that this alpha belongs to.")
    market: string = "";
    
    @Rest.Query("The name of the universe that this alpha belongs to.")
    universeId: string = "";
    
    @Rest.Query("The starting date of this alpha.", null, false)
    startDate: number = 0;
    
    @Rest.Query("The ending date percent used for out-sample calculations.", null, false)
    endDatePercent: number = 0.0;
    
    @Rest.Query("The bucket that this alpha belongs to.", AlphaBucket, false)
    bucket: AlphaBucket = AlphaBucket.Fundamental;

    @Rest.Body("Source code for the alpha", null, false)
    source: string = "";

    preValidate() { return Alpha.validate(this, this.alphaId, DB.pesaUser, ConfigType.user); }

    validate() {
        this.assert(!this._cached.alpha.isPublished, 400, `[Alpha] Alpha: ${this._cached.alpha._id} has already been published! Modifying a published alpha is NOT allowed!`);
        let info: any = Alpha.validateUpdate(this, this.market, this.universeId, this.startDate, this.endDatePercent);
        this.source = info.source;
        this.startDate = info.startDate;
        this.endDatePercent = info.endDatePercent;

        return true;
    }

    action() {
        this._cached.alpha.update(this, this.source)
            .then((alpha) => {
                CObject.sendClientRaw(this, 200, { 
                    _id: alpha._id
                });
            });
    }
}
