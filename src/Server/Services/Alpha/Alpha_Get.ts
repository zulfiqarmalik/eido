import { eido } from 'Shared/Core/ShCore';
import { Params, Collections } from "Server/Model/SvModel";

import { Model } from 'Shared/Model/ShModel';
import { Debug, PromiseUtil } from 'Shared/Core/ShUtil';
import { Util } from 'Server/Core/SvUtil';
import { ShUser, AccountType, AccountStatus, Gender } from 'Shared/Model/User/ShUser';
import { Email } from 'Server/System/Email';
import { Error } from 'Server/Core/Error';
import { User } from "Server/Model/User/SvUser";
import { Rest } from '../RouterUtil';
import { ApiPermissions } from 'Server/Services/Router';
import { SecrecyLevel } from 'Shared/Model/SecrecyLevel';

import { Alpha } from 'Server/Model/Config/SvAlpha';
import { CObject } from 'Shared/Core/CObject';
import { ObjectID } from 'Shared/Core/ObjectID';
import { ConfigType, AlphaType } from 'Shared/Model/Config/ShAlpha';
import { AlphaOrStrategy } from 'Server/Model/Config/SvConfigBase';

@Rest.Get('[apiRoot][apiVersion]/alpha', "Get an existing alpha.")
@Rest.LoggedIn()
@Rest.Permissions([ApiPermissions.alphaOwner, ApiPermissions.portfolioManager])
@Rest.Returns(AlphaOrStrategy)
class AlphaGet extends Rest.EndPoint {
    @Rest.Query('The alpha id')
    alphaId         : ObjectID = null;

    @Rest.Query('DB Name')
    dbName          : string = "pesa_us";

    @Rest.Query("What type of config do we want to load.", ConfigType, false)
    configType: ConfigType = ConfigType.any;
    
    @Rest.Query('Whether to get key stats or not', null, false)
    keyStatsTable   : string = "";

    @Rest.Query('Load source or not', null, false)
    noSource        : boolean = false;

    preValidate() { 
        return AlphaOrStrategy.validate(this, this.alphaId, this.dbName, this.configType, AlphaType.any); 
    };

    action() {
        let promise = null;
        if (!this._cached.alpha) {
            CObject.sendClientRaw(this, 400, {});
            return;
        }

        if (!this.noSource) 
            promise = this._cached.alpha.getSource();
        else
            promise = PromiseUtil.resolvedPromise(this._cached.alpha);

        promise.then((alpha) => {
            if (this.keyStatsTable)
                return alpha.loadKeyStats(this.keyStatsTable);
            return PromiseUtil.resolvedPromise(alpha);
        })
        .then((alpha) => alpha.sendClient(this, 200, SecrecyLevel.public));
    }
}
