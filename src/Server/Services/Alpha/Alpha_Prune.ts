import { DB } from "Server/Model/SvModel";

import { Rest } from '../RouterUtil';
import { ApiPermissions } from 'Server/Services/Router';

import { ObjectID } from 'Shared/Core/ObjectID';
import { ConfigType, AlphaType } from 'Shared/Model/Config/ShAlpha';
import { AlphaOrStrategy } from "Server/Model/Config/SvConfigBase";
import { CObject } from "Shared/Core/CObject";

@Rest.Put('[apiRoot][apiVersion]/alpha/prune', "Prune the alpha!")
@Rest.LoggedIn()
@Rest.Permissions([ApiPermissions.alphaOwner])
@Rest.Returns(Object)
class AlphaPrune extends Rest.EndPoint {
    @Rest.Query('Alpha ID')
    alphaId     : ObjectID = null;

    preValidate() { return AlphaOrStrategy.validate(this, this.alphaId, DB.pesaUser, ConfigType.any, AlphaType.any); }

    validate() {
        this._error.assert(!this._cached.alpha.isMaster, 400, `[${this._cached.alpha.type}] Cannot prune master: ${this.alphaId}`);
        return true;
    }

    async action() {
        console.log(`[Alpha] Pruning: ${this._cached.alpha._id}`);

        this._cached.alpha.prune().then(() => {
            CObject.sendClientRaw(this, 202, {});
        });
    }
}
