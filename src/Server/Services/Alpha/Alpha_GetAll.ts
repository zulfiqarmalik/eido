import { eido } from 'Shared/Core/ShCore';
import { Params, Collections } from "Server/Model/SvModel";

import { Model } from 'Shared/Model/ShModel';
import { Debug } from 'Shared/Core/ShUtil';
import { Util } from 'Server/Core/SvUtil';
import { ShUser, AccountType, AccountStatus, Gender } from 'Shared/Model/User/ShUser';
import { Email } from 'Server/System/Email';
import { Error } from 'Server/Core/Error';
import { User } from "Server/Model/User/SvUser";
import { Rest } from '../RouterUtil';
import { ApiPermissions } from 'Server/Services/Router';
import { SecrecyLevel } from 'Shared/Model/SecrecyLevel';

import { Alpha } from 'Server/Model/Config/SvAlpha';
import { CObject } from 'Shared/Core/CObject';
import { ObjectID } from 'Shared/Core/ObjectID';

@Rest.Get('[apiRoot][apiVersion]/alpha/all', "Get all the alphas of the current user.")
@Rest.LoggedIn()
@Rest.Returns<Alpha[]>([])
class AlphaGetAll extends Rest.EndPoint {
    @Rest.Query("The market.", null, false)
    market: string = "US";
    
    action() { 
        return Alpha.loadAllAlphas(this.userId(), this.market)
            .then((alphas) => {
                alphas.sendClient(this, 200, SecrecyLevel.public);
            });
    }
}
