import { eido } from 'Shared/Core/ShCore';
import { Params, Collections, DB } from "Server/Model/SvModel";

import { Model } from 'Shared/Model/ShModel';
import { Debug } from 'Shared/Core/ShUtil';
import { Util } from 'Server/Core/SvUtil';
import { ShUser, AccountType, AccountStatus, Gender } from 'Shared/Model/User/ShUser';
import { Email } from 'Server/System/Email';
import { Error } from 'Server/Core/Error';
import { User } from "Server/Model/User/SvUser";
import { Rest } from '../RouterUtil';
import { ApiPermissions } from 'Server/Services/Router';
import { SecrecyLevel } from 'Shared/Model/SecrecyLevel';

import { Alpha } from 'Server/Model/Config/SvAlpha';
import { CObject } from 'Shared/Core/CObject';
import { ObjectID } from 'Shared/Core/ObjectID';
import { ConfigType } from 'Shared/Model/Config/ShAlpha';

@Rest.Delete('[apiRoot][apiVersion]/alpha', "Deletes an unpublished alpha.")
@Rest.LoggedIn()
@Rest.Permissions([ApiPermissions.alphaOwner])
@Rest.Returns(Alpha)
class AlphaDelete extends Rest.EndPoint {
    @Rest.Query('Alpha ID')
    alphaId                 : ObjectID = null;

    preValidate() { return Alpha.validate(this, this.alphaId, DB.pesaUser, ConfigType.any); }

    validate() {
        this._error.assert(this._cached.alpha && !this._cached.alpha.isPublished, 400, "Cannot delete a published alpha!");
    }

    action() {
        return this._cached.alpha.delete()
            .then(() => CObject.sendClientRaw(this, 200, { _id: this.alphaId }) );
    }
}
