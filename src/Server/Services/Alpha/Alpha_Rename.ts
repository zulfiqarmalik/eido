// import { eido } from 'Shared/Core/ShCore';
// import { Params, Collections, DB } from "Server/Model/SvModel";

// import { Model } from 'Shared/Model/ShModel';
// import { Debug, PromiseUtil } from 'Shared/Core/ShUtil';
// import { Util } from 'Server/Core/SvUtil';
// import { ShUser, AccountType, AccountStatus, Gender } from 'Shared/Model/User/ShUser';
// import { Email } from 'Server/System/Email';
// import { Error } from 'Server/Core/Error';
// import { User } from "Server/Model/User/SvUser";
// import { Rest } from '../RouterUtil';
// import { ApiPermissions } from 'Server/Services/Router';
// import { SecrecyLevel } from 'Shared/Model/SecrecyLevel';

// import { Alpha } from 'Server/Model/Config/SvAlpha';
// import { CObject } from 'Shared/Core/CObject';
// import { ObjectID } from '../../Core/SvCore';
// import { Positions } from 'Shared/Model/Stats/Positions';
// import { LTStats } from 'Shared/Model/Stats/LTStats';
// import { SampleType, AlphaBucket } from 'Shared/Model/Config/ShAlpha';
// import { ConfigSection } from 'Shared/Model/Config/ConfigSection';

// @Rest.Patch('[apiRoot][apiVersion]/alpha/rename', "Update an alpha")
// @Rest.LoggedIn()
// @Rest.Permissions([ApiPermissions.alphaOwner])
// @Rest.Returns(Object)
// class AlphaUpdate extends Rest.EndPoint {
//     static get info() {
//         return {
//             path: '[apiRoot][apiVersion]/alpha/rename',
//             spec: {
//                 description: "Create a new alpha and return the uuid.",
//                 type: 'Array',
//                 parameters: [
//                     Params.field("alphaId", "The friendly name of this alpha.", "string"),
//                     Params.field("marketName", "The name of the market that this alpha belongs to.", "string"),
//                     Params.field("universeName", "The name of the universe that this alpha belongs to.", "string"),
//                     Params.body("source", "The source code for the alpha.", "string", false, null, "")
//                 ]
//             },

//             permissions: [eido.ApiPermissions.login, eido.ApiPermissions.alphaOwner],
//         }
//     }

//     constructor(args) {
//         super(args);
//     }

//     validate(args) {
//         return args._cached.alpha;
//     }

//     action(args) {
//         console.log("[Alpha] Getting thumbnail: %s@%s", args.dbName, args.alphaId);
//         (new Alpha()).create(this.userId(), args.name)
//             .then((alpha) => {
//                 CObject.sendClientRaw(this.args, 201, {
//                     _id: alpha._id
//                 });
//             });
//     }
// }

// export default function init(app) {
//     new RestPatch(app, AlphaUpdate);
// }
