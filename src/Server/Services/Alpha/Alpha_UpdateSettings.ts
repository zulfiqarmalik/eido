import { DB } from "Server/Model/SvModel";

import { Rest } from '../RouterUtil';
import { ApiPermissions } from 'Server/Services/Router';
import { SecrecyLevel } from 'Shared/Model/SecrecyLevel';

import { Alpha } from 'Server/Model/Config/SvAlpha';
import { ObjectID } from 'Shared/Core/ObjectID';
import { ConfigType, AlphaType } from 'Shared/Model/Config/ShAlpha';
import { ConfigSection } from 'Shared/Model/Config/ConfigSection';
import { AlphaOrStrategy } from "Server/Model/Config/SvConfigBase";

@Rest.Put('[apiRoot][apiVersion]/alpha/settings', "Change the settings of the alpha!")
@Rest.LoggedIn()
@Rest.Permissions([ApiPermissions.alphaOwner])
@Rest.Returns(Object)
class AlphaUpdateSettings extends Rest.EndPoint {
    @Rest.Query('Alpha ID')
    alphaId     : ObjectID = null;

    operations  : ConfigSection[] = null;
    settings    : any = null;

    preValidate() { return AlphaOrStrategy.validate(this, this.alphaId, DB.pesaUser, ConfigType.any, AlphaType.any); }

    validate() {
        this._error.assert(!this._cached.alpha.isPublished, 400, `[Alpha] Alpha: ${this._cached.alpha._id} has already been published! Modifying a published alpha is NOT allowed!`);
        
        // this.operation = (new ConfigSection()).copyFrom(this._req.body);
        this.operations = new Array(this._req.body.operations.length);
        this.settings = this._req.body.settings;

        this._req.body.operations.map((op, index) => { 
            let operation = new ConfigSection();
            operation.m_name = "Operation";
            operation.map = op;
            operation.children = [];
            this.operations[index] = operation;
        });
        
        // this.assert(this.operation.map.uuid, 400, "Invalid operation without a uuid");
        // this.assert(!this._cached.alpha.hasOperation(this.operation.map.uuid), 400, `Operation already exists: ${this.operation.map.uuid}`);
    }

    async action() {
        console.log(`[Alpha] Updating alpha settings: ${this._cached.alpha._id}`);

        return this._cached.alpha.updateSettings(this.settings, this.operations)
            .then((alpha) => alpha.sendClient(this, 200, SecrecyLevel.public));
    }
}
