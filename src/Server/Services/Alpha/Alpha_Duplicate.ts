import { Rest } from '../RouterUtil';
import { ApiPermissions } from 'Server/Services/Router';
import { SecrecyLevel } from 'Shared/Model/SecrecyLevel';

import { Alpha } from 'Server/Model/Config/SvAlpha';
import { ObjectID } from 'Shared/Core/ObjectID';
import { AlphaOrStrategy } from 'Server/Model/Config/SvConfigBase';
import { AlphaType, ConfigType } from 'Shared/Model/Config/ShAlpha';
import { DB } from 'Server/Model/SvModel';

@Rest.Put('[apiRoot][apiVersion]/alpha/duplicate', "Duplicate an alpha.")
@Rest.LoggedIn()
@Rest.Permissions([ApiPermissions.alphaOwner, ApiPermissions.portfolioManager])
@Rest.Returns(AlphaOrStrategy)
class AlphaPut extends Rest.EndPoint {
    @Rest.Query('The alpha id')
    alphaId         : ObjectID = null;

    @Rest.Query("Market Name.")
    market: string = "US";
    
    @Rest.Query("The friendly name of this alpha.")
    name: string = "";
    
    @Rest.Query("The category of this alpha.")
    category: string = "";
    
    preValidate() { return AlphaOrStrategy.validate(this, this.alphaId, DB.pesaUser, ConfigType.user, AlphaType.any); }

    validate() {
        let validName = /^[A-Z_][0-9A-Z_]/i;
        this.assert(this.name && validName.test(this.name), 400, `Invalid name specified for the Alpha: ${this.name}`);
        
        return AlphaOrStrategy.checkExists(this.userId(), this.name, this.market)
            .then((doesExist) => this.assert(!doesExist, 409, "Alpha/Strategy already exists: " + this.name));
    }

    action() {
        console.log(`[AlphaOrStrategy] Duplicating Alpha: ${this.alphaId}@${this.category}/${this.name}`);

        return AlphaOrStrategy.duplicate(this.userId(), this.alphaId, this.name, this.category)
            .then((alpha: AlphaOrStrategy) => alpha.sendClient(this, 201, SecrecyLevel.public));
    }
}
