import { eido } from 'Shared/Core/ShCore';
import { Params, Collections } from "Server/Model/SvModel";

import { Model } from 'Shared/Model/ShModel';
import { Debug } from 'Shared/Core/ShUtil';
import { Util } from 'Server/Core/SvUtil';
import { ShUser, AccountType, AccountStatus, Gender } from 'Shared/Model/User/ShUser';
import { Email } from 'Server/System/Email';
import { Error } from 'Server/Core/Error';
import { User } from "Server/Model/User/SvUser";
import { Rest } from '../RouterUtil';
import { ApiPermissions } from 'Server/Services/Router';
import { SecrecyLevel } from 'Shared/Model/SecrecyLevel';

import { Alpha } from 'Server/Model/Config/SvAlpha';
import { AlphaSubType } from 'Shared/Model/Config/ShAlpha';

@Rest.Put('[apiRoot][apiVersion]/alpha', "Create a new alpha and return the uuid.")
@Rest.LoggedIn()
@Rest.Permissions(ApiPermissions.login)
@Rest.Returns(Alpha)
class AlphaPut extends Rest.EndPoint {
    @Rest.Query("The friendly name of this alpha.")
    name: string = "";
    
    @Rest.Query("The category of this alpha.")
    category: string = "";
    
    @Rest.Query("The name of the market that this alpha belongs to.")
    market: string = "";
    
    @Rest.Query("The name of the universe that this alpha belongs to.")
    universeId: string = "";
    
    @Rest.Query("What type of alpha this is. The two categories are 'python' and 'expr'.", AlphaSubType)
    type: AlphaSubType = AlphaSubType.python;
    
    @Rest.Query("What is the delay of the alpha.")
    delay: number = 1;
    
    validate() {
        let validName = /^[A-Z_][0-9A-Z_]/i;
        this.assert(this.name && validName.test(this.name), 400, `Invalid name specified for the Alpha: ${this.name}`);
        this.assert(Alpha.validateMarket(this.market), 400, `Invalid market: ${this.market}`);
        this.assert(Alpha.validateUniverse(this.market, this.universeId), 400, `Invalid universe: ${this.universeId}`);
        
        return Alpha.checkExists(this.userId(), this.name, this.market)
            .then((doesExist) => this.assert(!doesExist, 409, "Alpha already exists: " + this.name));
    }

    action() {
        console.log(`[Alpha] Creating Alpha: ${this.userId()}@${this.category}/${this.name}:${this.type}:${this.market}:${this.universeId}`, this.userId().toString(), this.name);
        
        return (new Alpha()).createNew(this, this.type, this.userId(), this.name, this.category, this.market, this.universeId, this.delay)
            .then((alpha) => alpha.sendClient(this, 201, SecrecyLevel.public));
    }
}
