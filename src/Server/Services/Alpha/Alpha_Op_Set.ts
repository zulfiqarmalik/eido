import { DB } from "Server/Model/SvModel";

import { Rest } from '../RouterUtil';
import { ApiPermissions } from 'Server/Services/Router';
import { SecrecyLevel } from 'Shared/Model/SecrecyLevel';

import { Alpha } from 'Server/Model/Config/SvAlpha';
import { ObjectID } from 'Shared/Core/ObjectID';
import { ConfigType } from 'Shared/Model/Config/ShAlpha';
import { ConfigSection } from 'Shared/Model/Config/ConfigSection';

@Rest.Put('[apiRoot][apiVersion]/alpha/op', "Set the operations on this alpha. Note that OpNormalise is always implied!")
@Rest.LoggedIn()
@Rest.Permissions([ApiPermissions.alphaOwner])
@Rest.Returns(Object)
class AlphaOpSet extends Rest.EndPoint {
    @Rest.Query('Alpha ID')
    alphaId     : ObjectID = null;

    @Rest.Body("JSON encoded operation data which should be a ConfigSection object.")
    operations  : ConfigSection[];

    preValidate() { return Alpha.validate(this, this.alphaId, DB.pesaUser, ConfigType.user); }

    validate() {
        this._error.assert(!this._cached.alpha.isPublished, 400, `[Alpha] Alpha: ${this._cached.alpha._id} has already been published! Modifying a published alpha is NOT allowed!`);
        
        // this.operation = (new ConfigSection()).copyFrom(this._req.body);
        this.operations = new Array(this._req.body.operations.length);
        this._req.body.operations.map((op, index) => { 
            let operation = new ConfigSection();
            operation.m_name = "Operation";
            operation.map = op;
            operation.children = [];
            this.operations[index] = operation;
        });
        
        // this.assert(this.operation.map.uuid, 400, "Invalid operation without a uuid");
        // this.assert(!this._cached.alpha.hasOperation(this.operation.map.uuid), 400, `Operation already exists: ${this.operation.map.uuid}`);
    }

    async action() {
        console.log(`[Alpha] Adding operations to alpha-${this._cached.alpha._id}: ${this.operations}`);

        return this._cached.alpha.setOperations(this.operations)
            .then((alpha) => alpha.sendClient(this, 200, SecrecyLevel.public));
    }
}
