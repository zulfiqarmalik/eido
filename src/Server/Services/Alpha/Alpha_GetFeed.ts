import { Rest } from '../RouterUtil';
import { ApiPermissions } from 'Server/Services/Router';
import { SecrecyLevel } from 'Shared/Model/SecrecyLevel';

import { Alpha } from 'Server/Model/Config/SvAlpha';
import { CObject } from 'Shared/Core/CObject';
import { ObjectID } from 'Shared/Core/ObjectID';
import { ConfigType, AlphaType } from 'Shared/Model/Config/ShAlpha';
import { SortOrder } from 'Shared/Model/ShModel';
import { AlphaFeed } from 'Shared/Model/Config/AlphaFeed';
import { Strategy } from 'Server/Model/Config/SvStrategy';
import { AlphaOrStrategy } from 'Server/Model/Config/SvConfigBase';

@Rest.Post('[apiRoot][apiVersion]/alpha/feed', "Get the alpha/strategy feed.")
@Rest.LoggedIn()
@Rest.Permissions([ApiPermissions.portfolioManager])
@Rest.Returns<AlphaFeed<Alpha>>(new AlphaFeed<Alpha>("US"))
class AlphaGetFeed extends Rest.EndPoint {
    @Rest.Query("Are we looking for strategies by a particular owner?", null, false)
    ownerId: ObjectID = null;
    
    @Rest.Query("Market.", null, false)
    market: string = "US";
    
    @Rest.Query("The universeId that we want to search.", null, false)
    universeId: string = "";
    
    @Rest.Query("The starting index.", null, false)
    startId: number = 0;
    
    @Rest.Query("How many alphas to fetch.", null, false)
    count: number = 100;
    
    @Rest.Query("What to sort the alphas by.", null, false)
    sortBy: string = "ir";
    
    @Rest.Query("What is the order of the sort.", null, false)
    sortOrder: SortOrder = SortOrder.desc;
    
    @Rest.Query("What is the type of alpha we need. Empty means human alphas.", ConfigType, false)
    dbName: string = "pesa_us";
    
    @Rest.Query("What type of config do we want to load.", ConfigType, false)
    configType: ConfigType = ConfigType.user;
    
    @Rest.Query("Whether we're looking for Alpha or Strategy.")
    type: string = AlphaType.alpha;
    
    @Rest.Query("The delay.", null, false)
    delay: number = 1;
    
    @Rest.Query("Whether to get publised items or not.", null, false)
    isPublished: boolean = true;
    
    @Rest.Query("Search View.", null, false)
    searchView: string = "LT";

    advSearches: any[] = [];

    validate() {
        this.advSearches = this._req.body.advSearches;
        return true;
    }
    
    async action() { 
        let promise = null;

        if (this.type == AlphaType.alpha) {
            promise = AlphaOrStrategy.getAlphaFeed<Alpha>(Alpha, this, this.market, this.universeId, this.startId, this.count, this.sortBy, this.sortOrder, 
                this.ownerId, this.dbName, this.type, this.configType, this.delay, this.isPublished, this.searchView, this.advSearches)
        }
        else {
            promise = AlphaOrStrategy.getAlphaFeed<Strategy>(Strategy, this, this.market, this.universeId, this.startId, this.count, this.sortBy, this.sortOrder, 
                this.ownerId, this.dbName, this.type, ConfigType.any, this.delay, this.isPublished, this.searchView, this.advSearches)
        }

        promise.then((feed: AlphaFeed<AlphaOrStrategy>) => {
            feed.alphas.map((alpha) => console.log(`[Feed] ${alpha._id} - ${alpha.name}`));
            // CObject.sendClientRaw(args, 200, result);
            feed.sendClient(this, 200, SecrecyLevel.public);
        });

        // Alpha.getFeed(this, this.market, this.universeId, this.startId, this.count, this.sortBy, this.sortOrder, 
        //     this.ownerId, this.dbName, this.configType, this.delay, this.isPublished)
        //     .then((feed: AlphaFeed<Alpha>) => {
        //         feed.alphas.map((alpha) => console.log(`[Feed] ${alpha._id} - ${alpha.name}`));
        //         // CObject.sendClientRaw(args, 200, result);
        //         feed.sendClient(this, 200, SecrecyLevel.public);
        //     });
    }
}
