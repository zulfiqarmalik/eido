import { eido } from 'Shared/Core/ShCore';

import { Alpha } from 'Server/Model/Config/SvAlpha';
import "./Alpha_GetThumbnail";
import "./Alpha_Create";
import "./Alpha_Get";
import "./Alpha_GetAll";
import "./Alpha_Delete";
import "./Alpha_Update";
import "./Alpha_Publish";
import "./Alpha_RunTests";
import "./Alpha_Run";
import "./Alpha_Cancel";
import "./Alpha_Duplicate";

import "./Alpha_GetStats";
import "./Alpha_GetPositions";
import "./Alpha_GetPositionsNet";

import "./Alpha_Op_Set";
import "./Alpha_UpdateSettings";
import "./Alpha_Prune";
// import initAlpha_Op_Delete from "./Alpha_Op_Delete";

import "./Alpha_GetFeed";
import { ApiPermissionsCallbacks, ApiPermissions } from 'Server/Services/Router';

export default function initRouter(app) {
	ApiPermissionsCallbacks[ApiPermissions.alphaOwner] = Alpha.isOwner;
	ApiPermissionsCallbacks[ApiPermissions.alphaAccess] = Alpha.hasAccess;
};
