// import { eido } from 'Shared/Core/ShCore';
// import { RestDelete, RestEndPoint } from "Server/Services/RouterUtil";
// import { Params, Collections } from "Server/Model/SvModel";
// import { Alpha } from 'Server/Model/Config/SvAlpha';
// import { SecrecyLevel, Model } from 'Shared/Model/ShModel';
// import { Debug } from 'Shared/Core/ShUtil';
import { CObject } from 'Shared/Core/CObject';
// import { ConfigSection } from "Shared/Model/Config/ConfigSection";

// class AlphaOpDelete extends RestEndPoint {
//     static get info() {
//         return {
//             path: '[apiRoot][apiVersion]/alpha/op',
//             spec: {
//                 description: "Add an operation to the alpha.",
//                 type: 'Array',
//                 parameters: [
//                     Params.field("alphaId", "The friendly name of this alpha.", "string", true),
//                     Params.field("uuid", "The uuid of the operation to delete.", "string", true)
//                 ]
//             },

//             permissions: [eido.ApiPermissions.login, eido.ApiPermissions.alphaOwner],
//         }
//     }

//     constructor(args) {
//         super(args);
//     }

//     validate(args) {
//         args.assert(args._cached.alpha.hasOperation(args.uuid), 400, `Operation does not exist: ${args.uuid}`);
//     }

//     action(args) {
//         console.log(`[Alpha] Deleting operation: ${args.uuid}`);

//         args._cached.alpha.deleteOperation(args.uuid)
//             .then((alpha) => alpha.sendClient(args, 200, SecrecyLevel.public));
//     }
// }

// export default function init(app) {
//     new RestDelete(app, AlphaOpDelete);
// }
