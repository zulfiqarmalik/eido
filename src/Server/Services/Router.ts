import { eido } from 'Shared/Core/ShCore';
import { PromiseUtil } from 'Shared/Core/ShUtil';

export const enum ApiPermissions {
	none 			= '',
	public 			= 'public',
	login 			= 'login',

	alphaOwner 		= 'alphaOwner',
	alphaAccess 	= 'alphaAccess',

	strategyOwner 	= 'strategyOwner',
	strategyAccess 	= 'strategyAccess',

	portfolioManager= 'portfolioManager',
	cio 			= "cio",

	admin 			= 'admin',
	superUser 		= 'superUser'
};

export let ApiPermissionsCallbacks: object = {
	public: function () {
		return PromiseUtil.resolvedPromise(true);
	}
}

// eido.ApiPermissions.oneOf = function(list) {
// 	let id = 0;
// 	for (let i = 0; i < list.length; i++)
// 		id = id | list[i];

// 	eido.apiPermissionsCallbacks[id] = function() {

// 	};
// }

import initUserRouter from "./User/R_User";
// import initStratsRouter from "./Strats/R_Strats";
import initDataRouter from "./Data/R_Data";
import initAlphaRouter from "./Alpha/R_Alpha";
import initStrategyRouter from "./Strategy/R_Strategy";
import initTradeRouter from "./Trade/R_Trade";

export default function initRouter(app) {
	initUserRouter(app);
	initAlphaRouter(app);
	// initStratsRouter(app);
	initDataRouter(app);
	initStrategyRouter(app);
	initTradeRouter(app);
};