import { eido } from 'Shared/Core/ShCore';
import { Rest } from "Server/Services/RouterUtil";
import { Alpha } from 'Server/Model/Config/SvAlpha';
import { Strategy } from 'Server/Model/Config/SvStrategy';
import { Debug } from 'Shared/Core/ShUtil';
import { CObject } from 'Shared/Core/CObject';
import { ApiPermissions } from '../Router';
import { SecrecyLevel } from 'Shared/Model/SecrecyLevel';
import { ObjectID } from 'Shared/Core/ObjectID';
import { ShStrategy } from 'Shared/Model/Config/ShStrategy';

@Rest.Put('[apiRoot][apiVersion]/strategy/strategy', "Add a strategy to a master.")
@Rest.LoggedIn()
@Rest.Permissions([ApiPermissions.cio])
@Rest.Returns(Strategy)
class MasterStrategyAdd extends Rest.EndPoint {
    @Rest.Query("The ID of the master strategy.")
    masterId    : ObjectID = null;

    @Rest.Query("Strategy ID that will be added to the master.")
    strategyId  : ObjectID = null;

    @Rest.Query("Market.", null, true)
    market      : string = "US";
    
    @Rest.Query("Weight.", null, true)
    weight      : number = 1.0;
    
    preValidate() { return Promise.all([
            Strategy.validateMaster(this, this.masterId, this.market),
            Strategy.validate(this, this.strategyId)
        ]); 
    }
    
    validate() {
        this.assert(this._cached.master.hasStrategy(this.strategyId, ShStrategy.embeddedStrategyTypes) < 0, 400, `Strategy: ${this.strategyId} already added to MASTER: ${this.masterId}`);
        return true;
    }

    action() {
        console.log(`[Master] Adding strategy: ${this.strategyId} - To master: ${this.masterId}`);
        return this._cached.master.addStrategy(this._cached.strategy, this.weight)
            .then((strategy) => strategy.sendClient(this, 200, SecrecyLevel.public));
    }
}
