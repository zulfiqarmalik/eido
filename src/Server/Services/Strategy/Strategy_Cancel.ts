import { eido } from 'Shared/Core/ShCore';
import { Rest } from "Server/Services/RouterUtil";
import { ApiPermissions } from '../Router';
import { CObject } from 'Shared/Core/CObject';
import { ObjectID } from 'Shared/Core/ObjectID';
import { Strategy } from 'Server/Model/Config/SvStrategy';

@Rest.Delete('[apiRoot][apiVersion]/strategy/run', "Cancel the running of a strategy.")
@Rest.LoggedIn()
@Rest.Permissions([ApiPermissions.strategyOwner])
@Rest.Returns(Object)
class StrategyCancel extends Rest.EndPoint {
    @Rest.Query("Strategy ID")
    strategyId              : ObjectID = null;
    
    preValidate() { return Strategy.validate(this, this.strategyId); }

    action() {
        this._cached.strategy.cancel()
            .then(() => CObject.sendClientRaw(this, 200, {}));
    }
}
