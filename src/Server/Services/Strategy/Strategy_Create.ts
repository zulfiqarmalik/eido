import { eido } from 'Shared/Core/ShCore';
import { Rest } from "Server/Services/RouterUtil";
import { ApiPermissions } from '../Router';
import { CObject } from 'Shared/Core/CObject';
import { Strategy } from 'Server/Model/Config/SvStrategy';

@Rest.Put('[apiRoot][apiVersion]/strategy', "Create a new strategy and return the uuid.")
@Rest.LoggedIn()
@Rest.Permissions([ApiPermissions.portfolioManager])
@Rest.Returns(Strategy)
class StrategyPut extends Rest.EndPoint {
    @Rest.Query("Strategy Name")
    name: string = "";
    
    @Rest.Query("Category Name")
    category: string = "";
    
    @Rest.Query("Market Name")
    market: string = "";

    @Rest.Query("Universe Id")
    universeId: string = "";

    @Rest.Query("Ref config name")
    refConfigfilename: string = "";

    @Rest.Query("Delay")
    delay: number = 1;

    validate() {
        this.assert(this.market, 400, `Invalid market specified!`);
        this.assert(this.universeId, 400, `Invalid market specified!`);
        this.assert(this.category, 400, `Invalid category specified!`);
        this.assert(this.name, 400, `Invalid name specified!`);
        this.assert(this.refConfigfilename, 400, `Invalid refConfig specified!`);

        return Strategy.checkExists(this.userId(), this.name, this.market)
            .then((doesExist) => this.assert(!doesExist, 409, `[Strategy] Strategy already exists: ${this.name} - Market: ${this.market}`))
    }

    action() {
        console.log(`[Strategy] Creating Strategy: ${this.userId()}@${this.name}`);

        return (new Strategy()).createNew(this, this.userId(), this.name, this.category, this.market, this.universeId, false, this.refConfigfilename, this.delay)
            .then((strategy) => {
                strategy.sendClient(this, 201);
            });
    }
}
