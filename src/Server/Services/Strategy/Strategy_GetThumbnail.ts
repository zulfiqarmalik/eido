import { eido } from 'Shared/Core/ShCore';
import { Rest } from "Server/Services/RouterUtil";
import { ApiPermissions } from '../Router';
import { CObject } from 'Shared/Core/CObject';
import { Strategy } from 'Server/Model/Config/SvStrategy';
import { SecrecyLevel } from 'Shared/Model/SecrecyLevel';
import { Positions } from 'Shared/Model/Stats/Positions';
import { PromiseUtil } from 'Shared/Core/ShUtil';
import { SampleType } from 'Shared/Model/Config/ShAlpha';
import { DB, Collections } from 'Server/Model/SvModel';
import { Util } from 'Server/Core/SvUtil';
import { ObjectID } from 'Shared/Core/ObjectID';

@Rest.Get('[apiRoot][apiVersion]/strategy/thm', "Get the thumbnail for a particular strategy.")
@Rest.Permissions([ApiPermissions.public])
@Rest.Returns(String)
@Rest.Content("image/png")
class StrategyGetThumbnail extends Rest.EndPoint {
    @Rest.Query("Strategy ID")
    strategyId  : ObjectID = null;

    @Rest.Query("Market Name")
    market      : string = "US";

    @Rest.Query(`Sample name. Values = ${Object.keys(SampleType)}`, true)
    sampleName  : SampleType = SampleType.IS;

    @Rest.Query("The db to use.")
    dbName      : string = "pesa_us";

    preValidate() { return Strategy.validate(this, this.strategyId); }
    
    action() {
        console.log(`[Strategy] Getting thumbnail: ${this.dbName}@${this.strategyId} [Sample: ${this.sampleName}]`);
        return Util.sendThumbnail(this, this.strategyId, this._cached.strategy.getAssetClass())
            .catch(() => { 
                console.error(`[Alpha] Error getting thumbnail: ${this.dbName}@${this.strategyId} [Sample: ${this.sampleName}]`);
                this._res.status(404).send({});
            });
    }
}
