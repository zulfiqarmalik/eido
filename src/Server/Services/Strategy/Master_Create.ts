import { eido } from 'Shared/Core/ShCore';
import { Rest } from "Server/Services/RouterUtil";
import { ApiPermissions } from '../Router';
import { CObject } from 'Shared/Core/CObject';
import { Strategy } from 'Server/Model/Config/SvStrategy';

@Rest.Put('[apiRoot][apiVersion]/master', "Create a new master.")
@Rest.LoggedIn()
@Rest.Permissions([ApiPermissions.cio])
@Rest.Returns(Strategy)
class StrategyPut extends Rest.EndPoint {
    @Rest.Query("Strategy Name")
    name: string = "";
    
    @Rest.Query("Category Name")
    category: string = "";
    
    @Rest.Query("Market Name")
    market: string = "";

    @Rest.Query("Delay")
    delay: number = 1;


    validate() {
        return Strategy.checkExists(this.userId(), this.name, this.market)
            .then((doesExist) => this.assert(!doesExist, 409, `[Strategy] Strategy already exists: ${this.name} - Market: ${this.market}`))
    }

    action() {
        console.log(`[Strategy] Creating Strategy: ${this.userId()}@${this.name}`);
        
        return (new Strategy()).createNew(this, this.userId(), this.name, this.category, this.market, "", true, "", this.delay)
            .then((strategy) => {
                strategy.sendClient(this, 201);
            });
    }
}
