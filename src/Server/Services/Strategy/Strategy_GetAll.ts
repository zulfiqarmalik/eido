import { eido } from 'Shared/Core/ShCore';
import { Rest } from "Server/Services/RouterUtil";
import { ApiPermissions } from '../Router';
import { CObject } from 'Shared/Core/CObject';
import { Strategy } from 'Server/Model/Config/SvStrategy';
import { SecrecyLevel } from 'Shared/Model/SecrecyLevel';

@Rest.Get('[apiRoot][apiVersion]/strategy/all', "Get all strategies for the current user.")
@Rest.LoggedIn()
@Rest.Permissions([ApiPermissions.portfolioManager])
@Rest.Returns<Strategy[]>([])
class StrategyGetAll extends Rest.EndPoint {
    @Rest.Query("Market Name")
    market: string = "";

    @Rest.Query("Published")
    isPublished: boolean;

    action() { 
        let userId = this.userId();

        if (this._cached.user.isCIO())
            userId = null;

        Strategy.loadAllStrategies(userId, this.market, this.isPublished)
            .then((strategies: Strategy[]) => {
                console.log(`[Strategy] User: ${this.userId()} - Num strategies: ${strategies.length}`);
                let toSend: Strategy[] = strategies;/// strategies.filter((strategy) => strategy.isMaster && this._cached.user.isCIO() || !strategy.isMaster);
                (toSend as any).sendClient(this, SecrecyLevel.public);
            });
    }
}
