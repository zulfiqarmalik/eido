import { eido } from 'Shared/Core/ShCore';
import { Params, Collections, DB } from "Server/Model/SvModel";

import { Model } from 'Shared/Model/ShModel';
import { Debug, PromiseUtil } from 'Shared/Core/ShUtil';
import { Util } from 'Server/Core/SvUtil';
import { ShUser, AccountType, AccountStatus, Gender } from 'Shared/Model/User/ShUser';
import { Email } from 'Server/System/Email';
import { Error } from 'Server/Core/Error';
import { User } from "Server/Model/User/SvUser";
import { Rest } from '../RouterUtil';
import { ApiPermissions } from 'Server/Services/Router';
import { SecrecyLevel } from 'Shared/Model/SecrecyLevel';

import { Strategy } from 'Server/Model/Config/SvStrategy';
import { CObject } from 'Shared/Core/CObject';
import { ObjectID } from 'Shared/Core/ObjectID';
import { Positions } from 'Shared/Model/Stats/Positions';
import { LTStats } from 'Shared/Model/Stats/LTStats';
import { SampleType } from 'Shared/Model/Config/ShAlpha';
import { ConfigSection } from 'Shared/Model/Config/ConfigSection';

@Rest.Put('[apiRoot][apiVersion]/strategy/op', "Set the operations on this strategy. Note that OpNormalise is always implied!")
@Rest.LoggedIn()
@Rest.Permissions([ApiPermissions.alphaOwner])
@Rest.Returns(Object)
class AlphaOpSet extends Rest.EndPoint {
    @Rest.Query('Strategy ID')
    strategyId  : ObjectID = null;

    @Rest.Body("JSON encoded operation data which should be a ConfigSection object.")
    operations  : ConfigSection[];

    preValidate() { return Strategy.validate(this, this.strategyId); }

    validate() {
        this._error.assert(!this._cached.strategy.isPublished, 400, `[Strategy] Strategy: ${this._cached.strategy._id} has already been published! Modifying a published strategy is NOT allowed!`);
        
        // this.operation = (new ConfigSection()).copyFrom(this._req.body);
        this.operations = new Array(this._req.body.operations.length);
        this._req.body.operations.map((op, index) => { 
            let operation = new ConfigSection();
            operation.m_name = "Operation";
            operation.map = op;
            operation.children = [];
            this.operations[index] = operation;
        });
        
        // this.assert(this.operation.map.uuid, 400, "Invalid operation without a uuid");
        // this.assert(!this._cached.strategy.hasOperation(this.operation.map.uuid), 400, `Operation already exists: ${this.operation.map.uuid}`);
    }

    async action() {
        console.log(`[Strategy] Adding operations to strategy-${this._cached.strategy._id}: ${this.operations}`);

        return this._cached.strategy.setOperations(this.operations)
            .then((strategy) => strategy.sendClient(this, 200, SecrecyLevel.public));
    }
}
