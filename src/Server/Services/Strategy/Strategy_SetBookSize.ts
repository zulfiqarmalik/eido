import { eido } from 'Shared/Core/ShCore';
import { Rest } from "Server/Services/RouterUtil";
import { Alpha } from 'Server/Model/Config/SvAlpha';
import { Strategy } from 'Server/Model/Config/SvStrategy';
import { Debug } from 'Shared/Core/ShUtil';
import { CObject } from 'Shared/Core/CObject';
import { ApiPermissions } from '../Router';
import { SecrecyLevel } from 'Shared/Model/SecrecyLevel';
import { ObjectID } from 'Shared/Core/ObjectID';
import { ConfigType } from 'Shared/Model/Config/ShAlpha';

@Rest.Patch('[apiRoot][apiVersion]/strategy/booksize', "Set the book size of a master strategy.")
@Rest.LoggedIn()
@Rest.Permissions([ApiPermissions.strategyOwner])
@Rest.Returns(Strategy)
class StrategyAlphaAdd extends Rest.EndPoint {
    @Rest.Query("Strategy ID")
    strategyId  : ObjectID = null;

    @Rest.Query("DB Name.")
    dbName: string;

    @Rest.Query("Book size.")
    bookSize: number;

    preValidate() { return Strategy.validate(this, this.strategyId); }

    validate() {
        this.assert(this._cached.strategy.isMaster, 400, `BookSize can only be set on a master strategy: ${this.strategyId}`);
        this.assert(this.bookSize > 0, 400, `Invalid book size being set: ${this.strategyId} - ${this.bookSize}`);
        return true;
    }

    action() {
        console.log(`[Strategy] Updating book size: ${this._cached.strategy._id} - To: ${this.bookSize}`);
        return this._cached.strategy.setBookSize(this.bookSize)
            .then(() => CObject.sendClientRaw(this, 200, {}));
    }
}
