import { eido } from 'Shared/Core/ShCore';
import { Rest } from "Server/Services/RouterUtil";
import { ApiPermissions } from '../Router';
import { CObject } from 'Shared/Core/CObject';
import { Strategy } from 'Server/Model/Config/SvStrategy';
import { SecrecyLevel } from 'Shared/Model/SecrecyLevel';
import { Positions } from 'Shared/Model/Stats/Positions';
import { PromiseUtil } from 'Shared/Core/ShUtil';
import { ObjectID } from 'Shared/Core/ObjectID';

@Rest.Get('[apiRoot][apiVersion]/strategy/getPositions', "Get the position information on a particular day.")
@Rest.LoggedIn()
@Rest.Permissions([ApiPermissions.strategyAccess, ApiPermissions.portfolioManager])
@Rest.Returns(Positions)
class StrategyGetPositions extends Rest.EndPoint {
    @Rest.Query("Strategy ID")
    strategyId  : ObjectID = null;

    @Rest.Query("Market Name")
    market      : string = "US";

    @Rest.Query("Date")
    date        : number = 0;

    @Rest.Query("Load Per-Stock Stats")
    loadPss     : boolean = false;

    loadFromDisk() {
        this._cached.strategy.loadPositionsForDate(this.date).then((positions: Positions) => positions.sendClient(this, 200));
    }

    action() {
        return this.loadFromDisk();
    }
}

