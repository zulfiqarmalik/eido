import { eido } from 'Shared/Core/ShCore';
import { Rest } from "Server/Services/RouterUtil";
import { ApiPermissions } from '../Router';
import { CObject } from 'Shared/Core/CObject';
import { Strategy } from 'Server/Model/Config/SvStrategy';
import { SecrecyLevel } from 'Shared/Model/SecrecyLevel';
import { Positions } from 'Shared/Model/Stats/Positions';
import { PromiseUtil } from 'Shared/Core/ShUtil';
import { ObjectID } from 'Shared/Core/ObjectID';

@Rest.Get('[apiRoot][apiVersion]/strategy/getPositionsNet', "Load a range of positions from all the positions of a strategy and net them in a certain way.")
@Rest.LoggedIn()
@Rest.Permissions([ApiPermissions.strategyAccess, ApiPermissions.portfolioManager])
@Rest.Returns(Positions)
export class StrategyGetPositionsNet extends Rest.EndPoint {
    @Rest.Query("Strategy ID")
    strategyId              : ObjectID = null;

    @Rest.Query("Market Name")
    market: string = "US";

    @Rest.Query("Start Date")
    startDate: number = 0;

    @Rest.Query("End Date")
    endDate: number = 0;

    @Rest.Query("Calculate Positions?")
    calculatePos: boolean = true;
    
    @Rest.Query("Calculate Information Coefficient?")
    calculateIC: boolean = false;
    
    @Rest.Query("Force it to not run as a task but as a normal REST API.")
    noAsync: boolean = false;

    ////////////////////////////////////////////////////////////////////////////////////
    /// Not part of the payload
    ////////////////////////////////////////////////////////////////////////////////////
    _posDir: string

    preValidate() { return Strategy.validate(this, this.strategyId); }

    getId(): string { 
        return `${this._cached.user._id}_${this.strategyId}_[${this.startDate},${this.endDate}]_${this._posDir}`;
    }

    runTask() {
        return this._cached.strategy.loadAllPosInfo(this, this.startDate, this.endDate, this.calculatePos, this.calculateIC);
    }

    action() {
        if (this._noTask || this.noAsync) {
            return this.runTask().then((positions) => positions.sendClient(this, 200));
        }

        return this.createTask();
    }
}
