import { eido } from 'Shared/Core/ShCore';
import { Rest } from "Server/Services/RouterUtil";
import { Alpha } from 'Server/Model/Config/SvAlpha';
import { Strategy } from 'Server/Model/Config/SvStrategy';
import { Debug } from 'Shared/Core/ShUtil';
import { CObject } from 'Shared/Core/CObject';
import { ApiPermissions } from '../Router';
import { SecrecyLevel } from 'Shared/Model/SecrecyLevel';
import { ObjectID } from 'Shared/Core/ObjectID';

@Rest.Patch('[apiRoot][apiVersion]/strategy/alpha', "Add an alpha to an existing strategy.")
@Rest.LoggedIn()
@Rest.Permissions([ApiPermissions.strategyOwner])
@Rest.Returns(Strategy)
class StrategyAlphaUpdateWeight extends Rest.EndPoint {
    @Rest.Query("Strategy ID")
    strategyId  : ObjectID = null;

    @Rest.Query("Alpha ID")
    alphaId     : ObjectID = null;

    @Rest.Query("Reversion.")
    reversion   : boolean = false;

    @Rest.Query("Weight.")
    weight      : number = 1.0;

    preValidate() { return Strategy.validate(this, this.strategyId); }

    validate() {
        this._error.assert(!this._cached.strategy.isPublished, 400, `[Strategy] Strategy: ${this._cached.strategy._id} has already been published! Modifying a published strategies is NOT allowed!`);
    }
    
    action() {
        console.log(`[Strategy] Updating alpha weight: ${this.alphaId} = ${this.weight.toFixed(2)} - In strategy: ${this._cached.strategy._id}`);
        return this._cached.strategy.updateWeight(this, this.alphaId, this.weight, this.reversion)
            .then((strategy) => CObject.sendClientRaw(this, 200, {}));
    }
}
