import { eido } from 'Shared/Core/ShCore';
import { Rest } from "Server/Services/RouterUtil";
import { ApiPermissions } from '../Router';
import { CObject } from 'Shared/Core/CObject';
import { Strategy } from 'Server/Model/Config/SvStrategy';
import { ObjectID } from 'Shared/Core/ObjectID';

@Rest.Delete('[apiRoot][apiVersion]/strategy', "Delete an existing strategy.")
@Rest.LoggedIn()
@Rest.Permissions([ApiPermissions.strategyOwner])
@Rest.Returns(Object)
class StrategyDelete extends Rest.EndPoint {
    @Rest.Query("Strategy ID")
    strategyId              : ObjectID = null;

    preValidate() { return Strategy.validate(this, this.strategyId); }
    
    action() {
        return this._cached.strategy.delete()
            .then(() => CObject.sendClientRaw(this, 200, { _id: this.strategyId }) );
    }
}
