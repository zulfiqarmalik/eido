import { eido } from 'Shared/Core/ShCore';
import { Rest } from "Server/Services/RouterUtil";
import { Alpha } from 'Server/Model/Config/SvAlpha';
import { Strategy } from 'Server/Model/Config/SvStrategy';
import { Debug } from 'Shared/Core/ShUtil';
import { CObject } from 'Shared/Core/CObject';
import { ApiPermissions } from '../Router';
import { SecrecyLevel } from 'Shared/Model/SecrecyLevel';
import { ObjectID } from 'Shared/Core/ObjectID';
import { ConfigType } from 'Shared/Model/Config/ShAlpha';

@Rest.Put('[apiRoot][apiVersion]/strategy/alpha', "Add an alpha to an existing strategy.")
@Rest.LoggedIn()
@Rest.Permissions([ApiPermissions.strategyOwner])
@Rest.Returns(Strategy)
class StrategyAlphaAdd extends Rest.EndPoint {
    @Rest.Query("Strategy ID")
    strategyId  : ObjectID = null;

    @Rest.Query("Alpha ID")
    alphaId     : ObjectID = null;

    @Rest.Query("Weight.", null, false)
    weight      : number = 1.0;

    @Rest.Query("Reversion.", null, false)
    reversion   : boolean = false;

    @Rest.Query("DB Name.")
    dbName: string;

    @Rest.Query("What type of config do we want to load.", ConfigType, false)
    configType: ConfigType = ConfigType.any;
    
    preValidate() { return Strategy.validate(this, this.strategyId); }

    validate() {
        this.assert(this._cached.strategy.hasAlpha(this.alphaId) < 0, 400, `Alpha already added to strategy: ${this.alphaId}`);
        return Alpha.validate(this, this.alphaId, this.dbName, this.configType);
    }

    action() {
        console.log(`[Strategy] Adding alpha: ${this._cached.alpha._id} - To strategy: ${this._cached.strategy._id}`);
        return this._cached.strategy.addAlpha(this._cached.alpha, this.weight, this.reversion)
            .then((strategy) => strategy.sendClient(this, 200, SecrecyLevel.public));
    }
}
