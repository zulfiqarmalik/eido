import { Rest } from "Server/Services/RouterUtil";
import { ApiPermissions } from '../Router';
import { Strategy } from 'Server/Model/Config/SvStrategy';
import { ObjectID } from 'Shared/Core/ObjectID';
import { RawStatsType } from 'Shared/Model/Config/ShStrategy';
import { ConfigType } from "Shared/Model/Config/ShAlpha";
import { CObject } from "Shared/Core/CObject";

@Rest.Get('[apiRoot][apiVersion]/strategy/getStats', "Get the stats of this strategy")
@Rest.LoggedIn()
@Rest.Permissions([ApiPermissions.strategyAccess, ApiPermissions.portfolioManager])
class StrategyGetStats extends Rest.EndPoint {
    @Rest.Query("Strategy ID")
    strategyId  : ObjectID = null;

    @Rest.Query("DB Name.", null, false)
    dbName: string;

    @Rest.Query("Load Raw", null, false)
    rawStatsType: RawStatsType = RawStatsType.normal;
    
    @Rest.Query("What type of config do we want to load.", ConfigType, false)
    configType: ConfigType = ConfigType.any;
    
    preValidate() { return Strategy.validate(this, this.strategyId, this.dbName, this.configType); }

    loadFromDisk() {
        let promise = null;

        if (this.rawStatsType == RawStatsType.normal)
            promise = this._cached.strategy.loadLTStats();
        else
            promise = this._cached.strategy.loadRawLTStats(this.rawStatsType);

        return promise.then((ltStats) => CObject.sendClientRaw(this, 200, ltStats));
        // return this._cached.strategy.loadLTStats()
        //     .then((ltStats) => {
        //         this._cached.strategy.loadRawLTStats(this.rawStatsType)
        //     console.log(`[StrategyStats] Sending stats for strategy: ${this.strategyId}`)
        //     // ltStats.sendClient(this, 200)
        //     CObject.sendClientRaw(this, 200, ltStats);
        //     console.log(`[StrategyStats] Sent stats for strategy: ${this.strategyId}`)
        // });
    }

    action() {
        return this.loadFromDisk();
    }
}
