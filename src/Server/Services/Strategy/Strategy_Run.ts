import { eido } from 'Shared/Core/ShCore';
import { Params, Collections, DB } from "Server/Model/SvModel";

import { Model } from 'Shared/Model/ShModel';
import { Debug, PromiseUtil } from 'Shared/Core/ShUtil';
import { Util } from 'Server/Core/SvUtil';
import { ShUser, AccountType, AccountStatus, Gender } from 'Shared/Model/User/ShUser';
import { Email } from 'Server/System/Email';
import { Error } from 'Server/Core/Error';
import { User } from "Server/Model/User/SvUser";
import { Rest } from '../RouterUtil';
import { ApiPermissions } from 'Server/Services/Router';
import { SecrecyLevel } from 'Shared/Model/SecrecyLevel';

import { Alpha } from 'Server/Model/Config/SvAlpha';
import { CObject } from 'Shared/Core/CObject';
import { ObjectID } from 'Shared/Core/ObjectID';
import { Positions } from 'Shared/Model/Stats/Positions';
import { LTStats } from 'Shared/Model/Stats/LTStats';
import { SampleType } from 'Shared/Model/Config/ShAlpha';
import { ConfigSection } from 'Shared/Model/Config/ConfigSection';
import { Strategy } from '../../Model/Config/SvStrategy';

@Rest.Put('[apiRoot][apiVersion]/strategy/run', "Run a simulation of the alpha")
@Rest.LoggedIn()
@Rest.Permissions([ApiPermissions.strategyAccess, ApiPermissions.portfolioManager])
@Rest.Returns(Object)
class AlphaRun extends Rest.EndPoint {
    @Rest.Query("Strategy ID")
    strategyId              : ObjectID;

    @Rest.Query("Market Name")
    market: string = "US";

    preValidate() { return Strategy.validate(this, this.strategyId); }

    validate() {
        // args.assert(!args._cached.alpha.isRunning, 400, "Alpha is already running!");
        // let ws = args._appServer.wsServer.getWebSocket(args.userId().toString());
        // args.assert(ws, 500, "No websocket. Cannot start ")

        /// Cancel any existing run 
        // return args._cached.alpha.cancel();
        return this._cached.strategy.clearStatus();
    }

    action() {
        this._cached.strategy.run(this);
    }
}
