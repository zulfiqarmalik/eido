import { eido } from 'Shared/Core/ShCore';

import { Strategy } from 'Server/Model/Config/SvStrategy';
import "./Strategy_Create";
import "./Strategy_GetAll";
import "./Strategy_Delete";

import "./Strategy_AlphaAdd";
import "./Strategy_AlphaDelete";
import "./Strategy_GetThumbnail";

import "./Strategy_Run";
import "./Strategy_Cancel";

import "./Strategy_GetStats";
import "./Strategy_GetPositions";
import "./Strategy_GetPositionsNet";
import "./Strategy_AlphaUpdateWeight";
import "./Strategy_SetBookSize";

import "./Strategy_Op_Set"

import "./Master_Create";
import "./Master_StrategyAdd";

import { ApiPermissionsCallbacks, ApiPermissions } from 'Server/Services/Router';

export default function initRouter(app) {
	ApiPermissionsCallbacks[ApiPermissions.strategyOwner] = Strategy.isOwner;
	ApiPermissionsCallbacks[ApiPermissions.strategyAccess] = Strategy.hasAccess;
};
