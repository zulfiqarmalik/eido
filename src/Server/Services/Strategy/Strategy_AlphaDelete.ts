import { eido } from 'Shared/Core/ShCore';
import { Rest } from "Server/Services/RouterUtil";
import { Alpha } from 'Server/Model/Config/SvAlpha';
import { Strategy } from 'Server/Model/Config/SvStrategy';
import { Debug } from 'Shared/Core/ShUtil';
import { CObject } from 'Shared/Core/CObject';
import { ApiPermissions } from '../Router';
import { SecrecyLevel } from 'Shared/Model/SecrecyLevel';
import { ObjectID } from 'Shared/Core/ObjectID';

@Rest.Delete('[apiRoot][apiVersion]/strategy/alpha', "Deletes an alpha from a strategy.")
@Rest.LoggedIn()
@Rest.Permissions([ApiPermissions.strategyOwner])
@Rest.Returns(Strategy)
class StrategyAlphaDelete extends Rest.EndPoint {
    @Rest.Query("Strategy ID")
    strategyId              : ObjectID = null;

    @Rest.Query("Alpha ID")
    alphaId                 : ObjectID = null;

    preValidate() { return Strategy.validate(this, this.strategyId); }

    validate() {
        this.assert(this._cached.strategy.hasAlpha(this.alphaId) >= 0, 400, `Alpha not in strategy: ${this.alphaId}`);
    }

    action() {
        console.log(`[Strategy] Removing alpha: ${this.alphaId} - From strategy: ${this._cached.strategy._id}`);
        return this._cached.strategy.removeAlpha(this.alphaId)
            .then(() => CObject.sendClientRaw(this, 200, {}));
    }
}
