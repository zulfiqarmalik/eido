var resetPassword = isinr.RestClass({
    path: '[apiRoot][apiVersion]/user/verify',
    spec: {
        description: "Verify a user account.",
        parameters: [
            isinr.Params.query("email", "Email address of the user", "string", true),
            isinr.Params.query("verificatonCode", "Verification code that was sent in email", "string", true)
        ]
    },
    
    validate: function(args) {
        return isinr.User.loadFromEmail(args.email)
            .then(function(user) {
                args._error.assert(user, 404, "Unable to load user:", args.email);
                args._cached.user = user;
                return user;
            });
    },

    action: function(args) {
        return args._cached.user.verifyAccount(args.verificationCode)
            .then(function() {
                this._res.status(200).send({});
            }.bind(args));
    },

    // ****************************************************************************************************************
    // ----------------------------------------------------------------------------------------------------------------
    //                                                     TESTS
    // ----------------------------------------------------------------------------------------------------------------
    // ****************************************************************************************************************
});

module.exports = function(app) {
    app.put(resetPassword);
};
