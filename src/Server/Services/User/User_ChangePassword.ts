import { eido } from 'Shared/Core/ShCore';
import { Params, Collections } from "Server/Model/SvModel";

import { Model } from 'Shared/Model/ShModel';
import { Debug } from 'Shared/Core/ShUtil';
import { Util } from 'Server/Core/SvUtil';
import { ShUser, AccountType, AccountStatus, Gender } from 'Shared/Model/User/ShUser';
import { Email } from 'Server/System/Email';
import { Error } from 'Server/Core/Error';
import { User } from "Server/Model/User/SvUser";
import { Rest } from '../RouterUtil';
import { ApiPermissions } from 'Server/Services/Router';
import { SecrecyLevel } from 'Shared/Model/SecrecyLevel';

@Rest.Post('[apiRoot][apiVersion]/user/password', "Change the password of the currently logged in user.")
@Rest.LoggedIn()
@Rest.Returns(User)
class UserChangePassword extends Rest.EndPoint {
    @Rest.Query('Old Password')
    oldPassword             : string = ""; 

    @Rest.Query('New Password')
    newPassword             : string = "";

    action() {
        return this._cached.user.changePassword(this, this.oldPassword, this.newPassword)
            .then(() => this._res.status(200).send({}));
    }
}

