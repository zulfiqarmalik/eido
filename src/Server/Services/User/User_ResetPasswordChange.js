var resetPasswordChange = isinr.RestClass({
    path: '[apiRoot][apiVersion]/user/resetpassword_change',
    spec: {
        description: "Change password for a user for who a reset token has been generated.",
        parameters: [
            isinr.Params.query("email", "Email address of the user", "string", true),
            isinr.Params.query("verificationCode", "Verification code that was sent in email", "string", true),
            isinr.Params.query("password", "New Password", "string", true)
        ]
    },
    
    validate: function(args) {
        return isinr.User.loadFromEmail(args.email)
            .then(function(user) {
                this._error.assert(user, 404, "Unable to load user:", this.email);
                this._cached.user = user;
                return user;
            }.bind(args));
    },

    action: function(args) {
        return args._cached.user.resetPasswordChange(args.verificationCode, args.password)
            .then(function() {
                this._res.status(200).send({});
            }.bind(args));
    },

    // ****************************************************************************************************************
    // ----------------------------------------------------------------------------------------------------------------
    //                                                     TESTS
    // ----------------------------------------------------------------------------------------------------------------
    // ****************************************************************************************************************
    'Test:Reset Password Change': {
        priority: isinr.TestPriority.normal, // normal test priority
        after: "Reset Password",
        
        'Reset password change': function(test) {
            return isinr.User.getResetPasswordVerificationCode(test.client(12).email())
                .then(function(verificationCode) {
                    test.expect(verificationCode).isNotEmpty();

                    return test.put({
                        email: test.client(12).email(),
                        verificationCode: verificationCode,
                        password: 'resetpassword'
                    }); 
                })
                .then(function(data) {
                    test.expect(data).checkResponse('Unable to reset password!');
                });
        }
    }
});

module.exports = function(app) {
    app.put(resetPasswordChange);
};
