import { Rest } from "Server/Services/RouterUtil";
import { ServerArgs } from "../../Core/ServerArgs";
import { CObject } from "Shared/Core/CObject";

@Rest.Get('[apiRoot][apiVersion]/UserSetting', "Get a user setting.")
@Rest.LoggedIn()
@Rest.Returns(Object)
class UserSetting_Get extends Rest.EndPoint {
    @Rest.Query("Which key are we getting.")
    key: string = "";
    
    @Rest.Query("What is the default value.")
    defaultValue: string = "";
    
    action() {
        return this._cached.user.loadSetting(this as ServerArgs, this.key, this.defaultValue)
            .then((value: string) => {
                CObject.sendClientRaw(this as any, 200, {
                    key: this.key,
                    value: value
                });
            });
    }
}
