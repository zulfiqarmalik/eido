var facebookLogin = isinr.RestClass({
    path: '[apiRoot][apiVersion]/user/social',
    spec: {
        description: "Login a user using facebook information.",
        type: 'User',
        parameters: [
            isinr.Params.field("id", "Social ID", "string", true),
            isinr.Params.field("name", "Name", "string", true),
            isinr.Params.arrayField("emails", "Email", "string", true), // can be an array of emails
            isinr.Params.field("gender", "Email", "string", true),
            isinr.Params.field("profileImage", "Profile Picture", "string", true),
            isinr.Params.field("authInfo", "Auth information", "string", true),
            isinr.Params.field("deviceId", "DeviceId", "string", true),
            isinr.Params.field("accountType", "Account Type", "string", true, isinr.AccountType.values(), isinr.AccountType.facebook),
        ]
    },

    permissions: isinr.ApiPermissions.public,

    validate: function(args) {
        args.authInfo = JSON.parse(args.authInfo);

        return isinr.User.tryLoadUser(args.emails, args.deviceId)
            .then(function(user) {
                this._cached.user = user;
                return user;
            }.bind(args));
    },

    action: function(args) {
        var promise = null;
        var statusCode = 200;

        if (args._cached.user) {
            console.info('[User] Updating existing user account with social info:', args);
            promise = args._cached.user.updateExternalAccountInfo(args, args.accountType);
        }
        else {
            console.info('[User] Creating new account from social info:', args);
            promise = (new isinr.User()).createFromExternalAccount(args.deviceId, args, args.accountType);
        }

        return promise
            .then(function(user) {
                this._cached.user = user;
                return this.login(args);
            }.bind(args))
            .then(function(user) {
                user.sendClient(this, statusCode, isinr.SecrecyLevel.private);
            }.bind(args));
    },

    // ****************************************************************************************************************
    // ----------------------------------------------------------------------------------------------------------------
    //                                                     TESTS
    // ----------------------------------------------------------------------------------------------------------------
    // ****************************************************************************************************************
});

module.exports = function(app) {
    app.post(facebookLogin);
};
