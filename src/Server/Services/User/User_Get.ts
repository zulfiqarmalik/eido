import { User } from "Server/Model/User/SvUser";
import { Rest } from '../RouterUtil';
import { SecrecyLevel } from 'Shared/Model/SecrecyLevel';
import { ObjectID } from 'Shared/Core/ObjectID';

@Rest.Get('[apiRoot][apiVersion]/user', "Login a user and sets a session cookie. Alternatively this creates a new user as well.")
@Rest.LoggedIn()
@Rest.Returns(User)
class UserGet extends Rest.EndPoint {
    @Rest.Query("ID", null, false)
    id              : ObjectID;

    action() {
        // the user is already logged in ... so just send the current user
        if (!this.id) {
            this._error.assert(this._cached.user, 400, `[User] No user loaded!`);
            return this._cached.user.sendClient(this, 200, SecrecyLevel.public);
        }

        this._error.assert(this._cached.user.isAdminOrBetter(), 401, `Access Denied: User does not have admin privileges: ${this._cached.user.email}`);

        console.trace(`[User] Getting user from id: ${this.id}`);

        // if a valid id has been specified then we send the user information ...
        return User.loadFromId(this.id)
            .then((user) => {
                // no user was found ...
                if (!user) {
                    console.log(`[User] Unable to send user with id: ${this.id}`);
                    return this._res.status(404).send({});
                }

                // otherwise we just send the public informatino for the user
                return user.sendClient(this, 200, SecrecyLevel.public);
            });
    }

    // ****************************************************************************************************************
    // ----------------------------------------------------------------------------------------------------------------
    //                                                     TESTS
    // ----------------------------------------------------------------------------------------------------------------
    // ****************************************************************************************************************
    // 'Test:get user': {
    //     after: 'Login',

    //     'session validate'(test) {
            
    //     }
    // }
}
