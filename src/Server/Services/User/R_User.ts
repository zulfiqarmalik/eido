import { eido } from 'Shared/Core/ShCore';
import { User } from "Server/Model/User/SvUser";
// import initUserCreate from "./User_Create";

import './User_Create';
import './User_Login';
import './User_Get';
import './User_ChangePassword';
import './User_Logout';
import './UserSetting_Get';
import './UserSetting_Set';
import { WSConnection } from 'Server/Model/WSConnection';
import { ApiPermissions, ApiPermissionsCallbacks } from '../Router'; 
import { ObjectID } from 'Shared/Core/ObjectID';
import { Debug } from 'Shared/Core/ShUtil';

let passport = require('passport');
let LocalStrategy = require('passport-local').Strategy;

let g_userCache = {};

passport.use(new LocalStrategy({
    usernameField: 'email',
    passwordField: 'password'
}, (email: string, password: string, done: any) => {
    console.info(`[User] Logging in user: ${email}`);
    User.loadFromEmail(email)
        .then((user: User) => {
            let didLogin = user.matchPassword(password);
            if (didLogin) {
                console.info(`[User] Password match: ${email}`);
                done(null, user);
            }
            else {
                console.info(`[User] Password mismatch: ${email}`);
                done(null, false, { 
                    message: 'Incorrect email or password'
                });                
            }
        })
        .catch((err) => done(err, null));
}));

passport.serializeUser((user, done) => {
    console.log(`[Session] Serialising: ${user._id}`);
    done(null, `${user._id}`);
});

passport.deserializeUser((id, done) => {
    console.trace(`[Session] De-serialising: ${id} => ${id}`);

    let idStr = id.toString();

    if (g_userCache[idStr]) {
        let user = g_userCache[idStr];
        console.trace(`[Session] Got cached user: ${user._id}`);
        done(null, user);
        return;
    }

    // console.info(`[User] Deserialising user: ${id}`);
    User.loadFromId(ObjectID.create(idStr))
        .then((user: User) => {
            if (!user) {
                done(`Error loading user from session: ${id}`);
                return;
            }

            console.log(`[Session] Got user: ${user._id}`);
            g_userCache[idStr] = user;
            done(null, user)
        })
        .catch((err) => {
            console.error(`${JSON.stringify(err)}`);
            done(err, null)
        });
});

export default function initRouter(app) {
    ApiPermissionsCallbacks[ApiPermissions.login]               = User.isUserLoggedIn;
    ApiPermissionsCallbacks[ApiPermissions.portfolioManager]    = User.isLoggedInUserPortfolioManager;
    ApiPermissionsCallbacks[ApiPermissions.cio]                 = User.isLoggedInUserCIO;
    ApiPermissionsCallbacks[ApiPermissions.admin]               = User.isLoggedInUserAdmin;
    ApiPermissionsCallbacks[ApiPermissions.superUser]           = User.isLoggedInUserSuperUser;

    // initUserCreate(app);
    // initUserGet(app);
    // initUserChangePassword(app);
    // initUserLogout(app);

    ///////////////////////////////////////////////////////////////////////////////////////
    WSConnection.s_wsConnect.add((wsConn) => {
        User.loadFromId(ObjectID.create(wsConn.ws.id))
            .then((user) => {
                if (!user)
                    return;

                user.setOnline(true);
            })
            .catch((err) => {
                console.error("Error:", err);
            });
    });

    WSConnection.s_wsDisconnect.add((wsConn) => {
        if (!wsConn || !wsConn.ws)
            return;
            
        User.loadFromId(ObjectID.create(wsConn.ws.id))
            .then((user) => {
                if (!user)
                    return;

                user.setOnline(false);
            })
            .catch(function(err) {
                console.error("Error:", err);
            });
    });
};

// module.exports = function(app) {
//     eido.apiPermissionsCallbacks[eido.ApiPermissions.login]     = User.isUserLoggedIn;
//     eido.apiPermissionsCallbacks[eido.ApiPermissions.admin]     = User.isLoggedInUserAdmin;
//     eido.apiPermissionsCallbacks[eido.ApiPermissions.superUser] = User.isLoggedInUserSuperUser;

//     initUserLogin(app);

//     // if (!app.isTms()) {
//     //     app.addRestEndPoint(require('./user_login.js'), "User");
//     //     require('./user_login_social.js')(app);
//     //     require('./user_logout.js')(app);
//     //     require('./user_get.js')(app);
//     //     require('./user_set_profileimage.js')(app);
//     //     require('./user_changepassword.js')(app);
//     //     require('./user_update.js')(app);
//     //     require('./user_reset_password.js')(app);
//     //     require('./user_reset_password_change.js')(app);

//     //     require('./prefs/s_prefs.js')(app);

//     //     require('./notifications/notifications_get.js')(app);

//     //     app.swagger.configureDeclaration("user", {
//     //         description: "Operations about users and their accounts",
//     //         authorizations: ["isinr"],
//     //         produces: ["application/json"]
//     //     });
//     // }

//     // eido.WSConnection.s_wsConnect.add(function(wsConn) {
//     //     User.loadFromId(wsConn.ws.id)
//     //         .then(function(user) {
//     //             if (!user)
//     //                 return;

//     //             user.setOnline(true);
//     //         })
//     //         .catch(function(err) {
//     //             console.error("Error:", err);
//     //         });
//     // });

//     // eido.WSConnection.s_wsDisconnect.add(function(wsConn) {
//     //     User.loadFromId(wsConn.ws.id)
//     //         .then(function(user) {
//     //             if (!user)
//     //                 return;

//     //             user.setOnline(false);
//     //         })
//     //         .catch(function(err) {
//     //             console.error("Error:", err);
//     //         });
//     // });
// };
