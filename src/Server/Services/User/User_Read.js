module.exports = isinr.RestGet({
    path: '[apiRoot][apiVersion]/user/read',
    spec: {
        description: "Read a user based on userId.",
        type: 'User',
        parameters: [
            isinr.Params.query("userId", "User Id", "string", true)
        ],

        permissions: eido.ApiPermissions.login
    },

    permissions: isinr.ApiPermissions.login, // User needs to be logged in for this

    validate: function(args) {
        return isinr.User.validate(args);
    },

    action: function(args) {
        return args._cached.targetUser.sendClient(args, 200, isinr.SecrecyLevel.public);
    },

    // ****************************************************************************************************************
    // ----------------------------------------------------------------------------------------------------------------
    //                                                     TESTS
    // ----------------------------------------------------------------------------------------------------------------
    // ****************************************************************************************************************
    // 'Test:get user': {
    //     after: 'Login',

    //     'session validate': function(test) {
            
    //     }
    // }
});

