import { User } from "Server/Model/User/SvUser";
import { Rest } from '../RouterUtil';
import { ApiPermissions } from 'Server/Services/Router';
import { SecrecyLevel } from 'Shared/Model/SecrecyLevel';
import { CObject } from "Shared/Core/CObject";

@Rest.Put('[apiRoot][apiVersion]/user', "Create a new user with a password.")
@Rest.Permissions(ApiPermissions.login) /// The middle ware is going to handle the logging in before the handler is executed
@Rest.Returns(User)
class UserRegister extends Rest.EndPoint {
    @Rest.Query("Email")
    email               : string;

    @Rest.Query("Password")
    password            : string;

    validate() {
        this._error.assert(this.password, 400, "Invalid password specified");
        this._error.assert(this.email, 400, "Invalid email specified");

        return User.loadFromEmail(this.email)
            .then((user: User) => {
                this._error.assert(!user, 409, `User already exists: ${this.email}`);
            });
    }

    action() {
        return (new User()).create(this as any)
            .then((user: User) => {
                CObject.sendClientRaw(this as any, 201, {});
            });
    }
}
