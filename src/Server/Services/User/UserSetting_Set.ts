import { Rest } from "Server/Services/RouterUtil";
import { ServerArgs } from "../../Core/ServerArgs";
import { CObject } from "Shared/Core/CObject";

@Rest.Put('[apiRoot][apiVersion]/UserSetting', "Get a user setting.")
@Rest.LoggedIn()
@Rest.Returns(Object)
class UserSetting_Set extends Rest.EndPoint {
    @Rest.Query("Which key are we getting.")
    key: string = "";
    
    @Rest.Query("What is the value.")
    value: string = "";
    
    action() {
        return this._cached.user.saveSetting(this as ServerArgs, this.key, this.value)
            .then(() => CObject.sendClientRaw(this as any, 200, {}))
            .catch(() => this._error.internalServerError("Error setting user setting!"));
    }
}
