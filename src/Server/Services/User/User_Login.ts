import { Rest } from '../RouterUtil';
import { ApiPermissions } from 'Server/Services/Router';
import { SecrecyLevel } from 'Shared/Model/SecrecyLevel';
import { User } from 'Server/Model/User/SvUser';

let passport = require('passport');

@Rest.Post('[apiRoot][apiVersion]/user', "Login a user and sets a session cookie. Alternatively this creates a new user as well.")
@Rest.Permissions(ApiPermissions.login) /// The middle ware is going to handle the logging in before the handler is executed
@Rest.Returns(User)
@Rest.MiddleWare(passport.authenticate('local'))
class UserLogin extends Rest.EndPoint {
    @Rest.Query("Email", false)
    email               : string;

    @Rest.Query("Password", false)
    password            : string;

    action() {
        this._error.assert(this._cached.user, 400, `[User] No user loaded!`);
        console.info(`[User] User logged in. Sending confirmation: ${this._cached.user.email} - ${this._cached.user._id}`);
        this._cached.user.send(this, 200, SecrecyLevel.public);
    }
}
