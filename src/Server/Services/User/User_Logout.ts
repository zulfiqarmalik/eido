import { eido } from 'Shared/Core/ShCore';
import { Params, Collections } from "Server/Model/SvModel";

import { Model } from 'Shared/Model/ShModel';
import { Debug } from 'Shared/Core/ShUtil';
import { Util } from 'Server/Core/SvUtil';
import { ShUser, AccountType, AccountStatus, Gender } from 'Shared/Model/User/ShUser';
import { Email } from 'Server/System/Email';
import { Error } from 'Server/Core/Error';
import { User } from "Server/Model/User/SvUser";
import { Rest } from '../RouterUtil';
import { ApiPermissions } from 'Server/Services/Router';
import { SecrecyLevel } from 'Shared/Model/SecrecyLevel';
import { CObject } from 'Shared/Core/CObject';

@Rest.Delete('[apiRoot][apiVersion]/user', "Logout the user and reset the session cookie.")
@Rest.LoggedIn()
@Rest.Returns(User)
class UserLogout extends Rest.EndPoint {
    action() {
        this._cached.user.logout(this);
        CObject.sendClientRaw(this, 200, {});
    }

    // ****************************************************************************************************************
    // ----------------------------------------------------------------------------------------------------------------
    //                                                     TESTS
    // ----------------------------------------------------------------------------------------------------------------
    // ****************************************************************************************************************
    // 'Test:Logout': {
    //     priority: isinr.TestPriority.min + 1, // has to be the last test to run

    //     'logout'(test) {
    //         test.done();
    //     }
    // }
}
