module.exports = isinr.RestPatch({
    path: '[apiRoot][apiVersion]/user',
    spec: {
        description: "Update a user profile Request must be done by the logged in user.",
        type: 'User',
        parameters: [
            isinr.Params.field("diff", "Flat Diff", "object", true)
        ]
    },

    permissions: isinr.ApiPermissions.login, // User needs to be logged in for this

    action: function(args) {
        var diffValid = false;
        for (var i in args.diff) {
            if (args.diff.hasOwnProperty(i)) {
                diffValid = true;
                break;
            }
        }

        if (!diffValid) {
            console.trace("Diff is empty. Nothing to update ...");
            args._cached.user.sendClient(args, 200, isinr.SecrecyLevel.private);
            return;
        }

        console.trace("Updating user from diff:", args.diff);

        return args._cached.user.updateFromDiff(args.diff)
            .then(function(user) {
                user.sendClient(this, 200, isinr.SecrecyLevel.private);
            }.bind(args));
    },

    // ****************************************************************************************************************
    // ----------------------------------------------------------------------------------------------------------------
    //                                                     TESTS
    // ----------------------------------------------------------------------------------------------------------------
    // ****************************************************************************************************************
    'Test:User Update': {
        priority: isinr.TestPriority.max - 1, // max is the highest priority
        after: "Login Crowd", // run it after the user has logged in

        'Update user profiles': function(test) {
            test.log('Updating user profiles ...');

            var promises = new Array(test.numClients());
            var maleCount = Math.ceil(test.numClients() * 0.5);
            var femaleCount = test.numClients() - maleCount;
            var startBirthDate = Date.yearsInPast(65);
            var endBirthDate = Date.yearsInPast(18);

            test.log("Num males:", maleCount);
            test.log("Num females:", femaleCount);

            for (var i = 0; i < test.numClients(); i++) {
                var client = test.client(i);
                var user = client.user;
                var index = i.toString();
                var name = "testuser-" + index;
                var country = test.getRandomCountry();

                user.name = name;

                isinr.Debug.assert(country, 'Invalid country returned!');

                user.location = isinr.TestUtil.getAnyRandomLocation(); 

                var diff = {
                    name: name,
                    gender: i <= maleCount ? isinr.Gender.male : isinr.Gender.female,
                    location: user.location,
                    dateOfBirth: Date.rand(startBirthDate, endBirthDate),
                    address: {
                        streetAddress: "House " + index + ", Test Street",
                        city: "Test City-" + index,
                        state: "Test State-" + index,
                        country: country.name,
                        phone: "0987654321-" + index,
                        type: isinr.LocationType.private
                    }
                }

                var promise = test.patch({
                    diff: diff,
                    __userId: client.userId()
                });

                promises[i] = promise;
            }

            return isinr.allPromises(promises)
                .then(function(results) {

                    for (var i = 0; i < results.length; i++) {
                        var data = results[i];
                        test.expect(data).checkResponse('Unable to update the testuser-' + i.toString());
                        test.client(i).updateUser(data.body);
                    }
                });
        }
    },

    'Test:Profile Image': {
        priority: isinr.TestPriority.max - 1, // max is the highest priority
        after: "User Update",

        'Set user profile image': function(test) {
            test.log('Uploading profile images ...');

            var promises = new Array(test.numClients());

            for (var i = 0; i < test.numClients(); i++) {
                promises[i] = test.client(i).setProfileImage();
            }

            return isinr.allPromises(promises)
                .then(function(results) {
                    for (var i = 0; i < results.length; i++) {
                        test.expect(results[i]).isNotNull();
                    }
                });
        },
    }
});
