var updateUserImage = isinr.RestClass({
    path: '[apiRoot][apiVersion]/user/update',
    spec: {
        description: "Updates the permissions of a particular user. Request must be done by a super user.",
        // type: 'User',
        parameters: [
            isinr.Params.query("imageId", "The ID of the image which will be set as the profile image.", "string", true)
        ]
    },

    permissions: isinr.ApiPermissions.login, // User needs to be logged in for this

    action: function(args) {
        return isinr.Image.loadFromId(args.imageId)
            .then(function(image) {
                args._error.assert(image, 404, 'Image not found: ' + args.imageId);
                
                args._cached.image = image;
                return args._cached.user.setProfileImage(args._cached.image);
            })
            .then(function() {
                // send an empty success status ...
                args._res.status(200).send({});
            });
    },

    // ****************************************************************************************************************
    // ----------------------------------------------------------------------------------------------------------------
    //                                                     TESTS
    // ----------------------------------------------------------------------------------------------------------------
    // ****************************************************************************************************************
});

module.exports = function(app) {
    app.post(updateUserImage);
};
