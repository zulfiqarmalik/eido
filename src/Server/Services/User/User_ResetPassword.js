var resetPassword = isinr.RestClass({
    path: '[apiRoot][apiVersion]/user/resetpassword',
    spec: {
        description: "Reset the user password.",
        parameters: [
            isinr.Params.query("email", "The email of the user for whom the password is being reset.", "string", true),
        ]
    },

    validate: function(args) {
        return isinr.User.loadFromEmail(args.email)
            .then(function(user) {
                args._error.assert(user, 404, "Unable to load user:", args.email);
                args._cached.user = user;
                return user;
            });
    },

    action: function(args) {
        return args._cached.user.resetPassword()
            .then(function() {
                this._res.status(200).send({
                    passwordResetCode: this._cached.user.passwordResetCode
                });
            }.bind(args));
    },

    // ****************************************************************************************************************
    // ----------------------------------------------------------------------------------------------------------------
    //                                                     TESTS
    // ----------------------------------------------------------------------------------------------------------------
    // ****************************************************************************************************************
    'Test:Reset Password': {
        priority: isinr.TestPriority.normal, // normal test priority
        after: "Notifications",
        
        'Reset password': function(test) {
            return test.put({
                email: test.client(12).email()
            }).then(function(data) {
                test.expect(data).checkResponse('Unable to reset password!');
                test.info("Password reset code:", data.body.passwordResetCode);
                test.expect(data.body.passwordResetCode).isNotEmpty();
            });
        }
    }
});

module.exports = function(app) {
    app.put(resetPassword);
};
