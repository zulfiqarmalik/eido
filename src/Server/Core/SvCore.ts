import { eido, IError, CoreUtil as ShCoreUtil, IUser } from 'Shared/Core/ShCore';
import { Error as ServerError } from './Error';
import { ObjectID } from "Shared/Core/ObjectID";
import './SvObjectID';
import './SvObject';

export let DOMParser = require('xmldom').DOMParser;

export class CoreUtil implements ShCoreUtil {
    apiRoot(): string {
        return eido.appServer.apiRoot();
    }

    apiVersion(): string {
        return eido.appServer.apiVersion();
    }

    createError(code: number, msg: string): IError {
        return new ServerError(code, msg);
    }

    user(): IUser {
        throw new Error("Cannot get user from server side.");
    }
}

eido.coreUtil = new CoreUtil();

export class Routes {
    static routes       : any[] = []
    static add(route: any) {
        Routes.routes.push(route);
    }
}
