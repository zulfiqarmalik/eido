import { Debug, PromiseUtil } from 'Shared/Core/ShUtil';
import * as mongo from 'mongodb';

var gConnUrl;

export namespace db {
    export class ConnArgs {
        dbName: string;
        cls: any = null;

        constructor(cls: any = null, dbName: string = "") {
            this.dbName = dbName;
            this.cls = cls;
        }
    }

    export class Manager {
        static s_instance = new Manager();

        objectDBUrl     : string = "";
        blobDBUrl       : string = "";
        blobConn        : mongo.Db = null;
        objectConn      : mongo.Db = null;
        metricsConn     : mongo.Db = null;
        dbConns         : object = {};
        blobDBConns     : object = {};
        connections     : object = {};
    
        private constructor() {
        }
        
        getBlobConn(args: ConnArgs): mongo.Db {
            let dbName = "";
    
            if (args && args.dbName)
                dbName = args.dbName;
            else if (args && args.cls && args.cls.dbName)
                dbName = args.cls.dbName;
    
            if (!dbName)
                return this.objectConn;
    
            /// If we have loaded the DB
            Debug.assert(this.blobDBConns[dbName], `Blob DB not loaded: ${dbName}`);
            return this.blobDBConns[dbName];
        }
    
        getObjectConn(args: ConnArgs = null): mongo.Db {
            let dbName = "";
    
            if (args && args.dbName)
                dbName = args.dbName;
            else if (args && args.cls && args.cls.dbName)
                dbName = args.cls.dbName;
    
            if (!dbName)
                return this.objectConn;
    
            /// If we have loaded the DB
            Debug.assert(this.dbConns[dbName], `DB not loaded: ${dbName}`);
            return this.dbConns[dbName];
        }
    
        getDBConns(): object {
            return this.dbConns;
        }

        public static get instance() { return Manager.s_instance; }

        async init(dbPath: string, blobDBPath: string, dbNames: string[], isTest: boolean) {
            console.log("[DB] Init DBs: [%s]", dbNames.toString());
    
            return new Promise(async (resolve, reject) => {
                let promises = [];
                dbNames.map((dbName) => promises.push(this.initMongoObjectDB(dbPath, dbName, isTest)));

                if (blobDBPath) {
                    dbNames.map((dbName) => promises.push(this.initMongoBlobConn(blobDBPath, dbName, isTest)));
                }
                
                Promise.all(promises).then(resolve).catch(reject);
            });
        }
    
        initMongoDB(connUrl: string, dbName: string, isTest: boolean) {
            gConnUrl = connUrl;
            // let MongoClient = mongodb.MongoClient;
            // Promise.promisifyAll(mongodb.MongoClient);
            // Promise.promisifyAll(mongodb.Collection);
            
            // console.trace(`[DB] Connecting to MongoDB database: ${connUrl}`);
            // return mongodb.MongoClient.connect(connUrl);
            
            return new Promise((resolve, reject) => {
                Debug.assert(connUrl.indexOf("localhost") < 0, "Hey!!!");
                mongo.MongoClient.connect(connUrl, { useNewUrlParser: true } as mongo.MongoClientOptions, (err, client) => {
                    if (err)
                        reject(err);
                    else {
                        console.log(`[DB] Connected: ${connUrl}`);
                        resolve({
                            connUrl: connUrl,
                            client: client,
                            dbName: dbName,
                            conn: client.db(dbName)
                        });
                    }
                });
            });
        }
    
        initDB(dbPath: string, dbName: string, isTest: boolean = false, dbParams: string = "") {
            if (!dbPath)
                dbPath = process.env['MONGO_URL'] ? process.env['MONGO_URL'] : 'mongodb://localhost:27017/'; 
    
            let connUrl = dbPath;
    
            console.log(`[DB] Mongo connection URL: ${connUrl}`);
    
            if (dbParams)
                connUrl += dbParams;
    
            if (isTest === true) {
                connUrl += '_test';
                console.info('[DB] Using test database');
            }
    
            return this.initMongoDB(connUrl, dbName, isTest);
                // .then((conn) => {
                //     console.trace(`[DB] Connected initDB: ${connUrl}`);
                //     return {
                //         connUrl: connUrl,
                //         conn: conn
                //     };
                // });
        }
    
        initMongoObjectDB(dbPath: string, dbName: string, isTest: boolean) {
            if (this.objectConn)
                return;
    
            return this.initDB(dbPath, dbName || "pesa", isTest)
                .then((dbInfo: any) => {
                    console.trace(`[DB] Connected initMongoObjectDB: ${dbPath}${dbName}`);
                    this.objectDBUrl = dbInfo.connUrl;
                    this.objectConn = dbInfo.conn;
                    this.dbConns[dbPath] = this.objectConn;
                    this.dbConns[dbName] = this.objectConn;
                    return this.objectConn;
                });
        }
    
        initMongoBlobConn(dbPath: string, dbName: string, isTest: boolean) {
            if (this.blobConn)
                return;
    
            return this.initDB(dbPath, dbName, isTest)
                .then((dbInfo: any) => {
                    console.trace(`[DB] Connected initMongoObjectDB: ${dbPath}${dbName}`);
                    this.blobDBUrl = dbInfo.connUrl;
                    this.blobConn = dbInfo.conn;
                    this.blobDBConns[dbPath] = this.blobConn;
                    this.blobDBConns[dbName] = this.blobConn;
                    return this.blobConn;
                });
        }
    }
}
