import { Debug } from "Shared/Core/ShUtil";
import { Env } from "Shared/Core/ShCore";

var util        = require('util');
var mkdirp      = require('mkdirp');
var path        = require('path');
var moment      = require('moment');
var fs          = require('fs');
var stack       = require('callsite');
var colors      = require('colors');

var self: any   = {};
var empty       = function() {};
var sysLogger   = null;
var dualLog     = true;
var logLevel    = 'info';

var logLevels   = { // we do not list warn and error here because they are always logged no matter what!
    trace: 4,
    debug: 3,
    log: 3, // log is the same as debug
    info: 2
};

function shouldLog(level) {
    if (!logLevels[level])
        return true;

    if (logLevels[level] <= logLevels[logLevel])
        return true;

    return false;
}

self.express = {};

// Configuration
// ---------------------------------

var $ = {
    app: "eido server",
    logPath: "./logs",
    mainUser: "",
    maxTagLength: 50,
    indentation: 2,
    divider: ':::',
    defaultUserDir: '_user',
    testOutput: function(result, pipes, actual, expected, opts) {

        var pipe = result ? 'log' : 'error';
        var ne = expected.indexOf("\n") > -1;
        var na = actual.indexOf("\n") > -1;

        pipes[pipe]((" " + opts + " ").bold["inverse"] + (result ? " PASSED "["green"] : " FAILED "["red"]).bold.inverse);
        pipes[pipe](" EXPECTED " [result ? "green" : "red"].bold.inverse + " " + (!ne ? expected : ""));
        if (ne) {
            pipes[pipe](expected);
        }
        pipes[pipe](" ACTUAL " [result ? "green" : "red"].bold.inverse + " " + (!na ? actual : ""));
        if (na) {
            pipes[pipe](actual);
        }
    }
};


// Active settings
// ---------------------------------
var undefined;

var active = {
    defaultTag: undefined,
    userDir: undefined,
    dateDir: undefined,
    dayIso: undefined,
    dir: undefined
}

var loggers = {};
var theme = {};
var fsOptions = {
    encoding: 'utf8'
};
var __reserved = ["f", "t", "tag", "file", "should", "be", "test", "assert"];

// Cache pipe out
// ---------------------------------

var __stdpipe = (function() {
    return console.log;
})();

// Utility functions
// ---------------------------------

var pretty = function(a) {
    if (!a) {
        return a + "";
    }
    if (typeof a === 'object') {
        return JSON.stringify(a, null, 4);
    }
    return a.toString();
};

var compress = function(a) {
    if (!a) {
        return a + "";
    }
    if (typeof a === 'object') {
        return JSON.stringify(a);
    }
    return a.toString();
};

var tag = function(a) {
    return "[" + a + "]";
};

var createDir = function() {
    mkdirp.sync(path.normalize($.logPath), '0777');
    mkdirp.sync(path.normalize(path.join($.logPath, active.dateDir)), '0777');
    mkdirp.sync(active.dir = path.normalize(path.join($.logPath, active.dateDir, active.userDir)), '0777');
};

var getUser = function() {
    try {
        active.userDir = process.env[(process.platform === 'win32') ? 'USERPROFILE' : 'HOME'].toLowerCase();
        active.userDir = active.userDir.slice(active.userDir.lastIndexOf((process.platform === 'win32') ? '\\' : '/') + 1);
    } catch (e) {
        active.userDir = $.defaultUserDir;
    } finally {
        return active.userDir;
    }
};

var validate = function() {
    var dayIso = moment().day();
    if (active.dayIso !== dayIso) {
        active.dayIso = dayIso;
        active.dateDir = moment().format('MMM_D_YY').toLowerCase();
        return true;
    }

    return false;
};

function Extender(tag: string, opts: any = null, mode: any = null) {

    var self = this;

    self.tag = tag;
    self.opts = opts;
    self.mode = mode;

    self.do = self.invoke = self.should = function(actual) {

        var _actual = compress(actual);

        self.expect = self.be = function(expected) {

            var _expected = compress(expected);

            $.testOutput(_actual === _expected, self, pretty(actual), pretty(expected), self.opts);

            return self;
        };

        return self;
    };
}

var spaces = function(sp) {
    var s = '';
    for (var z = 0; z < sp; z++) {
        s += ' ';
    }
    return s;
};

var stackTag = function(_stack) {
    try {
        return tag(path.basename(_stack.getFileName()) + ":" + _stack.getLineNumber());
    } catch (e) {
        return tag(active.defaultTag);
    }
};

// Exports
// ---------------------------------

self.addLogger = function(name, file, console, color) {
    if (arguments.length < 4) {
        return false;
    }

    if (__reserved.indexOf(name) > -1) {
        throw Error("Reserved pipe - " + name);
    }

    theme[name] = color;
    loggers[name] = {
        "file": file,
        "console": console
    };

    colors.setTheme(theme);

    addPipe(name);

    return true;
};

exports.removeLogger = function(name) {
    if (loggers[name]) {
        delete loggers[name];
        delete console[name];
        return true;
    }

    return false;
};

self.set = function(a, b) {
    $[a] = b;
};

self.configure = function(callback) {

    var _logPath = $.logPath;
    var userDir = active.userDir;

    if (callback) {
        callback();
    }

    active.defaultTag = tag($.app.toLowerCase());
    active.userDir = getUser();

    if ($.logPath !== _logPath) {
        if (_logPath) {
            fs.unlink(_logPath, empty);
        }
    } else if (userDir !== active.userDir) {
        if (userDir) {
            fs.unlink(userDir, empty);
        }
    }

    validate();
    createDir();
};

self.express.logger = function(validate) {

    return function(req, res, next) {

        if (!validate || validate(req, res)) {
            console.info('[%s]%s %s %s %s', "express.js", req.ip.red, req.method.green, req.url.grey, (/mobile/i.test(req.headers['user-agent']) ? 'MOBILE' : 'DESKTOP')["yellow"]);
        }

        next();
    };
};

self.express.webpipe = function() {

    self.addLogger('user', true, true, 'magenta');

    return function(req, res) {
        console["t"]('scribe.js').user(req.ip.red + " " + req.body.data);
        res.send('received');
    };
};

// Web Control Panel
// ---------------------------------

var datetemplate = fs.readFileSync(path.join(process.cwd(), "logs", "template.html"), {
    encoding: "utf8"
});
var flatColors = ["#16a085", "#27ae60", "#2980b9", "#8e44ad", "#f39c12", "#d35400", "#c0392b", "#7f8c8d"];

self.express.controlPanel = function() {

    return function(req, res) {
        var date = req.param('date');

        if (!date) {

            var datePath = path.normalize(path.join($.logPath));

            fs.readdir(datePath, function(err, files) {

                if (err) {
                    console.error(err);
                    return res.send(err);
                }

                var loggerDates = "";

                for (var i = 0; i < files.length; i++) {
                    try {
                        var file = files[i];
                        var fileSplit = file.split("_");
                        loggerDates += '<div style="background:' +
                            flatColors[Math.floor(Math.random() * flatColors.length)] + '"data-types="' +
                            fs.readdirSync(path.join(datePath, file, $.mainUser)).join(',').replace(/app./g, '') + '", data-raw="' +
                            file + '" class="date"><div class="date-month">' + fileSplit[0] + '</div><div class="date-day">' +
                            fileSplit[1] + '</div><div class="date-year">' + ("20" + fileSplit[2]) + '</div></div>';
                    } catch (e) {
                        continue;
                    }
                }

                return res.send(datetemplate.replace("__title", $.app + " - Scribe.js Control Panel")
                    .replace('__content', files.join(","))
                    .replace('__logdates', loggerDates)
                    .replace('__logpath', active.dir)
                    .replace('__username', $.mainUser)
                    .replace('__divider', $.divider));
            });

            return;
        }

        var type = req.param('type');
        type = type ? type : "log";

        var filepath = path.join(active.dir, "app." + type);

        if (fs.existsSync(filepath)) {
            var stream = fs.createReadStream(filepath);

            res.writeHead(200, {
                'Content-Length': fs.statSync(filepath).size,
                'Content-Type': 'text/plain',
            });

            stream.pipe(res);
        } else {
            res.statusCode = 404;
            res.send();
        }
    };
};

// Additional Features
// ---------------------------------

console["t"] = function(n, _) {
    n = _ ? _ + ":" + n : n;
    var _tag = n;
    var ext = new Extender(n ? tag(n) : active.defaultTag);
    ext.t = function() {
        return console["t"].call(console, arguments[0], _tag);
    };

    ext.t = function() {
        return console["t"].call(console, arguments[0], _tag);
    };

    return ext;
};

console["f"] = function(n, _) {

    if (!n) {
        var st = stack()[1];
        n = path.basename(st.getFileName());
    } else {
        n = path.basename(n);
    }

    (
        n = _ ? _ + ":" + n : n);

    var _tag = n;
    var ext = new Extender(n ? n : active.defaultTag, undefined, "f");

    ext.f = function() {
        return console["f"].call(console, arguments[0], _tag);
    };

    ext.t = function() {
        return console["t"].call(console, arguments[0], _tag);
    };

    return ext;
};

Extender.prototype.test = Extender.prototype.asset = console.assert = console["test"] = function(name, tag) {
    tag = tag ? tag : stackTag(stack()[1]);
    return new Extender(tag, name, stack()[1]);
};

function addPipe(n) {

    Extender.prototype[n] = function() {
        var args = Array.prototype.splice.call(arguments, 0);
        if (this.mode === "f") {
            var st = stack()[1];
            args.unshift(tag(this.tag + ":" + st.getLineNumber()) + args.shift());
        } else {
            args.unshift(this.tag + args.shift());
        }
        console[n].apply(this, args);
    };

    console[n] = (function(i) {

        if (validate()) {
            createDir();
        }

        return function() {

            if (!shouldLog(i))
                return;


            var utfs = (arguments.length === 1 ? pretty(arguments[0]) : util.format.apply(util, arguments)).trim();
            var time = moment().format('h:mm:ss A');
            var file = path.join(active.dir, 'app.' + i);
            var indent = spaces($.indentation);
            var tag = utfs.match(/^\[(.*?)\]\s{0,}/m);
            var cleanTag;
            var decorateTags = true;

            if (loggers[i].file && utfs) {

                var outfs = utfs.stripColors;
                cleanTag = "";

                if (tag) {
                    outfs = outfs.replace(tag[0], "");
                    cleanTag = tag[0].trim();
                }

                outfs = time + $.divider + cleanTag + outfs.replace(/\n/g, '\n' + time + $.divider + cleanTag) + '\n';

                fs.appendFileSync(file, outfs, fsOptions, empty);
            }

            if (sysLogger) {
                if (!tag) {
                    tag = [stackTag(stack()[1])];
                }

                cleanTag = tag[0].trim();
                utfs = utfs.replace(tag[0], "");

                if (sysLogger[i])
                    sysLogger[i](cleanTag + ' - ' + utfs);
                else
                    sysLogger.log(cleanTag + ' - ' + utfs);
            }

            if (loggers[i].console) {

                if (!tag) {
                    tag = [stackTag(stack()[1])];
                }

                var tabIn;
                cleanTag = tag[0].trim();

                if (cleanTag.length <= $.maxTagLength) {
                    utfs = utfs.replace(tag[0], "");
                    tabIn = spaces($.maxTagLength - cleanTag.length);
                    cleanTag = cleanTag[i].bold;
                    utfs = indent + cleanTag + tabIn + utfs.replace(/\n/g, '\n' + indent + cleanTag + tabIn);
                } else {
                    tabIn = indent + spaces($.maxTagLength);
                    utfs = tabIn + utfs.replace(/\n/g, '\n' + tabIn);
                }

                __stdpipe(utfs);
            }
        };

    })(n);

    // console.t()[n]('Created pipe console.%s', n.toUpperCase());
}

// Startup
$.mainUser = active.userDir = getUser();
self.configure();

// Express missing notification
try {
    var express = require("express");
    if (!express) {
        throw new Error();
    }
} catch (e) {
    console.warn("SCRIBE WARNING: Express not installed - visual web logger is disabled.");
}

export default class Log {
    static init(appServer, logServer, newLogLevel) {
        if (!newLogLevel || !logLevels[newLogLevel]) {
            console.log('[Log] Invalid log level specified: ' + logLevel + '. Defaulting to: ' + logLevel);
            newLogLevel = logLevel;
        } 
        else {
            console.log('[Log] Using log level: ' + newLogLevel);
            logLevel = newLogLevel;
        }

        this.initLocalLog(appServer, logServer);

        if ((Env.prod || Env.preProd) && logServer) {
            Debug.assert(logServer, `Invalid log server for environment: ${Env.type}`);
//            this.initGraylog(logServer);
            return;
        }
    }

    static setLogLevel(newLogLevel) {
        console.info('[Log] Changing log level:', newLogLevel);
        logLevel = newLogLevel;
    }

    static getLogLevel() {
        return logLevel;
    }

    static initGraylog(logServer, facility) {
        // var graylog2 = require("graylog2");
        // var urlPrefix = "graylog://";

        // if (logServer.indexOf(urlPrefix) == 0)
        //     logServer = logServer.slice(urlPrefix.length);

        // var parts = logServer.split(':');
        // Debug.assert(parts.length == 2, `Invalid log server URL: ${logServer}`);

        // var host = parts[0];
        // var port = parts[1];
        // if (port.indexOf('/') > 0)
        //     port = port.substr(0, port.indexOf('/'));

        // port = parseInt(port);

        // if (!facility) {
        //     facility = process.env.LOG_TAG_PLATFORM ? process.env.LOG_TAG_PLATFORM : 'eido dev';
        // }

        // console.info('Using graylog facility: ' + facility + ' [LOG_TAG_PLATFORM=' + process.env.LOG_TAG_PLATFORM + ']');

        // var logger = new graylog2.graylog({
        //     servers: [{
        //         host: host,
        //         port: port
        //     }],
        //     facility: facility
        // });

        // logger.on('error', function(error) {
        //     console.error('Error while trying to write to graylog2:', error);
        // });

        // sysLogger = logger;

        // console.log('[Log] Setup graylog with url: ' + logServer);

        // return sysLogger;
    }

    static initLocalLog(appServer, logServer) {
        var scribe = self;
        var shouldLog = (!Env.preProd && !Env.prod) || !logServer;

        if (appServer && appServer.cmdArgs && appServer.cmdArgs.consolelog)
            shouldLog = true;

        // Configuration
        // --------------
        self.configure(function() {
            self.set('app', 'eido');
            // self.set('logPath', './.logs');
            self.set('defaultTag', 'PH');
            self.set('divider', ':::');
            self.set('indentation', 2);

            self.set('maxTagLength', 30);

            //self.set('mainUser', 'root'); // Username of the account which is running
            // the NodeJS server
        });

        // Create Loggers
        // --------------
        self.addLogger('trace', shouldLog, shouldLog, 'grey');
        self.addLogger("log", shouldLog, shouldLog, 'green'); // (name, save to file, print to console,
        self.addLogger('info', shouldLog, shouldLog, 'yellow');
        self.addLogger('warn', shouldLog, shouldLog, 'magenta');
        self.addLogger('error', shouldLog, shouldLog, 'red');

        // Filter out any Express messages
        if (appServer && appServer.expressApp) {
            appServer.expressApp.use(self.express.logger(function(req, res) {
                return false;
            }));

            // Control Panel (use the default onp)
            // --------------
            appServer.expressApp.get('/log', self.express.controlPanel());
        }
    }
};
