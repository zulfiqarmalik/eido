// ALL of these work this way:
// var a = new ConnArray();
// a[0] = 412;
// a.push(22);
// console.log(a); // prints [ 412, 22 ]
// console.log(Array.isArray(a)) // prints true
// console.log(require('util').isArray(a)); // prints true
// a['stuff'] = 8;
// console.log(a); // prints [ 412, 22, stuff: 8 ]
// a.push('anotherStuff'); // prints [ 412, 22, 'anotherStuff', stuff: 8 ]
// console.log(a.length); // prints 3 exactly as you'd expect (stuff is a pushed property hence is not counted)
// a[14] = 1;
// console.log(a.length); // prints 15 (because it considers you pushed undefined objects up to the 15th position exactly like Array does)
// console.log(a); // prints [ 412, 22, 'anotherStuff', , , , , , , , , , , , 1, stuff: 8 ]

// The Array drops any values that aren't ws incoming connections.
/**
 * Incoming Connection Array Class.
 * THE most twisted way of extending something. Have to do this otherwise things like Array.isArray or util.isArray from node won't work
 */
var WS = require('ws');
var ConnArray = function() {
    var returnedArray = Object.create(Array.prototype);
    var name = "";
    var args = Array.prototype.slice.apply(arguments);
    if (arguments.length) {
        name = Array.prototype.shift.apply(args); // I HATE when this ridiculous stuff needs to be written
    }

    returnedArray = Array.apply(returnedArray, args); // respects the constructor on creation.

    for (var fn in ConnArray.prototype) {
        Object.defineProperty(returnedArray, fn, {
            value: ConnArray.prototype[fn]
        });
    }
    Object.defineProperty(returnedArray, "name", {
        get: function() {
            return name;
        },
        set: function() {
            return false;
        }
    });
    if (typeof Object.observe == 'function') {
        Object.observe(returnedArray, function() {
            for (var vCnt = 0; vCnt < this.length; vCnt++) {
                if (!(this[vCnt] instanceof WS) || this[vCnt].readyState >= 2) { // removing dead / closed connections. these things are causing problems!!
                    // console.log("[CA] Removing in object observe of ", this.name, " item #", vCnt);
                    this.splice(vCnt, 1);
                }
            }
            if (this.length == 0 && typeof this.onEmpty == "function") {
                // console.log('[CA] Calling onEmpty on ', this.name);
                this.onEmpty();
            }
        }.bind(returnedArray));
    }
    return returnedArray;
};

ConnArray.prototype.push = function(wsIncomingConnection) {
    if (wsIncomingConnection instanceof WS) {
        wsIncomingConnection.on('close', function() {
            console.log('[CA] connClose in push');
            for (var i = 0; i < this.length; i++) {
                if (this[i] == wsIncomingConnection) {
                    //console.log('[CA] Removing #', i, " from ", this.name);
                    this.splice(i, 1);
                    //console.log("[CA] Remaining ", this.length, " items in ", this.name );
                    return;
                }
            }
        }.bind(this));
        Array.prototype.push.apply(this, [wsIncomingConnection]);
        console.log("[CA] Pushed into: ", this.name, ". Length now: ", this.length);
    }
    else {
        // just don't do anything ? or throw some stuff
    }
};

ConnArray.prototype.send = function(message, flags) {
    switch (typeof message) {
        case "undefined":
            return;
        case "string":
            // don't do anything just send it as is
            break;
        default:
            if (flags && !flags.binary) {
                if (message == null) return;
                message = JSON.stringify(message);
            } // no break! (it's the last option)
    }
    for (var i = 0; i < this.length; i++) {
        this[i].send(message, flags);
    }
};
ConnArray.prototype.close = function() {
    for (var i = 0; i < this.length; i++) {
        this[i].close();
    }
};
ConnArray.prototype.sendJSON = function(messageObject) {
    this.send(JSON.stringify(messageObject));
};

ConnArray.prototype.on = function(event, cb) {
    switch (event) {
        case "empty":
            this.onEmpty = cb;
            if (this.length == 0 && typeof this.onEmpty == "function") {
                // console.log('[CA] Calling onEmpty on ', this.name);
                this.onEmpty();
            }
            break;
    }

};

module.exports = ConnArray;
