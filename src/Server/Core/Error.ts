import { IError, Env, eido } from 'Shared/Core/ShCore';
import { Util, WebResponse } from "./SvUtil";
import { ShUtil, Debug } from 'Shared/Core/ShUtil';
import * as express from 'express';
import { ServerArgs } from './ServerArgs';
import WSResponse from 'Server/Core/WS/WSResponse';

type Response = express.Response | WSResponse;

export class Error implements IError {
    code            : number = 500;
    message         : string = "";
    stack           : object = {};
    res             : Response = null;

    constructor(code: number, message: string, res: Response = null) {
        this.code = code;
        this.message = message;
        // this.stack = (new Error()).stack;
        this.res = res;
    }

    toString() {
        let msg = this.message;
        if (typeof msg != 'string' && typeof msg == 'object')
            msg = JSON.stringify(msg);
        return `${this.code.toString()} : ${msg}`;
    }

    send(res: Response) {
        Debug.assert(res, 'Cannot send an error without a valid response object!');
        console.error('[Error] Sending error to client ...');

        // let toSend = {
        //     code: this.code,
        //     message: this.message, 
        //     stack: {}
        // };

        // if (eido.appServer.isTestEnvironment() || !Env.release)
        //     toSend.stack = this.stack;

        if (eido.appServer.newrelic)
            eido.appServer.newrelic.noticeError(this.message);

        res.status(this.code).send(this.message);
    }

    log(prefix: string = ""): Error {
        // console.error(`[Error] ${prefix} ${this.toString()}`);

        // if (this.stack)
        //     console.error(`[Error] Stack: ${this.stack}`);

        return this;
    }

    logAndSend(res: Response) {
        this.log();
        this.send(res ? res : this.res);
    }

    static unhandledException(e, res: Response) {
        //let msg = 'Unhandled exception: ' + e.toString();
        console.exception(e);
        return (new Error(500, e.message)).logAndSend(res);
    }
}

export class ErrorGen {
    res         : Response;

    constructor(res: Response) {
        this.res = res;
    }

    assert(exp, code: number = 500, message: string) {
        if (!exp) {
            debugger;
            let error = new Error(code, message);
            console.error(`Runtime Error :  ${message}`);
            console.error(`Call stack : ${Debug.getCallStack()}`);
            throw error;
        }
    }

    throwIf(exp, code, message: string) {
        if (exp) {
            throw (new Error(code, message));
        }
    }

    internalServerError(err) {
        //TODO: send email when this happens
        console.log(err);
        this.res.status(500).send({
            code: 500,
            message: 'Internal Server Error'
        });
    }

    log(err) {
        if (!Util.isType(err, Error)) {
            return new Error(err.code, err.message).log();
        }

        return err.log();
    }

    RESTError() {
        return function(err) {
            this.reportRESTError(err);
        }.bind(this);
    }

    reportRESTError(err) {
        try {
            if (!Util.isType(err, Error)) {
                return new Error(err.code || err.statusCode, err.message).logAndSend(this.res);
            }

            return err.logAndSend(this.res);
        } 
        catch (e) {
            console.error('Fatal error while trying to send error. Perhaps the error is not of type Error');
            console.error('Original error: ' + err.toString());

            if (err.stack)
                console.error('Original error stack: ' + err.stack);

            console.error('New exception generated');
            console.exception(e);

            console.error('Sending a generic error to the caller instead');
            //TODO: send email when this happens
            this.internalServerError(e);
        }
    }

    generic(code: number, msg: string, res: Response) {
        return new Error(code, msg, res);
    }

    notFound(field, res) {
        return this.generic(404, '\'' + field + '\'' + ' not found', res);
    }

    invalid(field, res) {
        return this.generic(400, 'Invalid \'' + field + '\'', res);
    }

    forbidden(msg, res) {
        return this.generic(403, msg ? msg : 'Forbidden!', res);
    }

    accessDenied(condition: any, msg: string, args: ServerArgs) {
        if (!condition) {
            console.error(msg);
            args.redirectLogin();
            throw (new Error(401, msg));
            // res.redirect(req.baseUrl + '/dk/login');
        }
    }

    unhandledException(e, res) {
        //let msg = 'Unhandled exception: ' + e.toString();
        console.exception(e);
        return (new Error(e.code, e.message, res)).logAndSend(res);
    }

    confict(msg, res) {
        return this.generic(409, msg, res);
    }
}
