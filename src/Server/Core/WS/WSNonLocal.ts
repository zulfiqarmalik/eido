import { eido } from 'Shared/Core/ShCore';

export default class WSNonLocal {
    id              : string = "";
    serverIP        : string = "";
    serverPort      : number = 0;
    user            : any;

    constructor(wsConnection) {
        this.id = wsConnection.id;
        this.serverIP = wsConnection.serverIP;
        this.serverPort = wsConnection.serverPort;
        this.user = {
            id: wsConnection.id
        };
    }

    send(data, flags) {
        // send via the connection manager here!
    }
}

