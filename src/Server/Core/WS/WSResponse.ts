import { eido } from 'Shared/Core/ShCore';
import { PushNotificationType } from 'Shared/Core/PushNotificationType';

export default class WSResponse {
    statusCode              : number = 200;
    data                    : any = {};
    ws                      : any = null;

    constructor(ws, data: any = null, message: string = null, req: any = null) {
        console.trace('[App] Setting up websocket response for path: ' + (data && data.path ? data.path : '<connect>'));

        this.statusCode = 200;
        this.data = data;
        this.ws = ws;
    }

    status(statusCode: number) {
        this.statusCode = statusCode;
        return this;
    }

    send(body) {

        try {
            let json = '';

            let data = {
                id: PushNotificationType.RESPONSE,
                path: this.data.path,
                statusCode: this.statusCode,
                __reqId: (this.data && this.data.__reqId ? this.data.__reqId : ""),
                body: body
            };

            json = JSON.stringify(data);

            this.ws.send(json);
        }
        catch (e) {
            console.error('JSON.stringify error: ' + e.toString());
            throw e;
        }

        return this;
    }

    header() {
        // no-op
    }
}

