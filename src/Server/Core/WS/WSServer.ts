import Signal from 'Shared/Core/Signals';
import { eido } from 'Shared/Core/ShCore';
import { ObjectID } from "Shared/Core/ObjectID";
import { ShUtil, Debug, PromiseUtil } from 'Shared/Core/ShUtil';
import { User } from 'Server/Model/User/SvUser';
import { Util } from 'Server/Core/SvUtil';
import { Error, ErrorGen } from 'Server/Core/Error';
import AppServer from 'Server/Core/BaseServer';
import { WSConnection } from 'Server/Model/WSConnection';
import { CObject } from 'Shared/Core/CObject';
import { PushNotificationType } from 'Shared/Core/PushNotificationType'
import { SecrecyLevel } from 'Shared/Model/SecrecyLevel';

import WSRequest from './WSRequest';
import WSResponse from './WSResponse';
import { db } from '../DB/DBManager';
import BaseServer from 'Server/Core/BaseServer';
import { ServerArgs } from '../ServerArgs';

let expressSession = require('express-session');

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// WebSocket server
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

export default class WSServer {
    static s_cmConnect;
    static s_cmDisconnect;
    static s_cmMessage;

    wsConnectionsLookup             : any = {};
    wsRouters                       : any = [];
    connMaster                      : any = null;
    serverId                        : string = "";
    wsServer                        : any;
    wsRouterTimer                   : any;

    constructor(httpServer: BaseServer) {
        this.initWebSocketServer(httpServer);
    }

    static get collectionName()     { return 'ws_routers'; }

    tryWSUserAuth(ws, args) {
        let deferred = PromiseUtil.defer();

        User.isUserLoggedIn(args)
            .then((user) => {
                deferred.resolve(user);
            })
            .catch((err) => {
                deferred.resolve(null);
            });

        return deferred.promise;
    }

    onWSConnect(ws, wsReq) {
        try {
            console.log(wsReq.url);
            ws.upgradeReq = wsReq;

            // first of all we need to verify that this user is logged in ...
            let req = new WSRequest(ws);
            let res = new WSResponse(ws);

//            if (req.headers)
//                console.trace('[App] WSS Headers: ', req.headers);

            console.trace('[App/WS] WSS Client connected: ' + Util.getRequestIPv4(req));

            let args = AppServer.initWSRequset(req, res, null, '/', 'WS', true);
            let userId = wsReq.session.passport.user;

            // first of all we try to see whether its a normal user sending the request ...

            User.loadFromId(ObjectID.create(userId.toString()))
                .then((user: User) => {
                    args._error.assert(user, 403, 'Unable to determine user!');
                    args._cached.user = user;
                    return this.acceptWSConnection(args, ws, user);
                })
                .catch((err) => {
                    console.error("[App/WS] WSConnect Error:", err.toString(), err.stack);
                    ws.terminate();
                });
        }
        catch (e) {
            console.error('[App] WSConnect Error:', e, e.stack);
            ws.terminate();
        }
    }

    handleWSRequest(ws, userId, data, message) {
        let req = new WSRequest(ws, data, message);
        let res = new WSResponse(ws, data, message);
        let path = data.path;

        Debug.assert(userId, 'Invalid user id associated with websocket!');

        if (!path)
            throw (new Error(404, 'Invalid path sent as part of the websocket request', res));

        let args = AppServer.initWSRequset(req, res, null, path, null, true); // let the method be chosen by the request

        // if (user)
        //     args._cached.user = user;

        try {
            // load the user first
//            User.loadFromId(userId)
//                .then(function(user) {
//                    Debug.assert(user, 'Invalid user. User Id:', userId);
//                    args._cached.user = user;

                    AppServer.handleWSRequest(req, res, null, args);
//                });
        }
        catch (e) {
            ws.send(JSON.stringify(e));
            //console.log("ERROR FFS", e, "args : ", args._method);
            return e;
        }
    }

    sendClientHandshake(ws) {
        try {
            console.trace(`[App] Sending initial client handshake: ${ws.id}`);
            let json = '';

            let data = {
                id: PushNotificationType.INIT_HANDSHAKE,
                statusCode: 200
            };

            ws.send(JSON.stringify(data));

            console.trace('[App] Client handshake sent!');
        }
        catch (e) {
            console.error('Error:', e);
            throw e;
        }
    }

    acceptWSConnection(args: ServerArgs, ws, user) {
        try {
            args.assert(user._id, 400, 'Invalid id for the user!');

            // cache the relevand data in the socket object ...
            ws.id = `${user._id}`;
            ws.server = this;
            ws.user = user;

            // Notify the wsRouter about the connection? Think this bit is not needed anymore.
            // first calculate the list of his online friends

            ws.on('message', function(message) {
                try {
                    let userId = this.id;

                    if (!userId) {
                        // user is not authorized!
                        console.error('[App] Un-authorized user trying to run a transaction!');
                        return;
                    }

                    WSConnection.s_wsMessage.dispatch({
                        ws: this,
                        message: message
                    });

                    console.trace('[App] Client Message: ' + message);
                    let data = null;

                    if (typeof message === 'object') {
                        // Treat as a BSON request ...
                        console.log('WSS BSON request ...');
                        let Client = require('mongodb');

                        try {
                            let binary = Client.Binary(message);
                            data = binary.toJSON();
                        }
                        catch (e) {
                            console.error('[App] Error parsing JSON: ' + e.toString());
                            data = null;
                        }
                    }
                    else {
                        // Treat as JSON request ...
                        console.log('[App] WSS JSON request ...');

                        try {
                            data = JSON.parse(message);
                        }
                        catch (e) {
                            console.error('[App] Error parsing JSON: ' + e.toString());
                            data = null;
                        }

                        if (!data || !data.path)
                            return;
                    }

                    // SIMPLE "whisper" functionality to test whether the blasted thing works.
                    this.server.handleWSRequest(this, userId, data, message);
                }
                catch (e) {
                    console.error('[App] Error on websocket message: ', e, '\nMessage: ', message);
                }

            }.bind(ws));

            ws.on('close', function() {
                this.server.disconnectClient(this);
            }.bind(ws));

            ws.on('error', function(err) {
                console.error('[App] Error on client websocket: ', err);
                this.server.disconnectClient(this);
            }.bind(ws));


            //if(this.connMaster){
            //    this.connMaster
            //}
            Util.getServerIPv4().then((ip) => {
                (new WSConnection()).create({
                        id: ws.id,
                        serverIP: ip,
                        serverPort: parseInt(eido.appServer.port)
                    }, ws)
                    .then((wsConn) => {
                        console.trace('[App] Created entry for the new connection in the database!');
                        this.wsConnectionsLookup[ws.id.toString()] = ws; // quicker lookup

                        // send the final handshake
                        this.sendClientHandshake(ws);
                    })
                    .catch((err) => {
                        args._error.log(err);
                        ws.terminate();
                    });
            });
            // first of all we make an entry for this in the collection
        }
        catch (e) {
            console.error('[App] Error on accepting client ws connection: ', e);
            this.disconnectClient(ws);
        }
    }

    disconnectClient(ws) {
        console.trace('[App] Client disconnected: ', ws.id.toString());

        // remove the WSConnection
        (new WSConnection()).delete(ws.id)
            .then((wsConn) => {
                delete this.wsConnectionsLookup[ws.id.toString()];
            })
            .catch((err) => {
                console.error('[App] Error while deleting WS connection: ', err);
                delete this.wsConnectionsLookup[ws.id.toString()];
            });
    }

    getWebSocket(id: ObjectID) {
        return this.wsConnectionsLookup[id.toString()];
    }

    sendRawNotification(ids, notificationId, notificationData, excludeIds) {
        try {
            Debug.assert(ids && require('util').isArray(ids), 'Invalid ids array passed!');
    
            // check whether the ids are actually ids or user objects
            // let searchIds = new Array(ids.length);
            // for (let i = 0; i < ids.length; i++) {
            //     if (typeof ids[0] === 'object' && typeof ids[0]._id !== 'undefined') 
            //         searchIds[i] = ids[i]._id;
            //     else 
            //         searchIds[i] = ids[i];
            // }
            
            return eido.appServer.wsServer.getWebSockets(ids, excludeIds)
                .then((socketSender) => {
                    socketSender({
                        id: notificationId,
                        data: notificationData
                    });

                    return socketSender;
                });
        }
        catch (e) {
            console.error('Error:', e);
            throw e;
        }

    }

    sendData(ids, data, excludeIds) {
        Debug.assert(ids && require('util').isArray(ids), 'Invalid ids array passed!');

        // check whether the ids are actually ids or user objects
        // let searchIds = new Array(ids.length);
        // for (let i = 0; i < ids.length; i++) {
        //     if (typeof ids[0] === 'object' && typeof ids[0]._id !== 'undefined') 
        //         searchIds[i] = ids[i]._id;
        //     else 
        //         searchIds[i] = ids[i];
        // }
        
        return eido.appServer.wsServer.getWebSockets(ids, excludeIds)
            .then((socketSender) => {
                let clone = data.cleanClone({ level: SecrecyLevel.public });
                clone.__classId = data.constructor && data.constructor.__classId ? data.constructor.__classId.toString() : null;

                socketSender(clone);
                return socketSender;
            });
    }

    broadcast(message) {
        console.info('[App/WSR] Broadcasting message:', message);
        // let wsRouter = this.getNextWSRouter();

        // wsRouter.send(JSON.stringify({
        //     messageType: "broadcast",
        //     message: message
        // }));
    }

    getWebSockets(ids: string[], excludeIds: any = {}) {
        if (!excludeIds)
            excludeIds = {};
        else if (typeof excludeIds === 'object') {
            let eid = excludeIds;
            excludeIds = {};
            excludeIds[eid.toString()] = true;
        }

        let localSockets = new Array();
        let nonlocalSockets = [];
        let alreadyLookedUp = {};

        for (let i = 0; i < ids.length; i++) {
            let id = ids[i];

            if (id) {
                id = id.toString();

                // we have already handled this socket!
                if (alreadyLookedUp[id] !== true && excludeIds[id] !== true) {
                    // set as looked up in this session
                    alreadyLookedUp[id] = true;

                    let ws = this.wsConnectionsLookup[id];
                    if (ws)
                        localSockets.push(ws);
                    else
                        nonlocalSockets.push(id);
                }
            }
        }

        return PromiseUtil.resolvedPromise((message) => {
            console.trace("[App] Num local ws connections:", localSockets.length);
            console.trace("[App] Num non-local ws connections:", nonlocalSockets.length);

            let messageJson = JSON.stringify(message);
            //localSockets.sendJSON(message);

            localSockets.forEach((socket) => {
                socket.send(messageJson);
            });

            if (nonlocalSockets.length) {
                // if (this.connMaster) {
                //     this.connMaster.send(JSON.stringify({
                //         messageType: "FWD",
                //         message: message,
                //         ids: nonlocalSockets
                //     }));
                // }
                // else {
                //     console.error('[App] WSRouter Error! Unable to send messages to SEM Ids: ', nonlocalSockets);
                // }
            }
        });
    }

    getWSRouters() {
        let defer = PromiseUtil.defer();

        let conn = db.Manager.instance.getObjectConn();
        let collection = conn.collection(WSServer.collectionName);

        CObject["getCollection"]().find({}, (err, docs) => {
            if (err) {
                defer.reject(err);
            }
            else {
                defer.resolve(docs);
            }
        });

        return defer.promise;
    }

    wsRouterPanic(wsUrl: string = "") {
        if (this.wsRouters.length == 0 || !this.connMaster) {
            console.error("[App/WSR] Error! Panic! No more connection managers! Trying ...");
            this.initWSRoutersConnections(); // goes to the database in case of panic attacks.
            // process.exit() // after x attempts eventually
        }
    }

    connectToServer(wsURL) {
        // each so many seconds check it's still connected to some conn Manager
        console.log("[App/WSR] Connecting to WSRouter:", wsURL);
        let wsConn = new(require('ws'))(wsURL);

        wsConn.on('open', () => {
            WSServer.s_cmConnect.dispatch(wsConn);

            let handshake = JSON.stringify({
                messageType: "handshake",
                serverType: "WSS",
                serverId: this.serverId
            });

            console.log("[App/WSR] Sending handshake: ", handshake);
            wsConn.send(handshake);
        });

        wsConn.on('message', (message, flags) => {

            WSServer.s_cmMessage.dispatch({
                ws: wsConn,
                message: message
            });

            if (flags && flags.binary) {
                // parse the binary message
            }
            else {
                try {
                    let jsnMsg = JSON.parse(message);

                    if (jsnMsg.messageType != "pong" && jsnMsg.messageType != "ping")
                        console.trace('[App/WSR] Received WSRouter message:', message);

                    if (jsnMsg) {
                        switch (jsnMsg.messageType) {
                            case "ping":
                                wsConn.send(JSON.stringify({
                                    messageType: "pong"
                                }));
                                break;

                            case "pong":
                                if (wsConn.pingTimestamp)
                                    delete wsConn.pingTimestamp;
                                break;

                            case "handshake":
                                if (jsnMsg.serverType == "wsRouter") {
                                    if (jsnMsg.isMaster) {
                                        console.log('[App/WSR] Promoting ', wsURL, " as WSRouter Master");
                                        this.connMaster = wsConn;
                                    }

                                    console.log("[App/WSR] Connected to WSRouter: ", wsURL);
                                    this.wsRouters.push(wsConn);

                                }
                                else {
                                    console.error("[App/WSR] Received unknown message from someone. Message payload: ", jsnMsg);
                                }
                                break;

                            case "connMgrPromote":
                                // this shouldn't be received unless it was already connected to it... ;)
                                if (jsnMsg.isMaster) {
                                    console.log('[App/WSR] Promoting ', wsURL, " as WSRouter Master");
                                    this.connMaster = wsConn;
                                }
                                break;

                            case "connMgrSpawn":
                                console.log("[App/WSR] New WSRouter spawn: ", jsnMsg.connMgrURL);

                                if (typeof jsnMsg.connMgrURL == "string")
                                    this.connectToServer(jsnMsg.connMgrURL);
                                else
                                    console.error("[App/WSR] WSRouter spawned but no url given: ", jsnMsg);
                                break;

                            case "broadcast":
                                console.trace('[App/WSR] Got broadcasted message:', message);
                                eido.appServer.handleBroadcast(jsnMsg.message);
                                break;

                            case "FWD":
                                if (typeof jsnMsg.ids == "object" && require('util').isArray(jsnMsg.ids)) {
                                    console.log('[App/WSR] Got forwarded message: ' + message);

                                    this.getWebSockets(jsnMsg.ids).then((sender) => {
                                        sender(jsnMsg.message, jsnMsg.message.from);
                                    });
                                }
                                else {
                                    console.warn('[App/WSR] Got a message for forwarding but nobody to forward to. Ignoring: ', message);
                                }
                                break;

                            default:
                                console.log("[App/WSR] Unknown message type received: ", jsnMsg.messageType, " The whole message: ", message);
                                break;
                        }
                    }
                }
                catch (e) {
                    console.error("[App/WSR] Error while parsing WSRouter message: ", e);
                }
            }
        });

        wsConn.on('close', () => {
            //this.serverDisconnected(wsConn);
        });
    }

    serverDisconnected(wsConn) {
        WSServer.s_cmDisconnect.dispatch(wsConn);

        // remove from the array
        for (let i = 0; i < this.wsRouters.length; i++) {
            if (this.wsRouters[i] == wsConn) {
                console.info('[App/WSR] Removing WSRouter: ', Util.extractWSInfo(wsConn));
                this.wsRouters.splice(i, 1);
                break;
            }
        }

        if (wsConn == this.connMaster) {
            console.warn('[App/WSR] ConnMaster just disconnected! Going into panic mode!');
            this.connMaster = null;
        }

        this.wsRouterPanic();
    }

    forceDisconnectWSRouter(wsConn) {
        let wsInfo = Util.extractWSInfo(wsConn);
        console.error('[App/WSR] Force disconnecting WSRouter: ', wsInfo.toString());
        wsConn.close();
        this.serverDisconnected(wsConn);
    }

    static pingConn(wsConn) {
        if (wsConn.pingTimestamp) {
            let wsInfo = Util.extractWSInfo(wsConn);
            let currTime = (new Date()).getTime();
            let elapsedTime = currTime - wsConn.pingTimestamp;

            if (elapsedTime > 6000) {
                console.trace('[App/WSR] Waited too long for the WSRouter to respond! Throwing!');
                throw "WSRouter took too long! Killing!";
            }
            else {
                console.trace('[App/WSR] Already pinged server: ', wsInfo.toString(), ' - ElapsedTime: ', elapsedTime, '. Waiting ...');
            }

            return;
        }

        wsConn.pingTimestamp = (new Date()).getTime();
        wsConn.ping();
    }

    initWSRoutersConnections() {
        if (eido.appServer.cmdArgs.noWSRouter) {
            //console.warn('[App] Conn managers disabled')
            return;
        }

        console.trace("[App/WSR] Initializing conn managers");

        if (!this.wsRouterTimer) {
            console.info('[App/WSR] Setting up timer to check the WSRouter health status');

            this.wsRouterTimer = setInterval(() => {
                // console.trace('[App/WSR] Checking status for connection managers. Count: ' + this.wsRouters.length);

                this.wsRouters.forEach((wsConn) => {
                    try {
                        WSServer.pingConn(wsConn);
                    }
                    catch (e) {
                        console.error('[App/WSR] Exception while trying to ping the WSRouter. Considering as disconnected!');
                        // an exception happened ... close this connection
                        this.forceDisconnectWSRouter(wsConn);
                    }
                });

                // check conn manager panic
                this.wsRouterPanic();
            }, 5000);
        }

        this.getWSRouters()
            .then((servers) => {
                console.log('[App/WSR] Got num conn managers: ' + servers.length);

                servers.forEach((server) => {
                    let timeSinceLastSeen = (new Date()).getTime() - server.lastSeen;

                    if (timeSinceLastSeen >= 6000) {
                        console.info('[App/WSR] Server has not been seen for more than: ', timeSinceLastSeen, 'ms. Ignoring for the time being: ', server.ip + ':' + server.port);
                        return;
                    }

                    let wsUrl = "ws" + (server.ssl ? "s" : "") + "://" + server.ip + ":" + server.port + "/";
                    this.connectToServer(wsUrl);
                });
            })
            .catch((err) => {
                console.error("[App/WSR] Error getting conn managers: " + err);
            });
    }

    initWebSocketServer(httpServer: BaseServer) {
        if (eido.appServer.cmdArgs.simple) {
            console.info('[App/WS] Simple server requested. Not creating websocket server');
            return;
        }

        console.info('[App/WS] Creating websocket server ...');

        let WSS = require('ws').Server;
        this.wsServer = new WSS({
            verifyClient: (info, done) => {
                console.log('[WS] Parsing session from request...');
                httpServer.sessionParser(info.req, {}, () => {
                    if (info && info.req && info.req.session && info.req.session.passport && info.req.session.passport.user) {
                        let userId = info.req.session.passport.user;
                        console.log(`[WS] User ID: ${userId}`);
                        done(userId, 200, {});
                    }
                });
              },
              server: httpServer.server
        });

        this.wsServer.on('connection', this.onWSConnect.bind(this));
    }

    tick() {
    }
}

WSServer.s_cmConnect    = new Signal();
WSServer.s_cmDisconnect = new Signal();
WSServer.s_cmMessage    = new Signal();
