import { eido } from 'Shared/Core/ShCore';
import { Debug } from 'Shared/Core/ShUtil';
import * as express from 'express';

export default class WSRequest {
    method              : string = "WS";
    data                : any = {};
    path                : string = "";
    url                 : string = "";
    query               : any = {};
    form                : any = {};
    body                : any = {};
    user                : any = null;

    constructor(ws, data: any = null, message: string = "") {
        console.trace('[App] Setting up websocket request for path: ' + (data && data.path ? data.path : '<connect>'));

        for (let prop in ws.upgradeReq) 
            this[prop] = ws.upgradeReq[prop];

        this.method = 'WS';

        if (data) {
            console.trace('[App] Setting up websocket request for path: ' + data.path);
            this.data = data;
            this.url = data.path;
            this.query = data.query || {};
            this.form = data.form || {};
            this.body = data.body || {};
            this.method = data.method || this.method;
        }

        this.query = this.query || {};

        Debug.assert(this.url, "Invalid source URL!");

        // now parse the url
        let url = this.url.replace("/?", "");
        let queryStrings = url.split("&");

        for (let i = 0; i < queryStrings.length; i++) {
            let queryString = queryStrings[i];
            let qsValues = queryString.split("=");

            if (qsValues.length >= 2) {
                let qsName = qsValues[0];
                let qsValue = qsValues[1];

                for (let j = 2; j < qsValues.length; j++)
                    qsValue += qsValues[j];

                this.query[qsName] = qsValue;
            }
        }

        this.user = null;
    }

    cookie() {
        throw "Cannot set cookie on a websocket request";
    }
}
