import { ObjectID } from 'Shared/Core/ObjectID';
import * as mongo from 'mongodb';

ObjectID.create = function(...args): ObjectID {
    return new mongo.ObjectID(...args) as any;
}

// ObjectID.prototype.constructor = function(...args) {
//     this._id = new mongo.ObjectID(...args);
// }

// ObjectID.prototype.copyFrom = function(id: any): any {
//     return new mongo.ObjectID(id);
// }

// ObjectID.prototype.equals = function(other: any): boolean {
//     if (other instanceof ObjectID)
//         return this._id.equals((other as ObjectID)._id);
//     else if (other instanceof mongo.ObjectID)
//         return this._id.equals(other);
//     return this.toString() === other.toString();
// }

// ObjectID.prototype.generate = function(time?: number): string {
//     return (new mongo.ObjectID()).generate(time);
// }

// ObjectID.prototype.getTimestamp = function(): Date {
//     return this._id.getTimestamp();
// }

// ObjectID.prototype.toString = function(): string {
//     return this._id.toString();
// }

// ObjectID.prototype.toHexString = function(): string {
//     return this._id.toHexString();
// }

