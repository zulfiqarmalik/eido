export class ParseObject { 
    constructor(name, options) {
        this.name = name;
        this.parser = null;
        this.mainObject = null;
        this.options = options;
    }

    isStruct() { return true; }
    size() { return this.options.size * (this.options.length || 1); }
    read(retObject, buffer, offset) {}
}

export class UInt8 extends ParseObject {
    constructor(name) { super(name, { size: 1 }); }
    read(retObject, buffer, offset) { retObject[this.name] = buffer.readUInt8(offset); return this.size(); }
}

export class UInt32 extends ParseObject {
    constructor(name) { super(name, { size: 4 }); }
    read(retObject, buffer, offset) { retObject[this.name] = buffer.readUInt32LE(offset); return this.size(); }
}

export class Int32 extends ParseObject {
    constructor(name) { super(name, { size: 4 }); }
    read(retObject, buffer, offset) { retObject[this.name] = buffer.readInt32LE(offset); return this.size(); }
}

export class LengthParseObject extends ParseObject {
    constructor(name, options) { super(name, options); }

    size() {
        if (typeof this.options.length === 'function')
            return 0;
        return this.options.length;
    }

    length() {
        if (typeof this.options.length === 'function')
            return this.options.length.bind(this.mainObject)();
        return this.options.length;
    }
}

export class StringObject extends LengthParseObject {
    constructor(name, options) { super(name, options); }

    read(retObject, buffer, offset) { 
        let length = this.length();
        retObject[this.name] = buffer.toString('ascii', offset, length); ///.replace(/\\x00+$/g, '');
        return length;
    }
}

export class ArrayObject extends LengthParseObject {
    constructor(name, options) { super(name, options); }

    size() {
        if (typeof this.options.length === 'function')
            return 0;
        return this.options.length * this.options.type.sizeOf();
    }

    read(retObject, buffer, offset) {
        let length = this.length();
        let orgOffset = offset;
        let Type = this.options.type;
        retObject[this.name] = new Array(length);

        for (let i = 0; i < length; i++) {
            Type.offset = offset;
            let childObject = Type.parse(buffer);
            retObject[this.name][i] = childObject;
            offset = Type.offset;
        }

        return (offset - orgOffset);
    }
}

export default class CustomBinaryParser {
    constructor(offset) {
        this.def = [];
        this.offset = 0;
    }

    endianess() { return this; }

    addClass(name, className) {
        this.def.push({ 
            name: name,
            type: new className(name) 
        });
        return this;
    }

    uint8(name)     { return this.addClass(name, UInt8)    }
    uint32(name)    { return this.addClass(name, UInt32)    }
    int32(name)     { return this.addClass(name, Int32)     }

    string(name, options) { 
        this.def.push({
            name: name,
            type: new StringObject(name, options)
        });
        return this;
    }

    array(name, options) {
        this.def.push({
            name: name,
            type: new ArrayObject(name, options)
        });
        return this;
    }

    nest(name, info) {
        this.def.push({
            name: name,
            type: info.type, 
            nested: true
        });
        return this;
    }

    sizeOf() {
        let size = 0;

        this.def.map((po, index) => {
            if (po.nested) 
                size += po.type.sizeOf();
            else 
                size += po.type.size();
        });
        
        return size;
    }

    parse(buffer) {
        let retObject = {};

        this.def.map((po, index) => {
            po.parser = this;
            po.mainObject = retObject;

            if (po.nested) {
                po.type.offset = this.offset;
                let childObject = po.type.parse(buffer);
                retObject[po.name] = childObject;
                this.offset = po.type.offset;
            }
            else {
                let numRead = po.type.read(retObject, buffer, this.offset);
                this.offset += numRead;
            }

        });

        return retObject;
    }
}


