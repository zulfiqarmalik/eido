import { Env, eido } from '../../Shared/Core/ShCore';
import { db } from './DB/DBManager';
import { Error, ErrorGen } from './Error';
import { ShUtil, Debug, PromiseUtil } from '../../Shared/Core/ShUtil';
import { Model } from '../../Shared/Model/ShModel';
import { SecrecyLevel } from '../../Shared/Model/SecrecyLevel';
import { Util } from './SvUtil';
import Log from './Log';
import Security from './Security';

import WSServer from '../../Server/Core/WS/WSServer';
import { WSConnection } from '../Model/WSConnection';

import { DB as DBList, Collections, getComponentDocs } from '../Model/SvModel';

import { PushNotificationType } from '../../Shared/Core/PushNotificationType';
import PushNotification from '../System/PushNotification';
import parseCmdArgs from '../Core/CmdArgs';

import * as express from 'express';
import * as bodyParser from 'body-parser';
import * as path from 'path';
import { ServerArgs } from './ServerArgs';
import { Server } from 'http';

import { ApiPermissionsCallbacks } from '../Services/Router';
import { Rest } from '../Services/RouterUtil';
import WSRequest from './WS/WSRequest';
import WSResponse from './WS/WSResponse';
import { Routes } from './SvCore';
import { Session } from '../Model/User/SvSession';
import { BaseObject } from 'Shared/Core/BaseObject';
import { CDN } from 'Server/System/CDN/CDN';

let expressSession = require('express-session');
let MongoDBStore = require('connect-mongodb-session')(expressSession);
let passport = require('passport');
let expressFunc = require('express');
let DB = db.Manager.instance;

export default abstract class BaseServer {
    cmdArgs             : any = null;
    args                : ServerArgs = null;
    rawConfig           : any = null;
    cdn                 : CDN = null;
    jwtSecret           : string = "";
    apiRoot             : string = "";
    apiVersion          : string = "";
    port                : number = 4000; 
    host                : string = "";
    protocol            : string = "http";
    expressApp          : express.Express = null;
    server              : Server;
    wsServer            : WSServer = null;
    distDir             : string = "";
    serverIP            : string = "";
    serverId            : string = "";
    sessionParser       : any;
    dbPath              : string = "";
    dbName              : string = "";

    handlers            : Rest.EndPoint[] = [];
    handlersLUT         : object = {};
    
    constructor(cmdArgs: any) {
        Debug.assert(cmdArgs.jwtSecret, "Invalid JWT Secret provided!");

        if (cmdArgs || cmdArgs.config)
            this.loadConfig(cmdArgs.config);

        this.cmdArgs    = cmdArgs;
        this.jwtSecret  = cmdArgs.jwtSecret;
        this.apiRoot    = this.rawConfig["apiRoot"] || cmdArgs.apiRoot || "";
        this.apiVersion = this.rawConfig["apiVersion"] || cmdArgs.apiVersion || "";

        // parse the arguments and read the environment vars for production use
        // this.parseArgs();
        this.readEnvironment();

        if (!this.cmdArgs.loglevel) {
            this.cmdArgs.loglevel = 'debug';
        }
    
        // initialize the logger
        this.initLogger();

        // 1st arg is path to node and second one is path to the top level script
        // this.initDatabase(args.dbName);
        
        // initialize the server and swagger
        if (this.cmdArgs.port)
            this.port = this.cmdArgs.port;
        else if (this.rawConfig.port)
            this.port = this.rawConfig.port;
    }

    loadConfig(configFilename: string) {
        if (!configFilename) {
            this.rawConfig = {};
            return;
        }

        console.log("[App] Loading config: %s", configFilename);
        let fs = require('fs');
        let data = fs.readFileSync(configFilename, "utf8");
        this.rawConfig = JSON.parse(data);
    }

    getLogLevel() {
        return Log.getLogLevel();
    }

    isTms() {
        return false;
    }

    setLogLevel(loglevel: string, suppressBroadcast: boolean = false) {
        Log.setLogLevel(loglevel);

        // now set it in others
        if (!suppressBroadcast && this.wsServer)
            this.wsServer.broadcast({
                type: 'loglevel',
                data: loglevel
            });
    }

    getUrl(): string {
        return `${this.protocol}://${this.host}`;
    }

    handleBroadcast(msg: any) {
        if (msg.type == 'loglevel')
            this.setLogLevel(msg.data, true);
    }

    setCDN(cdn: CDN) {
        Debug.assert(!this.cdn, "Cannot replace a set CDN!");
        this.cdn = cdn;
    }

    cleanup() {}

    getApiVersion(): string {
        return this.apiVersion;
    }

    getBucketName(): string {
        return this.cmdArgs.bucket ? this.cmdArgs.bucket : 'eido_data';
    }

    getCDNPath(): string {
        return this.cmdArgs.cdn ? this.cmdArgs.cdn : require('path').resolve(__dirname, '..', 'cdn');
    }
    
    getTestUuid(): string {
        return this.cmdArgs.uuid;
    }

    isTestServer() {
        if (Env.release)
            return false;

        return this.cmdArgs.testmode;
    }

    isTestEnvironment() {
        if (Env.release)
            return false;

        // !!!!!!!! Be careful with this! Users created in test environment are always super-users !!!!!!!!
        return (this.cmdArgs.test && this.cmdArgs.isLocalTests) ||
            (this.isTestServer());
    }

    cleanStateData() {
        return;
        // console.info('[App] Deleting state data (that might have gone stale) from the database ...');

        // // see if there is any existing data that we may need to delete
        // let conn = DB.getObjectConn();
        // let collection = conn.collection('wsCollection');

        // collection.remove({
        //     serverIP: Util.getServerIPv6().toString(),
        //     serverPort: this.port.toString()
        // });
    }

    needsCleanState() {
        return this.cmdArgs.test && this.cmdArgs.isLocalTests;
    }

    abstract initDatabase(dbName_: string);

    initServer(): express.Express {
        let expressApp = expressFunc();
        let distDir = path.join(process.cwd());

        // expressApp.use(express.static(publicPath));

        // let distDir = path.join(publicPath, "client", "platform", "browser");

        /// If we are running a production server
        // if (!isDevEnvironment()) {
        //     distDir = path.join(distDir, "dist");
        // }
        // else {
        //     // distDir = path.join(distDir, "src");
        //     this.initWebPack(expressApp, distDir);
        // }

        // distDir = "P:\\Work\\Eido\\public\\client\\platform\\browser\\public\\img"
        
        // expressApp.use(express.static(distDir));
        
        let publicDir = path.join(distDir, "dist", "Client");
        
        // if (Env.dev || Env.debug) 
        //     publicDir = path.join(distDir, "build", "Client");

        console.info(`[App] Setting public dir: ${publicDir}`);

        expressApp.use(express.static(publicDir));

        let staticDir = path.join(process.cwd(), "static");
        expressApp.use(express.static(staticDir));

        expressApp.use(bodyParser.urlencoded({
            extended: false
        }));
        
        console.log(`[BaseServer] Session store: ${this.dbPath}@${this.dbName}`);
        let sessionStore = new MongoDBStore({
            uri: this.dbPath,
            databaseName: this.dbName,
            collection: Collections.sessions
        });

        expressApp.use(bodyParser.json());

        this.sessionParser = expressSession({ 
            secret: this.jwtSecret,
            cookie: { maxAge : Session.maxAge },
            store: sessionStore,
            resave: true,
            saveUninitialized: true            
        });

        expressApp.use(this.sessionParser);

        /// Setup passportjs
        expressApp.use(passport.initialize());
        expressApp.use(passport.session());
        // allow cors
        // expressApp.use(require('cors')());
        // expressApp.set('jwtTokenSecret', "arsdh02yjg10qw9fjst09gq2owhdars0tj3210jd0qwdqy20h0wshtyarehddh02jtyorhdw");

        return expressApp;
    }

    readEnvironment() {
        if (!this.cmdArgs)
            this.cmdArgs = {};

        if (process.env.PORT) {
            console.info(`[App] Using environment port: ${process.env.PORT}`);
            this.cmdArgs.port = parseInt(process.env.PORT);
        }

        if (process.env.CDN_URL) {
            console.info(`[App] Using CDN path: ${process.env.CDN_URL}`);
            this.cmdArgs.cdn = process.env.CDN_URL;
        }

        if (process.env.eido_LOG_LEVEL) {
            console.info(`[App] Using log level: ${process.env.eido_LOG_LEVEL}`);
            this.cmdArgs.loglevel = process.env.eido_LOG_LEVEL;
        }
    }

    parseArgs() {
        console.log('[App] Parsing command line arguments');

        try {
            let argv = [
                process.argv[0],
                process.argv[1]
            ];
            
            for (let i = 2; i < process.argv.length; i++) {
                let arg = process.argv[i];
                if (arg.charAt(0) != '-') 
                    argv[argv.length - 1] = argv[argv.length - 1] + ' ' + arg;
                else
                    argv.push(arg);
            }
            
            process.argv = argv;
            
            this.cmdArgs = parseCmdArgs();

            this.cmdArgs.isLocalTests = false;
            if (this.cmdArgs.test && !this.cmdArgs.host)
                this.cmdArgs.isLocalTests = true;

            this.cmdArgs.appDir = require('path').resolve(__dirname, '..');
            require('path').resolve(__dirname);
        }
        catch (e) {
            console.exception(e);
        }
    }

    initLogger() {
        // tests to not need the logger
        if (this.cmdArgs.isLocalTests && !this.cmdArgs.verbose)
            return;

        Log.init(this, this.cmdArgs.logserver, this.cmdArgs.loglevel);
    }

    startTests() {
        return;
        // console.info('[App] Starting application in test mode!');

        // //Debug.assert(this.cmdArgs.host, 'Must specify target host for running tests!');
        // if (!this.cmdArgs.username || !this.cmdArgs.password) {
        //     console.warn('No username or password given. The tests might present unexpected problems. Running anyway ...');
        // }

        // // require('../test/test_framework.js');

        // this.testFramework = new eido.TestFramework(this.cmdArgs);
        // this.testFramework.runAll(this, this.handlers)
        //     .then(function() {
        //         // finish off the process ... this will also kill any locally running servers
        //         // process.exit(0);
        //     })
        //     .catch(function(err) {
        //         console.error('Error running test:', err);

        //         if (err.stack)
        //             console.error(err.stack);
        //         process.exit(1);
        //         // process.exit(1);
        //     });
    }

    start() {
        if (this.isTestServer())
            Env.test = true;

        if (!Env.release) { // this is only available for debug builds!
            if (this.cmdArgs.test) {
                if (!this.cmdArgs.isLocalTests) {
                    console.info('[App] Running the tests for host:', this.cmdArgs.host);
                    return this.startTests();
                }

                console.info('[App] Starting localhost server for running tests');
            }
            else if (this.cmdArgs.testmode)
                console.info('[App] RUNNING IN TEST MODE!');
        }

        if (!this.cmdArgs.secure || Env.release) {
            let http = require('http');

            this.server = this.expressApp.listen(this.port, function() {
                console.info('[App] Listening on port: %d', this.server.address().port); // this isn't c++ no matter how much I'd love it to :) %d

                if (this.cmdArgs.test && !this.cmdArgs.host) {
                    this.startTests();
                }
            }.bind(this));
        }
        else {
            console.warn('[App] Starting a secure localhost server. This is for testing only!');
            let fs = require('fs');
            let key = fs.readFileSync('./eido-key.pem');
            let cert = fs.readFileSync('./eido-cert.pem');

            let options = {
                key: key,
                cert: cert
            };

            let https = require('https');
            this.server = https.createServer(options, this.expressApp).listen(this.port);
        }
        
        // only enable public swagger file serving for the debug builds
        // if (this.server) {
        //     let apiPath = path.join(process.cwd(), "api");

        //     console.info(`[App] Serving swagger through path: ${apiPath}`);

        //     let docs_handler = express.static(apiPath);
        //     this.expressApp.get(/^\/docs(\/.*)?$/, function(req, res, next) {
        //         if (req.url === '/docs') { // express static barfs on root url w/o trailing slash
        //             res.writeHead(302, {
        //                 'Location': req.url + '/'
        //             });
        //             res.end();
        //             return;
        //         }
        //         // take off leading /docs so that connect locates file correctly
        //         req.url = req.url.substr('/docs'.length);
        //         return docs_handler(req, res, next);
        //     });
        // }

        if (this.cmdArgs.embedWSRouter)
            this.startWSRouterServer();

        console.info('[App] Starting a web socket server ...');
        this.wsServer = new WSServer(this);

        this.initServerRoutes();

        Util.getServerIPv4()
            .then((ip) => {
                this.serverIP = ip;
                this.serverId = ip + ":" + this.port + ":" + process.pid;

                this.startTick();

                // Connect to the ConnManagers.
                // if (this.wsServer)
                //     this.wsServer.initConnManagersConnections();
            })
            .catch((err) => {
                console.error('[App] Failed to get IP address for the server');
            });
    }

    initServerRoutes() {
        console.info('[App] Init server routes ...');

        /// Load all the routes
        Routes.routes.map((route, index) => this.initRoute(route));

        this.expressApp.get('/api-docs', (req: express.Request, res: express.Response, next: any) => this.getApiDocs(req, res, next));

        let apiPath = path.join(process.cwd(), "static", "api");
        console.log(`[App] Serving swagger through: ${apiPath}`);
        this.expressApp.use('/api', express.static(apiPath));
        
        console.info(`[App] ${Routes.routes.length} server routes initialised!`);
    }

    getApiDocs(req: express.Request, res: express.Response, next: any) {
        if (Env.release) {
            res.status(400).send('Only supported in debug mode!');
            return;
        }

        let docs = {
            title: "Eido [PSim]",
            description: "The front end of the PSim(c) ending by QuanVolve(c)",
            termsOfService: "http://www.quanvolve.com/terms/",
            contact: {
                name: "API Support",
                url: "http://www.quanvolve.com/support",
                email: "support@quanvolve.com"
            },
            license: {
                name: "Proprietary",
                url: "http://www.quanvolve.com/license"
            },
            version: "1.0.0",
            components: {
                schemas: getComponentDocs()
            },
            paths: this.getApiPaths()
        };

        res.header('Content-Type', 'application/json');
        res.status(200).send(docs);
    }

    getApiPaths() {
        let paths = {};

        this.handlers.map((handler: any, index: number) => {
            let info: Rest.Info = handler.info;

            if (!paths[info.path])
                paths[info.path] = {};

            let pathParts = info.path.split('/');
            let tag = pathParts.length >= 4 ? pathParts[3] : "default";
            let content = info.spec.content;
            let contentInfo = {};
            contentInfo[content] = {
                schema: {
                    type: info.spec.type
                }
            };

            let pathInfo = {
                tags: [tag],
                description: info.desc,
                produces: content,
                parameters: [],
                responses: {
                    "200": {
                        content: contentInfo
                    }
                }
            };

            info.spec.parameters.map((param: Rest.Param) => {
                pathInfo.parameters.push({
                    name: param.name,
                    required: param.required,
                    in: param.paramType,
                    description: param.desc,
                    schema: {
                        type: param.type,
                        enum: param["enum"]
                    }
                });
            });

            let method = info.method.toLowerCase();
            paths[info.path][method] = pathInfo;
        });

        return paths;
    }

    startWSRouterServer() {
        return;
        // console.info("[App] Starting an embedded instance of WSRouter");
        
        // require('path').resolve(__dirname, "..", "ws_router");
        // // require('ws_router.js');
        // require('path').resolve(__dirname);
    }

    getProcessStats() {
        let os = require('os');

        return {
            memoryUsage: process.memoryUsage(),
            title: process.title,
            pid: process.pid,
            cwd: process.cwd(),
            platform: process.platform,
            uptime: process.uptime(),
            os: {
                loadAverage: os.loadavg(),
                totalMem: os.totalmem(),
                freeMem: os.freemem(),
                uptime: os.uptime(),
                platform: os.platform(),
                architecture: os.arch(),
                release: os.release(),
                cpus: os.cpus()
            }
        };
    }

    startTick() {
        let conn = DB.getObjectConn();
        let collection = conn.collection(Collections.servers);
        let timeoutSeconds = 60;

        collection.createIndex({
            lastSeen: 1
        }, {
            expireAfterSeconds: timeoutSeconds * 5
        });

        collection.updateOne({
            _id: this.serverId
        }, {
            $set: {
                startTime: new Date(),
                lastSeen: new Date(),
                stats: this.getProcessStats(),
                clientWebSockets: {
                    bytesSent: 0,
                    bytesRecv: 0,
                    count: 0,
                    totalBytesSent: 0,
                    totalBytesRecv: 0
                },
                maxClientWebSockets: 0
            }
        }, {
            upsert: true
        }, function(err) {
            if (err)
                console.error('[App] DB Error:', err);
        });

        if (this.wsServer) {
            setInterval(function() {
                this.tick();
            }.bind(this), timeoutSeconds * 1000);
        }
    }

    tick() {
        if (!this.wsServer)
            return;

        this.wsServer.tick();

        return;

        // let conn = DB.getObjectConn();
        // let collection = conn.collection(Collections.servers);
        // let timestamp = new Date();

        // let clientWebSockets = this.wsServer.getClientWSInfo();
        // let dsSockets = clientWebSockets.dsSockets;

        // // console.trace('[App] Server tick: ', this.serverId);

        // delete clientWebSockets.dsSockets;

        // collection.update({
        //     _id: this.serverId,
        // }, {
        //     $set: {
        //         lastSeen: timestamp,
        //         stats: this.getProcessStats(),
        //         clientWebSockets: clientWebSockets
        //     },
        //     $max: {
        //         maxClientWebSockets: clientWebSockets.count
        //     }
        // }, {}, (err) => {
        //     if (err)
        //         console.error('[App] DB Error: ', err);
        // });

        // // now we want to remove dead servers from the wsCollections
        // WSConnection.updateAll(this.serverIP, this.port, timestamp);
    }

    marshalParam(req, param) {
        function getParamValue(parent, name: string) {
            Debug.assert(parent, 'Invalid parent object!');
            if (param.nested != true)
                return parent[name];
            return BaseObject.getNestedParam(parent, param.name);
        }
        
        let paramValue = getParamValue(req[param.paramType], param.name);

        if (paramValue === undefined) {
            if (param.paramType === 'body')
                paramValue = getParamValue(req.body, param.name);
            else if (param.internalParamType && param.internalParamType === 'field') {
                if (req.body['params'])
                    paramValue = getParamValue(req.body['params'], param.name);
                else if (req.body['fields'])
                    paramValue = getParamValue(req.body['fields'], param.name);
                else
                    paramValue = getParamValue(req.body, param.name);
            }
        }
        
        // if we do not get anything then we just return
        if (paramValue === undefined)
            return paramValue;

        let parser = Model.getParser(param.type);

        if (param.dataType && param.dataType == 'file')
            return null;

        if (parser)
            return parser.parse(paramValue, param);

        // check that its an object
        if (param.isUserClass || param.isUserContainerClass) {
            let cls = Model.getClass(param.type);
            Debug.assert(cls, 'Could not find class for param: ' + param.type);

            // create a new instance of the object and then copy the properties over
            let instance = new cls();
            instance.copyFrom(paramValue, {
                level: SecrecyLevel.REST
            });

            return instance;
        }

        return paramValue; // just return the value as is if no parser exists!

        // Debug.assert(false, 'Could not find parser for param: ' + param.type);
    }

    static initWSRequset(req: WSRequest, res: WSResponse, next: any, path: string = "", method: string = "", isWebSocket: boolean = false) {
        // path = path || Util.getRequestUrl(req);
        // method = method || req.method.toUpperCase();

        // let accessToken = req.headers['x-access-token'] || (req.body && req.body.accessToken) || (req.query && req.query.accessToken);
        let args: ServerArgs = new ServerArgs();

        args._appServer     = eido.appServer;
        args._req           = req;
        args._res           = res;
        args._next          = next;
        args._path          = path;
        args._isWebSocket   = !!isWebSocket, // boolea;
        args._method        = method;
        args._info          = null;
        args._appId         = req.query.appId;
        args._accessToken   = "";
        args._error         = new ErrorGen(res);
        args._cdn           = eido.appServer.cdn;
        args._noTask        = false;

        return args;
    }

    static initRequest(args: ServerArgs, req: express.Request, res: express.Response, next: any, path: string = "", method: string = "", isWebSocket: boolean = false) {
        path = path || Util.getRequestUrl(req);
        method = method || req.method.toUpperCase();

        console.log(`[AppServer] Request: ${path}@${method}`);

        let accessToken = req.headers['x-access-token'] || (req.body && req.body.accessToken) || (req.query && req.query.accessToken);

        args._appServer     = eido.appServer;
        args._req           = req;
        args._res           = res;
        args._next          = next;
        args._path          = path;
        args._isWebSocket   = !!isWebSocket, // boolea;
        args._method        = method;
        args._info          = null;
        args._appId         = req.query.appId;
        args._accessToken   = accessToken;
        args._error         = new ErrorGen(res);
        args._cdn           = eido.appServer.cdn;
        args._noTask        = false;

        return args;
    }

    static handleWSRequest(req, res, next, args) {
        // we just call the default implementation ...
        return BaseServer.handleRequest.apply(null, arguments);
    }

    static execHandler(inst, args) {
        try {
            let ret = inst.action(args);
            // if the return value is a promise then catch the errors
            if (ret && PromiseUtil.isPromise(ret)) {
                ret.catch(args._error.RESTError());
                return ret;
            }

            return PromiseUtil.resolvedPromise(args);
        }
        catch (e) {
            Error.unhandledException(e, args._res);
        }
    }
        
    static runValidationStage(validateCallbacks, args) {
        if (!validateCallbacks || !validateCallbacks.length)
            return PromiseUtil.resolvedPromise(true);

        /// This is the most common scenario
        if (validateCallbacks.length == 1) {
            return validateCallbacks[0].apply(args);
        }
        else {
            let funcs = validateCallbacks.map(validateCallback => () => { return validateCallback.apply(args) });
            return PromiseUtil.serial(funcs);
        }
        
        // new eido.Promise((resolve, reject) => {
        //     if (validateCallbacks && validateCallbacks.length) {
        //         console.trace("[App] Num validation callbacks: %d", validateCallbacks.length);
        //         let promises = new Array(validateCallbacks.length);

        //         for (let i = 0; i < validateCallbacks.length; i++) {
        //             Debug.assert(typeof validateCallbacks[i] == 'function', 'Validate callback is not a function');
        //             promises[i] = validateCallbacks[i](args);
        //         }

        //         // fulfill all the promises!
        //         Promise.allPromises(promises)
        //             .then(function(results) {
        //                 resolve();
        //             })
        //             .catch(args._error.RESTError());
        //     }
        //     else
        //         resolve(); // resvole the promise already!
        // });
    }

    // forcing static ... don't want unwanted closures!
    static handleRequest(req: express.Request, res: express.Response, next: any) {
        console.log(`[App] ${req.method}: ${req.path}`);
        let path = req.path;
        let method = req.method;

        let RestEPClass = eido.appServer.getHandler(path, method);
        Debug.assert(RestEPClass, `Unable to find handler for: ${path} [${method}]`);

        let args = new RestEPClass();
        BaseServer.initRequest(args, req, res, next);

        // if (!args)
        //     args = BaseServer.initRequest(req, res, next);

        // args._error.assert(args._appId === eido.appId, 403, "Access Denied: No API access!");


        let apiPermissionsCallbacks = null;

//        if (req.headers)
//            console.trace('[App/HTTP] HTTP Headers: ', req.headers);

        args._error.assert(RestEPClass && typeof RestEPClass === "function", 404, 'Not found -', args._method, '=>', args._path);

        let inst = args;
        let info: Rest.Info = RestEPClass.info;
        args._info = RestEPClass.info;

        let params = info.spec.parameters;

        for (let i = 0; i < params.length; i++) {
            let param: Rest.Param = params[i];
            
            // we ignore nested parameters
            if (param.nested === true)
                continue;
            
            // if its a user class then we just create a nested object of that type
            if (param.isUserContainerClass == true) {
                let paramValue = eido.appServer.marshalParam(req, param);
                if (paramValue)
                    BaseObject.setNestedParam(args, param.name, paramValue);
                continue;
            }

            let paramValue = eido.appServer.marshalParam(req, param);
            let paramValueValid = !param.required;

            if (param.required) {
                paramValueValid = !(paramValue == undefined || typeof paramValue == 'undefined');
            }

            args._error.assert(paramValueValid, 400, `Invalid value specified for param: ${param.name}`);

            if (paramValue != undefined) {
                if (!param.nested)
                    args[param.name] = paramValue;
                else
                    BaseObject.setNestedParam(args, param.name, paramValue);
            }
        }

        try {
            let validateCallbacks = [inst.validate];
            let preValidateCallbacks = [inst.preValidate];
            let postValidateCallbacks = [inst.postValidate];

            // if (info.permissions) {
            //     if (info.permissions instanceof Array) {
            //         info.permissions.map((permission) => {
            //             let permissionCallback = ApiPermissionsCallbacks[permission];
            //             Debug.assert(permissionCallback && typeof permissionCallback == 'function', `Invalid permissions: ${permission}`);
            //             preValidateCallbacks.push(permissionCallback);
            //         });
            //     }
            //     else {
            //         let permissionCallback = ApiPermissionsCallbacks[info.permissions];
            //         Debug.assert(permissionCallback && typeof permissionCallback == 'function', `Invalid permissions: ${info.permissions}`);
            //         preValidateCallbacks.push(permissionCallback);
            //     }
            // }
            // if there are validation callbacks being specified then do the validation first

            // run the pre-validation stage ...
            console.trace("[App] Running pre-validation");
            BaseServer.runValidationStage(preValidateCallbacks, args)
                .then(() => {
                    console.trace("[App] Running permissions");
                    return info.permissions.run(args)
                })
                // then the validation stage ...
                .then(() => {
                    console.trace("[App] Running validation");
                    return BaseServer.runValidationStage(validateCallbacks, args)
                })
                // and finally the post-valitagion stage
                .then(() => {
                    console.trace("[App] Running post-validation");
                    return BaseServer.runValidationStage(postValidateCallbacks, args)
                })
                // ... at last we can run the handler now!
                .then(() => {
                    console.trace("[App] Executing handler ...");
                    BaseServer.execHandler(inst, args);
                })
                // handle the errors!
                .catch((err) => {
                    if (args._redirect) {
                        console.info(`[App] Validation failed. Redirecting to: ${args._redirect}`);
                        args._res.redirect(401, args._redirect);
                    }
                    else {
                        console.error('[App] Validation failed with error(s)');
                        if (err.logAndSend)
                            err.logAndSend(res);
                        else
                            Error.unhandledException(err, res);
                    }
                });
        }
        catch (e) {
            args._error.reportRESTError(e);
        }
    }

    // setup(path, spec, method, callback, validateCallbacks, preValidateCallbacks, postValidateCallbacks) {
    //     let info = {
    //         spec: spec
    //     };

    //     let spec = info.spec;

    //     Debug.assert(spec.description, 'Must specify a valid description');
    //     Debug.assert(spec.parameters, 'Must specify a valid parameters or an empty array if none required');
    //     Debug.assert(path, 'Must specify a valid path');
        
    //     // remove user classes from the parameter list
    //     if (spec.hasUserContainerClasses) {
    //         let parameters = [];

    //         for (let i = 0; i < spec.parameters.length; i++) {
    //             let sparam = spec.parameters[i];
    //             if (sparam.isUserContainerClass) {
    //                 continue;
    //             }
    //             parameters.push(sparam);
    //         }
            
    //         spec.orgParameters = spec.parameters;
    //         spec.parameters = parameters;
    //     }

    //     spec.path = path.replace("[apiRoot]", this.apiRoot).replace("[apiVersion]", this.apiVersion);

    //     if (!spec.summary)
    //         spec.summary = spec.description;
    //     if (!spec.notes)
    //         spec.notes = spec.description;

    //     if (!spec.produces)
    //         spec.produces = ["application/json"];

    //     spec.method = method;
    //     spec.nickname = spec.method.replace('/', '');

    //     info.category = spec.category;
    //     info.callback = callback;
    //     info.validateCallbacks = validateCallbacks;
    //     info.preValidateCallbacks = preValidateCallbacks;
    //     info.postValidateCallbacks = postValidateCallbacks;

    //     info.action = eido.BaseServer.handleRequest;

    //     return info;
    // }

    static makeName(path: string, type: string = "POST") {
        let typeU = type.toUpperCase();
        return typeU + '->' + path;
    }

    fixRestPath(path: string) {
        return path.replace("[apiRoot]", this.apiRoot).replace("[apiVersion]", this.apiVersion);
    }

    getHandler(path: string, type: string) {
        path = this.fixRestPath(path);
        let name = BaseServer.makeName(path, type);
        return this.handlersLUT[name];
    }

    initHandler(EPClass: any, method: string) {
        Debug.assert(EPClass.info, `[App] Invalid end point class with no info!`);

        let epInfo: Rest.Info = EPClass.info;

        epInfo.path = this.fixRestPath(epInfo.path);
        // RestEPClass.spec.method = RestEPClass.spec.path;

        if (epInfo.spec.type) 
            Debug.assert(Model.models[epInfo.spec.type], `Invalid model class being returned: ${epInfo.spec.type}`);

        // if (epInfo.permissions) {
        //     let permissionCallback = eido.apiPermissionsCallbacks[epInfo.permissions];
        //     Debug.assert(permissionCallback && typeof permissionCallback == 'function', 'Invalid permissions:', epInfo.permissions);
        // }

        let params = epInfo.spec.parameters;
        epInfo.__params = epInfo.__params || {};

        for (let i = 0; i < params.length; i++) {
            let param = params[i];
            
            if (param.isUserContainerClass)
                epInfo.spec.hasUserContainerClasses = true;
                
            epInfo.__params[param.name] = param;
        }

        let name = BaseServer.makeName(epInfo.path, method);
        console.info(`[App] \t\t [${method.toUpperCase()}] => ${epInfo.path}`);

        Debug.assert(!this.handlersLUT[name], `Handler already exists for path: ${epInfo.path}`);

        this.handlersLUT[name] = EPClass;
        this.handlers.push(EPClass);

        // let info = this.setup(path, spec, type, callback, validateCallbacks, preValidateCallbacks, postValidateCallbacks);

        let middleWares = epInfo.middleWares;

        if (!middleWares.length) {
            this.expressApp[method.toLowerCase()](epInfo.path, BaseServer.handleRequest);
        }
        else {
            let epArgs: any[] = [epInfo.path];
            middleWares.map((middleware) => epArgs.push(middleware));
            epArgs.push(BaseServer.handleRequest);
            this.expressApp[method.toLowerCase()].apply(this.expressApp, epArgs);
        }
    }

    initRoute(EPClass) {
        Debug.assert(EPClass.info, `[App] Invalid end point class with no info!`);
        let epInfo: Rest.Info = EPClass.info;
        return this.initHandler(EPClass, epInfo.method);
    }

    delete(RestEPClass) {
        return this.initHandler(RestEPClass, 'DELETE');
    }

    patch(RestEPClass) {
        return this.initHandler(RestEPClass, 'PATCH');
    }

    put(RestEPClass) {
        return this.initHandler(RestEPClass, 'PUT');
    }

    post(RestEPClass) {
        return this.initHandler(RestEPClass, 'POST');
    }

    get(RestEPClass) {
        return this.initHandler(RestEPClass, 'GET');
    }

    ws(RestEPClass) {
        return this.initHandler(RestEPClass, 'WS');
    }

    timedExit(code, timeout) {
        if (!timeout)
            timeout = 10 * 1000;
    
        console.error('[App] FATAL ERROR! EXITING IN: ' + timeout + ' milliseconds!');
    
        setTimeout(function() {
            console.error('[App] Exiting with code: ' + code);
            process.exit(code);
        }, timeout)
    }
    
    static get testConfig() {
        return {
            appName: 'Test-App',
            componentName: 'Test-Component',
            branchName: 'Test-Branch'
        };
    }
    
    setupSignals(appDomain) {
        if (this.isTestEnvironment() || !Env.release)
            return;
            
        process.on('exit', function(code) {
            console.info('[App] Exit -', code);
        });

        process.on('SIGINT', function() {
            console.info('[App] Exit - SIGINT. Arguments:', arguments);
            process.exit(0);
        });

        process.on('SIGTERM', function() {
            console.info('[App] Exit - SIGTERM. Arguments:', arguments);
            process.exit(1);
        });
        
        function timedExit(code: number, timeout: number = 10 * 1000) {
            console.error('[App] FATAL ERROR! EXITING IN:', timeout, 'milliseconds!');
        
            setTimeout(function() {
                console.error('[App] Exiting with code:', code);
                process.exit(code);
            }, timeout)
        }
        
        appDomain.on('error', function(err) {
            console.error('[AppDomain] Error received: ', err, err.stack);
            timedExit(2);
        });
        
        process.on('uncaughtException', function(err) {
            console.error('[AppDomain] Unhandled exception!: ', err);
            timedExit(2);
        });
        
        process.on("unhandledRejection", function(reason, promise) {
            console.error('[AppDomain] Unhandled promise rejection. Reason: ', reason, "- Promise:", promise);
            timedExit(3);
        });
        
        process.on("rejectionHandled", function(promise) {
            console.error('[AppDomain] Unhandled promise: ', promise);
            timedExit(3);
        });
    }
}

// let app = new eido.BaseServer();
// eido.appServer = app;

// require('../model/sv_model.js')(app);
// require('../services/s_router.js')(app);
// require('../system/cdn/cdn.js')(app);

// app.setupSignals();

// appDomain.run(function() {
//     this.start();
// }.bind(app));
