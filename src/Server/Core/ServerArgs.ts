import { User } from '../Model/User/SvUser';
import { Alpha } from '../Model/Config/SvAlpha';
import { Strategy } from '../Model/Config/SvStrategy';
import { ErrorGen } from './Error';
import BaseServer from './BaseServer';
import * as express from 'express';
import { Rest } from '../Services/RouterUtil';
import WSRequest from './WS/WSRequest';
import WSResponse from './WS/WSResponse';
import { Session } from '../Model/User/SvSession';
import { ObjectID } from "Shared/Core/ObjectID";
import { AppServer } from 'Server/App/EidoApp';
import { CDN } from 'Server/System/CDN/CDN';

export class Cached {
    session             : Session = null;
    user                : User = null;
    targetUser          : User = null;
    alpha               : Alpha = null;
    strategy            : Strategy = null;
    master              : Strategy = null;
}

export class ServerArgs {
    _appServer          : AppServer = null;;
    _req                : express.Request | WSRequest = null;
    _res                : express.Response | WSResponse = null;
    private _redirect   : string = "";
    _next               : any = null;
    _path               : string = ""; 
    _isWebSocket        : boolean = false;
    _method             : string = "";
    _info               : any = null;
    _appId              : string = "";
    _accessToken        : string = "";
    _cdn                : CDN = null;
    _noTask             : boolean = false;
    
    _cached             : Cached = new Cached();
    _error              : ErrorGen = null;

    constructor() {
        this._redirect  = "";
    }

    user(): User         { return this._cached.user;        }
    userId(): ObjectID { 
        return this._cached.user._id;
    }

    assert(exp, code: number, message: string) { 
        this._error.assert(exp, code, message); 
    }

    redirect(path: string) {
        console.info(`[App] Redirecting to path: ${path}`);
        this._redirect = path;
    }

    getHomeUrl() {
        return this._appServer.rawConfig.postLogin;
    }

    getLoginUrl() {
        return this._appServer.rawConfig.login;
    }

    gotoHome() {
        this.redirect(this._appServer.rawConfig.postLogin);
    }
    
    gotoLogin() {
        this.redirect(this._appServer.rawConfig.login);
    }

    redirectLogin() {
        this.redirect('/dk/login');
    }

}
