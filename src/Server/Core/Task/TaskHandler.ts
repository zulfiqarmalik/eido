import { db } from 'Server/Core/DB/DBManager';
import { Error, ErrorGen } from 'Server/Core/Error';
import { ShUtil, Debug, PromiseUtil } from 'Shared/Core/ShUtil';
import { Model } from 'Shared/Model/ShModel';
import { Util } from 'Server/Core/SvUtil';
import { CObject } from 'Shared/Core/CObject';

import { PushNotificationType } from 'Shared/Core/PushNotificationType';
import PushNotification from 'Server/System/PushNotification';

import { Task } from 'Server/Model/Task/SvTask'
import TaskResult from 'Server/Model/Task/TaskResult'
import { ServerArgs } from '../ServerArgs';
import { SecrecyLevel } from '../../../Shared/Model/SecrecyLevel';

export default class TaskHandler extends ServerArgs {
    _task           : Task = null;
    static info     : any = null;

    constructor() {
        super();
    }

    runTask() {
        Debug.assert(false, "TaskHandler::runTask - implementation must implement");
        return null;
    }

    getId(): string {
        Debug.assert(false, "TaskHandler::getId - implementation must implement");
        return null;
    }

    getArgs() {
        let epInfo = TaskHandler.info;
        let epParams = epInfo.spec.parameters;
        let args = {};
        
        epParams.map((epParam) => {
            args[epParam.name] = this[epParam.name];
        });

        return args;
    }

    createTask(writeResults: boolean = false): Promise<Task> {
        /// First of all we try to see whether a task has already been completed with the required results ...
        return CObject.dbLoadOne({
            collectionName: TaskResult.collectionName,
            cls: null,
            query: {
                $and: [{
                    _id: this.getId()
                }, {
                    lastUpdated: {
                        $gte: ShUtil.startOfDayToday()
                    }
                }]
            }
        }).then((taskResult) => {
            // if (taskResult && taskResult.result) {
            //     console.log("[TaskHandler] Sending existing result: %s - From Date: %s", taskResult._id, taskResult.lastUpdated || new Date());
            //     CObject.sendClientRaw(serverArgs, taskResult.httpStatus, taskResult.result);
            //     return;
            // }

            this._task = new Task();
            this._task.create(this, this)
                .then(() => {
                    console.log("[TaskHandler] Sending task id to client!");
                    /// Send the task to the client ...
                    this._task.sendClient(this, 200, SecrecyLevel.public);

                    /// And then we start the task
                    return this._task.begin();
                });

            return this._task;
        });
    }

    updateStatusMessage(statusMessage: string) {
        if (!this._task)
            return;

        this._task.updateStatusMessage(statusMessage);
    }

    update(percent: number, statusMessage: string) {
        if (!this._task)
            return;

        this._task.update(percent, statusMessage);
    }

    tick() {
    }
}
