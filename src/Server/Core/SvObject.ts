import { CopyOptions, BaseObject } from 'Shared/Core/BaseObject';
import { Env, eido } from 'Shared/Core/ShCore';
import { ObjectID } from "Shared/Core/ObjectID";
import { Debug, PromiseUtil } from 'Shared/Core/ShUtil';
import { TypeParser, Model, ModelProperties, ModelProperty } from 'Shared/Model/ShModel';
import { SecrecyLevel } from 'Shared/Model/SecrecyLevel';
import { CObject } from 'Shared/Core/CObject';
import { ServerArgs } from 'Server/Core/ServerArgs';
require('../../Shared/Core/Array.js');

function DB() {
    return eido.DB;
}

declare module 'Shared/Core/CObject' {
    interface CObject extends BaseObject {
        collectionName: string;
        dbName: string;

        copyFrom(src: any, options?: CopyOptions);
        getCollectionName();
        getCollection();
        getDBName();
        makeDataFromRow(row: any);
        embedCloneRef();
        embedClone(level: SecrecyLevel);
        cleanClone(options?: CopyOptions);
        isEmbedded();
        canHandleDBObjects();
        loadFromId(id: any, cls: any);
        getUserId(args: ServerArgs);
        isDocObject();
        setupSearch();
        parseSet(set);
        dbLoad(qinfo: any, treatSingleResultsSpecially?: boolean);
        dbLoadOne(qinfo?: any);
        docDelete(qinfo: any);
        dbDelete(qinfo?: any);
        dbBulkInsert(qinfo: any);
        dbInsert(qinfo?: any);
        dbUpdate(qinfo?: any);
        dbFindAndModify(qinfo: any);
        dbClone();
        send(args: ServerArgs, status: number, level: SecrecyLevel);
        sendClient(args: ServerArgs, status: number, level?: SecrecyLevel);
        addToSet(data: any);
        removeFromSet(data: any);
        getLevel(propName: string);
        pushData(query: string, data: any, index: number, level: SecrecyLevel);
        dbUpdateFromDiff(qinfo: any, current: any, update?: any);
    }
}

CObject.prototype.copyFrom = function(src, options?: CopyOptions) {
    let ref: any = CObject.dbRef(src);

    // if its not a DBRef then just call the base implementation
    if (!ref)
        return BaseObject.prototype.copyFrom.call(this, src, options);

    return this.loadFromId(ref._id, ref.cls);
}

CObject.prototype.makeDataFromRow = function(row: any): CObject {
    return this.copyFrom(row, new CopyOptions(SecrecyLevel.all, true));
}

CObject.makeDataFromRows = function(cls: any, rows: any[]) {
    let results = new Array(rows.length);
    let promises = [];

    (results as any).__classId = cls.__classId;

    // construct an array of objects
    for (let i = 0; i < rows.length; i++) {
        let row = rows[i];
        let o = new cls();
        
        let ret = o.makeDataFromRow(row, {
            isDB: true,
            level: SecrecyLevel.all
        });

        if (PromiseUtil.isPromise(ret)) 
            promises.push(ret);

        results[i] = o;
    }

    if (promises.length) {
        return Promise.all(promises).then(function() {
            return this;
        }.bind(results));
    }

    return PromiseUtil.resolvedPromise(results);
}

CObject.prototype.getCollection = function(qinfo: any = null): any {
    let conn = qinfo.dbConn || DB().getObjectConn(qinfo);
    return conn.collection(this.getCollectionName());
}

CObject.getCollection = function(qinfo: any = null) {
    Debug.assert(this.collectionName, `No collectionName for class: ${this.name}`);
    let conn = qinfo.dbConn || DB().getObjectConn(qinfo);
    return conn.collection(this.collectionName);
}

CObject.prototype.embedCloneRef = function() {
    Debug.assert(this["_id"], 'Invalid, cannot $ref an object without an id!');
    Debug.assert((CObject as typeof CObject).__classId, 'Invalid classId');

    return {
        _dbRef: true,
        _id: this["_id"],
        __classId: (CObject as typeof CObject).__classId
    };
}

CObject.prototype.embedClone = function(level: SecrecyLevel = SecrecyLevel.public) {
    if (level & SecrecyLevel.embeddedRef) {
        return this.embedCloneRef();
    }

    return this.cleanClone(level);
}

CObject.prototype.cleanClone = function(options: CopyOptions | SecrecyLevel) {
    if (!(options instanceof CopyOptions))
        options = new CopyOptions(options as SecrecyLevel);

    if (options && (options.level & SecrecyLevel.embeddedRef))
        return this.embedClone(options.level);

    return BaseObject.prototype.cleanClone.call(this, options);
}

CObject.prototype.isEmbedded = function() {
    return false;
}

CObject.prototype.canHandleDBObjects = function() {
    return true;
}

CObject.dbRef = function(ref) {
    if (typeof ref._dbRef !== 'undefined' && ref._dbRef === true) {
        let cls = Model.getClass(ref.__classId);

        return {
            _id: ref._id,
            cls: cls
        };
    }

    // Debug.assert(ref, 'Invalid db-ref passed!');
    // if (typeof ref._bsontype === 'string') {
    //     return {
    //         collectionName: ref.namespace,
    //         id: ref.oid
    //     };
    // }

    return null;
}

CObject.prototype.loadFromId = function(id: any, cls: any): CObject {
    if (typeof id === 'string')
        id = ObjectID.create(id);

    this["_id"] = id;

    return this.dbLoadOne();
}

CObject.prototype.getUserId = function(args: ServerArgs) {
    return args.userId ? args.userId : args._cached.user.email;
}

CObject.prototype.isDocObject = function() {
    return true;
}

CObject.prototype.setupSearch = function() {
    Debug.assert(false, 'Must be overridden by implementation');
}

CObject.parseSet = function(set, object) {
    let parts = set.split('.');

    for (let i = 0; i < parts.length; i++) {
        let part = parts[i];
        if (part[0] == '{') {
            let name = part.substring(1, part.length - 1);
            Debug.assert(object[name], 'Cannot have empty value for part: ' + part);
            parts[i] = object[name];
        }
    }

    return parts;
}

CObject.combineSet = function(parts) {
    return parts.join('.');
}

CObject.getBulkKey = function() { return '*'; }

CObject.makeSubsetDataFromRows = function(cls, rows, parts, isBulk) {
    let results = new Array<typeof cls>();

    for (let i = 0; i < rows.length; i++) {
        let src = rows[i];
        for (let j = 0; src && j < parts.length; j++) {
            src = src[parts[j]];
        }

        if (!src) {
            results[i] = null;
            continue;
        }

        if (src instanceof Array) {
            src.forEach(function(s) {
                let dst = new cls();
                dst.copyFrom(s, {
                    isDB: true,
                    level: SecrecyLevel.all
                });
                results.push(dst);
            });
        }
        else if (isBulk) {
            for (let s in src) {
                if (!src.hasOwnProperty(s))
                    continue;
                let dst = new cls();
                dst.copyFrom(src[s], {
                    isDB: true,
                    level: SecrecyLevel.all
                });
                results.push(dst);
            }
        }
        else if (typeof(src) == 'object') {
            let dst = new cls();
            dst.copyFrom(src, {
                isDB: true,
                level: SecrecyLevel.all
            });
            results.push(dst);
        }
    }

    return results;
}

CObject.getNestedObject = function(src, set, object) {
    if (!src)
        return null;

    let parts = set instanceof Array ? set : this.parseSet(set, object);

    parts.forEach(function(part) {
        src = src[part];
        if (!src)
            return src;
    });

    return src;
}

CObject.find = function(qinfo, collectionName, callback) {
    let conn = qinfo.dbConn || DB().getObjectConn(qinfo);
    let collection = conn.collection(collectionName);

    collection.find(qinfo, callback);
}

CObject.convertResults = function(qinfo, rows) {
    Debug.assert(qinfo && qinfo.deferred, "Invalid information passed!");
    
    if (qinfo.treatSingleResultsSpecially && rows.length === 1 && qinfo.self && 
        qinfo.self.constructor && qinfo.self.constructor.__classId === qinfo.cls.__classId) {
        let promise = qinfo.self.makeDataFromRow(rows[0]);

        if (PromiseUtil.isPromise(promise)) {
            promise.then(function(result) {
                this.deferred.resolve(result);
            }.bind(qinfo));
        }

        return qinfo.deferred.resolve(qinfo.self);
    }
    else {
        this.makeDataFromRows(qinfo.cls, rows)
            .then(function(results) {
                if (this.treatSingleResultsSpecially && results.length === 1)
                    return this.deferred.resolve(results[0]);

                this.deferred.resolve(results);
            }.bind(qinfo));
    }

    return qinfo.deferred.promise;
}

CObject.loadAll = function(qinfo) {
    let skip = qinfo.$skip;
    let limit = qinfo.$limit;
    
    delete qinfo.$skip;
    delete qinfo.$limit;
    
    if ((!qinfo.cls && qinfo.cls !== null) || typeof(qinfo.cls) === 'undefined')
        qinfo.cls = this;

    let collectionName = qinfo.collectionName ? qinfo.collectionName : qinfo.cls.collectionName;
    Debug.assert(collectionName, `Invalid qinfo passed: ${qinfo}`);
    // Debug.assert(qinfo && qinfo.cls && qinfo.cls.collectionName) || qinfo.collectionName), `[CObject] Invalid qinfo passed: ${qinfo}`);

    if (!qinfo.dbName && qinfo.self) {
        qinfo.dbName = qinfo.self.getDBName();
        Debug.assert(qinfo.dbName, 'Invalid DB name for object');
    }

    let treatSingleResultsSpecially = qinfo.treatSingleResultsSpecially;

    qinfo.extendedQuery = false;

    for (let key in qinfo) {
        if (key.charAt(0) === '$') {
            qinfo.extendedQuery = true;
            break;
        }
    }

    if (typeof(qinfo.parts) === 'string') {
        qinfo.parts = qinfo.parts.split('.');
    }

    let conn = qinfo.dbConn || DB().getObjectConn(qinfo);
    qinfo.collection = conn.collection(collectionName);

    // first of all check the cache first
    qinfo.deferred = PromiseUtil.defer();

    if (!qinfo.extendedQuery || !qinfo.id) {
        let query = qinfo.query;

        if (!query && qinfo.id) {
            query = {
                _id: qinfo.id
            };
        }

        let fnPromise = null;
        if (qinfo.treatSingleResultsSpecially === true) {
            fnPromise = new Promise((resolve, reject) => {
                qinfo.collection.findOne(query, (err, doc) => !err ? resolve(doc) : reject(err)) ;
            });
        }
        else {
            let cursor = qinfo.collection.find(query);

            if (cursor) {
                if (typeof skip !== "undefined")
                    cursor.skip(skip);

                if (typeof limit !== "undefined")
                    cursor.limit(limit);

                if (qinfo.sort)
                    cursor.sort(qinfo.sort);
            }

            fnPromise =new Promise((resolve, reject) => {
                cursor.toArray((err, r) => !err ? resolve(r) : reject(err));
            });
        }

        fnPromise
            .then(function(rows) {
                if ((this.treatSingleResultsSpecially === true && !rows) || 
                    (!this.treatSingleResultsSpecially && (!rows || !rows.length))) {
                    if (this.treatSingleResultsSpecially)
                        return this.deferred.resolve(null);
                    return this.deferred.resolve(new Array());
                }

                if (this.cls) {
                    if (!require('util').isArray(rows))
                        rows = [rows];

                    this.cls.convertResults(this, rows);
                }
                else {
                    if (this.treatSingleResultsSpecially && rows.length === 1)
                        this.deferred.resolve(rows[0]);
                    else
                        this.deferred.resolve(rows);
                }
            }.bind(qinfo))
            .catch(function(err) {
                console.error('[DB] Database error:', err);
                return this.deferred.reject(eido.coreUtil.createError(500, err));
            }.bind(qinfo));

        return qinfo.deferred.promise;
    }

    qinfo.isBulk = false;

    if (qinfo.parts.length && qinfo.parts[qinfo.parts.length - 1] === CObject.getBulkKey()) {
        qinfo.isBulk = true;
        qinfo.parts.pop();
    }

    let findQuery = qinfo.query ? qinfo.query : { _id: qinfo.id };

    let fn = "find";
    if (qinfo.treatSingleResultsSpecially === true) 
        fn = "findOne";

    CObject[fn](findQuery, collectionName, function(err, rows) {
        if (err) {
            console.error('[DB] Database error:', err);
            return this.deferred.reject(eido.coreUtil.createError(500, err));
        }

        if (this.cls) {
            let results = this.cls.makeSubsetDataFromRows(this.cls, rows, qinfo.parts, this.isBulk);

            if (treatSingleResultsSpecially && results.length === 1) {
                if (!results[0])
                    return this.deferred.resolve(null);

                return this.deferred.resolve(results[0]);
            }

            this.deferred.resolve(results);
        }
        else
            this.deferred.resolve(rows);
    }.bind(qinfo));

    // if (typeof skip !== "undefined")
    //     cursor.skip(skip);
        
    // if (typeof limit !== "undefined")
    //     cursor.limit(limit);

    return qinfo.deferred.promise;
}

CObject.dbExec = function(qinfo) {
    let collectionName = (qinfo.self ? qinfo.self.getCollectionName() : "") || qinfo.collectionName;
    Debug.assert(qinfo && (qinfo.self || collectionName), 'Invalid query info object passed!');
        
    qinfo.dbName = qinfo.dbName ? qinfo.dbName : qinfo.self.getDBName();
    Debug.assert(qinfo.dbName, 'Invalid DB name for object');

    qinfo.collectionName = qinfo.collectionName ? qinfo.collectionName : qinfo.self.getCollectionName();
    Debug.assert(qinfo.collectionName, 'Invalid collection name for object');

    qinfo.dbName = qinfo.dbName ? qinfo.dbName : qinfo.self.getDBName();
    Debug.assert(qinfo.dbName, 'Invalid DB name for object');

    if (!qinfo.level)
        qinfo.level = SecrecyLevel.db;

    let conn = qinfo.dbConn || DB().getObjectConn(qinfo);
    
    qinfo.deferred = PromiseUtil.defer();
    qinfo.collection = conn.collection(qinfo.collectionName);
    qinfo.extendedQuery = false;

    for (let key in qinfo) {
        if (key.charAt(0) === '$') {
            qinfo.extendedQuery = true;
            break;
        }
    }

    if (!qinfo.extendedQuery) {
        let cleanClone = qinfo.self.cleanClone({
            level: qinfo.level
        });

        if (qinfo.constants) {
            for (let key in qinfo.constants) {
                cleanClone[key] = qinfo.constants[key];
            }
        }

        if (!qinfo.upsert) {
            if (!cleanClone._id) // this isn't needed and mongodb is smart enough to deal with this . let it do its job.
                cleanClone._id = qinfo.self._id;

            (new Promise((resolve, reject) => {
                qinfo.collection.insertOne(cleanClone, (err, r) => !err ? resolve(r) : reject(err));
            })).then(function() {
                    this.deferred.resolve(this.self);
                }.bind(qinfo))
                .catch(function(err) {
                    console.error('[DB] Database error:', err);
                    return this.deferred.reject(eido.coreUtil.createError(500, err));
                }.bind(qinfo));
        }
        else {
            (new Promise((resolve, reject) => {
                qinfo.collection.update({ _id: qinfo.self._id }, cleanClone, { upsert: true }, (err, r) => !err ? resolve(r) : reject(err));
            })).then(function() {
                    this.deferred.resolve(this.self);
                }.bind(qinfo))
                .catch(function(err) {
                    console.error('[DB] Database error:', err);
                    return this.deferred.reject(eido.coreUtil.createError(500, err));
                }.bind(qinfo));
        }

        return qinfo.deferred.promise;
    }

    let updateObj = {};

    for (let key in qinfo) {
        if (key.charAt(0) === '$') {
//                if (typeof qinfo[key] === 'string') 
//                    updateObj[key][qinfo[key]] = self.cleanClone({ level: qinfo.level });
//                else
            updateObj[key] = qinfo[key];
        }
    }

    qinfo.query = qinfo.query ? qinfo.query : {
        _id: qinfo._id || qinfo.self._id || qinfo.self._id
    };

    // qinfo.collection.update(qinfo.query, updateObj, true)
    (new Promise((resolve, reject) => {
        qinfo.collection.updateMany(qinfo.query, updateObj, (err, r) => !err ? resolve(r) : reject(err));
    })).then(function() {
            this.deferred.resolve(this.self);
        }.bind(qinfo))
        .catch(function(err) {
            console.error('[DB] Database error:', err);
            return this.deferred.reject(err);
        }.bind(qinfo));

    return qinfo.deferred.promise;
}

CObject.advancedSearch = function(qinfo) {
    Debug.assert(qinfo && qinfo.query, 'Invalid query info passed for aggregate loading!');

    if (!qinfo.cls)
        qinfo.cls = this.constructor;

    if (!qinfo.dbName) {
        Debug.assert(qinfo.cls, 'Invalid class provided!');
        qinfo.dbName = qinfo.cls.dbName;
    }

    if (!qinfo.collectionName) {
        Debug.assert(qinfo.cls, 'Invalid class provided!');
        qinfo.collectionName = qinfo.cls.collectionName;
    }

    Debug.assert(qinfo.collectionName, 'Invalid collection name for object!');

    let conn = qinfo.dbConn || DB().getObjectConn(qinfo);
    let collection = conn.collection(qinfo.collectionName);
    let query = qinfo.query;
    let searchType = qinfo.searchType || 'aggregate';
    
    qinfo.deferred = PromiseUtil.defer();

    collection[searchType](query, function(err, rows) {
        if (err) {
            console.error('[DB] Database error:', err);
            return this.deferred.reject(err);
        }

        // no error ... filter out the rows
        let filteredData = new Array();

        if (this.filter && typeof this.filter === 'function') {
            rows.forEach(function(row) {
                let data = this.filter(row)
                if (data)
                    filteredData.push(data);
            });
        }
        else {
            filteredData = rows;
        }

        // single result
        if (!filteredData.length) {
            return this.deferred.resolve(new Array());
        }

        if (!this.noConvert) 
            return this.cls.convertResults(this, filteredData);
        else 
            this.deferred.resolve(filteredData);

    }.bind(qinfo));

    return qinfo.deferred.promise;
}

CObject.search = function(qinfo) {
    qinfo.searchType = 'find';
    return this.advancedSearch(qinfo);
}

CObject.dbLoad = function(qinfo) {
    if (typeof qinfo == 'undefined' || !qinfo)
        qinfo = {};

    qinfo.parts = qinfo.$set ? this.parseSet(qinfo.$set, {}) : null;

    return this.loadAll(qinfo);
}

CObject.dbLoadOne = function(qinfo?: any) {
    if (typeof qinfo == 'undefined' || !qinfo) {
        qinfo = {
            treatSingleResultsSpecially: true
        };
    }
    else {
        qinfo.treatSingleResultsSpecially = true;
    }

    return this.dbLoad(qinfo);
}

CObject.prototype.dbLoad = function(qinfo?: any, treatSingleResultsSpecially: boolean = false) {
    if (!qinfo)
        qinfo = {};
    
    if (typeof qinfo.id === 'undefined' && typeof qinfo.query === 'undefined') {
        if (!this["_id"])
            qinfo.id = !qinfo.$set ? this["_id"] : this["_id"];
        else
            qinfo.id = this["_id"];
    }

    if (treatSingleResultsSpecially)
        qinfo.treatSingleResultsSpecially = treatSingleResultsSpecially;

    qinfo.self = this;
    if (qinfo.cls !== null || typeof(qinfo.cls) === 'undefined')
        qinfo.cls = this.constructor;

    if (!qinfo.collectionName)
        qinfo.collectionName = this.getCollectionName();
    if (!qinfo.dbName)
        qinfo.dbName = this.getDBName();

    return CObject.dbLoad(qinfo);
}

CObject.prototype.dbLoadOne = function(qinfo?: any) {
    return this.dbLoad(qinfo, true);
}

CObject.prototype.docDelete = function(qinfo: any) {
    let collectionName = qinfo.collectionName ? qinfo.collectionName : this.getCollectionName();

    Debug.assert(collectionName, 'Invalid collection name for object');

    let conn = qinfo.dbConn || DB().getObjectConn(qinfo);
    
    qinfo.collection = conn.collection(collectionName);
    qinfo.deferred = PromiseUtil.defer();

    if (!qinfo.$set && !qinfo.$pull) {
        // qinfo.collection.remove({ _id: this["_id"] })
        (new Promise((resolve, reject) => {
            qinfo.collection.deleteOne({ _id: this["_id"] }, (err, r) => !err ? resolve(r) : reject(err));
        })).then(function() {
                this.deferred.resolve();
            }.bind(qinfo))
            .catch(function(err) { 
                /// If no error is specified then just return ...
                if (this.noError) 
                    return this.deferred.resolve();
                
                console.error('[DB] Database error:', err);
                return this.deferred.reject(eido.coreUtil.createError(500, err));
            }.bind(qinfo));

        return qinfo.deferred.promise;
    }
    else if (qinfo.$pull) {
        let query = qinfo.query ? qinfo.query : { _id: this["_id"] };

        // qinfo.collection.update(query, { $pull: qinfo.$pull }, true)
        (new Promise((resolve, reject) => {
            qinfo.collection.updateMany(query, { $pull: qinfo.$pull }, (err, r) => !err ? resolve(r) : reject(err));
        })).then(function() {
                this.deferred.resolve(this);
            }.bind(qinfo))
            .catch(function(err) {
                /// If no error is specified then just return ...
                if (this.noError)
                    return this.deferred.resolve();

                console.error('[DB] Database error:', err);
                return this.deferred.reject(eido.coreUtil.createError(500, err));
            }.bind(qinfo));

        return qinfo.deferred.promise;
    }

    return CObject.dbExec({
        self: this,
        execKey: '$unset'
    });
}

CObject.prototype.dbDelete = function(qinfo: any = null) {
    if (!qinfo) {
        qinfo = {
            noError: false
        };
    }

    return this.docDelete(qinfo)
        .then(() => {
            // return qinfo.doAudit ? CObject.runAudit(self, self, eido.AuditType.delete, false) : Promise.resolvedPromise(self);
            return this;
        });
}

CObject.dbBulkDelete = function(qinfo) {
    Debug.assert(qinfo && qinfo.query, 'Invalid query info passed for bulk deletion!');
    
    let conn = qinfo.dbConn || DB().getObjectConn(qinfo);
    qinfo.deferred = PromiseUtil.defer();
    qinfo.collection = conn.collection(qinfo.collectionName);

    return new Promise((resolve, reject) => {
        qinfo.collection.deleteMany(qinfo.query, (err) => {
            if (!err)
                return resolve();
            reject(err);
        })
    });
   
    // qinfo.collection.deleteMany(qinfo.query)
    //     .then(function() {
    //         this.deferred.resolve();
    //     }.bind(qinfo))
    //     .catch(function(err) {
    //         console.error('[DB] Database error:', err);
    //         return this.deferred.reject(eido.coreUtil.createError(500, err));
    //     });
    
    // return qinfo.deferred.promise;
}

CObject.dbBulkInsert = function(qinfo) {
    Debug.assert(qinfo, 'Invalid information passed for bulk insert.');
    let collectionName = qinfo.collectionName;

    Debug.assert(collectionName, 'Invalid collection name for object');

    let conn = qinfo.dbConn || DB().getObjectConn(qinfo);
    qinfo.collection = conn.collection(collectionName);

    qinfo.deferred = PromiseUtil.defer();

    // if a bulk index is given then we are just going to replace bulkValues with a modified index
    if (qinfo.bulkIndex) {
        let bulkValues = qinfo.bulkValues;
        qinfo.bulkValues = new Array(bulkValues.length);

        for (let i = 0; i < bulkValues.length; i++) {
            let bulkValue = bulkValues[i];
            qinfo.bulkValues[i] = qinfo.self.cleanClone(SecrecyLevel.db);

            if (CObject.isModelObject(bulkValue)) 
                qinfo.bulkValues[i][qinfo.bulkIndex] = bulkValue.cleanClone(SecrecyLevel.db);
            else    
                qinfo.bulkValues[i][qinfo.bulkIndex] = bulkValue;
        }
    }
    else {
        qinfo.bulkValues = qinfo.bulkValues.cleanClone(SecrecyLevel.db);
    }

    qinfo.collection.insert(qinfo.bulkValues, function(err) {
        if (err) {
            console.error('[DB] Database error:', err);
            return this.deferred.reject(eido.coreUtil.createError(500, err));
        }

        this.deferred.resolve(this.bulkValues);
    }.bind(qinfo));

    return qinfo.deferred.promise;
}

CObject.prototype.dbBulkInsert = function(qinfo: any) {
    Debug.assert(qinfo, 'Invalid information passed for bulk insert.');

    if (!qinfo.collectionName)
        qinfo.collectionName = this.getCollectionName(); 

        if (!qinfo.dbName)
        qinfo.dbName = this.getDBName();

    if (!qinfo.self)
        qinfo.self = this;

    return CObject.dbBulkInsert(qinfo);
}

CObject.dbInsert = function(qinfo) {
    if (!qinfo)
        qinfo = {};

    if (typeof qinfo.upsert === 'undefined')
        qinfo.upsert = false;

    return this.dbExec(qinfo);
}

CObject.prototype.dbInsert = function(qinfo: any = null) {
    if (!qinfo)
        qinfo = {};
        
    qinfo.self = this;
    
    return CObject.dbInsert(qinfo)
        .then(function(results) {
            // if (!qinfo.auditType)
            //     auditType = eido.AuditType.create;
            // return doAudit ? CObject.runAudit(self, results, auditType, true) : Promise.resolvedPromise(self);
            return this;
        }.bind(this));
}

CObject.dbUpdate = function(qinfo) {
    if (!qinfo)
        qinfo = {};

    if (typeof qinfo.upsert === 'undefined')
        qinfo.upsert = false;

    return this.dbExec(qinfo);
}

CObject.prototype.dbUpdate = function(qinfo: any = null) {
    if (!qinfo)
        qinfo = {};

    qinfo.self = this;
    
    return CObject.dbUpdate(qinfo)
        .then(function(results) {
            // if (!qinfo.auditType)
            //     auditType = eido.AuditType.create;
            // return doAudit ? CObject.runAudit(self, results, auditType, true) : Promise.resolvedPromise(self);
            return this;
        }.bind(this));
}

CObject.prototype.dbFindAndModify = function(qinfo?: any) {
    if (!qinfo)
        qinfo = {};

    let query = qinfo.query ? qinfo.query : {
        _id: qinfo._id || this["_id"] || this["_id"]
    };

    let update = {};
    for (let prop in qinfo) {
        if (prop.charAt(0) == '$')
            update[prop] = qinfo[prop];
    }

    let deferred = PromiseUtil.defer();
    let collection = this.getCollection(qinfo);

    collection.findAndModify({
        query: query,
        update: update,
        upsert: qinfo.upsert ? qinfo.upsert : false,
        new: qinfo.new === false || qinfo.remove === true ? false : true,
        remove: !!qinfo.remove
    })
        .then((object) => {
            if (qinfo.raw === true)
                return deferred.resolve(object);

            this.copyFrom(object, new CopyOptions(SecrecyLevel.db));

            return deferred.resolve(this);
        })
        .catch((err) => {
            console.error('[DB] Database error:', err);
            return deferred.reject(err);
        });

    return deferred.promise;
}

CObject.prototype.dbClone = function() {
}

// this will be a CObject.function = function ... call using CObject.send(...) - remove the CObject.keyword = function
CObject.send = function(args, input, status, level) {
    let data;
    
    if (!level)
        level = SecrecyLevel.public;

    if (require('util').isArray(input)) {
        let clones = new Array(input.length);
        for (let i = 0; i < input.length; i++) {
            let clone = input[i];

            if (CObject.isModelObject(input[i])) {
                clone = input[i].cleanClone({ level: SecrecyLevel.public });
            }

            clones[i] = clone;
        }

        // if (treatSingleResultsSpecially && clones.length == 1)
        //     data = clones[0];
        // else
        data = clones;
    }
    else {
        if (CObject.isModelObject(input))
            data = input.cleanClone();
        else
            data = input;
    }

    status = status || 200;

    args._res.header('Cache-Control', 'no-cache, private, no-store, must-revalidate, max-stale=0, post-check=0, pre-check=0');
    return args._res.status(status).send(data);
}

// send the current object to the client ... by default we just use the sendClient logic 
// unless we are being forced to send server data for some reason
CObject.prototype.send = function(args: ServerArgs, status: number, level: SecrecyLevel = SecrecyLevel.public) {
    return CObject.sendClient(args, status, this, CObject.__classId, level);
}

CObject.sendClientRaw = function(args: any, status: number, data: any) {
    (args._res as any).header('Cache-Control', 'no-cache, private, no-store, must-revalidate, max-stale=0, post-check=0, pre-check=0');
    (args._res as any).header('Content-Type', 'application/json');
    return args._res.status(status ? status : 200).send(data);
}

CObject.sendClient = function(args: any, status: number, input: CObject | Array<any>, clsId: string, level: SecrecyLevel = SecrecyLevel.public) {
    Debug.assert(!level || level < SecrecyLevel.server, `[CObject] Cannot use this level and above to send to client: ${level}`);
    Debug.assert(args, `Invalid server args while trying to send array!`);

    (args._res as any).header('Cache-Control', 'no-cache, private, no-store, must-revalidate, max-stale=0, post-check=0, pre-check=0');
    (args._res as any).header('Content-Type', 'application/json');
    
    let cls = Model.getClass(clsId);
    let clientClsId = null;
    let clone = null;
    
    Debug.assert(level < SecrecyLevel.server, 'Cannot send sensitive inforation to the client!');

    // if there is a client class available then we use that
    // otherwise we will just send our own clone
    // if (cls) {
    //     clone = input.toClass(cls, {
    //         level: level
    //     });
    // }
    // else { 
    clone = (input as any).cleanClone(level);
    // }
        
    // when we are sending data to the client we always include the original classId 
    // so that the client can make the appropriate conversion when it receives the 
    // data back on the client side and constructor the appropriate datastructures
    // if (clsId)
    //     clone.__classId = clsId;
        
    // send the data to the client ...
    return args._res.status(status ? status : 200).send(clone);
}

// send the client equivalent of this class
CObject.prototype.sendClient = function(args: any, status: number, level: SecrecyLevel = SecrecyLevel.public) {
    let cls = this.constructor as typeof CObject;
    let clsId = cls.__classId || "Object"; 
    return CObject.sendClient(args, status, this as CObject, cls.__classId, level);
}

CObject.prototype.addToSet = function(data) {
    // return this.dbUpdate({
    //     _id: this["_id"],
        
    //     $addToSet: data
    // }).then(() => {
    //     console.trace('[User] Successfully blocked:', args.userId);
    //     return this;
    // });
    return null;
}

CObject.prototype.removeFromSet = function(data) {
    return this.dbUpdate({
        _id: this["_id"],
        
        $pull: data
    }).then(() => {
        return this;
    });
}

CObject.prototype.getLevel = function(propName: string) {
    let props = this.getModelProperties().getAllProps();
    Debug.assert(props[propName], `[CObject] Unable to find prop: ${propName}`);
    return props[propName].level;
}

CObject.prototype.pushData = function(query: any, data: any, index: number, level: SecrecyLevel = SecrecyLevel.public) {
    let push = {};
    push[index] = data;
    
    return this.dbUpdate({
        query: query,
        $push: push,
        level: level
    });
}

CObject.prototype.dbUpdateFromDiff = function(qinfo?: any, current?: any, update: boolean = true) {
    let diff = CObject.getDiff(current, update, "", {});

    if (!qinfo)
        qinfo = {};

    qinfo["$set"] = diff;

    return this.dbUpdate(qinfo);
}

// CObject.invalidate = function(args) {
//     return this.validate(args, true);
// }

CObject.loadFromIds = function(ids, cls) {
    if (!cls) {
        Debug.assert(this.__classId, 'Invalid object with no classId!');
        cls = Model.getClass(this.__classId);
    }

    Debug.assert(cls, 'No class provided for loading!');
    
    for (let i = 0; i < ids.length; i++) {
        let id = ids[i];
        if (typeof id === 'string')
            ids[i] = ObjectID.create(id);
    }
    
    return this.dbLoad({
        query: {
            _id: {
                $in: ids
            }
        },
        collectionName: cls.collectionName,
        cls: cls
    });
}

// CObject.validate = function(args: any, invalidate: boolean, object: any, index: string | string[]) {
//     if (invalidate) {
//         args._error.assert.apply(args._error, [!object, 400, "Already exists:"].concat([].splice.call(arguments, 4)));
//         return null;
//     }

//     args._error.assert.apply(args._error, [object, 404, "Does not exist:"].concat([].splice.call(arguments, 4)));

//     if (index instanceof Array) {
//         let indexes = index;
//         for (let i = 0; i < indexes.length; i++) {
//             let index = indexes[i];
//             if (typeof args._cached[index] === 'undefined' || args._cached[index] === null) {
//                 args._cached[index] = object;
//                 break;
//             }
//         }
//     }
//     else
//         args._cached[index] = object;

//     return object;
// }

CObject.loadFromId = function(id: ObjectID, cls: any = null) {
    Debug.assert(id, 'Invalid id provided for loading!');
    
    if (!cls) {
        Debug.assert(this.__classId, 'Invalid object with no classId!');
        cls = Model.getClass(this.__classId);
    }
    
    Debug.assert(cls, 'No class provided for loading!');
    
    if (typeof id === 'string')
        id = ObjectID.create(id);

    return this.dbLoadOne({
        id: id,
        collectionName: cls.collectionName,
        cls: cls
    });
}