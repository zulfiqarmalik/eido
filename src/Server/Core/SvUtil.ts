// eido.require('@shared/core/sh_util.js');
var resolvedIpAddress = undefined;
import { eido } from 'Shared/Core/ShCore';
import { ObjectID } from 'Shared/Core/ObjectID';
import { Debug, PromiseUtil } from 'Shared/Core/ShUtil';
import { Position } from 'Shared/Model/Stats/Position';
import * as express from 'express';
import * as fs from 'fs';
import { ServerArgs } from './ServerArgs';
import { Error } from './Error';
import { Positions } from 'Shared/Model/Stats/Positions';
import WSRequest from './WS/WSRequest';
import { ServerConfig } from 'Server/App/ServerConfig';
import { AssetClass } from 'Shared/Model/Config/ShAlpha';
import { CDN } from 'Server/System/CDN/CDN';
let BinaryParser = require('binary-parser').Parser;

export class Util {
    static getRequestIPv4(req: express.Request | WSRequest): string {
        // our proxy HTTP server is going to handle this correctly
        // return  req.headers['x-real-ip'] ||
        //         req.headers['x-forwarded-for'] ||
        //         req.headers['X-Forwarded-For'];
        return "";
    }

    static getFullRequestIP(req: express.Request | WSRequest): string {
        // TODO: Need to make this work properly with proxied AWS setup
        // var v6 = require('ipv6').v6;
        var ip = Util.getRequestIPv4(req);
        return ip;
    }

    static getServerIPOfType(type: string): string {
        var os = require('os');

        var interfaces = os.networkInterfaces();
        //var addresses = [];

        for (var k in interfaces) {
            for (var k2 in interfaces[k]) {
                var address = interfaces[k][k2];
                if (address.family === type && !address.internal) {
                    //addresses.push(address.address);
                    return address.address;
                }
            }
        }

        //return addresses;

        return null;
    }

    static roundTimeQuarterHour(time: Date = null): Date {
        if (!time)
            time = new Date();

        let timeToReturn = new Date(time);

        timeToReturn.setMilliseconds(Math.floor(timeToReturn.getMinutes() / 1000) * 1000);
        timeToReturn.setSeconds(Math.floor(timeToReturn.getSeconds() / 60) * 60);
        timeToReturn.setMinutes(Math.floor(timeToReturn.getMinutes() / 15) * 15); // we want the nearest quarter below

        return timeToReturn;
    }

    static extractWSInfo(ws): any {
        if (!ws || !ws._socket)
            return ws;

        return {
            host: ws._socket._host,
            address: ws._socket.remoteAddress,
            port: ws._socket.remotePort,
            family: ws._socket.remoteFamily,
            destroyed: ws._socket.destroyed,
            readyState: ws._socket.readyState,
            bytesRead: ws._socket.bytesRead,
            bytesWritten: ws._socket.bytesDispatched,
            connecting: ws._socket._connecting,
            consuming: ws._socket.consuming,

            toString: function() {
                return this.host + ":" + this.port.toString() + '-(' + this.family + ")";
            }
        }
    }

    static getServerUrl(args) {
        // TODO: find a way to determine http/https
        if (!args || !args._req)
            return "";
        return "http://" + args._req.headers.host;
    }

    static getServerIPv4(): Promise<string> {
        if (typeof resolvedIpAddress != "undefined") {
            return PromiseUtil.resolvedPromise(resolvedIpAddress);
        }

        let defer = PromiseUtil.defer();
        // Since promises can only be resolved ONCE there's no need to put a verification here.
        let getRequestOptions = {
            hostname: '169.254.169.254',
            port: 80,
            path: '/latest/meta-data/local-ipv4',
            method: 'GET'
        };

        let resolveWithFixedIp = function() {
            var ip = Util.getServerIPOfType('IPv4');
            console.warn('[Util] Unable to get IP from AWS. Assuming non-AWS server.', ip);
            defer.resolve(ip);
            resolvedIpAddress = ip;
        }.bind(this);

        let request = require('http').request(getRequestOptions, function(res: any) {
            var ip = "";

            res.on('data', function(data) {
                ip += data;
            });

            res.on('end', function() {
                defer.resolve(ip);
                resolvedIpAddress = ip;
            });

            res.on('error', resolveWithFixedIp)
        });

        request.setTimeout(200, resolveWithFixedIp);

        request.on('error', resolveWithFixedIp);
        request.end();
        
        return defer.promise;
    }

    static getServerIPv6(): Promise<string> {
        // if (typeof resolvedIpAddress != "undefined") {
        //     return PromiseUtil.resolvedPromise(resolvedIpAddress);
        // }

        // var defer = PromiseUtil.defer();
        // var getRequestOptions = {
        //     hostname: '169.254.169.254',
        //     port: 80,
        //     path: '/latest/meta-data/local-ipv6',
        //     method: 'GET'
        // };

        // var request = require('http').request(getRequestOptions, function(res) {
        //     if (res.statusCode == 404) {
        //         return this.getRequestIPv4();
        //     }
        //     var ip = "";
        //     res.on('data', function(data) {
        //         ip += data;
        //     });
        //     res.on('end', function() {
        //         defer.resolve(Util.ipToString(ip));
        //         resolvedIpAddress = Util.ipToString(ip);
        //     }.bind(this));
        //     res.on('error', this.getRequestIPv4.bind(this));
        // }.bind(this));

        // request.setTimeout(200, this.getRequestIPv4.bind(this));

        // request.on('error', this.getRequestIPv4.bind(this));
        // request.end();

        // return defer.promise;

        return null;
    }

    static ipV4ToInt(ip: string): number {
        let parts = ip.split('.');
        Debug.assert(parts.length == 4, 'Invalid IP sent: ' + ip);

        let p0 = parseInt(parts[3]),
            p1 = parseInt(parts[2]),
            p2 = parseInt(parts[1]),
            p3 = parseInt(parts[0]);

        return p0 | (p1 << 8) | (p2 << 16) | (p3 << 24);
    }

    static ipToString(ip: number): string {
        let v6 = require('ipv6').v6;
        let address = new v6.Address(ip);

        Debug.assert(address.isValid(), 'Invalid IPv6 address passed: ' + ip);

        return address.decimal();
    }

    static semLoginError(args: ServerArgs, errorStr: string, response: express.Response, body: string) {
        var status = response.statusCode;

        if (!errorStr) {
            var data = JSON.parse(body);
            if (data.error) {
                if (data.error.code && data.error.user_message)
                    errorStr = data.error.code.toString() + ": " + data.error.user_message;
                else if (data.error.user_message)
                    errorStr = data.error.user_message;
                else
                    errorStr = data.error;
            }
        }

        return (new Error(status, errorStr, args._res)).logAndSend(response);
    }
    
    static getRequestUrl(req: express.Request): string {
        var parts = req.originalUrl.split('?');
        return !parts || !parts.length ? req.originalUrl : parts[0];
    }

    static parseCookies(request: express.Request): object {
        let list = {},
            rc = request.headers.cookie;

        let parts: string[] = [];
        if (typeof rc === 'string')
            parts = rc.split(';');
        else
            parts = rc;

        if (parts) {
            parts.map((cookie) => {
                let cparts = cookie.split('=');
                list[cparts.shift().trim()] = unescape(cparts.join('='));
            });
        }

        return list;
    }

    static padInt(num: number, size: number, c: string = '0') {
        let s = num + "";
        while (s.length < size)
            s = c + s;
        return s;
    }

    static defaultBuildName(): string {
        var now = new Date();
        return '' + // stringify autimatically
            now.getFullYear() +
            Util.padInt(now.getMonth() + 1, 2) +
            Util.padInt(now.getDate(), 2) +
            '_' +
            Util.padInt(now.getHours(), 2) +
            '-' +
            Util.padInt(now.getMinutes(), 2);
    }

    static runCmd(cmd: string, args: string[]) {
        var deferred = PromiseUtil.defer();

        var spawn = require('child_process').spawn;
        var child = spawn(cmd, args);
        var resp = "";

        child.stdout.on('data', function(buffer) {
            resp += buffer.toString();
        });

        child.stdout.on('end', function() {
            // callBack(resp);
            deferred.resolve(resp);
        });

        return deferred.promise;
    }

    static rmDir(dir: string) {
        var deferred = PromiseUtil.defer();
        var fs = require('fs.extra');

        fs.rmrf(dir, function(err) {
            if (err)
                deferred.reject(new Error(400, err));
            else
                deferred.resolve(dir);
        });

        return deferred.promise;
    }

    static mkdir(dir: string) {
        var deferred = PromiseUtil.defer();

        require('mkdirp')(dir, function(err) {
            if (err)
                deferred.reject(new Error(400, err));
            else
                deferred.resolve();
        });

        return deferred.promise;
    }

    static moveFile(src: string, dst: string) {
        var deferred = PromiseUtil.defer();
        
        require('fs').rename(src, dst, function(err) {
            if (err)
                return deferred.reject(err);
            return deferred.resolve(dst);
        });

        return deferred.promise;
    }

    static copyFile(src: string, dst: string, unsafe: boolean = false) {
        if (!unsafe) {
            if (!require('fs').existsSync(src))
                return PromiseUtil.resolvedPromise(true);
        }

        var deferred = PromiseUtil.defer();

        require('fs-extra').copy(src, dst, function(err) {
            if (err)
                return deferred.reject(err);
            return deferred.resolve(dst);
        });

        return deferred.promise;
    }

    static unzip(srcFilename: string, dstDir: string) {
        return Util.runCmd('unzip', ['-o', '-d', dstDir, srcFilename]);
    }

    static rmFile(filename: string) {
        var deferred = PromiseUtil.defer();
        var fs = require('fs.extra');

        fs.unlink(filename, function(err) {
            if (err)
                deferred.reject(new Error(400, err));
            else
                deferred.resolve(filename);
        });

        return deferred.promise;
    }

    static isType(o, t) {
        return o.__type && typeof o.__type == 'function' ? o.__type(t) : false;
    }

    static readFile(path, options) {
        return new Promise((resolve, reject) => {
            let fs = require('fs');
            fs.readFile(path, options, (err, contents) => {
                if (err)
                    return reject(err);
                resolve(contents);
            });
        });
    }

    static writeFile(path: string, contents: Buffer) {
        return new Promise((resolve, reject) => {
            let fs = require('fs');
            fs.writeFile(path, contents, (err) => {
                if (err)
                    return reject(err);
                resolve(contents);
            });
        });
    }

    static deleteFile(path: string, doThrow: boolean = false) {
        /// This will never throw any errors by default!
        let fs = require('fs');
        return new Promise((resolve, reject) => {
            fs.unlink(path, (err) => {
                if (err && doThrow)
                    return reject(err);
                resolve();
            });
        });
    }

    static uploadDirToAWS(dir: string, bucket: string, tickCallback) {
        /********************************************************************************************************************************************
        set env var AWS_ACCESS_KEY_ID=<your key>
        set env var AWS_SECRET_ACCESS_KEY=<your secret>
        on non-prod machines set env var (in one line) EC2_JVM_ARGS="-DproxySet=true -DproxyHost=eurprx01.eidos.com -DproxyPort=8080 -Dhttps.proxySet=true 
        -Dhttps.proxyHost=eurprx01.eidos.com -Dhttps.proxyPort=8080 -Dhttp.proxyUser=<windows username> -Dhttp.proxyPass=<windows password 
        -Dhttps.proxyUser=<windows username> -Dhttps.proxyPass=<windows passwods>" 
        ********************************************************************************************************************************************/
        var files = [];
        var path = require('path');
        var fs = require('fs.extra');
        var walk = require('walk');
        var AWS = require('aws-sdk');

        var deferred = PromiseUtil.defer();

        var s3 = new AWS.S3();

        function doBucketUpload() {

            if (this.error) {
                deferred.reject(this.error);
                return;
            }

            console.info('[AWS] Bucket opened successfully! Uploading ...');

            var srcRoot = dir;
            var dstRoot = ''; //'/' + bucket;
            var numUploading = 0;
            var numUploaded = 0;
            var numFailed = 0;
            var waitingQueue = [];
            var waitingQueueIndex = 0;
            var maxConcurrentUploads = 50;
            var totalBytesUploaded = 0;
            var totalBytesToUpload = 0;
            var totalFilesUploaded = 0;
            var totalFilesToUpload = 0;
            var uploadError = null;
            var uploadStartTime = new Date();

            function uploadFinished() {
                if (!deferred) {
                    console.warn('[AWS] UploadFinished called multiple times');
                    return;
                }
                if (uploadError) {
                    return deferred.reject(uploadError);
                }

                var uploadEndTime = new Date();
                var uploadTime = (uploadEndTime.getTime() - uploadStartTime.getTime()) * 0.001; // getTime returns milliseconds!
                console.info('[AWS] Total upload time: ' + uploadTime);

                console.info('[AWS] Upload finished for: ' + dir);
                deferred.resolve({
                    totalBytesUploaded: totalBytesUploaded,
                    totalBytesToUpload: totalBytesToUpload,
                    totalFilesUploaded: totalFilesUploaded,
                    totalFilesToUpload: totalFilesToUpload,
                    uploadStartTime: uploadStartTime,
                    uploadEndTime: uploadEndTime,
                    uploadPercent: 100.0,
                    dir: dir
                });

                deferred = null;
            }

            function handleUploadError(err) {
                if (!uploadError) {
                    if (Util.isType(err, Error))
                        uploadError = err;
                    else
                        uploadError = new Error(400, err);
                }
            }

            function doNextUpload() {
                if (waitingQueueIndex == waitingQueue.length || uploadError) {
                    if (!numUploading)
                        uploadFinished();

                    return false;
                }

                var uploadPercent = (totalBytesUploaded / totalBytesToUpload);
                var nextItem = waitingQueue[waitingQueueIndex];
                Debug.assert(nextItem.srcFilename && nextItem.dstFilename, 'Invalid queued item!');
                doUpload(nextItem.srcFilename, nextItem.dstFilename);
                waitingQueueIndex++;

                return true;
            }

            function doUpload(srcFilename, dstFilename) {
                // console.trace('[' + numUploading + ' / ' + waitingQueue.length - waitingQueueIndex + ']' + ' - Uploading: ' + srcFilename + ' -> ' + dstFilename);
                numUploading++;

                try {
                    fs.readFile(srcFilename, function(err, data) {
                        try {
                            totalFilesUploaded++;
                            // Raise the error but do not reject the promise. We need to finish files currently being uploaded so that
                            // they can be deleted successfully without any locking issues!
                            if (err) {
                                handleUploadError(err);
                            }

                            var fileLength = data.length;
                            s3.putObject({
                                Bucket: bucket,
                                Key: dstFilename,
                                Body: data
                            }, function(err, data) {

                                numUploading--;

                                if (err) {
                                    handleUploadError(err);
                                } else {
                                    try {
                                        totalBytesUploaded += fileLength;
                                        var uploadPercent = (totalBytesUploaded / totalBytesToUpload) * 100.0;
                                        console.trace('[AWS] [' + totalBytesUploaded + ' / ' + totalBytesToUpload + ' (' + uploadPercent.toFixed(2) + '%)]' + ' - Uploaded ' + dstFilename);

                                        // send tick information if a callback has been specified!
                                        if (tickCallback) {
                                            // the callback must throw an exception if an error is to be returned!
                                            tickCallback({
                                                totalBytesUploaded: totalBytesUploaded,
                                                totalBytesToUpload: totalBytesToUpload,
                                                totalFilesUploaded: totalFilesUploaded,
                                                totalFilesToUpload: totalFilesToUpload,
                                                uploadStartTime: uploadStartTime,
                                                uploadPercent: uploadPercent
                                            });
                                        }
                                    } catch (e) {
                                        handleUploadError(e);
                                    }
                                }

                                doNextUpload();
                            });
                        } catch (e) {
                            handleUploadError(e);
                            doNextUpload();
                        }
                    });
                } catch (e) {
                    handleUploadError(e);
                    doNextUpload();
                }
            }

            function queueUpload(srcFilename, dstFilename) {
                try {
                    Debug.assert(srcFilename && dstFilename, 'Trying to upload an invalid item!');

                    // see whether we have space to push more uploads ...
                    if (numUploading >= maxConcurrentUploads) {
                        // console.trace('Max concurrent uploads reached. Queuing: ' + srcFilename);
                        // if not then we simply push to a waiting queue and go back
                        waitingQueue.push({
                            srcFilename: srcFilename,
                            dstFilename: dstFilename
                        });
                        return;
                    }

                    // we can upload this file here
                    doUpload(srcFilename, dstFilename);
                } catch (e) {
                    handleUploadError(e);
                    doNextUpload();
                }
            }

            var walker = walk.walk(srcRoot, {
                followLinks: false
            });

            walker.on('file', function(root, stat, next) {
                // Add this file to the list of files
                var srcFilename = root + '/' + stat.name;
                var name = srcFilename.slice(srcRoot.length + 1); // +1 is for removing the leadeng /
                var dstFilename = dstRoot + name;
                totalBytesToUpload += stat.size;
                //files.push(srcRoot + '/' + stat.name);
                queueUpload(srcFilename, dstFilename);

                totalFilesToUpload++;

                next();
            });

            walker.on('end', function() {});
        }

        doBucketUpload();

        return deferred.promise;
    }

    static round(num, decimals) {
        return Math.round(num * Math.pow(10, decimals)) / Math.pow(10, decimals);
    }
    
    // static getSequencedObjectId(name: string) {
    //     return Util.getNextSequence(name)
    //         .then(function(id) {
    //             return new eido.ObjectID(0, 0, 0, id);
    //         });
    // }
    
    // static getNextSequence(name: string, collectionName: string) {
    //     var db = eido.db.getObjectConn();
    //     var collection = db.collection(collectionName || 'unique_counters');
    //     var deferred = PromiseUtil.defer();

    //     collection.findAndModify({
    //         query: {
    //             _id: name
    //         },
    //         update: {
    //             $inc: {
    //                 seq: 1
    //             }
    //         },
    //         new: true,
    //         upsert: true
    //     }, function(err, doc) {
    //         if (err) {
    //             return deferred.reject('[DB] Error!', err);
    //         }

    //         return deferred.resolve(doc.seq);
    //     });

    //     return deferred.promise;
    // }

    static rand(min: number, max: number) {
        return Math.floor(Math.random() * max) + min;
    }

    static getHomeDir(): string {
        let os = require('os');
        return os.homedir();
    }

    static getOSName(): string {
        let os = require('os');
        let type = os.type();

        if (type == "Linux")
            return "linux";
        else if (type == "Darwin")
            return "macos";
        return "windows";
    }

    static getVarValue(obj) {
        let os = require('os');
        let type = os.type();
        let value;

        if (typeof obj === 'string')
            value = obj;
        else {
            if (type == "Linux")
                value = obj.linux;
            else if (type == "Darwin")
                value = obj.macos;
            else
                value = obj.windows;
        }

        if (typeof value === 'undefined')
            return "";

        let homeDir = os.homedir();
        return value.replace("{$HOME}", homeDir);
    }

    static searchFiles(searchString: string, fullName: string = null, pathSep: string = null, noRev: boolean = false) {
        return new Promise((resolve, reject) => {
            let glob = require('glob');

            glob(searchString, {}, function (err, files) {
                if (err)
                    return reject(err);

                let path = require('path');
                let rfiles = new Array(files.length);

                if (!fullName) {
                    files.map((file, i) => {
                        let dstIndex = !noRev ? files.length - i - 1 : i;
                        rfiles[dstIndex] = path.basename(files[i]).split(".")[0];
                    });
                }
                else {
                    files.map((file, i) => {
                        let dstIndex = !noRev ? files.length - i - 1 : i;
                        rfiles[dstIndex] = !pathSep ? files[i] : files[i].replace(/\/|\\/g, pathSep);
                    });
                }

                resolve(rfiles);
            });
        });
    }

    static getPerStockStats(id: string) {
        console.log("[Strat] Searching for per stock stats: %s", id);
        return Util.searchFiles(id);
    }

    static getPositionNames(id: string, fullName: string) {
        console.log("[Strat] Searching for positions: %s", id);
        return Util.searchFiles(id, fullName);
    }

    static loadPositionsFromFile<T extends Positions>(filename: string, PositionClass: new () => T): T {
        let fs = require('fs');
        let data = fs.readFileSync(filename, "utf8");
        let positions = new PositionClass();

        positions.loadString(data);

        return positions;
    }

    static loadFileAsync(filename, callback: (filename: string) => void = null) {
        let fs = require('fs');

        return new Promise((resolve, reject) => {
            fs.readFile(filename, 'utf8', (err, data: string) => {
                if (callback)
                    callback(filename);
                    
                if (err)
                    return reject(err);

                resolve(data);
            });
        });
    }

    static loadPositionsFromFileAsync<T extends Position>(filename: string, PositionClass: new () => T, callback: (filename: string) => void = null) {
        return Util.loadFileAsync(filename)
            .then((data: string) => {
                let positions = new PositionClass();
                positions.loadString(data);
                if (callback)
                    callback(filename);
                return positions;
            });
    }

    static loadBinaryFloatArray(buffer, length) {
        if (!length)
            length = 0;

        let parser = new BinaryParser()
            .array('data', {
                type: 'floatle',
                length: length
            });

        let result = parser.parse(buffer);
        return result.data;
    }

    static splitMongoId(id) {
        return id.toString().split(/(.{2})/).filter(O=>O);
    }

    static checkFileExists(filename: string): Promise<boolean> {
        return new Promise((resolve, reject) => {
            let fs = require('fs');
            fs.exists(filename, (exists: boolean) => {
                resolve(true);
            });
        });
    }

    static getLocalCacheFilename(blobCacheDir: string, containerId: string, iid: string | ObjectID, suffix: string = ""): string {
        let path = require('path');
        let dirId = Util.splitMongoId(iid);
        let baseDir = path.join(blobCacheDir, containerId, path.join.apply(null, dirId))
        return path.join(baseDir, `${iid}${suffix}`);
    }

    static getIdFilename(sconfig: ServerConfig, containerId: string, iid: string | ObjectID, suffix: string = ""): string {
        let path = require('path');
        let blobCacheDir = sconfig.blobCacheDir;
        let dirId = Util.splitMongoId(iid);
        let baseDir = path.join(blobCacheDir, containerId, path.join.apply(null, dirId))
        return path.join(baseDir, `${iid}${suffix}`);
    }

    static async getFileBuffer(filename: string): Promise<Buffer> {
        console.log(`[Util] Serving: ${filename}`);

        return new Promise<Buffer>((resolve, reject) => {
            require('fs').readFile(filename, (err, contents: Buffer) => !err ? resolve(contents) : reject(err));
        });
    }

    static async getIdFileData(containerId: string, iid: string | ObjectID, suffix: string = "", forceLocal: boolean = false): Promise<Buffer> {
        let cdn: CDN = eido.appServer.cdn as CDN;
        Debug.assert(cdn, `No CDN setup!`);
        return cdn.pull(`${containerId}.${iid}`, {
            suffix: suffix,
            forceLocal: forceLocal
        });
    }

    static async sendThumbnail(args: ServerArgs, thmId: string | ObjectID, assetClass: AssetClass) {
        return args._cdn.pull(`thm.${thmId}`, null)
            .then((buffer: Buffer) => {
                (args._res as any).contentType("image/png");
                (args._res as any).end(buffer);
            });
    }

    static async createDir(dir: string): Promise<any> {
        return new Promise((resolve, reject) => {
            // let mkdirp = require('mkdirp');
            // mkdirp(dir, (err) => !err ? resolve() : reject(err));
            let fsExtra = require('fs-extra');
            fsExtra.ensureDir(dir, (err) => !err ? resolve() : reject(err));
        });
    }

    static mkDirByPathSync(targetDir: string, { isRelativeToScript = false } = {}) {
        let path = require('path');
        const sep = path.sep;
        const initDir = path.isAbsolute(targetDir) ? sep : '';
        const baseDir = isRelativeToScript ? __dirname : '.';

        return targetDir.split(sep).reduce((parentDir, childDir) => {
            const curDir = path.resolve(baseDir, parentDir, childDir);
            try {
                fs.mkdirSync(curDir);
            } 
            catch (err) {
                if (err.code === 'EEXIST') { // curDir already exists!
                    return curDir;
                }

                // To avoid `EISDIR` error on Mac and `EACCES`-->`ENOENT` and `EPERM` on Windows.
                if (err.code === 'ENOENT') { // Throw the original parentDir error on curDir `ENOENT` failure.
                    throw `EACCES: permission denied, mkdir '${parentDir}'`;
                }

                const caughtErr = ['EACCES', 'EPERM', 'EISDIR'].indexOf(err.code) > -1;
                if (!caughtErr || caughtErr && curDir === path.resolve(targetDir)) {
                    throw err; // Throw if it's just the last created dir.
                }
            }

            return curDir;
        }, initDir);
    }    
};

// Need to wrap the response object because of newrelic bug :/ 
export class WebResponse {
    nativeRequest       : any = null;
    nativeResponse      : any = null;
    headers             : object = null;
    serverId            : string = "";

    constructor(nativeRequest, nativeResponse) {
        Debug.assert(nativeResponse, 'Invalid response object given!');

        this.nativeRequest = nativeRequest;
        this.nativeResponse = nativeResponse;
        this.headers = nativeResponse.headers;
        this.serverId = eido.appServer.serverId();

        // if (eido.Stats)
        //     eido.Stats.beginRequest(nativeRequest, this);
    }

    status(status) {
        this.nativeResponse.status(status);
        return this;
    }

    send(data) {
        // send the response and the log it ...
        this.nativeResponse.send(data);

        // only report success
        // if (eido.Stats && (this.nativeResponse.statusCode >= 200 && this.nativeResponse.statusCode < 300))
        //     eido.Stats.endRequest(this);

        return this;
    }

    cookie() {
        this.nativeResponse.cookie.apply(this.nativeResponse, arguments);
    }

    header() {
        this.nativeResponse.header.apply(this.nativeResponse, arguments);
    }
}
