export default function parseCmdArgs() {
    let cmdArgs = require('node-getopt').create([
            ['c', 'consolelog',     'Forces console logging'],
            ['C', 'config=ARG',     'The configuration file (defaults to config.json)'],
            ['b', 'db=ARG',         'The database to use'],
            ['D', 'cdntype=ARG',    'CDN Type: local and remote are the only options'],
            ['d', 'cdnpath=ARG',    'CDN base path'],
            ['f', 'firstExit',      '(optional with -t) Exit on the first test failure'],
            ['g', 'geolevel',       'The level till which we will split the sectors'],
            ['h', 'host=ARG',       '(optional with -t) The host:port to connect to when running tests'],
            ['j', 'threads=ARG',    'The number of threads to run'],
            ['l', 'logserver=ARG',  'Path to the log file. Also set using GRAY_LOG environment url'],
            ['L', 'loglevel=ARG',   'The desired log level. Default is trace'],
            ['n', 'noClientTest',   '(optional with -t) Do not run client side tests'],
            ['N', 'noServerTest',   '(optional with -t) Do not run server side tests'],
            ['o', 'only=ARG',       '(optional with -t) Only run a particluar test suite'],
            ['P', 'port=ARG',       'The port on which to run the server'],
            ['p', 'password=ARG',   '(required with -t) The password of the user against which the tests will be performed'],
            ['s', 'simple',         'Run simple server without any web sockets'],
            ['S', 'secure',         'Run a secure server'],
            ['t', 'test',           'Run unit-tests on the server'],
            ['T', 'testmode',       'Run the server in test mode which opens up test login and other bits that are only for testing'],
            ['u', 'username=ARG',   '(required with -t) The username of the user against which the tests will be performed'],
            ['U', 'uuid=ARG',       '(optional with -T) The username of the user against which the tests will be performed'],
            ['v', 'verbose',        'Verbose including server log'],
            ['w', 'embedWSRouter',  'Embed WSRouter server within the app server'],
            ['W', 'noWSRouter',     'No WS router requirement'],
            ['X', 'noexit',         'Do not exit after running the tests. This helps generate test data on which the UI can then be tested'],
        ])
        .bindHelp() // bind option 'help' to default action
        .parseSystem() // do the parsing
        .options;

    if (cmdArgs.config == null || cmdArgs.config == undefined)
        cmdArgs.config = "./serverConfig.json";

    return cmdArgs;
};
