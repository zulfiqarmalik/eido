import { Debug } from "Shared/Core/ShUtil"; 

export abstract class CDN {
    blobCacheDir        : string;

    constructor(settings: any) {
        this.blobCacheDir       = process.env[settings.blobCacheDir];
        console.log(`[AzureCDN] Initialising blob cache dir: ${this.blobCacheDir}`);
    }

    abstract async push(uuid: string, srcFilename: string, options: any): Promise<boolean>;
    abstract async pull(uuid: string, options: any): Promise<Buffer>;
    abstract localFilename(uuid: string, suffix: string): string;

    static resolveVar(value): string {
        if (value.indexOf("{$") >= 0) {
            value.replace("{$", "").replace("}");
            return 
        }
        return value;
    }
}

// export default function initCDN(cdnSettings: any): CDN {
//     if (!cdnSettings) {
//         console.log("[CDN] No CDN specified!");
//         return null;
//     }

//     let cdn: CDN = null;
//     let type: string = cdnSettings.type;
//     if (type) {
//         console.info(`[CDN] Initialising CDN: ${type}`);

//         if (type == "Azure") 
//             cdn = new AzureCDN(cdnSettings);
//         else {
//             Debug.assert(false, `[CDN] Unsupported CDN: ${type}`);
//         }
//     }

//     return cdn;
// };
