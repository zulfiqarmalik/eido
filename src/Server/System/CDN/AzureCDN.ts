import { CDN } from "./CDN";
import * as azure from 'azure-storage';
import * as path from 'path';
import * as fs from 'fs';
import { Debug, PromiseUtil } from "Shared/Core/ShUtil";
import { Util } from 'Server/Core/SvUtil';

export class AzureCDN extends CDN {
    hotAccount          : string;
    hotKey              : string;
    coldAccount         : string;
    coldKey             : string;

    blobKeep            : boolean;

    bbsHot              : azure.BlobService;
    bbsCold             : azure.BlobService;

    constructor(settings: any) {
        super(settings);

        this.hotAccount         = process.env[settings.hotAccount];
        this.hotKey             = process.env[settings.hotKey];
        this.coldAccount        = process.env[settings.coldAccount];
        this.coldKey            = process.env[settings.coldKey];

        this.bbsHot             = azure.createBlobService(this.hotAccount, this.hotKey);
        this.bbsCold            = azure.createBlobService(this.coldAccount, this.coldKey);

        console.log(`[AzureCDN] Initialised!`);
    }

    static splitUuid(uuid: string) {
        let parts = uuid.split('.');
        Debug.assert(parts.length == 2, `Invalid uuid: ${uuid}. This must be of the format <containerId>.<blobId>`);
        return {
            containerId: parts[0],
            iid: parts[1]
        };
    }

    async push(uuid: string, srcFilename: string, options: any): Promise<boolean> {
        return null;
    }

    localFilename(uuid: string, suffix: string): string {
        let uuidInfo = AzureCDN.splitUuid(uuid);
        
        let dirId = Util.splitMongoId(uuidInfo.iid);
        let baseDir = path.join(this.blobCacheDir, uuidInfo.containerId, path.join.apply(null, dirId))
        return path.join(baseDir, `${uuidInfo.iid}${suffix}`);

    }

    private async downloadFileNow(bbs: azure.BlobService, containerId: string, iid: string, filename: string): Promise<string> {
        return new Promise<string>(async (resolve, reject) => {
            console.log(`[AzureCDN] Downloading: ${containerId}.${iid} => ${filename}`)
            bbs.getBlobToLocalFile(containerId, iid, filename, (error: Error, result: azure.BlobService.BlobResult, response: azure.ServiceResponse) => {
                if (error) {
                    console.error(`[AzureCDN] Error while downloading blob: ${containerId}.${iid} => ${filename}: ${error.name} - ${error.message}`);
                    reject(error);
                    return;
                }

                resolve(filename);
            });
        });
    }

    private async shouldDownload(bbs: azure.BlobService, containerId: string, iid: string, filename: string): Promise<boolean> {
        return new Promise<boolean>((resolve, reject) => {
            let exists: boolean = fs.existsSync(filename);

            /// If it doesn't exist then we need to download anyway ...
            if (!exists)
                return resolve(true);

            let stat = fs.statSync(filename);

            /// If the file is busted then we still download
            if (!stat || !stat.size)
                return resolve(true);

            let timestamp: Date = stat.mtime;
            let size: number = stat.size;
        
                /// Otherwise we get the properties of the blob
            bbs.getBlobProperties(containerId, iid, (error: Error, result: azure.BlobService.BlobResult) => {
                /// If an error has occured then we try to download it 
                /// TODO: Handle different error types so that we can actually reject here ...
                if (error)
                    return resolve(true);

                /// If the file size isn't the same, or the modified date is later on, then we need to download
                let modifiedDate: Date = new Date(result.lastModified);
                if (parseInt(result.contentLength) != size && modifiedDate > timestamp)
                    return resolve(true);

                /// We don't need to download this anymore ...
                resolve(false);
            });
        });

    }
    
    async downloadFile(bbs: azure.BlobService, containerId: string, filename: string): Promise<string> {
        let iid = path.basename(filename);

        /// First of all we make sure that 
        // await Util.createDir(path.dirname(filename));
        Util.mkDirByPathSync(path.dirname(filename));
        let shouldDownload = await this.shouldDownload(bbs, containerId, iid, filename);

        if (!shouldDownload)
            return PromiseUtil.resolvedPromise(filename);

        return this.downloadFileNow(bbs, containerId, iid, filename);
    }

    async pull(uuid: string, options: any): Promise<Buffer> {
        let uuidInfo = AzureCDN.splitUuid(uuid);

        let filename: string = Util.getLocalCacheFilename(this.blobCacheDir, uuidInfo.containerId, uuidInfo.iid, options && options.suffix ? options.suffix : "");

        /// If force local is set, then just get the local file ...
        if (options && options.forceLocal)
            return Util.getFileBuffer(filename);
            
        let downloadedFilename = await this.downloadFile(this.bbsHot, uuidInfo.containerId, filename);
        return Util.getFileBuffer(downloadedFilename);
        // return new Promise<Buffer>(async (resolve, reject) => {
        //     let filename: string = Util.getLocalCacheFilename(this.blobCacheDir, uuidInfo.containerId, uuidInfo.iid, options && options.suffix ? options.suffix : "");
        //     let downloadFilename = await this.downloadFile(this.bbsHot, uuidInfo.containerId, filename);

        // })
        // /// Otherwise, we shall download the file
        // return this.downloadFile(this.bbsHot, uuidInfo.containerId, filename)
        // /// and then send the entire file ...
        //     .then((filename: string) => Util.getFileBuffer(filename));
    }
}

