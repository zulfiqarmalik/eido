export default class LocalCDN extends CDN {
	constructor(app, options) {
		super(app, options);
		this.localPath = app.cmdArgs.cdnpath;

		eido.Debug.assert(this.localPath, 'Invalid local CDN path specified.');

		// make the content available as static content
		var path = '[apiRoot][apiVersion]/cdn';
		path = app.fixRestPath(path);
		console.log("[CDN] CDN Serving At Path:", path);
		app.expressApp.use(path, require('express').static(this.localPath));

		if (app.needsCleanState()) {
			console.info('[cdn] Cleaning local cdn directory ...');
			var fs = require('fs-extra');
			fs.removeSync(this.localPath);

			console.info('[cdn] Making sure cdn directory exists ...');
			fs.mkdirsSync(this.localPath);

			console.info('[cdn] Cleanup finished!');
		}
	}

	upload(src, dst, options) {
		eido.Debug.assert(src && dst, "Must specify valid source and destination files.");

		if (!options) {
			options = {
				copy: true
			};
		}

		options.copy = !!options.copy;

		dst = this.path(dst);

		if (options.copy)
			return eido.Util.copyFile(src, dst);

		return eido.Util.moveFile(src, dst)
	}

	path(path, options) {
		return require('path').join(this.localPath, path);
	}

    resolve(path) {
    	// cdn is supposed to be flat!
    	if (path.indexOf('/') >= 0 || path.indexOf('\\') >= 0)
    		return path;

    	return eido.appServer.getUrl() + 'cdn/' + path;
    }
}

module.exports = function(app) {
	new eido.LocalCDN(app);
}

