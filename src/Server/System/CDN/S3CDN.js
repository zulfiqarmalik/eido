/********************************************************************************************************************************************
set env var AWS_ACCESS_KEY_ID=<your key>
set env var AWS_SECRET_ACCESS_KEY=<your secret>
on non-prod machines set env var (in one line) EC2_JVM_ARGS="-DproxySet=true -DproxyHost=eurprx01.eidos.com -DproxyPort=8080 -Dhttps.proxySet=true 
-Dhttps.proxyHost=eurprx01.eidos.com -Dhttps.proxyPort=8080 -Dhttp.proxyUser=<windows username> -Dhttp.proxyPass=<windows password 
-Dhttps.proxyUser=<windows username> -Dhttps.proxyPass=<windows passwods>" 
********************************************************************************************************************************************/

isinr.S3CDN = isinr.Class({
	Extends: isinr.CDN,

	initialize: function(app, options) {
		this._super(app, options);
		this.path = app.cmdArgs.cdnpath;
	},

	getType: function() {
		return isinr.cdnType.s3;
	},

	upload: function(src, dst, options) {
        if (!options)
            options = {};

        var deferred = isinr.defer();
        var bucket = options.bucket || isinr.appServer.getBucketName();

        isinr.Debug.assert(bucket, 'Invalid bucket to upload to!');

        require('fs-extra').readFile(src, function(err, data) {
            if (err)
                return deferred.reject(err);

            var AWS = require('aws-sdk');
            var s3 = new AWS.S3();
            var fileLength = data.length;

            s3.putObject({
                Bucket: bucket,
                Key: dst,
                Body: data
            }, function(err, data) {
                if (err)
                    return deferred.reject(err);

                deferred.resolve();
            });
        });

        return deferred.promise;
	},

	uploadDir: function(dir, options) {
        var files 			= [];
        var path 			= require('path');
        var fs 				= require('fs-extra');
        var walk 			= require('walk');
        var AWS 			= require('aws-sdk');
        var bucket 			= options.bucket || isinr.appServer.getBucketName();
        var tickCallback    = options.tickCallback;
        var deferred 		= isinr.defer();
        var args            = options.args;

        var s3 				= new AWS.S3();

        isinr.Debug.assert(bucket, 'Invalid bucket to upload to!');

        function doBucketUpload() {

            if (this.error) {
                deferred.reject(this.error);
                return;
            }

            console.info('[AWS] Bucket opened successfully! Uploading ...');

            var srcRoot = dir;
            var dstRoot = args.appName + '/'  + args.componentName + '/' + args.buildName;
            // var dstRoot = ''; //'/' + bucket;
            var numUploading = 0;
            var numUploaded = 0;
            var numFailed = 0;
            var waitingQueue = [];
            var waitingQueueIndex = 0;
            var maxConcurrentUploads = 50;
            var totalBytesUploaded = 0;
            var totalBytesToUpload = 0;
            var totalFilesUploaded = 0;
            var totalFilesToUpload = 0;
            var uploadError = null;
            var uploadStartTime = new Date();

            function uploadFinished() {
                if (!deferred) {
                    console.warn('[AWS] UploadFinished called multiple times');
                    return;
                }
                if (uploadError) {
                    return deferred.reject(uploadError);
                }

                var uploadEndTime = new Date();
                var uploadTime = (uploadEndTime.getTime() - uploadStartTime.getTime()) * 0.001; // getTime returns milliseconds!
                console.info('[AWS] Total upload time: ' + uploadTime);

                console.info('[AWS] Upload finished for: ' + dir);
                deferred.resolve({
                    totalBytesUploaded: totalBytesUploaded,
                    totalBytesToUpload: totalBytesToUpload,
                    totalFilesUploaded: totalFilesUploaded,
                    totalFilesToUpload: totalFilesToUpload,
                    uploadStartTime: uploadStartTime,
                    uploadEndTime: uploadEndTime,
                    uploadPercent: 100.0,
                    dir: dir
                });

                deferred = null;
            }

            function handleUploadError(err) {
                if (!uploadError) {
                    if (isinr.Util.isType(err, isinr.Error))
                        uploadError = err;
                    else
                        uploadError = new isinr.Error(400, err);
                }
            }

            function doNextUpload() {
                if (waitingQueueIndex == waitingQueue.length || uploadError) {
                    if (!numUploading)
                        uploadFinished();

                    return false;
                }

                var uploadPercent = (totalBytesUploaded / totalBytesToUpload);
                var nextItem = waitingQueue[waitingQueueIndex];
                isinr.Debug.assert(nextItem.srcFilename && nextItem.dstFilename, 'Invalid queued item!');
                doUpload(nextItem.srcFilename, nextItem.dstFilename);
                waitingQueueIndex++;

                return true;
            }

            function doUpload(srcFilename, dstFilename) {
                // console.trace('[' + numUploading + ' / ' + waitingQueue.length - waitingQueueIndex + ']' + ' - Uploading: ' + srcFilename + ' -> ' + dstFilename);
                numUploading++;

                try {
                    fs.readFile(srcFilename, function(err, data) {
                        try {
                            totalFilesUploaded++;
                            // Raise the error but do not reject the promise. We need to finish files currently being uploaded so that
                            // they can be deleted successfully without any locking issues!
                            if (err) {
                                handleUploadError(err);
                            }

                            var fileLength = data.length;
                            s3.putObject({
                                Bucket: bucket,
                                Key: dstFilename,
                                Body: data
                            }, function(err, data) {

                                numUploading--;

                                if (err) {
                                    handleUploadError(err);
                                }
                                else {
                                    try {
                                        totalBytesUploaded += fileLength;
                                        var uploadPercent = (totalBytesUploaded / totalBytesToUpload) * 100.0;
                                        console.trace('[AWS] [' + totalBytesUploaded + ' / ' + totalBytesToUpload + ' (' + uploadPercent.toFixed(2) + '%)]' + ' - Uploaded ' + dstFilename);

                                        // send tick information if a callback has been specified!
                                        if (tickCallback) {
                                            // the callback must throw an exception if an error is to be returned!
                                            tickCallback({
                                                totalBytesUploaded: totalBytesUploaded,
                                                totalBytesToUpload: totalBytesToUpload,
                                                totalFilesUploaded: totalFilesUploaded,
                                                totalFilesToUpload: totalFilesToUpload,
                                                uploadStartTime: uploadStartTime,
                                                uploadPercent: uploadPercent
                                            });
                                        }
                                    }
                                    catch (e) {
                                        handleUploadError(e);
                                    }
                                }

                                doNextUpload();
                            });
                        }
                        catch (e) {
                            handleUploadError(e);
                            doNextUpload();
                        }
                    });
                }
                catch (e) {
                    handleUploadError(e);
                    doNextUpload();
                }
            }

            function queueUpload(srcFilename, dstFilename) {
                try {
                    isinr.Debug.assert(srcFilename && dstFilename, 'Trying to upload an invalid item!');

                    // see whether we have space to push more uploads ...
                    if (numUploading >= maxConcurrentUploads) {
                        // console.trace('Max concurrent uploads reached. Queuing: ' + srcFilename);
                        // if not then we simply push to a waiting queue and go back
                        waitingQueue.push({
                            srcFilename: srcFilename,
                            dstFilename: dstFilename
                        });
                        return;
                    }

                    // we can upload this file here
                    doUpload(srcFilename, dstFilename);
                }
                catch (e) {
                    handleUploadError(e);
                    doNextUpload();
                }
            }

            var walker = walk.walk(srcRoot, {
                followLinks: false
            });

            walker.on('file', function(root, stat, next) {
                // Add this file to the list of files
                var srcFilename = root + '/' + stat.name;
                var name = srcFilename.slice(srcRoot.length + 1); // +1 is for removing the leadeng /
                var dstFilename = dstRoot + name;
                totalBytesToUpload += stat.size;
                //files.push(srcRoot + '/' + stat.name);
                queueUpload(srcFilename, dstFilename);

                totalFilesToUpload++;

                next();
            });

            walker.on('end', function() {});
        }

        doBucketUpload();

        return deferred.promise;
	},

	path: function(path, options) {
		return require('path').join(this.path, path);
	}
});

module.exports = function(app) {
	new isinr.S3CDN(app);
}

