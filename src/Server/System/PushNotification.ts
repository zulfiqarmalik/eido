import { Notification } from 'Shared/Model/Notification';
import { eido } from 'Shared/Core/ShCore';
import { PushNotificationType } from 'Shared/Core/PushNotificationType';

////////////////////////////////////////////////////////////////////////////////////////////////////////////////

export default class PushNotification {
    constructor(userIds) {
    }
    
    send() {
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /// Static methods
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    static send(userIds, notification, excludeIds) {
        return eido.appServer.wsServer.sendData(userIds, notification, excludeIds);
    }

    // static sendPrivateChat(senderId, targetId, msg) {
    //     return PushNotification.send([targetId], {
    //         id: PushNotificationType.privateChat,
    //         msg: msg.cleanClone()
    //     }, senderId);
    // }
    
    // static sendEventChat(event, sender, targets, msg) {
    //     eido.Debug.assert(targets && targets.length, 'Invalid targets passed for sending event invite notification!');

    //     return PushNotification.send(targets, {
    //         id: eido.PushNotificationType.eventChat,
    //         event: event.minimalClone(),
    //         sender: sender.minimalClone(),
    //         msg: msg.cleanClone()
    //     }, sender._id);
    // }

    // static sendEventInvitationRequest(event, sender, targets) {
    //     eido.Debug.assert(targets && targets.length, 'Invalid targets passed for sending event invite notification!');

    //     return PushNotification.send(targets, {
    //         id: eido.PushNotificationType.eventInviteRequest,
    //         event: event.minimalClone(),
    //         sender: sender.minimalClone()
    //     });
    // }

    // static sendEventInvitationResponse(event, user, targets, response) {
    //     eido.Debug.assert(targets && targets.length, 'Invalid targets passed for sending event invite notification!');

    //     return PushNotification.send(targets, {
    //         id: eido.PushNotificationType.eventInviteResponse,
    //         event: event.minimalClone(),
    //         user: user.minimalClone(),
    //         response: response
    //     });
    // }

    // static sendEventInterested(event, user) {
    //     return PushNotification.send([event.creator._id], {
    //         id: eido.PushNotificationType.eventInterest,
    //         event: event.minimalClone(),
    //         user: user.minimalClone()
    //     });
    // }
}

