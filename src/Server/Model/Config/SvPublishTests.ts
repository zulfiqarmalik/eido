import { eido } from 'Shared/Core/ShCore';
import { Alpha } from './SvAlpha';
import { Strategy } from './SvStrategy';
import { ShPublishTests } from 'Shared/Model/Config/ShPublishTests';
import { Model } from 'Shared/Model/ShModel';
import { Util } from 'Server/Core/SvUtil';
import * as path from 'path';
import { ShUtil, Debug, PromiseUtil } from 'Shared/Core/ShUtil';
import { db } from 'Server/Core/DB/DBManager';
import { DB, Collections } from 'Server/Model/SvModel';
import { ServerArgs } from 'Server/Core/ServerArgs';
import { LiveStats } from 'Shared/Model/Config/BaseConfig';
import { LTStats } from 'Shared/Model/Stats/LTStats';
import { CumStats } from 'Shared/Model/Stats/CumStats';

let correlation = require('node-correlation');

export class PublishTests extends ShPublishTests {
    static get collectionName() { return Collections.sys; }

    _alpha              : Alpha = null;
    _subAlpha           : Alpha = null;
    _superAlpha         : Alpha = null;
    _pnlData            : LiveStats = null;

    constructor(...args) {
        super();
    }

    get dbName() {
        /// Always put in the 
        return DB.pesaUser;
    }

    write(args?: ServerArgs, userId?: string, name?: string, category?: string, market?: string, universeId?: string) {
        return this.dbUpdate({
            upsert: true
        }).then(() => this);
    }

    loadId(id) {
        return this.dbLoadOne({ 
            query: {
                _id: id
            }
        }).then(() => this);
    }

    run(alpha) {
        this._id = alpha._id;
        console.log(`[Config] Running post-publish tests on: ${this._id}`);
        this._alpha = alpha;

        return Promise.all([alpha.loadSubAlpha(true), alpha.loadSuperAlpha(true)])
            .then((alphas) => {
                this._subAlpha = alphas[0];
                this._superAlpha = alphas[1];

                return this.testCorrISOS();
            })
            .then((corrISOS: number) => {
                this.corrISOS = corrISOS;

                if (this._subAlpha)
                    return this.testCorrSub(this._subAlpha);
                return PromiseUtil.resolvedPromise(1.0);
            })
            .then((corrSub) => {
                this.corrSub = corrSub;

                if (this._superAlpha)
                    return this.testCorrSuper(this._superAlpha);
                return PromiseUtil.resolvedPromise(1.0);
            })
            .then((corrSuper) => {
                this.corrSuper = corrSuper;
                return this.write();
            });
    }

    testCorrSub(subAlpha) {
        return subAlpha.getStatsPnlStream()
            .then((subData) => {
                return PublishTests.testAgainstSample(this._pnlData, subData);
            });
    }

    testCorrSuper(superAlpha) {
        return superAlpha.getStatsPnlStream()
            .then((superData) => {
                return PublishTests.testAgainstSample(this._pnlData, superData);
            });
    }

    testCorrISOS(): Promise<{}> {
        /// Load the livePnl 
        return this._alpha.getStatsPnlStream()
            .then((pnlData: LiveStats) => {
                this._pnlData = pnlData;
                /// Then load the OS stats
                return this._alpha.loadStats(['OS']);
            })
            .then((statsOS: CumStats) => {
                Debug.assert(statsOS, `Unable to load the OS stats correctly for: ${this._id}`);
                let osDate = statsOS.startDate;

                let totalCount = this._pnlData.dates.length;
                let osIndex = this._pnlData.dates.indexOf(osDate);
                Debug.assert(osIndex >= 0 && osIndex < totalCount, `Unable to find OutSample date in the pnl data: ${this._id} - OS Date: ${osDate}`);

                let osCount = totalCount - osIndex;
                let corr1Weight = osCount / totalCount;
                let corr2Weight = Math.max(osIndex - osCount, 0) / totalCount;
                let pnls = this._pnlData.pnls;

                /// Now that we have it ... we calculate the correlation
                let corr1 = ShUtil.correlation(correlation.calc, pnls, pnls, 0, osCount, osIndex, osIndex + osCount);
                let corr2 = ShUtil.correlation(correlation.calc, pnls, pnls, osCount, osIndex, osIndex, osIndex + osCount);

                console.log(`[Alpha-Tests] IS/OS Correlations: ${corr1} [${corr1Weight} | ${corr2} [${corr2Weight}]`);

                this.corrISOS = (corr1 * corr1Weight + corr2 * corr2Weight) * 0.5;
                return this.corrISOS;
            });
    }

    static testAgainstSample(pnlData, derivedData) {
        // if (!startIndex)
        //     startIndex = 0;
        // if (!endIndex || endIndex <= startIndex)
        //     endIndex = samplePnl.pnls.length;

        return ShUtil.correlation(correlation.calc, pnlData.pnls, derivedData.pnls);
    }
};

