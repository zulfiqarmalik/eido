import { eido } from 'Shared/Core/ShCore';
import { ObjectID } from "Shared/Core/ObjectID";
import { ShAlpha, AlphaSubType, AlphaBucket, Universes, DerivedUniverses, getDefaultOp, ConfigType, AlphaType } from 'Shared/Model/Config/ShAlpha';
import { Util } from 'Server/Core/SvUtil';
import * as path from 'path';
import { Debug, PromiseUtil } from 'Shared/Core/ShUtil';
import { DB, Collections, gCache } from 'Server/Model/SvModel';
import { CObject } from 'Shared/Core/CObject';
import { db } from 'Server/Core/DB/DBManager';
import { AlphaOrStrategy, AlphaOrStrategyLoadOptions } from './SvConfigBase';
import { PushNotificationType } from 'Shared/Core/PushNotificationType'
import { PublishTests } from './SvPublishTests';
import { ServerArgs } from '../../Core/ServerArgs';
import { ConfigSection } from 'Shared/Model/Config/ConfigSection';
import { ModelObject, ModelValue, ModelClassId } from 'Shared/Core/BaseObject';
import { LTStats } from 'Shared/Model/Stats/LTStats';
import { CheckpointType } from 'Server/Services/Data/Checkpoint';
import { SortOrder } from 'Shared/Model/ShModel';
import { AlphaFeed } from 'Shared/Model/Config/AlphaFeed';

@ModelObject()
export class Alpha extends AlphaOrStrategy {
    @ModelClassId(Alpha, "Alpha") __dummy: string;

    private static _refConfig   : string = "{$APP}/Configs/Misc/minimal_ref_dev.xml";

    static get collectionName() { return Collections.alpha; }

    logSuffix               : string = "";
    _subAlpha               : SubAlpha = null;
    _superAlpha             : SuperAlpha = null;

    constructor() {
        super();
    }

    get dbName() {
        if (this.isPublished && this.market)
            return DB["pesa" + this.market];

        // if (this.origin == AlphaOrigin.auto)
        //     return DB.pesaAuto;

        return DB.pesaUser;
    }

    getRunStartNotificationId(): string { return PushNotificationType.alphaRun; }
    getRunFinishedNotificationId(): string { return PushNotificationType.alphaRun_Finished; }
    getRunStatusNotificationId(): string { return PushNotificationType.alphaRun_Status; }
    getCheckpointType(): CheckpointType { return CheckpointType.alpha; }

    getRunLogFilename(): string {
        /// Get the log filename from the path of the python file
        if (this.isPython()) {
            let sourcePath = this.getSourcePath();
            let sourceDir = path.dirname(sourcePath);
            return path.join(sourceDir, `${this.name}${this.logSuffix}.log`);
        }

        /// For expression alphas just use the owner's directory as the base path for the log filename
        let ownerDir = path.join(eido.appServer.config(this.getAssetClass()).alphaDir.path, this.market, this.ownerId.toString());
        return path.join(ownerDir, `${this.name}${this.logSuffix}.log`);
    }

    createExpression(args: ServerArgs, userId: ObjectID, name: string, category: string, market: string, universeId: string, delay: number) {
        console.log(`[Alpha] Creating expression alpha: ${name}`);
        super.create(userId, name, category, market, universeId, AlphaSubType.expr, AlphaType.alpha);

        this.market     = market;
        this.bucket     = AlphaBucket.PriceVolume;

        let assetClass  = this.getAssetClass();
        let sconfig     = args._appServer.config(assetClass);
        let aconfig     = sconfig.alpha;
        let idStr       = this._id.toString();

        this.subType    = AlphaSubType.expr;
        this.startDate  = aconfig.startDate;
        this.refConfig  = Alpha._refConfig;
        this.defs.map.dbName = DB.pesaUser;
        this.defs.map.endDatePercent = aconfig.defOutSamplePercent;
        
        this.setDelay(delay);
        this.portfolio.map.universeId = universeId;

        this.portfolio.children = [new ConfigSection(
            "Alpha", {
                type        : "Alpha",
                id          : "PyAlpha",
                moduleId    : "PyImpl",
                pyPath      : "PyExpr.py",
                pyClassName : "PyExpr",
                uuid        : idStr,
                iid         : idStr,
                expr        : aconfig.defaultExpr,
                universeId  : universeId,
                // creationDate: this.portfolio.map.creationDate
            },

            [getDefaultOp(assetClass)]
        )];

        this.addRequiredPortfolioComponents();
        
        console.log("[Alpha] Inserting alpha: %s", this._id.toString());
        return this.dbInsert({
            constants: {
                isMinimal: true,
                source: ""
            }
        });
    }

    addPosInfo() {
        let dstDir      = path.join(eido.appServer.config(this.getAssetClass()).alphaDir.path, this.ownerId.toString());
        this.portfolio.children.push(new ConfigSection(
            "Pos", {
                dir: dstDir
            }, []
        ));
    }

    addStatsInfo() {
        let dstDir      = path.join(eido.appServer.config(this.getAssetClass()).alphaDir.path, this.ownerId.toString());
        this.portfolio.children.push(new ConfigSection(
            "Stats", {
                fileDir: dstDir
            }, []
        ));
    }

    addRequiredPortfolioComponents() {
        this.addPosInfo();
        this.addStatsInfo();
    }

    createNew(args: ServerArgs, type: AlphaSubType, userId: ObjectID, name: string, category: string, market: string, universeId: string, delay: number) {
        if (type == AlphaSubType.expr)
            return this.createExpression(args, userId, name, category, market, universeId, delay);

        return this.createPython(args, userId, name, category, market, universeId, delay);
    }

    createPython(args: ServerArgs, userId: ObjectID, name: string, category: string, market: string, universeId: string, delay: number) {
        console.log(`[Alpha] Creating python alpha: ${name}`);
        super.create(userId, name, category, market, universeId, AlphaSubType.python, AlphaType.alpha);

        this.market     = market;
        this.bucket     = AlphaBucket.PriceVolume;

        let assetClass  = this.getAssetClass();
        let sconfig     = args._appServer.config(assetClass);
        let aconfig     = sconfig.alpha;
        let defSrc      = sconfig.resolvePath(aconfig.defaultSrc);
        let path        = require('path');
        let idStr       = this._id.toString();
        let dstDir      = path.join(sconfig.alphaDir.rawPath, this.market, this.ownerId.toString());
        let dstFilename = path.join(dstDir, this.name + ".py");

        this.setDelay(delay);

        console.log(`[Alpha] Reading default source from: ${defSrc}`);
        return Util.readFile(defSrc, 'utf8')
            .then((src: string) => {
                /// Ok we have the data now
                this.source         = src.replace(/PyRefAlpha/g, name);
                this.startDate      = aconfig.startDate;
                this.refConfig      = Alpha._refConfig;
                this.defs.map.dbName= DB.pesaUser;
                this.defs.map.endDatePercent = aconfig.defOutSamplePercent;

                this.portfolio.children = [new ConfigSection(
                    "Alpha", {
                        type        : AlphaType.alpha,
                        id          : "PyAlpha",
                        moduleId    : "PyImpl",
                        uuid        : idStr,
                        iid         : idStr,
                        pyPath      : dstFilename,///this.dbName() + "@" + idStr,
                        pyClassName : this.name,
                        universeId  : universeId,
                        // creationDate: this.portfolio.map.creationDate
                    }, [getDefaultOp(assetClass)]
                )];

                this.addRequiredPortfolioComponents();
                
                console.log("[Alpha] Inserting alpha: %s", this._id.toString());
                return this.dbInsert({
                    constants: {
                        isMinimal: true,
                        source: ""
                    }
                });
            })
            .then(() => {
                dstDir = sconfig.resolvePath(dstDir);
                return Util.mkdir(dstDir);
            })
            .then(() => { 
                return this.writeSource(this.source);
                // console.log("[Alpha] Writing alpha source in filename: %s", dstFilename);
                // return Util.writeFile(dstFilename, this.source);
            })
            .then(() => {
                return this;
            });
    }

    deleteSource() {
        if (this.isExpression())
            return PromiseUtil.resolvedPromise(this);

        let sourcePath = this.getSourcePath();
        return Util.deleteFile(sourcePath)
            .then(() => this);
    }

    writeSource(source) {
        let sourcePath = this.getSourcePath();
        let sourceBackup = sourcePath + "." + this.version;
        console.log(`[Alpha] Backing up source: ${sourcePath} => ${sourceBackup}`, );
        return Util.copyFile(sourcePath, sourceBackup)
            .then(() => {
                console.log(`[Alpha] Writing alpha source in filename: ${sourcePath}`);
                this.source = source;
                return Util.writeFile(sourcePath, source);
            });
    }

    updateExpression(args, updateObj) {
        // updateObj["portfolio.children.0.map.expr"] = args.source;
        return this.dbUpdate({
            $set: updateObj
        });
    }

    prepareUpdate(args) {
        this.version++;

        let updateObj = {
            modifiedDate: new Date(),
            version: this.version,
            lastError: "",
            "portfolio.map.universeId": args.universeId,
            "defs.map.market": args.market,
            "defs.map.bucket": args.bucket,
            "defs.map.startDate": args.startDate.toString(),
            // "defs.endDatePercent": args.endDatePercent.toString(),
            "portfolio.children.0.map.universeId": args.universeId,
        };

        /// If the startDate does not match the existing startDate that we have ... then we reintroduce the endDatePercent
        if (args.startDate != this.startDate) {
            let assetClass      = this.getAssetClass();
            let sconfig         = args._appServer.config(assetClass);
            let aconfig         = sconfig.alpha;
            let endDatePercent  = aconfig.defOutSamplePercent;

            console.log(`[Alpha] Start date changed from ${this.startDate} => ${args.startDate}. Resetting the endDatePercent to: ${endDatePercent}`)
            updateObj["defs.map.endDatePercent"] = endDatePercent.toString();
        }

        return updateObj;
    }

    updateSource(args: ServerArgs, source: string) {
        args.assert(source.length, 400, `Cannot have empty source code!`);

        if (this.isExpression()) {
            this.expr = source;

            return this.dbUpdate({
                $set: {
                    "portfolio.children.0.map.expr": source
                }
            });
        }
        
        return this.writeSource(source);
    }

    update(args: ServerArgs, source: string = "") {
        args.assert(!this.isPublished, 400, `Alpha ${this._id} has been published! Modification of published alphas is NOT allowed!`);

        let promise = null;
        let updateObj = this.prepareUpdate(args);

        if (source)
            promise = this.updateSource(args, source);
        else
            promise = PromiseUtil.resolvedPromise(true);

        return promise.then(() => {
            return this.dbUpdate({
                $set: updateObj
            });
        })

        // if (this.isExpression())
        //     return this.updateExpression(args, updateObj);

        // let promise = PromiseUtil.resolvedPromise(this);

        // if (args.source) {
        //     promise = this.writeSource(args.source)
        // }

        // return promise.then(() => {
        //     // this.source = "";
        //     // updateObj.source = args.source;

        //     return this.dbUpdate({
        //         $set: updateObj
        //     });
        // })
        // .then(() => { 
        //     this.source = args.source;
        //     return this;
        // });
    }

    runSubAlpha(args) {
        if (this._subAlpha) {
            console.info(`[Alpha] Running sub-alpha: ${this.dbName}@${this._id}`);
            return this._subAlpha.run(args, true);
        }

        console.info(`[Alpha] No sub-alpha for: ${this._id}`);
        return PromiseUtil.resolvedPromise(this);
    }

    runSuperAlpha(args) {
        if (this._superAlpha) {
            console.info(`[Alpha] Running super-alpha: ${this.dbName}@${this._id}`);
            return this._superAlpha.run(args, true);
        }

        console.info(`[Alpha] No super-alpha for: ${this._id}`);
        return PromiseUtil.resolvedPromise(this);
    }

    prepareForPublish(args: ServerArgs) {
        console.log(`[Alpha] Preparing to publish ...`);
        let universeId = this.getUniverseId();
        let derivedUniverses = DerivedUniverses[this.market][universeId];
        let subUniverse = derivedUniverses ? derivedUniverses[0] : null;
        let superUniverse = derivedUniverses ? derivedUniverses[1] : null;

        this._subAlpha = null;
        this._superAlpha = null;

        let promises = [];

        /// First of all we prepare the sub and super alphas ...
        if (subUniverse) {
            console.log(`[Alpha] Found sub-universe: ${universeId} => ${subUniverse}`);

            this._subAlpha = new SubAlpha();
            promises.push(this._subAlpha.load(this, subUniverse));

            /// Since the ID of the sub-alpha will be the same as ours
            this.subAlphaId = this._id;
        }

        if (superUniverse) {
            console.log(`[Alpha] Found super-universe: ${universeId} => ${superUniverse}`);

            this._superAlpha = new SuperAlpha();
            promises.push(this._superAlpha.load(this, superUniverse));

            /// Since the ID of the super-alpha will be the same as ours
            this.superAlphaId = this._id;
        }

        console.log(`[Alpha] Updating with sub/super universe info: ${this._id}`);

        promises.push(this.dbUpdate({
            $set: {
                subAlphaId: this.subAlphaId,
                superAlphaId: this.superAlphaId
            }
        }));

        return Promise.all(promises).then(() => this);
    }

    rollbackPublish(args, error) {
        Debug.assert(error, `Cannot rollback a publish without giving a proper error: ${this._id}`);
        this.isPublished = false;
        this.modifiedDate = new Date();
        this.lastError = error;

        return this.dbUpdate({
            $set: {
                isPublished: this.isPublished,
                modifiedDate: this.modifiedDate,
                lastError: this.lastError
            }
        });
    }

    publish(args: ServerArgs): Promise<Alpha> {
        args.assert(!this.defs.map.endDatePercent && this.defs.map.endDate, 400, `[Alpha] Must run the alpha at least once before publishing!`);

        console.info(`[Alpha] Updating: ${this.dbName}.${this._id}`);

        this.modifiedDate = new Date();
        this.publishedDate = new Date();
        this.lastError = "";

        /// Get the index for stats
        let statsIndex: number = -1;

        for (let index = 1; index < this.portfolio.children.length && statsIndex < 0; index++) {
            if (this.portfolio.children[index].m_name == "Stats") 
                statsIndex = index;
        }

        let updateObj: any = {
            $set: {
                isPublished: true,
                lastError: "",
                modifiedDate: this.modifiedDate,
                publishedDate: this.publishedDate
            }
        };

        if (statsIndex >= 0 && statsIndex < this.portfolio.children.length) {
            updateObj["$set"][`portfolio.children.${statsIndex}.map.generateThumbnail`] = "true";
        }
        
        /// First of all we update the record in the local database!
        return this.dbUpdate(updateObj).then(() => {
            /// Then we set the local isPublished flag true ... this will change the db in the dbName accessor
            this.isPublished = true;
            this.defs.map.dbName = this.dbName;

            /// This will ensure that the OutSample is collected correctly!
            this.portfolio.map.creationDate = this.defs.map.endDate;
            this.portfolio.children[0].map.creationDate = this.portfolio.map.creationDate;

            /// Now we set the endDate of the Alpha to run all the way up to today
            this.defs.map.endDate = "TODAY";
            
            console.info(`[Alpha] Publishing: ${this.dbName}.${this._id}`);

            /// This will be an insert operation (in ANOTHER DB). the dbName accessor will change the name of the 
            /// db depending on whether the alpha has been published or not!
            /// Here we run an upsert operation instead of an insert because there could have been a failed 
            /// publish in the past, which would cause the insert operation to fail. 
            return this.dbUpdate({
                upsert: true,
                constants: {
                    isMinimal: true,
                    source: ""
                }
            });
        }).then(() => {
            let promises = [];

            if (this._subAlpha) {
                console.info(`[Alpha] Publishing sub-universe: ${this._id} :: ${this._subAlpha.getUniverseId()}`);
                promises.push(this._subAlpha.insert());
            }

            if (this._superAlpha) {
                console.info(`[Alpha] Publishing super-universe: ${this._id} :: ${this._superAlpha.getUniverseId()}`);
                promises.push(this._superAlpha.insert());
            }

            return Promise.all(promises);
        }).then(() => this);
    }

    loadByName(userId: ObjectID, name: string, market: string, noLoadSource: boolean = false) {
        super.loadByName(userId, name, market)
            .then((alpha: AlphaOrStrategy) => {
                if (!alpha) 
                    return null;

                return this.getSource();
            });
    }

    loadId(alphaId: ObjectID, options: AlphaOrStrategyLoadOptions = null) {
        if (!options)
            options = new AlphaOrStrategyLoadOptions();

        console.trace(`[Alpha] Loading alpha: ${alphaId} - Source Load: ${!options.noLoadSource}`);

        let query: any = {
            _id: alphaId
        };

        // if (options.configType)
        //     query.configType = options.configType;

        return this.dbLoadOne({ 
            query: query,
            dbName: options.dbName
        }).then((alpha: Alpha) => {
            if (!alpha) 
                return null;

            console.trace(`[Alpha] Loaded alpha: ${alphaId}`);
            this.keyStats       = options.keyStats;
            this.containerId    = options.dbName;

            if (!this.containerId)
                this.containerId = this.dbName;

            // noLoadSource = !!noLoadSource || this.isExpression();

            if (options.noLoadSource)
                return PromiseUtil.resolvedPromise(this);

            return this.getSource();
        });
    }

    loadSubAlpha(checkCached) {
        if (this._subAlpha)
            return PromiseUtil.resolvedPromise(this._subAlpha);
        return (new SubAlpha()).loadId(this._id);
    }

    loadSuperAlpha(checkCached) {
        if (this._superAlpha)
            return PromiseUtil.resolvedPromise(this._superAlpha);
        return (new SuperAlpha()).loadId(this._id);
    }

    loadIdWithKeyStats(alphaId: ObjectID, keyStatsTable: string) {
        let options = new AlphaOrStrategyLoadOptions();
        options.noLoadSource = true;

        return this.loadId(alphaId, options)
            .then(() => {
                if (!keyStatsTable)
                    return PromiseUtil.resolvedPromise(this);
                return this.loadKeyStats(keyStatsTable);
            })
    }
    
    delete() {
        return this.deleteSource()
            .then(() => this.dbDelete());
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /// Static methods
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    static validateUniverse(market: string, universeId: string) {
        return Alpha.validateMarket(market) && Universes[market].indexOf(universeId) >= 0;
    }

    static validateMarket(market) {
        return Universes[market] ? true : false;
    }

    static loadAllPublishedAlphas(userId, market) {
        return Alpha["dbLoad"]({
            dbName: DB[DB.pesa + market],

            query: {
                ownerId: userId,
                isPublished: true
            },

            sort: {
                publishedDate: 1
            }
        });
    }

    static loadAllUnpublishedAlphas(userId) {
        return Alpha["dbLoad"]({
            dbName: DB.pesaUser,

            query: {
                ownerId: userId,
                isPublished: false
            },

            sort: {
                creationDate: 1
            }
        });
    }

    static loadAllAlphas(userId: ObjectID, market: string) {
        console.log(`[Alpha] Loading all alphas for user: ${userId}`);
        
        return Alpha["dbLoad"]({
            dbName: DB.pesaUser,

            query: {
                ownerId: userId,
                type: AlphaType.alpha,
                market: market
            },

            sort: {
                creationDate: 1
            }
        });
    }

    static async getFeed(args: ServerArgs, market: string, universeId: string, startId: number, count: number, sortBy: string, 
        sortOrder: SortOrder, ownerId: ObjectID, dbName: string, configType: ConfigType, delay = 1, isPublished: boolean = true): Promise<AlphaFeed<AlphaOrStrategy>> {
        return AlphaOrStrategy.getAlphaFeed<Alpha>(Alpha, args, market, universeId, startId, count, sortBy, sortOrder, ownerId, dbName, "Alpha", configType, delay, isPublished);
    }

    static hasAccess(args: ServerArgs): Promise<Alpha> {
        args.assert(args._cached.alpha, 400, `Cannot access because alpha not loaded: ${(args as any).alphaId}`);
        return args._cached.alpha.hasAccess(args) as Promise<Alpha>;
    }

    static isOwner(args: any): Promise<Alpha> {
        let promise = null;

        if (!args._cached.alpha) {
            args.assert((args as any).alphaId, 400, `Must pass an alpha id!`);

            let alpha: Alpha = new Alpha();
            promise = alpha.loadId((args as any).alphaId);
        }
        else
            promise = PromiseUtil.resolvedPromise(args._cached.alpha);

        return promise.then((alpha: Alpha) => {
            args._cached.alpha = alpha;
            return args._cached.alpha.isOwner(args) as Promise<Alpha>;
        });
    }

    static validateUpdate(args: ServerArgs, market: string, universeId: string, startDate: number, endDatePercent: number) {
        /// Validate the market and universeId first
        args.assert(market && Alpha.validateMarket(market), 400, `Invalid market: ${market}`);
        args.assert(universeId && Alpha.validateUniverse(market, universeId), 400, `Invalid universe: ${universeId}`);

        let source          = args._req.body.data;
        let sconfig         = args._appServer.config(ShAlpha.getAssetClassForMarket(market));
        let aconfig         = sconfig.alpha;
        let minStartDate    = parseInt(aconfig.startDate);
        let minOSPercent    = parseFloat(aconfig.minOutSamplePercent);
        let maxOSPercent    = parseFloat(aconfig.maxOutSamplePercent);

        args._error.assert((endDatePercent >= minOSPercent && endDatePercent <= maxOSPercent) || args._cached.user.isPortfolioManagerOrBetter, 400,
             `Invalid OUT SAMPLE: Percent out-sample given: ${endDatePercent} - Range: [${minOSPercent}, ${maxOSPercent}]`);
        args._error.assert(startDate >= minStartDate, 400, `Invalid start date: ${startDate} - Minimum start date: ${minStartDate}`);
        // args._error.assert(AlphaBucket.validate(args.bucket), 400, `Invalid bucket: ${args.bucket}. Valid values: ${AlphaBucket.values()}`);

        return {
            source: source,
            startDate: startDate,
            endDatePercent: endDatePercent
        };
    }

    static validate(args: ServerArgs, alphaId: ObjectID, dbName: string, configType: ConfigType = ConfigType.any) {
        args.assert(dbName, 400, "Invalid DB Name passed for validating alpha!");
        
        if (args._cached.alpha) {
            return args._cached.alpha._id.equals(alphaId);
        }

        let configObj = gCache.getAlpha(alphaId);
        if (configObj) {
            args._cached.alpha = configObj;
            return PromiseUtil.resolvedPromise(args._cached.alpha);
        }

        return (new Alpha()).loadId(alphaId, new AlphaOrStrategyLoadOptions(true, null, dbName, configType))
            .then((alpha) => {
                args._cached.alpha = alpha;
                gCache.addAlpha(alphaId, alpha);
                return alpha;
            });
    }
}

/// DerivedAlpha: Alpha in another db/collection, that is derived from a User Alpha
@ModelObject()
export class DerivedAlpha extends Alpha {
    @ModelClassId(DerivedAlpha, "DerivedAlpha") __dummy: string;

    _alpha          : Alpha = null;

    constructor() {
        super();
    }

    load(alpha: Alpha, universeId: string) { 
        Debug.assert(alpha, "Cannot create a derived alpha without a parent alpha!");
        Debug.assert(universeId, "Cannot create a derived alpha without a universe!");
        Debug.assert(universeId != alpha.getUniverseId(), `Universes of derived and parent alpha must be different. Derived alpha universe: ${universeId} - Parent alpha universe: ${alpha.getUniverseId()}`);

        this._alpha = alpha;

        /// first of all we load the alpha
        return this.loadId(this._alpha._id)
            .then(() => {
                this._alpha = null;

                this.setUniverseId(universeId);
                this.defs.map.dbName = this.dbName;
                this.refConfig = "{$APP}/Configs/Misc/minimal_ref_derived.xml";

                return this;
            });
    }

    insert() { 
        this.defs.map.endDate = "TODAY";

        /// Again, just like the publish process of the main alpha, we use the upsert rather than an insert.
        /// The reasons are the same i.e. if a failed previous publish has happened (since the process isn't atomic)
        /// the DB state can be inconsistent and a manual intervention would be required if insert is used.
        /// Using an upsert would mean that things would correct themselves!
        return this.dbUpdate({
            upsert: true,
            constants: {
                isMinimal: true,
                source: ""
            }
        });
    }

    create(alpha, universeId) {
        return this.load(alpha, universeId)
            .then(() => this.insert());
    }
}

/// SubAlpha: Represents published alpha but on the SUB-Universe
@ModelObject()
export class SubAlpha extends DerivedAlpha {
    @ModelClassId(SubAlpha, "SubAlpha") __dummy: string;

    static get collectionName() { return Collections.alpha; }
    get dbName() {
        if (this._alpha)
            return this._alpha.dbName;
        
        return DB["pesaSub" + this.market];
    }

    constructor() {
        super();
        this.logSuffix = '.sub';
    }

    getRunStartNotificationId() { return PushNotificationType.subAlphaRun; }
    getRunFinishedNotificationId() { return PushNotificationType.subAlphaRun_Finished; }
    getRunStatusNotificationId() { return PushNotificationType.subAlphaRun_Status; }

}

/// SuperAlpha: Represents published alpha but on the SUPER-Universe
@ModelObject()
export class SuperAlpha extends DerivedAlpha {
    @ModelClassId(SuperAlpha, "SuperAlpha") __dummy: string;

    static get collectionName() { return Collections.alpha; }
    get dbName() {
        if (this._alpha)
            return this._alpha.dbName;
        
        return DB["pesaSuper" + this.market];
    }

    constructor() {
        super();
        this.logSuffix = '.super';
    }

    getRunStartNotificationId() { return PushNotificationType.superAlphaRun; }
    getRunFinishedNotificationId() { return PushNotificationType.superAlphaRun_Finished; }
    getRunStatusNotificationId() { return PushNotificationType.superAlphaRun_Status; }

}

export default function init(app) {
    console.info("[Model] Init Alpha");
    new Alpha();
    new SubAlpha();
    new SuperAlpha();
    new PublishTests();

    // initialize the indexes!
    let dbConns = db.Manager.instance.getDBConns();

    for (let dbConnId in dbConns) { 
        let conn = dbConns[dbConnId];
        let collection = conn.collection(Alpha.collectionName);
    
        // collection.ensureIndex({ ownerId: 1, market: 1 } );
        // collection.ensureIndex({ ownerId: 1, market: 1, isPublished: 1 } );
        // collection.ensureIndex({ ownerId: 1, market: 1, name: 1 }, { unique: true });
        // collection.ensureIndex({ universeId: 1, market: 1, delay: 1 });
    }
}

