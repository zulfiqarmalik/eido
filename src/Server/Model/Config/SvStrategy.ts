import { eido } from 'Shared/Core/ShCore';
import { ObjectID } from "Shared/Core/ObjectID";
import { ShStrategy, RawStatsType } from 'Shared/Model/Config/ShStrategy';
import { Model, SortOrder } from 'Shared/Model/ShModel';
import { Util } from 'Server/Core/SvUtil';
import { Positions } from 'Shared/Model/Stats/Positions';
import * as path from 'path';
import { Debug, PromiseUtil } from 'Shared/Core/ShUtil';
import { DB, Collections, gDefaultUserId, gCache } from 'Server/Model/SvModel';
import { db } from 'Server/Core/DB/DBManager';
import { AlphaOrStrategy, AlphaOrStrategyLoadOptions } from './SvConfigBase';
import { PushNotificationType } from 'Shared/Core/PushNotificationType'
import { ServerArgs } from '../../Core/ServerArgs';
import { Alpha } from './SvAlpha';
import { ConfigSection } from 'Shared/Model/Config/ConfigSection';
import { AlphaSubType, ConfigType, AlphaType } from 'Shared/Model/Config/ShAlpha';
import { ModelObject, ModelClassId } from 'Shared/Core/BaseObject';
import { LTStats } from 'Shared/Model/Stats/LTStats';
import { CheckpointType, Checkpoint } from 'Server/Services/Data/Checkpoint';
import { AlphaFeed } from 'Shared/Model/Config/AlphaFeed';
 
@ModelObject()
export class Strategy extends AlphaOrStrategy {
    @ModelClassId(Strategy, "Strategy") __dummy: string;
    
    logSuffix           : string = "";

    static get collectionName() { return Collections.strategy; }

    static getDBName(market: string): string { 
        if (market)
            return DB["pesa" + market];
        return DB.pesaUser;
    }
    
    get dbName() { 
        if ((this.isPublished || this.isMaster) && this.market)
            return DB["pesa" + this.market];
        return DB.pesaUser;
    }

    constructor() {
        super();
    }

    getRunStartNotificationId(): PushNotificationType { return PushNotificationType.strategyRun; }
    getRunFinishedNotificationId(): PushNotificationType { return PushNotificationType.strategyRun_Finished; }
    getRunStatusNotificationId(): PushNotificationType { return PushNotificationType.strategyRun_Status; }
    getCheckpointType(): CheckpointType { return CheckpointType.portfolio; }

    getRunLogFilename() {
        /// For expression alphas just use the owner's directory as the base path for the log filename
        let ownerDir = path.join(eido.appServer.config(this.getAssetClass()).strategyDir.path, this.market, this.ownerId.toString());
        return path.join(ownerDir, `${this.name}${this.logSuffix}.log`);
    }

    createNew(args: ServerArgs, userId: ObjectID, name: string, category: string, market: string, universeId: string, isMaster: boolean, refConfig: string, delay: number) {
        super.create(userId || args.userId(), name, category, market, universeId, AlphaSubType.none, AlphaType.strategy);

        if (!refConfig) {
            /// If no ref config is specified, then use a default one!
            refConfig       = `{$APP}/Configs/Misc/minimal_ref_dev_strategy.xml`
            console.info(`[Strategy] No ref config given. Defaulting to: ${refConfig}`)
        }

        this.type           = AlphaType.strategy;
        this.subType        = AlphaSubType.none;
        this.isMaster       = isMaster;
        this.refConfig      = refConfig;
        this.configType     = ConfigType.user;

        if (isMaster)
            this.ownerId    = gDefaultUserId;

        if (!this.refConfig) {
            this.refConfig  = "{$APP}/Configs/Misc/{$market}/minimal_ref_dev.xml";
            console.log(`[Strategy] No refConfig supplied. Using default: ${this.refConfig}`);
        }

        this.defs.map.startDate = "20080101";

        this.portfolio.map.universeId = universeId;
        this.portfolio.map.iid = this._id.toString();
        this.portfolio.map.delay = `${delay}`.toString();
        this.portfolio.children = [new ConfigSection(
            "Alphas", {
                type        : "Alphas",
                id          : "SimpleCombination",
                uuid        : `Combo_${this._id}`,
                func        : "add"
            }, [new ConfigSection(
                "Operation", {
                    id      : "OpNormalise"
                }, []
            )]
        )];

        console.log("[Strategy] Inserting strategy: %s", this._id.toString());
        return this.dbInsert({
            constants: {
                isMinimal: true,
                source: ""
            }
        });
    }

    addAlphaGeneric(alphaSec: any): Promise<Strategy> {
        return this.dbUpdate({
            $set: {
                "defs.map.startDate": this.startDate,
                "portfolio.map.delay": this.getDelay().toString(),
                modifiedDate: new Date()
            },
            $push: {
                "portfolio.children.0.children": alphaSec
            }
        }).then(() => {
            /// Reload the document
            return this.dbLoadOne();
        });
    }

    updateWeight(args: ServerArgs, alphaId: ObjectID, weight: number, reversion: boolean): Promise<Strategy> {
        let index = this.getAlphaIndex(alphaId);
        args.assert(index >= 0, 400, `[Strategy] Unable to find alpha: ${alphaId}`);
        let updateInfo = {
            modifiedDate: new Date()
        };
        
        let key = `portfolio.children.0.children.${index}.map.weight`;

        let sweight = weight.toFixed(2);
        console.log(`[Strategy] Updating alpha weight: ${alphaId} = ${sweight} [Reversion: ${reversion}]`);

        updateInfo[`portfolio.children.0.children.${index}.map.weight`] = weight.toFixed(2);
        updateInfo[`portfolio.children.0.children.${index}.map.reversion`] = `${reversion}`;

        return this.dbUpdate({
            $set: updateInfo
        }).then(() => this);
    }

    setBookSize(bookSize: number) {
        super.setBookSize(bookSize);

        return this.dbUpdate({
            $set: {
                "portfolio.map.bookSize": this.getBookSize().toExponential(2),
                modifiedDate: new Date()
            }
        });
    }

    addAlpha(alpha: Alpha, weight: number, reversion: boolean = false): Promise<Strategy> {
        let alphaSec = {
            m_name: "Strategy",
            map: {
                iid: alpha._id.toString(),
                uuid: alpha._id.toString(),
                universeId: alpha.getUniverseId(),
                dbName: alpha.containerId || alpha.dbName,
                reversion: `${reversion}`,
                configType: alpha.configType,
                delay: (alpha.portfolio.map.delay != null && typeof(alpha.portfolio.map.delay) != 'undefined' ? `${alpha.portfolio.map.delay}` : "0").toString(),
                weight: weight.toFixed(2)
            }
        };

        /// Get the start date of the alpha
        let portStartDate = parseInt(this.startDate);
        let startDate = parseInt(alpha.startDate);

        if (startDate && (startDate > portStartDate || !portStartDate))
            this.startDate = alpha.startDate;

        if (this.getDelay() > alpha.getDelay())
            this.setDelay(alpha.getDelay());

        return this.addAlphaGeneric(alphaSec);
    }

    removeAlpha(alphaId: ObjectID) {
        return this.dbUpdate({
            query: {
                _id: this._id,
                market: this.market
            },
            $set: {
                modifiedDate: new Date()
            },
            $pull: {
                "portfolio.children.0.children": {
                    map: {
                        iid: alphaId.toString()
                    }
                }
            }
        });
    }

    addStrategy(strategy: Strategy, weight: number): Promise<Strategy> {
        let strategySec = {
            m_name: "PosStrategyDB",
            map: {
                iid: strategy._id.toString(),
                uuid: strategy._id.toString(),
                weight: weight.toFixed(2)
            }
        };

        return this.addAlphaGeneric(strategySec);
    }

    loadByName(userId: ObjectID, name: string, market: string) {
        this.market = market;

        return this.dbLoadOne({ 
            query: {
                ownerId: userId,
                name: name,
                market: market
            }
        });
    }

    loadId(strategyId: ObjectID, options: AlphaOrStrategyLoadOptions) {
        if (!options)
            options = new AlphaOrStrategyLoadOptions();

        let query: any = {
            _id: strategyId
        };

        if (options.configType)
            query.configType = options.configType;

        return this.dbLoadOne({ 
            query: query,
            dbName: options.dbName
        }).then(() => {
            this.keyStats = options.keyStats;
            this.containerId = options.dbName;

            if (!this.containerId)
                this.containerId = this.dbName;

            return this;
        });
    }

    delete() {
        return this.dbDelete();
    }
    
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /// Static methods
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    static loadAllStrategies(userId: ObjectID, market: string, isPublished: boolean) {
        let query: any = { 
            $and: [
                { type: AlphaType.strategy },
                { market: market },
                { isPublished: isPublished }  
            ]
        };

        if (userId) {
            query.$and.push({ ownerId: userId });
        }

        console.log(`[Strategy] Strategy query: ${JSON.stringify(query)}`);
        
        return Strategy.dbLoad({
            dbName: DB.pesaUser,

            query: query,

            sort: {
                creationDate: 1
            }
        });
    }

    static getStrategiesFromPath(basePath: string, filesOnly: boolean = false) {
        return new Promise((resolve, reject) => {
            let fs = require('fs');
            let path = require('path');

            console.log("[Strategy] Reading strategies from basePath: %s", basePath);
            
            fs.readdir(basePath, (err, files) => {
                if (err)
                    return reject(err);

                let strategies = [];

                files.map((file) => {
                    return path.join(basePath, file);
                }).filter((file) => {
                    return !filesOnly ? fs.statSync(file).isDirectory() : fs.statSync(file).isFile() && path.extname(file) === '.xml';
                }).forEach((file) => {
                    console.trace("[Strategy] Strat: %s", file);

                    let configFile      = file;

                    if (!filesOnly) {
                        configFile      = path.join(file, "config.xml");

                        if (!fs.existsSync(configFile))
                            return;
                    }

                    let util            = require('util');
                    let stats           = fs.statSync(configFile);
                    let strat           = new Strategy();
                    strat._id           = path.basename(file);
                    strat.isFilesystem  = true;
                    strat.path          = file;
                    strat.creationDate  = stats.birthtime;
                    strat.modifiedDate  = stats.mtime;

                    strategies.push(strat);
                });

                console.log("[Strategy] Num files: %d [Path = %s]", strategies.length, basePath);
                resolve(strategies);
                return strategies;
            });
        });
    }

    static getPosFilenames(posDir, uuid, pathSep) {
        if (!pathSep)
            pathSep = '|';
            
        return Util.searchFiles(path.join(posDir, uuid + "*.pos"), null, pathSep, true);
    }


    static getPssFilenames(pssDir, uuid, pathSep) {
        if (!pathSep)
            pathSep = '|';
            
        return Util.searchFiles(path.join(pssDir, uuid + "*.pss"), null, pathSep, true);
    } 

    static getPosAndPssFilenames(posDir, pssDir, uuid, startId, endId, pathSep) {
        if (!pathSep)
            pathSep = '|';

        let result = {
            _posFiles: null,
            _pssFiles: null,
            _pinfos: []
        };

        Debug.assert(uuid, "Invalid UUID passed for Pos and Pss file search!");

        return Strategy.getPosFilenames(posDir, uuid, pathSep)
            .then((posFiles) => {
                result._posFiles = posFiles;
                return Strategy.getPssFilenames(pssDir, uuid, pathSep);
            })
            .then((pssFiles) => {
                result._pssFiles = pssFiles;

                Debug.assert(result._pssFiles.length == result._posFiles.length, `Length of pos files and pss files does not match. Num pos files: ${result._posFiles.length} - Num pss files: ${result._pssFiles.length}`);

                let pssStartId = path.join(pssDir, startId + ".pss").replace(/\/|\\/g, pathSep);
                let pssEndId = path.join(pssDir, endId + ".pss").replace(/\/|\\/g, pathSep);
                let matchStarted = false;
                let matchEnded = false;
                let hasDateInEndFilename = !!Positions.getDateFromPosFilename("", endId);

                console.log("[Pos] Search range: %s [%s] - %s [%s]", pssStartId, startId, pssEndId, endId);

                for (let i = 0; i < result._pssFiles.length && !matchEnded; i++) {
                    let pssFile = result._pssFiles[i];
                    if (pssFile >= pssStartId || matchStarted) {
                        if (pssFile > pssEndId && hasDateInEndFilename) {
                            matchEnded = true;
                            break;
                        }
                        if (!matchStarted)
                            matchStarted = true;

                        // console.log("[Pos] Matched: %s", pssFile);
                        let posFile = result._posFiles[i];

                        result._pinfos.push({
                            posFilename: posFile.replace(/\|/g, '/'),
                            pssFilename: pssFile.replace(/\|/g, '/')
                        });
                    }
                }

                return result;
            });
    }

    static loadPositions(posFilename, pssFilename) {
        console.log("[Pos] Loading: %s", posFilename);
        let positions = Util.loadPositionsFromFile(posFilename, Positions);

        if (pssFilename) {
            let fs = require('fs');
            let pssData = fs.readFileSync(pssFilename, "utf8");

            positions.loadStringPSS(pssData);
        }

        return positions;
    }

    static checkExists(userId: ObjectID, name: string, market: string) {
        return (new Strategy()).loadByName(userId, name, market)
            .then((strategy) => { return strategy ? true : false; })
            .catch(() => { return false });
    }

    static hasAccess(args: ServerArgs): Promise<Strategy> {
        args.assert(args._cached.strategy, 400, `Strategy not loaded: ${(args as any).strategyId}`);
        return args._cached.strategy.hasAccess(args) as Promise<Strategy>;
    }

    static isOwner(args: any): Promise<Strategy> {
        args.assert(args._cached.strategy, 400, `Strategy not loaded: ${(args as any).strategyId}`);
        return args._cached.strategy.isOwner(args) as Promise<Strategy>;
    }

    static validateMaster(args: ServerArgs, masterId: ObjectID = null, dbName: string) {
        if (args._cached.master) {
            return args._cached.master._id.toString() == masterId.toString();
        }
        else if (masterId) {
            return (new Strategy()).loadId(masterId, new AlphaOrStrategyLoadOptions(true, null, dbName))
                .then((master) => {
                    args._cached.master = master;
                    return master;
                });
        }
    }

    static validate(args: ServerArgs, strategyId: ObjectID, dbName: string = "", configType: ConfigType = ConfigType.any) {
        if (args._cached.strategy) {
            return args._cached.strategy._id.toString() == strategyId.toString();
        }

        let configObj = gCache.getAlpha(strategyId);
        if (configObj) {
            args._cached.strategy = configObj;
            return PromiseUtil.resolvedPromise(configObj);
        }

        args.assert(strategyId, 400, `[Strategy] Invalid call to validate strategy without any strategyId!`);
        return (new Strategy()).loadId(strategyId, new AlphaOrStrategyLoadOptions(true, null, dbName, configType))
            .then((strategy) => {
                args._cached.strategy = strategy;
                gCache.addAlpha(strategyId, strategy);
                return strategy;
            });
    }
    
    static async getFeed(args: ServerArgs, market: string, universeId: string, startId: number, count: number, sortBy: string, 
        sortOrder: SortOrder, ownerId: ObjectID, dbName: string, configType: ConfigType, delay = 1, isPublished: boolean = true): Promise<AlphaFeed<AlphaOrStrategy>> {
        return AlphaOrStrategy.getAlphaFeed<Strategy>(Strategy, args, market, universeId, startId, count, sortBy, sortOrder, ownerId, dbName, "Strategy", configType, delay, isPublished);
    }
}

export default function init(app) {
    console.info("[Model] Init Strategy");
    new Strategy();
    // console.log(Strategy.__modelProperties);

    // initialize the indexes!
    let dbConns = db.Manager.instance.getDBConns();

    for (let dbConnId in dbConns) { 
        let conn = dbConns[dbConnId];
        let collection = conn.collection(Strategy.collectionName);
    
        // collection.ensureIndex({ ownerId: 1 });
        // collection.ensureIndex({ ownerId: 1, name: 1 }, { unique: true });
    }
}
