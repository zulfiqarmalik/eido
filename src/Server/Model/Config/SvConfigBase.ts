import { eido } from 'Shared/Core/ShCore';
import { ObjectID } from "Shared/Core/ObjectID";
import { getDefaultOp, ConfigType, AlphaType } from 'Shared/Model/Config/ShAlpha';
import { ShUtil, Debug, PromiseUtil } from 'Shared/Core/ShUtil';
import { StatsTypes, Collections, DB, gDefaultUserId, gCache } from 'Server/Model/SvModel';
import { Positions } from 'Shared/Model/Stats/Positions';
import { spawn } from 'child_process';
import Timer from 'Shared/Core/Timer';
import { PublishTests } from './SvPublishTests';
import { LTStats } from 'Shared/Model/Stats/LTStats';
import * as path from 'path';
import { Util } from 'Server/Core/SvUtil';
import { LiveStats } from 'Shared/Model/Config/BaseConfig';
import { ServerArgs } from '../../Core/ServerArgs';
import { ShStrategy, RawStatsType } from 'Shared/Model/Config/ShStrategy';
import { ModelObject, ModelClassId } from 'Shared/Core/BaseObject';
import { CObject } from 'Shared/Core/CObject';
import { ConfigSection } from 'Shared/Model/Config/ConfigSection';
import { db } from 'Server/Core/DB/DBManager';
import { Checkpoint, CheckpointType } from 'Server/Services/Data/Checkpoint';
import { ServerConfig } from 'Server/App/ServerConfig';
import { PosData } from 'Server/Services/Data/BinData';
import { AppServer } from 'Server/App/EidoApp';
import { Market, DataCache, Universe } from 'Server/Services/Data/DataCache';
import { UserLevel } from 'Shared/Model/User/ShUser';
import { SortOrder } from 'Shared/Model/ShModel';
import { AlphaFeed } from 'Shared/Model/Config/AlphaFeed';

export class AlphaOrStrategyLoadOptions {
    noLoadSource        : boolean = true;
    keyStats            : LTStats = null;
    dbName              : string = "";
    configType          : ConfigType = ConfigType.any;

    constructor(noLoadSource: boolean = true, keyStats: LTStats = null, dbName: string = "", configType: ConfigType = ConfigType.any) {
        this.noLoadSource = noLoadSource;
        this.keyStats = keyStats;
        this.dbName = dbName;
        this.configType = configType;
    }
}

@ModelObject()
export class AlphaOrStrategy extends ShStrategy {
    @ModelClassId(AlphaOrStrategy, "AlphaOrStrategy") __dummy: string;

    static get collectionName() { return Collections.userConfigs; }
    get collectionName() { return Collections.userConfigs; }

    _statsPnlData       : LiveStats = null;
    _livePnl            : LiveStats = null;
    _posInfo            : any = null;
    _userId             : ObjectID = null;
    _livePnlIndex       : number = 0;
    _didSendDates       : boolean = false;
    _timer              : any = null;
    _runningLog         : string[] = [];
    _ws                 : any = null;
    _stats              : LTStats = null;
    _positions          : Positions = null;
    _checkpoint         : Checkpoint = null;
    _rawCheckpoint      : Checkpoint = null;

    _totalCount         : number = 0;
    _progressIter       : number = 0;

    getRunStartNotificationId() { return ""; }
    getRunFinishedNotificationId() { return ""; }
    getRunStatusNotificationId() { return ""; }
    getCheckpointType(): CheckpointType { return CheckpointType.alpha; }

    dbUpdate(updateObj: any): Promise<AlphaOrStrategy> {
        return (new Promise((resolve, reject) => {
            let conn = db.Manager.instance.getObjectConn(new db.ConnArgs(null, this.dbName));

            console.log(`[${this.type}] Setting publish flags on stats: ${this._id}`);

            let collConfig = conn.collection(Collections.userConfigs);

            collConfig.updateOne({
                _id: this._id,
                market: this.market
            }, updateObj, (err) => {
                !err ? resolve(this) : reject(err)
            });
        }));
    }
    
    cancel(): Promise<AlphaOrStrategy> {
        if (!this.isRunning)
            return PromiseUtil.resolvedPromise(this);

        this.isCancelled = true; 
        this.isRunning = false;
        this.isFinished = true;

        return this.dbUpdate({
            $set: {
                isCancelled: this.isCancelled,
                isRunning: this.isRunning,
                isFinished: this.isFinished
            }
        });
    }

    hasAccess(args: ServerArgs): Promise<AlphaOrStrategy> {
        return this.isOwner(args);
    }

    isOwner(args: ServerArgs): Promise<AlphaOrStrategy> {
        args.assert(args.userId(), 400, "No current user!");

        if (args.user().hasLevel(UserLevel.portfolioManager) || this.ownerId.equals(args.userId()))
            return PromiseUtil.resolvedPromise(this);

        return PromiseUtil.failedPromise(null);
    }

    clearStatus() {
        this.isCancelled = false;
        this.isFinished = false;
        this.percent = 0.0;
        this.exitCode = 0;
        this._runningLog = [];

        /// Also clear out the live pnl 
        return this.clearLivePnl()
            .then(() => { 
                return this.dbUpdate({
                    $set: {
                        isCancelled: this.isCancelled,
                        isFinished: this.isFinished,
                        percent: this.percent,
                        exitCode: this.exitCode,
                        log: this._runningLog
                    }
                });
            });
    }

    setRunStatus(exitCode) {
        this.isFinished = true; 
        this.isRunning = false;
        this.exitCode = exitCode;

        return this.dbUpdate({ 
            $set: {
                isFinished: this.isFinished,
                isRunning: this.isRunning,
                exitCode: this.exitCode
            }
        });
    }

    getRunStatus(noLiveStream: boolean = false) { 
        this.isCancelled = false;

        // return this.dbFindAndModify({ 
        //     remove: true,
        //     raw: true,
        //     query: this._id,
        //     update: {
        //         isCancelled: this.isCancelled,
        //         $set: {
        //             log: []
        //         }
        //     }
        // });
        return this.dbLoadOne()
            .then((result) => {
                this.isFinished  = result.isFinished;
                this.isCancelled = result.isCancelled;
                this.isRunning   = result.isRunning;
                this.isFailed    = result.isFailed;
                this.percent     = result.percent;
                this.exitCode    = result.exitCode;
                // this._runningLog = [];

                if (noLiveStream)
                    return PromiseUtil.resolvedPromise(null);

                return this.getLivePnlStream();
            })
            .then((livePnl) => {
                if (livePnl) {
                    this._livePnl = livePnl;
                }

                return this;
            });
    }

    clearLivePnl(): Promise<AlphaOrStrategy> {
        // return PromiseUtil.resolvedPromise(this);
        return this.dbUpdate({
            collectionName: Collections.stats,
            dbName: this.containerId,
            cls: null,

            query: {
                _id: this._id,
                stype: "Live"
            },

            $set: {
                pnls: [],
                dates: []
            }
        });
    }

    loadCheckpoint(dbName: string): Promise<Checkpoint> {
        /// If a checkpoint has already been loaded then just return that ...
        if (this._checkpoint)
            return PromiseUtil.resolvedPromise(this._checkpoint);

        /// Otherwise load a fresh one
        return Checkpoint.load(eido.appServer.config(this.getAssetClass()), this._id, this.getCheckpointType())
            .then((checkpoint: Checkpoint) => {
                this._checkpoint = checkpoint;
                return this._checkpoint;
            });
    }

    getStatsPnlStream(): Promise<LiveStats> {
        if (this._statsPnlData)
            return PromiseUtil.resolvedPromise(this._statsPnlData);

        return this.dbLoadOne({
            collectionName: Collections.stats,
            dbName: this.containerId,
            cls: null,

            query: {
                _id: this._id.toString()
            }
        }).then((stats) => {
            this._statsPnlData = {
                pnls: new Array(stats.vec.length),
                dates: new Array(stats.vec.length)
            };

            stats.vec.map((st, i) => {
                this._statsPnlData.pnls[i] = st.pnl;
                this._statsPnlData.dates[i] = st.date;
            });

            return this._statsPnlData;
        });
    }
    
    getLivePnlStream(loadCached: boolean = false): LiveStats {
        if (loadCached && this._livePnl)
            return PromiseUtil.resolvedPromise(this._livePnl);

        return this.dbLoadOne({
            collectionName: Collections.stats,
            dbName: this.containerId,
            cls: null,

            query: {
                _id: this._id,
                stype: "Live"
            }
        }).then((livePnl) => {
            this._livePnl = livePnl;
            return this._livePnl;
        });
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////
    /// Related to running
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////
    getRunLogFilename() {
        Debug.assert(false, "This must be overridden by the correct implementation");
    }

    run(args: ServerArgs, noHttpResponse: boolean = false) {
        /// Here we start the process
        this._timer         = 0;

        let sconfig         = args._appServer.config(this.getAssetClass());
        let psimDir         = sconfig.psimDir;
        let psimPath        = sconfig.psimPath;
        let logPath         = this.getRunLogFilename();
        let configPath      = path.join(psimDir, "Configs", "Misc", "db_job.xml");
        let jobId           = this.containerId + "." + this.constructor["collectionName"] + "@" + this._id.toString();
        let psimArgs        = ['-c' + configPath, '-J' + jobId, '-l' + logPath, '-O', '-Q', '-C', '-W1', '-d', sconfig.dbPath];
        this._userId        = args.userId();
        this._livePnlIndex  = 0;
        this._didSendDates  = false;
        
        this._ws            = eido.appServer.wsServer.getWebSocket(this._userId);

        console.log(`[SV_Config] Starting psim: ${psimPath} [Dir: ${psimDir}, Args: ${JSON.stringify(psimArgs)}]`);

        return new Promise((resolve, reject) => {
            this.clearStatus()
                .then(() => {
                    /// Send the starting notification id
                    this.sendRunStatus(this.getRunStartNotificationId());

                    const proc      = spawn(psimPath, psimArgs);
                    console.log("Clear running log ...");
                    this._runningLog = [];
                    
                    if (proc) { 
                        proc.stdout.on('data', (data: Buffer) => {
                            console.log(`stdout: ${data}`);
                            if (data) {
                                this._runningLog = this._runningLog.concat(data.toString('utf8').split(/\n/));
                                // console.log(`READ RUNNING LOG: ${this._runningLog}`)
                            }
                        });
                        
                        proc.stderr.on('data', (data) => {
                            // console.log(`stderr: ${data}`);
                        });
                        
                        proc.on('close', (code) => {
                            console.log(`Child process exited with code ${code}`);
            
                            /// Reload the configData to get the latest status
                            this.setRunStatus(code)
                                .then(() => {
                                    // if (this._ws)
                                    this.sendRunStatus(this.getRunFinishedNotificationId());

                                    /// Fullfill our promise 
                                    if (!code)
                                        resolve(this);
                                    else
                                        reject(this);
                                });
                        });
                
                        /// Send the HTTP response. The rest is going to be sent via the websocket
                        if (!noHttpResponse)
                            CObject["sendClientRaw"](args, 200, {});
                    }
                    else {
                        args.assert(false, 400, "Unable to start the psim process!");
                    }
            
            
                    // if (!this._ws) {
                    //     console.info("[Alpha] No live websocket. Will not be sending live information. Caller can call /configData/poll");
                    //     return;
                    // }
            
                    this._timer = Timer.setInterval(1000, () => {
                        this.sendRunStatus(this.getRunStatusNotificationId())
                    });
                });
            });
    }

    sendRunStatus(notificationId) {
        if (!this._ws)
            return PromiseUtil.resolvedPromise(this);

        return this.getRunStatus()
            .then((configData) => {
                try {
                    let result = {
                        _id         : this._id,
                        isFinished  : configData.isFinished,
                        isCancelled : configData.isCancelled,
                        isRunning   : configData.isRunning,
                        isFailed    : configData.isFailed,
                        percent     : configData.percent,
                        exitCode    : configData.exitCode,
                        log         : this._runningLog,
                        livePnl     : [],
                        livePnlIndex: this._livePnlIndex,
                        dates       : []
                    };

                    // console.log(`SENT RUNNING LOG: ${result.log}`)

                    if (configData._livePnl) {
                        result.livePnl = configData._livePnl.pnls;
                        result.dates = configData._livePnl.dates;
                        // if (!this._didSendDates)
                        //     result.dates = configData._livePnl.dates;

                        // let length = configData._livePnl.pnls.length; 

                        // for (let i = this._livePnlIndex; i < length; i++) {
                        //     result.livePnl.push(configData._livePnl.pnls[i]);
                        // }

                        // this._livePnlIndex = length;
                    };

                    let notification = {
                        id          : notificationId,
                        data        : result
                    };

                    if (this._ws)
                        this._ws.send(JSON.stringify(notification));

                    this._runningLog = [];

                    if (result.isFinished) {
                        Timer.clearInterval(this._timer);
                        this._timer = 0;
                    }
                }
                catch (e) {
                    console.error('Error:', e);

                    if (this._timer)
                        Timer.clearInterval(this._timer);
                    this._timer = 0;
                }
            })
            .catch((err) => {
                console.error(err);

                if (this._timer)
                    Timer.clearInterval(this._timer);
                this._timer = 0;
            })
        }

        ///////////////////////////////////////////////////////////////////
        loadAllPositions(posInfo: any, startIndex: number, endIndex: number) {
            let numPositions = endIndex - startIndex + 1;
            console.log(`[Pos] Num positions: ${numPositions}`);
            
            let promises = new Array(numPositions);
            
            for (let ii = startIndex; ii <= endIndex; ii++) {
                promises[ii - startIndex] = this.loadPositionsForDate(posInfo.dates[ii]);
            }
            
            return Promise.all(promises);
        }
        
        ///////////////////////////////////////////////////////////////////
        loadAllPosInfo(args: ServerArgs, startDate: number, endDate: number, calculatePos: boolean = false, calculateIC: boolean = false) {
            return this.loadPosInfo()
                .then((posInfo) => {
                    let startIndex  = posInfo.dates.findIndex((elt) => elt == startDate);
                    let endIndex    = posInfo.dates.findIndex((elt) => elt == endDate);

                    console.log(`[Pos] ${startDate} = ${startIndex} and ${endDate} = ${endIndex}`);

                    args.assert(startIndex >= 0 && startIndex < posInfo.dates.length, 400, `Invalid startDate requested: ${startDate}`);
                    args.assert(endIndex >= 0 && endIndex < posInfo.dates.length, 400, `Invalid endDate requested: ${endDate}`);
                    args.assert(startIndex <= endIndex, 400, `Invalid start and end dates requested: [${startDate}, ${endDate}]`);

                    this._totalCount = endIndex - startIndex + 1;
                    this._progressIter = 0;

                    return this.loadAllPositions(posInfo, startIndex, endIndex);
                })
                .then((allPositions) => {
                    let correlation = require('node-correlation');

                    console.log(`[Pos] Num positions returned: ${allPositions.length}`);
                    this._positions = allPositions[0];
                    this._progressIter++;

                    if (calculateIC) {
                        console.log(`[Pos] Calculating Infomation Coefficient: true`);
                        this._positions.ic = this._positions.calculateInformationCoefficient(correlation.calc);
                    }
                    
                    // this.update(this._progressIter / this._totalCount, "Netting positions ...");

                    for (let i = 1; i < allPositions.length; i++) {
                        if (calculatePos)
                            this._positions.add(allPositions[i]);

                        if (calculateIC) {
                            allPositions[i].ic = allPositions[i].calculateInformationCoefficient(correlation.calc);
                            this._positions.ic.add(allPositions[i].ic);
                        }

                        this._progressIter++;
                        // this.update(this._progressIter / this._totalCount);
                    }

                    console.log("[Pos] Finalising ...");
                    // this.updateStatusMessage("Netting all positions ...");

                    this._positions.finaliseNet();

                    // this.update(1.0, "Calculations DONE! Rendering ...");
                    console.log("[Pos] DONE!");

                    let positions = this._positions;
                    
                    /// Delete temporaries
                    this._positions = null;
                    this._progressIter = 0;
                    this._totalCount = 0;
                    
                    return positions;
                });
            
        }

        publishAny() {
            this.publishedDate = new Date();

            /// We don't set the modified date here since publishing doesn't invalidate the checkpoint.
            /// If an action, prior to this would've invalidated the checkpoint, then that should've 
            /// set the modifiedDate as per requirement, and invalidated the checkpoint at the next run

            /// First of all we need to update the isPublished status in all the stats for this alpha/strategy
            return (new Promise((resolve, reject) => {
                let conn = db.Manager.instance.getObjectConn(new db.ConnArgs(null, this.dbName));
                let coll = conn.collection(Collections.stats);
    
                console.log(`[${this.type}] Setting publish flags on stats: ${this._id}`);

                coll.updateOne({ 
                    $and: [
                        { iid: this._id },
                        { stype: "LT" }
                    ]
                }, {
                    $set: {
                        isPublished: true
                    }
                }, { 
                    upsert: false 
                }, (err) => {
                    if (err) {
                        console.error(JSON.stringify(err));
                        reject(err);
                        return;
                    }

                    let collConfig = conn.collection(Collections.userConfigs);

                    /// If it's a strategy then mark the alphas as part of a published strategies
                    // if (this.type == AlphaType.strategy) {
                    //     let alphasToUpdate: ObjectID[] = [];
                    //     this.portfolio.children.map((child: ConfigSection, index: number) => { 
                    //         if (child.map && typeof(child.map.iid) !== 'undefined' && child.map.iid) {
                    //             alphasToUpdate.push(ObjectID.create(child.map.iid));
                    //         }
                    //     });

                    //     /// Now update the child alphas
                    //     console.log(`[${this.type}] Setting parent info on num children: ${alphasToUpdate.length}`);
                    //     collConfig.updateMany({
                    //         _id: { 
                    //             $in: alphasToUpdate
                    //         }
                    //     }, {
                    //         $addToSet: {
                    //             parentInfo: {
                    //                 iid: this._id,
                    //                 name: this.name
                    //             }
                    //         }
                    //     }, (err) => {
                    //         if (err) {
                    //             console.log(`[${this.type}] Error while setting parent info on alphas: ${err}`);
                    //         }
                    //     });
                    // }

                    // console.log(`[${this.type}] Setting publish flags on config: ${this._id}`);

                    collConfig.updateOne({
                        _id: this._id,
                        market: this.market
                    }, {
                        $set: {
                            isPublished: true,
                            // modifiedDate: this.publishedDate,
                            publishedDate: this.publishedDate,
                            "defs.map.endDate": "TODAY"
                        }
                    }, (err) => !err ? resolve(this) : reject(err));
                }
            )}));
  
        }

        loadId(id: ObjectID, options: AlphaOrStrategyLoadOptions) {
            return this.loadAny(id, options.dbName);
        }

        loadByName(userId: ObjectID, name: string, market: string, noLoadSource: boolean = false) {
            return this.dbLoadOne({ 
                query: {
                    ownerId: userId,
                    name: name,
                    market: market
                }
            });
        }

        getSourcePath() {
            // Debug.assert(this.portfolio.children && this.portfolio.children.length == 1, `Invalid portfolio node in alpha-${this._id}: ${this.portfolio}`);

            /// Slightly contrived logic because of backwards compatibility!
            let sconfig = eido.appServer.config(this.getAssetClass());
            return sconfig.resolvePath(this.portfolio.children[0].map.pyPath);
        }

        getSource() {
            if (this.type == AlphaType.strategy)
                return PromiseUtil.resolvedPromise(this);
                
            if (this.isExpression()) {
                this.source = this.expr;
                return PromiseUtil.resolvedPromise(this);
            }
    
            let sourcePath = this.getSourcePath();
            console.log(`[Alpha] Reading source: ${this._id}`);
            return Util.readFile(sourcePath, 'utf8')
                .then((source: string) => {
                    console.log(`[Alpha] Read source: ${this._id}`);
                    this.source = source;
                    return this;
                });
            }

        loadAny(id: ObjectID, dbName: string) {
            let query: any = {
                _id: id,
            };

            return this.dbLoadOne({ 
                query: query,
                dbName: dbName
            }).then((result) => {
                if (!result)
                    throw `Unable to load alpha/strategy: ${id}`
                if (!this.containerId)
                    this.containerId = dbName;
    
                return this;
            });
        }
    
        static checkExists(userId: ObjectID, name: string, market: string) {
            return (new AlphaOrStrategy()).loadByName(userId, name, market, true)
                .then((alpha) => { return alpha ? true : false; })
                .catch(() => { return false });
        }

        static validate(args: ServerArgs, id: ObjectID, dbName: string, configType: ConfigType, type: AlphaType) {
            args.assert(dbName, 400, "Invalid DB Name passed for validating alpha/strategy!");

            if (args._cached.alpha) {
                return args._cached.alpha._id.equals(id);
            }
            else if (args._cached.strategy) {
                return args._cached.strategy._id.equals(id);
            }

            let configObj = gCache.getAlpha(id);
            if (configObj) {
                args._cached.alpha = configObj;
                args._cached.strategy = configObj;
                return PromiseUtil.resolvedPromise(configObj);
            }
    
            return (new AlphaOrStrategy()).loadAny(id, dbName)
                .then((configObj) => {
                    gCache.addAlpha(id, configObj);
                    args._cached.alpha = configObj;
                    args._cached.strategy = configObj;
                    return configObj;
                });
        }

        static fillStatsVectorFromLTStats(stats: any[], rstats: any) {
            stats.map((stat) => {
                stat.__forceCopy = true;
                stat.vec = []
            });

            for (let i = 0; i < rstats.vec.length; i++) {
                let v = rstats.vec[i];

                stats.map((stat) => {
                    if (stat.startDate <= v.date && stat.endDate >= v.date) 
                        stat.vec.push(v);
                });
            }

            return stats;
        }

        static fillStatsVectorFromCheckpoint(stats: any, checkpoint: Checkpoint) {
            let alphaData   = checkpoint.alphaData;
            let rstats      = alphaData.calcData.cumStats.rs;

            for (let key in stats) {
                stats[key].vec = [];
                stats[key].__forceCopy = true;
            }

            /// Append post checkpoint
            if (stats.LT && stats.LT.vec_postCheckpoint) 
                stats.LT.vec_postCheckpoint.forEach((v) => rstats.vec.push(v))

            for (let i = 0; i < rstats.vec.length; i++) {
                let v = rstats.vec[i];

                for (let key in stats) {
                    let stat = stats[key];

                    if (stat.startDate <= v.date && stat.endDate >= v.date) 
                        stat.vec.push(v);
                }
            }

            return stats;
        }

        checkStatsComplete(stats: any) {
            if (stats.LT && (typeof stats.LT.vec !== undefined && stats.LT.vec && stats.LT.vec.length))
                return PromiseUtil.resolvedPromise(stats);

            return this.loadCheckpoint(this.containerId)
                .then((checkpoint: Checkpoint) => {
                    return AlphaOrStrategy.fillStatsVectorFromCheckpoint(stats, checkpoint);
                });
        }

        loadRawCheckpoint(dbName: string, rawStatsType: RawStatsType) {
            if (this._rawCheckpoint)
                return PromiseUtil.resolvedPromise(this._rawCheckpoint);

            /// Otherwise load a fresh one
            return Checkpoint.load(eido.appServer.config(this.getAssetClass()), this._id, rawStatsType == RawStatsType.raw ? CheckpointType.rawPortfolio : CheckpointType.preOptPortfolio)
                .then((checkpoint: Checkpoint) => {
                    this._rawCheckpoint = checkpoint;
                    return this._rawCheckpoint;
                });
        }
            
        checkRawStatsComplete(stats: any, rawStatsType: RawStatsType) {
            if (stats.LT && (typeof stats.LT.vec !== undefined && stats.LT.vec && stats.LT.vec.length))
                return PromiseUtil.resolvedPromise(stats);
    
            return this.loadRawCheckpoint(this.containerId, rawStatsType)
                .then((checkpoint: Checkpoint) => {
                    return AlphaOrStrategy.fillStatsVectorFromCheckpoint(stats, checkpoint);
                });
        }
    
        loadRawLTStats(rawStatsType: RawStatsType, types?: string[], asArray?: boolean) {
            console.log(`[Strategy] Loading raw LTStats: ${this._id} - ${rawStatsType}`);
            // return this.loadLTStats(types, asArray, RawStatsType.raw);
            return this.dbLoadOne({
                collectionName: Collections.stats,
                dbName: this.containerId || "pesa",
                cls: null,

                query: {
                    _id: `${this._id}_${rawStatsType}_LT`
                    // iid: this._id,
                    // stype: {
                    //     $in: types
                    // }
                }
            }).then((stats: any) => {
                let ltStats: any = {
                    LT: stats
                };

                return this.checkRawStatsComplete(ltStats, rawStatsType);
            });
        }

        loadLTStats(types?: string[], asArray?: boolean, rawStatsType: RawStatsType = RawStatsType.normal) {
            if (this._stats)
                return PromiseUtil.resolvedPromise(this._stats);

            this._stats = gCache.getStats(this._id, `ltStats`);
            if (this._stats)
                return PromiseUtil.resolvedPromise(this._stats);

            let ltStats: any;

            return this.loadStats(types, asArray, rawStatsType) 
                .then((stats: any) => {
                    return this.checkStatsComplete(stats);
                })
                .then((stats: any) => {
                    this._stats = stats;

                    ltStats = stats.LT;

                    if (stats.LT) {
                        let years = ShUtil.getYearsBetweenIntDates(stats.LT.startDate, stats.LT.endDate);
                        return this.loadStats(years, true, rawStatsType);
                    }
                    else {
                        return PromiseUtil.resolvedPromise([]);
                    }
                })
                .then((annual: any[]) => {
                    /// If this is an auto alpha, then checkpoint should've been loaded already!
                    if (annual.length && (!annual[0].vec || !annual[0].indexes) && this._checkpoint)
                        AlphaOrStrategy.fillStatsVectorFromLTStats(annual, ltStats);

                    this._stats.annual = annual;

                    gCache.addStats(this._id, this, this._stats, 'ltStats');
                    return this._stats;
                });
        }

        loadRawStats(stype: string, rawStatsType: RawStatsType = RawStatsType.normal) {
            let rawStats = gCache.getStats(this._id, `rawStats-${rawStatsType}`);
            if (rawStats)
                return PromiseUtil.resolvedPromise(rawStats);

            return this.dbLoadOne({
                collectionName: Collections.stats,
                dbName: this.containerId,
                cls: null,
    
                query: {
                    _id: `${this._id}_${stype}`,
                    stype: stype
                }
            }).then((rawStats) => {
                gCache.addStats(this._id, this, rawStats, `rawStats-${rawStatsType}`);
                return rawStats;
            });
        }

        updateSettings(settings: any, operations: ConfigSection[]) {
            // let defaultOp = getDefaultOp(this.getAssetClass());

            // if (defaultOp)
            //     operations.push(defaultOp);

            let updateObj = {
                $set: {
                    modifiedDate: new Date()
                }
            };

            // updateObj["$set"] = {
            //     "portfolio.children.0.children": (operations as any).cleanClone(),
            //     modifiedDate: new Date()
            // };

            if (settings.portfolio) {
                this.portfolio.map = settings.portfolio;
                updateObj["$set"]["portfolio.map"] = settings.portfolio;
            }

            if (settings.refConfig) {
                this.refConfig = settings.refConfig;
                updateObj["$set"]["refConfig"] = settings.refConfig;
            }

            if (settings.optimise) {
                let optimiseIndex = this.portfolio.findChildIndexWithName("Optimise");

                if (optimiseIndex >= 0) 
                    updateObj["$set"][`portfolio.children.${optimiseIndex}`] = settings.optimise;
                else {
                    if (!updateObj["$push"])
                        updateObj["$push"] = {};
                    updateObj["$push"][`portfolio.children`] = settings.optimise;
                }
            }

            return this.dbUpdate(updateObj).then(() => {
                // this.portfolio.children[0].children = operations;
                return this;
            });
        }

        setOperations(operations: ConfigSection[]) {
            let defaultOp = getDefaultOp(this.getAssetClass());

            if (defaultOp)
                operations.push(defaultOp);

            return this.dbUpdate({
                $set: {
                    "portfolio.children.0.children": (operations as any).cleanClone(),
                    modifiedDate: new Date()
                }
            }).then(() => {
                this.portfolio.children[0].children = operations;
                return this;
            });
        }

        addOperation(operation) {
            return this.dbUpdate({
                $push: {
                    "portfolio.children.0.children": operation.cleanClone()
                },
                $set: {
                    modifiedDate: new Date()
                }
            }).then(() => {
                this.portfolio.children[0].children.push(operation);
                return this;
            });
        }

        deleteOperation(uuid) {
            return this.dbUpdate({
                $pull: {
                    "portfolio.children.0.children": {
                        "map.uuid": uuid
                    }
                },
                $set: {
                    modifiedDate: new Date()
                }
            }).then(() => {
                let opIndex = this.findOperation(uuid);
                
                if (opIndex >= 0)
                    this.portfolio.children[0].children.splice(opIndex, 1);

                return this;
            });
        }

        findOperation(uuid) { 
            for (let i = 0; i < this.portfolio.children[0].children.length; i++) {
                let operation = this.portfolio.children[0].children[i];
                if (operation.map.uuid === uuid)
                    return i;
            }

            return -1;
        }

        hasOperation(uuid) {
            let opIndex = this.findOperation(uuid);
            return opIndex < 0 ? null : this.portfolio.children[0].children[opIndex];
        }

        loadStats(types: string[] = StatsTypes, asArray: boolean = false, rawStatsType: RawStatsType = RawStatsType.normal) {
            console.log(`[SV_Config] Loading stats for id: ${this.containerId}@${this._id}${rawStatsType} - Types: ${types}`);
            let typesStr = types.join(',');
    
            /// Check the cache first ...
            let allStats = gCache.getStats(this._id, `allStats-${typesStr}`);
            if (allStats)
                return PromiseUtil.resolvedPromise(this.keyStats);

            let ids = new Array(types.length);
            types.map((type: string, i: number) => ids[i] = `${this._id}_${type}`);

            return this.dbLoad({
                collectionName: Collections.stats,
                dbName: this.containerId,
                cls: null,
    
                query: {
                    _id: {$in: ids}
                    // iid: this._id,
                    // stype: {
                    //     $in: types
                    // }
                }
            }).then((stats: any[]) => {
                if (asArray)
                    return stats;

                let allStats = {};
                stats.map((stat, i) => { 
                    if (stat)
                        allStats[stat.stype] = stat;
                    else 
                        console.info(`[SV_Config] No stats loaded for: ${types[i]} - ID: ${this._id}`);
                });

                gCache.addStats(this._id, this, allStats, `allStats-${typesStr}`);

                return allStats;
            });
        }
    
        loadKeyStats(keyStatsTable: string) {
            /// Check the cache ...
            if (this.keyStats)
                return PromiseUtil.resolvedPromise(this);

            /// Check the cache first ...
            this.keyStats = gCache.getStats(this._id, 'keyStats');
            if (this.keyStats)
                return PromiseUtil.resolvedPromise(this);

            console.log(`[AlphaOrStrategy] Getting key stats for alpha: ${this._id}`);
            return CObject.dbLoadOne({
                collectionName: Collections.stats,
                dbName: this.containerId,
                cls: null,

                query: {
                    iid: this._id,
                    stype: keyStatsTable//`${this._id}_${keyStatsTable}`
                }
            })
            .then((keyStats) => {
                gCache.addStats(this._id, this, keyStats, 'keyStats');
                this.keyStats = keyStats; 
                return this;
            });
        }

        loadPosInfo(): Promise<any> {
            if (this._posInfo)
                return PromiseUtil.resolvedPromise(this._posInfo);

            console.log(`[Config] Loading posInfo: ${this._id}`);
            let market: Market = DataCache.instance.getMarket(this.market);
            Debug.assert(market, `Unable to load market: ${this.market}`);

            /// Now get the universe
            let universe: Universe = market.getUniverse(this.getUniverseId()) as Universe;
            Debug.assert(universe, `Unable to load universe: ${this.market}.${this.getUniverseId()}`);

            return this.loadRawStats("LT")
                .then((stats) => {
                    this._posInfo = {
                        tickers: universe.tickers,
                        cfigis: universe.uuids,
                        universeId: this.getUniverseId(),
                        market: this.market,
                        dates: market.getSimDatesBetween(stats.startDate, stats.endDate)
                    };

                    return this._posInfo;
                });
        }
    
        loadRawDataForDateDB(collectionName: string, date: number) {
            return this.dbLoadOne({
                dbConn: db.Manager.instance.getBlobConn(new db.ConnArgs(null, this.dbName)),
                collectionName: collectionName,
                dbName: this.containerId,
                cls: null,
    
                query: {
                    _id: this._id.toString() + "_" + date
                }
            });
        }

        loadRawDataForDate(date: number): Promise<any> {
            let sconfig: ServerConfig = eido.appServer.config(this.getAssetClass());

            /// TODO: replace this back with blob downloading logic
            return Util.getIdFileData(sconfig.posContainerId, this._id, `_${date}`, true)
                .then((buffer: Buffer) => {
                    return PosData.parse(buffer);
                });
        }

        prune() {
            let years = ["2008", "2009", "2010", "2011", "2012", "2013", "2014", "2015", "2016", "2017", "2018", "2019", "2020"];
            let stypes = ["LT", "D5", "D10", "D21", "D63", "D120", "D250", "IS", "OS"];

            years.forEach((year) => {
                stypes.push(year);
                stypes.push(`${year}_QT01`);
                stypes.push(`${year}_QT02`);
                stypes.push(`${year}_QT03`);
                stypes.push(`${year}_QT04`);
            });

            let promises = new Array(stypes.length);

            stypes.map((stype, si) => {
                let promise = CObject.dbBulkDelete({
                    dbName: DB.pesa,
                    collectionName: Collections.stats,
                    query: {
                        iid: this._id,
                        stype: stype
                    }
                });

                promises[si] = promise;
            })

            /// First of all we need to delete the stats of this alpha
            return promises[0].then(() => {
                return this.dbUpdate({
                    $set: {
                        pruned: true
                    }
                });
            });
        }
    
        loadPositionsForDate(date: number): Promise<Positions> {
            let data = {
                date: date,
                info: null,
                notionals: [],
                prices: [],
                shares: [],
                pssNotionals: [],
                pssTurnovers: [],
                pssClosePnls: [],
                pssCosts: []
            };
    
            return this.loadPosInfo()
                .then((posInfo: any) => {
                    data.info = posInfo;
                    // data.info.date = data.date;
                    // data.dateIndex = posInfo.dates.findIndex(date);
    
                    // if (data.dateIndex < 0)  
                    //     throw new Exception(`Invalid date requested that has no positional data for it: ${data.date}`);
    
                    /// OK now that we have it ... we need to load the notional data
                    return this.loadRawDataForDate(date); ///Promise.all([this.loadPosNotionals(date), this.loadPosPrices(date), this.loadNumShares(date)]);
                })
                .then((posData: any) => {
                    data.notionals      = posData.posNotionals && posData.posNotionals.length ? posData.posNotionals.data : [];
                    data.prices         = posData.posPrices && posData.posPrices.length ? posData.posPrices.data : [];
                    data.shares         = posData.posShares && posData.posShares.length ? posData.posShares.data : [];
                    data.pssNotionals   = posData.pssNotionals && posData.pssNotionals.length ? posData.pssNotionals.data : [];
                    data.pssClosePnls   = posData.pssClosePnls && posData.pssClosePnls.length ? posData.pssClosePnls.data : [];
                    data.pssTurnovers   = posData.pssTurnovers && posData.pssTurnovers.length ? posData.pssTurnovers.data : [];
                    data.pssCosts       = posData.pssCosts && posData.pssCosts.length ? posData.pssCosts.data : [];

                    /// If we've loaded all the necessary information, then we can calculate the positions 
                    if (data.notionals.length && data.prices.length && data.shares.length) {
                        let positions   = new Positions();

                        /// Calculate the position information ...
                        positions.loadObject([data]);
                        positions.loadObjectPSS(data);
                        positions.calculateQuantiles();
                        positions.calculatePerfs();
                            
                        return positions;
                    }
                    
                    /// Just return empty positions ...
                    return new Positions();
                });
        }
    
        //////////////////////////////////////////////////////////////////////////////////////////////////////
        runPostPublishTests() {
            return (new PublishTests()).run(this);
        }
        
        //////////////////////////////////////////////////////////////////////////////////////////////////////
        /// Check correlation of one against another ...
        checkCorrelation(other: AlphaOrStrategy) {
            return Promise.all([this.getStatsPnlStream(), other.getLivePnlStream()])
                .then((pnls) => {
                    let lhs = pnls[0];
                    let rhs = pnls[1];
                    let correlation = require('node-correlation');

                    return ShUtil.datedCorrelation(correlation.calc, lhs.pnls, lhs.dates, rhs.pnls, rhs.dates);
                });
        }

        //////////////////////////////////////////////////////////////////////////////////////////////////////
        /// Static functions
        //////////////////////////////////////////////////////////////////////////////////////////////////////
        static async duplicate(ownerId: ObjectID, iid: ObjectID, name: string, category: string): Promise<AlphaOrStrategy> {
            return CObject.dbLoadOne({
                collectionName: Collections.userConfigs,
                dbName: DB.pesa,
                cls: null,
    
                query: {
                    _id: iid
                }
            })
                .then((existingObj: any) => {
                    
                    /// Create a new object id
                    existingObj._id = ObjectID.create();
                    existingObj.name = name;
                    existingObj.category = category;
                    existingObj.isPublished = false;
                    existingObj._copiedFrom = iid.toString();

                    if (!existingObj.isMaster)
                        existingObj.ownerId = ownerId;

                    existingObj.portfolio.map.iid = existingObj._id.toString();

                    if (existingObj.type == AlphaType.strategy)
                        existingObj.portfolio.children[0].map.uuid = `Combo_${existingObj._id}`;
                    else {
                        existingObj.portfolio.children.map((child, cindex) => {
                            if (typeof(child.iid) !== 'undefined' && child.iid)
                                child.iid = child.uuid = existingObj._id.toString();
                        });
                    }

                    let conn = db.Manager.instance.getObjectConn(new db.ConnArgs(null, DB.pesa));
                    let coll = conn.collection(Collections.userConfigs);

                    return new Promise<any>((resolve, reject) => {
                        coll.insertOne(existingObj, (err) => {
                            if (err)
                                return reject(err);
                            return resolve(existingObj._id);
                        });
                    });
                })
                .then((id: ObjectID) => {
                    let alpha = new AlphaOrStrategy();
                    return alpha.loadAny(id, DB.pesa);
                })
        }

        static makeStatsQuery(market: string, universeId: string, stype: string, ownerId: ObjectID, configType: ConfigType, type: string, delay: number, isPublished: boolean, advSearches: any[]): any {
            Debug.assert(market, "makeQuery: Invalid market passed!");

            let query: any = {
                $and: [
                    { type: type },
                    { stype: stype },
                    // { isPublished: isPublished },
                    { market: market },
                    { delay: delay },
                    {
                        $or: [{
                            isPublished: isPublished
                        }, {
                            ownerId: gDefaultUserId
                        }]
                    }
                ]
            };

            if (configType)
                query["$and"].push({ configType: configType });

            if (ownerId) 
                query["$and"].push({ ownerId: ownerId });

            if (universeId && universeId != "*" && universeId != "ALL")
                query["$and"].push({universeId: universeId});

            if (advSearches) {
                advSearches.map((advSearch: any) => {
                    let search = {};
                    search[advSearch.name] = {};
                    search[advSearch.name][advSearch.op] = parseFloat(advSearch.value);
                    query["$and"].push(search);
                });
            }

            return query;
        }

        static async getTotalFeedCount(market: string, universeId: string, stype: string, ownerId: ObjectID, configType: ConfigType, type: string, delay: number, isPublished: boolean, dbName: string, advSearches: any[]): Promise<number> {
            Debug.assert(market, "Alpha.getTotalFeedCount: Invalid market passed!");

            let query: any = AlphaOrStrategy.makeStatsQuery(market, universeId, stype, ownerId, configType, type, delay, isPublished, advSearches);

            return new Promise<number>((resolve, reject) => {
                let conn = db.Manager.instance.getObjectConn(new db.ConnArgs(null, dbName));
                let collection = conn.collection(Collections.stats);
                collection.find(query).count((err, count) => !err ? resolve(count) : reject(err));
            });
        }

        static async getAlphaFeed<T extends AlphaOrStrategy>(ClassType: { new(): T ;}, args: ServerArgs, market: string, universeId: string, startId: number, count: number, sortBy: string, 
            sortOrder: SortOrder, ownerId: ObjectID, dbName: string, type: string, configType: ConfigType, delay = 1, isPublished: boolean = true,
            searchView: string = "LT", advSearches: any[] = []): Promise<AlphaFeed<AlphaOrStrategy>> {
            // let { ownerId, market, universeId, startId, count, sortBy, sortOrder } = args;
            let query: any = AlphaOrStrategy.makeStatsQuery(market, universeId, searchView, ownerId, configType, type, delay, isPublished, advSearches);
    
            let search: any = {
                dbName: dbName,
                collectionName: Collections.stats,
                query: query,
                cls: null
            };
    
            if (startId)
                search["$skip"] = startId;
    
            search["$limit"] = count;
    
            if (sortBy) {
                console.log(`[AlphaOrStrategy] Sorting feed by: ${sortBy}:${sortOrder}`);
                search.sort = {};
                search.sort[sortBy] = sortOrder == SortOrder.desc ? -1 : 1;
            }
    
            console.log(`[AlphaOrStraegy] Feed range. Start Index: ${startId}, Count: ${count}, DB: ${dbName}, UniverseId: ${universeId}, Query: ${JSON.stringify(search)}`);
    
            let feed: AlphaFeed<T> = new AlphaFeed<T>(market, universeId);
    
            // let conn = db.Manager.instance.getObjectConn(new db.ConnArgs(null, dbName));
            // let coll = conn.collection(Collections.stats);
    
            // return coll.aggregate();
    
            return AlphaOrStrategy.dbLoad(search)
                .then((keyStats: LTStats[]) => {
                    let promises = new Array(keyStats.length);
    
                    keyStats.map((keyStats, ki) => {
                        let inst: T         = new ClassType();
                        promises[ki] = inst.loadId(keyStats.iid, new AlphaOrStrategyLoadOptions(true, keyStats, dbName, configType));
                    });
    
                    return Promise.all(promises);
                })
                .then(async (alphas: T[]) => {
                    feed.alphas     = alphas;
                    feed.startId    = startId;
                    feed.count      = count;
                    feed.alphas     = alphas;
    
                    feed.totalCount = await AlphaOrStrategy.getTotalFeedCount(market, universeId, searchView, ownerId, configType, type, delay, isPublished, dbName, advSearches);

                    console.log(`[AlphaOrStrategy] Total count: ${feed.totalCount}`);
                    return feed;
                });
        }
    
    
    };
