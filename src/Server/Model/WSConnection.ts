import Signal from 'Shared/Core/Signals';
import { Model } from 'Shared/Model/ShModel';
import { SecrecyLevel } from 'Shared/Model/SecrecyLevel';
import { Collections } from 'Server/Model/SvModel';
import { CObject } from 'Shared/Core/CObject';
import { ShUtil, Debug, PromiseUtil } from 'Shared/Core/ShUtil';
import { db } from 'Server/Core/DB/DBManager'
import { eido } from 'Shared/Core/ShCore';
import { ObjectID } from 'Shared/Core/ObjectID';
import { ModelObject, ModelValue, ModelClassId } from 'Shared/Core/BaseObject';

@ModelObject()
export class WSConnection extends CObject {
    @ModelClassId(WSConnection, "WSConnection") __dummy: string;
    
    static get collectionName() { return Collections.wsConnection; }

    static s_wsConnect: any;
    static s_wsDisconnect: any;
    static s_wsMessage: any;

    @ModelValue("The DB identifier.", SecrecyLevel.minimal)
    _id: string = "";
    
    @ModelValue("The user identifier.", SecrecyLevel.minimal)
    iid: ObjectID = ObjectID.create();
    
    @ModelValue("The server IP.", SecrecyLevel.minimal)
    serverIP: string = "";
    
    @ModelValue("The server port.", SecrecyLevel.minimal)
    serverPort: number = 0;
    
    @ModelValue("The server PID.", SecrecyLevel.minimal)
    serverPid: number = 0;
    
    @ModelValue("The last time this was updated.", SecrecyLevel.minimal)
    lastUpdated: Date = new Date();

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /// Not part of the model
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ws: any = null;
    
    constructor() {
        super();
    }

    getId() {
        return this._id;
    }

    load(id) {
        this._id = `ws_${id}`;
        this.iid = ObjectID.create(id);
        return this.dbLoadOne();
    }
    
    create(info, ws) {
        Debug.assert(ws, `[WSSConnection] Must pass a valid WS object to create a WSConnection. ID: ${ws.id}`);

        this._id = `ws_${ws.id}`;
        this.iid = ObjectID.create(ws.id);
        this.serverIP = info.serverIP;
        this.serverPid = process.pid;
        this.serverPort = info.serverPort;

        return this.dbUpdate({ upsert: true })
            .then(() => {
                console.trace('[WS] Firing wssConnect event ...');
                this.ws = ws;
                WSConnection.s_wsConnect.dispatch(this);
                return this;
            });
    }

    delete(id) {
        this._id = `ws_${id}`;

        return this.dbDelete()
            .then(() => {
                console.trace('[WS] Firing wssDisconnect event ...');
                this.ws = eido.appServer.wsServer.getWebSocket(this._id.toString());

                if (!this.ws) 
                    console.warn('WSSDisconnect: Unable to get the websocket connection for id:', this._id.toString());
                
                WSConnection.s_wsDisconnect.dispatch(this);
                return this;
            });
    }

    // static loadAll(ids) {}

    static updateAll(serverIP, serverPort, timestamp) {
        let conn = db.Manager.instance.getObjectConn();
        let collection = conn.collection(WSConnection.collectionName);

        collection.updateMany({
            serverIP: serverIP,
            serverPort: serverPort,
            serverPid: process.pid
        }, {
            $set: {
                lastUpdated: timestamp
            }
        }, {
            upsert: false
        }, (err) => {
            if (err)
                console.error('[WS] DB Error: ', err);
        });
    }
}

export default function init() {
    WSConnection.s_wsConnect    = new Signal();
    WSConnection.s_wsDisconnect = new Signal();
    WSConnection.s_wsMessage    = new Signal();
    
    // initialize the indexes!
    let conn = db.Manager.instance.getObjectConn();
    let collection = conn.collection(WSConnection.collectionName);

    // collection.createIndex({ _id: 1 });

    // collection.createIndex({
    //     serverIP: 1,
    //     serverPort: 1,
    //     serverPid: 1
    // });

    // collection.createIndex({
    //     lastUpdated: 1
    // }, {
    //     expireAfterSeconds: 60 * 10 // every 10 minutes we drop the connections from the DB
    // });
}
