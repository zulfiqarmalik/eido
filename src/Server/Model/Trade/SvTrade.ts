import { ModelObject, ModelClassId, ModelValue } from "Shared/Core/BaseObject";
import { ShTrade, ShWave, TradeType } from "Shared/Model/Trade/ShTrade"
import { PromiseUtil, ShUtil } from "Shared/Core/ShUtil";
import moment from "moment-timezone";
import { MarketTimezones } from "Shared/Model/Config/ShAlpha";
import { DB as DBList, Collections, DB } from 'Server/Model/SvModel';

////////////////////////////////////////////////////////////////////////////////////
/// Trade: The main trade object
////////////////////////////////////////////////////////////////////////////////////
@ModelObject()
export class Trade extends ShTrade {
    @ModelClassId(Trade, "Trade") __dummy: string;

    static get collectionName() { return Collections.trades; }
    static get dbName() { return DBList.eido; }
    get dbName() { return DBList.eido; }
    
    _market: string; 
    _sdate: string;

    load(market: string, type: TradeType, date: Date = null): Promise<Trade> {
        this.type = type;
        this._market = market;

        let mdate = moment();

        /// If no date is given then we just assume the current date ...
        if (!date) {
            let tz = MarketTimezones[this._market];
            mdate = moment(new Date()).tz(tz);
        } 
        else 
            mdate = moment(date);

        this._sdate = ShUtil.dateInfoToString(mdate.year(), mdate.month() + 1, mdate.date(), "");
        this._id = `${this._market}_${this.type}_${this._sdate}`;

        /// Avtually load the object ...
        return this.dbLoadOne({
            query: {
                _id: this._id
            }
        }).then(() => {
            return this;
        })
    }
};

////////////////////////////////////////////////////////////////////////////////////
/// Wave: The wave
////////////////////////////////////////////////////////////////////////////////////
@ModelObject()
export class Wave extends ShWave {
    @ModelClassId(Wave, "Wave") __dummy: string;

    static get collectionName() { return Collections.trades; }
    static get dbName() { return DBList.eido; }
    get dbName() { return DBList.eido; }
    
    load(id: string): Promise<Wave> {
        this._id = id;

        return this.dbLoadOne({
            query: {
                _id: this._id
            }
        }).then(() => {
            return this;
        })
    }
};


export default function init() {
    console.info("[Model] Init Trade");
    new Wave();
    new Trade();
}
