import { Model, ModelFactory, ModelProperties } from 'Shared/Model/ShModel';
import { SecrecyLevel } from 'Shared/Model/SecrecyLevel';
import { Debug } from 'Shared/Core/ShUtil';
import { eido } from 'Shared/Core/ShCore';
import LRU from 'lru-cache';

export let Params = {
    query: function(name, description, type, required, allowableValuesEnum, defaultValue) {
        if (allowableValuesEnum)
            allowableValuesEnum = Object.keys(allowableValuesEnum);

        Debug.assert(!allowableValuesEnum || allowableValuesEnum instanceof Array, 'Invalid enum passed!');
        
        return {
            "name": name,
            "description": description,
            "type": type,
            "required": required,
            "enum": allowableValuesEnum,
            "defaultValue": defaultValue,
            "paramType": "query",
            "isUserClass": Model.getClass(type) ? true : false
        };
    },

    field: function(name, description, type, required, allowableValuesEnum, defaultValue) {
        if (allowableValuesEnum)
            allowableValuesEnum = Object.keys(allowableValuesEnum);
        
        Debug.assert(!allowableValuesEnum || allowableValuesEnum instanceof Array, 'Invalid enum passed!');

        return {
            "name": name,
            "description": description,
            "type": type,
            "required": required,
            "enum": allowableValuesEnum,
            "defaultValue": defaultValue,
            "paramType": "query",
            "internalParamType": "field",
            "isUserClass": Model.getClass(type) ? true : false
        };
    },

    arrayField: function(name, description, type, required) {
        return {
            "name": name,
            "description": description,
            "type": "array",
            "items": {
                $ref: type
            },
            "required": required,
            "defaultValue": [],
            "paramType": "query",
            "internalParamType": "field",
            "isUserClass": Model.getClass(type) ? true : false
        };
    },

    path: function(name, description, type, allowableValuesEnum, defaultValue) {
        if (allowableValuesEnum)
            allowableValuesEnum = Object.keys(allowableValuesEnum);
        
        Debug.assert(!allowableValuesEnum || allowableValuesEnum instanceof Array, 'Invalid enum passed!');

        return {
            "name": name,
            "description": description,
            "type": type,
            "required": true,
            "enum": allowableValuesEnum,
            "paramType": "path",
            "defaultValue": defaultValue,
            "isUserClass": Model.getClass(type) ? true : false
        };
    },

    body: function(name, description, type, defaultValue, notRequired) {
        return {
            "name": name,
            "description": description,
            "type": type,
            "required": !notRequired,
            "paramType": "body",
            "defaultValue": defaultValue,
            "isUserClass": Model.getClass(type) ? true : false
        };
    },

    form: function(name, description, type, required, allowableValuesEnum, defaultValue) {
        if (allowableValuesEnum)
            allowableValuesEnum = Object.keys(allowableValuesEnum);
        
        Debug.assert(!allowableValuesEnum || allowableValuesEnum instanceof Array, 'Invalid enum passed!');

        return {
            "name": name,
            "description": description,
            "type": "string",
            "required": (typeof required !== 'undefined') ? required : true,
            "enum": allowableValuesEnum,
            "paramType": "form",
            "defaultValue": defaultValue,
            "isUserClass": Model.getClass(type) ? true : false
        };
    },

    header: function(name, description, type, required) {
        return {
            "name": name,
            "description": description,
            "type": type,
            "required": required,
            "allowMultiple": false,
            "paramType": "header",
            "isUserClass": Model.getClass(type) ? true : false
        };
    },

    translateType: function(paramType, internalParamType) {
        if (paramType == 'query')
            return internalParamType && internalParamType === 'field' ? 'body' : 'qs';
        return paramType;
    }
}

// Model.restParams = function(prefix, type, paramType, required, exculdedList, includedList) {
//     Debug.assert(type.__modelProperties, 'Invalid model class:', type);

//     var mprops = type.__modelProperties;
//     var params = [];
    
//     if (!exculdedList)
//         exculdedList = {};
        
//     var args = [
//         prefix, 
//         prefix,
//         type.__classId,
//         required
//     ];
    
//     var mainParam = paramType.apply(null, args);
//     mainParam.isUserContainerClass = true;
//     mainParam.isUserClass = true;

//     params.push(mainParam);
    
//     for (var i = 0; i < mprops.names.length; i++) {
//         var name = mprops.names[i];
//         var prop = mprops.props[name];

//         // we exculde the items that are there in the exculded list!
//         if (exculdedList[name] === false || name === "_id")
//             continue;
            
//         if (includedList && includedList[name] !== true)
//             continue;
        
//         // if this is a user class then we get the parameters for that as well
//         if (prop.isUserClass) {
//             var uparams = Model.restParams(prefix + '.' + name, Model.classes[prop.type], paramType, required);

//             for (var i = 0; i < uparams.length; i++)
//                 params.push(uparams[i]);

//             continue;
//         }
        
//         var args = [
//             prefix + '.' + name, 
//             prop.desc || prop.description,
//             prop.type,
//             required
//         ];
        
//         if (prop.enum) 
//             args.push(prop.enum.values());
//         else
//             args.push(null);
            
//         if (prop.value != null && prop.value != undefined)
//             args.push(prop.value);
            
//         var param = paramType.apply(null, args);
        
//         if (prop.type == 'array') {
//             param.items = {
//                 $ref: prop.items.type
//             };
//         }
        
//         param.nested = true;
        
//         params.push(param);
//     }

//     return params;
// }

export let DB = {
    eido:           'pesa',
    pesa:           'pesa',
    pesaUser:       'pesa',
    
    pesaUSAuto:     'pesa',
    pesaEUAuto:     'pesa',
    // pesaAutoEEU:    'pesa_auto_eeu',
    // pesaAutoJP:     'pesa_auto_jp',
    // pesaAutoAPAC:   'pesa_auto_apac',
    // pesaAutoAU:     'pesa_auto_au',
    // pesaAutoLATAM:  'pesa_auto_latam',

    pesaUSUser:     'pesa',
    pesaEUUser:     'pesa',
    pesaUS:         'pesa',
    pesaEU:         'pesa',
    // pesaEEU:        'pesa_eeu',
    // pesaJP:         'pesa_jp',
    // pesaAPAC:       'pesa_apac',
    // pesaAU:         'pesa_au',
    // pesaLATAM:      'pesa_latam',

    pesaSubUS:      'pesa',
    pesaSubEU:      'pesa',
    // pesaSubEEU:     'pesa_eeu_sub',
    // pesaSubJP:      'pesa_jp_sub',
    // pesaSubAPAC:    'pesa_apac_sub',
    // pesaSubAU:      'pesa_au_sub',
    // pesaSubLATAM:   'pesa_latam_sub',

    pesaSuperUS:    'pesa',
    pesaSuperEU:    'pesa',
    // pesaSuperEEU:   'pesa_eeu_super',
    // pesaSuperJP:    'pesa_jp_super',
    // pesaSuperAPAC:  'pesa_apac_super',
    // pesaSuperAU:    'pesa_au_super',
    // pesaSuperLATAM: 'pesa_latam_super',

    // Futures Related
    pesaUSFut:      'pesa'
};

export let StatsTypes: string[] = [
    "LT", "D5", "D10", "D21", "D63", "D120", "D250", "IS", "OS"
];
 
export let Collections = {
    sys:            'Users',
    users:          'Users',
    userPrefs:      'Users',
    sessions:       'Users',
    wsConnection:   'Users',
    servers:        'Users',

    tasks:          'Users',
    taskResults:    'Users',

    alpha:          'Configs',
    strategy:       'Configs',
    userConfigs:    'Configs',

    trades:         'Users',

    // publishTests:   'PublishTests',

    // thumbnailsLT:   'ThumbnailsLT',
    // thumbnailsIS:   'ThumbnailsIS',
    // thumbnailsOS:   'ThumbnailsOS',
    // thumbnailsPS:   'ThumbnailsPS',

    // livePnl:        'Stats',
    stats:          'Stats',

    // posInfo:        'POS_Info',
    // posNotionals:   'POS_Notionals',
    // posPrices:      'POS_Prices',
    // posShares:      'POS_Shares',

    // pssNotionals:   'PSS_Notionals',
    // pssAlpha:       'PSS_Alphas',
    // pssTurnover:    'PSS_Turnovers',
    // pssClosePnl:    'PSS_ClosePnls',
    // pssCosts:       'PSS_Costs',
    
    // chat:           'chat',
    // events:         'events',
    // images:         'images',
    // sessions:       'sessions',
    // notifications:  'notifications',
    // company:        'company',
    // group:          'group',
    // accounts:       'accounts',
    // history:        'history',
    // quoteRequest:   'quote_request',
    // quote:          'quote',
};

import initSession from './User/SvSession';
import initUser from './User/SvUser';
import initConfig from 'Shared/Model/Config/Config';
import initStrategy from 'Server/Model/Config/SvStrategy';
import initStats from 'Shared/Model/Stats/Stats';
import initCumStats from 'Shared/Model/Stats/CumStats';
import initLTStats from 'Shared/Model/Stats/LTStats';
import initPositions from 'Shared/Model/Stats/Positions';
import initWSConnection from 'Server/Model/WSConnection';
import initNotifications from 'Shared/Model/Notification';
import initTask from 'Server/Model/Task/SvTask';
import initGroupPnl from 'Shared/Model/GroupPnl';
import initShData from 'Shared/Model/ShData'
import initTrade from 'Server/Model/Trade/SvTrade';
import { ObjectID } from 'Shared/Core/ObjectID';

export default function initModels(app) {
    console.info("[Model] Initialising server side models!");

    var mongo       = require('mongodb');

    initSession(app);
    initUser(app);
    initConfig();
    initStrategy(app);
    
    initPositions();
    initStats();
    initCumStats();
    initLTStats();

    initWSConnection();
    initNotifications();
    initTask(app);

    initGroupPnl();
    initShData();

    initTrade();

    console.info('[Model] Initializing models ...');

    /// TODO: add models to swagger
    // var models = Model.getAllModels();

    // app.swagger.addModels({
    //     models: models
    // });

    console.log('[Model] Models initialized and added to swagger');
}

export function getComponentDocs() {
    let schemas = {};
    let keys: string[] = Object.keys(Model.modelProperties);

    keys.map((name, index) => {
        let mprops: ModelProperties = Model.modelProperties[name];
        schemas[name] = mprops.getApiDocs();
    });

    return schemas;
}

export let gDefaultUserId = ObjectID.create("deadbeefdeadbeefdeadbeef");


export class ObjectCache {
    stats           = new LRU(5000);
    alphas          = new LRU(5000);

    addStats(id: ObjectID, alpha: any, stats: any, type: string = '') {
        if (!alpha.isPublished)
            return;
        this.stats.set(`${id.toString()}-${type}`, stats);
    }

    getStats(id: ObjectID, type: string = ''): any {
        // return this.stats.get(`${id.toString()}-${type}`);
        return null;
    }

    addAlpha(id: ObjectID, alpha: any) {
        /// If it isn't published then just return
        if (!alpha.isPublished)
            return;
        this.alphas.set(id.toString(), alpha);
    }

    getAlpha(id: ObjectID): any {
        // return this.alphas.get(id.toString());
        return null;
    }
}

export let gCache   = new ObjectCache();


// module.exports = function(app) {
//     if (!app.swagger)
//         return;

//     var mongo = require('mongodb');
//     eido.ObjectID = mongo.ObjectID;
//     eido.MLong = mongo.Long;

//     // shared core helpers
//     if (!app.isTms()) {
//         eido.require([
//             // core modules ...
//             '@sh_model/sh_m_geocoords.js',
//             '@sh_model/sh_m_geoposition.js',
//             '@sh_model/sh_m_range.js',
//             '@shared/core/geonode.js',
//             '@shared/core/geosector.js',
//             '@shared/core/world.js',

//             // server side models ...
//             './user/sv_m_device.js',
//             './sv_m_image.js',
//             './chat/sv_m_chat_message.js',
//             './chat/sv_m_chat.js',
//             './sv_m_ws_connection.js',
//             './event/sv_m_event_address.js',
//             './event/sv_m_event_details.js',
//             './event/sv_m_event_timeline.js',
//             './event/sv_m_event.js',
//             './user/sv_m_user.js',
//             './user/sv_m_session.js',
//             './user/sv_m_notification.js',

//             '@sh_model/event/sh_m_event_search.js'
//         ]);
//     }
//     else {
//         eido.require([
//             // core modules ...
//             '@sh_model/sh_m_geocoords.js',
//             '@sh_model/sh_m_geoposition.js',
//             '@sh_model/sh_m_range.js',
//             '@shared/core/geonode.js',
//             '@shared/core/geosector.js',
//             '@shared/core/world.js',

//             // server side models ...
//             './user/sv_m_device.js',
//             './sv_m_image.js',
//             './chat/sv_m_chat_message.js',
//             './chat/sv_m_chat.js',
//             './sv_m_ws_connection.js',

//             // TMS specific
//             './tms/sv_m_tms_object.js',
//             './tms/sv_m_tms_action.js',
//             './tms/sv_m_tms_group.js',
//             './tms/sv_m_tms_company.js',
//             './tms/sv_m_tms_account.js',
//             './tms/sv_m_tms_user.js',
//             './tms/sv_m_tms_quote_request.js',
//             './tms/sv_m_tms_quote.js',
//             './user/sv_m_session.js',
//             // './user/sv_m_notification.js',
//         ]);
//     }

//     console.log('[Model] Initializing models ...');

//     var models = Model.getAllModels();

//     app.swagger.addModels({
//         models: models
//     });

//     console.log('[Model] Models initialized and added to swagger');
// }
