import { Model } from 'Shared/Model/ShModel';
import { UserPreferences as ShUserPreferences } from 'Shared/Model/User/ShUserPreferences';
import { ModelObject, ModelValue, ModelClassId } from 'Shared/Core/BaseObject';
import { DB as DBList, Collections } from '../SvModel';

@ModelObject()
export class UserPreferences extends ShUserPreferences {
    @ModelClassId(UserPreferences, "UserPreferences") __dummy: string;

    static get collectionName() { return Collections.userPrefs; }
    static get dbName() { return DBList.eido; }
    get dbName() { return DBList.eido; }
    
    constructor() {
        super();
    }

    create(userId) {
        this._id = userId;
        return this.dbInsert();
    }

    load(args) {
        return this.dbLoadOne({ _id: args.userId() });
    }

    addAlpha(alpha) {
        // alphaInfo           = new AlphaInfo();
        // alphaInfo.id        = alpha._id;
        // alphaInfo.market    = alpha.market;

        // return this.dbUpdate({
        //     $push: {
        //         ownAlphas: alphaInfo
        //     }
        // });
    }

    removeAlpha(id) {
        return this.dbUpdate({
            $pull: {
                "ownAlphas.id": id
            }
        });
    }
};

export default function init(app) {
    console.info("[Model] Init SvUserPreferences");

    // initialize the model!
    new UserPreferences();
}
