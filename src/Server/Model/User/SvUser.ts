import { Model } from 'Shared/Model/ShModel';
import { DB as DBList, Collections, DB } from 'Server/Model/SvModel';
import { Debug, PromiseUtil, ShUtil } from 'Shared/Core/ShUtil';
import { Util } from 'Server/Core/SvUtil';
import { db } from 'Server/Core/DB/DBManager';
import { ShUser, AccountType, AccountStatus, Gender, UserLevel } from 'Shared/Model/User/ShUser';
import { Email } from 'Server/System/Email';
import { Session } from './SvSession';
import { Error } from 'Server/Core/Error';
import { eido } from 'Shared/Core/ShCore';
import Guid from 'Shared/Core/Guid';
import { ModelObject, ModelValue, ModelClassId } from 'Shared/Core/BaseObject';
import { SecrecyLevel } from 'Shared/Model/SecrecyLevel';
import { ObjectID } from 'Shared/Core/ObjectID';
import { ServerArgs } from '../../Core/ServerArgs';
import { CObject } from 'Shared/Core/CObject';

let gDefaultAdminEmail = "admin@quanvolve.com";

@ModelObject()
export class User extends ShUser {
    @ModelClassId(User, "User") __dummy: string;
    
    static get collectionName() { return Collections.users; }
    static get dbName() { return DBList.eido; }
    get dbName() { return DBList.eido; }

    @ModelValue("User password.", SecrecyLevel.server)
    password: string = "";

    @ModelValue("What is the type of the account.", SecrecyLevel.server)
    accountType: AccountType = AccountType.eido;
    
    constructor() {
        super();
    }

    load(args) {
        let email = args.email ? args.email : args.deviceId + '@eido.com';

        return this.dbLoadOne({
            query: {
                email: email,
                accountType: AccountType.eido
            }
        }).then((user) => {
            if (!user)
                return null;

            console.log(`[User] Authenticating user with password: ${args.email}`);
            // verify the password
            let password = User.digestPassword(args.email, args.password);
            args._error.assert(password === user.password, 403, 'Access Denied: Invalid password');

            console.log(`[User] Password authentication successful: ${args.email}`);
            
            return user;
        });
    }

    loadSetting(args: ServerArgs, key: string, defaultValue: string) {
        return this.loadSettings(args)
            .then((settings: any) => {
                let value: string;

                if (settings) {
                    value = ShUtil.getValue(settings, key, 0, "");
                }

                /// If we have a key then just load that, otherwise just use the default
                if (value)
                    return value;
                    
                /// Write this value to the DB (don't wait for it)
                this.saveSetting(args, key, defaultValue);

                return defaultValue;
            });
    }

    saveSetting(args: ServerArgs, key: string, value: string) {
        let updateObj: any = {};
        updateObj[key] = value;

        console.log(`[User] Writing setting: ${this._id}.${key} = ${value}`);

        return CObject.dbUpdate({
            _id: this._id,
            dbName: User.dbName,
            collectionName: Collections.userPrefs,
            cls: null,
            $set: updateObj
        });
    }

    loadSettings(args: ServerArgs) {
        return CObject.dbLoadOne({
            _id: this._id,
            dbName: User.dbName,
            cls: null,
            collectionName: Collections.userPrefs
        });
    }

    // loadPrefs(args) {
    //     if (this.prefs)
    //         return PromiseUtil.resolvedPromise(this.prefs);
            
    //     return (new UserPreferences()).load(args)
    //         .then((prefs) => {
    //             this.prefs = prefs;
    //             return prefs;
    //         });
    // }

    matchPassword(password: string): boolean {
        // verify the password
        password = User.digestPassword(this.email, password);
        return this.password === password;
    }
    
    // loadPreferences() {
    //     return UserPreferences.loadFromId(this._id)
    //         .then(function(prefs) {
    //             this.prefsLoaded = true;
    //             if (!prefs) {
    //                 this.prefs = new UserPreferences();
    //                 return this.prefs.create(this);
    //             }
                
    //             this.prefs = prefs;
    //             return PromiseUtil.resolvedPromise(this);
    //         }.bind(this));
    // },
    
    resetPassword() {
        this.passwordResetCode = (new Guid()).toString();
        this.passwordResetExpiryDate = (new Date()).offsetTime(24, 0, 0, 0); /// expires in 12 hours
        
        return this.dbUpdate({
            $set: {
                passwordResetCode: this.passwordResetCode,
                passwordResetExpiryDate: this.passwordResetExpiryDate
            }
        });
    }
    
    resetPasswordChange(passwordResetCode, password) {
        if (!this.passwordResetCode) {
            return PromiseUtil.failedPromise({
                status: 404,
                message: "No password reset instruction received!"
            });
        }
        
        if (this.passwordResetCode !== passwordResetCode) {
            return PromiseUtil.failedPromise({
                status: 403,
                message: "Access Denied: Invalid password reset token!"
            });
        }
        
        let now = new Date();
        if (now > this.passwordResetExpiryDate) {
            return PromiseUtil.failedPromise({
                status: 408,
                message: 'Password reset token timed out!'
            });
        }

        this.passwordResetCode = "";
        
        // now update the password
        let passwordDigest = User.digestPassword(this.email, password);
        
        return this.dbUpdate({
            query: {
                _id: this._id
            },
            
            $set: {
                password: passwordDigest,
                passwordResetCode: ""
            },
        });
    }
    
    anonymousLoad(args) {
        Debug.assert(args.deviceId, 'Invalid device id received for anonymously loading a new user!');

        return this.dbLoadOne({
            query: {
                'devices._id': args.deviceId
            }
        }).then((user) => {
            // if no user was found then we create a new one
            if (!user) {
                this.initCreate(args);
                this.email = args.deviceId + '@eido.com';
                this.accountStatus = AccountStatus.auto;
                
                return this.dbInsert();
            }

            // otherwise ... send the user
            return PromiseUtil.resolvedPromise(user);
        });
    }

    initCreate(args) {
        super.initCreate(args);
        return this;
    }

    isAutoAccount() {
        return this.accountStatus === AccountStatus.auto;
    }

    createInternal(args) {
        this.initCreate(args);
        this.accountType = AccountType.eido;
        this.verificationCode = (new Guid()).toString();
        this.level = args.userLevel || UserLevel.normal;

        // if an email address and password were specified then we just use those 
        // otherwise we will use the device id for creating a temporary account for the user!
        if (args.email) {
            console.debug(`[User] Creating a new user with email: ${args.email}`);
            this.password = User.digestPassword(args.email, args.password);
        }
        else {
            Debug.assert(args.deviceId, 'Invalid device id!');

            console.trace('[User] Creating a new user with device id:', args.deviceId);

            this.email = args.deviceId + '@eido.com';
            this.accountStatus = AccountStatus.auto;
            this.password = '';
        }

        return this.dbInsert()
            // .then(() => {
            //     this.prefs = new UserPreferences();
            //     // TODO: !!!
            //     // return this.prefs.create(this._id);
            //     return PromiseUtil.resolvedPromise(this.prefs);
            // })
            .then(() => {
                // send an email to verify the email address of the user ...
                return this.sendVerificationEmail(Util.getServerUrl(args._req));
            });
    }
    
    create(args) {
        // if (!eido.appServer.isTestEnvironment()) {
            this._id = ObjectID.create();
            return this.createInternal(args);
        // }
        
        // return eido.Util.getSequencedObjectId("user_ids")
        //     .then(function(id) {
        //         this._id = id;
        //         return this.createInternal(args);
        //     }.bind(this));
    }
    
    verifyAccount(verificationCode) {
        if (this.verificationCode !== verificationCode) {
            return PromiseUtil.failedPromise({
                status: 403,
                message: 'Access Denied: Invalid verification code!'
            });
        }
        
        this.verificationCode = "";
        return this.dbUpdate({
            query: {
                _id: this._id
            },
                
            $set: {
                verificationCode: ""
            }
        });
    }
    
    sendVerificationEmail(hostname) {
        return Email.sendNewUser(this.email, this.verificationCode, hostname)
            .then(() => {
                return this;
            });
    }

    changePassword(args: ServerArgs, oldPassword: string, newPassword: string) {
        let oldPasswordDigest = User.digestPassword(this.email, oldPassword);

        // make sure that the old password matches ...

        // TODO: return proper error codes over here ...
        if (oldPasswordDigest !== this.password) {
            return PromiseUtil.failedPromise({
                status: 412,
                message: "Old password does not match!"
            });
        }
        else if (!newPassword) {
            return PromiseUtil.failedPromise({
                status: 400,
                message: "Invalid new password!"
            });
        }

        let newPasswordDigest = User.digestPassword(this.email, newPassword);

        // make sure that the new password is not the same as the old one
        if (newPasswordDigest === oldPasswordDigest) {
            return PromiseUtil.failedPromise({
                status: 406,
                message: "New password the same as the old password!"
            });
        }

        // otherwise update the password ...
        return this.dbUpdate({
            query: {
                _id: this._id
            },
            $set: {
                password: newPasswordDigest
            }
        }).then(() => {
            this.password = newPasswordDigest;

            /// Log the user out
            this.logout(args);
            return this;
        });
    }

    // addDevice(device, update) {
    //     this.devices.push(device);

    //     if (update) {
    //         return this.dbUpdate({
    //             doAudit: true,
    //             query: {
    //                 _id: this._id
    //             },
    //             $push: {
    //                 devices: device
    //             }
    //         });
    //     }

    //     return PromiseUtil.resolvedPromise(this);
    // }

    isDeviceRegistered(rhs) {
        // for (let i = 0; i < this.devices.length; i++) {
        //     let device = this.devices[i];
        //     if (device.isSame(rhs))
        //         return true;
        // }

        return false;
    }

    updateSessionInfo(sessionInfo) {
        return PromiseUtil.resolvedPromise(this);
    }

    login(args) {
        let session = new Session();
        args._cached.session = session;

        return session.create(args)
            .then((session) => {
                this.accessToken = args._accessToken;
                return this;
            });
    }

    logout(args: ServerArgs) {
        console.log(`[User] Logging out: ${this._id}`);
        // args._cached.session.delete(args);
        (args._req as any).logout();
        // args.gotoLogin();
    }

    linkEmails(emails) {

        if (!this.linkedEmails)
            this.linkedEmails = [];

        let toLink = [];

        emails.forEach((email) => {
            if (this.linkedEmails.indexOf(email) < 0 && this.email !== email)
                toLink.push(email);
        });

        if (toLink.length) {
            this.linkedEmails = this.linkedEmails.concat(toLink);
            return true;
        }

        return false;
    }

    createFromExternalAccount(deviceId: string, info: any, accountType: AccountType) {
        // first of all create just a normal account
        console.info('Creating a new from facebook info:', info);

        if (require('util').isArray(info.emails)) {
            let emails      = info.emails;
            info.emails      = emails[0];

            this.linkEmails(emails.slice(1));
        }

        Debug.assert(info.emails, `Invalid email received from the external account: ${info}`);

        this.email          = info.emails;
        this.accountType    = accountType;
        this.name           = info.name;
        this.creationDate   = new Date();
        this.lastSeen       = new Date();
        this.gender         = info.gender == 'male' ? Gender.male : Gender.female;
        // this.profieImage    = new Image(info.profileImage);

        let authStr         = accountType.toString() + 'Auth';
        this[authStr]       = info.authInfo;

        if (!this.gender)
            this.gender     = Gender.male;

        return this.dbInsert()
            .then(() => {
                return this;
            });
    }

    createFromFacebook(deviceId, facebookInfo) {
        // first of all create just a normal account
        console.info('Creating a new from facebook info:', facebookInfo);
        return this.createFromExternalAccount(deviceId, facebookInfo, AccountType.facebook);
    }

    createFromGPlusInfo(deviceId, gplusInfo) {
        // first of all create just a normal account
        console.info('Creating a new from googlep plus info:', gplusInfo);
        return this.createFromExternalAccount(deviceId, gplusInfo, AccountType.gplus);
    }

    updateAutoAccountInfo(email: string, password: string) {
        Debug.assert(this.isAutoAccount(), 'Cannot update a non-auto account!');
        this.email = email;
        this.password = User.digestPassword(email, password);
        // this.deviceId = deviceId;
        this.accountStatus = AccountStatus.normal;
        this.accountType = AccountType.eido;

        return this.dbUpdate({
            query: {
                _id: this._id
            },
            $set: {
                email: this.email,
                password: this.password,
                // deviceId: this.deviceId,
                accountStatus: this.accountStatus,
                accountType: this.accountType
            }
        });
    }

    updateExternalAccountInfo(info: any, accountType: AccountType) {
        let didLinkEmails: boolean = false;
        let diff: any = {};

        Debug.assert(info.emails.length > 0, 'Emails array must have at least one item!');

        // if this is an auto account then we need to update the diff
        if (this.isAutoAccount()) {
            diff['accountStatus'] = AccountStatus.normal;
            diff['accountType'] = accountType;
            diff['email'] = info.emails[0];
            this.email = info.emails[0];

            console.trace('[User] Auto account found. Updating to email:', this.email);
        }

        didLinkEmails = this.linkEmails(info.emails);

        let authStr = accountType.toString() + 'Auth';
        diff[authStr] = info.authInfo;
        diff['gender'] = info.gender == 'male' ? Gender.male : Gender.female;

        if (!diff['gender'])
            diff['gender'] = Gender.male;

        // if (!this.profileImage || !this.profileImage.isValid())
        //     diff.profileImage = new Image(info.profileImage);

        if (!this.name)
            diff.name = info.name;

        // setup for the update entry into the DB
        if (didLinkEmails)
            diff['linkedEmails'] = this.linkedEmails;

        console.trace('[User] Updating account from diff:', diff);
        return this.updateFromDiff(diff);
    }

    isTemporary() {
    }

    updateFacebookAccount(facebookInfo) {
        return this.updateExternalAccountInfo(facebookInfo, AccountType.facebook);
    }

    updateGPlusAccount(gplusInfo) {
        return this.updateExternalAccountInfo(gplusInfo, AccountType.gplus);
    }

    addImage(image) {
        let imageClone = image.cleanClone({
            level: SecrecyLevel.dbEmbedded
        });

        return this.dbUpdate({
            query: {
                _id: this._id,
            },

            level: SecrecyLevel.dbEmbedded,

            $push: {
                images: imageClone
            }
        });
    }

    setProfileImage(args, image) {
        Debug.assert(args && image && image.path && image.thmPath, `Invalid image passed to be set as profile: ${image._id}`);

        let imageClone = image.cleanClone({
            level: SecrecyLevel.dbEmbedded
        });

        return this.dbUpdate({
            query: {
                _id: this._id,
            },

            level: SecrecyLevel.dbEmbedded,

            $set: {
                profileImage: imageClone
            },

            $push: {
                images: imageClone
            }
        }).then(() => {
            // this.images.push(image);
            // this.profileImage = image;
        });
    }

    // sendChatMessage(chat, msg) {
    //     return chat.addMessage(this, msg)
    //         .then(function(chat) {
    //             let targetUser = chat.getOtherUser(this._id);
    //             return eido.Notification.sendPrivateChatMessage(chat, this, targetUser, msg);
    //         }.bind(this));
    // }

    // sendMessage(targetUser, msg) {
    //     // first of all we see whether there is a chat id for conversations with this user
    //     return this.getOrCreateUserChat(targetUser)
    //         .then(function(chat) {
    //             Debug.assert(chat, 'Unable to load conversation chat!');
    //             return this.sendChatMessage(chat, msg);
    //         }.bind(this))
    // }

    // sendEventMessage(event, chat, targetUsers, msg) {
    //     return eido.Notification.sendEventMessage(event, chat, this, targetUsers, msg);
    // }

    setOnline(isOnline) {
        if (this.isOnline === isOnline)
            return PromiseUtil.resolvedPromise(this);

        this.isOnline = isOnline;
        return this.dbUpdate({
            query: {
                _id: this._id
            },
            $set: {
                isOnline: this.isOnline
            }
        });
    }

    // beginConversation(targetUser) {
    //     Debug.assert(!this.prefs.isDisliked(targetUser._id), 'User has been blocked:', targetUser._id.toString());

    //     // create a new chat
    //     let chat = new eido.Chat();

    //     chat.users.push(this);
    //     chat.users.push(targetUser);

    //     let myConversation = new eido.ChatConversation(targetUser._id, chat._id);
    //     let targetConversation = new eido.ChatConversation(this._id, chat._id);

    //     // add the conversation to the user
    //     return this.dbUpdate({
    //         query: {
    //             _id: this._id
    //         },
    //         $push: {
    //             chatConversations: myConversation
    //         }
    //     })
    //     .then(function() {
    //         return targetUser.dbUpdate({
    //             query: {
    //                 _id: targetUser._id
    //             },
    //             $push: {
    //                 chatConversations: targetConversation
    //             }
    //         });
    //     })
    //     .then(function() {
    //         return chat.create(null, eido.ChatType.user);
    //     });
    // },
    
    // getUserConversation(targetUserId) {
    //     return eido.Chat.loadConversation(this._id, targetUserId);
    //     // return this.chatTargets[userId];
    //     // for (let i = 0; i < this.chatConversations.length; i++) {
    //     //     if (this.chatConversations[i].targetId === userId.toString())
    //     //         return this.chatConversations[i];
    //     // }

    //     // return null;
    // }
    
    // getOrCreateUserChat(targetUser) {
    //     return this.getUserConversation(targetUser._id)
    //         .then(function(chat) {
    //             /// use the existing chat ... 
    //             if (chat)
    //                 return PromiseUtil.resolvedPromise(chat);
                    
    //             /// or create a new one
    //             return (new eido.Chat()).createPrivateConversation(this, targetUser);
    //         }.bind(this));
    // }

    update(user) {
        return this.dbUpdateFromDiff(this, user);
    }

    updateFromDiff(diff) {
        return this.dbFindAndModify({
            query: {
                _id: this._id
            },
            $set: diff
        });
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /// Static methods
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    static getResetPasswordVerificationCode(email) {
        Debug.assert(eido.appServer.isTestServer(), 'This function is accessible during test environment only!');
        return User.loadFromEmail(email)
            .then((user) => {
                Debug.assert(user, `[User] No user with email: ${email}`);
                console.log(`[User] Password reset code: ${user.passwordResetCode}`);
                return user.passwordResetCode;
            });
    }
    
    static tryLoadUser(email: string | string[], deviceId: string = "") {
        let emails: string[];
        if (emails instanceof Array)
            emails = email as string[];
        else
            emails = [email as string];

        // try to load the user first
        return User.loadFromEmails(emails)
            .then((users) => {
                // console.log("[User] Num users: %d", users.length);
                if (!users || !users.length) {
                    return User.loadFromDeviceId(deviceId); 
                }

                let user = users[0];
                if (users.length > 1)
                    console.warn(`User is registered already with more than one account: ${users}. Using the first email account`);

                return PromiseUtil.resolvedPromise(user);
            });
    }
    
    static async loadFromUsername(name: string): Promise<User> {
        return CObject["dbLoadOne"]({
            query: {
                name: name
            },
            
            cls: User
        });
    }

    static loadFromIds(ids: ObjectID[]): Promise<User[]> {
        return CObject.loadFromIds(ids, User);
    }

    static loadFromDeviceId(deviceId: string) {
        if (!deviceId)
            return PromiseUtil.resolvedPromise(null);

        return CObject.dbLoadOne({
            query: {
                'devices._id': deviceId
            },
            collectionName: User.collectionName,
            cls: User
        });
    }

    static loadFromEmails(emails: string[]): Promise<User[]> {
        return User.dbLoad({
            query: {
                email: {
                    $in: emails
                },
                accountType: AccountType.eido
            }
        });
    }
    
    static loadFromEmail(email: string): Promise<User> {
        return User.dbLoadOne({
            query: {
                email: email,
                accountType: AccountType.eido
            }
        });
    }

    static getLoggedInUser(args: ServerArgs) {
        // return (new Session()).load(args)
        //     .then(function(session) {
        //         this._error.accessDenied(session, `[User] Access Denied: Unable to find user session: ${Session.getSessionId(this)} [${User.dbName}]`, args);
        //         this._cached.session = session;
        //         return User.loadFromEmail(session.email);
        //     }.bind(args))
        //     .then(function(user) {
        //         this._error.accessDenied(user, `[User] Access Denied: Unable to find user for session: ${Session.getSessionId(this)} [${User.dbName}]`, args);
        //         this._cached.user = user;
        //         this._cached.user.accessToken = this._accessToken;
        //         return user;
        //     }.bind(args));
        return Promise.resolve(args._cached.user);
    }

    static isLoggedInUserOfLevel(args: ServerArgs, level: UserLevel) {
        // get the logged in user and see whether it is null!
        return User.getLoggedInUser(args)
            .then((user) => {
                // also check whether the user is an admin
                args._error.assert(user, 401, `No user logged in!`);
                if (user.hasLevel(level))
                    return user;
                return null;
                // args._error.assert(user && user.hasLevel(level), 401, 'Access Denied: No user logged in or insufficient privileges!');
                // args._cached.user = user;
                // return user;
            });
    }

    static isUserLoggedIn(args: ServerArgs): Promise<User> {
        let user: User = (args._req as any).user as User;

        if (user) {
            args._cached.user = user;
            return PromiseUtil.resolvedPromise(user);
        }
        
        console.trace("[User] Checking user logged in!");

        if (args._isWebSocket && args._cached.user) {
            console.trace('[User] WS request. User already verified: ' + args._cached.user.email);
            return PromiseUtil.resolvedPromise(args._cached.user);
        }

        return User.getLoggedInUser(args)
            .then((user) => {
                if (user)
                    args._cached.user = user;
                return user;
            });
    }

    static isLoggedInUserPortfolioManager(args) {
        return User.isLoggedInUserOfLevel(args, UserLevel.portfolioManager);
    }

    static isLoggedInUserCIO(args) {
        return User.isLoggedInUserOfLevel(args, UserLevel.cio);
    }

    static isLoggedInUserAdmin(args) {
        return User.isLoggedInUserOfLevel(args, UserLevel.admin);
    }

    static isLoggedInUserSuperUser(args) {
        console.log("Checking if the user is a super user!");
        return User.isLoggedInUserOfLevel(args, UserLevel.superUser);
    }

    static digestPassword(usersname: string, password: string, salt: string = null) { 
        if (!salt)
            salt = "0vaskljh98asfdvhjnavs8o7qawefvb,kmjasb vlkj arstardr astyy1j4uwf lyhiearsetihdnia ernstih12yu3ljyeh siaehtiarhsdihjyn eiinrastd34wparsat";

        let crypto = require('crypto');
        let lsalt = salt; // + '-' + username;
        return crypto.createHash('whirlpool').update(password).update("salt").update(lsalt).update(password).digest('hex');
    }

    static validate(args: ServerArgs, email: string, invalidate: boolean = true) {
        if (args.userId) {
            args._error.assert(args.userId, 400, 'No userId passed for validation!');
            
            if (!invalidate && args._cached.targetUser && args._cached.targetUser._id.toString() === args.userId.toString())
                return PromiseUtil.resolvedPromise(args._cached.targetUser);
            
            return User.loadFromId(args.userId());
                // .then(function(user) {
                //     return CObject.validate(this, invalidate, user, ["user", "targetUser"]);
                // }.bind(args));
        }
        else if (email) {
            return User.loadFromEmail(email);
                // .then(function(user) {
                //     return CObject.validate(this, invalidate, user, ["user", "targetUser"]);
                // }.bind(args));
        }
        else {
            args._error.accessDenied(false, "Access Denied: No user session found!", args);
        }
    }

    static createDefaultAdmin() { 
        console.info("[User] Creating default admin account!");

        return (new User()).create({
            email: gDefaultAdminEmail,
            password: "amd123",
            userLevel: UserLevel.superUser
        });
    }
}

export default function init(app) {
    console.info(`[Model] Init SvUser: ${User.dbName}.${User.collectionName}`);

    // initialize the model!
    new User();

    // initialize the indexes!
    let conn = db.Manager.instance.getObjectConn(new db.ConnArgs(User));
    let collection = conn.collection(User.collectionName);

    // collection.createIndex({ email: 1 }, { unique: true }); 

    // console.info("[User] Checking default admin account!");
    // User.tryLoadUser([gDefaultAdminEmail], "")
    //     .then((user) => { 
    //         if (!user) {
    //             console.log("Got no user");
    //             return User.createDefaultAdmin(); 
    //         }
    //     });
}
