import { ObjectID } from 'Shared/Core/ObjectID';
import { Debug } from 'Shared/Core/ShUtil';
import { DB as DBList, Collections } from 'Server/Model/SvModel';

import { CObject } from '../../../Shared/Core/CObject';
import { SecrecyLevel } from '../../../Shared/Model/SecrecyLevel';
import { ModelObject, ModelValue, ModelClassId } from '../../../Shared/Core/BaseObject';

@ModelObject()
export default class TaskResult extends CObject {
    @ModelClassId(TaskResult, "TaskResult") __dummy: string; 

    static get collectionName() { return Collections.taskResults; }
    static get dbName()         { return DBList.eido;             }

    @ModelValue("The unique ID of the task.", SecrecyLevel.public)
    _id: string = "";
    
    @ModelValue("The user id to whom the result belongs.", SecrecyLevel.server)
    userId: ObjectID = null;
    
    @ModelValue("The path handler for this task.", SecrecyLevel.server)
    path: string = "";
    
    @ModelValue("The type of the handler [GET, POST etc.].", SecrecyLevel.server)
    method: string = "";
    
    @ModelValue("The time when this task was created.", SecrecyLevel.public)
    lastUpdated: Date = new Date();
    
    @ModelValue("The http status code.", SecrecyLevel.public)
    httpStatus: number = 200;
    
    @ModelValue("The result for this task.", SecrecyLevel.server)
    result: object = null;
    

    constructor(task: any = null, result: object = null) {
        super();

        if (task) {
            this._id        = task._id;
            this.userId     = task.userId;
            this.path       = task.path;
            this.method     = task.method;
            this.lastUpdated= new Date();
            this.result     = result;
        }
    }

    create(httpStatus: number, result: object): Promise<object> {
        Debug.assert(result, "Invalid result for TaskResult!");
        console.log("[TaskResult] Got result: %s", this._id.toString());
        if (result)
            this.result = result;
        this.httpStatus = httpStatus;
        this.lastUpdated = new Date();

        return this.dbDelete({ noError: true }).then(() => {
            return this.dbInsert()
                .then(() => {
                    return this.result;
                });
        });
    }
}
