import { eido } from 'Shared/Core/ShCore';
import { ObjectID } from "Shared/Core/ObjectID";
import { Error, ErrorGen } from 'Server/Core/Error';
import { ShUtil, Debug, PromiseUtil } from 'Shared/Core/ShUtil';
import { Model } from 'Shared/Model/ShModel';
import { Util } from 'Server/Core/SvUtil';
import { DB as DBList, Collections } from 'Server/Model/SvModel';

import PushNotification from 'Server/System/PushNotification'; 
import { TaskUpdate, TaskFinished } from 'Shared/Model/Notification';

import TaskResult from './TaskResult';
import { TaskStatus, ShTask } from 'Shared/Model/ShTask';
import { ModelObject, ModelValue, ModelClassId } from 'Shared/Core/BaseObject';
import { ServerArgs } from '../../Core/ServerArgs';
import TaskHandler from '../../Core/Task/TaskHandler';
import { db } from '../../Core/DB/DBManager';

@ModelObject()
export class Task extends ShTask {
    @ModelClassId(Task, "Task") __dummy: string; 

    static get collectionName() { return Collections.tasks; }
    static get maxAge()         { return 1 * 24 * 60 * 60;  }
    static get dbName()         { return DBList.eido;       }

    @ModelValue("The user id to whom the task belongs.")
    userId: ObjectID = ObjectID.create();
    
    @ModelValue("The path handler for this task.")
    path: string = "";
    
    @ModelValue("The type of the handler [GET, POST etc.].")
    method: string = "";
    
    @ModelValue("The status of the task.")
    taskStatus: TaskStatus = TaskStatus.queued;
    
    @ModelValue("Message string that gives textual information about the status.")
    statusMessage: string = "";
    
    @ModelValue("What percentage of the task has been completed.")
    percent: number = 0.0;
    
    @ModelValue("The arguments for this task.")
    args: any = null;
    
    @ModelValue("Has the user been notified or not.")
    hasNotified: boolean = false;

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /// Not part of the Model
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    _args                   : ServerArgs;
    _handler                : TaskHandler = null;
    _result                 : any = null;
    _writeResults           : boolean = true;
    
    
    constructor() {
        super();
    }

    create(handler: TaskHandler, serverArgs: ServerArgs, statusMessage: string = "") {
        this._handler       = handler;

        this._id            = handler.getId();
        this.userId         = serverArgs._cached.user._id;
        this.path           = serverArgs._path;
        this.method         = serverArgs._method;
        this.taskStatus     = TaskStatus.queued;
        this.statusMessage  = statusMessage || "Queued ..."; 
        this.percent        = 0.0;
        this.args           = handler.getArgs();
        this.creationDate   = new Date();
        this.lastUpdated    = new Date();
        this.hasNotified    = false;

        console.info("[Task] Creating: %s", this._id);
        
        /// This can potentially be an insert
        return this.dbDelete({ noError: true }).then(() => {
            return this.dbInsert({ upsert: true });
        });
    } 

    updateStatusMessage(statusMessage) {
        this.statusMessage = statusMessage;

        // console.trace("[Task] Update Status: %s [%s]", statusMessage, this._id);

        // return this.dbUpdate({
        //     $set: {
        //         statusMessage: this.statusMessage
        //     }
        // });

        return this.update(this.percent, statusMessage);
    }

    update(percent, statusMessage) {
        this.percent = percent;

        // console.log("[Task] Update: %s - %s [%s]", statusMessage, percent.toFixed(2), this._id);

        // return this.dbUpdate({
        //     $set: {
        //         percent: this.percent,
        //         statusMessage: this.statusMessage
        //     }
        // });

        return this.updateStatus(this.taskStatus, percent, statusMessage);
    }

    updateStatus(taskStatus: number, percent: number, statusMessage: string) {
        this.taskStatus = taskStatus;
        this.statusMessage = statusMessage || this.statusMessage;

        let updateObj: any = {
            taskStatus: this.taskStatus,
            statusMessage: this.statusMessage
        };

        if (percent) {
            this.percent = percent;
            updateObj.percent = percent;
        }

        /// Send the update to the client side ...
        if (taskStatus !== TaskStatus.finished && taskStatus !== TaskStatus.terminated)
            (new TaskUpdate(percent, this.statusMessage)).wsSend([this.userId]);
        else
            (new TaskFinished(taskStatus, statusMessage, this._result.result)).wsSend([this.userId]);

        return this.dbUpdate({
            $set: updateObj
        });
    }

    begin(statusMessage: string = "") {
        this._result = new TaskResult(this);

        return this.updateStatus(TaskStatus.working, 0.0, statusMessage || "Working ...")
            .then(() => {
                this.args._error = new ErrorGen(this._args._res);
                this.args._res = this;

                return this._handler.runTask();
            })
            .then((result) => {
                console.info("[Task] Writing result: %s", this._id);
                /// Write the result to the DB
                // return (new TaskResult(this, result)).create();
                if (this._writeResults)
                    return this._result.create(200, result);
                return PromiseUtil.resolvedPromise(this);
            })
            .then(() => {
                return this.end();
            })
            .then(() => {
                return this._result;
            })
            .catch((err) => {
                Debug.assert(!this._result.result, "Invalid protocol followed while returning result from a Task! Please use ErrorGen and Error (args._error.assert, throwIf etc.) as you normally would in case of HTTP.");
                console.error("[Task] Error!");
                console.error(err);
                this.updateStatus(TaskStatus.failed, this.percent, err && err.message ? err.message : "Failed!");

                this._result.httpStatus = err.code || 500;
                if (this._writeResults)
                    this._result.create(err);
            });
    }

    end(statusMessage: string = "") {
        console.info(`[Task] End: ${this._id}`);
        return this.updateStatus(TaskStatus.finished, 1.0, statusMessage || "Finished!");
    }

    terminate(statusMessage: string = "") {
        console.info(`[Task] Terminate: ${this._id}`);
        return this.updateStatus(TaskStatus.terminated, 1.0, statusMessage || "Terminated!");
    }

    //////////////////////////////////////////////////////////////////////////////////////
    /// Only to be used by the error function and NOT by normal Task handling. The 
    /// communication to the client about the status of the Task is done by the Task
    /// itself over WebSocket!
    //////////////////////////////////////////////////////////////////////////////////////
    status(httpStatus) {
        this._result.httpStatus = httpStatus;
        return this;
    }

    sendResult(result) {
        this._result.result = result;
        return this;
    }
}

export default function init(app) {
    console.info("[Model] Init SvTask");

    // initialize the model!
    new Task();
    new TaskResult();

    // initialize the indexes!
    let conn = db.Manager.instance.getObjectConn(new db.ConnArgs(Task));
    
    let taskColl = conn.collection(Task.collectionName);
    /// TODO: Re-enable when we can afford more of Azure Cosmos DB collections
    // taskColl.createIndex({ userId: 1 }, { expireAfterSeconds: Task.maxAge });
    // taskColl.createIndex({ userId: 1, path: 1, method: 1 }, { expireAfterSeconds: Task.maxAge });
    
    let taskResultColl = conn.collection(TaskResult.collectionName);
    // taskResultColl.createIndex({ _id: 1, lastUpdated: 1 });
    // taskResultColl.createIndex({ lastUpdated: 1 }, { expireAfterSeconds: Task.maxAge });
    // taskResultColl.createIndex({ userId: 1 });
    // taskResultColl.createIndex({ userId: 1, path: 1, method: 1 });
}
