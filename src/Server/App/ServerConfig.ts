import { Env, eido } from '../../Shared/Core/ShCore';
import * as path from 'path';
import { Util } from '../Core/SvUtil';
import { AssetClass } from 'Shared/Model/Config/ShAlpha';
import { Debug } from 'Shared/Core/ShUtil';

export class Macro {
    name                    : string = "";
    value                   : string = "";
}

export class CacheInfo {
    root                    : string = "";
    markets                 : string[] = [];
}

export class PathValue {
    rawPath                 : string = "";
    path                    : string = "";

    constructor(path: string) {
        this.rawPath        = path;
        this.path           = path;
    }
}

export class ThumbnailsConfig {
    dbName                  : string = "pesa";
}

export class AlphaConfig {
    defaultSrc              : string;
    defaultExpr             : string;
    startDate               : string;
    minOutSamplePercent     : string;
    maxOutSamplePercent     : string;
    defOutSamplePercent     : string;
}

export class UniverseInfo {
    name                    : string;
    subName                 : string;
    superName               : string;
}

export class MarketInfo {
    name                    : string;
    assetClass              : AssetClass;
    title                   : string;
    universes               : UniverseInfo[];
}

export class Settings {
    defMarket               : string;
    defUniverse             : string;
    markets                 : MarketInfo[];
}

export class ServerConfig {
    rawConfig               : object = {};
    macros                  : Macro[] = [];
    macrosLUT               : object = {};
    strategiesTitleLookup   : object = {};
    strategiesPathLookup    : object = {};
    cacheInfo               : CacheInfo = new CacheInfo();
    psimDir                 : string = "";
    psimPath                : string = "";
    psimVersion             : string = "";
    groupPnl                : PathValue = null;
    resDir                  : PathValue = null;
    alphaDir                : PathValue = null;
    strategyDir             : PathValue = null;
    alpha                   : AlphaConfig = null;
    thumbnails              : ThumbnailsConfig = null;
    apiRoot                 : string = "/eido";
    apiVersion              : string = "/v1";
    dbName                  : string = "pesa";
    dbPath                  : string = "mongodb://localhost:27017/";
    blobCacheDir            : string = "";
    blobHotAccountName      : string = "";
    blobHotKey              : string = "";
    blobColdAccountName     : string = "";
    blobColdKey             : string = "";

    posContainerId          : string = "pos";
    checkpointContainerId   : string = "checkpoint";
    thmContainerId          : string = "thm";

    assetClass              : AssetClass = AssetClass.Equities;

    constructor(rawConfig: object, assetClass: AssetClass) {
        this.assetClass     = assetClass;
        this.rawConfig      = rawConfig;
        this.apiRoot        = rawConfig["apiRoot"] || this.apiRoot;
        this.apiVersion     = rawConfig["apiVersion"] || this.apiVersion;
        this.dbName         = rawConfig["dbName"] || this.dbName;
        this.dbPath         = rawConfig["dbPath"] || this.dbPath;
        this.dbPath         = this.dbPath.charAt(0) == '$' ? process.env[this.dbPath.substr(1)] : this.dbPath;

        this.initMacros();

        let cacheInfo: CacheInfo = this.rawConfig["cache"];
        this.cacheInfo.root = this.resolvePath(cacheInfo.root);

        this.psimDir        = this.getMacro("PSIM_DIR");
        this.psimPath       = this.getMacro("PSIM_PATH");
        this.psimVersion    = this.getMacro("PSIM_VER");

        this.blobCacheDir   = this.getMacro("BLOB_CACHE_DIR");

        if (!this.blobCacheDir)
            this.blobCacheDir= process.env.BLOB_CACHE_DIR;

        this.blobHotAccountName = process.env.BLOB_HOT_ACCOUNT_NAME;
        this.blobHotKey      = process.env.BLOB_HOT_KEY;

        this.blobColdAccountName = process.env.BLOB_COLD_ACCOUNT_NAME;
        this.blobColdKey    = process.env.BLOB_COLD_KEY;

        Debug.assert(this.blobCacheDir, "Unable to get any blob cache directory!");

        this.resDir         = new PathValue(this.getMacro("RES_DIR"));
        this.resDir.path    = this.resolvePath(this.resDir.path);

        this.alphaDir       = new PathValue(this.rawConfig["alphaDir"].path);
        this.alphaDir.path  = this.resolvePath(this.alphaDir.path);

        this.strategyDir    = new PathValue(this.rawConfig["strategyDir"].path);
        this.strategyDir.path= this.resolvePath(this.strategyDir.path);
        
        this.alpha          = this.rawConfig["alpha"][this.assetClass];
        this.thumbnails     = this.rawConfig["thumbnails"];

        console.log(`[App] Cache root: ${this.cacheInfo.root}`);
    }

    getMacro(name: string): string {
        if (name in this.macrosLUT)
            return this.macrosLUT[name];
        else if (name in process.env)
            return process.env[name];
        return "";
    }

    getPSimDir() {
        if (this.psimDir)
            return this.psimDir;

        /// Otherwise we get it
        this.psimDir = this.resolvePath(this.rawConfig["psimDir"]);
        return this.psimDir;
    }

    getConfigDir(market) {
        let psimDir = this.getPSimDir();
        if (market) 
            return path.join(psimDir, "Configs", market);
        return path.join(psimDir, "Configs");
    }

    getBaseConfigPath(market) {
        let psimDir = this.getPSimDir();
        return path.join(psimDir, "Configs", market, market.toLowerCase() + "_base.xml");
    }

    initMacros() {
        this.macros = new Array(this.rawConfig["macros"].length + 3);

        this.macros[0] = {
            name: "HOME",
            value: Util.getHomeDir()
        };

        let isWin = /^win/.test(process.platform);

        this.macros[1] = {
            name: "PLATFORM",
            value: isWin ? "windows" : "linux"
        };

        this.macros[2] = {
            name: "ASSET_CLASS",
            value: this.assetClass
        }

        this.macros[3] = {
            name: "PESA_DB",
            value: process.env["PESA_DB"]
        }

        let beginIndex = this.macros.length;
        this.rawConfig["macros"].map((macro, index) => {
            let acMacro = macro[this.assetClass];

            if (acMacro) {
                console.info(`Got nested macro for asset class: ${this.assetClass}`);
                macro = acMacro;
            }

            this.macros[index + beginIndex] = {
                name: macro.name,
                value: Util.getVarValue(macro.value)
            }

            console.log(`    => ${this.assetClass} - ${macro.name} = ${this.macros[index + beginIndex].value}`);
        });

        this.macros.map((macro) => this.macrosLUT[macro.name] = this.resolvePath(macro.value));
    }

    resolvePath(path_: string): string {
        let path = path_;

        while (path.indexOf("{$") >= 0) {
            this.macros.map((macro, index) => {
                path = path.replace(new RegExp("\\{\\$" + macro.name + "\\}", 'g'), macro.value);
            });

            path = path.replace(/\\/g, '/');
        }

        return path;
    }
}
