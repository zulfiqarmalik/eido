console.log('Main filename:', require.main.filename);

var Module = require('module').Module;
var nodePath = require('path');

var appModulePaths = [];
var old_nodeModulePaths = Module._nodeModulePaths;

Module._nodeModulePaths = function (from) {
    var paths = old_nodeModulePaths.call(this, from);

    // Only include the app module path for top-level modules
    // that were not installed:
    if (from.indexOf('node_modules') === -1) {
        paths = appModulePaths.concat(paths);
    }

    return paths;
};

function addPath(path) {
    path = nodePath.normalize(path);

    if (appModulePaths.indexOf(path) === -1) {
        appModulePaths.push(path);
        // Enable the search path for the current top-level module
        require.main.paths.unshift(path);
    }
}

// exports.addPath = addPath;
var mainDir = require('path').resolve(__dirname, 'node_modules');
addPath(mainDir);

require('../core/sv_core.js');
let express = require("express")
let app = express();

// Define the port to run on
app.set('port', 3000);
var distDir = "P:\\Work\\Eido\\public\\client\\platform\\browser\\public\\img"

app.use(express.static(distDir));

var server = app.listen(app.get('port'), function () {
    var port = server.address().port;
    console.log('Magic happens on port ' + port);
});


