var domain = require('domain');
var appDomain = domain.create();

import { eido, Env } from '../../Shared/Core/ShCore';
import BaseServer from '../Core/BaseServer';

import initModels from '../Model/SvModel'
import { DB as DBList } from '../Model/SvModel'
import initRouter from '../Services/Router';

import { db } from '../Core/DB/DBManager';

import { ServerConfig } from './ServerConfig';
import { AssetClass } from 'Shared/Model/Config/ShAlpha';
import { Debug } from 'Shared/Core/ShUtil';
import { AzureCDN } from 'Server/System/CDN/AzureCDN';
import * as express from 'express';
import * as path from 'path';

let compression = require('compression');
let cookieParser = require('cookie-parser');

const port = process.env.PORT || 8080;

function injectDebug (req, res, next) {
    let hostname = process.env.WP_HOST || "localhost";
    let devPort = process.env.WP_PORT || 8079;
    let staticPath = 'http://' + hostname + ':' + devPort;

    let dir = "dk";

    let mainjs = 'main.js',
        maincss = 'main.css';

    if (dir === 'rtl') {
        dir = 'rtl';
        mainjs = 'main-rtl.js';
        maincss = 'main-rtl.css';
    }

    res.locals.dir = dir;

    switch (process.env.NODE_ENV) {
        case "development":
            if (dir === 'rtl' && process.env.RTL !== 'true') {
                res.status(500).send('ERROR: Launch with "npm run dev:rtl -s" instead of "npm run dev -s"');
                return;
            }

            res.locals.pretty = true;
            res.locals.app_stylesheets = '\n      <script src=\'' + staticPath + '/assets/js/devServerClient.js\'></script>\n      <script src=\'' + staticPath + '/assets/js/' + mainjs + '\'></script>';
            res.locals.app_scripts = '\n      <script src=\'' + staticPath + '/assets/js/plugins.js\'></script>\n      <script src=\'' + staticPath + '/assets/js/app.js\'></script>';
            break;
        default:
            res.locals.app_stylesheets = '\n      <link rel=\'stylesheet\' href=\'/css/' + maincss + '\' />';
            res.locals.app_scripts = '\n      <script src=\'/js/plugins.js\'></script>\n      <script src=\'/js/app.js\'></script>';
            break;
    }

    next();
};

export class AppServer extends BaseServer {
    private configs     : { [assetClass: string]: ServerConfig };

    constructor(args: any) {
        args.jwtSecret = "jiahtidhatiaiohdiharisehd093jthdenthd8jvaybenbxud";

        if (!args.config)
            args.config = "./serverConfig.json"

        super(args);
    }

    config(assetClass: AssetClass) {
        Debug.assert(assetClass && this.configs[assetClass], "Invalid asset class passed for getting the config!");
        return this.configs[assetClass];
    }

    start(...args) {
        let distDir = process.cwd();
        let errCode = super.start();

        this.expressApp.use(compression());
        this.expressApp.use(cookieParser());
        // this.expressApp.use(express.static(path.join(process.cwd(), 'Public')));
        // this.expressApp.set('views', path.join(distDir, 'views'));
        // this.expressApp.set('view engine', 'pug');

        this.expressApp.get(
            ['/dk/*'], injectDebug, (req, res, next) => {
                // const content = renderApp(req.url);
                // res.render('index', {data: false, content });
                // res.render('index');
                res.sendFile(require('path').join(distDir, 'static', 'views', 'index.html'));
        });

        // this.expressApp.get(
        //     ['*', '/*', '/dk/*', '/dk/vcat/*', '/dk/vcat/*/vstrat/*', '/dk/vcat/*/vconfig/*', '/dk/settings/*'], 
        //     RubixAssetMiddleware('dk'), (req, res, next) => {
        //         renderHTML(req, res);
        //     });
    }

    async prepare() {
        this.configs                        = {};
        this.configs[AssetClass.Equities]   = new ServerConfig(this.rawConfig, AssetClass.Equities);
        this.configs[AssetClass.Futures]    = new ServerConfig(this.rawConfig, AssetClass.Futures);

        this.dbPath                         = this.configs[AssetClass.Equities].dbPath;
        this.dbName                         = this.configs[AssetClass.Equities].dbName;

        this.expressApp                     = super.initServer();

        let thmDir                          = path.join(this.configs[AssetClass.Equities].blobCacheDir, 'thm');
        console.info(`[App] Serving static thumbnails: /static_thm/ => ${thmDir}`);
        this.expressApp.use('/static_thm', express.static(thmDir));
    }

    async initDatabase(dbName_) {
        // test framework does not need any database if we are not doing this locally!
        // if (this.cmdArgs.test && !this.cmdArgs.isLocalTests)
        //  return;
        let dbKeys = Object.keys(DBList);
        let dbNames = new Array(dbKeys.length);

        dbKeys.map((key, index) => dbNames[index] = DBList[key]);
        
        let dbName = this.rawConfig.dbName || this.cmdArgs.db || dbName_ || "pesa";
        dbNames.push(dbName);

        let dbPath = this.dbPath;

        // let dbPath =  ///"mongodb://eido:hmLnlpMo7hPsaUZuQTK6dvQseCesacUeKAk4Mto6vJqtQHpbHlwB0HVaLQK7OIO6a20MXf7nQPzsnG1E7D35Ag%3D%3D@eido.documents.azure.com:10255/?ssl=true";///this.rawConfig.dbPath;

        // let blobDBPath = this.rawConfig.blobDBPath;

        return await db.Manager.instance.init(dbPath, "", [this.dbName], !this.cmdArgs.db && this.isTestEnvironment());

        // this.cleanStateData();
    }

    async waitForInit() {
        await this.prepare();
        await this.initDatabase(this.rawConfig.dbName);
        this.setCDN(new AzureCDN(this.rawConfig["cdn"]));
    }
}

export default function initApp(cmdArgs) {
    var app = new AppServer(cmdArgs);
    eido.appServer = app;
    
    app.waitForInit()
        .then(() => {
            eido.DB = db.Manager.instance;
            initModels(app);
            initRouter(app);
    
            app.setupSignals(appDomain);
    
            // Checkpoint.load(ObjectID.create("5bf2ef0231bd3195435b049f"), CheckpointType.alpha, "pesa_us_auto");

            // app.start();
            appDomain.run(function() {
                this.start();
            }.bind(app));
        })
        .catch((err) => {
            console.error(err.toString());
        });
}
