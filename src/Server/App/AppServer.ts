import { Env, eido } from '../../Shared/Core/ShCore';

// The same Promise API, everywhere.
import * as Promise from 'bluebird'
(global as any).Promise = Promise

Env.type    = process.env.NODE_ENV || "development";
Env.local   = true;
Env.dev     = true;
Env.preProd = false;
Env.prod    = false;
Env.linux   = false;
Env.windows = false;

function enableDebugPromises() {
    process.env["BLUEBIRD_DEBUG"] = "1";
    require('bluebird').longStackTraces();
}

if (process.platform == 'linux')
    Env.linux = true;
else if (process.platform == 'win32' || process.platform == 'cygwin')
    Env.windows = true;

if (Env.isDev()) {
    console.log('===================================== LOCAL SERVER ===================================== ');
    Env.local = true;
    Env.dev = true;
    enableDebugPromises();
}
else if (Env.isPreProd()) {
    console.log('===================================== PREPROD SERVER ===================================== ');
    Env.dev = false;
    Env.preProd = true;
    enableDebugPromises();
}
else if (Env.isProd()) {
    console.log('===================================== PRODUCTION SERVER ===================================== ');
    Env.prod = true;
    Env.release = true;
    Env.dev = false;
    Env.debug = false;
}

if (Env.dev) {
    console.log('Setting up source maps!');
    require('source-map-support').install();
}

// eido.serverTests = [];
//NODE_ eido.clientTests = [];
// eido.clientOnly = false;

// eido.testSuite = function(testSuite) {
//     if (testSuite.client === true || eido.clientOnly === true)
//         eido.clientTests.push(testSuite);
//     else
//         eido.serverTests.push(testSuite);
// }

import parseCmdArgs from '../Core/CmdArgs';
import initApp from './EidoApp';
import '../Core/SvCore';
import '../Model/SvModel';
// require('../model/sv_model.js');

// do a bit of a test over here
// var k = require('karma');
// var karma = require('karma').server;
// var path = nodePath.resolve(__dirname, "..", "..", "public", "client", "ionic")
// karma.start({
//     configFile: nodePath.join(path, "tests/ionic.conf.js"),
//     singleRun: true
// }, function() {
//     debugger;
//     console.log('Done with tests');
// });

let cluster = require('cluster');
let numCPUs = 1; //require('os').cpus().length;
let restartCount = 0;
let maxRestartCount = 10;

// process.on('unhandledRejection', err => console.error(`[App-Except] ${require('long-promise').getLongStack(err)}`));

if (cluster.isMaster) {
    let cmdArgs = parseCmdArgs(); 

    if (process.env.eido_NUM_WORKERS)
        cmdArgs.threads = parseInt(process.env.eido_NUM_WORKERS);

    if (cmdArgs.threads == 0 && (Env.prod && Env.release)) // use -j0 to start with max threads
        cmdArgs.threads = 4; ///require('os').cpus().length;
    else if (cmdArgs.threads == null || cmdArgs.threads == undefined)
        cmdArgs.threads = 1;

    let numWorkers = cmdArgs.threads || numCPUs;
    console.log(`[AppS] Num Workers: ${numWorkers}`);

    if (cmdArgs.threads && cmdArgs.threads != 1 && !cmdArgs.test) {
        // Fork workers.
        for (let i = 0; i < numWorkers; i++) {
            cluster.fork();
        }

        cluster.on('exit', function(worker, code, signal) {
            if (restartCount >= maxRestartCount) {
                console.log('[AppS] CLUSTER_MASTER_KILLING!');
                process.exit(1);
                return;
            }

            restartCount++;
            console.log(`[AppS] CLUSTER_MASTER! Worker ${worker.process.pid} died! Code: ${code}. Spawning a new one!`);
            cluster.fork();
        });
    }
    else {
        console.log('[AppS] Starting server in master!');
        initApp(cmdArgs);
    }
} 
else {
    console.log('[AppS] Starting worker!');
    let cmdArgs = parseCmdArgs();
    initApp(cmdArgs);
}
