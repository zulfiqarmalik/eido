var path = require('path');
var webpack = require('webpack');
const TsconfigPathsPlugin = require('tsconfig-paths-webpack-plugin');

let baseDir = process.cwd();
let outputDir = path.join(baseDir, "dist", "Server");
console.log(`Output: ${outputDir}`);

var isProduction = process.env.NODE_ENV === 'production';

let rules = [
    // All files with a '.ts' or '.tsx' extension will be handled by 'awesome-typescript-loader'.
    { test: /\.tsx?$/, loader: "awesome-typescript-loader" },
];

if (!isProduction) {
    // All output '.js' files will have any sourcemaps re-processed by 'source-map-loader'.
    rules.push({ enforce: "pre", test: /\.js$/, loader: "source-map-loader" });
}

module.exports = {
    entry: {
        app: "./src/Server/App/AppServer.ts"
    },
    output: {
        filename: "server.js",
        path: outputDir
    },
    target: 'node',

    // Enable sourcemaps for debugging webpack's output.
    // devtool: !isProduction ? "source-map" : undefined,

    optimization: {
        minimize: false // isProduction
    },

    resolve: {
        // Add '.ts' and '.tsx' as resolvable extensions.
        extensions: [".ts", ".tsx", ".js", ".json", "node_modules"],
        plugins: [
            new TsconfigPathsPlugin({ configFile: "./tsconfig.json" })
        ],
        modules: [
            path.join(baseDir, "node_modules"),
            path.join(baseDir, "src/Server"),
            path.join(baseDir, "src/Shared"),
        ]
    },

    resolve: {
        // Add '.ts' and '.tsx' as resolvable extensions.
        extensions: [".ts", ".tsx", ".js", ".json"],
        plugins: [
            new TsconfigPathsPlugin({ configFile: "./tsconfig.json" }),
            // extractSass
        ],
        modules: [
            "./node_modules",
            "./src/Shared",
            "./src/Server"
        ]
    },

    module: {
        rules: rules
    }
}