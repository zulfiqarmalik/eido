var path = require('path');
const TsconfigPathsPlugin = require('tsconfig-paths-webpack-plugin');

let baseDir = process.cwd();
let outputDir = path.join(baseDir, "dist", "Client");
console.log(`Output: ${outputDir}`);

var isProduction = process.env.NODE_ENV === 'production';

let rules = [
    // All files with a '.ts' or '.tsx' extension will be handled by 'awesome-typescript-loader'.
    { test: /\.tsx?$/, loader: "awesome-typescript-loader" },
];

if (!isProduction) {
    // All output '.js' files will have any sourcemaps re-processed by 'source-map-loader'.
    rules.push({ 
        enforce: "pre", 
        test: /\.js$/, 
        loader: "source-map-loader",
        exclude: [
            // instead of /\/node_modules\//
            path.join(process.cwd(), 'node_modules')
        ]
    });
}

module.exports = {
    entry: {
        app: "./src/Client/Browser/Src/Main.tsx"
    },
    output: {
        filename: "client.js",
        path: outputDir
    },
    target: 'web',

    // Enable sourcemaps for debugging webpack's output.
    devtool: !isProduction ? "source-map" : undefined,

    optimization: {
        minimize: isProduction
    },

    resolve: {
        // Add '.ts' and '.tsx' as resolvable extensions.
        extensions: [".ts", ".tsx", ".js", ".json"],
        plugins: [
            new TsconfigPathsPlugin({ configFile: "./tsconfig.json" }),
            // extractSass
        ],
        modules: [
            "./node_modules",
            "./src/Shared",
            "./src/Client"
        ]
    },

    module: {
        rules: rules
    },

    node: {
        "mongodb-core": "empty",
        "resolve-from": "empty",
        "fs": "empty",
        "child_process": "empty",
        "connection": "empty",
        "mongodb": "empty",
        "cluster": "empty",
        "resolve-from": "empty",
        "dns": "mock",
        "module": "empty",
        "tls": "mock",
        "net": "mock"
    },

    // When importing a module whose path matches one of the following, just
    // assume a corresponding global variable exists and use that instead.
    // This is important because it allows us to avoid bundling all of our
    // dependencies, which allows browsers to cache those libraries between builds.
    // externals: {
    //     "react": "React",
    //     "react-dom": "ReactDOM"
    // }
};
